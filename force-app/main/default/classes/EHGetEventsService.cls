/*
    Service Reponsible to hanlde the request for Delivery Tracking. 
*/
public class EHGetEventsService implements EHGetEventsInterface {
    final CustomSettings.Record config = null;
    
    Public static final String URLAppend = System.Label.EHURLAppend;
    Public static final String Eholdheaders = System.Label.EHOldHeaders;
    
    public EHGetEventsService(){
        config = CustomSettings.getCalloutSetting(EHConstants.EVENT_HUB_GET_EVENTS);
    }
    
    /* Get the Delivery Tracking Information for the provided OrderId or the Delivery id */
    public virtual  DeliveryTrackingVO getDeliveryTrackingInfo(String orderId, String deliveryId){
        DeliveryTrackingVO delTrackingVO =  null; 
        HttpRequest req = null;
        try {
        	String endpointParam = (String.isNotEmpty(orderId) ? (EHConstants.ORDER_ID + EHConstants.REQUEST_PARAM  + orderId.trim()) : (EHConstants.DELIVERY_ID + EHConstants.REQUEST_PARAM + deliveryId.trim()));
            req = prepareRequest(endpointParam);
            delTrackingVO = invokeWS(req);
            //Check if the response if success then populate the Cases
            if(delTrackingVO.success) {
                delTrackingVO.caseList = getRelatedCases(orderId, deliveryId);              
            }
            system.debug(Logginglevel.INFO,'DeliveryTrackingVO ==> ' + JSON.serialize(delTrackingVO));
        } catch (Exception exp) {
            delTrackingVO = handleException(req, EHConstants.ERROR_MESSAGE_UNEXPECTED +'\n '+ exp.getMessage()+ EHConstants.SPLITTER + exp.getStackTraceString());
        }   
        return delTrackingVO;
    }
    
    /* Get the Returns Tracking Information for the provided OrderId or the Package tracking id */
    public virtual  DeliveryTrackingVO getReturnsTrackingInfo(String orderId, String trackingId){
        DeliveryTrackingVO delTrackingVO =  null; 
        HttpRequest req = null;
        try {
        	String endpointParam = (String.isNotempty(orderId) ? (EHConstants.ORDER_ID + EHConstants.REQUEST_PARAM  + orderId.trim()) : (EHConstants.PACKAGE_ID + EHConstants.REQUEST_PARAM + trackingId.trim()));
            req = prepareRequest(endpointParam);
            delTrackingVO = invokeWS(req);
        } catch (Exception exp) {
            delTrackingVO = handleException(req, EHConstants.ERROR_MESSAGE_UNEXPECTED + EHConstants.NEW_LINE + exp.getMessage()+ EHConstants.SPLITTER + exp.getStackTraceString());
        }   
        return delTrackingVO;
    }
    
    // Method that makes the web service callout.
    public static List<Case> getRelatedCases(String orderId, String deliveryId) {
        List<Case> caseList = new List<Case>();
        if(String.isNotBlank(deliveryId)) {
            caseList = new List<Case>([select ID,Origin, Type, status, createddate, CaseNumber, OwnerId, RecordType.Name  from Case where jl_DeliveryNumber__c =: deliveryId.trim()]);
        } else if (String.isNotBlank(orderId)) {
            caseList = new List<Case>([select ID,Origin, Type, status, createddate, CaseNumber, OwnerId, RecordType.Name   from Case where jl_CRNumber__c =: orderId.trim() or jl_OrderManagementNumber__c =: orderId.trim()]);
        }
       system.debug(Logginglevel.INFO,'caseList' + caseList);
        return caseList;
    }
    /* Prepare the HTTP request for the provided input */
    private HttpRequest prepareRequest(String endpointParam) {
        String endpoint = config.getString(EHConstants.END_POINT);
        Integer timeout = Integer.valueof(config.getNumber(EHConstants.TIME_OUT));
        String ssl_clientCertificateName = config.getString(EHConstants.CERTIFICATE_NAME);
        
        HttpRequest req = new HttpRequest();
        req.setMethod(EHConstants.GET_HTTP_METHOD); 
        if (timeout  != null && timeout > 0) { 
            req.setTimeout(timeout);
        }
        if (String.isNotBlank(ssl_clientCertificateName)) { 
            req.setClientCertificateName(ssl_clientCertificateName);
        }
        
        system.debug('@@@ Endpoint Check1' + endpoint);
        system.debug('@@@ Endpoint Check3' + endpointParam);
        
        req.setEndpoint(endpoint + URLAppend + endpointParam);
        
        //Set the headers
        req.setHeader(EHConstants.CONTENT_TYPE,EHConstants.CONTENT_TYPE_JSON);
        req.setHeader(EHConstants.CONNECTION, EHConstants.CONNECTION_VAL);
        req.setHeader(EHConstants.SENDER_ID, EHConstants.SENDER_ID_VAL);
        req.setHeader(EHConstants.CHANNEL_ID, EHConstants.SENDER_ID_VAL);
        
        if(Eholdheaders == 'No'){
        
          req.setHeader(EHConstants.MESSAGE_ID, GuidUtil.NewGuid());
          req.setHeader(EHConstants.TRACKING_ID, GuidUtil.NewGuid());
            
        }
          
      
        
        return req;
    }
    
    /* Invoke the Web Service and populate the response in the VO */
    private DeliveryTrackingVO invokeWS(HttpRequest req) {
        DeliveryTrackingVO responseVo = new DeliveryTrackingVO(); 
        HttpResponse res = null;
        Http h = new Http();
        Long reqStartTime = DateTime.now().getTime();
        try {
            system.debug(Logginglevel.INFO, 'Request [' + req + ']');
            system.debug(Logginglevel.INFO, 'Request HEADER ['+ EHConstants.MESSAGE_ID + EHConstants.SPLITTER + req.getHeader(EHConstants.MESSAGE_ID) + ']');
            system.debug(Logginglevel.INFO, 'Request HEADER ['+ EHConstants.TRACKING_ID + EHConstants.SPLITTER + req.getHeader(EHConstants.TRACKING_ID) + ']');
            res = h.send(req);
            system.debug(Logginglevel.INFO,'Response Time in Milliseconds ==> ' + (DateTime.now().getTime() - reqStartTime) );
            system.debug(Logginglevel.INFO,'res.getStatusCode() [' + res.getStatusCode() + ']');
            system.debug(Logginglevel.INFO,'Response Body [' + res.getBody() + ']');
            //Check if the result is success
            if(EHConstants.NO_DATA_FOUND_STATUS_CODE ==  res.getStatusCode()){
            	responseVo = getErrorInfo(EHConstants.ERROR_TYPE_BUSINESS);
            } else if(EHConstants.SUCCESS_STATUS == res.getStatusCode()) {
				responseVo = (DeliveryTrackingVO)JSON.deserialize(res.getBody() , DeliveryTrackingVO.class);
				responseVo.success = true;
            } else {
            	responseVo = handleException(req, getFormattedReponseInfo(res));
            }
        } catch (System.CalloutException exp) {
            system.debug(Logginglevel.INFO,'error Response Time in Milliseconds ==> ' + (DateTime.now().getTime() - reqStartTime) );
            responseVo = handleException(req, EHConstants.ERROR_MESSAGE_CONNEX_TO_ESB+ EHConstants.NEW_LINE + exp.getMessage()+ EHConstants.SPLITTER +exp.getStackTraceString());
        } catch (System.JSONException exp) {
            system.debug(Logginglevel.INFO,'error Response Time in Milliseconds ==> ' + (DateTime.now().getTime() - reqStartTime) );
            responseVo = handleException(req, EHConstants.ERROR_MESSAGE_JSON_PARSING + EHConstants.NEW_LINE + exp.getMessage()+ EHConstants.SPLITTER  +exp.getStackTraceString());
        } catch (Exception exp) {
            system.debug(Logginglevel.INFO,'error Response Time in Milliseconds ==> ' + (DateTime.now().getTime() - reqStartTime) );
            responseVo = handleException(req, EHConstants.ERROR_MESSAGE_UNEXPECTED + EHConstants.NEW_LINE + exp.getMessage()+ EHConstants.SPLITTER  +exp.getStackTraceString());
        }   
        return responseVo;
    } 
    
	/* Hanlde the exception in case of Errors */    
    public static DeliveryTrackingVO handleException(HttpRequest req, String  expceptionMessage ) {
        system.debug(Logginglevel.ERROR,'Exception occured  ==> ' + expceptionMessage);
        DeliveryTrackingVO responseVo = getErrorInfo(EHConstants.ERROR_TYPE_TECHNICAL);
        if(req != null) {
            //Send Email for the technical errors.
            EmailNotifier.sendNotificationEmail(EHConstants.EMAIL_SUBJECT ,  
            		+ EHConstants.EMAIL_HEADER_MESSAGE
            		+ '  \n\r ' + EHConstants.EMAIL_REQ_HEADING + '  \n\r '
            		+ EHConstants.SPLITTER + req.getEndPoint() + '  \n\r '
            		+ EHConstants.MESSAGE_ID + EHConstants.SPLITTER + req.getHeader(EHConstants.MESSAGE_ID) + '  \n\r '
    				+ EHConstants.TRACKING_ID + EHConstants.SPLITTER + req.getHeader(EHConstants.TRACKING_ID) + '  \n\r '
            		+ '  \n\r' + EHConstants.EMAIL_ERROR_HEADING + '  \n\r ' 
            		+ expceptionMessage);
        }
        return responseVo;
    }
    
    /* 
    Pouplate Error Information from the exception.    
    */
    public static DeliveryTrackingVO getErrorInfo(String errorType){
        DeliveryTrackingVO  delTrackingVO = new DeliveryTrackingVO();
        DeliveryTrackingVO.ErrorInfo errorInfo = new DeliveryTrackingVO.ErrorInfo();
        errorInfo.errortype = errorType;
        delTrackingVO.errorInfo = errorInfo;
        delTrackingVO.success = false;
        return delTrackingVO;
                    } 
                    
    /* Prepare the formatted Http Response */
    private String getFormattedReponseInfo(HttpResponse res) {
    	String  expceptionMessage = null;
    	if(res != null){
    		expceptionMessage = EHConstants.RESPONSE_STATUS + '  \n\r ' + res.getStatusCode() + '  \n\r ';
    		expceptionMessage  = expceptionMessage + EHConstants.RESPONSE_HEADER + '  \n\r '
    								+ EHConstants.MESSAGE_ID + EHConstants.SPLITTER + res.getHeader(EHConstants.MESSAGE_ID) + '  \n\r '
    								+ EHConstants.TRACKING_ID + EHConstants.SPLITTER + res.getHeader(EHConstants.TRACKING_ID) + '  \n\r ';
    		expceptionMessage  = expceptionMessage + EHConstants.RESPONSE_BODY + '  \n\r ' + res.getBody();
        }
        system.debug(Logginglevel.INFO,'Response  [' + expceptionMessage + ']');
    	return expceptionMessage;
    }
}