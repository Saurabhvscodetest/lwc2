/**
* @description  Batch class to force an 'blank' update to Cases which should have moved Queues due to various Entitlement Processes, but have failed. 
*               See COPT-2146 for more information.
* @author       @tomcarman  
* @date         2017-09-19
* @version      1.0
*/

global class BatchCaseForceUpdateForEntitlements implements Database.Batchable<SObject>, Database.stateful{
    
    // Non-static to instance variables to maintain state across batches
    private Set<Id> masterSetOfCaseIdsUpdated;
    @TestVisible private Map<String, String> successfulMap;
    @TestVisible private Map<String, String> unsuccessfulMap;
    
    // Sets of Failed Queues, or Action Queues (derrived from the base/default Queue)                                        
    public Set<Id> nonFailedQueueIds = new Set<Id>();
    public Set<Id> defaultAndActionQueue = new Set<Id>();

    public Set<Id> defaultQueueIds = new Set<Id>();
    public Set<Id> actionQueueIds = new Set<Id>();

    // List of Milestones to consider
    private static List<String> milestoneNames = new List<String>{MilestoneHandlerSupport.FIRST_CONTACT_MILESTONE, 
                                                                    MilestoneHandlerSupport.FIRST_CONTACT_MILESTONE_OLD_ONE,
                                                                    MilestoneHandlerSupport.NEXT_ACTION_DATE,
                                                                    MilestoneHandlerSupport.REOPEN_MILESTONE,
                                                                    MilestoneHandlerSupport.EMAIL_CONTACT,
        															MilestoneHandlerSupport.MILESTONE_VIOLATION,
                                                                    MilestoneHandlerSupport.MILESTONE_WARNING,
                                                                    MilestoneHandlerSupport.NEXT_CASE_ACTIVITY
                                                                }; 
    @TestVisible private Boolean testForceError = false;



    /**
    * @description  Default constructor. 
    *               Initialises stateful variables. Identifies Teams Queues to process (based on Team__c.Batch_Process_Case_Milestones__c custom setting)           
    * @author       @tomcarman  
    */

    global BatchCaseForceUpdateForEntitlements() {

        // If this is first iteration of batch, init the stateful variables
        this.masterSetOfCaseIdsUpdated = new Set<Id>();
        this.successfulMap = new Map<String, String>();
        this.unsuccessfulMap = new Map<String, String>(); 

        setTeamQueuesToProcess();

    }


    /**
    * @description  Main batch start(). Returns Query Locatory with CaseMilestones (and related Cases) that have been identified as in the incorrect Queue.         
    * @author       @tomcarman
    */

    global Database.QueryLocator start(Database.BatchableContext BC) {

        Datetime currentTime = Datetime.now();
        Datetime currentTimePlus30min = currentTime.addMinutes(30);


        return Database.getQueryLocator([
                                    SELECT  Id, 
                                            IsViolated,
                                            TargetDate,
                                            Case.Id,
                                            Case.OwnerId,
                                            Case.CaseNumber,
                                            Case.JL_Next_Response_By__c
                                    FROM    CaseMilestone 
                                    WHERE   CompletionDate = null 
                                            AND MilestoneType.Name IN :milestoneNames
                                            AND Case.EntitlementId != null
                                            AND Case.Is_Owner_A_Queue__c = true 
                                            AND Case.IsClosed = false 
                                            AND Case.Priority != :EntitlementConstants.HIGH_PRIORITY
                                            AND (
                                                    (   
                                                        IsViolated = true 
                                                        AND Case.OwnerId =:nonFailedQueueIds 
                                                    )

                                                    OR 

                                                    (
                                                        IsViolated = false 
                                                        AND TargetDate <= :currentTimePlus30min
                                                        AND Case.OwnerId = :defaultQueueIds
                                                        AND Case.Priority != :EntitlementConstants.MEDIUM_PRIORITY
                                                    )

                                                    OR 

                                                    (
                                                        IsViolated = false
                                                        AND Case.OwnerId = :actionQueueIds
                                                        AND Case.JL_Next_Response_By__c <= :currentTimePlus30min
                                                        AND Case.Priority != :EntitlementConstants.MEDIUM_PRIORITY
                                                    )
                                                )
                                    ORDER BY Case.Id, TargetDate
                                    ]
        );

    }


    /**
    * @description  Scheduler. Can be us§ed to limit scope to 50.            
    * @author       @tomcarman
    */
    global void execute(SchedulableContext sc){

        Database.executeBatch(new BatchCaseForceUpdateForEntitlements(), 50);
    }


    /**
    * @description  Main batch execute. 
    *               Does some further analysis on the Cases returned that are in Warning Queue to limit the amount of uneccessary updates.
    *               Performs a blank update to Cases which need to be forced into correct Queue.
    *               Stores results for final() email.
    * @author       @tomcarman
    */
    global void execute(Database.BatchableContext BC, List<CaseMilestone> scope) {

        Set<Id> caseIdsToUpdate = new Set<Id>();

        for(CaseMilestone caseMilestone : scope) {
            if(!caseIdsToUpdate.contains(caseMilestone.Case.Id)) {
                caseIdsToUpdate.add(caseMilestone.Case.Id);
            }
        }

        masterSetOfCaseIdsUpdated.addAll(caseIdsToUpdate);


        List<Case> caseRecordsToUpdate = new List<Case>();

        caseRecordsToUpdate = MilestoneHandler.setCasePriorityAfterExecutionNonFuture(caseIdsToUpdate);

        // Force DML Exception for Unit Test
        if(Test.isRunningTest() && testForceError) {
            for(Case errorCase : caseRecordsToUpdate) {
                errorCase.SuppliedPhone = 'more_than_40_characters_more_than_40_characters';
            }   
        }

        
        List<Database.SaveResult> updateResults = Database.update(caseRecordsToUpdate, false);


        for(Integer i = 0; i < updateResults.size(); i++) {
            // If Success
            if(updateResults[i].isSuccess()) {
                successfulMap.put((String)updateResults[i].getId(), 'Successully Updated');
            
            // If Error
            } else {

                String errorsConcatenated = '';

                for(Database.Error error : updateResults[i].getErrors()) {
                    if(String.isEmpty(errorsConcatenated)) {
                        errorsConcatenated = error.getMessage();
                    } else {
                        errorsConcatenated = errorsConcatenated + ' | ' + error.getMessage();
                    }
                }

                unsuccessfulMap.put((String)caseRecordsToUpdate[i].Id, errorsConcatenated);
            }
        }

        System.debug('*** | BatchCaseForceUpdateForEntitlements.cls:179 | successfulMap: ' + successfulMap);
        System.debug('*** | BatchCaseForceUpdateForEntitlements.cls:180 | unsuccessfulMap: ' + unsuccessfulMap);

    }


    /**
    * @description  Main batch final() method.
    *               Sends email of results.
    * @author       @tomcarman      
    */
    global void finish(Database.BatchableContext BC) {

        try {

            Config_Settings__c emailEnabledForBatch = Config_Settings__c.getInstance('Enable Email Logging for Batch');
            
            if(emailEnabledForBatch != null && emailEnabledForBatch.Checkbox_Value__c) {

                Integer loggingLevel = 1; // default to 1;

                if(emailEnabledForBatch.Number_Value__c != null) {
                    loggingLevel = Integer.valueOf(emailEnabledForBatch.Number_Value__c);
                }

                if(loggingLevel == 1 || (loggingLevel == 2 && !unsuccessfulMap.isEmpty())) {
                 
                    String emailBody = '';

                    emailBody += 'BatchCaseForceUpdateForEntitlements job has run: \n\n\n';
                    emailBody += 'The following records were updated succesfully: \n\n';
                    emailBody += JSON.serializePretty(successfulMap);
                    emailBody += '\n\n';
                    emailBody += 'The following records encountered errors: \n\n';
                    emailBody += JSON.serializePretty(unsuccessfulMap);

                    EmailNotifier.sendNotificationEmail('BatchCaseForceUpdateForEntitlements Results', emailBody);
                }
            }

        } catch (Exception e) {

            System.debug('*** | BatchCaseForceUpdateForEntitlements.cls:220: EMAIL FAILED: ' + e); // ALlow to silently fail - some sandboxes maybe have reached emailsend limit
            System.debug('*** | BatchCaseForceUpdateForEntitlements.cls:221 | masterSetOfCaseIdsUpdated: ' + masterSetOfCaseIdsUpdated);
            System.debug('*** | BatchCaseForceUpdateForEntitlements.cls:221 | successfulMap: ' + successfulMap);
            System.debug('*** | BatchCaseForceUpdateForEntitlements.cls:222 | unsuccessfulMap: ' + unsuccessfulMap);

        }

    }



    /* Helpers */

    /**
    * @description  Identifies Team Queues to process based on Custom Setting.      
    * @author       @tomcarman
    */
    private void setTeamQueuesToProcess() {
        
        Set<String> defaultQueueNames = new Set<String>();

        for(Team__c team : [SELECT Id, Queue_Name__c FROM Team__c WHERE Batch_Process_Case_Milestones__c = TRUE]) {
            defaultQueueNames.add(team.Queue_Name__c);
        }

        Set<string> queriedQueueNames = new Set<String>();

        for(QueueSObject queueObject : [SELECT Queue.Id, Queue.Name FROM QueueSObject WHERE SObjectType = 'Case' AND Queue.DeveloperName IN :defaultQueueNames]) {
            defaultQueueIds.add(queueObject.Queue.Id);
            queriedQueueNames.add(queueObject.Queue.Name);
        }
		
		if ( ! defaultQueueIds.isEmpty() ) {
			nonFailedQueueIds.addAll(defaultQueueIds);
        	nonFailedQueueIds.addAll(getNonFailedQueueIds(defaultQueueIds));

        	actionQueueIds.addAll(getActionQueueIds(defaultQueueIds));
		}
        

    }


    /**
    * @description  Returns a set of 'Warning' and Actioned' Queue Ids, derived from base Queue Ids         
    * @author       @tomcarman
    * @param        defaultQueueIds   A set of Default/Base Queue Ids   
    * @return       A set of related 'Warning' and 'Actioned' Queue Ids
    */
    private static Set<Id> getNonFailedQueueIds(Set<Id> defaultQueueIds) {

        Set<Id> returnSet = new Set<Id>();

        for(Id defaultQueueId : defaultQueueIds) { 
			//Commented as a part of COPT-3585
            //returnSet.add(TeamUtils.getQueueId(defaultQueueId, TeamUtils.QUEUE_TYPES.WARNING));
            returnSet.add(TeamUtils.getQueueId(defaultQueueId, TeamUtils.QUEUE_TYPES.ACTIONED));
        }

        return returnSet;

    }


    /**
    * @description  Returns a set of 'Actioned' Queue Ids, derived from base Queue Ids          
    * @author       @tomcarman
    * @param        defaultQueueIds   A set of Default/Base Queue Ids   
    * @return       A set of related 'Actioned' Queue Ids
    */
    private static Set<Id> getActionQueueIds(Set<Id> defaultQueueIds) {

        Set<Id> returnSet = new Set<Id>();

        for(Id defaultQueueId : defaultQueueIds) {
            returnSet.add(TeamUtils.getQueueId(defaultQueueId, TeamUtils.QUEUE_TYPES.ACTIONED));
        }
 
        return returnSet;
    }

}