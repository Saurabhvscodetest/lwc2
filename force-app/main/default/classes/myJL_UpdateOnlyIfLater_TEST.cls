//
// The purpose of this test is to validate that only later updates are applied
//
//	Edit	Date		Author		Comment
// 	001 	19/02/15	NTJ			MJL-1570 - Add method to detect whether migration in progress		
//
//  Keys used for upsert
//	Contact_Profile__c.Shopper_ID__c
//	Loyalty_Account__c.Membership_Number__c
//  Loyalty_Card__c.Card_Number__c
//  Potential_Customer__c.Shopper_ID__c
//
//	001		26/02/15	NTJ			MJL-1581 - Make House No a Text field so that it does not display as 999,999 (should be 999999)
//	002		29/06/15	NTJ			MJL-2100 - Use Contact_Profile__c.AnonymousFlag2__c   	

@isTest
private class myJL_UpdateOnlyIfLater_TEST {

    static Contact c;
    static Contact_Profile__c cp;
    static Loyalty_Account__c la;
    static Loyalty_Card__c lc;
    static Potential_Customer__c pc;

    static Contact cX;
    static Contact_Profile__c cpX;
    static Loyalty_Account__c laX;
    static Loyalty_Card__c lcX;
    static Potential_Customer__c pcX;

    static void createTestData() {
        // create the records       
        c = UnitTestDataFactory.createContact();
        insert c;
        
        cp = UnitTestDataFactory.createContactProfile(c);
        cp.Mailing_House_Name__c='Ty Fawr';
		cp.Mailing_Street__c='Mailing_Street__c';
		cp.FirstName__c='FirstName__c';
		cp.Mailing_House_No_Text__c='15';
		cp.Telephone_Number__c='78787878';
		cp.Salutation__c='Mr';
		cp.Initials__c='I';
		cp.Mailing_Address_Line2__c='Mailing_Address_Line2__c';
		cp.MailingCountry__c='MailingCountry__c';
		cp.Date_of_Birth__c=System.Today();
		cp.Mailing_County_Name__c='Mailing_County_Name__c';
		cp.Source_CreatedDate__c=System.Today();
		cp.Extract_ConcatReference__c='Extract_ConcatReference__c';
		cp.Source_LastModifiedDate__c=System.Today();
		cp.Mailing_Address_Line3__c='Mailing_Address_Line3__c';
		cp.LastName__c='LastName__c';
		cp.Shopper_ID__c='Shopper_ID__c';
		cp.Mailing_Address_Line4__c='Mailing_Address_Line4__c';
		cp.MailingCity__c='MailingCity__c';
		cp.Email__c='test@test.comz';
		cp.MailingPostCode__c='MailingPostCode__c';
		cp.SourceSystem__c = 'johnlewis.com';
		cp.Source_LastModifiedDate__c = system.Today();
		cp.AnonymousFlag2__c = true;
        insert cp;
        
        la = UnitTestDataFactory.createLoyaltyAccount(cp);
		la.Name = '89';
		la.Activation_Date_Time__c = system.Today();
		la.Customer_Loyalty_Account_Card_ID__c = '89';
		la.Deactivation_Date_Time__c = system.Today();
		la.email_address__c = 'emai@email.com';
		la.Extract_ConcatReference__c = '89';
		la.IsActive__c = true;
		la.membership_number__c = '89';
		la.myjl_esb_message_id__c = '89';
		la.Registration_Business_Unit_ID__c = '89';
		la.Registration_Email_Sent_Flag__c = true;
		la.Registration_Workstation_ID__c = '89';
		la.Scheme_Joined_Date_Time__c = system.Today();
		la.Welcome_Email_Sent_Flag__c = true;
		la.Source_LastModifiedDate__c = system.Today();
		la.ShopperId__c = 'shopper1';
        insert la;
        
        lc = UnitTestDataFactory.createLoyaltyCard(la, '676767');
		lc.Card_Number__c = '90';
		lc.Extract_ConcatReference__c = '90';
		lc.Force_Orphan_Transaction_Search__c = false;
		lc.MyJL_ESB_Message_ID__c = '90';
		lc.Source_LastModifiedDate__c = system.Today();
		
        insert lc;
        
        pc = UnitTestDataFactory.createPotentialCustomer('email@email.com');
        pc.Mailing_House_Name__c='Ty Fawr';
		pc.Mailing_Street__c='Mailing_Street__c';
		pc.FirstName__c='FirstName__c';
		pc.Mailing_House_No_Text__c='15';
		pc.Telephone_Number__c='78787878';
		pc.Salutation__c='Mr';
		pc.Initials__c='I';
		pc.Mailing_Address_Line2__c='Mailing_Address_Line2__c';
		//pc.MailingCountry__c='MailingCountry__c';
		pc.Date_of_Birth__c=System.Today();
		pc.Mailing_County_Name__c='Mailing_County_Name__c';
		pc.Source_CreatedDate__c=System.Today();
		pc.Extract_ConcatReference__c='Extract_ConcatReference__c';
		pc.Source_LastModifiedDate__c=System.Today();
		pc.Mailing_Address_Line3__c='Mailing_Address_Line3__c';
		pc.LastName__c='LastName__c';
		pc.Shopper_ID__c='Shopper_ID__c';
		pc.Mailing_Address_Line4__c='Mailing_Address_Line4__c';
		pc.Mailing_City__c='Mailing_City__c';
		pc.Email__c='test@test.comz';
		pc.MailingPostCode__c='MailingPostCode__c';
		pc.Source_System__c = 'johnlewis.com';
		pc.Source_LastModifiedDate__c = system.Today();
		pc.AnonymousFlag__c = true;
        insert pc; 

    }
    
    private static void requery() {
        // requery
        //a = [SELECT Id FROM Account WHERE id=:a.Id];
        //c = [SELECT Id FROM Contact WHERE id=:c.Id];
        cpX = [SELECT Id, Source_LastModifiedDate__c, Source_CreatedDate__c, LastModifiedDate, mailing_House_Name__c, Mailing_Street__c, FirstName__c, Mailing_House_No_Text__c, Name, Telephone_Number__c, Salutation__c,
				Initials__c, Mailing_Address_Line2__c, MailingCountry__c, Date_of_Birth__c, Mailing_County_Name__c, Extract_ConcatReference__c, Mailing_Address_Line3__c, LastName__c, Shopper_ID__c, SourceSystem__c,
				Mailing_Address_Line4__c, MailingCity__c, Email__c, MailingPostCode__c, AnonymousFlag2__c FROM Contact_Profile__c WHERE id=:cp.Id];
        system.assertNotEquals(null, cpX);
        laX = [SELECT Id, Source_LastModifiedDate__c, Source_CreatedDate__c, LastModifiedDate, Scheme_Joined_Date_Time__c, Registration_Business_Unit_ID__c, Extract_ConcatReference__c, Activation_Date_Time__c, 
               Registration_Workstation_ID__c, IsActive__c, Welcome_Email_Sent_Flag__c, Name, Membership_Number__c, Deactivation_Date_Time__c, Email_Address__c, Channel_ID__c FROM Loyalty_Account__c WHERE id=:la.Id];
        system.assertNotEquals(null, laX);
        lcX = [SELECT Id, Source_LastModifiedDate__c, Source_CreatedDate__c, LastModifiedDate,  Name, Card_Number__c, Extract_ConcatReference__c FROM Loyalty_Card__c WHERE id=:lc.Id];
        system.assertNotEquals(null, lcX);
        pcX = [SELECT Id, Source_LastModifiedDate__c, Source_CreatedDate__c, LastModifiedDate, Mailing_House_Name__c,  Mailing_Street__c, FirstName__c, Mailing_House_No_Text__c, Telephone_Number__c, Salutation__c, Initials__c,
				Mailing_City__c, Mailing_Address_Line2__c, Date_of_Birth__c, Source_System__c, Mailing_County_Name__c, Extract_ConcatReference__c, Mailing_Address_Line3__c, LastName__c, Shopper_ID__c, Mailing_Address_Line4__c,
                Mailing_Country__c, Email__c FROM Potential_Customer__c WHERE id=:pc.Id];       
        system.assertNotEquals(null, pcX);
    }
    
    // method that changes all the fields
    private static void changeCP() {
        cpX.Mailing_House_Name__c += '2';
		cpX.Mailing_Street__c += '2';
		cpX.FirstName__c+= '2';
		cpX.Mailing_House_No_Text__c='16';
		cpX.Telephone_Number__c+= '2';
		cpX.Salutation__c+= '2';
		cpX.Initials__c+= '2';
		cpX.Mailing_Address_Line2__c+= '2';
		cpX.MailingCountry__c+= '2';
		cpX.Date_of_Birth__c=System.Today()+1;
		cpX.Mailing_County_Name__c+= '2';
		cpX.Extract_ConcatReference__c+= '2';
		cpX.Mailing_Address_Line3__c+= '2';
		cpX.LastName__c+= '2';
		cpX.Shopper_ID__c+= '2';
		cpX.Mailing_Address_Line4__c+= '2';
		cpX.MailingCity__c+= '2';
		cpX.Email__c+= '2';
		cpX.MailingPostCode__c+= '2';
		cpX.SourceSystem__c += '2'; 
		cpx.AnonymousFlag2__c = !cpx.AnonymousFlag2__c;   	
    }
    
    // method that changes all the fields
    private static void changeLA() {
    	laX.Scheme_Joined_Date_Time__c=System.Today()+1;
		laX.Registration_Business_Unit_ID__c += '2';
		laX.Extract_ConcatReference__c += '2';
		laX.Activation_Date_Time__c=System.Today()+1;
		laX.Registration_Workstation_ID__c += '2';
		//laX.Customer_Profile__c=Customer_Profile__r\:Shopper_ID__c
		laX.IsActive__c = !lax.IsActive__c;
		//laX.CreatedDate=Source_CreatedDate__c
		laX.Welcome_Email_Sent_Flag__c = !lax.Welcome_Email_Sent_Flag__c;
		//Name=Name
		//laX.Source_Last_Modified_Date__c=Source_LastModifiedDate__c
		laX.Membership_Number__c += '2';
		laX.Deactivation_Date_Time__c=System.Today()+1;
		laX.Email_Address__c += '2';
		laX.Channel_ID__c+= '2';
	}

    // method that changes all the fields
    private static void changeLC() {
		//Name=Name
		//CreatedDate=Source_CreatedDate__c
		lcX.Card_Number__c += '2';
		//Loyalty_Account__c=Loyalty_Account__r\:Membership_Number__c
		lcX.Extract_ConcatReference__c += '2';
	}
	
    // method that changes all the fields
    private static void changePC() {
        pcX.Mailing_House_Name__c += '2';	
		pcX.Mailing_Street__c += '2';
		pcX.FirstName__c += '2';
		pcX.Mailing_House_No_Text__c = '23';
		pcX.Telephone_Number__c += '2';
		pcX.Salutation__c = 'Mrs';
		pcX.Initials__c += '2';
		pcX.Mailing_City__c += '2';
		pcX.Mailing_Address_Line2__c += '2';
		pcX.Date_of_Birth__c = System.Today() + 1;
		pcX.Source_System__c += '2';
		pcX.Mailing_County_Name__c += '2';
		//pcX.CreatedDate= System.Today() + 1;
		pcX.Extract_ConcatReference__c += '2';
		//pcX.Source_Last_Modified_Date__c=Source_LastModifiedDate__c
		pcX.Mailing_Address_Line3__c += '2';
		pcX.LastName__c += '2';
		pcX.Shopper_ID__c += '2';
		pcX.Mailing_Address_Line4__c += '2';
		pcX.Mailing_Country__c += '2';
		pcX.Email__c += '2';
 	}

	private static void compareCP(Boolean equalsTest) {
		if (equalsTest) {
	 		system.assertEquals(cp.Mailing_House_Name__c, cpX.Mailing_House_Name__c);
	 		system.assertEquals(cp.Mailing_Street__c, cpX.Mailing_Street__c);
	 		system.assertEquals(cp.FirstName__c, cpX.FirstName__c);
	 		system.assertEquals(cp.Mailing_House_No_Text__c, cpX.Mailing_House_No_Text__c);
	 		system.assertEquals(cp.Telephone_Number__c, cpX.Telephone_Number__c);
	 		system.assertEquals(cp.Salutation__c, cpX.Salutation__c);
	 		system.assertEquals(cp.Initials__c, cpX.Initials__c);
	 		system.assertEquals(cp.Mailing_Address_Line2__c, cpX.Mailing_Address_Line2__c);
	 		system.assertEquals(cp.MailingCountry__c, cpX.MailingCountry__c);
	 		system.assertEquals(cp.Date_of_Birth__c, cpX.Date_of_Birth__c);
	 		system.assertEquals(cp.Mailing_County_Name__c, cpX.Mailing_County_Name__c);
	 		system.assertEquals(cp.Extract_ConcatReference__c, cpX.Extract_ConcatReference__c);
	 		system.assertEquals(cp.Mailing_Address_Line3__c, cpX.Mailing_Address_Line3__c);
	 		system.assertEquals(cp.LastName__c, cpX.LastName__c);
	 		system.assertEquals(cp.Shopper_ID__c, cpX.Shopper_ID__c);
	 		system.assertEquals(cp.Mailing_Address_Line4__c, cpX.Mailing_Address_Line4__c);
	 		system.assertEquals(cp.MailingCity__c, cpX.MailingCity__c);
	 		system.assertEquals(cp.Email__c, cpX.Email__c);
	 		system.assertEquals(cp.MailingPostCode__c, cpX.MailingPostCode__c);
	 		system.assertEquals(cp.SourceSystem__c, cpX.SourceSystem__c);			
	 		system.assertEquals(cp.AnonymousFlag2__c, cpX.AnonymousFlag2__c);			
		} else {
	 		system.assertNotEquals(cp.Mailing_House_Name__c, cpX.Mailing_House_Name__c);
	 		system.assertNotEquals(cp.Mailing_Street__c, cpX.Mailing_Street__c);
	 		system.assertNotEquals(cp.FirstName__c, cpX.FirstName__c);
	 		system.assertNotEquals(cp.Mailing_House_No_Text__c, cpX.Mailing_House_No_Text__c);
	 		system.assertNotEquals(cp.Telephone_Number__c, cpX.Telephone_Number__c);
	 		system.assertNotEquals(cp.Salutation__c, cpX.Salutation__c);
	 		system.assertNotEquals(cp.Initials__c, cpX.Initials__c);
	 		system.assertNotEquals(cp.Mailing_Address_Line2__c, cpX.Mailing_Address_Line2__c);
	 		system.assertNotEquals(cp.MailingCountry__c, cpX.MailingCountry__c);
	 		system.assertNotEquals(cp.Date_of_Birth__c, cpX.Date_of_Birth__c);
	 		system.assertNotEquals(cp.Mailing_County_Name__c, cpX.Mailing_County_Name__c);
	 		system.assertNotEquals(cp.Extract_ConcatReference__c, cpX.Extract_ConcatReference__c);
	 		system.assertNotEquals(cp.Mailing_Address_Line3__c, cpX.Mailing_Address_Line3__c);
	 		system.assertNotEquals(cp.LastName__c, cpX.LastName__c);
	 		system.assertNotEquals(cp.Shopper_ID__c, cpX.Shopper_ID__c);
	 		system.assertNotEquals(cp.Mailing_Address_Line4__c, cpX.Mailing_Address_Line4__c);
	 		system.assertNotEquals(cp.MailingCity__c, cpX.MailingCity__c);
	 		system.assertNotEquals(cp.Email__c, cpX.Email__c);
	 		system.assertNotEquals(cp.MailingPostCode__c, cpX.MailingPostCode__c);
	 		system.assertNotEquals(cp.SourceSystem__c, cpX.SourceSystem__c);			
	 		system.assertNotEquals(cp.AnonymousFlag2__c, cpX.AnonymousFlag2__c);			
		}
	}

	private static void compareLA(Boolean equalsTest) {
		if (equalsTest) {
	 		system.assertEquals(la.Scheme_Joined_Date_Time__c, laX.Scheme_Joined_Date_Time__c);
	 		system.assertEquals(la.Registration_Business_Unit_ID__c, laX.Registration_Business_Unit_ID__c);
	 		system.assertEquals(la.Extract_ConcatReference__c, laX.Extract_ConcatReference__c);
	 		system.assertEquals(la.Activation_Date_Time__c, laX.Activation_Date_Time__c);
	 		system.assertEquals(la.Registration_Workstation_ID__c, laX.Registration_Workstation_ID__c);
	 		system.assertEquals(la.IsActive__c, laX.IsActive__c);
	 		system.assertEquals(la.Welcome_Email_Sent_Flag__c, laX.Welcome_Email_Sent_Flag__c);
	 		system.assertEquals(la.Membership_Number__c, laX.Membership_Number__c);
	 		system.assertEquals(la.Deactivation_Date_Time__c, laX.Deactivation_Date_Time__c);
	 		system.assertEquals(la.Email_Address__c, laX.Email_Address__c);
	 		system.assertEquals(la.Channel_ID__c, laX.Channel_ID__c);
		} else {
	 		system.assertNotEquals(la.Scheme_Joined_Date_Time__c, laX.Scheme_Joined_Date_Time__c);
	 		system.assertNotEquals(la.Registration_Business_Unit_ID__c, laX.Registration_Business_Unit_ID__c);
	 		system.assertNotEquals(la.Extract_ConcatReference__c, laX.Extract_ConcatReference__c);
	 		system.assertNotEquals(la.Activation_Date_Time__c, laX.Activation_Date_Time__c);
	 		system.assertNotEquals(la.Registration_Workstation_ID__c, laX.Registration_Workstation_ID__c);
	 		system.assertNotEquals(la.IsActive__c, laX.IsActive__c);
	 		system.assertNotEquals(la.Welcome_Email_Sent_Flag__c, laX.Welcome_Email_Sent_Flag__c);
	 		system.assertNotEquals(la.Membership_Number__c, laX.Membership_Number__c);
	 		system.assertNotEquals(la.Deactivation_Date_Time__c, laX.Deactivation_Date_Time__c);
	 		system.assertNotEquals(la.Email_Address__c, laX.Email_Address__c);
	 		system.assertNotEquals(la.Channel_ID__c, laX.Channel_ID__c);			
		}
	}

	private static void compareLC(Boolean equalsTest) {
		if (equalsTest) {
	 		system.assertEquals(lc.Card_Number__c, lcX.Card_Number__c);
	 		system.assertEquals(lc.Extract_ConcatReference__c, lcX.Extract_ConcatReference__c);
		} else {
	 		system.assertNotEquals(lc.Card_Number__c, lcX.Card_Number__c);
	 		system.assertNotEquals(lc.Extract_ConcatReference__c, lcX.Extract_ConcatReference__c);		
		}
	}

	private static void comparePC(Boolean equalsTest) {
		if (equalsTest) {
	 		system.assertEquals(pc.Mailing_House_Name__c, pcX.Mailing_House_Name__c);
	 		system.assertEquals(pc.Mailing_Street__c, pcX.Mailing_Street__c);
	 		system.assertEquals(pc.FirstName__c, pcX.FirstName__c);
	 		system.assertEquals(pc.Telephone_Number__c, pcX.Telephone_Number__c);
	 		system.assertEquals(pc.Salutation__c, pcX.Salutation__c);
	 		system.assertEquals(pc.Initials__c, pcX.Initials__c);
	 		system.assertEquals(pc.Mailing_City__c, pcX.Mailing_City__c);
	 		system.assertEquals(pc.Mailing_Address_Line2__c, pcX.Mailing_Address_Line2__c);
	 		system.assertEquals(pc.Date_of_Birth__c, pcX.Date_of_Birth__c);
	 		system.assertEquals(pc.Source_System__c, pcX.Source_System__c);
	 		system.assertEquals(pc.Mailing_County_Name__c, pcX.Mailing_County_Name__c);
	 		system.assertEquals(pc.Extract_ConcatReference__c, pcX.Extract_ConcatReference__c);
	 		system.assertEquals(pc.Mailing_Address_Line3__c, pcX.Mailing_Address_Line3__c);
	 		system.assertEquals(pc.LastName__c, pcX.LastName__c);
	 		system.assertEquals(pc.Shopper_ID__c, pcX.Shopper_ID__c);
	 		system.assertEquals(pc.Mailing_Address_Line4__c, pcX.Mailing_Address_Line4__c);
	 		system.assertEquals(pc.Mailing_Country__c, pcX.Mailing_Country__c);
	 		system.assertEquals(pc.Email__c, pcX.Email__c);
		} else {
	 		system.assertNotEquals(pc.Mailing_House_Name__c, pcX.Mailing_House_Name__c);
	 		system.assertNotEquals(pc.Mailing_Street__c, pcX.Mailing_Street__c);
	 		system.assertNotEquals(pc.FirstName__c, pcX.FirstName__c);
	 		system.assertNotEquals(pc.Telephone_Number__c, pcX.Telephone_Number__c);
	 		system.assertNotEquals(pc.Salutation__c, pcX.Salutation__c);
	 		system.assertNotEquals(pc.Initials__c, pcX.Initials__c);
	 		system.assertNotEquals(pc.Mailing_City__c, pcX.Mailing_City__c);
	 		system.assertNotEquals(pc.Mailing_Address_Line2__c, pcX.Mailing_Address_Line2__c);
	 		system.assertNotEquals(pc.Date_of_Birth__c, pcX.Date_of_Birth__c);
	 		system.assertNotEquals(pc.Source_System__c, pcX.Source_System__c);
	 		system.assertNotEquals(pc.Mailing_County_Name__c, pcX.Mailing_County_Name__c);
	 		system.assertNotEquals(pc.Extract_ConcatReference__c, pcX.Extract_ConcatReference__c);
	 		system.assertNotEquals(pc.Mailing_Address_Line3__c, pcX.Mailing_Address_Line3__c);
	 		system.assertNotEquals(pc.LastName__c, pcX.LastName__c);
	 		system.assertNotEquals(pc.Shopper_ID__c, pcX.Shopper_ID__c);
	 		system.assertNotEquals(pc.Mailing_Address_Line4__c, pcX.Mailing_Address_Line4__c);
	 		system.assertNotEquals(pc.Mailing_Country__c, pcX.Mailing_Country__c);
	 		system.assertNotEquals(pc.Email__c, pcX.Email__c);
			
		}
	}

    private static void createCustomSettings() {
		Callout_Settings__c setting1 = new Callout_Settings__c();
		setting1.name = 'OUTBOUND_CONTACT_PROFILE_UPDATE_NOTIFY';
		setting1.Class_Name__c = 'MyJL_ContactProfileHandler';
		setting1.Timeout_Milliseconds__c = 10000;
		setting1.Number_of_Retries__c = 10;
		setting1.Endpoint__c = 'http://localhost';
		setting1.Topic__c = 'topic1';

		Callout_Settings__c setting2 = new Callout_Settings__c();
		setting2.name = 'LEAVE_NOTIFY';
		setting2.Class_Name__c = 'MyJL_LoyaltyAccountHandler.JoinLeaveCallout';
		setting2.Timeout_Milliseconds__c = 10000;
		setting2.Number_of_Retries__c = 10;
		setting2.Endpoint__c = 'http://localhost';
		setting2.Topic__c = 'topic2';

		Database.insert(new List<Callout_Settings__c>{setting1,setting2});  
		
		MyJL_Settings__c setting5= new MyJL_Settings__c();
		setting5.Migration_Mode__c = true;
		Database.insert(new List<MyJL_Settings__c>{setting5});  		  	
    }
    
    private static testMethod void testContactProfile() {
    	createCustomSettings();
        createTestData();
        requery();
        
        system.debug('cpX: '+cpX);
        
        DateTime dtAt = cp.Source_LastModifiedDate__c;
        DateTime dtEarlier = dtAt - 7;
        DateTime dtLater = dtAt + 7;
        
        // test what happens when we have an earlier date, the values should not change
		changeCP();
        cpX.Source_LastModifiedDate__c = dtEarlier;
        update cpX;
        
        requery();
        system.assertEquals(dtAt, cp.Source_LastModifiedDate__c);
		compareCP(true);

        // test what happens when we have a later date
		changeCP();
        cpX.Source_LastModifiedDate__c = dtLater;
        update cpX;
        
        requery();
        system.assertEquals(dtLater, cpX.Source_LastModifiedDate__c);
		compareCP(false);
		
		// test an upsert - this one should create a new contact profile
		cp.Shopper_ID__c = 'Shopper1';
		zeroCP(cp);
		Database.upsert(cp, Contact_Profile__c.Fields.Shopper_ID__c);
        requery();

		changeCP();
		Contact_Profile__c cpY = new Contact_Profile__c();
        cpY.Source_LastModifiedDate__c = dtEarlier;
		cpY.Shopper_ID__c = 'Shopper1';
		cpY.Email__c = 'newemail@newemail.co.uk';
		Database.upsert(cpY, Contact_Profile__c.Fields.Shopper_ID__c);
        
        requery();
        system.assertEquals(dtAt, cp.Source_LastModifiedDate__c);
        system.assertEquals(cp.email__c, cpX.email__c);

        // test what happens when we have a later date
		changeCP();
		Contact_Profile__c cpZ = new Contact_Profile__c();
        cpZ.Source_LastModifiedDate__c = dtLater;
		cpZ.Shopper_ID__c = 'Shopper1';
		cpZ.Email__c = 'newemail@newemail.co.uk';
		Database.upsert(cpZ, Contact_Profile__c.Fields.Shopper_ID__c);
        
        requery();
        system.assertEquals(dtLater, cpX.Source_LastModifiedDate__c);
        system.assertEquals(cpZ.email__c, cpX.email__c);
    }
    
    private static testMethod void testLoyaltyAccount() {
    	createCustomSettings();
        createTestData();
		CalloutHandlerTest.initCalloutSettings(MyJL_LoyaltyAccountHandler.PARTIAL_JOIN_NOTIFY, 'MyJL_LoyaltyAccountHandler.JoinLeaveCallout', 10);

        requery();
        
        system.debug('laX: '+laX);
        
        DateTime dtAt = la.Source_LastModifiedDate__c;
        DateTime dtEarlier = dtAt - 7;
        DateTime dtLater = dtAt + 7;
        
        // test what happens when we have an earlier date, the values should not change
		changeLA();
        laX.Source_LastModifiedDate__c = dtEarlier;
        update laX;
        
        requery();
        system.assertEquals(dtAt, cp.Source_LastModifiedDate__c);
		compareLA(true);

        // test what happens when we have a later date
		changeLA();
        laX.Source_LastModifiedDate__c = dtLater;
        update laX;
        
        requery();
        system.assertEquals(dtLater, laX.Source_LastModifiedDate__c);
		compareLA(false);

		// test an upsert - this one should create a new contact profile
		la.Membership_Number__c = '6777';
		zeroLA(la);
		Database.upsert(la, Loyalty_Account__c.Fields.Membership_Number__c);
        requery();

		changeLA();
		Loyalty_Account__c laY = new Loyalty_Account__c();
        laY.Source_LastModifiedDate__c = dtEarlier;
		laY.Membership_Number__c = '6777';
		laY.Registration_Workstation_ID__c = 'rwid';
		Database.upsert(laY,  Loyalty_Account__c.Fields.Membership_Number__c);
        
        requery();
        system.assertEquals(dtAt, la.Source_LastModifiedDate__c);
        system.assertEquals(la.Registration_Workstation_ID__c, laX.Registration_Workstation_ID__c);

        // test what happens when we have a later date
		changeLA();
		Loyalty_Account__c laZ = new Loyalty_Account__c();
        laZ.Source_LastModifiedDate__c = dtLater;
		laZ.Membership_Number__c = '6777';
		laZ.Registration_Workstation_ID__c = 'rwid';
		Database.upsert(laZ,  Loyalty_Account__c.Fields.Membership_Number__c);
        
        requery();
        system.assertEquals(dtLater, laX.Source_LastModifiedDate__c);
        system.assertEquals(laZ.Registration_Workstation_ID__c, laX.Registration_Workstation_ID__c);
    }    
    
    private static testMethod void testLoyaltyCard() {
    	createCustomSettings();
        createTestData();
        requery();
        
        system.debug('lcX: '+lcX);
        
        DateTime dtAt = lc.Source_LastModifiedDate__c;
        DateTime dtEarlier = dtAt - 7;
        DateTime dtLater = dtAt + 7;
        
        // test what happens when we have an earlier date, the values should not change
		changeLC();
        lcX.Source_LastModifiedDate__c = dtEarlier;
        update lcX;
        
        requery();
        system.assertEquals(dtAt, cp.Source_LastModifiedDate__c);
		compareLC(true);

        // test what happens when we have a later date
		changeLC();
        lcX.Source_LastModifiedDate__c = dtLater;
        update lcX;
        
        requery();
        system.assertEquals(dtLater, lcX.Source_LastModifiedDate__c);
		compareLC(false);

		// test an upsert - this one should create a new contact profile
		lc.Card_Number__c = '8777';
		zeroLC(lc);
		Database.upsert(lc, Loyalty_Card__c.Fields.Card_Number__c);
        requery();

		changeCP();
		Loyalty_Card__c lcY = new Loyalty_Card__c();
        lcY.Source_LastModifiedDate__c = dtEarlier;
		lcY.Card_Number__c = '8777';
		lcY.Extract_ConcatReference__c = 'newemail@newemail.co.uk';
		Database.upsert(lcY, Loyalty_Card__c.Fields.Card_Number__c);
        
        requery();
        system.assertEquals(dtAt, lc.Source_LastModifiedDate__c);
        system.assertEquals(lc.Extract_ConcatReference__c, lcX.Extract_ConcatReference__c);

        // test what happens when we have a later date
		changeCP();
		Loyalty_Card__c lcZ = new Loyalty_Card__c();
        lcZ.Source_LastModifiedDate__c = dtLater;
		lcZ.Card_Number__c = '8777';
		lcZ.Extract_ConcatReference__c = 'newemail@newemail.co.uk';
		Database.upsert(lcZ, Loyalty_Card__c.Fields.Card_Number__c);
        
        requery();
        system.assertEquals(dtLater, lcX.Source_LastModifiedDate__c);
        system.assertEquals(lcZ.Extract_ConcatReference__c, lcX.Extract_ConcatReference__c);
		
    }
    
    private static testMethod void testPotentialCustomer() {    	
    	createCustomSettings();
        createTestData();
        requery();
        
        system.debug('pcX: '+pcX);
        
        DateTime dtAt = pc.Source_LastModifiedDate__c;
        DateTime dtEarlier = dtAt - 7;
        DateTime dtLater = dtAt + 7;
        
        // test what happens when we have an earlier date, the values should not change
		changePC();
        pcX.Source_LastModifiedDate__c = dtEarlier;
        update pcX;
        
        requery();
        system.assertEquals(dtAt, cp.Source_LastModifiedDate__c);
		comparePC(true);

        // test what happens when we have a later date
		changePC();
        pcX.Source_LastModifiedDate__c = dtLater;
        update pcX;
        
        requery();
        system.assertEquals(dtLater, pcX.Source_LastModifiedDate__c);
		comparePC(false);

		// test an upsert - this one should create a new contact profile
		pc.Shopper_ID__c = 'Shopper1';
		zeroPC(pc);
		Database.upsert(pc, Potential_Customer__c.Fields.Shopper_ID__c);
        requery();

		changeCP();
		Potential_Customer__c pcY = new Potential_Customer__c();
        pcY.Source_LastModifiedDate__c = dtEarlier;
		pcY.Shopper_ID__c = 'Shopper1';
		pcY.Email__c = 'newemail@newemail.co.uk';
		Database.upsert(pcY, Potential_Customer__c.Fields.Shopper_ID__c);
        
        requery();
        system.assertEquals(dtAt, pc.Source_LastModifiedDate__c);
        system.assertEquals(pc.email__c, pcX.email__c);

        // test what happens when we have a later date
		changeCP();
		Potential_Customer__c pcZ = new Potential_Customer__c();
        pcZ.Source_LastModifiedDate__c = dtLater;
		pcZ.Shopper_ID__c = 'Shopper1';
		pcZ.Email__c = 'newemail@newemail.co.uk';
		Database.upsert(pcZ, Potential_Customer__c.Fields.Shopper_ID__c);
        
        requery();
        system.assertEquals(dtLater, pcX.Source_LastModifiedDate__c);
        system.assertEquals(pcZ.email__c, pcX.email__c);

    }
    
    private static void zeroCP(Contact_Profile__c cp1) {
    	cp1.Id = null;
		cp1.LastModifiedDate = null;
		cp1.CreatedDate = null;   	
    } 
    private static void zeroLA(Loyalty_Account__c la1) {
    	la1.Id = null;
		la1.LastModifiedDate = null;
		la1.CreatedDate = null;   	
    }
    private static void zeroLC(Loyalty_Card__c lc1) {
    	lc1.Id = null;
		lc1.LastModifiedDate = null;
		lc1.CreatedDate = null;   	
    }
    private static void zeroPC(Potential_Customer__c pc1) {
    	pc1.Id = null;
		pc1.LastModifiedDate = null;
		pc1.CreatedDate = null;   	
    }                    
}