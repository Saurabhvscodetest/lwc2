/*
* @File Name   : SF_Connex_Save_LP_Menu_Options 
* @Description : Rest Resource to connect LP to SF to save the options choosen by the customer
* @Copyright   : JohnLewis
* @Trello #    : #DC331
----------------------------------------------------------------------------------------------------------------
	Ver           Date               Author                     Modification
	---           ----               ------                     ------------
*   1.0         23-MAR-21              VR                        Created
*   1.0         29-MAR-21              VR                        Modified
*/

@RestResource(urlMapping='/SF_ConnexSaveLPMenuOptions/*')
Global class SF_Connex_Save_LP_Menu_Options {
    
    @HttpGet
    global static SF_ConnexToLivePersonWrapper fetchData() {
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
        String customer_Input;
		List<String> customerOptions = new List<String>();
            
        SF_ConnexToLivePersonWrapper livePersonResponse = NEW SF_ConnexToLivePersonWrapper();
			 
        customer_Input = req.params.get('customerChoosenOptions');   
        
        try{
            
            if( customer_Input != null){

                customerOptions = customer_Input.split('&');
				SF_Connex_LP_Tracking_Result.saveCustomerChoosenOptions(customerOptions);
            
            }

            res.statusCode = 200;
            livePersonResponse.statusCode = String.valueOf(res.statusCode);
            livePersonResponse.errorCode = 'DATA_INSERTED';
            
		return livePersonResponse;

        }
        
        catch(Exception e) {
            res.statusCode = 404;

			System.debug('@@@ SF_Connex_Save_LP_Menu_Options Exception message' + e.getMessage());
            System.debug('@@@ SF_Connex_Save_LP_Menu_Options Exception message Line Number' + e.getLineNumber());
            
            livePersonResponse.statusCode = String.valueOf(res.statusCode);
            livePersonResponse.errorCode = 'NO_DATA_INSERTED';
                        
            return livePersonResponse;
        }
    }    
    
    global class SF_ConnexToLivePersonWrapper { 
        
        global String statusCode{get;set;}
        global String errorCode{get;set;}
    }
    
}