@isTest
private class myJLRequestCreateCaseExtension_Test {
    
    static testMethod void myJLRequestCase() {
        PageReference pageRef = Page.myJL_MembershipDetails;
        Test.setCurrentPage(pageRef); 
        
        //Prepare Test Data
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        
        Contact customer = MyJL_TestUtil.createCustomer('Ordering_Customer');
        
        Loyalty_Account__c LA = new Loyalty_Account__c(Name='00000000012', Email_Address__c = 'harry.hoppo@animals.org', IsActive__c = true, ShopperId__c='shopper1');
        LA.Customer_Loyalty_Account_Card_ID__c = 'TestCard';
        LA.Channel_ID__c = 'johnlewis.com';
        LA.Contact__c = customer.id;
        LA.Activation_Date_Time__c = datetime.now();
        Insert LA;
        
        Loyalty_Card__c LC = new Loyalty_Card__c(Name = 'TestCard', Loyalty_Account__c = LA.Id);
        insert Lc;
        
        ApexPages.standardController controller = new ApexPages.standardController(customer);
        myJL_MembershipDetailsExtension pge = new myJL_MembershipDetailsExtension(controller);
        myJL_MembershipDetailsExtension.ProfileWithLoyaltyAccount myJlReq = new myJL_MembershipDetailsExtension.ProfileWithLoyaltyAccount(customer.id,customer,LA);
        myJLReq.getMyJLNewRequest();
        
        Test.setCurrentPage(myJLReq.getMyJLNewRequest()); 
        
        ApexPages.standardController controller1 = new ApexPages.standardController(customer);
        myJL_InformationPageController pge1 = new myJL_InformationPageController(controller1);
        
        pge1.goback();
        //pge1.cancelrequest();
        
        Case ca =  UnitTestDataFactory.createNormalCase(customer);
        
        
        Test.setCurrentPage(pge1.myjlnewrequest());
        ApexPages.standardController controller2 = new ApexPages.standardController(ca);
        ApexPages.currentPage().getParameters().put(myJL_InformationPageController.ADDRESS_LINE_1,customer.Mailing_Street__c);
        ApexPages.currentPage().getParameters().put(myJL_InformationPageController.ADDRESS_LINE_2,customer.Mailing_Address_Line2__c);
        ApexPages.currentPage().getParameters().put(myJL_InformationPageController.ADDRESS_LINE_3,customer.Mailing_Address_Line3__c);
        ApexPages.currentPage().getParameters().put(myJL_InformationPageController.ADDRESS_LINE_4,customer.Mailing_Address_Line4__c);
        myJLRequestCreateCaseExtension pge2 = new myJLRequestCreateCaseExtension(controller2);
        pge2.cancel();
        pge2.save();
        
        Test.startTest();
        List<Case> getcase = new list<Case>([SELECT id,despatch_status__c,status,Address_Line_1__c,Address_Line_2__c, Address_Line_3__c,Address_Line_4__c  FROM Case]);        
        List<MyJL_Outbound_Staging__c> getOutst = new list<MyJL_Outbound_Staging__c>([select id,Case_Reference_ID__c,Address_LIne_1__c, Address_Line_2__c, Address_Line_3__c, Address_Line_4__c  from MyJL_Outbound_Staging__c where Case_Reference_ID__c =:getcase[0].Id]);
        
        
        system.assert(!getcase.isEmpty(), 'Case should be created' );
        Case actualCase = getcase[0];        
        system.assertequals('Closed - No Response Required',actualCase.status); 
        system.assertequals(customer.Mailing_Street__c,actualCase.Address_Line_1__c); 
        system.assertequals(customer.Mailing_Address_Line2__c,actualCase.Address_Line_2__c); 
        system.assertequals(customer.Mailing_Address_Line3__c,actualCase.Address_Line_3__c); 
        system.assertequals(customer.Mailing_Address_Line4__c,actualCase.Address_Line_4__c); 
        system.assertequals('Processing',actualCase.despatch_status__c); 
        
        system.assertequals(1,getOutst.size());
        MyJL_Outbound_Staging__c outboundStagingRecord = getOutst[0]; 
        
        system.assertequals(customer.Mailing_Street__c,outboundStagingRecord.Address_Line_1__c); 
        system.assertequals(customer.Mailing_Address_Line2__c,outboundStagingRecord.Address_Line_2__c); 
        system.assertequals(customer.Mailing_Address_Line3__c,outboundStagingRecord.Address_Line_3__c); 
        system.assertequals(customer.Mailing_Address_Line4__c,outboundStagingRecord.Address_Line_4__c); 
        Test.stopTest(); 
    }
    static testMethod void testRequestOptions(){
        PageReference pageRef = Page.myJL_MembershipDetails;
        Test.setCurrentPage(pageRef); 
        
        //Prepare Test Data
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        
        Contact customer = MyJL_TestUtil.createCustomer('Ordering_Customer');
        
        Loyalty_Account__c la = new Loyalty_Account__c(Name='00000000012', Email_Address__c = 'harry.hoppo@animals.org', IsActive__c = true, ShopperId__c='shopper1');
        la.Customer_Loyalty_Account_Card_ID__c = 'TestCard';
        la.Channel_ID__c = 'johnlewis.com';
        la.Contact__c = customer.id;
        la.Activation_Date_Time__c = datetime.now();
        Insert la;
        
        Loyalty_Card__c lc = new Loyalty_Card__c(Name = 'TestCard', Loyalty_Account__c = LA.Id);
        insert lc;
        
        Case ca =  UnitTestDataFactory.createNormalCase(customer);
        ApexPages.standardController std = new ApexPages.standardController(ca);
        ApexPages.currentPage().getParameters().put('laid', la.Id);
        
        myJLRequestCreateCaseExtension controller = new myJLRequestCreateCaseExtension(std);
        
        System.assertEquals(4, controller.getRequestOptions().size());
        
        Loyalty_Card__c pc = new Loyalty_Card__c(Name = 'TestCard', Loyalty_Account__c = LA.Id, Card_Type__c = MyJL_Const.MY_PARTNERSHIP_CARD_TYPE);
        
        CalloutHandlerTest.initCalloutSettings(MyJL_LoyaltyAccountHandler.FULL_JOIN_NOTIFY, 'MyJL_LoyaltyAccountHandler.JoinLeaveCallout', 10);
        Test.startTest();
        BaseTriggerHandler.removeSkipReason('JoinNotification');
        insert pc;
        Test.stopTest();        
        Case ca2 =  UnitTestDataFactory.createNormalCase(customer);
        ApexPages.standardController std2 = new ApexPages.standardController(ca2);
        ApexPages.currentPage().getParameters().put('laid', la.Id);
        
        myJLRequestCreateCaseExtension controller2 = new myJLRequestCreateCaseExtension(std2);
        controller2.isPartnershipMember = true;
        System.assertEquals(3, controller2.getRequestOptions().size());
    }
}