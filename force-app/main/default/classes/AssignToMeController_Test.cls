@isTest
public class AssignToMeController_Test {
    @isTest
    public static void Assigntometest(){
        User testUser = UnitTestDataFactory.findStandardActiveUser();
        // Create a Contact
        Contact con = UnitTestDataFactory.createContact();
        insert con;		
        Case c = UnitTestDataFactory.createNormalCase(con); 
        // Create Some tasks on this case for someone else
        Task t = new Task();
        t.WhatId = c.Id;
        t.OwnerId = testUser.Id;
        insert t;
        User runningUser = UserTestDataFactory.getRunningUser();
        task tas = [select id,ownerid from task where id=:t.id];
        tas.OwnerId = runningUser.id;
        update tas;
        Test.startTest();
        AssignToMeController.assign(c.id, 'Case');
        AssignToMeController.assign(t.id, 'Task');
        Test.stopTest();
    }
}