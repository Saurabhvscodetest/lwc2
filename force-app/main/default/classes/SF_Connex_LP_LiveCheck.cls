/*
* @File Name   : SF_Connex_LP_LiveCheck 
* @Description : Rest Resource to connect LP to SF to verify the IDV
* @Copyright   : JohnLewis
* @Trello #    : #DC331
----------------------------------------------------------------------------------------------------------------
	Ver           Date               Author                     Modification
	---           ----               ------                     ------------
*   1.0         21-NOV-20              VR                        Created
*   1.0         14-FEB-20              VR                        Modified
*/

@RestResource(urlMapping='/SF_ConnexToLPCheck/*')
Global class SF_Connex_LP_LiveCheck {
    
   Public Static Boolean nameCheck = false;
   Public Static Boolean postCodeCheck = false;
   Public Static Boolean addressCheck = false;
   Public Static List<String> customerInputCheck = new List<String>();
   Public Static String dataInsertCheck;
    
    @HttpGet
    global static SF_ConnexToLivePersonWrapper fetchData() {
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String customer_Email;
        String customer_PostCode;
        String customer_PostCode_NoSpace;
        String customer_Name;
        String customer_address;
        String customer_PhoneNumber;
        String conversationId;
        
        Contact connexContact = new Contact();
        List<Contact> connexContactList = new List<Contact>();
            
        SF_ConnexToLivePersonWrapper livePersonResponse = NEW SF_ConnexToLivePersonWrapper();
        
        try {
            
            customer_Email = req.params.get('customeremail');
            customer_PostCode = req.params.get('customerpostcode');
            customer_Name = req.params.get('customername');
            customer_address = req.params.get('customeraddress');
            conversationId = req.params.get('conversationId');
            customer_PhoneNumber = req.params.get('customerphonenumber');
            customer_PostCode_NoSpace = customer_PostCode.remove(' ');
            
			String temp_customer_PostCode =  '%' + customer_PostCode + '%';
            String temp_customer_PostCode_NoSpace =  '%' + customer_PostCode_NoSpace + '%';
            
            customerInputCheck.add(conversationId);   //customerCheck[0]
            customerInputCheck.add(customer_PhoneNumber); //customerCheck[1]
            
            if( String.isBlank(customer_Email) || String.isBlank(customer_PostCode) || String.isBlank(customer_Name) || String.isBlank(customer_address)){
                 String dataInsertCheck = 'MISSING_PARAMS';
                 customerInputCheck.add(dataInsertCheck);
                
                SF_Connex_LP_Tracking_Result.trackingResultsMissingParams();
                
                 res.statusCode = 500;
                    livePersonResponse.statusCode = String.valueOf(res.statusCode);
                    livePersonResponse.errorCode = 'MISSING_PARAMS';
                
                return livePersonResponse;
            }else{
                
                customerInputCheck.add(customer_Email); //[2]
                customerInputCheck.add(customer_PostCode);//[3]
                customerInputCheck.add(customer_Name);//[4]
                customerInputCheck.add(customer_address);//[5]
                
                connexContactList = [SELECT name,email,MailingPostalCode,MailingStreet,Id from contact where 
                                 (MailingPostalCode like :temp_customer_PostCode  OR
								  MailingPostalCode like :temp_customer_PostCode_NoSpace) AND
                                 email =: customer_Email limit 1];
                
                System.debug('@@@ connexContact' + connexContactList);
                
            if (connexContactList != null && connexContactList.size() > 0) {
                
                customerInputCheck.add(connexContactList[0].Id);//[6]
                
                if(!String.isBlank(customer_Name) && connexContactList[0].name != null){
                    
                    nameCheck = SF_Connex_LP_Connection_Utils.customerNameCheck(customer_Name,connexContactList[0].name);
                }     
                if(!String.isBlank(customer_PostCode)  && connexContactList[0].MailingPostalCode != null){
                    
                   postCodeCheck = SF_Connex_LP_Connection_Utils.customerPostCodeCheck(customer_PostCode,connexContactList[0].MailingPostalCode);  
                }
                if(!String.isBlank(customer_address) && connexContactList[0].MailingStreet != null){
                    
                    addressCheck = SF_Connex_LP_Connection_Utils.customerAddressCheck(customer_address,connexContactList[0].MailingStreet);
                }
                
                System.debug('@@@ connexContact nameCheck' + nameCheck);
                System.debug('@@@ connexContact postCodeCheck' + postCodeCheck);
                System.debug('@@@ connexContact addressCheck' + addressCheck);
                
                if(nameCheck == true && postCodeCheck == true && addressCheck == true) {
                    System.debug('@@@ connexContact line 76');
                    res.statusCode = 200; 
                    livePersonResponse.statusCode = String.valueOf(res.statusCode);
                    livePersonResponse.errorCode = 'CUSTOMER_EXIST';
                    
                    customerInputCheck.add('CUSTOMER_EXIST');//[7]
                    customerInputCheck.add('Bot_Passed');//[8]
                }else if(nameCheck == true && postCodeCheck == true && addressCheck == false){
                    System.debug('@@@ connexContact line 83');
                    res.statusCode = 201; 
                    livePersonResponse.statusCode = String.valueOf(res.statusCode);
                    livePersonResponse.errorCode = 'CUSTOMER_EXIST_ADDRESS_FAIL';
                    
                    customerInputCheck.add('CUSTOMER_EXIST_ADDRESS_FAIL');
                    customerInputCheck.add('Bot_Passed');
                }else if(nameCheck == false && postCodeCheck == true && addressCheck == false){
                    System.debug('@@@ connexContact line 90');
                    res.statusCode = 202; 
                    livePersonResponse.statusCode = String.valueOf(res.statusCode);
                    livePersonResponse.errorCode = 'CUSTOMER_EXIST_ONLY_POSTCODE';
                    
					customerInputCheck.add('CUSTOMER_EXIST_ONLY_POSTCODE');
                    customerInputCheck.add('Bot_Passed');
                }
                else if(nameCheck == false && postCodeCheck == true && addressCheck == true){
                    System.debug('@@@ connexContact line 101');
                    res.statusCode = 202; 
                    livePersonResponse.statusCode = String.valueOf(res.statusCode);
                    livePersonResponse.errorCode = 'CUSTOMER_EXIST_ONLY_POSTCODE';
                    
					customerInputCheck.add('CUSTOMER_NAME_CHECK_FAILED');
                    customerInputCheck.add('Bot_Passed');
                }
                else {
                    System.debug('@@@ connexContact line 98');
                    res.statusCode = 404;
                    livePersonResponse.statusCode = String.valueOf(res.statusCode);
                    livePersonResponse.errorCode = 'NO_DATA_FOUND';
                    
                    customerInputCheck.add('NO_DATA_FOUND');
					customerInputCheck.add('Bot_Failed');
                }
                 
                
            }
            else{
                System.debug('@@@ connexContact line 107');
                    res.statusCode = 500;
                    livePersonResponse.statusCode = String.valueOf(res.statusCode);
                    livePersonResponse.errorCode = 'NO_DATA_FOUND';
                
                	customerInputCheck.add('NO_DATA_FOUND');//6
					customerInputCheck.add('Bot_Failed');//7
                
            }
                
                SF_Connex_LP_Tracking_Result.trackingResults(customerInputCheck);
                
            return livePersonResponse;
                
            }
            
        }
        catch(Exception e) {
            res.statusCode = 404;

			System.debug('@@@ SF_Connex_LP_LiveCheck Exception message' + e.getMessage());
            System.debug('@@@ SF_Connex_LP_LiveCheck Exception message Line Number' + e.getLineNumber());
            
            livePersonResponse.statusCode = String.valueOf(res.statusCode);
            livePersonResponse.errorCode = 'NO_DATA_FOUND';
            
            SF_Connex_LP_Tracking_Result.trackingResultsException();
            
            return livePersonResponse;
        }
    }    
    
    global class SF_ConnexToLivePersonWrapper { 
        
        global String statusCode{get;set;}
        global String errorCode{get;set;}
    }
    
}