@isTest
public class QueueOwnerFilterHelper_TEST {
    
    public static final String CONTACT_CENTRE_TEAM_NAME = '	Hamilton - CRD';
    public static final String CONTACT_CENTRE_QUEUE_NAME = 'CRD_Work_Allocation_Hamilton';

/**
* @description	Test the case escalate feature			
* @author		@ayanhore	
*/    
    @isTest static void testEscalateCaseFeature() {
        
        User runningUser = UserTestDataFactory.getRunningUser();
        
        User contactCentreUser;
        
        Contact testContact;
        Case testCase;
        
        System.runAs(runningUser) {
            
            CustomSettingTestDataFactory.initialiseTeamCustomSettings(CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_QUEUE_NAME);
            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
            system.assert(contactCentreUser.Id != NULL);
            List<QueueOwnerFilter__c> queueOwnerFilterCS = new List<QueueOwnerFilter__c>();
            queueOwnerFilterCS.add(new QueueOwnerFilter__c(Name='COT Hamilton',Filter_from_Bulk_Transfer__c=true,Filter_from_Change_Owner__c=true));
            queueOwnerFilterCS.add(new QueueOwnerFilter__c(Name='CSD',Filter_from_Bulk_Transfer__c=true,Filter_from_Change_Owner__c=false));
            insert queueOwnerFilterCS;
        }
        
        System.runAs(contactCentreUser) {
            test.startTest();
            Set<String> returnValues = new Set<String>();
            returnValues = QueueOwnerFilterHelper.getBulkTransferQueueFilter();
            system.assert(returnValues.size()>0);
            returnValues = QueueOwnerFilterHelper.getChangeOwnerQueueFilter();
            system.assert(returnValues.size()>0);
            returnValues = QueueOwnerFilterHelper.filterQueueRecords('Filter_from_Change_Owner__c');
            system.assert(returnValues.size()>0);
            returnValues = QueueOwnerFilterHelper.getNotFilteredQueueRecords('Filter_from_Change_Owner__c');
            system.assert(returnValues.size()>0);
            Map<String, QueueOwnerFilter__c> returnMap = QueueOwnerFilterHelper.getAllQueueFilterRecords();
            system.assert(returnMap.size()>0);
            test.stopTest();
        }
    }

}