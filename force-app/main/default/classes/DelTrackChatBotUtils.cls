/*
* @File Name   : DelTrackChatBotUtils 
* @Description : Utility class to related to MyJLChatBot Delivery Tracking functionalities  
* @Copyright   : JohnLewis
* @Jira #      : #5174
----------------------------------------------------------------------------------------------------------------
	Ver           Date              Author                     Modification
	---           ----               ------                    ------------
*  10.0      	 12-JAN-21          Vijay A                      Updated
*/

Public with sharing class DelTrackChatBotUtils {

    Public Static List<Case> relatedCaseList = new List<Case>();
    Public Static List<String> delIds;
    Public Static List<String> orderIds;  
    Public Static String JSONResponseString = '';
    Public Static String deliverySlotFrom;
    Public Static String deliverySlotTo;
    Public Static String deliveryPlannedDepartureTime;
    Public Static String deliveryPlannedArrivalTime;
    Public Static String deliveryTimeOnStatus;
    Public Static String dateRequired;
    Public Static String dPTime;
    Public Static String aRTime;
    Public Static Map<String, String> mMonthValue = new Map<String, String>{'Jan' => '01', 'Feb' => '02', 'Mar' => '03', 'Apr' => '04','May' => '05','Jun' => '06','Jul' => '07','Aug' => '08','Sep' => '09','Oct' => '10','Nov' => '11','Dec' => '12'};
  
    Public static String chatBotFetchDeliveryDetails(String deliveryIdInput,String orderIdInput){
        
        WrapperResponse returnWR = new WrapperResponse();
        
        try{
        
        DeliveryTrackingController serviceClass = new DeliveryTrackingController();
        DeliveryTrackingVO result ;    //serviceClass.searchDeliveryLex(orderIdInput,deliveryIdInput);
        				   result = serviceClass.searchDeliveryLex(orderIdInput,'');
        if(result!=null){
            if(result.ErrorInfo != null){
                returnWR.errorType = result.ErrorInfo.errorType;
                returnWR.errorDescription = (EHConstants.ERROR_TYPE_BUSINESS.equalsIgnoreCase(returnWR.errorType) ? 'NO_DATA_FOUND': 'REQUEST_TIMED_OUT')  ;
            }
            else {
   
            if(result.gvfDelInfList!= null && result.gvfDelInfList.size() > 0){
                deliverySlotFrom = result.gvfDelInfList[0].deliverySlotFrom != null ?  result.gvfDelInfList[0].deliverySlotFrom.format('d MMM yyyy HH:mm')   : '';
                deliverySlotTo = result.gvfDelInfList[0].deliverySlotTo != null ?  result.gvfDelInfList[0].deliverySlotTo.format('d MMM yyyy HH:mm')   : '';
                deliveryPlannedDepartureTime = result.gvfDelInfList[0].deliveryPlannedDepartureTime != null ?  result.gvfDelInfList[0].deliveryPlannedDepartureTime.format('d MMM yyyy HH:mm')   : '';
                deliveryPlannedArrivalTime = result.gvfDelInfList[0].deliveryPlannedArrivalTime != null ?  result.gvfDelInfList[0].deliveryPlannedArrivalTime.format('d MMM yyyy HH:mm')   : '';
                deliveryTimeOnStatus = result.gvfDelInfList[0].deliveryTimeOnStatus != null ?  result.gvfDelInfList[0].deliveryTimeOnStatus.format('d MMM yyyy HH:mm')   : '';
                
                returnWR.gvfData = true;
                returnWR.recipientName = result.gvfDelInfList[0].recipientName;
                returnWR.deliveryTypeName = DelTrackConstants.TWOMANGVF;
                returnWR.deliverySlotFrom = deliverySlotFrom;
                returnWR.deliverySlotTo = deliverySlotTo;
                
                returnWR.deliveryPlannedDepartureTime = deliveryPlannedDepartureTime;
                returnWR.deliveryPlannedArrivalTime = deliveryPlannedArrivalTime;
                returnWR.deliveryTimeOnStatus = deliveryTimeOnStatus;
                returnWR.deliveryCDHLocation = String.valueOf( result.gvfDelInfList[0].deliveryCDHLocation);
                returnWR.deliveryStatus = String.valueOf( result.gvfDelInfList[0].deliveryStatus);
                returnWR.deliveryID = result.gvfDelInfList[0].deliveryID;
                returnWR.postCode = result.gvfDelInfList[0].recipientPostcode;
               // returnWR.delTrack = result.gvfDelInfList[0].parcelInformationList;
                
               if(returnWR.deliveryPlannedDepartureTime != '' && returnWR.deliveryPlannedArrivalTime != ''){     
                   returnWR.twoHourSlot  = DelTrackConstants.EXIST;
                   dPTime = returnWR.deliveryPlannedDepartureTime;
                   aRTime =  returnWR.deliveryPlannedArrivalTime;
                   
               if(dPTime != NULL && aRTime != NULL){
                  List<String> departureTimeSplit =  dPTime.split(' ');
                  List<String> arrivalTimeSplit =  aRTime.split(' ');  
                   
                  returnWR.twoHourDPDate =   departureTimeSplit[0] + '/' + mMonthValue.get(departureTimeSplit[1]) + '/' +departureTimeSplit[2];              
                  returnWR.twoHourDPTime =   departureTimeSplit[3];
                  returnWR.twoHourARTime = arrivalTimeSplit[3];   
                    
                if(returnWR.twoHourARTime != NULL && returnWR.twoHourARTime != ''){
                  returnWR.twoHour8PMTimeCheck = DelTrackConstants.currentTimeCheckWithEightPM();   
                  returnWR.twoHourARTimeCompare = DelTrackConstants.twoHourARTimeCompare(returnWR.twoHourARTime);
                }
                   
                    }
                }
                
                if(returnWR.deliverySlotFrom != NULL &&  returnWR.deliverySlotFrom != '' ){

                List<string> delFromtimeList = returnWR.deliverySlotFrom.split(' ');    
                   
                returnWR.deliverySlotFromFormatted =  delFromtimeList[0] + '/' + mMonthValue.get(delFromtimeList[1]) + '/' +delFromtimeList[2];
                returnWR.deliverySlotFromFormattedTime = delFromtimeList[3];
                    
                }
                if(returnWR.deliverySlotTo != NULL && returnWR.deliverySlotTo != ''){
                List<string> delTotimeList = returnWR.deliverySlotTo.split(' ');
                     if(delTotimeList[0] != NULL){
                        if(delTotimeList[0].length() == 2){
                            dateRequired = delTotimeList[0];
                        }else {
                            dateRequired = '0' + delTotimeList[0];
                        }  
                    }
                    
                returnWR.deliverySlotToFormatted = dateRequired + '/' + mMonthValue.get(delTotimeList[1]) + '/' +delTotimeList[2];
                 returnWR.deliverySlotToFormattedTime = delTotimeList[3];
                    
                 String CurrentDate = System.now().format('dd/MM/YYYY');
                  returnWR.currentDateCheck = 'Future';
                    
                    if(Date.parse(returnWR.deliverySlotToFormatted)  == Date.parse(CurrentDate)){
                        returnWR.currentDateCheck = 'Current';
                    }else if(Date.parse(returnWR.deliverySlotToFormatted)  < Date.parse(CurrentDate)){
                        returnWR.currentDateCheck = 'Past';
                    }else{
                        returnWR.currentDateCheck = 'Future';
                    }
                   
                 String CurrentDateTime = System.now().format('hh.mm');

                    if(returnWR.deliverySlotToFormattedTime > CurrentDateTime){
                        returnWR.currentDateTimeCheck = 'Future';
                    }else{
                        returnWR.currentDateTimeCheck = 'Past';
                    }

                }
                
            }
            
            if(result.ccOrderInf != null){
                returnWR.ccData = true;
                returnWR.deliveryTypeName = 'ClickAndCollect';
                returnWR.CCcustomerName = result.ccOrderInf.customerName;
                returnWR.CCorderNumber = orderIdInput;
                returnWR.CCavailableForCollDate = result.ccOrderInf.availableForCollDate != null ?  result.ccOrderInf.availableForCollDate.format('d MMM yyyy HH:mm')   : '';
               
                String ccDateRequired;
                if(returnWR.CCavailableForCollDate != NULL && returnWR.CCavailableForCollDate != ''){
                    List<String> DateSplit = returnWR.CCavailableForCollDate.split(' ');
                    
                    if(DateSplit[0] != NULL){
                        if(DateSplit[0].length() == 2){
                            ccDateRequired = DateSplit[0];
                        }else {
                            ccDateRequired = '0' + DateSplit[0];
                        }  
                    }
                    
                 returnWR.CCavailableForCollDateAlone = ccDateRequired + '/' + mMonthValue.get(DateSplit[1]) + '/' +DateSplit[2];
                 returnWR.CCavailableForCollDateTime = DateSplit[3];

                     String CurrentDate = System.now().format('dd/MM/YYYY');

                    if(Date.parse(returnWR.CCavailableForCollDateAlone) == Date.Parse(CurrentDate)){
                        returnWR.currentDateCheck = 'Current';
                    }else if(Date.parse(returnWR.CCavailableForCollDateAlone) < Date.Parse(CurrentDate)){
                        returnWR.currentDateCheck = 'Past';
                    }else{
                         returnWR.currentDateCheck = 'Future';
                    }
                   
                 String CurrentDateTime = System.now().format('hh.mm');
                    if(returnWR.CCavailableForCollDateTime > CurrentDateTime){
                        returnWR.currentDateTimeCheck = 'Future';
                    }else{
                        returnWR.currentDateTimeCheck = 'Past';
                    }      
                }
                
                String AfterTimeCheck = System.Label.CurrentTimeCheck;   
                DateTime timenow = DateTime.parse(System.Now().format());
                String CurrentTime = string.valueOf(timenow);
                List<String> CurrentDateTimeList = CurrentTime.split(' ');
                
                if(CurrentDateTimeList.size()>0){
                String currentTimeWithCheck = CurrentDateTimeList[1];
                if(currentTimeWithCheck > AfterTimeCheck){
                returnWR.CCAfterTimeCheck = 'Past';
                }
                else
                {
                returnWR.CCAfterTimeCheck = 'Future';
                }          
                }
                
                returnWR.CCorderStatus = result.ccOrderInf.orderStatus;
                returnWR.CCmodeTransport = result.ccOrderInf.modeTransport;
                returnWR.CCexpectedLocation = result.ccOrderInf.expectedLocation;
                returnWR.CCtimeOfTheSatus = result.ccOrderInf.timeOfTheSatus != null  ?  result.ccOrderInf.timeOfTheSatus.format('d MMM yyyy HH:mm')   : '';
                returnWR.CCdelTrack = result.ccOrderInf.parcelInformationList;
            }
            
            if(result.cdOrderInf != null){
                returnWR.cdData = true;
                returnWR.CDOrderNumber = orderIdInput;
                returnWR.CDcustomerName = result.cdOrderInf.customerName;
                returnWR.CDbranchName = result.cdOrderInf.branchName;
              //  returnWR.CDdelTrack = result.cdOrderInf.parcelInformationList;
            }
            
            if(result.sdOrderInfList!= null && result.sdOrderInfList.size() > 0){
                returnWR.sdData = true;
                returnWR.SDCustomerName = result.sdOrderInfList[0].customerName;
                returnWR.SDPostcode = result.sdOrderInfList[0].postCode;
                returnWR.SDOrderNumber = orderIdInput;
                returnWR.SDBranch = result.sdOrderInfList[0].branchName;
                returnWR.SDSupplierName  =  result.sdOrderInfList[0].supplierName;
                returnWR.SDPurchaseOrderNumber =  result.sdOrderInfList[0].purchaseOrderNo;
            //    returnWR.SDdelTrack = result.sdOrderInfList[0].orderLineInformationList;
            }
               
              relatedCaseList = DelTrackConstants.getCaseListOrderNumber(orderIdInput);
         system.debug('@@@ line 221' + orderIdInput);       
         system.debug('@@@ line 222' + relatedCaseList);
                
       	    if(relatedCaseList !=null &&  relatedCaseList.size() > 0){ 
               returnWR.relatedCaseList = relatedCaseList;
                  }
            }
            
        }
        
        JSONResponseString = JSON.serialize( returnWR, true);    
        System.debug('@@@ Lie 228 JSONResponseString' + JSONResponseString);
        return JSONResponseString;   
        }
        
        catch(Exception e){
           
            System.debug('@@@ Exception in DelTrackChatBotUtils Line Number' + e.getLineNumber());
            System.debug('@@@ Exception in DelTrackChatBotUtils' + e.getMessage());
            
            return 'Error in DelTrackChatBotUtils';
            
        }
        
    }

     public class WrapperResponse{

         /*
       @AuraEnabled public List<DeliveryTrackingVO.ParcelInformation> delTrack {set;get;}
       @AuraEnabled public List<DeliveryTrackingVO.SDOrderLineItemInformation> SDdelTrack {set;get;}
       @AuraEnabled public List<DeliveryTrackingVO.CarrierParcelInformation> CDdelTrack {set;get;}
       @AuraEnabled public List<DeliveryTrackingVO.ParcelEvent> ParcelEventList {set;get;}
       */
        @AuraEnabled public List<DeliveryTrackingVO.ParcelInformation> CCdelTrack {set;get;}
        @AuraEnabled public List<DeliveryTrackingVO.ErrorInfo> errors {set;get;}
        @AuraEnabled public List < Case > relatedCaseList{get;set;}
        @AuraEnabled public String recipientName {set;get;}
         
        @AuraEnabled public String deliverySlotFrom {set;get;}
        @AuraEnabled public String deliverySlotTo {set;get;}
        @AuraEnabled public String deliverySlotFromFormatted {set;get;}
        @AuraEnabled public String deliverySlotFromFormattedTime {set;get;}
        @AuraEnabled public String deliverySlotToFormatted {set;get;} 
        @AuraEnabled public String deliverySlotToFormattedTime {set;get;}  
        @AuraEnabled public String currentDateCheck {set;get;}  
        @AuraEnabled public String currentDateTimeCheck {set;get;}  
        @AuraEnabled public String twoHourSlot = 'No Data';
        @AuraEnabled public String twoHourDPDate = 'No Data'; 
        @AuraEnabled public String twoHourDPTime = 'No Data';
        @AuraEnabled public String twoHourARTime = 'No Data';
        @AuraEnabled public String twoHour8PMTimeCheck = 'No Data';
        @AuraEnabled public String twoHourARTimeCompare = 'No Data';
         
         
        @AuraEnabled public String deliveryPlannedDepartureTime = 'No Data';
        @AuraEnabled public String deliveryPlannedArrivalTime = 'No Data';
        @AuraEnabled public String deliveryCDHLocation {set;get;}
        @AuraEnabled public String deliveryStatus {set;get;}
        @AuraEnabled public String deliveryTimeOnStatus {set;get;}
        @AuraEnabled public String deliveryTypeName {set;get;}
        @AuraEnabled public String deliveryID {set;get;}
        @AuraEnabled public String postCode {set;get;}
        @AuraEnabled public String CRCustomerName {set;get;}
        @AuraEnabled public String CROrderNumber {set;get;}
        @AuraEnabled public String CRPostcode  {set;get;}
        @AuraEnabled public String CRBranch  {set;get;}
        @AuraEnabled public String CCcustomerName {set;get;}
        @AuraEnabled public String CCorderNumber {set;get;}
         
        @AuraEnabled public String CCavailableForCollDate  {set;get;}
        @AuraEnabled public String CCavailableForCollDateAlone  {set;get;}
        @AuraEnabled public String CCavailableForCollDateTime  {set;get;}
        @AuraEnabled public String CCAfterTimeCheck  {set;get;} 
         
        @AuraEnabled public String CCorderStatus  {set;get;}
        @AuraEnabled public String CCmodeTransport  {set;get;}
        @AuraEnabled public String CCexpectedLocation  {set;get;}
        @AuraEnabled public String CCtimeOfTheSatus  {set;get;}
        @AuraEnabled public String CDcustomerName  {set;get;}
        @AuraEnabled public String CDbranchName  {set;get;}
        @AuraEnabled public String CDOrderNumber  {set;get;}
        @AuraEnabled public String SDCustomerName  {set;get;}
        @AuraEnabled public String SDPostcode  {set;get;}
        @AuraEnabled public String SDOrderNumber  {set;get;}
        @AuraEnabled public String SDBranch  {set;get;}
        @AuraEnabled public String SDSupplierName  {set;get;}
        @AuraEnabled public String SDPurchaseOrderNumber  {set;get;}
        @AuraEnabled public boolean gvfData = false; 
        @AuraEnabled public boolean sdData = false;
        @AuraEnabled public boolean ccData = false;
        @AuraEnabled public boolean cdData = false;
        @AuraEnabled public String errorType;
        @AuraEnabled public String errorDescription ;
    }

}