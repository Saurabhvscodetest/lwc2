/* Description  : GDPR - Task Records Data Deletion
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   28/12/2018        ChandraMouli Maddukuri          Created                COPT-4276
*
*/

global class GDPR_BatchDeleteCallLogsonTask implements Database.Batchable<sObject>,Database.Stateful
{
    global Map<Id,String> ErrorMap = new Map<Id,String>();
    global integer successcount = 0;
    private static final DateTime Prior_Date = System.now().addMonths(-12);
    private static final Id callLogRTId = CommonStaticUtils.getRecordTypeID('TASK','Call Log');
       
    global Database.QueryLocator start(Database.BatchableContext BC)
    {        
        //Find all task records where Record Type is Call Log and CreatedDate is more than 12 months AND Case ID is empty
        String query = 'SELECT Id, WhatId FROM TASK WHERE CreatedDate <: Prior_Date AND RecordTypeID =: callLogRTId AND WhatId = NULL';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Task> scope)
    {
        if(scope.size()>0 )
        { 
            try{
                List<Database.deleteResult> srList = Database.delete(scope, false);
                for(Integer counter = 0; counter < srList.size(); counter++) {
                    if (srList[counter].isSuccess()) {
                       successcount++;
                    }
                    else {
                        for(Database.Error err : srList[counter].getErrors()) {
                            ErrorMap.put(srList.get(counter).id,err.getMessage());
                        }
                    }
                }	
                /* Delete records from Recyclebin */
                Database.emptyRecycleBin(scope);
            }
            Catch(exception e){
                EmailNotifier.sendNotificationEmail('Exception from GDPR_BatchDeleteCallLogsonTask ', e.getMessage());
            }
        } 
    }
    
    global void finish(Database.BatchableContext BC) 
    {
        String textBody ='';
        set<Id> allIds = new set<Id>();
        textBody += successcount +' records deleted in object Task'+'\n';
        if (!ErrorMap.isEmpty()){
            for (Id recordids : ErrorMap.KeySet())
            {
                allIds.add(recordids);
            } 
            textBody+= 'Error log: '+allIds+'\n';
        }
        EmailNotifier.sendNotificationEmail('GDPR - Delete Call Log Tasks', textBody);
    }

}