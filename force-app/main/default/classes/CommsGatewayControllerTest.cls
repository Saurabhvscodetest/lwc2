@isTest
global class CommsGatewayControllerTest {
    
    public static final boolean NO_PAGE_MESSAGES = false;
    public static final boolean HAS_PAGE_MESSAGES = true;
    public static List<Comms_Gateway_Constants__c> commsContants;
    public static List<Exact_Target_Callout_Settings__c> exactCalloutSettings;
    public static List<Exact_Target_Status_Description__c> exactTargetStatusDesc;
    
     /* load the custom settings */  
     static {
        try{
            commsContants = Test.loadData(Comms_Gateway_Constants__c.sObjectType, 'Comms_Gateway_Constants');
            exactCalloutSettings = Test.loadData(Exact_Target_Callout_Settings__c.sObjectType, 'Exact_Target_Callout_Settings');
            exactTargetStatusDesc = Test.loadData(Exact_Target_Status_Description__c.sObjectType, 'Exact_Target_Status_Description');
            system.debug('commsContants'+commsContants);
        }catch(Exception e){
          System.debug('Exception'+e.getMessage());
        }
    }
    
    static testmethod void testExactTargetwithOrderNumber(){
        ExactTargetServiceMock exactTargetMock = new ExactTargetServiceMock(ExactTargetServiceMock.INPUT_ORDER_NUMBER);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, exactTargetMock);
        CommsGatewayController controller = new CommsGatewayController();
        controller.orderId = '93847234';
        controller.searchDelivery();
        Test.stopTest();
        system.assert(String.isBlank(controller.errorMessage), 'Error Should not be populated');
        system.assertEquals(NO_PAGE_MESSAGES , ApexPages.hasMessages(), 'Should not contain the page messages');
        System.assertNotEquals(0, controller.dataList.size());
    }
    
    static testmethod void testExactTargetwithDeliveryNumber(){
        ExactTargetServiceMock exactTargetMock = new ExactTargetServiceMock(ExactTargetServiceMock.INPUT_DELIVERY_NUMBER);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, exactTargetMock);
        CommsGatewayController controller = new CommsGatewayController();
        controller.deliveryId = '93847234';
        controller.searchDelivery();
        Test.stopTest();
        system.assert(String.isBlank(controller.errorMessage), 'Error Should not be populated');
        system.assertEquals(NO_PAGE_MESSAGES , ApexPages.hasMessages(), 'Should not contain the page messages');
        System.assertNotEquals(0, controller.dataList.size());
    }   
   
    static testmethod void testExactTargetwithPhoneNumber(){
        ExactTargetServiceMock exactTargetMock = new ExactTargetServiceMock(ExactTargetServiceMock.INPUT_PHONE_NUMBER);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, exactTargetMock);
        CommsGatewayController controller = new CommsGatewayController();
        controller.phoneNumber = '01234123123';
        controller.searchDelivery();
        Test.stopTest();
        system.assert(String.isBlank(controller.errorMessage), 'Error Should not be populated');
        system.assertEquals(NO_PAGE_MESSAGES , ApexPages.hasMessages(), 'Should not contain the page messages');
        System.assertNotEquals(0, controller.dataList.size());
    }   
    
    static testmethod void testExactTargetwithEmailAddress(){
        ExactTargetServiceMock exactTargetMock = new ExactTargetServiceMock(ExactTargetServiceMock.INPUT_EMAIL_ADDRESS);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, exactTargetMock);
        CommsGatewayController controller = new CommsGatewayController();
        controller.emailAddress = 'test@test.com';
        controller.searchDelivery();
        Test.stopTest();
        system.assert(String.isBlank(controller.errorMessage), 'Error Should not be populated');
        //System.assertNotEquals(0, controller.dataList.size());
    } 
    
    static testmethod void testWithNoParameters(){
        ExactTargetServiceMock exactTargetMock = new ExactTargetServiceMock(ExactTargetServiceMock.INPUT_EMAIL_ADDRESS);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, exactTargetMock);
        CommsGatewayController controller = new CommsGatewayController();
        controller.searchDelivery();
        Test.stopTest();
        system.assert(String.isNotBlank(controller.errorMessage), 'Error Should be populated');
        system.assertEquals(HAS_PAGE_MESSAGES , ApexPages.hasMessages(), 'Should contain the page messages');
        System.assertEquals(null, controller.dataList);
    }  
   
    static testmethod void testWithmorethanoneParameter(){
        ExactTargetServiceMock exactTargetMock = new ExactTargetServiceMock(ExactTargetServiceMock.INPUT_EMAIL_ADDRESS);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, exactTargetMock);
        CommsGatewayController controller = new CommsGatewayController();
        controller.orderId = '93847234';
        controller.deliveryId = '93847234';
        controller.phoneNumber = '01234123123';
        controller.emailAddress = 'test@test.com';
        controller.searchDelivery();
        Test.stopTest();
        system.assert(String.isNotBlank(controller.errorMessage), 'Error Should be populated');
        system.assertEquals(HAS_PAGE_MESSAGES , ApexPages.hasMessages(), 'Should contain the page messages');
        System.assertEquals(null, controller.dataList);
    } 
    
   static testmethod void testExactTargetwithPhoneNumberInvalidLength(){
        ExactTargetServiceMock exactTargetMock = new ExactTargetServiceMock(ExactTargetServiceMock.INPUT_PHONE_NUMBER);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, exactTargetMock);
        CommsGatewayController controller = new CommsGatewayController();
        controller.phoneNumber = '09384723434234324';
        controller.searchDelivery();
        Test.stopTest();
        system.assert(String.isNotBlank(controller.errorMessage), 'Error Should be populated');
        system.assertEquals(HAS_PAGE_MESSAGES , ApexPages.hasMessages(), 'Should contain the page messages');
        System.assertEquals(null, controller.dataList);
    }  
   
    static testmethod void testExactTargetwithEmailAddressInvalidLength(){
        ExactTargetServiceMock exactTargetMock = new ExactTargetServiceMock(ExactTargetServiceMock.INPUT_PHONE_NUMBER);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, exactTargetMock);
        CommsGatewayController controller = new CommsGatewayController();
        controller.emailAddress = 'testksdjiewuuusdhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh@xyz.cinnnnnnnnnn';
        controller.searchDelivery();
        Test.stopTest();
        system.assert(String.isNotBlank(controller.errorMessage), 'Error Should be populated');
        system.assertEquals(HAS_PAGE_MESSAGES , ApexPages.hasMessages(), 'Should contain the page messages');
        System.assertEquals(null, controller.dataList);
    } 
    
    static testmethod void testExactTargetwithOrderNumberwithAlphabets(){
        ExactTargetServiceMock exactTargetMock = new ExactTargetServiceMock(ExactTargetServiceMock.INPUT_ORDER_NUMBER);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, exactTargetMock);
        CommsGatewayController controller = new CommsGatewayController();
        controller.orderId = '9384b234';
        controller.searchDelivery();
        Test.stopTest();
        system.assert(String.isNotBlank(controller.errorMessage), 'Error Should be populated');
        System.assertEquals(null, controller.dataList);
    }
    
    static testmethod void testExactTargetwithDeliveryNumberwithAlphabets(){
        ExactTargetServiceMock exactTargetMock = new ExactTargetServiceMock(ExactTargetServiceMock.INPUT_DELIVERY_NUMBER);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, exactTargetMock);
        CommsGatewayController controller = new CommsGatewayController();
        controller.deliveryId = '938b7234';
        controller.searchDelivery();
        Test.stopTest();
        system.assert(String.isNotBlank(controller.errorMessage), 'Error Should be populated');
        system.assertEquals(HAS_PAGE_MESSAGES , ApexPages.hasMessages(), 'Should contain the page messages');
        System.assertEquals(null, controller.dataList);
    }   
    
    static testmethod void testExactTargetwithPhoneNumberwithAlphabets(){
        ExactTargetServiceMock exactTargetMock = new ExactTargetServiceMock(ExactTargetServiceMock.INPUT_PHONE_NUMBER);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, exactTargetMock);
        CommsGatewayController controller = new CommsGatewayController();
        controller.phoneNumber = '0938b7234';
        controller.searchDelivery();
        Test.stopTest();
        system.assert(String.isNotBlank(controller.errorMessage), 'Error Should be populated');
        system.assertEquals(HAS_PAGE_MESSAGES , ApexPages.hasMessages(), 'Should contain the page messages');
        System.assertEquals(null, controller.dataList);
    }  
    
    static testmethod void testExactTargetcleardata(){
        ExactTargetServiceMock exactTargetMock = new ExactTargetServiceMock(ExactTargetServiceMock.INPUT_PHONE_NUMBER);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, exactTargetMock);
        CommsGatewayController controller = new CommsGatewayController();
        controller.phoneNumber = '09387234';
        controller.searchDelivery();
        controller.clearData();
        Test.stopTest();
        system.assert(String.isNotBlank(controller.errorMessage), 'Error Should be populated');
        system.assertEquals(HAS_PAGE_MESSAGES , ApexPages.hasMessages(), 'Should contain the page messages');
        System.assertEquals(null, controller.dataList);
    }     
}