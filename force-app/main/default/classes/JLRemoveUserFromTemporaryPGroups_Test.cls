/******************************************************************************
* @author       Ranjan Samal             
* @date         27/09/2017
* @description  Removal of Users From Temporary Public Group after 8 hours 
******************************************************************************/
@isTest
private class JLRemoveUserFromTemporaryPGroups_Test {
    private static final Integer randomNo = UnitTestDataFactory.getRandomNumber();
    private static final string USER_FIRST_NAME = 'BatchTest';
    private static final string BOOKINGS_TEAM_NAME = 'TESTTEAM' + randomNo;
    private static final string HAMILTION_TEAM_NAME = BOOKINGS_TEAM_NAME+ 'A';

    @testSetup static void setupTestConfig() {

        Config_Settings__c teamConfig = Config_Settings__c.getInstance('AUTO_RETURN_TO_TEAM_PROFILE_I');

        if (teamConfig == null) {
            teamConfig = new Config_Settings__c(Name = 'AUTO_RETURN_TO_TEAM_PROFILE_I');
        }

        teamConfig.Text_Value__c = 'JL: CapitaContactCentre Tier2-3';
        
        Config_Settings__c  teamMinutesConfig = Config_Settings__c.getInstance('AUTO_RETURN_TO_TEAM_MINUTES');
        if (teamMinutesConfig == null) {
            teamMinutesConfig = new Config_Settings__c(Name = 'AUTO_RETURN_TO_TEAM_MINUTES');
        }

        teamMinutesConfig.Number_Value__c = 0;

        Config_Settings__c  teamBatchRerunConfig = Config_Settings__c.getInstance('AUTO_RETURN_TO_TEAM_BATCH_RERUN');
        if (teamBatchRerunConfig == null) {
            teamBatchRerunConfig = new Config_Settings__c(Name = 'AUTO_RETURN_TO_TEAM_BATCH_RERUN');
        }

        teamBatchRerunConfig.Number_Value__c = 10;
        List<Config_Settings__c> configsToUpsert = new List<Config_Settings__c>{teamConfig,teamMinutesConfig,teamBatchRerunConfig};

        upsert configsToUpsert;
    }
    @isTest
    private static List<User> BuildUsersToTest() {
        List<User> userCollection = new List<User>();

        User sysAdmin = UnitTestDataFactory.getSystemAdministrator('admin1');
        
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(runningUser) { // Avoid MIX_DML
            UnitTestDataFactory.setUpTeams(new List<String>{BOOKINGS_TEAM_NAME, HAMILTION_TEAM_NAME}, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
        }
        
        System.runAs(sysAdmin) {
            
            Group testGroup = new Group();
            testGroup.Name = 'testGroup';
            testGroup.DeveloperName = 'ABC';
            INSERT testGroup;
            
            UserRole role = [select Id FROM UserRole where Name='JLP:CC:CSA:T1' LIMIT 1];
            Profile prof = [SELECT ID FROM Profile WHERE Name = 'JL: CapitaContactCentre Tier2-3' LIMIT 1];
            Group grp = [SELECT ID FROM Group LIMIT 1];
            
            

            for(Integer i = 0; i < 5; i++) {
                User userRec = UnitTestDataFactory.getBackOfficeUser(USER_FIRST_NAME + i, HAMILTION_TEAM_NAME);
                userRec.UserRoleId = role.Id; 
                userRec.profileId = prof.Id;
                userRec.Is_User_Team_Manager__c = false;  
                userRec.Temporary_Public_Group__c = grp.Id;

                userCollection.add(userRec);    
            }
            insert userCollection;
        }

            return userCollection;                       
    }       

    @isTest 
    static void testExecuteBatchChangesTheUsersRemovedFromPublicGroups() {

        List<User> users = BuildUsersToTest();

        Test.startTest();
        JLRemoveUserFromTemporaryPublicGroups batch = new JLRemoveUserFromTemporaryPublicGroups();
        Database.executeBatch(batch);
        Test.stopTest();       
    }
}