/* Description  : GDPR - Delete Accounts from System
*
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   28/03/2019      Vijay Ambati                        Created             COPT-4521
*
*/

@IsTest
Public class BAT_DeleteAccountsForGDPRTest {
    
    @IsTest
    Public Static Void testInsertAccounts(){
        
        List<Account> accInsert = new List<Account>();
        
        for(integer i=0;i<300;i++){
            
            accInsert.add(new Account(Name = 'Vijay' + i ));
        }
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.GDPR_Class_Name__c = 'BAT_DeleteAccountsForGDPR';
        gDPR.Name = 'BAT_DeleteAccountsForGDPR';
        gDPR.Record_Limit__c = '5000'; 
        try{
            Insert accInsert;
            Insert gDPR;
            system.assertEquals(300, accInsert.size());
        }
        Catch(Exception e){
            system.debug('Exception Caught:'+e.getmessage());
        }
        
        Database.BatchableContext BC;
        BAT_DeleteAccountsForGDPR obj = new BAT_DeleteAccountsForGDPR();
        Test.startTest();
        Database.DeleteResult[] Delete_Result = Database.delete(accInsert, false);
        obj.start(BC);
        obj.execute(BC,accInsert); 
        obj.finish(BC);
        Test.stopTest();  
    }
    @isTest
    Public static void testSchedule(){
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.GDPR_Class_Name__c = 'BAT_DeleteAccountsForGDPR';
        gDPR.Name = 'BAT_DeleteAccountsForGDPR';
        gDPR.Record_Limit__c = '5000'; 
        Test.startTest();
        Insert gDPR;
        ScheduleBatchToDeleteAccounts obj = NEW ScheduleBatchToDeleteAccounts();
        obj.execute(null);  
        Test.stopTest();
    }
}