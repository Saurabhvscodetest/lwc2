@isTest
private class CaseInteractionLoggerTest {
	
	@testSetup static void initTest() {

		UnitTestDataFactory.createConfigSettings();

	}


	@isTest static void testInteractionLogStart() {

		// Create Non-Omni User

		User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		User nonOmniUser;

		Case newCase;
		Contact testContact;

		System.runAs(runningUser) {

			nonOmniUser = UnitTestDataFactory.getFrontOfficeUser('NONOMNIUSER');
			insert nonOmniUser;
			addNonOmniPermissionSetToUser(nonOmniUser);

		}


		List<Interaction__c> interactionRecords = [SELECT Id FROM Interaction__c WHERE Event_Type__c = 'Handle'];
		System.assert(interactionRecords.isEmpty());

		System.runAs(nonOmniUser) {

			newCase = createCase();
			insert newCase;

			PageReference currentPage = Page.CaseInteractionLogPage;
			Test.setCurrentPage(currentPage);

			ApexPages.StandardController standardController = new ApexPages.StandardController(newCase);
			CaseInteractionLogController controllerExtension = new CaseInteractionLogController(standardController);

			controllerExtension.logInteractionFromCasePageLoad();	

		}

		interactionRecords = [SELECT Id, Agent__c, Start_DT__c, End_DT__c, Case__c FROM Interaction__c WHERE Event_Type__c = 'Handle'];
		System.assertEquals(1, interactionRecords.size());
		System.assertEquals(nonOmniUser.Id, interactionRecords[0].Agent__c);
		System.assertEquals(newCase.Id, interactionRecords[0].Case__c);
		System.assertNotEquals(null, interactionRecords[0].Start_DT__c);
		System.assertEquals(null, interactionRecords[0].End_DT__c);

 	}
	

	@isTest static void testInteractionLogEnd() {

		User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
		User nonOmniUser;

		Case newCase;
		Contact testContact;

		System.runAs(runningUser) {

			nonOmniUser = UnitTestDataFactory.getFrontOfficeUser('NONOMNIUSER');
			insert nonOmniUser;
			addNonOmniPermissionSetToUser(nonOmniUser);

		}

		System.runAs(nonOmniUser) {

			newCase = createCase();
			insert newCase;

			PageReference currentPage = Page.CaseInteractionLogPage;
			Test.setCurrentPage(currentPage);

			ApexPages.StandardController standardController = new ApexPages.StandardController(newCase);
			CaseInteractionLogController controllerExtension = new CaseInteractionLogController(standardController);

			controllerExtension.logInteractionFromCasePageLoad();

		}

		List<Interaction__c> interactionRecords = [SELECT Id, Agent__c, Start_DT__c, End_DT__c, Case__c FROM Interaction__c WHERE Event_Type__c = 'Handle'];
		System.assertEquals(1, interactionRecords.size());
		System.assertEquals(null, interactionRecords[0].End_DT__c);

		Test.startTest();
			System.runAs(nonOmniUser) {
				Id returnId = CaseInteractionLogController.logInteractionFromCloseTab(String.valueOf(newCase.Id));
			}
		Test.stopTest();

		interactionRecords = [SELECT Id, Agent__c, Start_DT__c, End_DT__c, Case__c FROM Interaction__c WHERE Event_Type__c = 'Handle'];
		System.assertEquals(1, interactionRecords.size());
		System.assertNotEquals(null, interactionRecords[0].End_DT__c);	


	}

	@isTest static void testInteractionLogNotCreatedOmniUser() {

		// Create Non-Omni User

		User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		User omniUser;

		Case newCase;
		Contact testContact;

		System.runAs(runningUser) {
			omniUser = UnitTestDataFactory.getFrontOfficeUser('omniUser');
			insert omniUser;
		}


		List<Interaction__c> interactionRecords = [SELECT Id FROM Interaction__c WHERE Event_Type__c = 'Handle'];
		System.assert(interactionRecords.isEmpty());

		System.runAs(omniUser) {

			newCase = createCase();
			insert newCase;

			PageReference currentPage = Page.CaseInteractionLogPage;
			Test.setCurrentPage(currentPage);

			ApexPages.StandardController standardController = new ApexPages.StandardController(newCase);
			CaseInteractionLogController controllerExtension = new CaseInteractionLogController(standardController);

			controllerExtension.logInteractionFromCasePageLoad();	

		}

		interactionRecords = [SELECT Id, Agent__c, Start_DT__c, End_DT__c, Case__c FROM Interaction__c WHERE Event_Type__c = 'Handle'];
		System.assert(interactionRecords.isEmpty());


 	}
	


	/* Helper */

	private static void addNonOmniPermissionSetToUser(User testUser) {

		Id nonOmniPermissionSetId = [SELECT Id FROM PermissionSet WHERE Name = :CaseInteractionLogger.NON_OMNI_PERMISSION_SET LIMIT 1].Id;

		PermissionSetAssignment permissionSetAssignment = new PermissionSetAssignment();
		permissionSetAssignment.AssigneeId = testUser.Id;
		permissionSetAssignment.PermissionSetId = nonOmniPermissionSetId;

		insert permissionSetAssignment;

	}

	private static Case createCase() {

		Case newCase = UnitTestDataFactory.createCase(UnitTestDataFactory.createContact().Id);
		newCase.Bypass_Standard_Assignment_Rules__c = true;
		newCase.JL_Branch_master__c = 'Oxford Street';
		newCase.Contact_Reason__c = 'Pre-Sales';
		newCase.Reason_Detail__c = 'Buying Office Enquiry';
		
		return newCase;

	}

}