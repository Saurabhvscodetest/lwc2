/*************************************************
coOrderDetail_TEST

test class for coOrderDetail

Author: Steven Loftus (MakePositive)
Created Date: 20/11/2014
Modification Date: 
Modified By: 

**************************************************/
@isTest
private class coOrderDetail_TEST {
    
    public testmethod static void fullCodeExercise() {


        coOrderDetail orderDetail = new coOrderDetail();

        // top level details
        orderDetail.OrderNumber = '123456';
        orderDetail.OrderDate = Date.today();
        orderDetail.OrderStatusCode = '400';
        orderDetail.OrderStatusDescription = 'Complete';
		orderDetail.TotalOrderValue = 2000.10 ;
        // addresses
        coOrderDetail.AddressType address = new coOrderDetail.AddressType();
        address.LastName = 'Loftus';
        address.FirstName = 'Steven';
        address.Title = 'Mr';
        address.Company = 'Lobster Solutions';
        address.HouseNumberOrName = '6';
        address.Street1 = 'Eaves Avenue';
        address.City = 'Hebden bridge';
        address.County = 'West Yorkshire';
        address.Country = 'UK';
        address.Phone = '07734740113';
        address.PhoneEvening = '01422843767';
        address.Email = 'steven.loftus@gmail.com';
        address.PostCode = 'HX7 6DJ';

        orderDetail.BillingAddress = address;
        orderDetail.ShippingAddress = address;

        // tenders
        coOrderDetail.TenderType tender = new coOrderDetail.TenderType();
        tender.CardHolderName = 'Steven Loftus';
        tender.CardNumber = '1234567890';
        tender.StartDate = '01/01/2001';
        tender.ExpiryDate = '01/01/2020';
        tender.TenderAmount = 123.45;
        tender.CardType = 'VISA';

        orderDetail.addTender(tender);

        // batches
        coOrderDetail.BatchType batch = new coOrderDetail.BatchType();
        batch.InternalDeliveryMethodDescription = 'Standard Delivery';
        //batch.DeliveryDate = '01/01/2020';
        batch.LeadTime = 1 ;

        // item
        coOrderDetail.ItemType item = new coOrderDetail.ItemType();
        item.ProductCode = '123456';
        item.Item = 'Some Product';
        item.Distributor = 'John Lewis';
        item.UnitPrice = 2.34;
        item.UnitPartnerDiscount = 1;
        item.QuantityOrdered = 1;
        item.TotalPrice = 1.34;
        item.Status = 'Delivered';
        item.DistributorReference = '028045165';
        item.PurchaseOrderId = 7200282 ;
        item.QuantityShipped = 1;
        //item.DateShipped = '11/12/2013' ;
        item.QuantityCancelled = 1;
        item.QuantityReturned = 0;

        batch.addItem(item);

        orderDetail.addBatch(batch);
        
        // payment
        coOrderDetail.PaymentType payment = new coOrderDetail.PaymentType();
        payment.BillingEmail = 'testing@gmail.com';
        payment.Title = 'Mr';
        payment.FirstName = 'test266';
        payment.LastName = 'test764';
        payment.CompanyName = 'John Lewis';
        payment.HouseNumberOrName = '171';
        payment.StreetName = 'Victoria Street';
        payment.AddressLine2 = '';
        payment.AddressLine3 = '';
        payment.AddressLine4 = '';
        payment.Town = 'London';
        payment.County = 'London';
        payment.PostCode = 'CB42DD';
        payment.Country = 'UK';
        payment.DayTimeTelephoneNumber = '0123456789';
        payment.EveningTelephoneNumber = '';
        payment.PaymentMethod = 'Card';
        payment.NameonCreditCard = 'test266764';
        payment.MaskedCardNumber = '1234567890123456';
        payment.ExpiryMonth = '05';
        payment.ExpiryYear = '2019';
        payment.StartMonth = '06';
        payment.Year = '2016';
        payment.StartYear = '2015';
        payment.IssueNumber = '1';
        payment.SmartPayID = '';
        payment.Curency = 'GBP';
        payment.PayPalPayerId = '';
        payment.PayPalStatus = '';
        
        orderDetail.addPayment(payment);
        
        //History
        coOrderDetail.HistoryType history = new coOrderDetail.HistoryType();
        //history.TransactionDate = 27/11/2014;
        history.OrderNumber = 26530618;
        history.TransactionMethod = 'Partnership Card';
        history.AuthCode = '073888';                                                                                                                                                                                                                                                                                                                                                                                                                                                               
        history.AVSCode = '';
        history.TransactionType ='Authorisation';
        history.TotalAmount = 20.00 ;
        history.TransactionResult = 'Success';
        history.Reason = 'Code: 00 - AUTH CODE:073888';
       	history.Authentication = 'Failed';
       	history.TransactionID = 20041265;
        
        orderDetail.addHistory(history); 
        
        //Transaction
        coOrderDetail.TransactionType transactions = new coOrderDetail.TransactionType();
        transactions.OrderNumbers = 28044653 ;
        transactions.Description = 'Standard delivery';
        transactions.Type = 'Bill';
        transactions.Quantity = 1 ;
        transactions.TotalAmount = 20.00;
        transactions.TotalVoucher = 1.0;
        transactions.TotalGiftCard = 10.10 ;
        transactions.TotalDiscount = 5.05 ;
        transactions.ReceiptItemID = 23369348 ;
        
        //GiftCard
        coOrderDetail.GiftCardType giftcards = new coOrderDetail.GiftCardType();
        giftcards.GiftCardNumber = '1234567890' ;
    	giftcards.TransactionType = 'Bill';
    	giftcards.Amount = 2000.20 ;
    	giftcards.Result = 'Success';
    	
    	orderDetail.addGiftCard(giftcards);

        // email
        coOrderDetail.EmailType email = new coOrderDetail.EmailType();
        email.MessageQueueId = '123';
        email.RecipientEmail = 'bob@nowhere.com';
        email.SentDate = DateTime.now();
        email.Subject = 'A Subject';
        email.Content = 'Some Content';

        orderDetail.addEmail(email);

        // comment
        coOrderDetail.CommentType comment = new coOrderDetail.CommentType();
        comment.OrderComments = 'Some Order Comments';
        comment.CommentTime = DateTime.now();
        comment.EnteredByName = 'Dave';
        comment.EnteredByPosition = 'Boss';

        orderDetail.addComment(comment);


        // asserts
        system.assertEquals('Mr Steven Loftus', orderDetail.BillingAddress.FullName, 'FullName of the Billing Address is incorrect');
        system.assertEquals('Mr Steven Loftus<BR/>Lobster Solutions<BR/>6<BR/>Eaves Avenue<BR/>Hebden bridge<BR/>West Yorkshire<BR/>HX7 6DJ<BR/>UK', orderDetail.ShippingAddress.HtmlAddress, 'Shipping Html Address is incorrect');
        system.assertEquals('VISA', orderDetail.PrimaryTender.CardType, 'Should be Visa');
    }

}