/******************************************************************************
* @author       Shion Abdillah
* @date         14 Jul 2016
* @description  Controller class for Case Search Results Components.
*
*
* LOG	DATE			AUTHOR	JIRA		COMMENT      
* 001	28/06/2016		SA			        Original code

*
*******************************************************************************
*/
public class CaseSearchResultsController extends DataTablePaginator { 
	
	//Pagination Data members		
    @TestVisible private static final String DEFAULT_SORT_FIELD = 'CaseNumber'; 
    @TestVisible private static final Integer DEFAULT_PAGE_SIZE = 50;
    @TestVisible private Set<Id> selectedCaseIds = new Set<Id>();
	public List<SelectOption> recordSelectOptions {get; private set;}
	public String recordSelectItem {get; set;}
	public String CaseId {get; set;}
	public string contextItem {get;set;}
	public List<RowItem> caseRows {get;set;}
	// public List<RowItem> errorRows {get;set;}
	public Integer totalRecordCount {get; private set;}		
    public String searchCriteria {get; set;}     
    private String previousSearchCriteria = null;     

	//Collection Data members 
    @TestVisible private List<Case> caseCollectionLoc;
	public List<Case> CaseCollection{
		set{
			caseCollectionLoc = value;	
            if (String.isEmpty(previousSearchCriteria) || String.isEmpty(searchCriteria) || !searchCriteria.equals(previousSearchCriteria)) {
                previousSearchCriteria  = searchCriteria;
			setCon = null;		
	        if(caseCollectionLoc != null){
	        	fullDataList = caseCollectionLoc;
	        }
	        else
	        {
	            fullDataList =  new List<SObject>();
	        }	
			pageSize = 20;
			totalRecordCount = fullDataList.size();		
		}
        }
		get{
			return caseCollectionLoc;
		}
	}

	public CaseSearchResultsController() {
		pageSize = 20;
		noOfRecords = 0;
		resetVariables();
        /*
        if(CaseCollection != null){
            fullDataList = CaseCollection;
        }
        else
        {
			fullDataList =  new List<SObject>();
        }
        */	
	}

	/**
	 * Return an integer value of the number of pages needed to display all selected records
	 * based on page size.
	 * @params:	None
	 * @rtnval: Integer		Total number of pages
	 */
	public Integer getTotalPages(){
		Decimal totalSize = setCon.getResultSize();
		Decimal pageSize = setCon.getPageSize();
		Decimal pages = totalSize/pageSize;
		return (Integer)pages.round(System.RoundingMode.CEILING);
	}

	/**
	 * Inner class for displaying case rows
	 */
	public class RowItem {
		public Case caseRec {get; private set;}
		public Boolean isSelected {get;set;}
		public String errorMessage {get; private set;}
		public String shortDescription {get; private set;}
		
		public RowItem (Case c, Boolean s) {
			this.caseRec = c;
			this.isSelected = s;
			if (c.Description != null) {
				this.shortDescription = c.Description.left(100);
			}
		}
		
		public RowItem (Case c, String errorMsg) {
			this.caseRec = c;
			this.isSelected = false;
			this.errorMessage = errorMsg;
		}
	}
	
	/**
	 * Reset lists and variables
	 * @params:	None
	 * @rtnval: void
	 */
    @TestVisible private void resetVariables() {
		setCon = null;
		selectedCaseIds.clear();
		fullDataList = new List<Case>();
		totalRecordCount = 0;
		sortField = null;
		previousSortField = null;
	}		

	/**
	 * Retrieve a page of records from the set controller.
	 * @params:	None
	 * @rtnval: List<RowItem>	List of RowItem objects for a single page
	 */
	public List<RowItem> getCases() {
		if(CaseCollection != null){
			caseRows = new List<RowItem>();
			for (Case c : (List<Case>)setCon.getRecords()) {
				RowItem row = new RowItem(c, false);
				caseRows.add(row);
			}
		}
		noOfRecords = caseRows.size();
		return caseRows;

    }

	public PageReference GetCaseID(){		
		PageReference pageRef;		 
		pageRef = new PageReference('/apex/CustomerContact?Id=' + CaseId);		
		pageRef.setRedirect(true);		
		return pageRef;
	}

	public PageReference LoadCase(){		
		PageReference pageRef;		
		pageRef = new PageReference('/apex/CustomerContact?Id=' + CaseId);		
		pageRef.setRedirect(true);		
		return pageRef;
	}	


}