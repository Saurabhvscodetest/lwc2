/* Description  : Delete Customer records as per given criteria in COPT-4283
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   29/03/2019        Vignesh Kotha                   Created                COPT-4283
*
*/
@isTest
public class BAT_DeleteSecondaryCustomerTest {
    static testmethod void testDeleteCustomerData(){        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        // Running user Context
        System.runAs(runningUser){
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;       
            insert CustomSettingTestDataFactory.createConfigSettings();
        }        
        contact testContact = CustomerTestDataFactory.createContact();
        testContact.Email = 'test@gmail.com';
        insert testContact;     
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.GDPR_Class_Name__c = 'BAT_DeleteSecondaryCustomer';
        gDPR.Name = 'BAT_DeleteSecondaryCustomer';
        gDPR.Record_Limit__c = '5000';
        Test.startTest();
        Insert gDPR;
        List<contact> ConList=new  List<contact>();
        for(integer i=0; i<99; i++){
            ConList.add(new contact(FirstName='test',LastName='test1',Email = 'Test@gmail.com'));
        }       
        insert ConList;
        task tas = new task();
        tas.subject= 'Email:hii';
        insert tas;
        delete tas;
        Database.BatchableContext BC;
        BAT_DeleteSecondaryCustomer obj=new BAT_DeleteSecondaryCustomer();
        Database.DeleteResult[] Delete_Result = Database.delete(ConList, false);
        obj.start(BC);
        obj.execute(BC,ConList);
        obj.finish(BC);
        DmlException expectedException;
        Test.stopTest();
    }
    @IsTest
    Public static void testSchedule(){
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.Name = 'BAT_DeleteSecondaryCustomer';   
        gDPR.GDPR_Class_Name__c = 'BAT_DeleteSecondaryCustomer';
        gDPR.Record_Limit__c = '5000';
        Test.startTest();
        Insert gDPR;
        ScheduleBatchdeleteSecondaryCustomers obj = NEW ScheduleBatchdeleteSecondaryCustomers();
        obj.execute(null); 
        Test.stopTest();
    }

}