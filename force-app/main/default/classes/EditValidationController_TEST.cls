@IsTest
public with sharing class EditValidationController_TEST {

    @IsTest
    public static void testExtController()
    {
        /*
        User testUser = UnitTestDataFactory.findStandardActiveUser();
        UnitTestDataFactory.setRunValidationRules(false);        
        Contact con = UnitTestDataFactory.createContact(new Account());
        con.Email = 'a@bb.com';        
        insert con;        
        Case c = UnitTestDataFactory.createNormalCase(con);                
        insert c;        
       */
        Case c;  
        Id caseId;
        User sysAdmin           = UnitTestDataFactory.getSystemAdministrator('admin1');
        User backOfficeUser     = UnitTestDataFactory.getBackOfficeUser('bo', 'JL.com - Bookings');
        
        insert sysAdmin;    
        insert backOfficeUser;

        System.runAs(sysAdmin) {
            UnitTestDataFactory.initialiseContactCentreAndTeamSettings();           
        } 
        Test.startTest();
        System.runAs (backOfficeUser) {
            Contact ctc = UnitTestDataFactory.createContact(1, true);
            c = UnitTestDataFactory.createQueryCase(ctc.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', false, true);
            caseId = c.Id;                                  
        }
       
        System.debug('caseId: ' + caseId);
        Case testCase = [select Id, Status, ContactId,LastModifiedDate,jl_Last_Edited__c , LastModifiedBy.Id, LastModifiedBy.Name, CreatedDate from Case where Id = :caseId];
        system.assertNotEquals(testCase,null,'The returned Case is null.');
        
        ApexPages.StandardController sc = new ApexPages.standardController(testCase);
        EditValidationController testEditValidationController = new EditValidationController(sc);
        testEditValidationController.CaseId =  testCase.Id;
        testEditValidationController.CaseRec =  testCase;                
        PageReference pageRef = testEditValidationController.ViewRecord();       
        pageRef = testEditValidationController.CheckForEditable();       
        pageRef = testEditValidationController.ForceEditRecord();
        Boolean IsTimeResult = testEditValidationController.getIsWithinRecentlyEditedTimeRange();
        Test.stopTest();                    
    }

}