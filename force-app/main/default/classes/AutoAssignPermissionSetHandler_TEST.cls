/**
 * Unit tests for AutoAssignPermissionSetHandler class methods.
 */
@Istest
public class AutoAssignPermissionSetHandler_TEST {
	/**
	 * Test inserting a new Auto_Assign_Permission_Set__c updates users with those Roles / Profiles
	 */
	static testMethod void testCreateAutoAssignPermissionSet() {
		Profile prof = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		Map<String, PermissionSet> permissionSetNameXrefMap = new Map<String, PermissionSet>();
		List<PermissionSet> permissionSetList = [SELECT Id, Label FROM PermissionSet WHERE IsOwnedByProfile = false];
		if (!permissionSetList.isEmpty()) {
			for (PermissionSet ps : permissionSetList) {
				String psLabel = ps.Label.left(40);
				permissionSetNameXrefMap.put(psLabel, ps);
			}
		}
		Id apiEnabledPSId = permissionSetNameXrefMap.get('API Enabled').Id;
		Id apiOnlyUserPSId = permissionSetNameXrefMap.get('Api Only User').Id;
		Id dataloaderPSId = permissionSetNameXrefMap.get('Dataloader').Id;

		UserRole ur1 = new UserRole(RollupDescription = 'UT User Role 1', Name = 'UT User Role 1', DeveloperName = 'UT_User_Role_1');
		insert ur1;
	    
		User usr = new User(FirstName = 'Freddie', LastName = 'Admin', Username = 'freddietest@test.com.test', 
							Email = 'freddietest.test@test.com.test', CommunityNickname = 'freddiet', 
							Alias = 'freddie', TimeZoneSidKey = 'Europe/London', 
							LocaleSidKey = 'en_GB', EmailEncodingKey = 'ISO-8859-1', 
							ProfileId = prof.Id, LanguageLocaleKey = 'en_US', isActive = true,
							Team__c = 'JL.com - Bookings', Tier__c = 2, UserRoleId = ur1.Id);		
    	insert usr;
    	clsUserTriggerHandler.mapsInitialised = false;
    	
		Auto_Assign_Permission_Set__c aaps1 = new Auto_Assign_Permission_Set__c(Name = 'ROLE: UT User Role 1', Permission_Sets_To_Assign__c = 'API Enabled;Api Only User');
		Auto_Assign_Permission_Set__c aaps2 = new Auto_Assign_Permission_Set__c(Name = 'PROFILE: System Administrator', Permission_Sets_To_Assign__c = 'Dataloader');
		List<Auto_Assign_Permission_Set__c> aapsList = new List<Auto_Assign_Permission_Set__c> {aaps1, aaps2};

    	test.startTest();
		insert aapsList;
    	test.stopTest();
		
		List<PermissionSetAssignment> psaList = [SELECT Id, AssigneeId, PermissionSetId, PermissionSet.Name
												 FROM PermissionSetAssignment
												 WHERE AssigneeId = :usr.Id AND PermissionSet.ProfileId = null];    	
    	
    	system.assertEquals(3, psaList.size());
    	for (PermissionSetAssignment psa : psaList) {
    		system.assert(psa.PermissionSetId == apiEnabledPSId || psa.PermissionSetId == apiOnlyUserPSId || psa.PermissionSetId == dataloaderPSId);
    	}
    }

	/**
	 * Test updating an Auto_Assign_Permission_Set__c record updates users with those Roles / Profiles
	 */
	static testMethod void testUpdateAssignPermissionSet() {
		Profile prof = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		Map<String, PermissionSet> permissionSetNameXrefMap = new Map<String, PermissionSet>();
		List<PermissionSet> permissionSetList = [SELECT Id, Label FROM PermissionSet WHERE IsOwnedByProfile = false];
		if (!permissionSetList.isEmpty()) {
			for (PermissionSet ps : permissionSetList) {
				String psLabel = ps.Label.left(40);
				permissionSetNameXrefMap.put(psLabel, ps);
			}
		}
		Id apiEnabledPSId = permissionSetNameXrefMap.get('API Enabled').Id;
		Id apiOnlyUserPSId = permissionSetNameXrefMap.get('Api Only User').Id;
		Id dataloaderPSId = permissionSetNameXrefMap.get('Dataloader').Id;
		Id interactionReportingPSId = permissionSetNameXrefMap.get('Interaction Reporting Permission').Id;

		UserRole ur1 = new UserRole(RollupDescription = 'UT User Role 1', Name = 'UT User Role 1', DeveloperName = 'UT_User_Role_1');
		insert ur1;
	    
		User usr = new User(FirstName = 'Freddie', LastName = 'Admin', Username = 'freddietest@test.com.test', 
							Email = 'freddietest.test@test.com.test', CommunityNickname = 'freddiet', 
							Alias = 'freddie', TimeZoneSidKey = 'Europe/London', 
							LocaleSidKey = 'en_GB', EmailEncodingKey = 'ISO-8859-1', 
							ProfileId = prof.Id, LanguageLocaleKey = 'en_US', isActive = true,
							Team__c = 'JL.com - Bookings', Tier__c = 2, UserRoleId = ur1.Id);		
    	insert usr;
    	clsUserTriggerHandler.mapsInitialised = false;
    	
		Auto_Assign_Permission_Set__c aaps1 = new Auto_Assign_Permission_Set__c(Name = 'ROLE: UT User Role 1', Permission_Sets_To_Assign__c = 'API Enabled;Api Only User');
		Auto_Assign_Permission_Set__c aaps2 = new Auto_Assign_Permission_Set__c(Name = 'PROFILE: System Administrator', Permission_Sets_To_Assign__c = 'Dataloader');
		List<Auto_Assign_Permission_Set__c> aapsList = new List<Auto_Assign_Permission_Set__c> {aaps1, aaps2};
		insert aapsList;
    	clsUserTriggerHandler.mapsInitialised = false;

		List<PermissionSetAssignment> psaList = [SELECT Id, AssigneeId, PermissionSetId, PermissionSet.Name
												 FROM PermissionSetAssignment
												 WHERE AssigneeId = :usr.Id AND PermissionSet.ProfileId = null];    	
    	
    	system.assertEquals(3, psaList.size());
    	for (PermissionSetAssignment psa : psaList) {
    		system.assert(psa.PermissionSetId == apiEnabledPSId || psa.PermissionSetId == apiOnlyUserPSId || psa.PermissionSetId == dataloaderPSId);
    		system.assertNotEquals(interactionReportingPSId, psa.PermissionSetId);
    	}

    	aaps1.Permission_Sets_To_Assign__c = 'API Enabled';
    	aaps2.Permission_Sets_To_Assign__c = 'Dataloader;Interaction Reporting Permission';

    	test.startTest();
    	update aapsList;
    	test.stopTest();
		
		List<PermissionSetAssignment> psaList2 = [SELECT Id, AssigneeId, PermissionSetId, PermissionSet.Name
												  FROM PermissionSetAssignment
												  WHERE AssigneeId = :usr.Id AND PermissionSet.ProfileId = null];    	
    	
    	system.assertEquals(3, psaList2.size());
    	for (PermissionSetAssignment psa : psaList2) {
    		system.assert(psa.PermissionSetId == apiEnabledPSId || psa.PermissionSetId == interactionReportingPSId || psa.PermissionSetId == dataloaderPSId);
    		system.assertNotEquals(apiOnlyUserPSId, psa.PermissionSetId);
    	}
    }

	/**
	 * Test deleting an Auto_Assign_Permission_Set__c record updates users with those Roles / Profiles
	 */
	static testMethod void testDeleteAssignPermissionSet() {
		Profile prof = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		Map<String, PermissionSet> permissionSetNameXrefMap = new Map<String, PermissionSet>();
		List<PermissionSet> permissionSetList = [SELECT Id, Label FROM PermissionSet WHERE IsOwnedByProfile = false];
		if (!permissionSetList.isEmpty()) {
			for (PermissionSet ps : permissionSetList) {
				String psLabel = ps.Label.left(40);
				permissionSetNameXrefMap.put(psLabel, ps);
			}
		}
		Id apiEnabledPSId = permissionSetNameXrefMap.get('API Enabled').Id;
		Id apiOnlyUserPSId = permissionSetNameXrefMap.get('Api Only User').Id;
		Id dataloaderPSId = permissionSetNameXrefMap.get('Dataloader').Id;
		Id interactionReportingPSId = permissionSetNameXrefMap.get('Interaction Reporting Permission').Id;

		UserRole ur1 = new UserRole(RollupDescription = 'UT User Role 1', Name = 'UT User Role 1', DeveloperName = 'UT_User_Role_1');
		insert ur1;
	    
		User usr = new User(FirstName = 'Freddie', LastName = 'Admin', Username = 'freddietest@test.com.test', 
							Email = 'freddietest.test@test.com.test', CommunityNickname = 'freddiet', 
							Alias = 'freddie', TimeZoneSidKey = 'Europe/London', 
							LocaleSidKey = 'en_GB', EmailEncodingKey = 'ISO-8859-1', 
							ProfileId = prof.Id, LanguageLocaleKey = 'en_US', isActive = true,
							Team__c = 'JL.com - Bookings', Tier__c = 2, UserRoleId = ur1.Id);		
    	insert usr;
    	clsUserTriggerHandler.mapsInitialised = false;
    	
		Auto_Assign_Permission_Set__c aaps1 = new Auto_Assign_Permission_Set__c(Name = 'ROLE: UT User Role 1', Permission_Sets_To_Assign__c = 'API Enabled;Api Only User');
		Auto_Assign_Permission_Set__c aaps2 = new Auto_Assign_Permission_Set__c(Name = 'PROFILE: System Administrator', Permission_Sets_To_Assign__c = 'Dataloader');
		List<Auto_Assign_Permission_Set__c> aapsList = new List<Auto_Assign_Permission_Set__c> {aaps1, aaps2};
		insert aapsList;
    	clsUserTriggerHandler.mapsInitialised = false;

		List<PermissionSetAssignment> psaList = [SELECT Id, AssigneeId, PermissionSetId, PermissionSet.Name
												 FROM PermissionSetAssignment
												 WHERE AssigneeId = :usr.Id AND PermissionSet.ProfileId = null];    	
    	
    	system.assertEquals(3, psaList.size());
    	for (PermissionSetAssignment psa : psaList) {
    		system.assert(psa.PermissionSetId == apiEnabledPSId || psa.PermissionSetId == apiOnlyUserPSId || psa.PermissionSetId == dataloaderPSId);
    		system.assertNotEquals(interactionReportingPSId, psa.PermissionSetId);
    	}

    	test.startTest();
    	delete aaps1;
    	test.stopTest();
		
		List<PermissionSetAssignment> psaList2 = [SELECT Id, AssigneeId, PermissionSetId, PermissionSet.Name
												  FROM PermissionSetAssignment
												  WHERE AssigneeId = :usr.Id AND PermissionSet.ProfileId = null];    	
    	
    	system.assertEquals(1, psaList2.size());
    	PermissionSetAssignment psa = psaList2.get(0); 
   		system.assertEquals(dataloaderPSId, psa.PermissionSetId);
    }

	/**
	 * Test duplicate name validation works - duplicates in same list
	 */
	static testMethod void testDuplicateSameList() {
		Auto_Assign_Permission_Set__c aaps1 = new Auto_Assign_Permission_Set__c(Name = 'ROLE: UT User Role 1', Permission_Sets_To_Assign__c = 'API Enabled;Api Only User');
		Auto_Assign_Permission_Set__c aaps2 = new Auto_Assign_Permission_Set__c(Name = 'ROLE: UT User Role 1', Permission_Sets_To_Assign__c = 'Dataloader');
		List<Auto_Assign_Permission_Set__c> aapsList = new List<Auto_Assign_Permission_Set__c> {aaps1, aaps2};

    	test.startTest();
		try {
			insert aapsList;
			system.assert(false);
		} catch (Exception e) {
			system.assert(true);
		}
    	test.stopTest();
		
		system.assertEquals(null, aaps1.Id);
		system.assertEquals(null, aaps2.Id);
    }

	/**
	 * Test duplicate name validation works - existing duplicate name
	 */
	static testMethod void testExistingDuplicate() {
		Auto_Assign_Permission_Set__c aaps1 = new Auto_Assign_Permission_Set__c(Name = 'ROLE: UT User Role 1', Permission_Sets_To_Assign__c = 'API Enabled;Api Only User');
		Auto_Assign_Permission_Set__c aaps2 = new Auto_Assign_Permission_Set__c(Name = 'ROLE: UT User Role 1', Permission_Sets_To_Assign__c = 'Dataloader');
		List<Auto_Assign_Permission_Set__c> aapsList = new List<Auto_Assign_Permission_Set__c> {aaps1, aaps2};

    	insert aaps1;
		system.assertNotEquals(null, aaps1.Id);
    	
    	test.startTest();
		try {
			insert aaps2;
			system.assert(false);
		} catch (Exception e) {
			system.assert(true);
		}
    	test.stopTest();
		
		system.assertEquals(null, aaps2.Id);
    }
}