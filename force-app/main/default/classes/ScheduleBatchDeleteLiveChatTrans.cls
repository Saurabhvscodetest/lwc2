/* Description  : Delete Live chat Transcripts when criteria satisfy in  COPT-4293
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   20/02/2019        Vignesh Kotha                   Created               COPT-4293
*
*/
global class ScheduleBatchDeleteLiveChatTrans implements Schedulable{

     global void execute(SchedulableContext sc){
        BAT_DeleteLiveChatTranscript obj = new BAT_DeleteLiveChatTranscript();
        Database.executebatch(obj);
    }

}