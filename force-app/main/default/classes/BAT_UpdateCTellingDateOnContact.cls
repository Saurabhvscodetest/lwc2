/*
* @File Name   : BAT_UpdateCTellingDateOnContact
* @Description : Batch class used to update the contact record new date field with the last updated date value from the Contact
         or Task object based on the recent date
* @Copyright   : Zensar
* @Jira #      : #4277
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0       16-April-19        Ragesh G                     Created
*/
global class BAT_UpdateCTellingDateOnContact implements Database.Batchable<sObject>, Database.Stateful{
    
    global Map<Id,String> exceptionMap = new Map<Id,String>();
    global Integer result=0;
    global String contactSOQL = 'SELECT Id,CT_Appointment_date__c,GDPR_Data_Deletion__c  FROM contact';
  
    /**
    *@purpose : Interface Method needs to be implemented to get the batch started, querying the required records from the contact 
          with the custom settings limit if it is available.
    *@param   : Database.BatchableContext BC 
    *@return  : Database.QueryLocator
    **/
    global Database.QueryLocator start( Database.BatchableContext BC ){
      
      // Fetch the custom setting record limit for the Client Telling Data Deletion Batch
      GDPR_Record_Limit__c gDPRLimit = GDPR_Record_Limit__c.getValues(ConstantValues.CLIENT_TELLING_CONTACT_UPDATE_BATCH_NAME);
      if( gDPRLimit != NULL ){
          String recordLimit = ' ' + 'Limit' + ' ' + gDPRLimit.Record_Limit__c;
          contactSOQL  += recordLimit;
      }
        
      return Database.getQueryLocator( contactSOQL );
    }
    
    /**
    *@purpose : Interface Method needs to be implemented to get the batch executed, process the records coming through the 
          scope list and check whether the all the conditions are satisfied 
    *@param   : Database.BatchableContext BC, List < SObject > - In this case list of Contacts 
    *@return  : void
    **/
    global void execute( Database.BatchableContext BC, List< Contact > scope ){
        try{
      List< Contact > contactRecordList = [ SELECT CT_Appointment_date__c 
                          FROM Contact 
                          WHERE Id IN :scope AND CT_Appointment_date__c !=null ORDER By CT_Appointment_date__c DESC];
      System.debug ( ' Contact record list size ' + contactRecordList.size());
                        
      Map < Id, List < Task>> contactTaskMap = new Map < Id, List < Task >> ();
      List < Contact > contactListToUpdate = new List < Contact> ();
      for ( Task taskRecord :  [ SELECT whoId,PRXC__Outreach_Delivered__c 
                     FROM Task 
                     WHERE whoid IN : contactRecordList AND PRXC__Outreach_Delivered__c !=null ORDER By PRXC__Outreach_Delivered__c DESC] ) {
        if ( contactTaskMap.containsKey ( taskRecord.whoId )  ) {
          contactTaskMap.get ( taskRecord.whoId ).add ( taskRecord );
        } else {
          contactTaskMap.put ( taskRecord.whoId, new List < Task > {taskRecord});
        }
      }
             for( Contact contactRecord : contactRecordList ){
                 List< Task > contactTaskList = contactTaskMap.get( contactRecord.Id );
                 if( contactTaskList != NULL && contactRecordList != NULL) {
                     if( contactTaskList[0].PRXC__Outreach_Delivered__c > contactRecord.CT_Appointment_date__c ){
                         contactRecord.GDPR_Data_Deletion__c = contactTaskList[0].PRXC__Outreach_Delivered__c;
                     }else{
                         contactRecord.GDPR_Data_Deletion__c = contactRecord.CT_Appointment_date__c; 
                     }
                     contactRecord.Skip_Batch_Validation__c = true;
                 } else if ( contactRecordList != NULL ) {
                   contactRecord.GDPR_Data_Deletion__c = contactRecord.CT_Appointment_date__c;
                 } else if ( contactRecord.CT_Appointment_date__c  == null ) {
                     contactRecord.isGDPRCriteriaEmpty__c = true;
                 }
                 contactListToUpdate.add ( contactRecord );
             }
            List< Database.SaveResult > contactResultList = Database.update ( contactListToUpdate, false );
                for(Integer counter = 0; counter < contactResultList.size(); counter++){
                    if (contactResultList[counter].isSuccess()){
                        result++;
                    }else{
                        for(Database.Error err : contactResultList[counter].getErrors()){
                            exceptionMap.put(contactResultList.get(counter).id,err.getMessage());
                        }
                    }
                }
             if(Test.isRunningTest())  //Condition to ensure test is running
               integer intTest =1/0;    //This will throw some exception, 
        }Catch( Exception e ){
            EmailNotifier.sendNotificationEmail( ConstantValues.CT_CONTACT_UPDATE_BATCH_EXECUTION_FAILED, e.getMessage());
        }
    }
    
    /**
    *@purpose : Interface Method needs to be implemented post batch execution finishes 
          sending a notification with the number of records successfully updated and number of failed records 
    *@param   : Database.BatchableContext BC
    *@return  : void
    **/
    
    global void finish( Database.BatchableContext BC ) {
        String textBody ='';
        set<Id> allIds = new set<Id>();
        textBody+= result + ConstantValues.CT_CONTACT_UPDATE_BATCH_EXECUTION_SUCCESS;
        if (!exceptionMap.isEmpty()){
            for ( Id recordids : exceptionMap.KeySet() ) {
                allIds.add(recordids);
            } 
            textBody+= 'Error log: '+allIds+'\n';
        }
        EmailNotifier.sendNotificationEmail(ConstantValues.CT_GDPR_DELETE_LOG_SUBJECT, textBody);
        
        Database.executeBatch(new BAT_UpdateClienTellingData());
    }
    
    
}