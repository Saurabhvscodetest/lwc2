/* Description  : Delete Contact Profile Records one time.
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   21/12/2018        Vignesh Kotha                   Created                 COPT-4271
*
*/
@isTest
public class GDPR_BatchDeleteContactProfilesTest {
    @isTest
    Public static void testsinglecontactprofiles() {
        contact c = new contact();
        c.FirstName = 'Vignesh';
        c.LastName='kotha';
        insert c;        
        Contact_Profile__c cp = new Contact_Profile__c();
        cp.name='test';   
        cp.Contact__c = c.id;
        insert cp;
        Loyalty_Account__c LA = new Loyalty_Account__c();
        LA.Activation_Date_Time__c= system.now().addYears(-6);
        LA.Source_CreatedDate__c = system.now().addYears(-6);
        LA.IsActive__c=false;
        insert LA;
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.GDPR_Class_Name__c = 'GDPR_BatchDeleteContactProfiles';
        gDPR.Name = 'GDPR_BatchDeleteContactProfiles';
        gDPR.Record_Limit__c = '500';
        test.startTest();
        Insert gDPR;
        GDPR_BatchDeleteContactProfiles Laaccount = new GDPR_BatchDeleteContactProfiles();
        Database.executeBatch(Laaccount, 200);
        test.stopTest();
    }
    @isTest
    Public static void testbulkcontactprofiles() {
        contact c = new contact();
        c.FirstName = 'Vignesh';
        c.LastName='kotha';
        insert c;
        
        list<Contact_Profile__c> conprofdata = new list<Contact_Profile__c>();
        for(integer counter =0; counter<300 ; counter++){
            Contact_Profile__c cp = new Contact_Profile__c();
            cp.Name = 'test'+counter;
            cp.Contact__c = c.id;
            conprofdata.add(cp);
        }
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.GDPR_Class_Name__c = 'GDPR_BatchDeleteContactProfiles';
        gDPR.Name = 'GDPR_BatchDeleteContactProfiles';
        gDPR.Record_Limit__c = '500';
        try{
            Insert gDPR;
            insert conprofdata;
            system.assertEquals(300, conprofdata.size());
        }
        Catch(exception e){
            system.debug('Exception Caught:'+e.getmessage());
        }
        Database.BatchableContext BC;
       GDPR_BatchDeleteContactProfiles obj = new GDPR_BatchDeleteContactProfiles();
        Test.startTest();
        Database.DeleteResult[] Delete_Result = Database.delete(conprofdata, false);
       obj.start(BC);
       obj.execute(BC,conprofdata); 
       obj.finish(BC);
        Test.stopTest();
    }
    @isTest
    Public static void testDmlException_Delete()
    {
        contact c = new contact();
        c.FirstName = 'Vignesh';
        c.LastName='kotha';
        insert c;
        list<Contact_Profile__c> conprofdata = new list<Contact_Profile__c>();
        for(integer counter =0; counter<300 ; counter++){
            Contact_Profile__c cp = new Contact_Profile__c();
            cp.Name = 'test'+counter;
            cp.Contact__c = c.id;
            conprofdata.add(cp);
        }
        try{
            insert conprofdata;
            system.assertEquals(300, conprofdata.size());
        }
        Catch(exception e){
            system.debug('Exception Caught:'+e.getmessage());
        }
        Database.DeleteResult[] Delete_Result = Database.delete(conprofdata, false);
        DmlException expectedException;
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.GDPR_Class_Name__c = 'GDPR_BatchDeleteContactProfiles';
        gDPR.Name = 'GDPR_BatchDeleteContactProfiles';
        gDPR.Record_Limit__c = '500';
        Test.startTest();
        try { 
            Insert gDPR;
            delete conprofdata;
        }
        catch (DmlException dException) {
            expectedException = dException; 
            system.assertNotEquals(NULL, expectedException);
        }
        Test.stopTest();
    }   
}