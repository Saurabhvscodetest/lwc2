/**
* Author:       Nawaz Ahmed
* Date:         09/03/2016
* Description:  HELPER CLASS TO MAKE WEBSERVICE CALLOUT AND INITIATED FROM CommsGateywayController.cls
*
* ******************* Change Log *******************
* Modified by       Change Date     Change
* Nawaz Ahmed	     09/03/2016     Initial Version
*/
public with sharing class ExactTargetServiceHelper { 
    
    public class ResponseInfo{
        public List<DataExtensionObject_DO> responseDataList {get;set;}
        public boolean areThereSomeMoreCommunications {get;set;}
    }
    
	  public static final string POST_HTTP_METHOD = Exact_Target_Callout_Settings__c.getInstance().Http_Method__c;
    public static final String CONTENT_TYPE = Exact_Target_Callout_Settings__c.getInstance().Content_Type__c;
    public static final String CONTENT_TYPE_TEXT_OR_XML = Exact_Target_Callout_Settings__c.getInstance().Content_Type_Text_or_XML__c;
    public static final String ENDPOINT = Exact_Target_Callout_Settings__c.getInstance().Endpoint__c;
    public static final String USERNAME = Exact_Target_Callout_Settings__c.getInstance().Username__c;
    public static final String PASSWORD = Exact_Target_Callout_Settings__c.getInstance().Password__c;
    public static final String SECURITY_TOKEN = Exact_Target_Callout_Settings__c.getInstance().Security_Token__c;
    public static final String TARGET_NAME_SPACE = Exact_Target_Callout_Settings__c.getInstance().Target_Namespace__c;
   	public static final String STATUS_MORE_RECORDS = Comms_Gateway_Constants__c.getinstance('STATUS_MORE_RECORDS').Value__c;
   	public static final String TAG_BODY = Comms_Gateway_Constants__c.getinstance('TAG_BODY').Value__c;
   	public static final String TAG_RETRIEVE_RESPONSE = Comms_Gateway_Constants__c.getinstance('TAG_RETRIEVE_RESPONSE').Value__c;
   	public static final String TAG_OVERALL_STATUS = Comms_Gateway_Constants__c.getinstance('TAG_OVERALL_STATUS').Value__c;
   	public static final String TAG_RESULTS = Comms_Gateway_Constants__c.getinstance('TAG_RESULTS').Value__c;
   	public static final String TAG_PROPERTIES = Comms_Gateway_Constants__c.getinstance('TAG_PROPERTIES').Value__c;
   	public static final String SEND_LOG_ARCHIVE_DATA_EXTENSION = Comms_Gateway_Constants__c.getinstance('SEND_LOG_ARCHIVE_DATA_EXTENSION').Value__c;
   	public static final String SEND_LOG_DATA_EXTENSION = Comms_Gateway_Constants__c.getinstance('SEND_LOG_DATA_EXTENSION').Value__c;
   	
   	
   	public static final String PHONE_NUMBER_PREFIX = '44';
   	
   	
   	/**
	* Entry point for the webervice callout
	* @params:	input seach value
	* @rtnval:	Response Data object
	*/
   	public static ResponseInfo getResponseList(String orderId, String deliveryId , String phoneNumber, String emailAddress, boolean searchArchive)	{
            if(String.isNotBlank(phoneNumber) && phoneNumber.startsWith('0')){
        		phoneNumber = phoneNumber.removeStart('0');
        		phoneNumber = PHONE_NUMBER_PREFIX +phoneNumber;
        	}
        	String filterPart = getFilterPart(orderId, deliveryId,phonenumber,emailaddress);
            String reqBody = getSoapRequest(filterPart,searchArchive);
            Http httpProtocol = new Http();
   			HttpRequest request = getHttpRequest(reqBody);
   		    HttpResponse response = httpProtocol.send(request);
   			return parseResponse(response);
   	}

   	/**
	* Parse the response using xml parser
	* @params:	Http Response
	* @rtnval:	ResponseData of Data objects
	*/   		
   	private static ResponseInfo parseResponse(HttpResponse response)	{
   		 Dom.Document resDoc  = response.getBodyDocument(); 
         dom.XmlNode  envelop = resDoc.getRootElement(); 
         String envelopNameSpace = envelop.getNamespace();
         List<DataExtensionObject_DO> dataList = new List<DataExtensionObject_DO>();
         
         Dom.XmlNode bodyNode = envelop.getChildElement(TAG_BODY, envelopNameSpace);
         String overallStatus = null;
         Dom.XmlNode RetrieveResponseMsgNode = bodyNode.getChildElement(TAG_RETRIEVE_RESPONSE, TARGET_NAME_SPACE);
         List<DOM.XmlNode> childrenNodesInResponse = RetrieveResponseMsgNode.getChildren();
         for(DOM.XmlNode childNode : childrenNodesInResponse)	 {
             if(childNode.getName()!=null && childNode.getName().equalsIgnoreCase(TAG_RESULTS)) {
                 for(Dom.XMLNode node : childNode.getChildren()) {
             		if(node.getName()!=null && node.getName().equalsIgnoreCase(TAG_PROPERTIES)) {
                 		dataList.add(new DataExtensionObject_DO(node,TARGET_NAME_SPACE));
             		}
         	      }
                 
             }
             if(String.isBlank(overallStatus) && childNode.getName()!=null && childNode.getName().equalsIgnoreCase(TAG_OVERALL_STATUS)) {
                 overallStatus = childNode.getText();
             }
         }
         ResponseInfo respInfo = new ResponseInfo();
   		 respInfo.responseDataList = dataList;
         respInfo.areThereSomeMoreCommunications = (String.isNotBlank(overallStatus) &&  (STATUS_MORE_RECORDS.equalsIgnoreCase(overallStatus))? true : false);
   		 return respInfo;
   	}

   	/**
	* Build the http request
	* @params:	string
	* @rtnval:	HttpRequest
	*/      		
   	private static HTTPRequest getHttpRequest(String reqBody)	{
   		 HttpRequest req = new HttpRequest();
    	 req.setBody(reqBody);
    	 req.setMethod('GET'); 
         req.setEndpoint(ENDPOINT);
         req.setHeader(CONTENT_TYPE,CONTENT_TYPE_TEXT_OR_XML);
         return req;	
   	}
   	
   	/**
	* build the xml request
	* @params:	input search value
	* @rtnval:	string
	*/         	
   	private static String getSoapRequest(String filterPart, boolean searchArchive){
   	        String dataExtensionName = (searchArchive) ? SEND_LOG_ARCHIVE_DATA_EXTENSION :SEND_LOG_DATA_EXTENSION ;
   	        system.debug('dataExtensionName ==>' + dataExtensionName + '== searchArchive' + searchArchive);
	    	String soapRequest = '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:wsa="http://schemas.xmlsoap.org/ws/2004/08/addressing" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">' +
	  								'<soap:Header>' +
	    								'<wsa:Action>Perform</wsa:Action>' +
	    								'<wsa:MessageID>urn:uuid:30ffce73-2bdc-422a-b3da-1e71e3f15270</wsa:MessageID>' +
	   									 '<wsa:ReplyTo>' +
	     									 '<wsa:Address>http://schemas.xmlsoap.org/ws/2004/08/addressing/role/anonymous</wsa:Address>' +
	    								'</wsa:ReplyTo>' +
	   									 '<wsa:To>'+ENDPOINT+'</wsa:To>' +
	   									 '<wsse:Security soap:mustUnderstand="1">' +
	     									 '<wsse:UsernameToken wsu:Id="SecurityToken-'+SECURITY_TOKEN+' ">' +
	       										 '<wsse:Username>'+USERNAME+'</wsse:Username>' +
	        									'<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'+PASSWORD+'</wsse:Password>' +
	     									 '</wsse:UsernameToken>' +
	    								'</wsse:Security>' +
	 									 '</soap:Header>' +
	 									' <soap:Body>' +
	     								 '<RetrieveRequestMsg xmlns="http://exacttarget.com/wsdl/partnerAPI">' +
	         								'<RetrieveRequest>' +
	           									 '<ObjectType>DataExtensionObject['+ dataExtensionName +']</ObjectType>' +
	           									 '<Properties>Channel</Properties>' +
                                                 '<Properties>CampaignName</Properties>' +
	           									 '<Properties>DeliveryNumber</Properties>' +
	            								 '<Properties>DeliveryStatus</Properties>' +
	            								 '<Properties>DeliveryStatus_DT</Properties>' +
                                                 '<Properties>Send_DT</Properties>'+
                                                 '<Properties>EmailAddress</Properties>'+
	            								 '<Properties>MessageCopy</Properties>' +
	            								 '<Properties>PhoneNumber</Properties>' +
	           									 '<Properties>OrderNumber</Properties>' +
	           									 '<Properties>VAMP_URL</Properties>' +
	           									  filterPart +
	         									'</RetrieveRequest>' +
	      								'</RetrieveRequestMsg>' +
	 								 '</soap:Body>' +
									'</soap:Envelope>';
	    	return soapRequest;
    	}
    	
     /**
	* Build the filter part of xml request based on input search value
	* @params:	input search value
	* @rtnval:	string
	*/  
    Private static string getFilterPart (String orderId, String deliveryId,string phonenumber,string emailaddress){
    		string filterpart = '';
    		if(orderId!=null && orderId!=''){
    			filterpart =  '<Filter xsi:type="SimpleFilterPart">' +
	              			  '<Property>OrderNumber</Property>' +
	               			  '<SimpleOperator>equals</SimpleOperator>' +
	              			  '<Value>'+orderId+'</Value>' +
	           				  '</Filter>';	
    		}
    		else if(deliveryId!=null && deliveryId!=''){
    			filterpart =  '<Filter xsi:type="SimpleFilterPart">' +
	              			  '<Property>DeliveryNumber</Property>' +
	               			  '<SimpleOperator>equals</SimpleOperator>' +
	              			  '<Value>'+deliveryId+'</Value>' +
	           				  '</Filter>';	
    		}
    		else if(phonenumber!=null && phonenumber!=''){
    			filterpart =  '<Filter xsi:type="SimpleFilterPart">' +
	              			  '<Property>PhoneNumber</Property>' +
	               			  '<SimpleOperator>equals</SimpleOperator>' +
	              			  '<Value>'+phonenumber+'</Value>' +
	           				  '</Filter>';	
    		}
    		else if(emailaddress!=null && emailaddress!=''){
    			filterpart =  '<Filter xsi:type="SimpleFilterPart">' +
	              			  '<Property>EmailAddress</Property>' +
	               			  '<SimpleOperator>equals</SimpleOperator>' +
	              			  '<Value>'+emailaddress+'</Value>' +
	           				  '</Filter>';	
    		}
    		return filterpart;
    		
    	}
}