// class is Stateful to allow for batch update of Load User records
// This need to be batched as you cannot update a User and a custom record in the same context

global class UserLoadBatch implements Database.Batchable<sObject>, Database.Stateful
{

    // to execute 
    // Id batchInstanceId = Database.executeBatch(new UserLoadBatch(), 10);

    global Set<ID> doneids = new Set<Id> ();
    global Map<Id,String> mapErrors = new Map<Id,String>();

    global UserLoadBatch(){}

    global Database.QueryLocator start(Database.BatchableContext BC){
        String q = 'select ' +
        ' l.Name,' +
        ' l.Role__c,' +
        ' l.Profile__c,' +
        ' l.Primary_Team__c,' + 
        ' l.Netdom__c,' +
        ' l.Department__c,' +
        ' l.Last_Name__c,' +
        ' l.ErrorStack__c,' +
        ' l.JL_License_Type__c,' +
        ' l.First_Name__c,' +
        ' l.Contract__c,' +
        ' l.Access_Weeks__c,' +
        ' l.Tier__c,    ' +
        ' l.Email__c,' +
        ' l.SST__c,' +
        ' l.SiteType__c,' +
        ' l.Function__c,' +
        ' l.Site__c,' +
        ' l.Approved__c,' +
        ' l.Done__c' +
        ' From Load_User__c l' +
        ' where l.Done__c = false ' +
        ' and l.Has_Error__c = false ' +
        ' and l.Last_Name__c != null ' +
        ' order by l.Last_Name__c';
    
        return Database.getQueryLocator(q);
    }

    // assuming a batch size of 1 here - logic should change if that is not the case in the future. 
    global void execute(Database.BatchableContext BC, List<Load_User__c> scope){
		Boolean requireApproval = CustomSettingsManager.getConfigSettingBooleanVal('Load User Requires Approval');
        executeHelper(scope, requireApproval);
    }
    
    public void executeHelper(List<Load_User__c> scope, Boolean requireApproval)
    {
        Load_User__c lu = scope[0];

        System.debug('Loading user: ' + lu);
        
        if (!lu.approved__c && requireApproval)
        {
        	// record is not approved and approval is required
        	return;
        }

        String error = UserLoad.validateLoadUser(lu);
        if (error != null)
        {
            System.debug('validateLoadUser failed: ' + error);      
            mapErrors.put(lu.id, 'Configuration Error: ' + error);
            return;
        }

        List<User> listu = UserLoad.createUserObjects (scope);
        
        User u = listu[0];
        
        System.debug('proposed username: ' + u.username);       
        String username = u.username;
        
        List<User> existing = [select id, username from User where username = :username];
        
        if (existing.size() > 0)
        {
            System.debug('existing username: ' + u.username);       
            mapErrors.put(lu.id, 'Username already exists: ' + username);
            return;
        }

        try
        {
            // happy path
            insert u;
            doneids.add(lu.id);
        }
        catch (Exception e)
        {
            mapErrors.put(lu.id, 'Unexpected error: ' + e.getMessage());
        }
        
   }

	global void finish(Database.BatchableContext BC)
	{
		updateLoadUserRecords (doneids, mapErrors);
	}

	public static void updateLoadUserRecords (Set<Id> doneids, Map<Id,String> mapErrors)
	{
        List<Load_User__c> done = [select id, done__c from Load_User__c where id IN :doneids];  
        for (Load_User__c lu : done)
        {
            lu.done__c = true;
        }
        
        List<Load_User__c> errors = new List<Load_User__c>();
        for (Id luid : mapErrors.keyset())
        {
            String error = mapErrors.get(luid);
            Load_User__c lu = new Load_User__c(id = luid, errorstack__c = error, has_error__c = true);
            errors.add(lu);
        }

        List<Load_User__c> empty = [select id, done__c from Load_User__c where done__c = false and last_name__c = null];    
        for (Load_User__c lu : empty)
        {
            lu.done__c = true;
        }
        
        update done;
        update errors;
        update empty;
   }

}