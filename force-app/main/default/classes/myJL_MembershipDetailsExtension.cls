public with sharing class myJL_MembershipDetailsExtension {
    
    public Contact sourceContact {get; set;}
    public List<ProfileWithLoyaltyAccount> plaList {get; private set;}
    public static set<id> userids = new set<id>();

    
    public myJL_MembershipDetailsExtension (ApexPages.StandardController con) {
        sourceContact = (Contact) addFieldsToController(con).getRecord();

		if(sourceContact.Id == NULL){
			sourceContact.Id = ApexPages.currentPage().getParameters().get('contactId');
		}

        plaList = new List<ProfileWithLoyaltyAccount>();
        processData();
    }
    
    public void processData(){
        
        List<Loyalty_Account__c> loyActs = [select id,join_date__c,Activation_Date_Time__c,Scheme_Joined_Date_Time__c,Email_Address__c,Name,contact__c,Deactivation_Date_Time__c,
                                            IsActive__c,channel_id__c,Customer_Loyalty_Account_Card_ID__c, Voucher_Preference__c, Number_Of_Partnership_Cards__c,
                                            (select id, Date_Created__c, Pack_Type__c, Card_Number__c  from MyJL_Cards__r where Card_Type__c =: MyJL_Const.MY_PARTNERSHIP_CARD_TYPE AND Disabled__c = FALSE)
                                            from loyalty_account__c  where contact__c = :sourceContact.id order by join_date__c desc];                               
                                
        for(PermissionSetAssignment getper : [SELECT Id, Assignee.id, PermissionSet.Name FROM PermissionSetAssignment where 
                                              PermissionSet.Name = 'JL_MYJL_Phase_3_Contact_Center_Permissions'] ){
        	userids.add(getper.Assignee.id);	                                      	
        
        } 
		
		if(!loyActs.isempty()){
			for (Loyalty_Account__c la : loyActs) {
				contact cp = new contact(id=la.contact__c);
				ProfileWithLoyaltyAccount pla = new ProfileWithLoyaltyAccount(sourceContact.Id, cp, la);
				plaList.add(pla);
			}
		}
		
		if (plaList.isEmpty()) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.info, 'Not yet a myJL member'));
		}
    }
    
    
    private ApexPages.StandardController addFieldsToController (ApexPages.StandardController con) {
        List<String> fieldNames = new List<String>{'Id', 'Name'};
		return con;
    }

	public class ProfileWithLoyaltyAccount {
	    private Id sourceContactId;
	    private Id profileId;
	    private Id loyaltyId;
        
        public boolean isPartnershipAccount {get; private set;}
	    public String memNumber {get; private set;}
    	public String cardNumber {get; private set;}
	    public String activeMem {get; private set;}
    	public String joinMyJLDate {get; private set;}
        public String joinedMyPartnershipDate {get; private set;}

    	public string deActivationDate {get; private set;}
    	public string joinType {get; private set;}
    	public string email {get; private set;}
    	public string myJLURL {get; private set;}
        public string vouchPref {get; private set;}
    	public Boolean viewButton {get; private set;}
        public string  partnershipCardPack {get;private set;}

		Public String UPDATE_MY_VOUCHER_PREFERENCE = Label.UPDATE_MY_VOUCHER_PREFERENCE;
        public string BarcodeLabel {
        get{
                return (isPartnershipAccount)? 'My Partnership Barcode Number' : 'myJL Barcode Number';
            }
            set;
        }

        public string ViewMyButtonLabel{
            get{
                return (isPartnershipAccount)? 'View Partnership Information' : 'View my John Lewis Information';
            }
            set;
        }


    	public ProfileWithLoyaltyAccount(Id inSourceContactId, contact cp, Loyalty_Account__c la) {

            this.sourceContactId = inSourceContactId;
            this.profileId = cp.Id;
            this.loyaltyId = la.Id;
            this.memNumber = la.Name;
           // this.vouchPref = la.Voucher_Preference__c;
			
			if(la.Voucher_Preference__c != NULL){
                this.vouchPref = la.Voucher_Preference__c;
            }else{
                this.vouchPref = UPDATE_MY_VOUCHER_PREFERENCE;
            }
			
            this.joinType = la.channel_id__c != null ? (la.channel_id__c.tolowercase() == 'johnlewis.com' ? 'Online' : 'In Store') : '';
            this.email = la.Email_Address__c;
            this.activeMem = (la.Isactive__c)? 'Yes' : 'No';
            this.deActivationDate = la.Deactivation_Date_Time__c != null ? la.Deactivation_Date_Time__c.format('dd MMMM, yyyy HH:mm') : '';
            this.myJLURL = '/apex/myJL_Information?conId='+profileId+'&id='+sourceContactId+'&laId='+loyaltyId;
            this.viewButton = userids.contains(userinfo.getuserid());
            this.cardNumber = la.Customer_Loyalty_Account_Card_ID__c;

            if (this.joinType == 'In Store') {
                this.joinMyJLDate = la.Scheme_Joined_Date_Time__c != null ? la.Scheme_Joined_Date_Time__c.format('dd MMMM, yyyy HH:mm') : '';   
            } 
            else {
                this.joinMyJLDate = la.Activation_Date_Time__c != null ? la.Activation_Date_Time__c.format('dd MMMM, yyyy HH:mm') : '';
            }

            if(la.MyJL_Cards__r.size() > 0){
                this.isPartnershipAccount = true;

                Loyalty_Card__c card = la.MyJL_Cards__r[0];
                this.cardNumber = card.Card_Number__c;
                this.joinedMyPartnershipDate = card.Date_Created__c.format('dd MMMM, yyyy HH:mm');
                this.partnershipCardPack = card.Pack_Type__c;
            }
            else{
                this.isPartnershipAccount = false; 
            }
    		
    	}
    	
    	public PageReference getMyJLNewRequest() {
	    	PageReference pr = new PageReference('/apex/myJL_Information?scontrolCaching=1&id=' + sourceContactId);
	    	pr.getParameters().put('nonce', ApexPages.currentPage().getParameters().get('nonce')); 
            pr.getParameters().put('sfdcIFrameOrigin', ApexPages.currentPage().getParameters().get('sfdcIFrameOrigin')); 
    	
    		pr.setredirect(true);
    		pr.getParameters().put('laId', loyaltyId); 
	    	pr.getParameters().put('conId', profileId); 
	    	
    		return pr;
    	}
	}
}