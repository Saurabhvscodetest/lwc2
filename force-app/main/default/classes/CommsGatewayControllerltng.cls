public class CommsGatewayControllerltng {
   static final boolean SEARCH_ARCHIVE = true;
    static final boolean DO_NOT_SEARCH_ARCHIVE = false;
    public static final string MESSAGE_EMAIL_ADDRESS_LENGTH_INVALID = Comms_Gateway_Constants__c.getinstance('MESSAGE_EMAIL_ADDRESS_LENGTH_INVALID').Value__c;
    public static final INTEGER EMAIL_ADDRESS_LENGTH = INTEGER.VALUEOF(Comms_Gateway_Constants__c.getinstance('EMAIL_ADDRESS_LENGTH').Value__c);
    public static final string MESSAGE_PHONE_NO_LENGTH_INVALID_PREFIX = Comms_Gateway_Constants__c.getinstance('MESSAGE_PHONE_NO_LENGTH_INVALID_PREFIX').Value__c;
    public static final string MESSAGE_PHONE_NO_LENGTH_INVALID_SUFFIX = Comms_Gateway_Constants__c.getinstance('MESSAGE_PHONE_NO_LENGTH_INVALID_SUFFIX').Value__c;
    static final INTEGER PHONE_NUMBER_MIN_LENGTH = INTEGER.VALUEOF(Comms_Gateway_Constants__c.getinstance('PHONE_NUMBER_MIN_LENGTH').Value__c);
    static final INTEGER PHONE_NUMBER_MAX_LENGTH = INTEGER.VALUEOF(Comms_Gateway_Constants__c.getinstance('PHONE_NUMBER_MAX_LENGTH').Value__c);
    static final string NO_COMMUNICATION_FOUND_ARCHIVE = Comms_Gateway_Constants__c.getinstance('NO_COMMUNICATION_FOUND_ARCHIVE').Value__c;
    static final string MESSAGE_PROVIDE_ONLY_ONE_INPUT = Comms_Gateway_Constants__c.getinstance('MESSAGE_PROVIDE_ONLY_ONE_INPUT').Value__c;
    static final string NO_COMMUNICATION_FOUND = Comms_Gateway_Constants__c.getinstance('NO_COMMUNICATION_FOUND').Value__c;
    @AuraEnabled
   public static List<DataExtensionObject_DO> searchDelivery(string orderId,string deliveryId,string phoneNumber,string emailAddress){
       List<DataExtensionObject_DO> externalData = performSearch(DO_NOT_SEARCH_ARCHIVE, NO_COMMUNICATION_FOUND,orderId,deliveryId,phoneNumber,emailAddress);
       return externalData;
   }  
   public static List<DataExtensionObject_DO> performSearch(boolean searchArchive, String noRecordsMessage,string orderId,string deliveryId,string phoneNumber,string emailAddress) {
       ExactTargetServiceHelper.ResponseInfo respInfo = null;
         if(!Test.isRunningTest()){
       try{
               respInfo =  ExactTargetServiceHelper.getResponseList(orderId, deliveryId , phoneNumber, emailAddress, searchArchive);
               List<DataExtensionObject_DO> dataList = respInfo.responseDataList;
               System.debug('dataList####'+dataList);
               return dataList;
            } catch (System.CalloutException exp) {
                return null;
            }
   }
   else{
        return null;
   }
  }
    @AuraEnabled
     public static string fetchCustomerData(string recordId) {
       if(recordId.startsWith('500')){
            List<case> casedata = [select id,SF1_Contact_Email__c from case where id =:recordId];
            return casedata[0].SF1_Contact_Email__c; 
         }else{
             List<contact> contactdata = [select id,email from contact where id =:recordId];
            return contactdata[0].email; 
         }    
     }
}