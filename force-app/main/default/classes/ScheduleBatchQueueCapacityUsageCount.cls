global class ScheduleBatchQueueCapacityUsageCount Implements Schedulable
{
    global void execute (SchedulableContext sc)
    {
        /*BatchQueueCapcityUsageCount objBatch = new BatchQueueCapcityUsageCount();
        integer iBatchSize = 200;
        Queue_Usage_Last_Run__c setting = Queue_Usage_Last_Run__c.getOrgDefaults();
        if(setting != null && setting.BatchSize__c != null )
            iBatchSize = integer.valueOf(setting.BatchSize__c);
        
        Database.executeBatch(objBatch, iBatchSize);*/
        
        DateTime dtTimeUpdate;
        DateTime dtTimeClose;
        Boolean scheduledBatch = boolean.valueOf(label.QC_CONTINUE_BATCH_SCHEDULE);  
        
        integer totalRecordCount = 0;//
        integer totalincrementcount = 0;
        integer totalDecrementcount = 0;
        
        DateTime caseActivityCreated5min;
        DateTime caseActivityClosed5min;
        Queue_Usage_Last_Run__c setting = Queue_Usage_Last_Run__c.getOrgDefaults();
        if(setting != null && setting.QueueCapacityBatchStartDate__c != null )
            caseActivityCreated5min = setting.QueueCapacityBatchStartDate__c;
        else
            caseActivityCreated5min = datetime.now().addMinutes(-5);
        
        dtTimeUpdate = caseActivityCreated5min;
        
        if(setting != null && setting.QueueCapacityBatchCloseDate__c != null )
            caseActivityClosed5min = setting.QueueCapacityBatchCloseDate__c;
        else
            caseActivityClosed5min = datetime.now().addMinutes(-5);
        system.debug('setting.QueueCapacityBatchStartDate__c  ' + setting.QueueCapacityBatchStartDate__c );
        system.debug('setting.QueueCapacityBatchCloseDate__c   ' + setting.QueueCapacityBatchCloseDate__c );
        system.debug('caseActivityCreated5min  ' + caseActivityCreated5min );
        system.debug('caseActivityClosed5min   ' + caseActivityClosed5min );
        String query  = 'SELECT id,Activity_Slot__c,Customer_Promise_Type__c,Slot_Queue_Name__c,Activity_Date__c,Activity_Completed_Date_Time__c,Activity_Start_Date_Time__c,CreatedDate from Case_Activity__c WHERE (CreatedDate > :caseActivityCreated5min or Activity_Completed_Date_Time__c > :caseActivityClosed5min) and Slot_Queue_Name__c != null order by CreatedDate DESC limit 39999'; 
        try
        { 
        
        list<Case_Activity__c> lstCaseActivity = database.query(query);
        
        if(lstCaseActivity != null && lstCaseActivity.size() > 0)
        {
            
                integer dtSlot1StartDate = 8;
                integer dtSlot2StartDate = 10;
                integer dtSlot3StartDate = 12;
                integer dtSlot4StartDate = 14;
                integer dtSlot5StartDate = 16;
                
                integer dtSlot1EndDate = 12;
                integer dtSlot2EndDate = 14;
                integer dtSlot3EndDate = 16;
                integer dtSlot4EndDate = 18;
                integer dtSlot5EndDate = 20;
                
                set<string> setQueueName = new set<string>();
                set<date> setActivityDate = new set<date>();
                
                totalRecordCount += lstCaseActivity.size();
                if(dtTimeUpdate == null || dtTimeUpdate < lstCaseActivity[0].CreatedDate)
                    dtTimeUpdate = lstCaseActivity[0].CreatedDate;
                        
                /*if(dtTimeClose == null || dtTimeClose < lstCaseActivity[0].Activity_Completed_Date_Time__c)
                    dtTimeClose = lstCaseActivity[0].Activity_Completed_Date_Time__c;
                else dtTimeClose = datetime.now().addMinutes(-5);*/
                
                if(setting != null)
                {  
                    dtSlot5StartDate = integer.valueOf(setting.Slot_Time_5_Interval__c.split(':')[0]);
                    dtSlot4StartDate = integer.valueOf(setting.Slot_Time_4_Interval__c.split(':')[0]);
                    dtSlot3StartDate = integer.valueOf(setting.Slot_Time_3_Interval__c.split(':')[0]);
                    dtSlot2StartDate = integer.valueOf(setting.Slot_Time_2_Interval__c.split(':')[0]);
                    dtSlot1StartDate = integer.valueOf(setting.Slot_Time_1_Interval__c.split(':')[0]);
                    
                    dtSlot5EndDate = integer.valueOf(setting.Slot_Time_5_Interval__c.split(':')[1]);
                    dtSlot4EndDate = integer.valueOf(setting.Slot_Time_4_Interval__c.split(':')[1]);
                    dtSlot3EndDate = integer.valueOf(setting.Slot_Time_3_Interval__c.split(':')[1]);
                    dtSlot2EndDate = integer.valueOf(setting.Slot_Time_2_Interval__c.split(':')[1]);
                    dtSlot1EndDate = integer.valueOf(setting.Slot_Time_1_Interval__c.split(':')[1]);
                }
                
                for(Case_Activity__c CaseAct: lstCaseActivity)
                {
                     if(CaseAct.Activity_Completed_Date_Time__c != null && (dtTimeClose == null || dtTimeClose < CaseAct.Activity_Completed_Date_Time__c))
                        dtTimeClose = CaseAct.Activity_Completed_Date_Time__c;
                    
                    if(CaseAct.Slot_Queue_Name__c != null && CaseAct.Slot_Queue_Name__c != '')
                        setQueueName.add(CaseAct.Slot_Queue_Name__c);
                    if(CaseAct.Activity_Start_Date_Time__c!= null)
                        setActivityDate.add(CaseAct.Activity_Start_Date_Time__c.date()); 
                }
                map<string,Queue_Capacity_Usage__c> mapQueueCapacityUsage = new map<string,Queue_Capacity_Usage__c>();
                map<string,Queue_Capacity_Usage__c> mapQueueCapacityUsage_New = new map<string,Queue_Capacity_Usage__c>();
                if(setQueueName.size() > 0 && setActivityDate.size() > 0)
                {
                    list<Queue_Capacity_Usage__c> lstQueCapUsage = [select id,Queue_Name__c,Slot_Date__c,Slot1__c,Slot2__c,Slot3__c
                    ,Slot4__c,Slot5__c from Queue_Capacity_Usage__c where Queue_Name__c != null and Queue_Name__c != '' and 
                    Slot_Date__c != null and Queue_Name__c in :setQueueName and Slot_Date__c in :setActivityDate];
                    if(lstQueCapUsage != null && lstQueCapUsage.size() > 0)
                    {
                        for(Queue_Capacity_Usage__c QueCapUsg:lstQueCapUsage)
                        {
                            if(QueCapUsg.Queue_Name__c != null && QueCapUsg.Queue_Name__c != '' && QueCapUsg.Slot_Date__c != null)
                                mapQueueCapacityUsage.put(QueCapUsg.Queue_Name__c + string.valueof(QueCapUsg.Slot_Date__c),QueCapUsg);
                        }
                    }
                    set<Date> setNewSlotDate = new set<Date>();
                    set<string> setNewQueue = new set<string>();
                    for(Case_Activity__c CaseAct1: lstCaseActivity)
                    {
                        string keyQueCapUsg1 = CaseAct1.Slot_Queue_Name__c + string.valueof(CaseAct1.Activity_Start_Date_Time__c.Date() );
                        if(!mapQueueCapacityUsage.containskey(keyQueCapUsg1))
                        {
                            setNewSlotDate.add(CaseAct1.Activity_Start_Date_Time__c.Date() );
                            setNewQueue.add(CaseAct1.Slot_Queue_Name__c);
                            if(CaseAct1.Slot_Queue_Name__c != null && CaseAct1.Slot_Queue_Name__c != '' && CaseAct1.Activity_Start_Date_Time__c.Date() != null)
                                mapQueueCapacityUsage_New.put(keyQueCapUsg1,new Queue_Capacity_Usage__c(Queue_Capacity_Limit__c = null, Queue_Name__c = CaseAct1.Slot_Queue_Name__c,Slot_Date__c = CaseAct1.Activity_Start_Date_Time__c.Date(),Slot1__c = 0,Slot2__c = 0,Slot3__c = 0,Slot4__c = 0,Slot5__c = 0));
                        }
                    }
                    
                    if(mapQueueCapacityUsage_New.size() > 0)
                    {
                        //map<string,Queue_Capacity_Limit__c> mapNewQueCapLimit = 
                        list<Queue_Capacity_Limit__c> lstQueCapLimit = [select id,Slot_Date__c,Queue_Name__c from Queue_Capacity_Limit__c where Queue_Name__c in :setNewQueue and Slot_Date__c in :setNewSlotDate];
                        if(lstQueCapLimit != null && lstQueCapLimit.size() > 0)
                        {
                            for(Queue_Capacity_Limit__c QueCaplmt: lstQueCapLimit)
                            {
                                string keyQueCaplmt = QueCaplmt.Queue_Name__c + string.valueof(QueCaplmt.Slot_Date__c);
                                if(mapQueueCapacityUsage_New.containskey(keyQueCaplmt))
                                {
                                    mapQueueCapacityUsage_New.get(keyQueCaplmt).Queue_Capacity_Limit__c = QueCaplmt.Id;
                                    mapQueueCapacityUsage.put(keyQueCaplmt,mapQueueCapacityUsage_New.get(keyQueCaplmt));
                                }
                            }
                        }
                    }
                    
                    if(mapQueueCapacityUsage.size() > 0)
                    {
                        boolean bUpdateQueueCapacityUsage = false;
                        for(Case_Activity__c CaseActivity: lstCaseActivity)
                        {
                            //if(setCustomer_Promise_Type.contains(CaseActivity.Customer_Promise_Type__c))
                            //{
                                if(CaseActivity.Slot_Queue_Name__c != null && CaseActivity.Slot_Queue_Name__c != '' && CaseActivity.Activity_Start_Date_Time__c.Date() != null && CaseActivity.Activity_Start_Date_Time__c != null)
                                {
                                    string keyQueCapUsg = CaseActivity.Slot_Queue_Name__c + string.valueof(CaseActivity.Activity_Start_Date_Time__c.Date() );
                                    if(mapQueueCapacityUsage.containskey(keyQueCapUsg))
                                    {
                                        integer iActStartHour = CaseActivity.Activity_Start_Date_Time__c.hour();
                                        integer iActStartMin = CaseActivity.Activity_Start_Date_Time__c.minute();
                                        
                                        boolean bDeductSlotCount = false;
                                        
                                        if(CaseActivity.Activity_Completed_Date_Time__c != null && CaseActivity.Activity_Start_Date_Time__c > CaseActivity.Activity_Completed_Date_Time__c)
                                        {
                                            integer iActCompleteTimeInterval = 4;
                                            string strActCompleteTimeInterval = Label.QC_ACT_COMPLETE_HOUR_DIFFERENCE;
                                            if(strActCompleteTimeInterval != null && strActCompleteTimeInterval != '' && strActCompleteTimeInterval.isNumeric())
                                                iActCompleteTimeInterval = integer.valueOf(strActCompleteTimeInterval);
                                            DateTime dt = CaseActivity.Activity_Start_Date_Time__c;
                                            DateTime dt2 = CaseActivity.Activity_Completed_Date_Time__c;
                                            Integer days = Date.valueOf(dt2).daysBetween(date.valueof(dt));
                                            Integer hours = math.mod(Integer.valueOf((dt.getTime() - (dt2).getTime())/(1000*60*60)),24);                
                                            Integer minutes = math.mod(Integer.valueOf((dt.getTime() - (dt2).getTime())/(1000*60)),60);
                                            system.debug('~~~days='+days+', hours='+hours+', minutes='+minutes);
                                            
                                            integer iTimeDiff = Integer.ValueOf((Days*24*60) +  (Hours*60) + minutes);
                                            if(iTimeDiff >= iActCompleteTimeInterval * 60)
                                                bDeductSlotCount = true;
                                        }
                                        // Slot 1
                                        if((CaseActivity.Activity_Slot__c != null && CaseActivity.Activity_Slot__c != '' && CaseActivity.Activity_Slot__c == '1') || ( CaseActivity.Activity_Slot__c == '0' && (((iActStartHour * 60 + iActStartMin) == (dtSlot1StartDate * 60)) || ((iActStartHour * 60 + iActStartMin) > (dtSlot1StartDate * 60) && ((dtSlot1EndDate * 60) - (iActStartHour * 60 + iActStartMin) >= 120)))))
                                        {
                                            system.debug('In Slot 1 : dtSlot5StartDate.hour ' + dtSlot1StartDate + ' ' + dtSlot1StartDate + ' iActStartHour : ' + iActStartHour + ' iActStartMin: ' + iActStartMin + ' (iActStartHour * 60 + iActStartMin) ' + (iActStartHour * 60 + iActStartMin) + ' (dtSlot1StartDate.hour() * 60) ' + (dtSlot1StartDate * 60));
                                            if(CaseActivity.Activity_Completed_Date_Time__c == null )
                                            {
                                                totalincrementcount += 1;
                                                bUpdateQueueCapacityUsage = true;
                                                if(mapQueueCapacityUsage.get(keyQueCapUsg).Slot1__c == null)
                                                    mapQueueCapacityUsage.get(keyQueCapUsg).Slot1__c = 1;
                                                else
                                                    mapQueueCapacityUsage.get(keyQueCapUsg).Slot1__c += 1;
                                            }
                                                //else if(CaseActivity.Activity_Completed_Date_Time__c != null && CaseActivity.Activity_Completed_Date_Time__c > dtTimeUpdate)
                                            else if(CaseActivity.Activity_Completed_Date_Time__c != null && bDeductSlotCount == true)
                                            {
                                                if(mapQueueCapacityUsage.get(keyQueCapUsg).Slot1__c != null && mapQueueCapacityUsage.get(keyQueCapUsg).Slot1__c >= 1)
                                                {
                                                    totalDecrementcount += 1;
                                                    bUpdateQueueCapacityUsage = true;
                                                    mapQueueCapacityUsage.get(keyQueCapUsg).Slot1__c -= 1;
                                                }
                                            }
                                        }
                                        // Slot 2
                                        else if((CaseActivity.Activity_Slot__c != null && CaseActivity.Activity_Slot__c != '' && CaseActivity.Activity_Slot__c == '2') ||( CaseActivity.Activity_Slot__c == '0' && (((iActStartHour * 60 + iActStartMin) == (dtSlot2StartDate * 60)) || ((iActStartHour * 60 + iActStartMin) > (dtSlot2StartDate * 60) && ((dtSlot2EndDate * 60) - (iActStartHour * 60 + iActStartMin) >= 120)))))
                                        {
                                            system.debug('In Slot 2 : dtSlot2StartDate.hour ' + dtSlot2StartDate+ ' ' + dtSlot2StartDate + ' iActStartHour : ' + iActStartHour + ' iActStartMin: ' + iActStartMin + ' (iActStartHour * 60 + iActStartMin) ' + (iActStartHour * 60 + iActStartMin) + ' (dtSlot2StartDate.hour() * 60) ' + (dtSlot2StartDate * 60));
                                            if(CaseActivity.Activity_Completed_Date_Time__c == null )
                                            {
                                                totalincrementcount += 1;
                                                bUpdateQueueCapacityUsage = true;
                                                if(mapQueueCapacityUsage.get(keyQueCapUsg).Slot2__c == null)
                                                    mapQueueCapacityUsage.get(keyQueCapUsg).Slot2__c = 1;
                                                else
                                                    mapQueueCapacityUsage.get(keyQueCapUsg).Slot2__c += 1;
                                            }
                                                //else if(CaseActivity.Activity_Completed_Date_Time__c != null && CaseActivity.Activity_Completed_Date_Time__c > dtTimeUpdate)
                                            else if(CaseActivity.Activity_Completed_Date_Time__c != null && bDeductSlotCount == true)
                                            {
                                                if(mapQueueCapacityUsage.get(keyQueCapUsg).Slot2__c != null && mapQueueCapacityUsage.get(keyQueCapUsg).Slot2__c >= 1)
                                                {
                                                    totalDecrementcount += 1;
                                                    bUpdateQueueCapacityUsage = true;
                                                    mapQueueCapacityUsage.get(keyQueCapUsg).Slot2__c -= 1;
                                                }
                                            }
                                        }
                                        // slot 3
                                        else if((CaseActivity.Activity_Slot__c != null && CaseActivity.Activity_Slot__c != '' && CaseActivity.Activity_Slot__c == '3') || ( CaseActivity.Activity_Slot__c == '0' && (((iActStartHour * 60 + iActStartMin) == (dtSlot3StartDate * 60)) || ((iActStartHour * 60 + iActStartMin) > (dtSlot3StartDate * 60)  && ((dtSlot3EndDate * 60) - (iActStartHour * 60 + iActStartMin) >= 120)))))
                                        {
                                            system.debug('In Slot 3 : dtSlot3StartDate.hour() ' + dtSlot3StartDate+ ' ' + dtSlot3StartDate + ' iActStartHour : ' + iActStartHour + ' iActStartMin: ' + iActStartMin + ' (iActStartHour * 60 + iActStartMin) ' + (iActStartHour * 60 + iActStartMin) + ' (dtSlot3StartDate.hour() * 60) ' + (dtSlot3StartDate * 60));
                                            if(CaseActivity.Activity_Completed_Date_Time__c == null )
                                            {
                                                totalincrementcount += 1;
                                                bUpdateQueueCapacityUsage = true;
                                                if(mapQueueCapacityUsage.get(keyQueCapUsg).Slot3__c == null)
                                                    mapQueueCapacityUsage.get(keyQueCapUsg).Slot3__c = 1;
                                                else
                                                    mapQueueCapacityUsage.get(keyQueCapUsg).Slot3__c += 1;
                                            }
                                                //else if(CaseActivity.Activity_Completed_Date_Time__c != null && CaseActivity.Activity_Completed_Date_Time__c > dtTimeUpdate)
                                            else if(CaseActivity.Activity_Completed_Date_Time__c != null && bDeductSlotCount == true)
                                            {
                                                if(mapQueueCapacityUsage.get(keyQueCapUsg).Slot3__c != null && mapQueueCapacityUsage.get(keyQueCapUsg).Slot3__c >= 1)
                                                {
                                                    totalDecrementcount += 1;
                                                    bUpdateQueueCapacityUsage = true;
                                                    mapQueueCapacityUsage.get(keyQueCapUsg).Slot3__c -= 1;
                                                }
                                            }
                                        }
                                        // slot 4
                                        else if((CaseActivity.Activity_Slot__c != null && CaseActivity.Activity_Slot__c != '' && CaseActivity.Activity_Slot__c == '4') || ( CaseActivity.Activity_Slot__c == '0' && ( ((iActStartHour * 60 + iActStartMin) == (dtSlot4StartDate * 60)) || ((iActStartHour * 60 + iActStartMin) > (dtSlot4StartDate * 60)  && ((dtSlot4EndDate * 60) - (iActStartHour * 60 + iActStartMin) >= 120)))))
                                        {
                                            system.debug('In Slot 4 : dtSlot4StartDate.hour ' + dtSlot4StartDate + ' ' + dtSlot4StartDate + 'iActStartHour ' + iActStartHour + ' iActStartMin: ' + iActStartMin + ' (iActStartHour * 60 + iActStartMin) ' + (iActStartHour * 60 + iActStartMin) + ' (dtSlot4StartDate.hour() * 60) ' + (dtSlot4StartDate* 60));
                                            if(CaseActivity.Activity_Completed_Date_Time__c == null )
                                            {
                                                totalincrementcount += 1;
                                                bUpdateQueueCapacityUsage = true;
                                                if(mapQueueCapacityUsage.get(keyQueCapUsg).Slot4__c == null)
                                                    mapQueueCapacityUsage.get(keyQueCapUsg).Slot4__c = 1;
                                                else
                                                    mapQueueCapacityUsage.get(keyQueCapUsg).Slot4__c += 1;
                                            }
                                                //else if(CaseActivity.Activity_Completed_Date_Time__c != null && CaseActivity.Activity_Completed_Date_Time__c > dtTimeUpdate)
                                            else if(CaseActivity.Activity_Completed_Date_Time__c != null && bDeductSlotCount == true)
                                            {
                                                if(mapQueueCapacityUsage.get(keyQueCapUsg).Slot4__c != null && mapQueueCapacityUsage.get(keyQueCapUsg).Slot4__c >= 1)
                                                {
                                                    totalDecrementcount += 1;
                                                    bUpdateQueueCapacityUsage = true;
                                                    mapQueueCapacityUsage.get(keyQueCapUsg).Slot4__c -= 1;
                                                }
                                            }
                                        }
                                        // slot 5
                                        else if((CaseActivity.Activity_Slot__c != null && CaseActivity.Activity_Slot__c != '' && CaseActivity.Activity_Slot__c == '5') || ( CaseActivity.Activity_Slot__c == '0' && ( ((iActStartHour * 60 + iActStartMin) == (dtSlot5StartDate * 60)) ||((iActStartHour * 60 + iActStartMin) > (dtSlot5StartDate * 60)  && ((dtSlot5EndDate * 60) - (iActStartHour * 60 + iActStartMin) >= 120)))))
                                        {
                                            system.debug('In Slot 5 : dtSlot5StartDate.hour ' + dtSlot5StartDate + ' ' + dtSlot5StartDate + ' iActStartHour : ' + iActStartHour + ' iActStartMin: ' + iActStartMin + ' (iActStartHour * 60 + iActStartMin) ' + (iActStartHour * 60 + iActStartMin) + ' (dtSlot5StartDate.hour() * 60) ' + (dtSlot5StartDate * 60));
                                            if(CaseActivity.Activity_Completed_Date_Time__c == null )
                                            {
                                                totalincrementcount += 1;
                                                bUpdateQueueCapacityUsage = true;
                                                if(mapQueueCapacityUsage.get(keyQueCapUsg).Slot5__c == null)
                                                    mapQueueCapacityUsage.get(keyQueCapUsg).Slot5__c = 1;
                                                else
                                                    mapQueueCapacityUsage.get(keyQueCapUsg).Slot5__c += 1;
                                            }
                                            //else if(CaseActivity.Activity_Completed_Date_Time__c != null && CaseActivity.Activity_Completed_Date_Time__c > dtTimeUpdate)
                                            else if(CaseActivity.Activity_Completed_Date_Time__c != null && bDeductSlotCount == true)
                                            {
                                                if(mapQueueCapacityUsage.get(keyQueCapUsg).Slot5__c != null && mapQueueCapacityUsage.get(keyQueCapUsg).Slot5__c >= 1)
                                                {
                                                    totalDecrementcount += 1;
                                                    bUpdateQueueCapacityUsage = true;
                                                    mapQueueCapacityUsage.get(keyQueCapUsg).Slot5__c -= 1;
                                                }
                                            }
                                        }
                                    }
                                }
                            //}
                            
                        }
                        system.debug('## bUpdateQueueCapacityUsage: ' + bUpdateQueueCapacityUsage);
                        system.debug('## mapQueueCapacityUsage.values(): ' + mapQueueCapacityUsage.values());
                        if(bUpdateQueueCapacityUsage == true)
                            upsert mapQueueCapacityUsage.values();
                    }
                    
                }
                
           
        }
        }
        catch(exception ex)
        {
            system.debug('Exception from ScheduleBatchQueueCapacityUsageCount for Queue capacity update' + ex);
            sendNotification('ScheduleBatchQueueCapacityUsageCount', 'Queue capacity usage upsert',  ex);
        }
        
        try
        {
            Queue_Usage_Last_Run__c Custsetting = Queue_Usage_Last_Run__c.getOrgDefaults();
            if(Custsetting != null )
            {
                system.debug('## dtTimeUpdate dtTimeClose : ' + dtTimeUpdate + ' ' + dtTimeClose);
                if(dtTimeUpdate != null)            Custsetting.QueueCapacityBatchStartDate__c = dtTimeUpdate;
                if(dtTimeClose != null)            Custsetting.QueueCapacityBatchCloseDate__c = dtTimeClose;
                upsert Custsetting; 
            }
            system.debug('## setting.QueueCapacityBatchStartDate__c: ' + Custsetting.QueueCapacityBatchStartDate__c);
            system.debug('## setting.QueueCapacityBatchCloseDate__c: ' + Custsetting.QueueCapacityBatchCloseDate__c);
            system.debug('## totalRecordCount: ' + totalRecordCount);
            system.debug('## totalincrementcount: ' + totalincrementcount);
            system.debug('## totalDecrementcount: ' + totalincrementcount);
            
            
            //cron exp to schedule batch
            //Minutes+5 to execute the batch in every 5minutes
            integer iSchedulerTimeInterval = 5;
            string strSchedulerTimeInterval = Label.QC_SCHEDULER_TIME_INTERVAL;
            if(strSchedulerTimeInterval != null && strSchedulerTimeInterval != '' && strSchedulerTimeInterval.isNumeric())
                iSchedulerTimeInterval = integer.valueOf(strSchedulerTimeInterval);
            datetime dtCronDate = datetime.now().addminutes(iSchedulerTimeInterval);
            String strSchedule = '0 ' + string.valueOf(dtCronDate.minute()) + ' ' + string.valueOf(dtCronDate.hour())
                + ' ' + string.valueOf(dtCronDate.day()) + ' ' + string.valueOf(dtCronDate.month())
                + ' ?' + ' ' +  string.valueOf(dtCronDate.year());
            
            //To schedule the batch
            if(scheduledBatch){
               list<CronTrigger> lstsch= [SELECT Id, CronJobDetail.Name,CronJobDetail.Id, State FROM CronTrigger  where CronJobDetail.Name = 'Queue Capcity Usage Count Batch' limit 1];
                if(lstsch != null && lstsch.size() > 0 )    System.abortJOb(lstsch[0].Id);
                System.schedule('Queue Capcity Usage Count Batch', strSchedule, new ScheduleBatchQueueCapacityUsageCount());
            }
            else{
                System.debug('No batch job available to be scheduled');
            }  
        }
        catch(exception ex)
        {
            system.debug('Exception from ScheduleBatchQueueCapacityUsageCount for self Scheduling' + ex);
            sendNotification('ScheduleBatchQueueCapacityUsageCount', 'Schedule next job',  ex);
        }
        
    }
    
    public void sendNotification(String subject, String ErrorFrom, Exception ex){

        Config_Settings__c emailEnabledForBatch = Config_Settings__c.getInstance('Enable Email Logging for Batch');
            
        if(emailEnabledForBatch != null && emailEnabledForBatch.Checkbox_Value__c) {
             
                String emailBody = '';

                emailBody += subject+' job has run: \n\n\n';
                emailBody += 'The following Error occurred.: '+ ErrorFrom +' \n\n';
                emailBody += 'Type: '+ex.getTypeName();
                emailBody += '\n\n';
                emailBody += 'Message: '+ex.getMessage();
                emailBody += '\n\n';
                emailBody += 'Line Number: '+ex.getLineNumber();
                emailBody += '\n\n';
                emailBody += 'StackTrace: '+ex.getStackTraceString();
                emailBody += '\n\n';
                EmailNotifier.sendNotificationEmail(subject, emailBody);
            
        }
    }
}