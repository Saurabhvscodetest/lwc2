@isTest
public class NewCustomerContactLExControllerTest {

    public static final String CONTACT_CENTRE_TEAM_NAME = '	Hamilton - CRD';
    public static final String CONTACT_CENTRE_QUEUE_NAME = 'CRD_Work_Allocation_Hamilton';
    
    @isTest static void testGenerateLevelOnePicklist() {
        Test.startTest();
        List<String> levelOnePicklist = new List<String>();
        levelOnePicklist = NewCustomerContactLExController.initiateLevelOnePicklist();
        system.assert(levelOnePicklist.size()>1);
        Test.stopTest();
    }
    
    @isTest static void testDependentPicklistMap() {
        Test.startTest();
        Map<String,Map<String,List<String>>> dependentPicklistMap = new Map<String,Map<String,List<String>>>();
        dependentPicklistMap = NewCustomerContactLExController.generateTaskDependentFieldMap();
        system.assert(dependentPicklistMap.size()>1);
        system.assert(dependentPicklistMap.containsKey('Level-2'));
        system.assert(dependentPicklistMap.containsKey('Level-3'));
        system.assert(dependentPicklistMap.containsKey('Level-4'));
        system.assert(dependentPicklistMap.containsKey('Level-5'));
        Test.stopTest();
    }

    @isTest static void testGenerateCaseAssignmentLevelOnePicklist() {
        Test.startTest();
        List<String> levelOnePicklist = new List<String>();
        levelOnePicklist = NewCustomerContactLExController.initiateCaseAssignmentLevelOnePicklist();
        system.assert(levelOnePicklist.size()>1);
        Test.stopTest();
    }

    @isTest static void testGenerateCaseAssignmentLevelOnePicklistPSE() {
        Test.startTest();
        List<String> levelOnePicklist = new List<String>();
        levelOnePicklist = NewCustomerContactLExController.initiateCaseAssignmentLevelOnePicklistPSE();
        system.assert(levelOnePicklist.size()>1);
        Test.stopTest();
    }
    
    @isTest static void testCaseAssignmentFieldMap() {
        Test.startTest();
        Map<String,Map<String,List<String>>> dependentPicklistMap = new Map<String,Map<String,List<String>>>();
        dependentPicklistMap = NewCustomerContactLExController.generateCaseAssignmentFieldMap();
        system.assert(dependentPicklistMap.size()>1);
        system.assert(dependentPicklistMap.containsKey('ContactReason'));
        system.assert(dependentPicklistMap.containsKey('ReasonDetail'));
        system.assert(dependentPicklistMap.containsKey('CDH'));
        system.assert(dependentPicklistMap.containsKey('BranchDepartment'));
        Test.stopTest();
    }

    @isTest static void testCallLogAndCaseCreation() {
        
        CustomSettingTestDataFactory.initialiseTeamCustomSettings(CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_QUEUE_NAME);

        User runningUser = UserTestDataFactory.getRunningUser();

        User testUser;
        Contact testContact;

        System.runAs(runningUser) {

            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;

            UserTestDataFactory.assignPermissionSetToUser(testUser.Id, 'Call_Logging_Access');
        
        }

        System.runAs(testUser) {
        
            Test.startTest();
            testContact = CustomerTestDataFactory.createContact();
            insert testContact;
            system.assert(testContact.Id != NULL);
            Case testCase = CaseTestDataFactory.createComplaintCase(testContact.Id);
            insert testCase;
            casecomment cd = new casecomment();
            cd.ParentId = testCase.Id;
            cd.CommentBody = 'Call Logged';
            insert cd;
            system.assert(testCase.Id != NULL);
            system.assert(NewCustomerContactLExController.fetchCustomerDetails(testCase.Id) == testContact.Id);
            Task tempTask = new Task(
            	Description = 'Sample Description', WhoId = testContact.Id, Level_1__c = 'JohnLewis.com', Level_2__c = 'JL.Com Delivery',Level_3__c = 'JL.Com Chasing Delivery',Level_4__c = 'JL.Com Inside Scheduled Delivery Time',Level_5__c = 'JL.Com Collect +',CRNumber__c = '12345678',
                Delivery_Number__c = '98797865',Order_Number__c = '69285195',Product_Number__c = '39571639',Case__c = testCase.Id
            );

            String taskJSON = JSON.serialize(tempTask);
            String Id = testCase.Id;
            String description = 'Test Comment';
            NewCustomerContactLExController.createCallLog(taskJSON,Id,description);
            Case tempCase = CaseTestDataFactory.createComplaintCase(testContact.Id);
            String caseJSON = JSON.serialize(tempCase);
    	    Map<String, String> caseIdToCaseRedirectUrlMap = NewCustomerContactLExController.createCase(caseJSON,taskJSON,true);

            system.assert(caseIdToCaseRedirectUrlMap.size() == 2);
            system.assert(caseIdToCaseRedirectUrlMap.containsKey('CaseId'));
            system.assert(caseIdToCaseRedirectUrlMap.containsKey('redirectURL'));
            system.assert(caseIdToCaseRedirectUrlMap.get('redirectURL').contains(caseIdToCaseRedirectUrlMap.get('CaseId')));


            Task callLogRecord = [select Id,WhoId, Subject, Status, Priority, Owner.Name, Description, Level_1__c, Level_2__c, Level_3__c, Level_4__c, Order_Number__c, Level_5__c, Product_Number__c, Delivery_Number__c, CRNumber__c,Case__c from Task LIMIT 1].get(0);

            system.assert(callLogRecord.Id != NULL);
            system.assert(callLogRecord.WhoId == testContact.Id);
            system.assert(callLogRecord.Description == 'Sample Description');
            system.assert(callLogRecord.Level_1__c == 'JohnLewis.com');
            system.assert(callLogRecord.Level_2__c == 'JL.Com Delivery');
            system.assert(callLogRecord.Level_3__c == 'JL.Com Chasing Delivery');
            system.assert(callLogRecord.Level_4__c == 'JL.Com Inside Scheduled Delivery Time');
            system.assert(callLogRecord.Level_5__c == 'JL.Com Collect +');
            system.assert(callLogRecord.CRNumber__c == '12345678');
            system.assert(callLogRecord.Delivery_Number__c == '98797865');
            system.assert(callLogRecord.Order_Number__c == '69285195');
            system.assert(callLogRecord.Product_Number__c == '39571639');

            Test.stopTest();
        }
    }
    
    @isTest static void testCustomerResults(){
        
        User runningUser = UserTestDataFactory.getRunningUser();

        User testUser;
        Contact testContact;

        System.runAs(runningUser) {

            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;

            UserTestDataFactory.assignPermissionSetToUser(testUser.Id, 'Call_Logging_Access');
        
        }

        System.runAs(testUser) {

            Test.startTest();
            testContact = CustomerTestDataFactory.createContact();
            Id normalCustomerRecordTypeId = [select Id, Name from RecordType where SObjectType = 'Contact' AND Name = 'Normal'].get(0).Id;
            testContact.RecordTypeId = normalCustomerRecordTypeId;
            insert testContact;
            system.assert(testContact.Id != NULL);

            List<Contact> contactSearchResults = new List<Contact>();
            contactSearchResults = NewCustomerContactLExController.getCustomerResults('Test');
            Boolean callLogPermissionSetAssigned = NewCustomerContactLExController.isPermSetAssignedToUser('Call_Logging_Access');
            system.assert(callLogPermissionSetAssigned == true);
            
            UserTestDataFactory.assignPermissionSetToUser(testUser.Id, System.Label.Create_PSE_Tab_PS_Name);
            
            Boolean capitaProfileUser = NewCustomerContactLExController.isCreatePSETabEnabled();
            system.assert(capitaProfileUser == false);

            User currentUser = NewCustomerContactLExController.getCurrentUserDetails();
            system.assert(currentUser.Id == UserInfo.getUserId());
            
            insert new Team__c(Name = 'Branch - CST White City', Contact_Centre__c = 'Branch WHITE CITY', Queue_Name__c = 'Branch_WHITE_CITY',Task_Assignee__c='to.be.assigned@johnlewis.com');
            Id userQueue = NewCustomerContactLExController.getUsersPrimaryTeamQueueId('Branch - CST White City');
            
            String pseQueue = NewCustomerContactLExController.fetchPSEQueue('Branch-Aberdeen');
            List<String> availableDates = NewCustomerContactLExController.fetchAvailableSlots('Branch - CST White City');
            //NewCustomerContactLExController.getQueue();
            system.assert(userQueue != NULL);

            List<Id> recordIdList = NewCustomerContactLExController.getRecordTypeInformation();
            system.assert(recordIdList.size() == 3);
            Test.stopTest();

        }
    }
     
	@isTest static void UserHasKnowledgeAccessCheckTest() {
		Test.startTest();
        	NewCustomerContactLExController.UserHasKnowledgeAccessCheck();
        Test.stopTest();
     }
    
    /* COPT-5415 */
    @isTest static void getQueueTest() {
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        System.runAs(runningUser){
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;       
            insert CustomSettingTestDataFactory.createConfigSettings();
        }        
        System.runAs(testUser){
            Test.startTest();
            try {
            	NewCustomerContactLExController.getQueue();
            }
            catch(Exception e) { }
			Test.stopTest();
        }
    }
    
    @isTest static void fetchRelatedCustomerDetailsTest() {
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        System.runAs(runningUser){
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;       
            insert CustomSettingTestDataFactory.createConfigSettings();
        }        
        System.runAs(testUser){            
            contact testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'test@gmail.com';
            insert testContact;    
            
            
            case testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            insert testCase;
            
            LiveChatVisitor testvisitor=new LiveChatVisitor();
          //  testvisitor.CreatedDate=System.today();
            insert testvisitor;
            
            LiveChatTranscript lCT = new LiveChatTranscript(createddate = system.now(),LiveChatVisitorId = testvisitor.Id,CaseId = testCase.id);
            insert lCT;
            
            Test.startTest();
                NewCustomerContactLExController.fetchRelatedCustomerDetails(lCT.Id);
            Test.stopTest();
        }
    }
	/* COPT-5415 */

    @isTest static void testExceptionScenarioOnCase(){
        Test.startTest();
        NewCustomerContactLExController.createCase(JSON.serialize(new Case()),JSON.serialize(new Task()),true);
        Test.stopTest();
    }
    
     @isTest static void testExceptionScenarioOnTask(){
        Test.startTest();
        NewCustomerContactLExController.createCallLogTask(new Task());
        Test.stopTest();
    }
    
    @isTest static void ctestCreateContact(){
        Test.startTest();
        Id normalCustomerRecordTypeId = [select Id, Name from RecordType where SObjectType = 'Contact' AND Name = 'Normal'].get(0).Id;
        Contact tempContact = CustomerTestDataFactory.createContact();
        String contactJSON = JSON.serialize(tempContact);
        List<String> returnValue = NewCustomerContactLExController.createContact(contactJSON, String.valueOf(normalCustomerRecordTypeId));
        system.assert(returnValue.size()==3);
        system.assert(returnValue.get(0) == 'Success');
        system.assert(returnValue.get(1) != NULL);
        system.assert(returnValue.get(2) != NULL);
              
        Test.stopTest();
    }
    
    @isTest static void testRelatedCaseMethods(){
        CustomSettingTestDataFactory.initialiseTeamCustomSettings(CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_QUEUE_NAME);
        
        User runningUser = UserTestDataFactory.getRunningUser();
        
        User testUser;
        System.runAs(runningUser) {
            
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
            
            UserTestDataFactory.assignPermissionSetToUser(testUser.Id, 'Call_Logging_Access');
            
        }
        
        System.runAs(testUser) {
            Contact testContact = CustomerTestDataFactory.createContact();
            insert testContact;
            system.assert(testContact.Id != NULL);
            Case testCase = CaseTestDataFactory.createComplaintCase(testContact.Id);
            insert testCase;
            system.assert(testCase.Id != NULL);
            
            Test.startTest();
            Case currentCase = NewCustomerContactLExController.initializeRelatedCaseComponent(testCase.Id);
            String recordType = NewCustomerContactLExController.getRecordType(testCase.Id);
            Map<String, SObject> fetchCaseAssignment = NewCustomerContactLExController.fetchCaseAssignmentDetails(testCase.Id);
            String fetchQue = NewCustomerContactLExController.fetchQueue(testcase.jl_Branch_master__c,testCase.Contact_Reason__c, testCase.Reason_Detail__c,'');
            system.assert(currentCase.Id == null);
            system.assert(currentCase.jl_Action_Taken__c == 'New case created');
            system.assert(currentCase.Create_Case_Activity__c == false);
            system.assert(currentCase.jl_InitiatingSource__c == 'Contact Centre');
            system.assert(currentCase.Origin == 'Phone');
            system.assert(currentCase.Status == 'New');
            system.assert(currentCase.RecordTypeId == Schema.SObjectType.Case.getRecordTypeInfosByName().get('New Case').getRecordTypeId());
            system.assert(currentCase.jl_Branch_master__c == '');
            system.assert(currentCase.Contact_Reason__c == '');
            system.assert(currentCase.Reason_Detail__c == '');
            system.assert(currentCase.CDH_Site__c == '');
            system.assert(currentCase.Branch_Department__c == '');
            system.assert(currentCase.ParentId == testCase.Id);
            system.assert(currentCase.jl_Transfer_case__c == true);
            system.assert(currentCase.Description == '');
            
            Case relatedCase = NewCustomerContactLExController.createRelatedCase(JSON.serialize(currentCase));
            system.assert(relatedCase.Id != null);
            system.assert(!String.isEmpty(relatedCase.CaseNumber));
            Test.stopTest();
        }
    }
    
    @isTest static void testAssignmentQuickActionMethods(){
        CustomSettingTestDataFactory.initialiseTeamCustomSettings(CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_QUEUE_NAME);
        
        User runningUser = UserTestDataFactory.getRunningUser();
        
        User testUser;
        System.runAs(runningUser) {
            
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
            
            UserTestDataFactory.assignPermissionSetToUser(testUser.Id, 'Call_Logging_Access');
            
        }
        
        System.runAs(testUser) {
            Contact testContact = CustomerTestDataFactory.createContact();
            insert testContact;
            system.assert(testContact.Id != NULL);
            Case testCase = CaseTestDataFactory.createComplaintCase(testContact.Id);
            insert testCase;
            system.assert(testCase.Id != NULL);
            
            Test.startTest();
            Case currentCase = NewCustomerContactLExController.initializeAssignmentQuickActionComponent(testCase.Id);
            system.assert(currentCase.Id != null);
            system.assert(currentCase.jl_Transfer_case__c == true);
            system.assert(currentCase.jl_Case_RestrictedAddComment__c == '');
            
            Case relatedCase = NewCustomerContactLExController.updateCaseAssignment(JSON.serialize(currentCase));
            system.assert(relatedCase.Id != null);
            system.assert(!String.isEmpty(relatedCase.CaseNumber));
            Test.stopTest();
        }
    }
    

     @isTest static void testChatAndCaselink() {
        
        CustomSettingTestDataFactory.initialiseTeamCustomSettings(CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_QUEUE_NAME);

        User runningUser = UserTestDataFactory.getRunningUser();

        User testUser;
        Contact testContact;

        System.runAs(runningUser) {

            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;

            UserTestDataFactory.assignPermissionSetToUser(testUser.Id, 'Call_Logging_Access');
        
        }

        System.runAs(testUser) {
            Test.startTest();
            testContact = CustomerTestDataFactory.createContact();
            insert testContact;
            system.assert(testContact.Id != NULL);
            Case testCase = CaseTestDataFactory.createComplaintCase(testContact.Id);
            insert testCase;
            LiveChatVisitor lv = new LiveChatVisitor();
            insert lv;
            LiveChatTranscript lt = new LiveChatTranscript();
            lt.LiveChatVisitorId = lv.id;
            insert lt;
            NewCustomerContactLExController.linkChatWithCase(lt.id,testCase.id);
            Test.stopTest();
        }
     }
     @isTest static void testgetQueue(){
        Test.startTest();
        // String queue = NewCustomerContactLExController.getQueue();
        // system.assertNotEquals(null, queue);
        Test.stopTest();
    }
    
}