/******************************************************************************
* @author       Ranjan Samal
* @date         25/09/2017
* @description  Iterator Class used for Pagination

******************************************************************************/
@istest
private class JLCustomIterable_Test {
    testMethod private static void JLCustomIterableTest(){
        List<JLUserWrapper>usrWrappers=new List<JLUserWrapper>();
        Integer j=0;
        for(j=0;j<20;j++){
            
            //insert users
            User usr = UnitTestDataFactory.getSystemAdministrator('admin11' + j);
            insert usr;         
            boolean isSelected = True;
            usrWrappers.add(new JLUserWrapper(usr, isSelected)); 
        }
        JLCustomIterable newUsr= new JLCustomIterable(usrWrappers);
        Integer ps = newUsr.setPageSize;
        Boolean next = newUsr.hasNext();
        newUsr.next();
        newUsr.next();
        Boolean previous = newUsr.hasPrevious();
        newUsr.previous();
    }
}