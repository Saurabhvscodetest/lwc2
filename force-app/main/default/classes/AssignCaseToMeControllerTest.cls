@isTest
public class AssignCaseToMeControllerTest {
	
    @testSetup static void setup() {
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();        
        User runningUser = UserTestDataFactory.getRunningUser();        
        User contactCentreUser;        
        Contact con;
        Case cas;
        
        System.runAs(runningUser) {
            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
            con = CustomerTestDataFactory.createContact();
            insert con;               
        }
        System.runAs(contactCentreUser) {             
            cas = CaseTestDataFactory.createQueryCase(con.Id);
            insert cas;
        }
    }
    
    @isTest static void checkCaseWithQueueOrUserTestCaseQueue() {		
        List<Case> caseList = [ SELECT Id, OwnerId FROM Case WHERE RecordType.Name =: 'Query' LIMIT 1 ];
        Case cas = caseList[0];
        Test.startTest();
        	System.assert(!AssignCaseToMeController.checkCaseWithQueueOrUser(cas.Id));
        Test.stopTest();
        
    }
    
    @isTest static void checkCaseWithQueueOrUserTestCaseOwner() {
        List<Case> caseList = [ SELECT Id, OwnerId FROM Case WHERE RecordType.Name =: 'Query' LIMIT 1 ];
        Case cas = caseList[0];
        cas.OwnerId = UserInfo.getUserId();        
        Test.startTest();
        	update cas;
        	System.assert(AssignCaseToMeController.checkCaseWithQueueOrUser(cas.Id));
        Test.stopTest();
    }
    
    @isTest static void checkCaseWithQueueOrUserTestNoCase() {
        Test.startTest();
        	System.assert(!AssignCaseToMeController.checkCaseWithQueueOrUser(null));
        Test.stopTest();
    }
    
    @isTest static void assignCaseToMeTest() {
        List<Case> caseList = [ SELECT Id, OwnerId FROM Case WHERE RecordType.Name =: 'Query' LIMIT 1 ];
        Case cas = caseList[0];
        Test.startTest();
        	AssignCaseToMeController.assignCaseToMe(cas.Id);
        Test.stopTest();
        System.assertNotEquals(null, cas.OwnerId);
    }
}