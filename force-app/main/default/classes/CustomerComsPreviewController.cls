public with sharing class CustomerComsPreviewController {
	
	/*
	Name: 		CustomerComsPreviewController
	Author:		Shion Abdillah
	Date:		06/07/2015
	Date:	
	Desc: 		Controller for CustomerComsPreview which is a VF page used to 
				get all email communications related to a case and display in
				bubble speech format for easy. Used in 'Email Customer' screen 
				on a case.
	*/

	public string CaseId {get;set;}
	public boolean IsPrintView {get;set;}
	public boolean ContentExists {get;set;}
	public boolean ShowPrintIcon {get;set;}	
	public Case caseRec {get;set;}
	public List<EmailMessage> EmailActivityCollection {get;set;}
	public List<EmailDisplayWrapper> DisplayContentCollection {get;set;}

	 public CustomerComsPreviewController(ApexPages.StandardController stdController) 
	 {
	 	IsPrintView = false;	 	
        this.caseRec = (Case)stdController.getRecord();
        CaseId = caseRec.Id;
		LoadComsContent();		
		if(ApexPages.currentPage().getParameters().get('printview') == 'true')IsPrintView = true; 
     }

	public void LoadComsContent()
	{
		List<EmailMessage> tempEmailActivityCollection;
		system.debug('CaseId: ' + CaseId);		
		try
		{
			if(CaseId != null)
			{

				tempEmailActivityCollection = [select e.ToAddress, e.TextBody, e.SystemModstamp, e.Subject, e.Status, e.ReplyToEmailMessageId, e.ParentId, e.MessageDate, e.LastModifiedDate, e.LastModifiedById, e.IsDeleted, e.Incoming, e.Id, e.HtmlBody, e.Headers, e.HasAttachment, e.FromName, e.FromAddress, e.CreatedDate, e.CreatedById, e.CcAddress, e.BccAddress, e.ActivityId From EmailMessage e where e.ParentId = : CaseId AND Dummy_Email__c = false  ORDER BY CreatedDate DESC];					
				if( tempEmailActivityCollection.size() > 0 ) ContentExists = true; 
			
				System.debug('tempEmailActivityCollection: ' + tempEmailActivityCollection.size());
				List<string> ContentSplitCollection = new List<string>();
				EmailActivityCollection = new List<EmailMessage>();	
				string containsTest; 

				if(!tempEmailActivityCollection.isEmpty())
				{				
					for (Integer i = 0; i < tempEmailActivityCollection.size(); i++) 
					{
						
						containsTest = 	String.valueOf(tempEmailActivityCollection[i].FromAddress);	

						System.debug('LLJ - '+ containsTest);
						System.debug('LLJ - '+ tempEmailActivityCollection[i]);
						if(containsTest != null)
						{
							if(!containsTest.contains('no_reply')) 
							{
								EmailActivityCollection.add(tempEmailActivityCollection[i]);								
							}
						}
					}			
				}
							
				DisplayContentCollection = new List<EmailDisplayWrapper>();
				System.debug('EmailActivityCollection.size(): ' + EmailActivityCollection.size());				
				if(!EmailActivityCollection.isEmpty())
				{					
					for (Integer i = 0; i < EmailActivityCollection.size(); i++) 
					{
							
						EmailDisplayWrapper emailDisplayWrapper = new EmailDisplayWrapper();
						emailDisplayWrapper.Subject = EmailActivityCollection[i].Subject;
						emailDisplayWrapper.FromAddress = EmailActivityCollection[i].FromAddress;
						emailDisplayWrapper.ToAddress = EmailActivityCollection[i].ToAddress;
						emailDisplayWrapper.CCAddresses = EmailActivityCollection[i].CcAddress;
						emailDisplayWrapper.BccAddresses = EmailActivityCollection[i].BccAddress;
						emailDisplayWrapper.MessageDate = EmailActivityCollection[i].MessageDate;
						emailDisplayWrapper.Incoming = EmailActivityCollection[i].Incoming;
						string outputTextBody;
						string outputHTMLTextBody;												
						if(i == 0)
						{
							emailDisplayWrapper.TextBody = lineBreaks(EmailActivityCollection[i].TextBody);
						}
						else
						{
							if(!String.isBlank(EmailActivityCollection[i].TextBody))
							{
								
								integer wroteIndex = -1;
								integer originalMessageIndex = -1;
								integer hotmailIndex = -1;
								boolean IsOriginalMessageInContent = false;						
								boolean IsWroteInContent = false;
								boolean IsHotmailContent = false;
	
								string checkedBody =  lineBreaks(EmailActivityCollection[i].TextBody);
								System.debug('checkedBody:' + checkedBody);
	
								if(checkedBody.contains('<br/>Date:'))IsHotmailContent = true;																			
								if(EmailActivityCollection[i].TextBody.contains('wrote:'))IsWroteInContent = true;							
								if(EmailActivityCollection[i].TextBody.contains('--------------- Original Message ---------------'))IsOriginalMessageInContent = true;							
								System.debug('IsWroteInContent:' + IsWroteInContent);
								System.debug('IsOriginalMessageInContent:' + IsOriginalMessageInContent);
	
								if(IsHotmailContent)hotmailIndex = checkedBody.indexOf('<br/>Date:');
								if(IsWroteInContent)wroteIndex = EmailActivityCollection[i].TextBody.indexOf('wrote:');
								if(IsOriginalMessageInContent)originalMessageIndex = EmailActivityCollection[i].TextBody.indexOf('--------------- Original Message ---------------');
	
								System.debug('hotmailIndex:' + hotmailIndex);
								if(hotmailIndex != -1)
								{
	
									if(originalMessageIndex > hotmailIndex)
									{
										System.debug('originalMessageIndex inside it.');
										List<string> outputTextBodyCollection = checkedBody.split('<br/>Date:');	
										if(outputTextBodyCollection.size() > 1)
										{
											outputTextBody = outputTextBodyCollection[0];
											EmailActivityCollection[i].TextBody = lineBreaks(outputTextBody);									
										}	
									}
									else
									{
										System.debug('originalMessageIndex outside it.');
										List<string> outputTextBodyCollection = checkedBody.split('--------------- Original Message ---------------');	
										if(outputTextBodyCollection.size() > 1)
										{
											outputTextBody = outputTextBodyCollection[0];
											EmailActivityCollection[i].TextBody = lineBreaks(outputTextBody);									
										}
	
	
									}
									System.debug('EmailActivityCollection[i].TextBody:' + EmailActivityCollection[i].TextBody);
								}
								else
								{
									if((wroteIndex == -1 && originalMessageIndex != -1) || (wroteIndex < originalMessageIndex))
									{	
										System.debug('Not Stripping out wroteanddate.');
										List<string> outputTextBodyCollection = EmailActivityCollection[i].TextBody.split('--------------- Original Message ---------------');	
										if(outputTextBodyCollection.size() > 1)
										{
											outputTextBody = outputTextBodyCollection[0];										
											EmailActivityCollection[i].TextBody = outputTextBody;
										}	
									}
									else if((wroteIndex != -1 && originalMessageIndex == -1) || (originalMessageIndex > wroteIndex))
									{
										System.debug('Stripping out wroteanddate.');
										string tempVal = lineBreaks(EmailActivityCollection[i].TextBody.substring(0,EmailActivityCollection[i].TextBody.indexOf('wrote:')));								
										System.debug('tempVal:' + tempVal);									
										tempVal =  tempVal.substring(0,tempVal.LastIndexOf('<br/>'));	
										tempVal =  tempVal.substring(0,tempVal.LastIndexOf('On'));				
										EmailActivityCollection[i].TextBody = tempVal;								
									}
								}	
	
							}
	
						}
						emailDisplayWrapper.TextBody = lineBreaks(EmailActivityCollection[i].TextBody);				
						emailDisplayWrapper.TextBody = emailDisplayWrapper.TextBody.replace('(br/)', '<br/>');
						DisplayContentCollection.add(emailDisplayWrapper);										
					}				
				}
				if(DisplayContentCollection.size() == 0){ShowPrintIcon = false;ContentExists = false;}else{ShowPrintIcon = true;ContentExists = true;}			
			}						
		}
		catch(Exception ex)
		{
			LoadDefaultContentOnError(tempEmailActivityCollection);
		}	 						
	}
	
	public void LoadDefaultContentOnError(List<EmailMessage> messages)
	{
		List<EmailMessage> tempEmailActivityCollection = messages;
		try
		{
			if(CaseId != null)
			{				
				
				EmailActivityCollection = new List<EmailMessage>();	
				string containsTest; 
				if(!tempEmailActivityCollection.isEmpty())
				{				
					for (Integer i = 0; i < tempEmailActivityCollection.size(); i++) 
					{
						
						containsTest = 	String.valueOf(tempEmailActivityCollection[i].FromAddress);									
						if(containsTest != null)
						{
							if(!containsTest.contains('no_reply')) 
							{
								EmailActivityCollection.add(tempEmailActivityCollection[i]);								
							}
						}
					}								
				}							
				DisplayContentCollection = new List<EmailDisplayWrapper>();				
				if(!EmailActivityCollection.isEmpty())
				{					
					for (Integer i = 0; i < EmailActivityCollection.size(); i++) 
					{						
						string outputTextBody;
						string outputHTMLTextBody;		
						EmailDisplayWrapper emailDisplayWrapper = new EmailDisplayWrapper();
						emailDisplayWrapper.Subject = EmailActivityCollection[i].Subject;
						emailDisplayWrapper.FromAddress = EmailActivityCollection[i].FromAddress;
						emailDisplayWrapper.ToAddress = EmailActivityCollection[i].ToAddress;
						emailDisplayWrapper.CCAddresses = EmailActivityCollection[i].CcAddress;
						emailDisplayWrapper.BccAddresses = EmailActivityCollection[i].BccAddress;
						emailDisplayWrapper.MessageDate = EmailActivityCollection[i].MessageDate;
						emailDisplayWrapper.Incoming = EmailActivityCollection[i].Incoming;																					
						emailDisplayWrapper.TextBody = lineBreaks(EmailActivityCollection[i].TextBody);				
						emailDisplayWrapper.TextBody = emailDisplayWrapper.TextBody.replace('(br/)', '<br/>');						
						DisplayContentCollection.add(emailDisplayWrapper);										
					}				
				}
				if(DisplayContentCollection.size() == 0){ShowPrintIcon = false;ContentExists = false;}else{ShowPrintIcon = true;ContentExists = true;}			
			}						
		}
		catch(Exception ex)
		{
			System.debug(ex);
		}	
		
	}
	
	
	@TestVisible
	private string lineBreaks(string inText)
	{
	   if (inText == null)
	       return '';
	   else
	       return inText.replaceAll('<','(').replaceAll('>',')').replaceAll('\n','<br/>');	       
	}

/*
	private Integer ReturnSplitPosition(string intText)
	{
		
	}
*/
	public class EmailDisplayWrapper
	{
		public string FromAddress{get;set;}
		public string ToAddress{get;set;}
		public string CCAddresses{get;set;}
		public string BccAddresses{get;set;}
		public string Subject{get;set;}
		public string TextBody{get;set;}
		public string HTMLTextBody{get;set;}
		public DateTime MessageDate{get;set;}
		public String MessageDateDisplay{
			get
			{
				if (MessageDate == null) {
				return null;
				} else {
				return MessageDate.format('yyyy-MM-dd HH:mm:ss');
				}				
			}
			set;
		}		
		public Boolean Incoming {get;set;}
	}

}