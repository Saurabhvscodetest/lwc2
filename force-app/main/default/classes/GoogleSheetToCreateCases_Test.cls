@isTest
public class GoogleSheetToCreateCases_Test {
    //Test Method 1
    static testMethod void GoogleSheetToCreateCases_Test_MTD1() {
        //Custom Setting IFC Queries Sheet
        IFC_CSVFieldMapping__c setting1 = new IFC_CSVFieldMapping__c();
        setting1.Name = 'IFC Department';
        setting1.Feild_Name__c = 'IFC_Department__c';
        insert setting1;
        
        IFC_CSVFieldMapping__c setting2 = new IFC_CSVFieldMapping__c();
        setting2.Name = 'Branch Number'; 
        setting2.Feild_Name__c = 'Branch_Number__c';
        insert setting2;
        
        IFC_CSVFieldMapping__c setting3 = new IFC_CSVFieldMapping__c();
        setting3.Name = 'Financial Reporting Period';
        setting3.Feild_Name__c = 'Financial_Reporting_Period__c';
        insert setting3;
        
        IFC_CSVFieldMapping__c setting4 = new IFC_CSVFieldMapping__c();
        setting4.Name = 'Type of Entry Outstanding';
        setting4.Feild_Name__c = 'Type_of_Entry_Outstanding__c';
        insert setting4;
        
        IFC_CSVFieldMapping__c setting5 = new IFC_CSVFieldMapping__c();
        setting5.Name = 'Date Processed';
        setting5.Feild_Name__c = 'Date_Processed__c';
        setting5.Date__c = true;
        insert setting5;
        
        IFC_CSVFieldMapping__c setting6 = new IFC_CSVFieldMapping__c();
        setting6.Name = 'Error Description';
        setting6.Feild_Name__c = 'Error_Description__c';
        insert setting6;
        
        IFC_CSVFieldMapping__c setting7 = new IFC_CSVFieldMapping__c();
        setting7.Name = 'Amount';
        setting7.Feild_Name__c = 'Amount__c';
        setting7.Decimal__c = true;
        insert setting7;
        
        IFC_ErrorEmail__c setting8 = new IFC_ErrorEmail__c();
        setting8.Name = 'Error Email Id';
        setting8.Value__c = 'sd@sd.com';
        insert setting8;
        
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.subject = 'IFC Queries Sheet';
        email.plainTextBody = 'Please see attached CSV';
        env.fromAddress = 'Test@Test.com';
        
        //Inserting Attachment
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        string csvString = 'IFC Department,Branch Number,Financial Reporting Period,Type of Entry Outstanding,Date Processed,Till number - Transaction or Provider,Customer Name,Loan Ref,Post Code / Ref,Cash Management Reference,Amount,Error Description Electronics,\n, 1,PD08-20,Branch Loan,04/09/2019,B020995855,Churchill,1234,E6 1AR,Furniture,675,Missing sale through the till Electrical,\n, 2,PD08-20,CC Loan,21/09/2019,B310890368,Clark-Wilson,1234,SE16 7GA,Furniture,3052.66,Missing sale through the till';
        attachment.body = blob.valueOf(csvString);
        attachment.fileName = 'test.csv';
        attachment.mimeTypeSubType = 'text/csv';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
            GoogleSheetToCreateCases EmailServiceObj = new GoogleSheetToCreateCases();
        EmailServiceObj.handleInboundEmail(email, env ); 
    }
    
    //Test Method 2
    static testMethod void GoogleSheetToCreateCases_Test_MTD2() {
        //Custom Setting 7 Day Report
        Report7Day_CSVFieldMapping__c setting1 = new Report7Day_CSVFieldMapping__c();
        setting1.Name = 'Customer Name';
        setting1.Feild_Name__c = 'Last_Name_Web__c';
        insert setting1;
        
        Report7Day_CSVFieldMapping__c setting2 = new Report7Day_CSVFieldMapping__c();
        setting2.Name = 'Delay Reason';
        setting2.Feild_Name__c = 'Delay_Reason__c';
        insert setting2;
        
        Report7Day_CSVFieldMapping__c setting3 = new Report7Day_CSVFieldMapping__c();
        setting3.Name = 'Fabric Description';
        setting3.Feild_Name__c = 'Fabric_Description__c';
        insert setting3;
        
        Report7Day_CSVFieldMapping__c setting4 = new Report7Day_CSVFieldMapping__c();
        setting4.Name = 'Fabric ETA';
        setting4.Feild_Name__c = 'Fabric_ETA__c';
        insert setting4;
        
        Report7Day_CSVFieldMapping__c setting5 = new Report7Day_CSVFieldMapping__c();
        setting5.Name = 'Fabric Number';
        setting5.Feild_Name__c = 'Fabric_Number__c';
        setting5.Decimal__c = true;
        insert setting5;
        
        Report7Day_CSVFieldMapping__c setting6 = new Report7Day_CSVFieldMapping__c();
        setting6.Name = 'Order Date';
        setting6.Feild_Name__c = 'Order_Date__c';
        setting6.Date__c = true;
        insert setting6;
        
        Report7Day_CSVFieldMapping__c setting7 = new Report7Day_CSVFieldMapping__c();
        setting7.Name = 'Order Number';
        setting7.Feild_Name__c = 'jl_OrderManagementNumber__c';
        insert setting7;
        
        IFC_ErrorEmail__c setting8 = new IFC_ErrorEmail__c();
        setting8.Name = 'Error Email Id';
        setting8.Value__c = 'sd@sd.com';
        insert setting8;
        
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.subject = '7 Day Report';
        email.plainTextBody = 'Please see attached CSV';
        env.fromAddress = 'Test@Test.com';
        
        //Inserting Attachment
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        string csvString = 'Delay Reason,Customer Name, Order Number,Our Order No.,Order Date,Product Description,Fabric Number,Fabric Description,Qty Required,Fabric ETA,\n Awaiting Fabric,Mr Green,135010228,1037981,5/09/2019,curt pencil std 2.0w L,68822709,Luna paprika,9.4,15/09/2019';
        attachment.body = blob.valueOf(csvString);
        attachment.fileName = 'test.csv';
        attachment.mimeTypeSubType = 'text/csv';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
            GoogleSheetToCreateCases EmailServiceObj = new GoogleSheetToCreateCases();
        EmailServiceObj.handleInboundEmail(email, env ); 
    }
    
    //Test Method 3
    static testMethod void GoogleSheetToCreateCases_Test_MTD3() {
        //Custom Setting All Directorate
        AllDirectorate_CSVFieldMapping__c setting1 = new AllDirectorate_CSVFieldMapping__c();
        setting1.Name = 'Customer Name';
        setting1.Feild_Name__c = 'Last_Name_Web__c';
        insert setting1;
        
        AllDirectorate_CSVFieldMapping__c setting2 = new AllDirectorate_CSVFieldMapping__c();
        setting2.Name = 'Disti Ref';
        setting2.Feild_Name__c = 'Disti_Ref__c';
        insert setting2;
        
        AllDirectorate_CSVFieldMapping__c setting3 = new AllDirectorate_CSVFieldMapping__c();
        setting3.Name = 'Product Description';
        setting3.Feild_Name__c = 'Product_Description__c';
        insert setting3;
        
        AllDirectorate_CSVFieldMapping__c setting4 = new AllDirectorate_CSVFieldMapping__c();
        setting4.Name = 'Supplier Comments';
        setting4.Feild_Name__c = 'Supplier_Comments__c';
        insert setting4;
        
        AllDirectorate_CSVFieldMapping__c setting5 = new AllDirectorate_CSVFieldMapping__c();
        setting5.Name = 'Supplier Name';
        setting5.Feild_Name__c = 'Supplier_Name__c';
        insert setting5;
        
        IFC_ErrorEmail__c setting6 = new IFC_ErrorEmail__c();
        setting6.Name = 'Error Email Id';
        setting6.Value__c = 'sd@sd.com';
        insert setting6;
        
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.subject = 'All Directorate Exceptions';
        email.plainTextBody = 'Please see attached CSV';
        env.fromAddress = 'Test@Test.com';
        
        //Inserting Attachment
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        string csvString = 'Supplier Name,Customer Name,Disti Ref,Product Description,Supplier Comments,\n Jo Malone,Not On Email,JLP402480-1911,Not On Email,Please may you kindly arrange for the below orders to be refunded? These products are currently out of stock and therefore the orders have been cancelled our side.';
        attachment.body = blob.valueOf(csvString);
        attachment.fileName = 'test.csv';
        attachment.mimeTypeSubType = 'text/csv';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
            GoogleSheetToCreateCases EmailServiceObj = new GoogleSheetToCreateCases();
        EmailServiceObj.handleInboundEmail(email, env ); 
    }
    
    //Test Method 4
    static testMethod void GoogleSheetToCreateCases_Test_MTD4() {
        //Custom Setting Back Orders
        BackOrders_CSVFieldMapping__c setting1 = new BackOrders_CSVFieldMapping__c();
        setting1.Name = 'Branch #';
        setting1.Feild_Name__c = 'Branch_Number__c';
        insert setting1;
        
        BackOrders_CSVFieldMapping__c setting2 = new BackOrders_CSVFieldMapping__c();
        setting2.Name = 'Buying Dissection';
        setting2.Feild_Name__c = 'Buying_Dissection__c';
        insert setting2;
        
        BackOrders_CSVFieldMapping__c setting3 = new BackOrders_CSVFieldMapping__c();
        setting3.Name = 'Customer Name';
        setting3.Feild_Name__c = 'Last_Name_Web__c';
        insert setting3;
        
        BackOrders_CSVFieldMapping__c setting4 = new BackOrders_CSVFieldMapping__c();
        setting4.Name = 'Customer Ordered Date';
        setting4.Feild_Name__c = 'Customer_Ordered_Date__c';
        insert setting4;
        
        BackOrders_CSVFieldMapping__c setting5 = new BackOrders_CSVFieldMapping__c();
        setting5.Name = 'EDI/Disti';
        setting5.Feild_Name__c = 'Disti_Ref__c';
        insert setting5;
        
        BackOrders_CSVFieldMapping__c setting6 = new BackOrders_CSVFieldMapping__c();
        setting6.Name = 'Expected Delivery Date to CDH Customer';
        setting6.Feild_Name__c = 'Expected_Delivery_Date_to_CDH_Customer__c';
        insert setting6;
        
        BackOrders_CSVFieldMapping__c setting7 = new BackOrders_CSVFieldMapping__c();
        setting7.Name = 'Supplier';
        setting7.Feild_Name__c = 'Supplier_Name__c';
        insert setting7;
        
        IFC_ErrorEmail__c setting8 = new IFC_ErrorEmail__c();
        setting8.Name = 'Error Email Id';
        setting8.Value__c = 'sd@sd.com';
        insert setting8;
        
        //Custom Setting Suppliers
        BackOrders_SuppliersName__c setting9 = new BackOrders_SuppliersName__c();
        setting9.Name = 'Supplier Name';
        setting9.Value__c = 'Herbert Parkinson,Curtainwise,Curtainique';
        insert setting9;
        
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.subject = 'John Lewis Order Tracking Reports';
        email.plainTextBody = 'Please see attached CSV';
        env.fromAddress = 'Test@Test.com';
        
        //Inserting Attachment
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        string csvString = 'Branch #,Buying Dissection,Customer Name,Customer Ordered Date,EDI/Disti,Expected Delivery Date to CDH/Customer,Supplier,\n 2,BEDROOM FURN,Test,04-04-2020,1QQNVA95EHO/1/032,25-03-2040,Herbert Parkinson';
        attachment.body = blob.valueOf(csvString);
        attachment.fileName = 'BackOrders.csv';
        attachment.mimeTypeSubType = 'text/csv';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
            GoogleSheetToCreateCases EmailServiceObj = new GoogleSheetToCreateCases();
        EmailServiceObj.handleInboundEmail(email, env ); 
    }
    
    //Test Method 5
    static testMethod void GoogleSheetToCreateCases_Test_MTD5() {
        
        //Custom Setting MP3 Exceptions/Queries
        MP3Exceptions_FieldMapping__c setting1 = new MP3Exceptions_FieldMapping__c();
        setting1.Name = 'Exception or Query';
        setting1.Feild_Name__c = 'RecordTypeId';
        insert setting1;
        
        MP3Exceptions_FieldMapping__c setting2 = new MP3Exceptions_FieldMapping__c();
        setting2.Name = 'MP3 Partner';
        setting2.Feild_Name__c = 'Partner_Name__c';
        insert setting2;
        
        MP3Exceptions_FieldMapping__c setting3 = new MP3Exceptions_FieldMapping__c();
        setting3.Name = 'SKU';
        setting3.Feild_Name__c = 'jl_Stock_Number__c';
        insert setting3;
        
        MP3Exceptions_FieldMapping__c setting4 = new MP3Exceptions_FieldMapping__c();
        setting4.Name = 'Stock Status';
        setting4.Feild_Name__c = 'Stock_Status__c';
        insert setting4;
        
        MP3Exceptions_FieldMapping__c setting5 = new MP3Exceptions_FieldMapping__c();
        setting5.Name = 'Customer Surname';
        setting5.Feild_Name__c = 'Last_Name_Web__c';
        insert setting5;
        
        MP3Exceptions_FieldMapping__c setting6 = new MP3Exceptions_FieldMapping__c();
        setting6.Name = 'Comments';
        setting6.Feild_Name__c = 'Supplier_Comments__c';
        insert setting6;
        
        IFC_ErrorEmail__c setting7 = new IFC_ErrorEmail__c();
        setting7.Name = 'Error Email Id';
        setting7.Value__c = 'sd@sd.com';
        insert setting7;
        
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.subject = 'MP3 Exceptions/Queries';
        email.plainTextBody = 'Hi, Here is the template the MP3 team are proposing to use \n '+
            +' Exception or Query Exception\n '+
            +'Date 27/08/2020\n '+
            +'MP3 Partner Fay Hawkins\n '+
            +'Order Number 254751546 \n '+
            +'Customer Surname Watson\n '+
            +'Sku 50424752 \n '+
            +'Stock Status Out of Stock \n '+
            +'Comments Remaining stock all faulty, awaiting a delivery date from BO \n' ;
        env.fromAddress = 'Test@Test.com';
        GoogleSheetToCreateCases EmailServiceObj = new GoogleSheetToCreateCases();
        EmailServiceObj.handleInboundEmail(email, env ); 
    }
    
    //Test Method 6
    static testMethod void GoogleSheetToCreateCases_Test_MTD6() {
        //Custom Setting IFC Queries Sheet
        ISB_CSVFieldMapping__c setting1 = new ISB_CSVFieldMapping__c();
        setting1.Name = 'Address Line 1';
        setting1.Feild_Name__c = 'Address_Line_1__c';
        insert setting1;
        
        ISB_CSVFieldMapping__c setting2 = new ISB_CSVFieldMapping__c();
        setting2.Name = 'Booked By'; 
        setting2.Feild_Name__c = 'Booked_By__c';
        insert setting2;
        
        ISB_CSVFieldMapping__c setting3 = new ISB_CSVFieldMapping__c();
        setting3.Name = 'Booking Compliance';
        setting3.Feild_Name__c = 'Booking_Compliance__c';
        insert setting3;
        
        ISB_CSVFieldMapping__c setting4 = new ISB_CSVFieldMapping__c();
        setting4.Name = 'Branch Short Name';
        setting4.Feild_Name__c = 'Branch_Short_Name__c';
        insert setting4;
        
        ISB_CSVFieldMapping__c setting5 = new ISB_CSVFieldMapping__c();
        setting5.Name = 'Customer Delivery Number';
        setting5.Feild_Name__c = 'jl_DeliveryNumber__c';
        setting5.Date__c = true;
        insert setting5;
        
        ISB_CSVFieldMapping__c setting6 = new ISB_CSVFieldMapping__c();
        setting6.Name = 'DES Name';
        setting6.Feild_Name__c = 'DES_Name__c';
        insert setting6;
        
        ISB_CSVFieldMapping__c setting7 = new ISB_CSVFieldMapping__c();
        setting7.Name = 'JL Del CenShort Name';
        setting7.Feild_Name__c = 'CDH_Short_Name__c';
        setting7.Decimal__c = true;
        insert setting7;
		
		ISB_CSVFieldMapping__c setting8 = new ISB_CSVFieldMapping__c();
        setting8.Name = '	Product Code';
        setting8.Feild_Name__c = 'Product_Code__c';
        setting8.Decimal__c = true;
        insert setting8;
		
		ISB_CSVFieldMapping__c setting9 = new ISB_CSVFieldMapping__c();
        setting9.Name = 'Product Type';
        setting9.Feild_Name__c = 'Product_Type__c';
        setting9.Decimal__c = true;
        insert setting9;
		
		
        
        IFC_ErrorEmail__c setting10 = new IFC_ErrorEmail__c();
        setting10.Name = 'Error Email Id';
        setting10.Value__c = 'sd@sd.com';
        insert setting10;
        
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.subject = 'Incorrect Service Booked';
        email.plainTextBody = 'Please see attached CSV';
        env.fromAddress = 'Test@Test.com';
        
        //Inserting Attachment
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        string csvString = 'Name,Address Line 1,Booked By,Booking Compliance,Branch Short Name,Customer Delivery Number,DES Name,JL Del CenShort Name,Product Code,Product Type,\n, 1,PD08-20,Branch Loan,04/09/2019,B020995855,Churchill,1234,E6 1AR,Furniture,675,Missing sale through the till Electrical,\n, 2,PD08-20,CC Loan,21/09/2019,B310890368,Clark-Wilson,1234,SE16 7GA,Furniture,3052.66,Missing sale through the till';
        attachment.body = blob.valueOf(csvString);
        attachment.fileName = 'test.csv';
        attachment.mimeTypeSubType = 'text/csv';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
            GoogleSheetToCreateCases EmailServiceObj = new GoogleSheetToCreateCases();
        EmailServiceObj.handleInboundEmail(email, env ); 
    }
    
    //Test Method 6
    static testMethod void GSheetToCreateCases_Test_MTDSupplier() {
        //Custom Setting IFC Queries Sheet
        LM6_RPA_CSVFieldMapping__c setting1 = new LM6_RPA_CSVFieldMapping__c();
        setting1.Name = 'Branch Name';
        setting1.Feild_Name__c = 'jl_Branch_master__c';
        insert setting1;
        
        LM6_RPA_CSVFieldMapping__c setting2 = new LM6_RPA_CSVFieldMapping__c();
        setting2.Name = 'EDI/Disti'; 
        setting2.Feild_Name__c = 'Disti_Ref__c';
        insert setting2;
        
        LM6_RPA_CSVFieldMapping__c setting3 = new LM6_RPA_CSVFieldMapping__c();
        setting3.Name = 'Order #';
        setting3.Feild_Name__c = 'jl_OrderManagementNumber__c';
        insert setting3;
        
        LM6_RPA_CSVFieldMapping__c setting4 = new LM6_RPA_CSVFieldMapping__c();
        setting4.Name = 'Supplier';
        setting4.Feild_Name__c = 'Supplier_Name__c';
        insert setting4;		
        
        IFC_ErrorEmail__c setting10 = new IFC_ErrorEmail__c();
        setting10.Name = 'Error Email Id';
        setting10.Value__c = 'sd@sd.com';
        insert setting10;
        
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.subject = 'RPA Supplier Direct Cancellations Summary';
        email.plainTextBody = 'Please see attached CSV';
        env.fromAddress = 'Test@Test.com';
        
        //Inserting Attachment
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        string csvString = 'Name,Branch Name,Booked By,EDI/Disti,Order #,Supplier';
        attachment.body = blob.valueOf(csvString);
        attachment.fileName = 'test.csv';
        attachment.mimeTypeSubType = 'text/csv';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
            GoogleSheetToCreateCases EmailServiceObj = new GoogleSheetToCreateCases();
        EmailServiceObj.handleInboundEmail(email, env ); 
    }
    
    //Test Method 6
    static testMethod void GSheetToCreateCases_Test_RPAChargeNotifications() {
        //Custom Setting IFC Queries Sheet
        RPA_Charge_Notifications_CSVFieldMapping__c setting1 = new RPA_Charge_Notifications_CSVFieldMapping__c();
        setting1.Name = 'Branch';
        setting1.Feild_Name__c = 'jl_Branch_master__c';
        insert setting1;
        
        RPA_Charge_Notifications_CSVFieldMapping__c setting2 = new RPA_Charge_Notifications_CSVFieldMapping__c();
        setting2.Name = 'Customer'; 
        setting2.Feild_Name__c = 'SuppliedName';
        insert setting2;
        
        RPA_Charge_Notifications_CSVFieldMapping__c setting3 = new RPA_Charge_Notifications_CSVFieldMapping__c();
        setting3.Name = 'Mobile Number';
        setting3.Feild_Name__c = 'SuppliedPhone';
        insert setting3;
        
        RPA_Charge_Notifications_CSVFieldMapping__c setting4 = new RPA_Charge_Notifications_CSVFieldMapping__c();
        setting4.Name = 'Num';
        setting4.Feild_Name__c = 'jl_CRNumber__c';
        insert setting4;		
        
        
        
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.subject = 'RPA Charge Notifications Process Report';
        email.plainTextBody = 'Please see attached CSV';
        env.fromAddress = 'Test@Test.com';
        
        //Inserting Attachment
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        string csvString = 'Branch,Customer,Mobile Number,Num';
        attachment.body = blob.valueOf(csvString);
        attachment.fileName = 'test.csv';
        attachment.mimeTypeSubType = 'text/csv';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
            GoogleSheetToCreateCases EmailServiceObj = new GoogleSheetToCreateCases();
        EmailServiceObj.handleInboundEmail(email, env ); 
    }
    
    //TEST METHOD 7
    static testMethod void GSheetToCreateCases_Test_NoCSV() {
        
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.subject = 'RPA Charge Notifications Process Report';
        env.fromAddress = 'Test@Test.com';
         GoogleSheetToCreateCases EmailServiceObj = new GoogleSheetToCreateCases();
        EmailServiceObj.handleInboundEmail(email, env ); 
    
    }
    
     //TEST METHOD 8
     static testMethod void GSheetToCreateCases_Test_NoEmailSub() {
        //Custom Setting IFC Queries Sheet
        RPA_Charge_Notifications_CSVFieldMapping__c setting1 = new RPA_Charge_Notifications_CSVFieldMapping__c();
        setting1.Name = 'Branch';
        setting1.Feild_Name__c = 'jl_Branch_master__c';
        insert setting1;
        
        RPA_Charge_Notifications_CSVFieldMapping__c setting2 = new RPA_Charge_Notifications_CSVFieldMapping__c();
        setting2.Name = 'Customer'; 
        setting2.Feild_Name__c = 'SuppliedName';
        insert setting2;
        
        RPA_Charge_Notifications_CSVFieldMapping__c setting3 = new RPA_Charge_Notifications_CSVFieldMapping__c();
        setting3.Name = 'Mobile Number';
        setting3.Feild_Name__c = 'SuppliedPhone';
        insert setting3;
        
        RPA_Charge_Notifications_CSVFieldMapping__c setting4 = new RPA_Charge_Notifications_CSVFieldMapping__c();
        setting4.Name = 'Num';
        setting4.Feild_Name__c = 'jl_CRNumber__c';
        insert setting4;		
        
        
        
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        email.plainTextBody = 'Please see attached CSV';
        env.fromAddress = 'Test@Test.com';
        
        //Inserting Attachment
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        string csvString = 'Branch,Customer,Mobile Number,Num';
        attachment.body = blob.valueOf(csvString);
        attachment.fileName = 'test.csv';
        attachment.mimeTypeSubType = 'text/csv';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
            GoogleSheetToCreateCases EmailServiceObj = new GoogleSheetToCreateCases();
        EmailServiceObj.handleInboundEmail(email, env ); 
    }
    
}