public with sharing class ChangeOwnerController 
{
	public String SearchText {get;set;}
	public String RecordId {get;set;}
	public Case caseRec  {get;set;}
	public String AssigneeId {get;set;}	
	public String QueueName {get;set;}	
	public Boolean displayQueueResults {get;set;}
	public Boolean displayAdditionalComment {get;set;}
	public Boolean displaySearchPanel {get;set;}
	public Set <String> excludedQueues{get;set;}	
	public List<QueueSobject> OwnerRecords {get;set;}
	public String SearchLabel {get;set;}


	public ChangeOwnerController()
	{			
		displayQueueResults = false;
		excludedQueues = QueueOwnerFilterHelper.getChangeOwnerQueueFilter();
		
		if(ApexPages.currentPage().getParameters().get('Id') != null)
		{
			RecordId = ApexPages.currentPage().getParameters().get('Id');
			List<Case> cases = [select Id, ContactId, OwnerId, jl_Case_RestrictedAddComment__c, CaseNumber  From Case where Id = : RecordId];
	
			if(! cases.isEmpty()){
				caseRec = cases.get(0);
				ShowSearchBox();
			}
			else{
				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Case ID provided not found'));
				HideAllPanels();
			}	
		}	
		else{
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Case ID Not provided in the URL'));
			HideAllPanels();
		}	
	}
				
	public void Search()
	{
		OwnerRecords = new List<QueueSobject>();

		if(!string.isBlank(SearchText)){	
		
			List<QueueSobject> tempOwnerRecords = [Select Id, Queue.Name from QueueSobject where Queue.Name LIKE : '%' + SearchText + '%' and Queue.Name NOT in :excludedQueues order by Queue.Name];
			
			for(QueueSobject queueObj : tempOwnerRecords){
				if((queueObj.Queue.Name.length() > CommonStaticUtils.MAX_SEARCH_TEXT_LENGTH)) {
					system.debug('LONG NAME : ' + queueObj.Queue.Name);
	            	boolean queueNameFound = false;
	            	for(String qo:excludedQueues) {
	            		if(queueObj.Queue.Name.startswith(qo)) {
	            			queueNameFound = true;
		                	break;
	            		}
	            	}
	            	if(!queueNameFound){
	            		system.debug('queueNameFound : ' + queueNameFound +  ' -Queue Name - ' + queueObj.Queue.Name);
						OwnerRecords.add(queueObj);
					}
				} else {
					OwnerRecords.add(queueObj);
				}
			}		
	
			if( !OwnerRecords.isEmpty()){
				ShowSearchResults();
			}
			else{
				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'No results found.'));
				ShowSearchBox();
			}
		}
		else{
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'Please enter a search term.'));
			ShowSearchBox();
		}		
	}	
			
	public void AssignRec()
	{
		ShowAdditionalComments();
		SearchLabel = 'Please enter a comment when assigning case ' + caseRec.caseNumber + ' to queue ' + QueueName;

		caseRec.OwnerId = AssigneeId;
	}
	
	public PageReference Save()
	{	
		try{
			if( String.isBlank(caseRec.jl_Case_RestrictedAddComment__c)){
				ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You must add a comment when reassigning a case.'));
				return null;
			}
			else{
				update caseRec;				
				return ReturnToConsole();	
			}	
		}
		catch(DmlException e) 
		{
			ApexPages.addMessages(e);
			return null;
		}
		catch(Exception e) 
		{			
			ApexPages.addMessages(e);
			return null;
		}	
	}	

	public void ShowSearchBox(){
		displayQueueResults = false;
		displayAdditionalComment = false;
		displaySearchPanel = true;
	}

	public void ShowSearchResults(){
		displayQueueResults = true;
		displayAdditionalComment = false;
		displaySearchPanel = true;	
	}

	public void ShowAdditionalComments(){
		displayQueueResults = false;
		displayAdditionalComment = true;
		displaySearchPanel = true;	
	}

	public void HideAllPanels(){
		displayQueueResults = false;
		displayAdditionalComment = false;
		displaySearchPanel = false;	
	}


	public PageReference ReturnToConsole(){

		ID ContactID = (caseRec == null)? null: caseRec.ContactId;

		PageReference pageRef = Page.ServiceConsoleRedirect;
		pageRef.getParameters().put('id',  RecordId);
		pageRef.getParameters().put('IsContact',  (ContactId != null) ? 'true' : 'false');
		pageRef.getParameters().put('isdtp',  'vw');

		return pageRef;	
	}
}