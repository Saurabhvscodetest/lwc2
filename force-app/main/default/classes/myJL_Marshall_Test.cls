/**
 *  001		23/04/15		NTJ		MJL-1823 - Class that can marshall objects across @future boundaries
*/
@isTest
private class myJL_Marshall_Test {

    static testMethod void myUnitTest() {
		SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
		
		String s = myJL_Marshall.prepare(request);
		system.assertNotEquals(null, s);
		system.assertNotEquals('', s);
		
		SFDCMyJLCustomerTypes.CustomerRequestType request2 = myJL_Marshall.unprepareCustomerRequestType(s);
		system.assertNotEquals(null, request2);
		
		// now compare original with new
		system.assertEquals(request.RequestHeader.MessageId, request2.RequestHeader.MessageId);
		
    }
}