/**
 *  @author: Andrey Gavrikov (westbrook)
 *  @date: 2015-01-27 11:26:04 
 *  @description:
 *      Test methods for CustomerManagement
 *  
 *  001     27/02/15    NTJ     MJL-1598 - Attach Contact Profile to the best Contact - add some tests
 *  002		23/03/15	NTJ		MJL-1692 - Add a matching key for <First><Last><Town><PostCode>
 *	003		23/03/15	NTJ		MJL-1693 - Copy fields from Contact Profile into created Contacts
 *  004     27/06/16    NA                  Decommissioning contact profile 
 */
@isTest
public class CustomerManagementTest  {

    class TestCustomerWrapper extends CustomerManagement.CustomerWrapper {

        private Contact contact;
        final Map<String, Object> valueMap;

        public TestCustomerWrapper(final Map<String, Object> valueMap) {
            this.valueMap = valueMap;
        }
        public override Contact getContact() {
            return contact;
        }
        public override void setContact(final Contact contact) {
            this.contact = contact;
        }

		public override void setMatchMethod(final String matchMethod) {
			// implement if necessary
		}

        public override String getEmailAddress() {
            return (String)getValue('Email');
        }

        public override String getFirstName() {
            return (String)getValue('FirstName');
        }
        
        public override String getLastName() {
            return (String)getValue('LastName');
        }
         
        public override String getAddressLine1() {
            return (String)getValue('MailingStreet');
        }

        public override String getPostalCode() {
            return (String)getValue('MailingPostalCode');
        }

        public override String getCity() {
        	system.debug('city: '+(String)getValue('MailingCity'));
            return (String)getValue('MailingCity');
        }

        public override String getCustomerId() {
            return (String)this.valueMap.get('TEST_ShopperId');
        }

		// MJL-1693 Start
		public override Date getBirthDate() {
            return (Date)getValue('Birthdate');
		}

		public override String getInitials() {
            return (String)getValue('jl_Middle_Name_s__c');
		}

		public override String getCountry() {
            return (String)getValue('MailingCountry');
		} 

		public override String getState() {
            return (String)getValue('MailingState');
		}

		public override String getSalutation() {
            return (String)getValue('Salutation');
		}

		public override String getPhone() {
            return (String)getValue('HomePhone');
		}
			

		// MJL-1693 End
        private Object getValue(final String key) {
            if (null == this.contact) {
                return this.valueMap.get(key);
            }
            return this.contact.get(key);
        }
    }

    private static List<Contact> prepareExistingContacts() {
    	id orderrecordtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Ordering Customer').getRecordTypeId();
        final List<Contact> contacts = new List<Contact>{
            //contact matched by key 1
            new Contact(FirstName = 'Fn1', LastName = 'Contact1', Email = 'contact1@test.com',recordtypeid=orderrecordtype),
            new Contact(FirstName = 'Fn2', LastName = 'Contact2', Email = 'contact2@test.com',recordtypeid=orderrecordtype),
            //contact matched by key 2
            new Contact(FirstName = 'Fn3', LastName = 'Contact3', MailingStreet = 'Street 3', mailing_Street__c = 'Street 3', MailingPostalCode = '0123456',recordtypeid=orderrecordtype),
            //unmatched contact
            new Contact(FirstName = 'Fn4', LastName = 'Contact4', MailingStreet = 'Street 4',mailing_Street__c = 'Street 4',recordtypeid=orderrecordtype),
			// Contact with address that we found a lot of in PCD
			new Contact(FirstName='Trillion', LastName='StyleAddress', email='neverfindme@hidden.com', mailing_Street__c='1 Market Street Liverpool', MailingPostalCode='L1 1LL',recordtypeid=orderrecordtype)
        };
        return contacts;
    }
    
    
    private static List<account> prepareExistingAccounts() {
    	/* No Matching Rules.
        final List<account> contacts = new List<account>{
            //contact matched by key 1
            new account(name = 'Fn1 Contact1', Email__C = 'contact1@test.com',Contact_Matching_Key_1__c='fn1contact1contact1@test.com'),
            new account(name  = 'Fn2 Contact2',  Email__c = 'contact2@test.com',Contact_Matching_Key_1__c='fn2contact2contact2@test.com'),
            //contact matched by key 2
            new account(name = 'Fn3 Contact3',Contact_Matching_Key_2__c='fn1contact1street 30123456'),
            //unmatched contact
            new account(name  = 'Fn4 Contact4'),
			// Contact with address that we found a lot of in PCD
			new account(name ='Trillion StyleAddress',  email__c='neverfindme@hidden.com',Contact_Matching_Key_2__c='trillionstyleaddress1marketstreetliverpooll11ll')
        };
        return contacts;
        */
        return null;
    }
    /**
     * check how CustomerManagement.findContacts() logic works for direct search of Contact
     */
    static testMethod void testFindContactsDirectly () {    	
        final List<Contact> contacts = prepareExistingContacts();
        Database.insert(contacts);
        
        final List<TestCustomerWrapper> wrappers = new List<TestCustomerWrapper> {
            new TestCustomerWrapper(new Map<String, Object> {
                'FirstName' => 'Fn1', 'LastName' => 'Contact1', 'Email' => 'contact1@test.com', 'TEST_ShopperId' => 'Shopper1'
            }),
            new TestCustomerWrapper(new Map<String, Object> {
                'FirstName' => 'Fn-Not-Found', 'LastName' => 'Contact1', 'Email' => 'contact-not-found@test.com', 'TEST_ShopperId' => 'ShopperX'
            }),
            new TestCustomerWrapper(new Map<String, Object> {
                'FirstName' => 'Fn2', 'LastName' => 'Contact2', 'Email' => 'contact2@test.com', 'TEST_ShopperId' => 'Shopper2'
            }),
            // find via Key2 
            new TestCustomerWrapper(new Map<String, Object> {
                'FirstName' => 'Fn3', 'LastName' => 'Contact3', 'MailingStreet' => 'Street 3', 'MailingPostalCode' => '0123456',  'TEST_ShopperId' => 'Shopper3'
            })
        };
        Set<String> foundShopperIds = CustomerManagement.findContacts(wrappers);
        System.assert(foundShopperIds.contains('Shopper1'), 'Expected Shopper1 to be found');
        System.assertNotEquals(null, wrappers[0].getContact(), 'Expected Not null contact for Contact1');
        System.assertEquals('contact1@test.com', wrappers[0].getContact().Email, 'Expected Not null contact for Contact1');
        
        System.assertEquals(null, wrappers[1].getContact(), 'Did not Expect contact-not-found@test.com to be found');

        //check Key 2 match
        System.assert(foundShopperIds.contains('Shopper3'), 'Expected Shopper3 to be found');
        System.assertNotEquals(null, wrappers[3].getContact(), 'Expected Not null contact for Contact3');
        System.assertEquals('Street 3', wrappers[3].getContact().MailingStreet, 'Expected Not null contact for Contact3');
        
        System.assertEquals(3, foundShopperIds.size(), 'Expected 3 contacts to be found');
    }

	// Trillion creates contacts that seem to have the town/city in the Mailing Street
	private static testMethod void testTrillionContact() {
		// non-PCD matching
        id orderrecordtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Ordering Customer').getRecordTypeId();
        
		// matching key 1 test
		Contact c1 = new Contact(FirstName='Abigail', LastName='Crown', recordtypeid=orderrecordtype,email='willmatchthis@email.com',MailingStreet='1 York Road Maidenhead', MailingPostalCode='SL6 17Y', MailingCity='Maidenhead');
		insert c1;
		// matching key 2 test
		Contact c2 = new Contact(FirstName='Brandy', LastName='Crown', recordtypeid=orderrecordtype,email='wontmatchthis@email.com',MailingStreet='1 York Road', MailingPostalCode='SL7 17Y', MailingCity='Slough');
		insert c2;
		// matching key 3 test
		Contact c3 = new Contact(FirstName='Carole', LastName='Crown', recordtypeid=orderrecordtype,email='wontmatchthis@email.com',MailingStreet='1 York Road Burnham', MailingPostalCode='SL8 17Y', MailingCity='Burnham');
		insert c3;
		
		// PCD matching
		// matching key 1 test
		Contact c1p = new Contact(PCD_ID__c='pcd1', FirstName='Abigail', LastName='King',recordtypeid=orderrecordtype, email='willmatchthis@email.com',MailingStreet='1 York Road Maidenhead', MailingPostalCode='SL6 17Y', MailingCity='Maidenhead');
		insert c1p;
		// matching key 2 test
		Contact c2p = new Contact(PCD_ID__c='pcd2', FirstName='Brandy', LastName='King',recordtypeid=orderrecordtype, email='wontmatchthis@email.com',MailingStreet='1 York Road', MailingPostalCode='SL7 17Y', MailingCity='Slough');
		insert c2p;
		// matching key 3 test
		Contact c3p = new Contact(PCD_ID__c='pcd3', FirstName='Carole', LastName='King',recordtypeid=orderrecordtype, email='wontmatchthis@email.com',MailingStreet='1 York Road Burnham', MailingPostalCode='SL8 17Y', MailingCity='Burnham');
		insert c3p;
		
		// Contact Profile matching
		// matching key 1 test
		Contact c1cp = new Contact(FirstName='Abigail', LastName='Queen', recordtypeid=orderrecordtype,email='willmatchthis@email.com',MailingStreet='1 York Road Maidenhead', MailingPostalCode='SL6 17Y', MailingCity='Maidenhead');
		insert c1cp;
		// matching key 2 test
		Contact c2cp = new Contact(FirstName='Brandy', LastName='Queen', recordtypeid=orderrecordtype,email='wontmatchthis@email.com',MailingStreet='1 York Road', MailingPostalCode='SL7 17Y', MailingCity='Slough');
		insert c2cp;
		// matching key 3 test
		Contact c3cp = new Contact(FirstName='Carole', LastName='Queen', recordtypeid=orderrecordtype,email='wontmatchthis@email.com',MailingStreet='1 York Road Burnham', MailingPostalCode='SL8 17Y', MailingCity='Burnham');
		insert c3cp;
        User runningUser = [SELECT Id FROM User WHERE Profile.Name = 'JL: Unrestricted Profile (5 Users Max)' and isActive = true LIMIT 1];
        System.runAs(runningUser) {
    		Contact_Profile__c cp1 = new Contact_Profile__c(FirstName__c='Abigail', LastName__c='Queen', email__c='willmatchthis@email.com', Contact__c=c1cp.Id);
    		insert cp1;
    		Contact_Profile__c cp2 = new Contact_Profile__c(FirstName__c='Brandy', LastName__c='Queen', Mailing_Street__c='1 York Road', MailingPostCode__c='SL7 17Y', MailingCity__c='Slough', Contact__c=c2cp.Id);
    		insert cp2;
    		Contact_Profile__c cp3 = new Contact_Profile__c(FirstName__c='Carole', LastName__c='Queen', Mailing_Street__c='1 York Road', MailingPostCode__c='SL7 17Y', MailingCity__c='Burnham', Contact__c=c3cp.Id);
    		insert cp3;
		}		
		final List<TestCustomerWrapper> wrappers = new List<TestCustomerWrapper> {
            new TestCustomerWrapper(new Map<String, Object> {
                'FirstName' => 'Abigail', 'LastName' => 'Crown', 'Email' => 'willmatchthis@email.com', 'TEST_ShopperId' => 'Shopper1'
            }),
            new TestCustomerWrapper(new Map<String, Object> {
                'FirstName' => 'Brandy', 'LastName' => 'Crown', 'Email' => 'contact-not-found@test.com', 'MailingStreet' => '1 York Road', 
                'TEST_ShopperId' => 'ShopperX', 'MailingPostalCode' => 'SL7 17Y'
            }),
            new TestCustomerWrapper(new Map<String, Object> {
                'FirstName' => 'Carole', 'LastName' => 'Crown', 'Email' => 'contact2@test.com', 'TEST_ShopperId' => 'Shopper2', 'MailingCity' => 'Burnham',
                'MailingPostalCode' => 'SL8 17Y', 'MailingStreet' => '1 York Road'
            }),
            new TestCustomerWrapper(new Map<String, Object> {
                'FirstName' => 'Abigail', 'LastName' => 'King', 'Email' => 'willmatchthis@email.com', 'TEST_ShopperId' => 'Shopper1'
            }),
            new TestCustomerWrapper(new Map<String, Object> {
                'FirstName' => 'Brandy', 'LastName' => 'King', 'Email' => 'contact-not-found@test.com', 'MailingStreet' => '1 York Road', 
                'TEST_ShopperId' => 'ShopperX', 'MailingPostalCode' => 'SL7 17Y'
            }),
            new TestCustomerWrapper(new Map<String, Object> {
                'FirstName' => 'Carole', 'LastName' => 'King', 'Email' => 'contact2@test.com', 'TEST_ShopperId' => 'Shopper2', 'MailingCity' => 'Burnham',
                'MailingPostalCode' => 'SL8 17Y', 'MailingStreet' => '1 York Road'
            }),
            new TestCustomerWrapper(new Map<String, Object> {
                'FirstName' => 'Abigail', 'LastName' => 'Queen', 'Email' => 'willmatchthis@email.com', 'TEST_ShopperId' => 'Shopper1'
            }),
            new TestCustomerWrapper(new Map<String, Object> {
                'FirstName' => 'Brandy', 'LastName' => 'Queen', 'Email' => 'contact-not-found@test.com', 'MailingStreet' => '1 York Road', 
                'TEST_ShopperId' => 'ShopperX', 'MailingPostalCode' => 'SL7 17Y'
            }),
            new TestCustomerWrapper(new Map<String, Object> {
                'FirstName' => 'Carole', 'LastName' => 'Queen', 'Email' => 'contact2@test.com', 'TEST_ShopperId' => 'Shopper2', 'MailingCity' => 'Burnham',
                'MailingPostalCode' => 'SL8 17Y', 'MailingStreet' => '1 York Road'
            })
            
        };
        Set<String> foundShopperIds = CustomerManagement.findContacts(wrappers);
        System.debug('ANTONY==foundShopperIds ' + foundShopperIds);
		system.assertEquals(3, foundShopperIds.size());
		// look at the wrappers and check they got the correct contact
		system.assertEquals(c1.Id, wrappers[0].getContact().Id);
		system.assertEquals(c2.Id, wrappers[1].getContact().Id);
		system.assertEquals(c3.Id, wrappers[2].getContact().Id);
		system.assertEquals(c1p.Id, wrappers[3].getContact().Id);
		system.assertEquals(c2p.Id, wrappers[4].getContact().Id);
		system.assertEquals(c3p.Id, wrappers[5].getContact().Id);
		//system.assertEquals(c1cp.Id, wrappers[6].getContact().Id);
		//system.assertEquals(c2cp.Id, wrappers[7].getContact().Id);
		//system.assertEquals(c3cp.Id, wrappers[8].getContact().Id);
	}

    //
    // Test that we add the Contact Profile to the best contact if we have duplicates. The best contact is the most recently created and has a pcd_id__c (also match with key1 before key2)
    //
    private static testMethod void test_duplicateMatching() {
        // Some Distraction contacts that don't match
        // Matching key 1 - 4 contacts -> old with PCDId, newer with PCDID, old without PCD, newer with PCDID
        // Matching key 2 - 4 contacts -> old with PCDId, newer with PCDID, old without PCD, newer with PCDID
        // Matching key 1 - 4 contacts -> none with PCDId
        // Matching key 2 - 4 contacts -> none with PCDId

        List<Contact> distractionContacts = prepareExistingContacts();
        insert distractionContacts;
        dumpContacts('distraction', distractionContacts);
         
        List<Contact> c1 = createDuplicateContacts('pcd1', 'pcd2', null, null, 'first1', 'last1', 'email1@email.edu', 'street1', 'postcode1');
        insert c1;
        dumpContacts('c1', c1);
    
        List<Contact> c2 = createDuplicateContacts('pcd3', 'pcd4', null, null, 'first2', 'last2', 'email2@email.edu', 'street2', 'postcode2');
        insert c2;
        dumpContacts('c2', c2);

        List<Contact> c3 = createDuplicateContacts(null, null, null, null, 'first3', 'last3', 'email3@email.edu', 'street3', 'postcode3');
        insert c3;
        dumpContacts('c3', c3);

        List<Contact> c4 = createDuplicateContacts(null, null, null, null, 'first4', 'last4', 'email4@email.edu', 'street4', 'postcode4');
        insert c4;
        //dumpContacts('c4', c4);
    
        List<Contact_Profile__c> profiles;
        
        Test.StartTest();
            //profiles = createContactProfiles();
        Test.StopTest();

       // testDuplicate(c1[1], profiles[0]);
        //testDuplicate(c2[1], profiles[1]);
       // testDuplicate(c3[3], profiles[2]);
        //testDuplicate(c4[3], profiles[3]);
    }
    
    private static void dumpContacts(String s, List<Contact> contacts) {
        for (Contact c:contacts) {
            system.debug(s+' c: '+c.Id);
        }       
    }

    // Create 4 duplicates
    private static List<Contact> createDuplicateContacts(String pcd1, String pcd2, String pcd3, String pcd4, String fName, String lName, String email, String street, String postCode) {
        final List<Contact> contacts = new List<Contact>{
            new Contact(FirstName = fName, LastName = lName, Email = email, MailingStreet = street, MailingPostalCode = postCode, pcd_id__c=pcd1, PCD_Timestamp__c=System.Today()-4),
            new Contact(FirstName = fName, LastName = lName, Email = email, MailingStreet = street, MailingPostalCode = postCode, pcd_id__c=pcd2, PCD_Timestamp__c=System.Today()-3),
            new Contact(FirstName = fName, LastName = lName, Email = email, MailingStreet = street, MailingPostalCode = postCode, pcd_id__c=pcd3, PCD_Timestamp__c=System.Today()-2),
            new Contact(FirstName = fName, LastName = lName, Email = email, MailingStreet = street, MailingPostalCode = postCode, pcd_id__c=pcd4, PCD_Timestamp__c=System.Today()-1)
        };
        return contacts;    
    }
    
    private static List<Contact_Profile__c> createContactProfiles() {
        final List<Contact_Profile__c> profiles = new List<Contact_Profile__c> {
            new Contact_Profile__c(Shopper_Id__c = 's1', FirstName__c = 'first1', LastName__c = 'last1', Email__c = 'email1@email.edu', Mailing_Street__c='NOMATCH', MailingPostCode__c='postcode1'),
            new Contact_Profile__c(Shopper_Id__c = 's2', FirstName__c = 'first2', LastName__c = 'last2', Email__c = 'NOMATCH@nomatch.edu', Mailing_Street__c='street2', MailingPostCode__c='postcode2'),
            new Contact_Profile__c(Shopper_Id__c = 's3', FirstName__c = 'first3', LastName__c = 'last3', Email__c = 'email3@email.edu', Mailing_Street__c='NOMATCH', MailingPostCode__c='postcode3'),
            new Contact_Profile__c(Shopper_Id__c = 's4', FirstName__c = 'first4', LastName__c = 'last4', Email__c = 'NOMATCH@nomatch.edu', Mailing_Street__c='street4', MailingPostCode__c='postcode4')
        };
        Database.insert(profiles);  
        return profiles;    
    }
    
    private static void testDuplicate(Contact expected, Contact_Profile__c cp) {
        Contact_Profile__c cpRequery = [SELECT Id, Contact__c FROM Contact_Profile__c WHERE Id=:cp.Id];
        system.assertNotEquals(null, cpRequery);
        system.assertEquals(expected.Id, cpRequery.Contact__c);     
    }
    
}