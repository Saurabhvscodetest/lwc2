@isTest
private class JLMoveUserBackToPrimaryTeamBatch_Test {
	private static final Integer randomNo = UnitTestDataFactory.getRandomNumber();
	private static final string USER_FIRST_NAME = 'BatchTest';
	private static final string BOOKINGS_TEAM_NAME = 'TESTTEAM' + randomNo;
	private static final string HAMILTION_TEAM_NAME = BOOKINGS_TEAM_NAME+ 'A';

	@testSetup static void setupTestConfig() {

		Config_Settings__c teamConfig = Config_Settings__c.getInstance('AUTO_RETURN_TO_TEAM_PROFILE_I');

		if (teamConfig == null) {
			teamConfig = new Config_Settings__c(Name = 'AUTO_RETURN_TO_TEAM_PROFILE_I');
		}

		teamConfig.Text_Value__c = 'JL: CapitaContactCentre Tier2-3';
		
		Config_Settings__c  teamMinutesConfig = Config_Settings__c.getInstance('AUTO_RETURN_TO_TEAM_MINUTES');
		if (teamMinutesConfig == null) {
			teamMinutesConfig = new Config_Settings__c(Name = 'AUTO_RETURN_TO_TEAM_MINUTES');
		}

		teamMinutesConfig.Number_Value__c = 0;

		Config_Settings__c  teamBatchRerunConfig = Config_Settings__c.getInstance('AUTO_RETURN_TO_TEAM_BATCH_RERUN');
		if (teamBatchRerunConfig == null) {
			teamBatchRerunConfig = new Config_Settings__c(Name = 'AUTO_RETURN_TO_TEAM_BATCH_RERUN');
		}

		teamBatchRerunConfig.Number_Value__c = 10;
		List<Config_Settings__c> configsToUpsert = new List<Config_Settings__c>{teamConfig,teamMinutesConfig,teamBatchRerunConfig};

		upsert configsToUpsert;
	}

	private static List<User> BuildUsersToTest()
	{
		List<User> userCollection = new List<User>();

		User sysAdmin = UnitTestDataFactory.getSystemAdministrator('admin1');
		
		
    	User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
		System.runAs(runningUser) { // Avoid MIX_DML
   			UnitTestDataFactory.setUpTeams(new List<String>{BOOKINGS_TEAM_NAME, HAMILTION_TEAM_NAME}, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
		}
   		
		System.runAs(sysAdmin) {

			UserRole role = [select Id FROM UserRole where Name='JLP:CC:CSA:T1' LIMIT 1];
			User userManagerRec = UnitTestDataFactory.getBackOfficeUser('Manager', BOOKINGS_TEAM_NAME);
			userManagerRec.UserRoleId = role.Id; 
			userManagerRec.Is_User_Team_Manager__c = true;        

			insert userManagerRec; 

			string result;

			for(Integer i = 0; i < 5; i++)
			{
				User userRec = UnitTestDataFactory.getBackOfficeUser(USER_FIRST_NAME + i, HAMILTION_TEAM_NAME);
				userRec.UserRoleId = role.Id; 
				userRec.Is_User_Team_Manager__c = false;  
				userRec.My_Team_Manager__c = userManagerRec.id;

				userCollection.add(userRec);    
			}
			insert userCollection;
		}
   
		return userCollection;          
	}

	@isTest static void testExecuteBatchChangesTheUsersTeamBackToManagersTeam() {

		List<User> users = BuildUsersToTest();

		Test.startTest();
		JLMoveUserBackToPrimaryTeamBatch batch = new JLMoveUserBackToPrimaryTeamBatch();
		Database.executeBatch(batch);
		Test.stopTest();

		string tempInput =  USER_FIRST_NAME + '%';
		users = [SELECT id, team__c  FROM User WHERE Name LIKE : tempInput ];
		System.assertEquals(BOOKINGS_TEAM_NAME, users[0].team__c);
	}
}