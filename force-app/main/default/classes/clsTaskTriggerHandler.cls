public without sharing class clsTaskTriggerHandler {
	public static String Closed_Status = 'Closed';
	public static String Completed = 'Completed';
    public static Integer counter = 0;
    public static boolean FIRST_AFTER_TRIGGER_RUN = false;   
	public static String TASK_CLOSED_RESOLVED_STATUS = 'Closed - Resolved';
	public static String EMAIL_COMMENT_DESCRIPTION = 'Email sent from Case';
	public static String EMAIL_TASK_SUBJECT_PREFIX = 'Email:';
	private static boolean COMMENTS_INSERT_FIRST_RUN = true;
	private static boolean INCREASE_REPEAT_CONTACT = false;
	private static Map<Id, Case> linkedCaseMap = new Map<Id, Case>();
	private static Set<Id> processedTaskIdSet = new Set<Id>(); // <<006>>
	private static Set<Id> processedCaseIdSet = new Set<Id>();
    private static Set<Id> emailTaskIds = new Set<Id>();
    private static Set<Id> failedMilestoneIds = new Set<Id>();
    public static Map<Id, String> taskIdToMilestoneTypeMap = new Map<Id, String>();
    private static Map<Id, Case> taskIdToCaseMap = new Map<Id, Case>();
	private static final Config_Settings__c AUTO_RESPONSE_CONFIG = Config_Settings__c.getInstance('AUTO_RESPONSE_USING_WORKFLOW');
	private static final Boolean USE_WORKFLOWS_AS_AUTO_RESPONSE = AUTO_RESPONSE_CONFIG == null ? false : AUTO_RESPONSE_CONFIG.Checkbox_Value__c;
	private static final String EMAIL_STATUS_DRAFT = '5';
	public static String MILESTONE_REPORTING_RECORD_TYPE = 'Milestone Reporting';
	public static final Id MS_REPORTING_RECORD_TYPE_ID = Schema.SObjectType.Task.getRecordTypeInfosByName().get(MILESTONE_REPORTING_RECORD_TYPE).getRecordTypeId();
	private static final String myUserName = Userinfo.getName();
	
	private static final DateTime closeInteractionStamp = system.now(); // <<009>>
	private static final List<String> milestoneNames = new List<String>{MilestoneHandlerSupport.FIRST_CONTACT_MILESTONE, MilestoneHandlerSupport.QUERY_RESOLUTION_MILESTONE, MilestoneHandlerSupport.NEXT_ACTION_DATE, MilestoneHandlerSupport.REOPEN_MILESTONE, MilestoneHandlerSupport.EMAIL_CONTACT, MilestoneHandlerSupport.COMPLAINT_RESOLUTION_MILESTONE, MilestoneHandlerSupport.PSE_RESOLUTION_MILESTONE, MilestoneHandlerSupport.NKU_RESOLUTION_MILESTONE, MilestoneHandlerSupport.TRIAGE_RESOLUTION_MILESTONE, MilestoneHandlerSupport.EXCEPTION_RESOLUTION_MILESTONE};
    @Testvisible private static final string MILESTONE_WARNING_TASK_SUBJECT_PREFIX = CommonStaticUtils.getValue('MILESTONE_WARNING_TASK_SUBJECT_PREFIX', 'MILESTONE - WARNING NOTIFICATION');
    @Testvisible private static final string MILESTONE_FAILED_TASK_SUBJECT_PREFIX = CommonStaticUtils.getValue('MILESTONE_FAILED_TASK_SUBJECT_PREFIX', 'MILESTONE - FAILED NOTIFICATION');
 
    /**
     * mainEntry method called from the Trigger.
	 * @params:	Standard Trigger context variables. 
	 * @rtnval:	void
     */
	public static void mainEntry(Boolean isBefore, 
								 Boolean isDelete, Boolean isAfter,
								 Boolean isInsert, Boolean isUpdate,
								 Boolean isExecuting, List<Task> newList,
								 Map<Id, SObject> newMap, List<Task> oldList,
								 Map<Id, SObject> oldMap) {

		System.debug('@@entering clsTaskTriggerHandler');

		if(isDelete) {
        	if(isBefore) {
	        	if(oldList.size()>0) {
        			if(!CommonStaticUtils.isPermissionSetAssignedToSingleUserByName(UserInfo.getUserId(), 'Delete_Tasks')){
			            for (Task t : oldList) {
			                t.addError('You cannot delete a Task');
			            }
			            return;
            		}
        		}
	        }		
        } else {                             
            List<CaseComment> listcc = new List<CaseComment>();
            List<FeedItem> listfi = new List<FeedItem>();
            Map<Id, Case> casesToUpdateMap = new Map<Id, Case>();
            Map<Id, Interaction__c> interactionUpdateMap = new Map<Id, Interaction__c>();
    
            Map<Id, Task> newValues = (Map<Id, Task>)newMap;
            Map<Id, Task> oldValues = (Map<Id, Task>)oldMap;
    
            // Build list of linked Cases - only do this for Cases not already present.
            Set<Id> setWhatIds = new Set<Id>();
            for (Task t : newList) {
                if (CommonStaticUtils.isCaseId(t.WhatId) && !linkedCaseMap.containsKey(t.WhatId)) {
                    setWhatIds.add(t.WhatId);
                }
            }
    
            if(!setWhatIds.isEmpty()) {
                // List<Case> linkedCaseList = [SELECT Id, Status, CaseNumber, OwnerId, jl_HasEmailOrigin__c, IsClosed, Origin, jl_Queue_Name__c, jl_Last_Queue_Name__c, LastModifiedDate, EH_Active_Subsequent_Exception__c,(SELECT Id, CreatedDate, ActivityId, ToAddress, FromAddress,Incoming,  Internal_Email__c, Dummy_Email__c FROM EmailMessages WHERE status !=:EMAIL_STATUS_DRAFT  AND Internal_Email__c = false AND Dummy_Email__c = false), (SELECT Id, jl_team__c, Status, Owner.Name FROM Tasks), (SELECT End_DT__c, Event_Type__c, Case_Status__c, Case__r.Status FROM Interaction__r WHERE Event_Type__c = 'Contact' AND End_DT__c = null) FROM Case WHERE Id IN :setWhatIds];
                List<Case> linkedCaseList = [SELECT Id, Number_of_Repeat_Contacts__c, Repeat_Contact_from_Customer__c, Status, CaseNumber, OwnerId, jl_HasEmailOrigin__c, IsClosed, Origin, jl_Queue_Name__c, jl_Last_Queue_Name__c, LastModifiedDate, Inbound_Emails_Not_Handled__c , EH_Active_Subsequent_Exception__c, 
                								(SELECT Id, jl_team__c, Status, Owner.Name FROM Tasks), 
                								(SELECT End_DT__c, Event_Type__c, Case_Status__c, Case__r.Status FROM Interaction__r WHERE Event_Type__c = 'Contact' AND End_DT__c = null), 
                								(SELECT Id, IsViolated, CompletionDate, TimeRemainingInMins, TargetResponseInMins, MilestoneType.Name FROM CaseMilestones WHERE CompletionDate = null AND IsViolated = TRUE AND MilestoneType.Name IN :milestoneNames) FROM Case WHERE Id IN :setWhatIds];
                linkedCaseMap.putAll(linkedCaseList);
            }
    
            if(isAfter && (!linkedCaseMap.isEmpty())) {
                // After insert/update of Tasks - will need the corresponding user info map building <<009>>
                InteractionData.buildUserInfoMap(newList);
            }
	    
		    if(isInsert && isBefore) {
		    	relateCaseToTask(newlist);
		    }
			
            // Main Processing Loop
            for (Task t : newlist) {
            	//Do Not do any post processing for a Milestone reporting task 
            	if(isMilestoneReportingTask(t)){
            		continue;
            	}                
                // Handle before insert operations
                if (isInsert && isBefore) {
                    handleTaskBeforeInserts(t);
                }                
                // Handle after insert operations
                if (isInsert && isAfter) {
                    handleTaskAfterInserts(t, listcc, casesToUpdateMap, interactionUpdateMap);
                }    
                // Handle before update operations
                if (isUpdate && isBefore) {
                    Task oldTask = oldValues.get(t.Id);
                    handleTaskBeforeUpdates(t, oldTask);
                }                
                // Handle after update operations and case counter updates
                // where the task has been created or updated manually we update the parent case lists 
                if (isUpdate && isAfter) {
                    Task oldTask = oldValues.get(t.Id);
                    handleTaskAfterUpdates(t, oldTask, listcc, listfi, casesToUpdateMap);
                }
            }

            // Above context calls are not bulkified, so adding additional bulkified call to VocEmailHandler
            if(isInsert && isBefore) {
            	VocEmailHandler.processRecords(null, newList);
            }

            if(isInsert && isAfter) {
            	VocEmailHandler.processRecords(null, newList);
            	
            	System.debug('** FIRST_AFTER_TRIGGER_RUN ' +  (++counter) + FIRST_AFTER_TRIGGER_RUN);
            	System.debug('** failedMilestoneIds.isEmpty()' + counter + failedMilestoneIds.isEmpty());
            	System.debug('** failedMilestoneIds ==' + counter+ failedMilestoneIds);
            	if(!FIRST_AFTER_TRIGGER_RUN && !failedMilestoneIds.isEmpty()){
            		//Milestone Reporting task creation will not invoke any case updates 
	            	MilestoneReportingTaskHelper.createMilestoneReportingTask(failedMilestoneIds);
	            }
            	FIRST_AFTER_TRIGGER_RUN = true; 
        		System.debug('** AFTER FLAG' + counter + FIRST_AFTER_TRIGGER_RUN);
            }
            
            // Insert updated linked record lists
            if(!listcc.isEmpty()) {
                Database.insert(listcc,false);
            }    
            if(!listfi.isEmpty()) {
                Database.insert(listfi,false);
            }            
            if(!casesToUpdateMap.isEmpty()) {
                updateLinkedCases(casesToUpdateMap);
                try {
                	Database.update(casesToUpdateMap.values(),false);
                }
                catch(Exception e) {
            		System.debug('Error : '+e.getMessage());
                }
            }            
            if (isUpdate && isAfter) {
                COMMENTS_INSERT_FIRST_RUN = false;
                //SA: 12/5/15 - Closing task and updating interaction record related to it.
                if (!emailTaskIds.isEmpty()) {
                    InteractionData.closeOverallEmailHandlingPerformanceRecord(emailTaskIds);
                    emailTaskIds.clear();
                }
            }
            
            // Close off any interactions where call logged <<009>>
            if (!interactionUpdateMap.isEmpty()) {
                Database.update(interactionUpdateMap.values(),false);
            }
        	System.debug('@@exiting clsTaskTriggerHandler');
    	}
    }
    
	/**
	 * Perform all Before Insert trigger actions on each individual task
	 * @params:	Task newTask	New Task record.
	 * @rtnval:	void
	 */
	private static void handleTaskBeforeInserts(Task newTask) {
		if(linkedCaseMap.containsKey(newTask.WhatId)) {
			Case thisCase = linkedCaseMap.get(newTask.WhatId);
			System.debug('Check to see if the parent case is closed / closing');
			System.debug('IsClosed: ' + thisCase.IsClosed);
			System.debug('Status: ' + thisCase.Status);
			if(thisCase.IsClosed && !newTask.Status.Contains(Closed_Status) && !isEmailTask(newTask)) {
				// Note that you can't use addError on a task
				throw new JLException('A closed case cannot have open tasks. Case: ' + thisCase.CaseNumber + ', Case Status: ' + thisCase.Status + ', Task Status:' + newTask.Status + ', Task Subject: ' + newTask.Subject);
			}
					
			// <<06>> CMP-94 spam filtering
			if(thisCase.Status.contains('SPAM')) {
				newTask.Status = 'Closed - SPAM';
				newTask.Subject += ' (SPAM)';
			}			
			//ES:31/03/2015 -Identify Auto-Reply Email Task and mark it as such
			identifyAutoReplyEmailTasks(newTask);
		}
	}

	private static void relateCaseToTask(List<Task> tasks) {
		Id CALL_LOG_TASK_RECORD_TYPE_ID = Schema.SObjectType.Task.getRecordTypeInfosByName().get(CommonStaticUtils.TASK_RT_NAME_CALL_LOG).getRecordTypeId();

		for (Task t : tasks) {
			if(t.RecordTypeId == CALL_LOG_TASK_RECORD_TYPE_ID && t.Case__c !=null && t.Whatid == null){
				 t.Whatid= t.Case__c;
			}
		 }
	}

	/**
	 * Perform all After Insert trigger actions on each individual task
	 * @params:	Task newTask									New Task record (post change).
	 *			Task oldTask									Old Task record (pre change).
	 *			List<CaseComment> listcc						List of Case Comments to be inserted later.
	 *			Map<Id, Case> casesToUpdateMap					Map of Cases to be updated later.
	 *			Map<Id, Interaction__c> interactionUpdateMap	Map of Interactions to be updated later.
	 * @rtnval:	void
	 */
	private static void handleTaskAfterInserts(Task newTask, List<CaseComment> listcc, Map<Id, Case> casesToUpdateMap, Map<Id, Interaction__c> interactionUpdateMap) {
	
		if(isMilestoneFailedTask(newTask)) {
			
			// store map of task to it's correspondoing milestone type/name
			String milestoneName = MilestoneReportingTaskHelper.convertMilestoneReportingTaskSubjectToCorrespondingMilestoneName(newTask);
			taskIdToMilestoneTypeMap.put(newTask.Id, milestoneName);
			
			// store map of task to it's corresponding case
			Case thisCase = linkedCaseMap.get(newTask.WhatId);
			if(thisCase != null) {
				taskIdToCaseMap.put(newTask.Id, thisCase);
			}

			Id caseMilestoneViolated = MilestoneReportingTaskHelper.isCaseMilestoneViolated(milestoneName, thisCase);

			if(caseMilestoneViolated != null) {
				failedMilestoneIds.add(caseMilestoneViolated);
			} 
		}
		
		// Make sure that this task belongs to Case
		if(CommonStaticUtils.isCaseId(newTask.WhatId) && ((newTask.Subject == CommonStaticUtils.TASK_SUBJECT_CALL_LOG || newTask.Subject == CommonStaticUtils.TASK_SUBJECT_EMAIL_LOG) || !newTask.jl_created_from_case__c)) {
			Case thisCase = linkedCaseMap.get(newTask.WhatId);
			// Process the case updates if all the 3 conditions are satisfied
			// 1. Add this Case id to the casesToUpdateMap so that counts are rebuilt
		    // 2. Its Not a milestone task &  Owner of the case is a user .(since all milestone tasks are created with a closed status and  if the case is owned by an user then ignore the case update)
			if(!processedCaseIdSet.contains(newTask.WhatId) && 
					(!((isMilestoneTask(newTask) && (CommonStaticUtils.isUserId(thisCase.ownerId)))))
				) {
				resetParentCaseCounters(thisCase);
				casesToUpdateMap.put(thisCase.Id, thisCase);
				processedCaseIdSet.add(thisCase.Id);
			}
			
			//Create Case Comments for Log a Call tasks
			if(newTask.Subject == CommonStaticUtils.TASK_SUBJECT_CALL_LOG || newTask.Subject == CommonStaticUtils.TASK_SUBJECT_EMAIL_LOG) {
				CaseComment cc = new CaseComment(
					ParentId = newTask.WhatId,
					CommentBody = newTask.Subject + '\n' + newTask.Description
				);
				cc.setOptions(getDMLOptions());
				listcc.add(cc);
				
				// Close contact interactions linked to this Case now customer called <<009>>
				InteractionData.closeCaseInteractions(closeInteractionStamp, interactionUpdateMap, thisCase.Interaction__r);
			}
		}
	}

	/**
	 * Perform all Before Update trigger actions on each individual task
	 * @params:	Task newTask	New Task record (post change).
	 *			Task oldTask	Old Task record (pre change).
	 * @rtnval:	void
	 */
	private static void handleTaskBeforeUpdates(Task newTask, Task oldTask) {
		if (newTask.jl_team__c != null && (oldTask.jl_team__c == null || oldTask.jl_team__c != newTask.jl_team__c)) {
			if (newTask.ownerid != null && (oldTask.ownerid == null || oldTask.ownerid == newTask.ownerid)) {
				newTask.ownerid = TeamUtils.getAssigneeForTeam (newTask.jl_team__c);
			}
		}		
		if(newTask.Status != null && newTask.Status.contains(Closed_Status) && newTask.JL_Closed_Date_Time__c == null && newTask.jl_created_from_case__c == false && linkedCaseMap.containsKey(newTask.WhatId)) {
			Case thisCase = linkedCaseMap.get(newTask.WhatId);
			String AgentName = myUserName;
			newTask.jl_Agent_Name__c = AgentName.length() > 180 ?  AgentName.substring(0,179) : AgentName;
			newTask.jl_Related_Case_Last_Queue_Name__c = String.IsBlank(thisCase.jl_Queue_Name__c) ? thisCase.jl_Last_Queue_Name__c : thisCase.jl_Queue_Name__c;
			newTask.JL_Closed_Date_Time__c = DateTime.now();
            if (newTask.Closed_No_Response_Required__c) {
            	emailTaskIds.add(newTask.Id);
            }
		}
	}
	
	/**
	 * Perform all After Update trigger actions on each individual task
	 * @params:	Task newTask					New Task record (post change).
	 *			Task oldTask					Old Task record (pre change).
	 *			List<CaseComment> listcc		List of Case Comments to be inserted later.
	 *			List<FeedItem> listfi			List of Feed Items to be inserted later.
	 *			Map<Id, Case> casesToUpdateMap	Map of Cases to be updated later.
	 * @rtnval:	void
	 */
	private static void handleTaskAfterUpdates(Task newTask, Task oldTask, List<CaseComment> listcc, List<FeedItem> listfi, Map<Id, Case> casesToUpdateMap) {
		
		if (!processedTaskIdSet.contains(newTask.Id) && newTask.whatid != null && (newTask.Status.startsWith(Completed) || newTask.Status.startsWith(Closed_Status))) {
			System.debug('creating a casecomment');
			
			if (linkedCaseMap.containsKey(newTask.WhatId)) {
				Case thisCase = linkedCaseMap.get(newTask.WhatId);
				CaseComment cc = new CaseComment(parentid = newTask.whatid);
				
				// removed the 'truncated' message from it is now done in the CaseComment trigger
				cc.CommentBody = getCaseCommentBody(newTask, thisCase);

				// 001/002 - the very last line of code is ensure length does not exceed maximum.
				// 004 - Removed truncation logic because the DML allowFieldTruncation option is handling it
				//       and because we need a near exact byte size truncation to trigger the commentTruncated 
				//       test in the clsCaseCommentTriggerHandler class
						
				cc.setOptions(getDMLOptions());
				if(!isEmailTask(newTask)) {
					listcc.add(cc); 
				}

				if (COMMENTS_INSERT_FIRST_RUN) {
					if (oldTask.Status != newTask.Status && TASK_CLOSED_RESOLVED_STATUS.equals(oldTask.Status)) {
						System.debug('got a new tasks that is closed');
						System.debug('oldTaskValues.Status ' + oldTask.Status);
						System.debug('newTaskValues.Status ' + newTask.Status);
						
						CaseComment cc2 = new CaseComment();
						cc2.ParentId = newTask.WhatId;
						// cc.IsPublished TODO - do we need to set this field?
						// Select t.jl_Team__c, t.Subject, t.Priority From Task t
						cc2.CommentBody = 'Task ' + newTask.Subject + ' Assigned to Team ' + newTask.jl_Team__c + ' has been closed by ' + myUserName;
						listcc.add(cc2);
                    }   	
				}
				
				// Check if the owner is a user or a group - can't chatter to a queue
				String caseOwnerString = thisCase.OwnerId;
				if (CommonStaticUtils.isUserId(thisCase.OwnerId)) {
					FeedItem fi = new FeedItem();
					fi.ParentId = thisCase.OwnerId;
					fi.Body = cc.CommentBody;
					// 004 - Enable the DML allowFieldTruncation option for the Case Feed Item insert
					fi.setOptions(getDMLOptions());
					listfi.add(fi);
				}
			}
		}
		
		processedTaskIdSet.add(newTask.Id); // <<006>>

		// If not already done, rebuild the totals fields on the parent Case
		if (newTask.WhatId != null && CommonStaticUtils.isCaseId(newTask.WhatId) && !processedCaseIdSet.contains(newTask.WhatId)) {
			Case thisCase = linkedCaseMap.get(newTask.WhatId);
			// Process the case updates if all the 2 conditions are satisfied
		    // 1. Its Not a milestone task &  Owner of the case is a user .(since all milestone tasks are created with a closed status and  if the case is owned by an user then ignore the case update)
			// 2. If its not a milestone reporting record type.
			//since all milestone tasks are created with a closed status and  if the case is owned by an user then ignore the case update
			if((!(isMilestoneTask(newTask) && CommonStaticUtils.isUserId(thisCase.ownerId)))){
				resetParentCaseCounters(thisCase);
				casesToUpdateMap.put(thisCase.Id, thisCase);
				processedCaseIdSet.add(thisCase.Id);
			}
		}
	}

	private static boolean isMilestoneWarningTask(Task taskObj) {
		if(taskObj != null && String.isNotBlank(taskObj.subject)) {
			return (taskObj.subject.startsWithIgnoreCase(MILESTONE_WARNING_TASK_SUBJECT_PREFIX));
		}
		return false;
	}
	
	private static boolean isMilestoneFailedTask(Task taskObj) {
		if(taskObj != null && String.isNotBlank(taskObj.subject)) {
			return (taskObj.subject.startsWithIgnoreCase(MILESTONE_FAILED_TASK_SUBJECT_PREFIX));
		}
		return false;
	}

	private static boolean isMilestoneTask(Task taskObj) {
		return (isMilestoneWarningTask(taskObj) || isMilestoneFailedTask(taskObj));
	}
	
	private static boolean isMilestoneReportingTask(Task taskObj) {
		return (taskObj.recordTypeId == MS_REPORTING_RECORD_TYPE_ID);
	}

	/**
	 * Perform destructive rebuild on Task summary fields on the linked Case objects
	 * specifically:
	 * jl_Outstanding_Team_Tasks__c
	 * jl_OutstandingTaskOwners__c
	 * jl_NumberOutstandingTasks__c
	 * Number_of_Call_Contacts__c
	 * Last_Call_Contact__c
	 * @params:	Map<Id, Case> casesToUpdate		Build map of linked cases to be updated
	 * @rtnval:	void
	 */
	private static void updateLinkedCases(Map<Id, Case> casesToUpdateMap) {
		if (!casesToUpdateMap.isEmpty()) {
			List<Task> allLinkedTasks = [SELECT Id, jl_team__c, jl_Related_to_Incoming_Email__c, Repeat_Contact_from_Customer__c, Status, OwnerId, Owner.Name, WhatId, CreatedDate, Subject, RecordTypeId FROM Task WHERE WhatId IN :casesToUpdateMap.keySet()];
			
			if (!allLinkedTasks.isEmpty()) {
				Map<Id, EHTaskCounter> ehCaseTaskCounterMap = new Map<Id, EHTaskCounter>(); // <<010>>
				Set<Id> processedInboundEmailCaseList = new Set<Id>(); // <<014>>
                Map<Id,Set<String>> caseUniqueOwnerMap = new Map<Id,set<String>>();
				for (Task thisTask : allLinkedTasks) {
					Case thisCase = casesToUpdateMap.get(thisTask.WhatId);
				
					if (!thisTask.Status.startsWith(Closed_Status)) {
						Set<String> uniqueOwners;
						if (caseUniqueOwnerMap.get(thisTask.WhatId) == null) {
							uniqueOwners = new set<string>();
							uniqueOwners.add(thisTask.Owner.Name);
							caseUniqueOwnerMap.put(thisTask.WhatId,uniqueOwners);
						} else {
							caseUniqueOwnerMap.get(thisTask.WhatId).add(thisTask.Owner.Name);
						}
						// <<011>> End

						// <<014>> Start
						if (thisTask.jl_Related_to_Incoming_Email__c && !processedInboundEmailCaseList.contains(thisTask.WhatId) && Trigger.isInsert) {
							thisCase.Inbound_Emails_Not_Handled__c = true ;
						}
						// <<014>> End
						
						thisCase.jl_NumberOutstandingTasks__c = thisCase.jl_NumberOutstandingTasks__c + 1;
						if (String.isNotBlank(thisTask.jl_team__c)) {
							if (String.isBlank(thisCase.jl_Outstanding_Team_Tasks__c)) {
								thisCase.jl_Outstanding_Team_Tasks__c = thisTask.jl_team__c;
							} else {
								thisCase.jl_Outstanding_Team_Tasks__c = thisCase.jl_Outstanding_Team_Tasks__c + ', ' + thisTask.jl_team__c;
							}
						}
					}

					// <<010>> Start
					if (thisTask.RecordTypeId != null && (thisTask.RecordTypeId.equals(EHBaseExceptionHandler.eventHubGVFTaskRecordTypeId) || thisTask.RecordTypeId.equals(EHBaseExceptionHandler.eventHubSDTaskRecordTypeId) || thisTask.RecordTypeId.equals(EHBaseExceptionHandler.eventHubCCTaskRecordTypeId))) {
						EHTaskCounter ehtc;
						if (ehCaseTaskCounterMap.containsKey(thisTask.WhatId)) {
							ehtc = ehCaseTaskCounterMap.get(thisTask.WhatId);
						} else {
							ehtc = new EHTaskCounter();
							ehCaseTaskCounterMap.put(thisTask.WhatId, ehtc);
						}
						ehtc.ehTotalTasksCount++;
						if (!thisTask.Status.startsWith(Closed_Status)) {
							ehtc.ehOpenTasksCount++;
						}
					}
					// <<010>> End
					
					if (thisTask.Subject == CommonStaticUtils.TASK_SUBJECT_CALL_LOG) {
						thisCase.Number_of_Call_Contacts__c = thisCase.Number_of_Call_Contacts__c + 1;
						if (thisCase.Last_Call_Contact__c == null || thisTask.CreatedDate > thisCase.Last_Call_Contact__c) {
							thisCase.Last_Call_Contact__c = thisTask.CreatedDate;
						}
						if(!INCREASE_REPEAT_CONTACT && thisTask.Repeat_Contact_from_Customer__c == true){
							increaseNumberOfRepeatContacts(thisCase);
							thisCase.Repeat_Contact_from_Customer__c = true;
						}
					}
				}
                // <<011>> Adding Unique Task Owners
                for (Id csId : caseUniqueOwnerMap.KeySet()) {
                    casesToUpdateMap.get(csId).jl_OutstandingTaskOwners__c = getUniqueTaskOwnerAsCommaSeperated(caseUniqueOwnerMap.get(csId)) ;
                }
                // <<011>> End
				// <<010>> Start
				for (Id ehCaseId : ehCaseTaskCounterMap.keySet()) {
					EHTaskCounter ehtc = ehCaseTaskCounterMap.get(ehCaseId);
					if (ehtc.ehTotalTasksCount > 1 && ehtc.ehOpenTasksCount > 0) {
						Case thisCase = casesToUpdateMap.get(ehCaseId);
						thisCase.EH_Active_Subsequent_Exception__c = true; 
					}
				}
				// <<010>> End
			}
		}
	}
	
	/**
	 * ES:31/03/2015: Identify Auto-Reply Email Task and mark it as such
	 * @params:	Task newTask	New Task record being inserted.
	 * @rtnval:	void
	 */
	private static void identifyAutoReplyEmailTasks(Task newTask) {
		if (!USE_WORKFLOWS_AS_AUTO_RESPONSE) {
			if (!String.IsBlank(newTask.WhatId) && linkedCaseMap.containsKey(newTask.Whatid)) {
				Case relatedCase = linkedCaseMap.get(newTask.WhatId);
				
				if (relatedCase.Origin != null && relatedCase.Origin.equals('Email') && relatedCase.Tasks.isEmpty()) {
					newTask.JL_Closed_Date_Time__c = DateTime.now();
					newTask.jl_Related_Case_Last_Queue_Name__c = String.IsBlank(relatedCase.jl_Queue_Name__c) ? relatedCase.jl_Last_Queue_Name__c : relatedCase.jl_Queue_Name__c;
					newTask.jl_task_linked_to_original_autoreply__c = true;
				}
			}
		}
	}
	
	/**
	 * Reset counter fields that are build from child tasks prior to destructive rebuild performed by the
	 * updateLinkedCases method.
	 * @params:	Case thisCase	The case for which counter fields should be reset prior to rebuild.
	 * @rtnval:	void
	 */
	private static void resetParentCaseCounters(Case thisCase) {
		thisCase.jl_OutstandingTaskOwners__c = null;
		thisCase.jl_NumberOutstandingTasks__c = 0;
		thisCase.jl_Outstanding_Team_Tasks__c = null;
		thisCase.Number_of_Call_Contacts__c = 0;
		thisCase.Last_Call_Contact__c = null;
		thisCase.EH_Active_Subsequent_Exception__c = false; // <<010>>
		thisCase.Inbound_Emails_Not_Handled__c = false;
	}
    
    /**
     * Method for use in test classes where tasks may be updated multiple times
     * @params: None
     * @rtnval: void
     */
    @TestVisible
    private static void resetTaskTriggerStaticVariables() {
		COMMENTS_INSERT_FIRST_RUN = true;
		linkedCaseMap.clear();
		processedTaskIdSet.clear();
		processedCaseIdSet.clear();
        emailTaskIds.clear();
    }
    
    /**
     * Private class for use in Event Hub task counter map.
     */
    private class EHTaskCounter {
    	Integer ehTotalTasksCount = 0;
		Integer ehOpenTasksCount = 0;
    }
        
	/**
	 * Function to Calculate the String of Unique Task Owners as Comma Seperated Values.      
	 * @params: Set<String> Owners : A Set containing all the unique task owners of the case
	 * @rtnval: String : Comma Seperated String of task Owners limited to 255 Characters.
	 */
	private static String getUniqueTaskOwnerAsCommaSeperated(Set<String> allOwners) {
		String outstandingTaskOwners = '';
		String checkOwnerSize = '';
		for (String owner : allOwners) {
			checkOwnersize= checkOwnerSize + owner + ',';
			// Limiting the Number of Allowed values to 255 Characters.
			if (checkOwnersize.length() <= 255) {
				outstandingTaskOwners = outstandingTaskOwners+owner + ',';
			}
		}
		if (outstandingTaskOwners.length() > 1) {
			outstandingTaskOwners = outstandingTaskOwners.substring(0, outstandingTaskOwners.length()-1);
		}
		return outstandingTaskOwners;
	}
	
	public static Database.DMLOptions getDMLOptions(){
	    Database.DMLOptions opts = new Database.DMLOptions();
        opts.EmailHeader.triggerAutoResponseEmail = true;
        opts.EmailHeader.triggerOtherEmail = true;
        opts.EmailHeader.triggerUserEmail = true;
        opts.AllowFieldTruncation = true;
        return opts;
	}
	
	public static boolean isEmailTask(Task t){
		return (t != null && t.Subject.startsWith(EMAIL_TASK_SUBJECT_PREFIX));
	}
	 
	public static String getCaseCommentBody(Task taskObj, Case caseObj){
		String description =  null;
		
		if(isEmailTask(taskObj)) {
			description = EMAIL_COMMENT_DESCRIPTION + '\n TASK URL ' +  URL.getSalesforceBaseUrl().toExternalForm() + '/' + taskObj.Id;
		} else {
			description = ('Task Description ' + taskObj.description);
		}
		
		return ('Task status change for related task ' + taskObj.Subject + 
								 '\n\n Case: ' + caseObj.CaseNumber +
								 '\n\n Task status: ' + taskObj.Status +
								 '\n\n Case URL: ' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + caseObj.Id +  
								 '\n ' + description);
	} 
	public static void increaseNumberOfRepeatContacts(Case thisCase){
		if (thisCase.Number_of_Repeat_Contacts__c == null || thisCase.Number_of_Repeat_Contacts__c < 99){
			if(thisCase.Number_of_Repeat_Contacts__c == null){
				thisCase.Number_of_Repeat_Contacts__c = 1;
			} 
			else {
				thisCase.Number_of_Repeat_Contacts__c ++;
			}
        }
        INCREASE_REPEAT_CONTACT = true;
	}
}