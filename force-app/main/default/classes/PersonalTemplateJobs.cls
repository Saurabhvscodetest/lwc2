public without sharing class PersonalTemplateJobs 
{			
	private static final String UNRESTRICTED_PROFILE = 'JL: Unrestricted Profile (5 Users Max)';
	private static final String IT_OPERATION_ADMINISTRATOR = 'JL: IT Operations Administrator';
	public static void SweepPersonalTemplates()
	{		 		
		List<User> adminUsers = [SELECT Id 
								 FROM User 
		   						 WHERE profile.Name = :UNRESTRICTED_PROFILE OR profile.Name = :IT_OPERATION_ADMINISTRATOR ];
		
		List<EmailTemplate> newlist = [SELECT Id,OwnerId,Name,FolderId
                                       FROM EmailTemplate 
									   WHERE CreatedDate = LAST_N_DAYS:1 AND OwnerId NOT in :adminUsers ];
		
		
		List<EmailTemplate> FilteredList = new List<EmailTemplate>();
		set<Id> userIds = new set<Id>();		    
    	Map<Id,Folder> publicFolderCollection = new Map<Id,Folder>([select id, Name from Folder]);    	    	
		for(EmailTemplate item : newList)
		{  		    			
			if(!publicFolderCollection.containsKey(item.FolderId))
			{
				userIds.add(item.OwnerId);
				FilteredList.add(item);
			}	     		  	
		}
		Map<string,PersonalTemplateSettings__c> config = PersonalTemplateSettings__c.getAll();
		if(userIds.size() > 0)
		{
			List<User> userCollection = [Select u.Id, u.ProfileId,u.Team__c,u.My_Team_Manager__r.Name, Name, u.Email From User u where Id IN : userIds];
          						
			set<Id> profileIds = new set<Id>();
			for(User userRec : userCollection)
			{
				if(!profileIds.contains(userRec.ProfileId))profileIds.add(userRec.ProfileId);							
			}  
			Map<Id,Profile> profileCollection = new Map<Id,Profile>([Select Id, Name From Profile where Id IN : profileIds]); 
			List<EmailTemplate> emailTemplateCollection = [select Id,Name,HtmlValue,Body from EmailTemplate where Name IN ('Personal Template Removal Direct','Personal Template Removal')];		
			EmailTemplate emailTemplate;
			EmailTemplate emailTemplateDirectToUser;
			
			for(EmailTemplate item : emailTemplateCollection)
			{
				if(item.Name == 'Personal Template Removal') emailTemplate = item;
				if(item.Name == 'Personal Template Removal Direct')emailTemplateDirectToUser = item;
			}
			
			//Perform the delete on the template collection			
			delete FilteredList;
												
			string emailAddress;	
			List<String> toAddresses = new List<String>();			
			String htmlBody;     	
 			String plainBody;
    		
			if(!profileCollection.isEmpty() && !config.isEmpty())//<<02>>
			{ 
				for(User user: userCollection)
				{	
                    PersonalTemplateSettings__c configRec;
					configRec = (profileCollection.get(user.ProfileId).Name.length() > 32) ? config.get(profileCollection.get(user.ProfileId).Name.substring(0,32)) : config.get(profileCollection.get(user.ProfileId).Name);					 
					if (Test.isRunningTest()) configRec = new PersonalTemplateSettings__c(FullName__c = 'JL: ContactCentre Tier2-3', IsCapita__c = false, Active__c = true, Type__c = 'Test', Name = 'Test');        

                    if(configRec != null)
					{
						if(configRec.Active__c )
						{						
							if(configRec.Type__c != null)
							{														
								plainBody = emailTemplate.Body;																					
								plainBody = plainBody.replace('{!User.Name}', user.Name);																																		
		    					plainBody = (User.Team__c != null) ? plainBody.replace('{!User.Team__c}', user.Team__c) : plainBody.replace('who is part of {!User.Team__c}', 'who has no assigned team');
		    					plainBody = (user.My_Team_Manager__r.Name != null) ? plainBody.replace('{!User.My_Team_Manager__c}', user.My_Team_Manager__r.Name) : plainBody.replace('and reports to {!User.My_Team_Manager__c}', 'and has no manager assigned to them');	    						    						    					
							}
							else
							{							
								plainBody = emailTemplateDirectToUser.Body;							
								plainBody = plainBody.replace('{!User.Name}', user.Name);							  
							}
																																							
							System.debug('configRec.Type__c: '+ configRec.Type__c);												
							System.debug('user.Email: '+ user.Email);																		
							if(!Test.isRunningTest()) emailAddress = (Config_Settings__c.getInstance(configRec.Type__c).Text_Value__c != null) ? Config_Settings__c.getInstance(configRec.Type__c).Text_Value__c : user.Email;
							System.debug('emailAddress: '+ emailAddress);  
							toAddresses.add(emailAddress);				
							Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
							System.debug('toAddresses: '+ toAddresses);
							mail.setToAddresses(toAddresses);
							mail.setReplyTo('salesforce@johnlewis.co.uk');																														
							mail.setPlainTextBody(plainBody);
							mail.setSenderDisplayName('Connex Administrator');				
							
							if(!Test.isRunningTest())Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
							toAddresses.clear();
						}												                
					}
				}					
			}																			
		}    	
	}
		 
}