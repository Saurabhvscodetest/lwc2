global class ScheduleBatchDelNALoyaltyAccount implements Schedulable{
   global void execute(SchedulableContext sc){
        BAT_DelNeverActivatedLoyaltyAccount obj = new BAT_DelNeverActivatedLoyaltyAccount();
        Database.executebatch(obj);
    }
   
}