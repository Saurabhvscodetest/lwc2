/**
 *	@author: Andrey Gavrikov (westbrook)
 *	@date: 2014-12-10 09:59:32 
 *	@description:
 *	    Test methods for clsContactTriggerHandler
 *	
 *	Version History :   
 *		
 */
@isTest
public class ContactTriggerHandlerTest  {
	/**
	 * MJL-1383
	 * check that when new cntact without an account is inserted system creates an Account automatically
	 */
	static testMethod void testContactInsert () {

		Account accExisting = new Account(Name = 'Test Existing');
		Database.insert(accExisting);

		final List<Contact> contacts = new List<Contact>{
			new Contact(FirstName = 'Test 1', LastName = 'Contact', Phone = '0123456'),
			new Contact(FirstName = 'Test With Existing Account', LastName = 'Contact', Phone = '000123456', AccountId = accExisting.Id),
			new Contact(FirstName = 'Test 2', LastName = 'Contact', Phone = '00123456')
		};

		Test.startTest();
		Database.insert(contacts);
		Test.stopTest();

		System.assertEquals(accExisting.Id, [select AccountId from Contact where FirstName = 'Test With Existing Account'].AccountId, 
				'Did not expect account of ths contact to change');

		final List<Contact> loadedContacts = [select FirstName, AccountId, Account.Phone, Account.Name from Contact 
											where FirstName <> 'Test With Existing Account' order by FirstName];
		//contact 0
		System.assertEquals('Test 1', loadedContacts[0].FirstName, 'Expected different contact here');
		System.assertNotEquals('0123456', loadedContacts[0].Account.Phone, 'Phone number must have been copied from contact');
		System.assertEquals(null, loadedContacts[0].AccountId, 'expected account to be created and assigned to contact' );
		//System.assert(loadedContacts[0].Account.Name.contains('Test 1'), 'Account name must have been a concatenation of contact First + Last names' );
		
		//contact 1
		System.assertEquals('Test 2', loadedContacts[1].FirstName, 'Expected different contact here');
		System.assertEquals(null, loadedContacts[1].AccountId, 'expected account to be created and assigned to contact' );
		//System.assert(loadedContacts[1].Account.Name.contains('Test 2'), 'Account name must have been a concatenation of contact First + Last names' );
	}
}