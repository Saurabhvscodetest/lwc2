/******************************************************************************
* @author       Matt Povey
* @date         03/08/2015
* @description  Batch class to migrate cases, profiles and tasks from superseded contacts
*
* EDIT RECORD
*
* LOG     DATE        Author	JIRA		COMMENT      
* 001     03/08/2015  MP		CMP-507		Original code
*
*******************************************************************************
*/
global class BatchPCDSupersededContactUpdate implements Database.Batchable<SObject>, Database.Stateful {
	private static final Id SUPERSEDED_RECORD_TYPE = [SELECT Id FROM RecordType WHERE SobjectType = 'Contact' AND DeveloperName = 'Superseded' LIMIT 1].Id;
	private Integer totalRecordCount = 0;
	
    /**
     * Start - select all contacts where the Superseded By SCV id has been set but we haven't linked this to
     * the superseding Salesforce record.
	 * @params:	Database.BatchableContext BC	Batchable Context from system.
     */
	global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([SELECT Id, Superseded_By_SCV_Id__c, Superseded_By_Customer__c, RecordTypeId
        								 FROM Contact
                                         WHERE Superseded_By_SCV_Id__c != null
                                         AND Superseded_By_Customer__c = null
                                         AND RecordTypeId != :SUPERSEDED_RECORD_TYPE]);
	}
	
    /**
     * Execute - simply set batch mode on handler and resave Contact list.
	 * @params:	Database.BatchableContext BC	Batchable Context from system.
	 *			List<Contact> conList			Batch of Contact records to be updated
     */
	global void execute(Database.BatchableContext BC, List<Contact> conList){
		PCDUpdateHandler.BATCH_MODE = true;
		PCDUpdateHandler.IS_RUNNING_PCD_UPDATE = true;
		totalRecordCount += conList.size();
		update conList;
	}	
	
    /**
     * Finish - send notification email.
	 * @params:	Database.BatchableContext BC	Batchable Context from system.
     */
    global void finish(Database.BatchableContext bc){
        EmailNotifier.sendNotificationEmail('BatchPCDSupersededContactUpdate COMPLETED', 'BatchPCDSupersededContactUpdate has completed.\n\nTotal Contacts updated: ' + totalRecordCount);
	}
}