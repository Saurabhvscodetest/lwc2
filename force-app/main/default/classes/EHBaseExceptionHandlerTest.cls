@isTest
public class EHBaseExceptionHandlerTest {
    
	@testSetup static void setup() {
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();        
        User runningUser = UserTestDataFactory.getRunningUser();        
        User contactCentreUser;        
        Contact con;
        Case cas;
        
        System.runAs(runningUser) {
            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
            con = CustomerTestDataFactory.createContact();
            insert con;
            List<Contact> conList = new List<Contact>();
            for(Integer i=0; i<10; i++) {
                con = CustomerTestDataFactory.createContact();
                conList.add(con);
            }
            insert conList;
        }
        System.runAs(contactCentreUser) {             
            cas = CaseTestDataFactory.createPSECase(con.Id);
            insert cas;
        }
    }
    
    @isTest static void getContactForAssigningCaseTest() { 
        List<Contact> conList = [ SELECT Id FROM Contact LIMIT 1 ];
        List<Case> caseList = [ SELECT Id, Contact_ID_List__c FROM Case LIMIT 1 ];
        Case cas = caseList[0];
        Test.startTest();
          EHBaseExceptionHandler.getContactForAssigningCase(conList, cas);
        Test.stopTest();
    }
    
    @isTest static void getContactForAssigningCaseTestMultipleContacts() { 
        List<Contact> conList = [ SELECT Id FROM Contact LIMIT 2 ];
        List<Case> caseList = [ SELECT Id, Contact_ID_List__c FROM Case LIMIT 1 ];
        Case cas = caseList[0];
        Test.startTest();
          EHBaseExceptionHandler.getContactForAssigningCase(conList, cas);
        Test.stopTest();
    }
}