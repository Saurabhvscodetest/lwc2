public with sharing class myJL_LoyaltyAccountHouseKeeping implements HouseKeeperBatch.Processor {
	private static Integer BATCH_SIZE = 200;//number of items in apex batch job scope

	public String getName() {
		return 'LoyaltyAccountBatch';
	}

	public Database.QueryLocator start(Database.BatchableContext bc) {
		Database.QueryLocator ql;

		// Get the custom setting
		Integer rowLimit = 0;
		if (Test.isRunningTest() == false) {
			MyJL_Settings__c cp = MyJL_Settings__c.getInstance();
			if (cp != null) {
				if (cp.Batch_Size__c != null) {
					rowLimit = (Integer)cp.Batch_Size__c;
				}
			}
		}
		
		//system.debug('rowLimit: '+rowLimit);
		if (rowLimit == 0) {
			ql = Database.getQueryLocator([SELECT Id, ShopperID__c, contact__c FROM Loyalty_Account__c WHERE adopted__c = false]);
		} else {
			ql = Database.getQueryLocator([SELECT Id, ShopperID__c, contact__c FROM Loyalty_Account__c WHERE adopted__c = false LIMIT :rowLimit]);
		}

		return ql;
		
	}

	public void execute(Database.BatchableContext bc, List<SObject> scope) {
		List<Loyalty_Account__c> las = (List<Loyalty_Account__c>) scope;
		
		Map<String, Loyalty_Account__c> shopper2LaMap = new Map<String, Loyalty_Account__c>();
		Set<String> shopperIdSet = new Set<String>();
		List<Loyalty_Account__c> updateList = new List<Loyalty_Account__c>();

		for (Loyalty_Account__c la:las) {
			if ((String.isNotBlank(la.ShopperID__c)) && (la.contact__c == null)) {
				shopperIdSet.add(la.ShopperId__c);
				shopper2LaMap.put(la.ShopperId__c, la);
				//system.debug('handle la: '+la);
			}
			la.adopted__c = true;
		}

		for (contact cp:[SELECT Id, Shopper_ID__c FROM contact WHERE Shopper_ID__c IN :shopperIdSet]) {
			if (String.isNotBlank(cp.Shopper_ID__c)) {
				Loyalty_Account__c la = (Loyalty_Account__c) shopper2LaMap.get(cp.Shopper_ID__c);
				if (la != null) {
					//system.debug('cpId: '+cp.Id+' la: '+la);
					la.contact__c = cp.Id;
					updateList.add(la);
				}
			}			
		}		
		if (las.isEmpty() == false) {
			update las;
			if (updateList.isEmpty() == false) {
				// send notifications
				//MyJL_LoyaltyAccountHandler.sendJoinOrLeaveNotification(BaseTriggerHandler.toIdSet(updateList));										
			}
		}
	}

	public void finish(Database.BatchableContext bc){ }

	public Integer getBatchSize() {
		return BATCH_SIZE;
	}
}