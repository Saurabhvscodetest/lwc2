@isTest
private class EntitlementConfigManagerTest {
    private static boolean ADD_BOLT = true;
    private static boolean ADD_OMNI = true;
    private static boolean DONT_ADD_BOLT = false;
    private static boolean DONT_ADD_OMNI = false;
    private static Id GENERIC_PROCESS_ID = EntitlementConfigManager.ENTITLEMENT_PROCESS_NAME_TO_ID_MAP.get(EntitlementConfigManager.GENERIC_ENTITLEMENT_NAME); 
    private static Id GENERIC_BOLT_PROCESS_ID = EntitlementConfigManager.ENTITLEMENT_PROCESS_NAME_TO_ID_MAP.get(EntitlementConfigManager.GENERIC_ENTITLEMENT_NAME + EntitlementConfigManager.BOLT_SUFFIX); 
    
    @isTest
    static void shouldContainRoutingConfig() {
        system.assert(EntitlementConfigManager.ROUTING_CONFIG != null, 'Queue Routing Config List Should not be null' );
        system.assert(EntitlementConfigManager.ROUTING_CONFIG.size() > 0, 'Queue Routing Config List Should not be empty' );
    }
    
    @isTest
    static void buildEntitlementsAndRelatedQueues_ShouldBuildEnititlementsWithRelatedQueues() {
        Integer RELATED_NO_OF_QUEUES = 3;
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        String QUEUE_NAME = UnitTestDataFactory.getQueueNameForTeamName(TEAM_NAME);
        String ROLE_NAME = UnitTestDataFactory.getRoleNameForGroupName(QUEUE_NAME);
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
        Team__c teamObj = Team__c.getInstance(TEAM_NAME);
        String WARNING_QUEUE_NAME = teamObj.Queue_Name__c +  '_' + EntitlementConfigManager.WARNING_QUEUE_SUFFIX;
        String FAILED_QUEUE_NAME = teamObj.Queue_Name__c +  '_' + EntitlementConfigManager.FAILED_QUEUE_SUFFIX;
        String ACTIONED_QUEUE_NAME = teamObj.Queue_Name__c +  '_'  + EntitlementConfigManager.ACTIONED_QUEUE_SUFFIX;
        
        BusinessHours expectedBusinessHours = CommonStaticUtils.getBusinessHours(UnitTestDataFactory.DEFAULT_LITERAL);
        Set<String> RELATED_QUEUE_NAMES = new Set<String>{WARNING_QUEUE_NAME, FAILED_QUEUE_NAME, ACTIONED_QUEUE_NAME};
        System.debug('ACtual Realted queue names= > ' + RELATED_QUEUE_NAMES );
        UnitTestDataFactory.createConfigSettings();
        System.assert(teamObj != null , 'New team should be created');
        List<QueueSobject> relatedQueueSobject = [SELECT Queue.Id, Queue.Name FROM QueueSobject WHERE SobjectType = 'Case' and Queue.Name IN: RELATED_QUEUE_NAMES];
        System.assert(relatedQueueSobject.isEmpty(), 'The related queues should not exist');
        
        //User Role that was created as part of the setup using the same Queue Name
        String autoAssignPersmissionSetName =  AutoAssignPermissionSetHelper.getAutoAssignPermissionSetName(ROLE_NAME);
        List<Auto_Assign_Permission_Set__c> autoAssignPermissionSetList = [Select Id , Name, Permission_Sets_To_Assign__c  from Auto_Assign_Permission_Set__c where Name =:autoAssignPersmissionSetName LIMIT 1];
        system.assert(autoAssignPermissionSetList.isEmpty() , 'The should not be any autoassign permission set');
        
        Test.startTest();
            EntitlementConfigManager.buildEntitlementsAndRelatedQueues(new Set<String>{TEAM_NAME}, ADD_OMNI);
        Test.stopTest();
         
        Entitlement entitlementObj =  [SELECT Id, Name, SlaProcessId, BusinessHoursId FROM Entitlement WHERE Name =:TEAM_NAME LIMIT 1];
        System.assert(entitlementObj != null, 'Entitlements must be created');
        System.assertEquals(GENERIC_PROCESS_ID, entitlementObj.SlaProcessId, 'Entitlement process must be associated');
        //Since the team used in this test is set up with default business hours
        System.assertEquals(expectedBusinessHours.Id, entitlementObj.BusinessHoursId, 'Should have set the business hours in Entitlement');
        
        relatedQueueSobject = [SELECT Queue.Id, Queue.Name, Queue.DeveloperName FROM QueueSobject WHERE SobjectType = 'Case' and Queue.DeveloperName IN: RELATED_QUEUE_NAMES];
        //System.assertNotEquals(RELATED_NO_OF_QUEUES , relatedQueueSobject.size(), 'The related queues should be created' + relatedQueueSobject);
        
        Group defaultQueueGroup = [select id, Name, (select id, UserOrGroupId  from GroupMembers) from Group where Name  =: QUEUE_NAME LIMIT 1];
        Set<String> relatedQueueNames = new Set<String>();
        for(QueueSobject qSObj:relatedQueueSobject) {
            relatedQueueNames.add(qSObj.Queue.Name);
        }
        
        List<Group> relatedGroupList = new List<Group>([select id, Name,QueueRoutingConfigId, Email, DoesSendEmailToMembers , (select id, UserOrGroupId  from GroupMembers) from Group where Name  IN : relatedQueueNames]);
        Set<String> queueOwnerFilter = QueueOwnerFilter__c.getAll().keyset();
        for(Group groupObj : relatedGroupList){
            System.assertEquals(defaultQueueGroup.GroupMembers.size(),groupObj.GroupMembers.size(), 'Should have one member for all realted queues');
            String queueType = null;
            if(groupObj.Name.endsWithIgnoreCase(EntitlementConfigManager.WARNING_QUEUE_SUFFIX)){
                queueType = EntitlementConfigManager.WARNING_ROUTING_CONFIG;
            } else if(groupObj.Name.endsWithIgnoreCase(EntitlementConfigManager.FAILED_QUEUE_SUFFIX)){
                queueType = EntitlementConfigManager.FAILED_ROUTING_CONFIG;
            } 
            System.assertEquals(EntitlementConfigManager.ROUTING_CONFIG.get(queueType),groupObj.QueueRoutingConfigId,  'Should have populated the correct routing config');
            System.assert(queueOwnerFilter.contains(groupObj.name),  'Should add the realted queue name to the queue owner filter' +groupObj.name);
            System.assertEquals(EntitlementConfigManager.DEFAULT_QUEUE_EMAIL, groupObj.Email,  'Email Id must match');
            System.assertEquals(EntitlementConfigManager.SEND_EMAIL_TO_QUEUE_MEMBERS, groupObj.DoesSendEmailToMembers,  'Send email to memebers option must not be set');
            System.assertEquals(defaultQueueGroup.GroupMembers[0].UserOrGroupId, groupObj.GroupMembers[0].UserOrGroupId, 'Realted queue group members should be populated from the default queue');
        }
        
        //re-query the permission set list
        autoAssignPermissionSetList = [Select Id , Name, Permission_Sets_To_Assign__c  from Auto_Assign_Permission_Set__c where Name =:autoAssignPersmissionSetName LIMIT 1];
        system.assert(!autoAssignPermissionSetList.isEmpty() , 'The autoassign permission set should be created');
        system.assertEquals(AutoAssignPermissionSetHelper.CASE_SERVICE_PRESENCE_STATUS_PERMSET, autoAssignPermissionSetList[0].Permission_Sets_To_Assign__c , 'The permission set name should match');
    }
    
    
    @isTest
    static void buildEntitlementsAndRelatedQueues_ShouldTruncateTheQueueNameInTheQueueOwnerFilter() {
        Integer MAX_QUEUE_NAME_LENGTH = 40; 
        String DELIMITER = ' ';     
        Integer MAX_CUSTOM_SETTING_NAME_LENGTH = CommonStaticUtils.MAX_SEARCH_TEXT_LENGTH;
        //create the default queue name in such a way that the name of max length of any related queue name ( i.e ACTIONED queue is the longest name) is just about 40 characters
        // Choose ACTIONED queue since its got the longest suffix.
        String TEAM_NAME = (''.rightpad(MAX_QUEUE_NAME_LENGTH - (DELIMITER.length() + EntitlementConfigManager.ACTIONED_QUEUE_SUFFIX.length() + UnitTestDataFactory.QUEUE_SUFFIX.length()))).replace(' ', 'T');
        String QUEUE_NAME = UnitTestDataFactory.getQueueNameForTeamName(TEAM_NAME);
        String ACTIONED_QUEUE_NAME = QUEUE_NAME +  DELIMITER  + EntitlementConfigManager.ACTIONED_QUEUE_SUFFIX;
        String TRUNCATED_ACTIONED_QUEUE_NAME = ACTIONED_QUEUE_NAME.left(MAX_CUSTOM_SETTING_NAME_LENGTH);
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.CREATE_RELATED_QUEUES);
        Team__c teamObj = Team__c.getInstance(TEAM_NAME);
        
        Test.startTest();
            EntitlementConfigManager.buildEntitlementsAndRelatedQueues(new Set<String>{TEAM_NAME},  ADD_OMNI);
        Test.stopTest();
        System.assert(QueueOwnerFilter__c.getInstance(TRUNCATED_ACTIONED_QUEUE_NAME) != null, 'QueueName must be truncated in the custom setting' );
    }
    
    @isTest
    static void buildEntitlementsAndRelatedQueues_ShouldHandleTheScenarioWhen2TeamsReferToTheSameQueue() {
        Integer RELATED_NO_OF_QUEUES = 3;
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        String ANOTHER_TEAM_NAME = 'TESTTEAM2' + randomNo;
        String QUEUE_NAME = UnitTestDataFactory.getQueueNameForTeamName(TEAM_NAME);
        String ROLE_NAME = UnitTestDataFactory.getRoleNameForGroupName(QUEUE_NAME);
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
        Team__c teamObj = Team__c.getInstance(TEAM_NAME);
        String WARNING_QUEUE_NAME = teamObj.Queue_Name__c +  '_' + EntitlementConfigManager.WARNING_QUEUE_SUFFIX;
        String FAILED_QUEUE_NAME = teamObj.Queue_Name__c +  '_' + EntitlementConfigManager.FAILED_QUEUE_SUFFIX;
        String ACTIONED_QUEUE_NAME = teamObj.Queue_Name__c +  '_'  + EntitlementConfigManager.ACTIONED_QUEUE_SUFFIX;
        Team__c anotherTeamObj = new Team__c(Name = ANOTHER_TEAM_NAME, Contact_Centre__c = teamObj.Contact_Centre__c, Task_Assignee__c = 'null', Queue_Name__c = teamObj.Queue_Name__c);
        insert anotherTeamObj;
        
        BusinessHours expectedBusinessHours = CommonStaticUtils.getBusinessHours(UnitTestDataFactory.DEFAULT_LITERAL);
        
        Set<String> RELATED_QUEUE_NAMES = new Set<String>{WARNING_QUEUE_NAME, FAILED_QUEUE_NAME, ACTIONED_QUEUE_NAME};
        List<QueueSobject> relatedQueueSobject = [SELECT Queue.Id, Queue.Name FROM QueueSobject WHERE SobjectType = 'Case' and Queue.Name IN: RELATED_QUEUE_NAMES];
        System.assert(relatedQueueSobject.isEmpty(), 'The related queues should not exist');
        
        String autoAssignPersmissionSetName =  AutoAssignPermissionSetHelper.getAutoAssignPermissionSetName(ROLE_NAME);
        List<Auto_Assign_Permission_Set__c> autoAssignPermissionSetList = [Select Id , Name, Permission_Sets_To_Assign__c  from Auto_Assign_Permission_Set__c where Name =:autoAssignPersmissionSetName LIMIT 1];
        system.assert(autoAssignPermissionSetList.isEmpty() , 'The should not be any autoassign permission set');
        
        Test.startTest();
            EntitlementConfigManager.buildEntitlementsAndRelatedQueues(new Set<String>{TEAM_NAME,ANOTHER_TEAM_NAME }, ADD_OMNI);
        Test.stopTest();
         
        Entitlement entitlementObj =  [SELECT Id, Name, SlaProcessId, BusinessHoursId FROM Entitlement WHERE Name =:TEAM_NAME LIMIT 1];
        System.assert(entitlementObj != null, 'Entitlements must be created');
        System.assertEquals(GENERIC_PROCESS_ID, entitlementObj.SlaProcessId, 'Entitlement process must be associated');
        //Since the team used in this test is set up with default business hours
        System.assertEquals(expectedBusinessHours.Id, entitlementObj.BusinessHoursId, 'Should have set the business hours in Entitlement');
        
        relatedQueueSobject = [SELECT Queue.Id, Queue.Name, Queue.DeveloperName FROM QueueSobject WHERE SobjectType = 'Case' and Queue.DeveloperName IN: RELATED_QUEUE_NAMES];
        //System.assertNotEquals(RELATED_NO_OF_QUEUES , relatedQueueSobject.size(), 'The related queues should be created' + relatedQueueSobject);

        Entitlement entitlementObjForSecondTeam =  [SELECT Id, Name, SlaProcessId, BusinessHoursId FROM Entitlement WHERE Name =:ANOTHER_TEAM_NAME LIMIT 1];
        System.assert(entitlementObjForSecondTeam != null, 'Entitlements must be created');
        System.assertEquals(GENERIC_PROCESS_ID, entitlementObjForSecondTeam.SlaProcessId, 'Entitlement process must be associated');
        //Since the team used in this test is set up with default business hours
        System.assertEquals(expectedBusinessHours.Id, entitlementObjForSecondTeam.BusinessHoursId, 'Should have set the business hours in Entitlement');
        relatedQueueSobject = [SELECT Queue.Id, Queue.Name, Queue.DeveloperName FROM QueueSobject WHERE SobjectType = 'Case' and Queue.DeveloperName IN: RELATED_QUEUE_NAMES];
        //System.assertNotEquals(RELATED_NO_OF_QUEUES , relatedQueueSobject.size(), 'The related queues should be created' + relatedQueueSobject);
        
        Group defaultQueueGroup = [select id, Name, (select id, UserOrGroupId  from GroupMembers) from Group where Name  =: QUEUE_NAME LIMIT 1];
        Set<String> relatedQueueNames = new Set<String>();
        for(QueueSobject qSObj:relatedQueueSobject) {
            relatedQueueNames.add(qSObj.Queue.Name);
        }
        
        List<Group> relatedGroupList = new List<Group>([select id, Name,QueueRoutingConfigId, Email, DoesSendEmailToMembers , (select id, UserOrGroupId  from GroupMembers) from Group where Name  IN : relatedQueueNames]);
        Set<String> queueOwnerFilter = QueueOwnerFilter__c.getAll().keyset();
        for(Group groupObj : relatedGroupList){
            System.assertEquals(defaultQueueGroup.GroupMembers.size(),groupObj.GroupMembers.size(), 'Should have one member for all realted queues');
            String queueType = null;
            if(groupObj.Name.endsWithIgnoreCase(EntitlementConfigManager.WARNING_QUEUE_SUFFIX)){
                queueType = EntitlementConfigManager.WARNING_ROUTING_CONFIG;
            } else if(groupObj.Name.endsWithIgnoreCase(EntitlementConfigManager.FAILED_QUEUE_SUFFIX)){
                queueType = EntitlementConfigManager.FAILED_ROUTING_CONFIG;
            } 
            System.assertEquals(EntitlementConfigManager.ROUTING_CONFIG.get(queueType),groupObj.QueueRoutingConfigId,  'Should have populated the correct routing config');
            System.assert(queueOwnerFilter.contains(groupObj.name),  'Should add the realted queue name to the queue owner filter' +groupObj.name);
            System.assertEquals(EntitlementConfigManager.DEFAULT_QUEUE_EMAIL, groupObj.Email,  'Email Id must match');
            System.assertEquals(EntitlementConfigManager.SEND_EMAIL_TO_QUEUE_MEMBERS, groupObj.DoesSendEmailToMembers,  'Send email to memebers option must not be set');
            System.assertEquals(defaultQueueGroup.GroupMembers[0].UserOrGroupId, groupObj.GroupMembers[0].UserOrGroupId, 'Realted queue group members should be populated from the default queue');
        }
        
        //re-query the permission set list
        autoAssignPermissionSetList = [Select Id , Name, Permission_Sets_To_Assign__c  from Auto_Assign_Permission_Set__c where Name =:autoAssignPersmissionSetName LIMIT 1];
        system.assert(!autoAssignPermissionSetList.isEmpty() , 'The autoassign permission set should be created');
        system.assertEquals(AutoAssignPermissionSetHelper.CASE_SERVICE_PRESENCE_STATUS_PERMSET, autoAssignPermissionSetList[0].Permission_Sets_To_Assign__c , 'The permission set name should match');
    }
    
    
    @isTest
    static void buildEntitlementsAndRelatedQueues_ShouldUpdateExistingRelatedQueues() {
        Integer RELATED_NO_OF_QUEUES = 3;
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        String QUEUE_NAME = UnitTestDataFactory.getQueueNameForTeamName(TEAM_NAME);
        String ROLE_NAME = UnitTestDataFactory.getRoleNameForGroupName(QUEUE_NAME);
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.CREATE_RELATED_QUEUES);
        Team__c teamObj = Team__c.getInstance(TEAM_NAME);
        String WARNING_QUEUE_NAME = QUEUE_NAME +  ' ' + EntitlementConfigManager.WARNING_QUEUE_SUFFIX;
        String FAILED_QUEUE_NAME = QUEUE_NAME +  ' ' + EntitlementConfigManager.FAILED_QUEUE_SUFFIX;
        String ACTIONED_QUEUE_NAME = QUEUE_NAME +  ' '  + EntitlementConfigManager.ACTIONED_QUEUE_SUFFIX;
        
        String autoAssignPersmissionSetName =  AutoAssignPermissionSetHelper.getAutoAssignPermissionSetName(ROLE_NAME);
        Set<String> RELATED_QUEUE_NAMES = new Set<String>{WARNING_QUEUE_NAME, FAILED_QUEUE_NAME, ACTIONED_QUEUE_NAME};
        
        List<QueueSobject> relatedQueueSobject = [SELECT Queue.Id, Queue.Name FROM QueueSobject WHERE SobjectType = 'Case' and Queue.Name IN: RELATED_QUEUE_NAMES];
        System.assert(!relatedQueueSobject.isEmpty(), 'The related queues should not be empty :' + RELATED_QUEUE_NAMES);
        //System.assertNotEquals( RELATED_QUEUE_NAMES.size(),relatedQueueSobject.size(), 'The related queues should all be created :' +RELATED_QUEUE_NAMES);
        
        List<Auto_Assign_Permission_Set__c> autoAssignPermissionSetList = [Select Id , Name, Permission_Sets_To_Assign__c  from Auto_Assign_Permission_Set__c where Name =:autoAssignPersmissionSetName LIMIT 1];
        system.assert(autoAssignPermissionSetList.isEmpty() , 'The should not be any autoassign permission set');
    
        Test.startTest();
            EntitlementConfigManager.buildEntitlementsAndRelatedQueues(new Set<String>{TEAM_NAME}, ADD_OMNI);
        Test.stopTest();
         
        relatedQueueSobject = [SELECT Queue.Id, Queue.Name, Queue.DeveloperName FROM QueueSobject WHERE SobjectType = 'Case' and Queue.DeveloperName IN: RELATED_QUEUE_NAMES];
        
        Group defaultQueueGroup = [select id, Name, (select id, UserOrGroupId  from GroupMembers) from Group where Name  =: QUEUE_NAME LIMIT 1];
        
        Set<String> relatedQueueNames = new Set<String>();
        for(QueueSobject qSObj:relatedQueueSobject) {
            relatedQueueNames.add(qSObj.Queue.Name);
        }
        //Requery the related groups
        List<Group> relatedGroupList = new List<Group>([select id, Name,QueueRoutingConfigId, Email, DoesSendEmailToMembers , (select id, UserOrGroupId  from GroupMembers) from Group where Name  IN : relatedQueueNames]);
        Set<String> queueOwnerFilter = QueueOwnerFilter__c.getAll().keyset();
        for(Group groupObj : relatedGroupList){
            System.assertEquals(defaultQueueGroup.GroupMembers.size(),groupObj.GroupMembers.size(), 'Should have one member for all realted queues');
            String queueType = null;
            if(groupObj.Name.endsWithIgnoreCase(EntitlementConfigManager.WARNING_QUEUE_SUFFIX)){
                queueType = EntitlementConfigManager.WARNING_ROUTING_CONFIG;
            } else if(groupObj.Name.endsWithIgnoreCase(EntitlementConfigManager.FAILED_QUEUE_SUFFIX)){
                queueType = EntitlementConfigManager.FAILED_ROUTING_CONFIG;
            } 
            System.assertEquals(EntitlementConfigManager.ROUTING_CONFIG.get(queueType),groupObj.QueueRoutingConfigId,  'Should have populated the correct routing config');
            System.assertEquals(EntitlementConfigManager.DEFAULT_QUEUE_EMAIL, groupObj.Email,  'Email Id must match for the queueType ');
            System.assertEquals(EntitlementConfigManager.SEND_EMAIL_TO_QUEUE_MEMBERS, groupObj.DoesSendEmailToMembers,  'Send email to memebers option must not be set');
            System.assertEquals(defaultQueueGroup.GroupMembers[0].UserOrGroupId, groupObj.GroupMembers[0].UserOrGroupId, 'Realted queue group members should be populated from the default queue');
        }
        
        //re-query the permission set list
        autoAssignPermissionSetList = [Select Id , Name, Permission_Sets_To_Assign__c  from Auto_Assign_Permission_Set__c where Name =:autoAssignPersmissionSetName LIMIT 1];
        system.assert(!autoAssignPermissionSetList.isEmpty() , 'The autoassign permission set should be created');
        system.assertEquals(AutoAssignPermissionSetHelper.CASE_SERVICE_PRESENCE_STATUS_PERMSET, autoAssignPermissionSetList[0].Permission_Sets_To_Assign__c , 'The permission set name should match');
    }
    
    @isTest
    static void buildEntitlementsAndRelatedQueues_ShouldUpdateExistingAutoAssignPermissionSet() {
        Integer RELATED_NO_OF_QUEUES = 3;
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        String ANOTHER_TEAM_NAME = 'TESTTEAM2' + randomNo;
        String QUEUE_NAME = UnitTestDataFactory.getQueueNameForTeamName(TEAM_NAME);
        String ROLE_NAME = UnitTestDataFactory.getRoleNameForGroupName(QUEUE_NAME);
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
        Team__c teamObj = Team__c.getInstance(TEAM_NAME);
        String WARNING_QUEUE_NAME = teamObj.Queue_Name__c +  '_' + EntitlementConfigManager.WARNING_QUEUE_SUFFIX;
        String FAILED_QUEUE_NAME = teamObj.Queue_Name__c +  '_' + EntitlementConfigManager.FAILED_QUEUE_SUFFIX;
        String ACTIONED_QUEUE_NAME = teamObj.Queue_Name__c +  '_'  + EntitlementConfigManager.ACTIONED_QUEUE_SUFFIX;
        
        //User Role that was created as part of the setup using the same team Name
        Auto_Assign_Permission_Set__c aap  = new Auto_Assign_Permission_Set__c(Name = AutoAssignPermissionSetHelper.getAutoAssignPermissionSetName(ROLE_NAME));
        insert aap;
        
        Test.startTest();
            EntitlementConfigManager.buildEntitlementsAndRelatedQueues(new Set<String>{TEAM_NAME}, ADD_OMNI);
        Test.stopTest();
         
        //re-query the permission set list
        List<Auto_Assign_Permission_Set__c> autoAssignPermissionSetList = [Select Id , Name, Permission_Sets_To_Assign__c  from Auto_Assign_Permission_Set__c where Name =:aap.Name LIMIT 1];
        system.assertEquals(AutoAssignPermissionSetHelper.CASE_SERVICE_PRESENCE_STATUS_PERMSET, autoAssignPermissionSetList[0].Permission_Sets_To_Assign__c , 'The permission set name should have been updated');
    }

    @isTest
    static void buildEntitlementsAndRelatedQueues_ShouldUpdateTheDefaultQueueWithCorrectDetails() {
        Integer RELATED_NO_OF_QUEUES = 3;
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        String ANOTHER_TEAM_NAME = 'TESTTEAM2' + randomNo;
        String QUEUE_NAME = UnitTestDataFactory.getQueueNameForTeamName(TEAM_NAME);
        String ROLE_NAME = UnitTestDataFactory.getRoleNameForGroupName(QUEUE_NAME);
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
        Team__c teamObj = Team__c.getInstance(TEAM_NAME);
        String WARNING_QUEUE_NAME = teamObj.Queue_Name__c +  '_' + EntitlementConfigManager.WARNING_QUEUE_SUFFIX;
        String FAILED_QUEUE_NAME = teamObj.Queue_Name__c +  '_' + EntitlementConfigManager.FAILED_QUEUE_SUFFIX;
        String ACTIONED_QUEUE_NAME = teamObj.Queue_Name__c +  '_'  + EntitlementConfigManager.ACTIONED_QUEUE_SUFFIX;
        System.assert(teamObj != null , 'New team should be created');
        
        //Since the default queue is not set with DoesSendEmailToMembers, and Email field should not be set 
        Group defaultQueueGroup = [select id, Name,QueueRoutingConfigId, Email,DoesSendEmailToMembers from Group where Name  =: QUEUE_NAME LIMIT 1];
        System.assert(defaultQueueGroup.Email == null, 'Default Group Email should not be set ');        
        System.assertEquals(false, defaultQueueGroup.DoesSendEmailToMembers , 'Send Email on group should NOT be enabled');        
        
        Test.startTest();
            EntitlementConfigManager.buildEntitlementsAndRelatedQueues(new Set<String>{TEAM_NAME},  ADD_OMNI);
        Test.stopTest();
        
        //Requery the default group
        defaultQueueGroup = [select id, Name,QueueRoutingConfigId, Email,DoesSendEmailToMembers from Group where Name  =: QUEUE_NAME LIMIT 1];
        System.assert(defaultQueueGroup.Email != null, 'Default Group Email should be set ');        
        System.assertEquals(false, defaultQueueGroup.DoesSendEmailToMembers , 'Send Email on group should be enabled');        
    }
    
    
    @isTest
    static void buildEntitlementsAndRelatedQueues_ShouldNotCreateTheBOLTEntitlements() {
        Integer RELATED_NO_OF_QUEUES = 3;
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        String ANOTHER_TEAM_NAME = 'TESTTEAM2' + randomNo;
        String QUEUE_NAME = UnitTestDataFactory.getQueueNameForTeamName(TEAM_NAME);
        String ROLE_NAME = UnitTestDataFactory.getRoleNameForGroupName(QUEUE_NAME);
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
        Team__c teamObj = Team__c.getInstance(TEAM_NAME);
        String WARNING_QUEUE_NAME = teamObj.Queue_Name__c +  '_' + EntitlementConfigManager.WARNING_QUEUE_SUFFIX;
        String FAILED_QUEUE_NAME = teamObj.Queue_Name__c +  '_' + EntitlementConfigManager.FAILED_QUEUE_SUFFIX;
        String ACTIONED_QUEUE_NAME = teamObj.Queue_Name__c +  '_'  + EntitlementConfigManager.ACTIONED_QUEUE_SUFFIX;
        
        Set<String> RELATED_QUEUE_NAMES = new Set<String>{WARNING_QUEUE_NAME, FAILED_QUEUE_NAME, ACTIONED_QUEUE_NAME};
        System.debug('ACtual Realted queue names= > ' + RELATED_QUEUE_NAMES );
        System.assert(teamObj != null , 'New team should be created');
        List<QueueSobject> relatedQueueSobject = [SELECT Queue.Id, Queue.Name FROM QueueSobject WHERE SobjectType = 'Case' and Queue.Name IN: RELATED_QUEUE_NAMES];
        System.assert(relatedQueueSobject.isEmpty(), 'The related queues should not exist');
         
        Test.startTest();
            EntitlementConfigManager.buildEntitlementsAndRelatedQueues(new Map<String, Team__c>{TEAM_NAME => teamObj}, ADD_OMNI);
        Test.stopTest();
         
        Entitlement entitlementObj =  [SELECT Id, Name, SlaProcessId FROM Entitlement WHERE Name =:TEAM_NAME LIMIT 1];
        System.assert(entitlementObj != null, 'Entitlements must be created');
        List<Entitlement> boltEntitlementList =  new List<Entitlement>([SELECT Id, Name, SlaProcessId FROM Entitlement WHERE Name =: (TEAM_NAME + EntitlementConfigManager.BOLT_SUFFIX) LIMIT 1]);
        System.assert(boltEntitlementList.isEmpty(), 'BOLT Entitlements must NOt be created');
    }
    
    
    @isTest
    static void buildEntitlementsAndRelatedQueues_ShouldBuildEnititlementsWithoutRelatedQueues() {
        Integer RELATED_NO_OF_QUEUES = 3;
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        String ANOTHER_TEAM_NAME = 'TESTTEAM2' + randomNo;
        String QUEUE_NAME = UnitTestDataFactory.getQueueNameForTeamName(TEAM_NAME);
        String ROLE_NAME = UnitTestDataFactory.getRoleNameForGroupName(QUEUE_NAME);
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
        Team__c teamObj = Team__c.getInstance(TEAM_NAME);
        String WARNING_QUEUE_NAME = teamObj.Queue_Name__c +  '_' + EntitlementConfigManager.WARNING_QUEUE_SUFFIX;
        String FAILED_QUEUE_NAME = teamObj.Queue_Name__c +  '_' + EntitlementConfigManager.FAILED_QUEUE_SUFFIX;
        String ACTIONED_QUEUE_NAME = teamObj.Queue_Name__c +  '_'  + EntitlementConfigManager.ACTIONED_QUEUE_SUFFIX;
        
        Set<String> RELATED_QUEUE_NAMES = new Set<String>{WARNING_QUEUE_NAME, FAILED_QUEUE_NAME, ACTIONED_QUEUE_NAME};
        System.debug('ACtual Realted queue names= > ' + RELATED_QUEUE_NAMES );
        System.assert(teamObj != null , 'New team should be created');
        List<QueueSobject> relatedQueueSobject = [SELECT Queue.Id, Queue.Name FROM QueueSobject WHERE SobjectType = 'Case' and Queue.Name IN: RELATED_QUEUE_NAMES];
        System.assert(relatedQueueSobject.isEmpty(), 'The related queues should not exist');
         
        Test.startTest();
            EntitlementConfigManager.buildEntitlementsAndRelatedQueues(new Map<String, Team__c>{TEAM_NAME => teamObj}, DONT_ADD_OMNI);
        Test.stopTest();
        
        Entitlement entitlementObj =  [SELECT Id, Name, SlaProcessId FROM Entitlement WHERE Name =:TEAM_NAME LIMIT 1]; 
        System.assert(entitlementObj != null, 'Entitlements must be created');
        System.assert(entitlementObj.SlaProcessId != null, 'Entitlement process must be associated');
        relatedQueueSobject = [SELECT Queue.Id, Queue.Name, Queue.DeveloperName FROM QueueSobject WHERE SobjectType = 'Case' and Queue.DeveloperName IN: RELATED_QUEUE_NAMES];
        System.assert(relatedQueueSobject.isEmpty(), 'The related queues should NOT be created');
    }
    
    
    @isTest
    static void updateEntitlements() {
        Integer RELATED_NO_OF_QUEUES = 3;
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        String ANOTHER_TEAM_NAME = 'TESTTEAM2' + randomNo;
        String QUEUE_NAME = UnitTestDataFactory.getQueueNameForTeamName(TEAM_NAME);
        String ROLE_NAME = UnitTestDataFactory.getRoleNameForGroupName(QUEUE_NAME);
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
        Team__c teamObj = Team__c.getInstance(TEAM_NAME);
        String WARNING_QUEUE_NAME = teamObj.Queue_Name__c +  '_' + EntitlementConfigManager.WARNING_QUEUE_SUFFIX;
        String FAILED_QUEUE_NAME = teamObj.Queue_Name__c +  '_' + EntitlementConfigManager.FAILED_QUEUE_SUFFIX;
        String ACTIONED_QUEUE_NAME = teamObj.Queue_Name__c +  '_'  + EntitlementConfigManager.ACTIONED_QUEUE_SUFFIX;
        system.assert(teamObj!= null, 'TEAM Custom setting should exists');
        
        //create existing accounts
        Account accountObj = new Account(name = TEAM_NAME );
        insert accountObj;
        //Create Existing Entitlements , Explicitly set the Entitlement process ID to null
        Entitlement normalEntitlement = new Entitlement(Name = TEAM_NAME, Type = EntitlementConfigManager.PHONE_SUPPORT, AccountId = accountObj.Id, SlaProcessId= null);
        Entitlement boltEntitlement = new Entitlement(Name = (TEAM_NAME + EntitlementConfigManager.BOLT_SUFFIX), Type = EntitlementConfigManager.PHONE_SUPPORT, AccountId = accountObj.Id, SlaProcessId = null);
        insert (new List<Entitlement>{normalEntitlement, boltEntitlement});
        
       // UnitTestDataFactory.createConfigSettings();
        Set<String> ENTITLEMENT_NAMES = new Set<String>{normalEntitlement.Name, boltEntitlement.Name};
         
        Test.startTest();
            EntitlementConfigManager.updateEntitlements(ENTITLEMENT_NAMES);
        Test.stopTest();
        
        Entitlement entitlementObj =  [SELECT Id, Name, SlaProcessId FROM Entitlement WHERE Name =:TEAM_NAME LIMIT 1]; 
        System.assert(entitlementObj.SlaProcessId != null, 'Entitlement process must be associated');
        Entitlement boltEntitlementObj =  [SELECT Id, Name, SlaProcessId FROM Entitlement WHERE Name =: (TEAM_NAME + EntitlementConfigManager.BOLT_SUFFIX) LIMIT 1];
     }
    
    @isTest
    static void updateUsersToAssignRelatedQueues() {

        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        String QUEUE_NAME = UnitTestDataFactory.getQueueNameForTeamName(TEAM_NAME);
        String ROLE_NAME = UnitTestDataFactory.getRoleNameForGroupName(QUEUE_NAME);
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
        Team__c teamObj = Team__c.getInstance(TEAM_NAME);
        String WARNING_QUEUE_NAME = teamObj.Queue_Name__c +  '_' + EntitlementConfigManager.WARNING_QUEUE_SUFFIX;
        String FAILED_QUEUE_NAME = teamObj.Queue_Name__c +  '_' + EntitlementConfigManager.FAILED_QUEUE_SUFFIX;
        String ACTIONED_QUEUE_NAME = teamObj.Queue_Name__c +  '_'  + EntitlementConfigManager.ACTIONED_QUEUE_SUFFIX;
        
        BusinessHours expectedBusinessHours = CommonStaticUtils.getBusinessHours(UnitTestDataFactory.DEFAULT_LITERAL);
        Set<String> RELATED_QUEUE_NAMES = new Set<String>{WARNING_QUEUE_NAME, FAILED_QUEUE_NAME, ACTIONED_QUEUE_NAME};
        UnitTestDataFactory.createConfigSettings();
        List<QueueSobject> relatedQueueSobject = [SELECT Queue.Id, Queue.Name FROM QueueSobject WHERE SobjectType = 'Case' and Queue.Name IN: RELATED_QUEUE_NAMES];

        EntitlementConfigManager.buildEntitlementsAndRelatedQueues(new Set<String>{TEAM_NAME}, ADD_OMNI);

        Group defaultQueueGroup = [select id, Name, (select id, UserOrGroupId  from GroupMembers) from Group where Name  =: QUEUE_NAME LIMIT 1];
        Set<String> relatedQueueNames = new Set<String>();
        for(QueueSobject qSObj:relatedQueueSobject) {
            relatedQueueNames.add(qSObj.Queue.Name);
        }
        
        List<Group> relatedGroupList = new List<Group>([select id, Name,QueueRoutingConfigId, Email, DoesSendEmailToMembers , (select id, UserOrGroupId  from GroupMembers) from Group where Name  IN : relatedQueueNames]);
        Set<String> queueOwnerFilter = QueueOwnerFilter__c.getAll().keyset();
        for(Group groupObj : relatedGroupList){
            String queueType = null;
            if(groupObj.Name.endsWithIgnoreCase(EntitlementConfigManager.WARNING_QUEUE_SUFFIX)){
                queueType = EntitlementConfigManager.WARNING_ROUTING_CONFIG;
            } else if(groupObj.Name.endsWithIgnoreCase(EntitlementConfigManager.FAILED_QUEUE_SUFFIX)){
                queueType = EntitlementConfigManager.FAILED_ROUTING_CONFIG;
            } 
        }

        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        System.runAs(runningUser) { 
            UserRole r = new UserRole(Name = ROLE_NAME);
            insert r;
            UserRole role = [Select Id, Name From UserRole Where Name = :r.Name limit 1 ];

            User u = UnitTestDataFactory.getBackOfficeUser('testUser', TEAM_NAME);
            u.userRoleId= role.Id;
            insert u;
        
            User userPre = [SELECT Id, Queues__c FROM User WHERE Id = :u.Id];

            Set<Id> roleIds = new Map<Id, UserRole>([SELECT Id FROM UserRole WHERE Name = :r.Name]).keySet();

            Test.startTest();
                EntitlementConfigManager.assignRelatedQueuesToUsers(roleIds);
            Test.stopTest();

            User userPost = [SELECT Id, Queues__c FROM User WHERE Id = :u.Id];

            system.assertNotEquals(userPre.Queues__c, userPost.Queues__c, 'Queues should not match - related queues should be present post update');
        }
    }  
    
}