/*************************************************
coEmailContentController_TEST

test class for the coEmailContentController controller

Author: Steven Loftus (MakePositive)
Created Date: 19/11/2014
Modification Date: 
Modified By: 

**************************************************/
@isTest
private class coEmailContentController_TEST {
    
    public static testmethod void getEmailContent() {
        //UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        Config_Settings__c configSearch = new Config_Settings__c();
        configSearch.Name = CustomSettingsManager.WS_Endpoint_SearchCustomer;
        configSearch.Text_Value__c = 'SearchCustomer';
        insert configSearch;

        Config_Settings__c configOrders = new Config_Settings__c();
        configOrders.Name = CustomSettingsManager.WS_Endpoint_ViewCustomerOrdersV1;
        configOrders.Text_Value__c = 'ViewCustomerOrdersV1';
        insert configOrders;

        Config_Settings__c configEmail = new Config_Settings__c();
        configEmail.Name = CustomSettingsManager.WS_Endpoint_EmailContent;
        configEmail.Text_Value__c = 'EmailContent';
        insert configEmail;

        // get the page
        PageReference emailContentPage = Page.coEmailContent;
        Test.setCurrentPage(emailContentPage);

        // push the url params
        ApexPages.currentPage().getParameters().put('orderId', '12345678');
        ApexPages.currentPage().getParameters().put('emailQueueId', '2726');


        Test.setMock(HttpCalloutMock.class, new coSoapGetEmailContent_MOCK());

        Test.startTest();
        // create a controller
        coEmailContentController emailContentController;
        try {
        	emailContentController = new coEmailContentController();
        }
        catch(Exception e) {}
        Test.stopTest();
    }

    public static testmethod void getEmailContentResponseError() {

        Config_Settings__c configSearch = new Config_Settings__c();
        configSearch.Name = CustomSettingsManager.WS_Endpoint_SearchCustomer;
        configSearch.Text_Value__c = 'SearchCustomer';
        insert configSearch;

        Config_Settings__c configOrders = new Config_Settings__c();
        configOrders.Name = CustomSettingsManager.WS_Endpoint_ViewCustomerOrdersV1;
        configOrders.Text_Value__c = 'ViewCustomerOrdersV1';
        insert configOrders;

        Config_Settings__c configEmail = new Config_Settings__c();
        configEmail.Name = CustomSettingsManager.WS_Endpoint_EmailContent;
        configEmail.Text_Value__c = 'EmailContentResponseError';
        insert configEmail;
        
        // get the page
        PageReference emailContentPage = Page.coEmailContent;
        Test.setCurrentPage(emailContentPage);

        // push the url params
        ApexPages.currentPage().getParameters().put('orderId', '12345678');
        ApexPages.currentPage().getParameters().put('emailQueueId', '2726');


        Test.setMock(HttpCalloutMock.class, new coSoapGetEmailContent_MOCK());

        /*Test.startTest();
        // create a controller       
        coEmailContentController emailContentController = new coEmailContentController();
        Test.stopTest();

        system.assertEquals('Error', emailContentController.SoapManager.Status, 'Not an error');*/

    }

}