/*
* @File Name   : BAT_UpdateCTellingDateOnContactTest
* @Description : Test class for the batch class used to fetch the contact records from the database based on the field - 
                 GDPR_Data_Deletion__c
* @Copyright   : Zensar
* @Jira #      : #4277
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0       16-April-19        Ragesh G                     Created
*/

@isTest
public class BAT_UpdateCTellingDateOnContactTest {
    
    static testMethod void testBatchExecutionWithPositiveCases () {
    
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        // Running user Context
        System.runAs(runningUser){
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;       
            insert CustomSettingTestDataFactory.createConfigSettings();
        
        List< Contact > contactRecordList = new List< Contact >();
        
        for(Integer counter =0; counter<30 ; counter++){
            contact testContact = CustomerTestDataFactory.createContact();
             testContact.Email = 'tester'+counter+'@gmail.com';
             testContact.Clienteling_Permission__c = ConstantValues.CLIENT_TELLING_PERMISSION;
             testContact.CT_Appointment_date__c = System.now().addYears (1).date();
             contactRecordList.add ( testContact);
            
             testContact = CustomerTestDataFactory.createContact();
             testContact.Email = 'tester'+counter+'@gmail.com';
             testContact.CT_Appointment_date__c = System.now().addYears (2).date();
             testContact.Clienteling_Permission__c = ConstantValues.CLIENT_TELLING_PERMISSION;
             contactRecordList.add ( testContact);
            
             testContact = CustomerTestDataFactory.createContact();
             testContact.Email = 'tester'+counter+'@gmail.com';
             testContact.CT_Appointment_date__c = null;
             testContact.Clienteling_Permission__c = ConstantValues.CLIENT_TELLING_PERMISSION;
             contactRecordList.add ( testContact);
        }
            Contact contactRec = new Contact ();
            contactRec.LastName = 'Test Name';
            insert contactRec;
            update contactRec;
            
             contact testContact = CustomerTestDataFactory.createContact();
             testContact.Email = 'testerNull'+'@gmail.com';
             testContact.Clienteling_Permission__c = ConstantValues.CLIENT_TELLING_PERMISSION;
             testContact.CT_Appointment_date__c = null;
             insert testContact;
             
            
        try {
            
            insert contactRecordList;
            
        } catch ( Exception ex ) {
            System.debug (' Error while insering the records ' + ex.getMessage ());
        }
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.Name = 'BatchUpdateCTellingDateOnContact';
        gDPR.GDPR_Class_Name__c = 'BatchUpdateCTellingDateOnContact';
        gDPR.Record_Limit__c = '5000';                
        try{
            Insert gDPR;
            Insert contactRecordList;
        }
        catch(Exception e){
            System.debug('Exception Caught:'+e.getmessage());
        }
        
        List<Task> Tasklist=new  List<Task>();
            for(integer i=0; i<10; i++){
                Tasklist.add(new Task(whoid=contactRecordList[0].id,Status='Open',Priority='Normal',Type = 'Call',Subject='Call',PRXC__Outreach_Delivered__c =  System.now().addYears(3)));
                Tasklist.add(new Task(whoid=contactRecordList[0].id,Status='Open',Priority='Normal',Type = 'Call',Subject='Call',PRXC__Outreach_Delivered__c =  System.now().addYears(4)));
                Tasklist.add(new Task(whoid=contactRecordList[0].id,Status='Open',Priority='Normal',Type = 'Call',Subject='Call',PRXC__Outreach_Delivered__c =  System.now().addYears(1)));
                Tasklist.add(new Task(whoid=contactRecordList[0].id,Status='Open',Priority='Normal',Type = 'Call',Subject='Call',PRXC__Outreach_Delivered__c =  System.now().addYears(2)));
                Tasklist.add(new Task(whoid=contactRecordList[0].id,Status='Open',Priority='Normal',Type = 'Call',Subject='Call',PRXC__Outreach_Delivered__c =  System.now().addYears(3)));
                Tasklist.add(new Task(whoid=contactRecordList[0].id,Status='Open',Priority='Normal',Type = 'Call',Subject='Call',PRXC__Outreach_Delivered__c =  System.now().addYears(4)));
                Tasklist.add(new Task(whoid=contactRecordList[1].id,Status='Open',Priority='Normal',Type = 'Call',Subject='Call',PRXC__Outreach_Delivered__c =  System.now().addYears(-1)));
                Tasklist.add(new Task(whoid=contactRecordList[1].id,Status='Open',Priority='Normal',Type = 'Call',Subject='Call',PRXC__Outreach_Delivered__c =  System.now().addYears(-2)));
                Tasklist.add(new Task(whoid=contactRecordList[2].id,Status='Open',Priority='Normal',Type = 'Call',Subject='Call',PRXC__Outreach_Delivered__c =  null));
            
            }     

            insert Tasklist;
            System.debug(' Task ' + Tasklist.size());   
        
        Test.startTest();
        Database.executeBatch ( new BAT_UpdateCTellingDateOnContact () );
        Test.stopTest();
        }
    }
    
    static testMethod void testSchedulemethod () {
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.Name = 'BAT_UpdateCTellingDateOnContact';
		gDPR.GDPR_Class_Name__c = 'BAT_UpdateCTellingDateOnContact';
		gDPR.Record_Limit__c = '5000';
        Test.startTest();
        Insert gDPR;
        ScheduleBatchToContactCTData obj = NEW ScheduleBatchToContactCTData();
        obj.execute(null);  
        Test.stopTest();
    }
    
}