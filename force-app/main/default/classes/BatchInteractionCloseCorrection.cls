/******************************************************************************
* @author       Matt Povey
* @date         15 Jun 2016
* @description  Batch class for Interaction data fix following bug.
*				EXECUTION:
*				BatchInteractionCloseCorrection intCorrectionJob = new BatchInteractionCloseCorrection();
*				Database.executeBatch(intCorrectionJob);
*
* EDIT RECORD
*
* LOG	DATE			AUTHOR	JIRA		COMMENT      
* 001	15/06/2016		MP		COPT-840	Original code
*
*******************************************************************************
*/
global class BatchInteractionCloseCorrection implements Database.Batchable<SObject>, Database.stateful {

	// In the Custom Setting the date format must use the standard date format “yyyy-MM-dd HH:mm:ss” in the local time zone.
	private static final DateTime DEFAULT_START_DATE_TIME = DateTime.newInstance(2016, 05, 03, 0, 0, 0);
	private static final DateTime DEFAULT_END_DATE_TIME = system.now();
	private static final Config_Settings__c START_DATE_CONFIG = Config_Settings__c.getInstance('INTERACTION_CORRECTION_START_DATE');
	private static final DateTime START_DATE_TIME = START_DATE_CONFIG != null ? DateTime.valueOfGmt(START_DATE_CONFIG.Text_Value__c) : DEFAULT_START_DATE_TIME;
	private static final Config_Settings__c END_DATE_CONFIG = Config_Settings__c.getInstance('INTERACTION_CORRECTION_END_DATE');
	private static final DateTime END_DATE_TIME = END_DATE_CONFIG != null ? DateTime.valueOfGmt(END_DATE_CONFIG.Text_Value__c) : DEFAULT_END_DATE_TIME;
	@TestVisible private static final Set<String> CASE_OWNERSHIP_TYPES = new Set<String> {'Agent', 'Queue'};

    private Set<Id> processedCaseIds = new Set<Id>();
	private Integer totalRecordCount = 0;
	

	/**
	 * Start - select all interactions since the start date provided in custom setting
	 * @params:	Database.BatchableContext BC	Batchable Context from system.
	 * @rtnval: Database.QueryLocator			The list of matching interactions returned.
	 */
	global Database.QueryLocator start(Database.BatchableContext BC){
		return Database.getQueryLocator([SELECT Id, Name, Case__c
										 FROM Interaction__c
										 WHERE End_DT__c >= :START_DATE_TIME
										 AND End_DT__c <= :END_DATE_TIME
										 AND Event_Type__c IN :CASE_OWNERSHIP_TYPES]);
	}
	
	/**
     * Execute - perform a second select using the case ids to get all the linked interactions.
     * This is necessary as case interactions may span more than one batch.
	 * @params:	Database.BatchableContext BC	Batchable Context from system.
	 *			List<Interaction__c> intList	Batch of Interaction records to be updated
     */
	global void execute(Database.BatchableContext BC, List<Interaction__c> intList){
		Set<Id> caseIds = new Set<Id>();
		List<Interaction__c> interactionUpdateList = new List<Interaction__c>();
		
		for (Interaction__c intRec : intList) {
			caseIds.add(intRec.Case__c);
		}
		
		system.debug('CASE ID LIST: ' + caseIds);
		
		List<Case> caseList = [SELECT Id, CaseNumber, (SELECT Id, Name, End_DT__c, Closing_Agent__c, Closing_Agent_Manager__c,
															  Closing_Agent_Team__c, Closing_Agent_Group__c, Start_DT__c, CreatedById,
															  CreatedBy.My_Team_Manager__c, CreatedBy.Custom_Team__c,
															  CreatedBy.Group__c, CreatedDate
													   FROM Interaction__r
													   WHERE Event_Type__c IN :CASE_OWNERSHIP_TYPES ORDER BY CreatedDate) 
							   FROM Case WHERE Id IN :caseIds];
		
		system.debug('CASE RECORD LIST: ' + caseList);

		for (Case caseRec : caseList) {
			// Don't process more than once
			if (!processedCaseIds.contains(caseRec.Id)) {
				List<Interaction__c> caseIntList = caseRec.Interaction__r;
				system.debug('CASE INTERACTION LIST: ' + caseIntList);
				if (caseIntList.size() > 1) {
					Integer pos = 1;
					for (Interaction__c intRec : caseIntList) {
						if (pos < caseIntList.size()) {
							Interaction__c intRec2 = caseIntList.get(pos);
							intRec.End_DT__c = intRec2.Start_DT__c;
							intRec.Closing_Agent__c = intRec2.CreatedById;
							intRec.Closing_Agent_Manager__c = intRec2.CreatedBy.My_Team_Manager__c;
							intRec.Closing_Agent_Team__c = intRec2.CreatedBy.Custom_Team__c;
							intRec.Closing_Agent_Group__c = intRec2.CreatedBy.Group__c;
							interactionUpdateList.add(intRec);
						}
						pos++;
					}
				}
			}
			processedCaseIds.add(caseRec.Id);
			system.debug('PROCESSED CASE SET: ' + processedCaseIds);
		}
		
		if (!interactionUpdateList.isEmpty()) {
			totalRecordCount += interactionUpdateList.size();
			system.debug('INTERACTION UPDATE LIST: ' + interactionUpdateList);
			try {
				update interactionUpdateList;
			} catch (Exception e) {
				EmailNotifier.sendNotificationEmail('BatchInteractionCloseCorrection ERROR', 'BatchInteractionCloseCorrection has encountered an error during interaction update:\n\nException: ' + e.getMessage());
			}
		}
	}	
	
	/**
	 * Finish - send notification email.
	 * @params:	Database.BatchableContext BC	Batchable Context from system.
	 */
	global void finish(Database.BatchableContext bc){
		EmailNotifier.sendNotificationEmail('BatchInteractionCloseCorrection COMPLETED', 'BatchInteractionCloseCorrection has completed.\n\nTotal Interactions updated: ' + totalRecordCount);
	}
}