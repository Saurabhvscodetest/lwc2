/**
 * Unit tests for WorkAllocationToolReportScheduler class.
 */
@isTest (seeAllData=false)
private class WorkAllocationToolReportScheduler_TEST {
	
    /**
     * Code coverage method for WorkAllocationToolReportScheduler.execute()
     */
	static testMethod void testMissingReportIdParam() {
    	system.debug('TEST START: testMissingReportIdParam');
    	Config_Settings__c defaultNotificationEmail = new Config_Settings__c(Name = 'EMAIL_NOTIFICATION_DEFAULT_ADDRESS', Text_Value__c = 'Matthew.Povey@johnlewis.co.uk');
		insert defaultNotificationEmail;
		SchedulableContext sc = null;
		WorkAllocationToolReportScheduler watScheduler = new WorkAllocationToolReportScheduler();
		Test.startTest();
		watScheduler.execute(sc);
		Test.stopTest();
    	system.debug('TEST END: testMissingReportIdParam');
	}

    /**
     * Code coverage method for WorkAllocationToolReportScheduler.execute()
     */
	static testMethod void testMissingReportRecipientsParam() {
    	system.debug('TEST START: testMissingReportRecipientsParam');
    	Config_Settings__c defaultNotificationEmail = new Config_Settings__c(Name = 'EMAIL_NOTIFICATION_DEFAULT_ADDRESS', Text_Value__c = 'Matthew.Povey@johnlewis.co.uk');
    	Config_Settings__c reportIdConfig = new Config_Settings__c(Name = 'WORK_ALLOCATION_REPORT_ID', Text_Value__c = '00Om0000000JuVz');
    	List<Config_Settings__c> csList = new List<Config_Settings__c> {defaultNotificationEmail, reportIdConfig};
    	insert csList;
		SchedulableContext sc = null;
		WorkAllocationToolReportScheduler watScheduler = new WorkAllocationToolReportScheduler();
		Test.startTest();
		watScheduler.execute(sc);
		Test.stopTest();
    	system.debug('TEST END: testMissingReportRecipientsParam');
	}

    /**
     * Code coverage method for WorkAllocationToolReportScheduler.execute()
     */
	static testMethod void testExecuteMethod() {
    	system.debug('TEST START: testExecuteMethod');
    	Config_Settings__c defaultNotificationEmail = new Config_Settings__c(Name = 'EMAIL_NOTIFICATION_DEFAULT_ADDRESS', Text_Value__c = 'Matthew.Povey@johnlewis.co.uk');
    	Config_Settings__c reportIdConfig = new Config_Settings__c(Name = 'WORK_ALLOCATION_REPORT_ID', Text_Value__c = '00Om0000000JuVz');
    	Config_Settings__c reportRecipientsConfig = new Config_Settings__c(Name = 'WORK_ALLOCATION_REPORT_RECIPIENTS', Text_Value__c = 'Matthew.Povey123456@johnlewis.co.uk');
    	List<Config_Settings__c> csList = new List<Config_Settings__c> {defaultNotificationEmail, reportIdConfig, reportRecipientsConfig};
    	insert csList;
		SchedulableContext sc = null;
		WorkAllocationToolReportScheduler watScheduler = new WorkAllocationToolReportScheduler();
		Test.startTest();
		watScheduler.execute(sc);
		Test.stopTest();
    	system.debug('TEST END: testExecuteMethod');
	}
}