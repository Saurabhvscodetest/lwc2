/**
 */
@isTest
private class TestCreditCardMask {

    static testMethod void testNoMatches() {
        String emptyStr = '';
        String okStr = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';

        // test that an empty string is returned as an empty string
        system.assertEquals(emptyStr, CreditCardMask.maskCC(emptyStr));
        // test that a string that does not contain credit cards is unchanged
        system.assertEquals(okStr, CreditCardMask.maskCC(okStr));       
    }
    
    /*
        American Express: 3711-078176-03214  |  371107817603214  |  3711 078176 03214
        Visa: 4321-5321-6321-7321  |  4321532163217321  |  4321 5321 6321 7321
        Master Card: 5321-4321-6321-7321  |  5321432163217321  |  5321 4321 6321 7321
        Discover: 6011-0009-9013-9424  |  6500000000000002  |  6011 0009 9013 9424  
    */
    static testMethod void testMatches() {
        // simple strings which just contain the card number
        String amex1 = '3711-078176-03214';
        String amex2 = '371107817603214';
        String amex3 = '3711 078176 03214';
        String visa1 = '4321-5321-6321-7321';
        String visa2 = '4321532163217321';
        String visa3 = '4321 5321 6321 7321';

        // redacted versions
        String amex1R = '*************3214';
        String amex2R = '***********3214';
        String amex3R = '*************3214';
        String visa1R = '***************7321';
        String visa2R = '************7321';
        String visa3R = '***************7321';
        
        // strings that contain other text
        String myCardIs = 'My card number is '+amex1;
        String myCardsAre = 'My first card is '+ visa1 + ' and my second is '+visa2+ ' do you want the third'+visa3;
 
        // redacted versions
        String myCardIsR = 'My card number is '+amex1R;
        String myCardsAreR = 'My first card is '+ visa1R + ' and my second is '+visa2R+ ' do you want the third'+visa3R;
        
        // now test
        system.assertEquals(amex1R, CreditCardMask.maskCC(amex1));
        system.assertEquals(amex2R, CreditCardMask.maskCC(amex2));
        system.assertEquals(amex3R, CreditCardMask.maskCC(amex3));
        system.assertEquals(visa1R, CreditCardMask.maskCC(visa1));
        system.assertEquals(visa2R, CreditCardMask.maskCC(visa2));
        system.assertEquals(visa3R, CreditCardMask.maskCC(visa3));
        system.assertEquals(myCardIsR, CreditCardMask.maskCC(myCardIs));
        system.assertEquals(myCardsAreR, CreditCardMask.maskCC(myCardsAre));        
    }
    
    static testmethod void testHtmlNotKnownCreditCard() {
        String html = '<p align="center">Chat Started: Friday, September 26, 2014, 15:36:01 (+0100)</p><p align="center">Chat Origin: Buying Assistance</p><p align="center">Agent Neil J</p>( 0s ) Neil J: 26/09/2014: Welcome to John Lewis, how can I help you today?<br>( 16s ) Visitor: 1234-5678-1234-5678<br>';
        String htmlR = '<p align="center">Chat Started: Friday, September 26, 2014, 15:36:01 (+0100)</p><p align="center">Chat Origin: Buying Assistance</p><p align="center">Agent Neil J</p>( 0s ) Neil J: 26/09/2014: Welcome to John Lewis, how can I help you today?<br>( 16s ) Visitor: ***************5678<br>';

        system.assertEquals(html, CreditCardMask.maskCC(html));
        
    }

    static testmethod void testHtmlKnownCreditCards() {
        String html = '<p align="center">Chat Started: Friday, September 26, 2014, 15:45:31 (+0100)</p><p align="center">Chat Origin: Buying Assistance</p><p align="center">Agent Neil J</p>( 1s ) Neil J: 26/09/2014: Welcome to John Lewis, how can I help you today?<br>( 15s ) Visitor: 3711-078176-03214<br>( 1m 5s ) Visitor: 371107817603214 3711 078176 03214 4321-5321-6321-7321 4321532163217321 4321 5321 6321 7321<br>';
        String htmlR = '<p align="center">Chat Started: Friday, September 26, 2014, 15:45:31 (+0100)</p><p align="center">Chat Origin: Buying Assistance</p><p align="center">Agent Neil J</p>( 1s ) Neil J: 26/09/2014: Welcome to John Lewis, how can I help you today?<br>( 15s ) Visitor: *************3214<br>( 1m 5s ) Visitor: ***********3214 *************3214 ***************7321 ************7321 ***************7321<br>';

        system.assertEquals(htmlR, CreditCardMask.maskCC(html));
        
    }    
}