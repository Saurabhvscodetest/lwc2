/******************************************************************************
 * @author       Kevin Burke
 * @date         14.04.2014
 * @description  Class that calls trigger actions for the Contact object

 *	Version History :   
 *	2014-12-09 - MJL-1383 - AG
 *		wrote more efficient implementation of generating Account for contact which
 *		does not have one on Insert
 *		@see original version of clsContactTriggerHandler attached to Jira MJL-1383 
 *		@see unit tests in ContactTriggerHandlerTest.cls
 *		Reimplemented/removed methods
 *		- populateAccountForContact()
 *		- getCorrespondingAccountFieldName()
 *		- getContactCreateableFields()
 *		- getAccountCreateableFields
 *		- handleContactBeforeInserts()
 *		Other notes: this handler should use BaseTriggerHandler based implementation.
 *		The one it currently uses (based on clsTriggerHandler) is way too verbose
 *		and encourages lots of copy/paste which leads to unnecessary bugs
 *	
 * EDIT RECORD
 *
 * LOG     DATE        Author	JIRA		COMMENT      
 * 001     23/07/2015  MP		CMP-507		Add calls to PCDUpdateHandler
 *
******************************************************************************/
public without sharing class clsContactTriggerHandler 
{
        private static final Integer MIN_PHONE_LENGTH = 8;
    private static final Integer MAX_PHONE_LENGTH = 19;
    private static final String UNRESTRICTED_PROFILE_ID = '00eb0000000ZJYa';
    private static final String ORDERING_CUSTOMERS_RECORD_TYPE = 'Ordering Customer';
    private static final Map<ID,Schema.RecordTypeInfo> rt_Map = Contact.sObjectType.getDescribe().getRecordTypeInfosById();
    @TESTVISIBLE private static final String ORDERING_CUSTOMERS_EDIT_PERMISSION_SET_NAME = 'Key_Edit_Ordering_Customer';
    @TESTVISIBLE private static final String ORDERING_CUSTOMERS_EDIT_ERROR = 'You cannot make changes to this customer in Connex. Please advise the customer to go online and change their details in the \'My Account\' area.';
    public static Boolean userHasEditOrderingCustomerPermission =  false;

	public static void mainEntry(Boolean isBefore, 
                               Boolean isDelete, Boolean isAfter, 
                               Boolean isInsert, Boolean isUpdate, 
                               Boolean isExecuting, List<Contact> newlist, 
                               Map<ID,SObject> newmap, List<Contact> oldlist, 
                               Map<Id,SObject> oldmap)
	{
		System.debug('@@entering clsContactTriggerHandler');

		if (isBefore && (isInsert || isUpdate))
		{
			System.debug('@@invoking QAS address component');
			// TODO - reinsert when in Case environment
			//QAS_NA.RecordStatusSetter.InvokeRecordStatusSetterConstrained(newlist,oldlist,isInsert,2);
		}
		
		if (newlist != null)
		{
			for (Contact c : newlist) 
			{
				if ((isInsert || isUpdate) && isBefore)
				{
					c.HomePhone = formatPhone(c.HomePhone);
					System.debug(c.HomePhone);
					c.MobilePhone = formatPhone(c.MobilePhone);
					System.debug(c.MobilePhone);
					c.OtherPhone = formatPhone(c.OtherPhone);
					System.debug(c.OtherPhone);
					c.Phone = formatPhone(c.Phone);
					System.debug(c.Phone);
				}
			}
		}
		
		// Added by AH
		// Would like this mainEntry method not to be static and 
		// pass all records to a trigger specific method - e.g. handleContactBeforeInserts
		clsContactTriggerHandler handler = new clsContactTriggerHandler();
		
		if (isInsert && isBefore)
		{
			System.debug('@@calling handleContactBeforeInserts');
			handler.handleContactBeforeInserts(newlist);
		}
		
		if (isUpdate && isBefore)
        {
        	userHasEditOrderingCustomerPermission = CommonStaticUtils.isPermissionSetAssignedToSingleUserByName(UserInfo.getUserId(), ORDERING_CUSTOMERS_EDIT_PERMISSION_SET_NAME);
            System.debug('@@calling handleContactBeforeUpdates');
            handler.handleContactBeforeUpdates((Map<ID,Contact>)newmap, (Map<ID,Contact>)oldmap);
        }
        
        // <<01>>
        if (isUpdate && isAfter) {
            System.debug('@@calling handleContactAfterUpdates');
            handler.handleContactAfterUpdates((Map<Id,Contact>) oldmap, (Map<Id,Contact>) newmap);
        }

        // <<004>>
        if (isInsert && isAfter) {
            System.debug('@@calling handleContactAfterInserts');
            handler.handleContactAfterInserts(newlist);
        }        

		System.debug('@@exiting clsContactTriggerHandler');
	}
	
	public void handleContactBeforeUpdates(Map<ID,Contact> newmap, Map<ID,Contact>oldmap) {
		
		// in case any of the fields used to create the matching keys change, update the matching keys
		for (Contact newC : newmap.values()) {
			Contact oldC = oldMap.get(newC.id);
            newC.phone = fixPhoneNumber(newC.phone);
            
			if (newC.FirstName != oldC.FirstName
					|| newC.LastName != oldC.LastName
					|| newC.Email != oldC.Email
					|| newC.MailingStreet != oldC.MailingStreet
					|| newC.MailingPostalCode != oldC.MailingPostalCode) {
					
				ContactUtils.updateContactCaseMatchKeys(newC);
					
			}
		}
		
		checkRecordsForVocUpdate(newmap.values());
		
		if(!System.isFuture() && !System.isBatch()){
            Set<String> customerEmailIds = new Set<String>();
            for(Contact newC : newmap.values()){
                Contact oldC = oldMap.get(newC.id);
                if(newC.VoC_Opt_Out__c != oldC.VoC_Opt_Out__c   &&  newC.VoC_Opt_Out__c){
                    customerEmailIds.add(newC.email);              
             	}
           }
           if(customerEmailIds.size() > 0){         
            	ContactUtils.updateCustomers(customerEmailIds);
           }
        }              
				
		PCDUpdateHandler.handleContactBeforeUpdate(oldMap, newMap); // <<01>>
		ContactUtils.UpdateCustomerOwner(oldMap, newMap);
	}
	
    /**
     * After Update Trigger Handler method <<01>>
	 * @params:	Map<Id, Contact> oldMap		Map of Contacts from Trigger.oldMap
	 *			Map<Id, Contact> newMap		Map of Contacts from Trigger.newMap
     */
	public void handleContactAfterUpdates(Map<Id, Contact> oldMap, Map<Id, Contact> newMap) {
		PCDUpdateHandler.handleContactAfterUpdate(oldMap, newMap);
        ContactUtils.handleLoyaltyAccountAfterContact(newMap);
	}
	
	public static string formatPhone (string strInput)
	{
		System.debug('@@entering formatPhone');
		
		String strReturn = '';
		
		if (strInput != null && strInput != '') 
		{		
			for (integer i = 0; i < strInput.length(); i++)
			{
				string sub = strInput.substring(i,i+1);
				System.debug(sub);
				if (!( '-'.equals(sub) || ' '.equals(sub) || '('.equals(sub) || ')'.equals(sub)))
				{
					if ( '+'.equals(sub))
					{
						system.debug('replacing a plus');
						strReturn += '00';
					}
					else
					{
						system.debug('adding on the sub');
						strReturn += sub;
					}
				} 
				else
				{
					system.debug('ignoring character');
				}
			}
		}

		System.debug('@@exiting formatPhone');
		
		System.debug(strReturn);
		
		return strReturn;
		
	}
	
	public clsContactTriggerHandler()
	{
		
	}
	
	
	// Will extend to all all before contact insert logic - e.g. format phone
	// Should format phone also be added to before update
	// For now will look at the copy of contact field data to an account record
	// if the account id is empty? Will use metadata introspection to 
	// copy values between fields with the same name
	// will not check types and lengths - so may just catch and report exceptions?
	// Don't have enough custom objects to create an apex_errors object!
	public void handleContactBeforeInserts(List<Contact> newContacts)
	{
		
		// create contact matching keyes
		for(Contact newC : newContacts){
			ContactUtils.updateContactCaseMatchKeys(newC);
            newC.phone = fixPhoneNumber(newC.phone);
		}
	}

	/**
    * Perform after insert actions
    * @params: Trigger.new list
    * @rtnval: void
    */  
    public void handleContactAfterInserts(List<Contact> newContacts)
    {
        mapLoyaltyAccounts(newContacts);
    }   
     
     private static void checkRecordsForVocUpdate(list<contact> newContacts) {
     	//check if the ordering customer could be edited as any user as part of VOC update 
     	if(CommonStaticUtils.canEditOrderingCustomerForVocUpdate()){
     		return;
     	}
     	//check if the current user profile is unrestricted
     	if(userinfo.getProfileId().containsIgnoreCase(UNRESTRICTED_PROFILE_ID)){
     		return;
     	}

     	//check if the current user has permissions to edit ordering customers
     	if(userHasEditOrderingCustomerPermission){
     		return;
     	}
     	
     	for(Contact con:newContacts){
     		Schema.RecordTypeInfo recTypeInfo = rt_map.get(con.recordTypeID);
     		if(recTypeInfo != null && con.Skip_Batch_Validation__c != true && recTypeInfo.getName().containsIgnoreCase(ORDERING_CUSTOMERS_RECORD_TYPE) && !Test.isRunningTest()){
     			con.addError(ORDERING_CUSTOMERS_EDIT_ERROR);
     		}
     	}
     } 
     //<<004>>  Start
     
     /**
     * Perform after update action, if any of details change on contact update accounts
     * @params: Trigger.new map, trigger.old map
     * @rtnval: void
     */ 
     private static void mapLoyaltyAccounts(list<contact> newContacts) { 
        Map<String,Id> conMap = new Map<String,Id>(); 
        List<Loyalty_Account__c> laUpdateList = new List<Loyalty_Account__c>();
        
        
        for (contact c:newContacts) {
            if(c.email !=null){
                conMap.put(c.email, c.Id);
            }   
        }
        
        for (Loyalty_Account__c la:[SELECT Id, contact__c, Email_Address__c FROM Loyalty_Account__c WHERE Email_Address__c IN :conMap.keyset() limit 50000]) {
            //system.debug('la: '+la);
            // if the Loyalty Account is not already linked, we need to link it
            if (la.contact__c == null) {
                // find the email in the Contact Profile map
                Id cid = (Id) conMap.get(la.Email_Address__c);
                //system.debug('cpId: '+cpId);
                if (cid != null) {
                    // Link the Loyalty Account to the Contact profile
                    la.contact__c = cid;
                    la.adopted__c = true;
                    laUpdateList.add(la);
                }
            }
        }
        if (laUpdateList.size() > 0) {
            update laUpdateList;
        }        
        
     }


     /**
     * Perform after update action, if any of details change on contact update accounts
     * @params: phone number
     * @rtnval: processed phone number
     */      
     private String fixPhoneNumber(String phone) {
        String normalised = phone;
        if (!String.isBlank(phone)) {
            // replace apostrophe, period and comma, semi colon and colon
            normalised = phone.replace('\'', '');
            normalised = normalised.replace('.', '');
            normalised = normalised.replace(',', '');
            normalised = normalised.replace(':', '');
            normalised = normalised.replace(';', '');
            
            
            // MJL-1763 - try to stop bad phone numbers from stopping Contact Profile insert
            Integer length = normalised.length();
            if (length < MIN_PHONE_LENGTH) {
                // just omit it
                normalised = null;
            } else if (length > MAX_PHONE_LENGTH){
                // truncate it
                normalised = normalised.substring(0, MAX_PHONE_LENGTH);
            }
            // finally check for alphabetic characters
            if (String.isNotBlank(normalised)) {
                Boolean bAlpha = normalised.toLowerCase().containsAny('abcdefghijklmnopqrstuvwxyz');
                if (bAlpha) {
                    normalised = null;
                }                           
            }
        }
        return normalised;
    }
     
     //<<004>>  end
}