public class EntitlementConfigManager {
    
    private static boolean ADD_OMNI = true;
    private static boolean DO_NOT_ADD_OMNI = false;
    @TestVisible private static boolean SEND_EMAIL_TO_QUEUE_MEMBERS = false;
    public static String  BOLT_SUFFIX = ' - BOLT';
    
    public static String  FAILED_ROUTING_CONFIG = 'Failed_Entitlement';
    public static String  WARNING_ROUTING_CONFIG = 'Warning_Entitlement';
    public static String  DEFAULT_ROUTING_CONFIG = 'Standard_Entitlement';
    public static String  ROUTING_CONFIG_SUFFIX = '_Entitlement';
    public static String  GENERIC_ENTITLEMENT_NAME = 'Generic';
    public static String  PHONE_SUPPORT = 'Phone Support';
    public static String  QUEUE_LITERAL = 'QUEUE';
    public static final String CASE_LITERAL = 'Case';
    public static String  FAILED_QUEUE_SUFFIX = 'FAILED';
    public static String  WARNING_QUEUE_SUFFIX = 'WARNING';
    public static String  ACTIONED_QUEUE_SUFFIX = 'ACTIONED';
    public static String  DEFAULT_QUEUE_EMAIL = 'salesforce_null@johnlewis.co.uk';
    public static final String SEPARATOR = ' ';
    public static final Map<String, Id> ENTITLEMENT_PROCESS_NAME_TO_ID_MAP = getEntitlementProcesses(); 
    public static final Map<String, Id> ROUTING_CONFIG = getRoutingConfig();
    public static Set<String> rolesAssignedToGroup = new Set<String>(); 

    public static void updateEntitlements(Set<String> entitlementNames) {
        Map<String, Team__c> teamMap = new Map<String, Team__c>();
        
        for(String entitlementName : entitlementNames) {
            if(!entitlementName.endsWith(BOLT_SUFFIX) && (Team__c.getInstance(entitlementName) != null)){
                teamMap.put(entitlementName, Team__c.getInstance(entitlementName));
            }
        }
        buildEntitlementRecords(teamMap);
    }
     
    public static void buildEntitlementsAndRelatedQueues(Set<String> teamNames,boolean addOmni) {
        Map<String, Team__c> teamMap = new Map<String, Team__c>();
        for(String teamName : teamNames){
            teamMap.put(teamName, Team__c.getInstance(teamName));
        }
        buildEntitlementsAndRelatedQueues(teamMap,addOmni);
    }
    
    public static void buildEntitlementsAndRelatedQueues(Map<String, Team__c> teamMap,boolean addOmni) {
        ADD_OMNI = addOmni;

        buildEntitlementRecords(teamMap); 

        if(addOmni) {
            createRelatedQueuesThatAreNotPresent(teamMap);
        }
        

    } 

    private static Map<String, Id> buildEntitlementAccounts(Map<String, Team__c> teamMap) {

        Map<String, Id> nameToAccountIdMap = new Map<String, Id>();
        Set<String> accountNames = teamMap.clone().keySet();
        // Get existing Accounts
        for(Account account : [SELECT Id, Name FROM Account WHERE Name IN : accountNames]) {
            nameToAccountIdMap.put(account.Name, account.Id);
        }

        // Build others
        List<Account> accountsToInsert = new List<Account>();
        accountNames.removeAll(nameToAccountIdMap.keySet());
        
        for(String remainingAccountName : accountNames) {
            Account account = new Account();
            account.Name = remainingAccountName;
            accountsToInsert.add(account);
        }

        insert accountsToInsert;

        for(Account account : accountsToInsert) {
            nameToAccountIdMap.put(account.Name, account.Id);
        }

        return nameToAccountIdMap;

    }
    
    
    private static void createRelatedQueuesThatAreNotPresent(Map<String, Team__c> teamMap) {

        List<QueueOwnerFilter__c> queueFilterList = new List<QueueOwnerFilter__c>();
        Set<String> queueFilterNameSet = new Set<String>();
        Map<String, Group> groupsToBeUpsertedMap = new Map<String, Group>();
        Set<Id> defaultQueueIds = new Set<Id>();
        Set<String> queueTypes = EntitlementConstants.RELATED_QUEUE_TYPES;

        Map<String, Group> queueNameToGroupMap = new Map<String, Group>();
        for(Group grp : [SELECT Id, Name, Type FROM Group WHERE Type = 'Queue']){
            queueNameToGroupMap.put(grp.Name, grp);
        }

        for(Team__c config : teamMap.values()) {
            Id defaultQueueId = TeamUtils.getQueueIdForTeam(config.Name);
            QueueSobject defaultQueueObj = TeamUtils.getQueueObject(defaultQueueId); 

            defaultQueueIds.add(defaultQueueId);

            Group defaultGroup = queueNameToGroupMap.get(defaultQueueObj.Queue.Name);
            defaultGroup.QueueRoutingConfigId = ROUTING_CONFIG.get(DEFAULT_ROUTING_CONFIG);
            defaultGroup.Email= DEFAULT_QUEUE_EMAIL;
            defaultGroup.DoesSendEmailToMembers= SEND_EMAIL_TO_QUEUE_MEMBERS;

            groupsToBeUpsertedMap.put(defaultQueueObj.Queue.Name, defaultGroup);

            for(String queueType : queueTypes) {
                
                Group grp;
                Id queueId = MilestoneHandlerSupport.getQueueId(defaultQueueId, queueType);

                if(queueId == null) {
                    grp = new Group();
                    grp.Name = defaultQueueObj.Queue.Name + SEPARATOR + queueType;
                    grp.Type = QUEUE_LITERAL;
                } else {
                    QueueSobject queueObj = TeamUtils.getQueueObject(queueId);
                    grp = queueNameToGroupMap.get(queueObj.Queue.Name);
                }
                setStandardGroupValues(grp);
                
                if(queueType != ACTIONED_QUEUE_SUFFIX){
                    createRoutingConfigForGroup(grp, queueType.toLowerCase().capitalize() + ROUTING_CONFIG_SUFFIX);
                }
                
                groupsToBeUpsertedMap.put(grp.Name, grp);
                QueueOwnerFilter__c qOwnerFilter = createQueueFilterList(grp);
                //check if the queue name is already added , this check is done for queue with longer names and where the name is truncated due to platform restriction 
                if(qOwnerFilter != null && !queueFilterNameSet.contains(qOwnerFilter.Name) && QueueOwnerFilter__c.getInstance(qOwnerFilter.Name) == null){
                    queueFilterNameSet.add(qOwnerFilter.Name);
                    queueFilterList.add(qOwnerFilter);
                }
            }
        }
        system.debug('queueFilterList =>' + queueFilterList);
            insert queueFilterList;
        upsert groupsToBeUpsertedMap.values();

            Set<Id> relatedQueueIds = new Set<Id>();

        for(Group grp : groupsToBeUpsertedMap.values()) {
                if(!defaultQueueIds.contains(grp.Id)) {
                    relatedQueueIds.add(grp.Id);
                }
            }
            retrieveRolesAssignedToGroup(defaultQueueIds);
            upsertQueueSObjectAndAddGroupMembersForRelatedQueues(relatedQueueIds);
    }

    public static void createQueueSObjects(Set<Id> groupIds) {
        
        List<QueuesObject> queueSObjectList = new List<QueuesObject>();
        List<QueuesObject> existingQueueSobjects = [select ID,queueid from  QueuesObject where queueid IN :groupIds];
        Set<Id> existingQueueIds = new Set<Id>();
        
        for(QueuesObject qSobj : existingQueueSobjects){
            existingQueueIds.add(qSobj.queueid);
        }
        for(Id grpId:groupIds){
            if(!existingQueueIds.contains(grpId)) {
                QueuesObject q= new QueuesObject (queueid = grpId, sobjectType = CASE_LITERAL);
                queueSObjectList.add(q);
            }
        }
        insert queueSObjectList;
        
        //refresh the cache to include the new queues
        TeamUtils.buildTheStaticCache();
    }
    
    public static void upsertGroupMembers(Set<Id> groupIds) {

        List<Group> relatedQueueGroupsList = new List<Group>([select id, Name from Group where Id IN :groupIds]);
        Map<String, Id> groupNameToIDToMap = new Map<String, Id>();
        Map<String, Set<String>> defaultQueueToRealtedQueuesMap = new Map<String, Set<String>>();
        List<GroupMember> groupMembersToBeInserted = new List<GroupMember>(); 

        for(Group grp: relatedQueueGroupsList){
            groupNameToIDToMap.put(grp.Name, grp.Id);
        }

        List<QueueSobject> queueObjList = new List<QueueSobject>([SELECT Queue.Id, Queue.DeveloperName,Queue.Name  FROM QueueSobject WHERE SobjectType = 'Case' AND Queue.Name IN:groupNameToIDToMap.keyset()]);

        for(QueueSobject qSobj: queueObjList){
            //from the related queue Id get the default queue Id
            Id defaultQueueId = TeamUtils.getQueueId(qSobj.Queue.Id, TeamUtils.QUEUE_TYPES.DEFAULT_QUEUE);

            QueueSobject defaultQueueSobject = TeamUtils.getQueueObject(defaultQueueId);
            Set<String> realtedQueueNames = defaultQueueToRealtedQueuesMap.get(defaultQueueSobject.Queue.Name);
            
            if(realtedQueueNames == null) {
                realtedQueueNames = new Set<String>();
            } 
            
            realtedQueueNames.add(qSobj.Queue.Name);
            defaultQueueToRealtedQueuesMap.put(defaultQueueSobject.Queue.Name, realtedQueueNames);
        }
        
        Map<Id, Group> existingDefaultQueueGroups = new Map<Id, Group>([select id, Name , (select id, UserOrGroupId  from GroupMembers) from Group where Name IN :defaultQueueToRealtedQueuesMap.KeySet()]);        
        
        for(Group grp:existingDefaultQueueGroups.values()) {
            for(GroupMember grpMem : grp.GroupMembers) {
                for(String realtedQueueName : defaultQueueToRealtedQueuesMap.get(grp.Name)){
                    GroupMember gm = new GroupMember(GroupId = groupNameToIDToMap.get(realtedQueueName) ,   UserOrGroupId = grpMem.UserOrGroupId);
                    groupMembersToBeInserted.add(gm);
                    rolesAssignedToGroup.add(gm.UserOrGroupId);
                }
            }
        }
        upsert  groupMembersToBeInserted;   
    }
    
    @Future 
    public static void upsertQueueSObjectAndAddGroupMembersForRelatedQueues(Set<Id> groupIds) {
        createQueueSObjects(groupIds);
        upsertGroupMembers(groupIds);
    }
    
    private static Map<String, Id> buildEntitlementRecords(Map<String, Team__c> teamMap) {
        
        Map<String, Id> accountNameToIdMap =buildEntitlementAccounts(teamMap);
        Map<String, Id> entitlementRecordNameToIdMap = new Map<String, Id>();

        Set<String> entitlementNames = new Set<String>();
        for(Team__c config : teamMap.values()) {
            entitlementNames.add(config.name);
        }

        List<Entitlement> existingEntitlementRecords = [SELECT Id, Name, AccountId FROM Entitlement WHERE Name IN :entitlementNames];
        
        Map<String, Entitlement> entitlementNameToEntitlementMap = new Map<String, Entitlement>();

        for(Entitlement entitlement : existingEntitlementRecords) {
            entitlementNameToEntitlementMap.put(entitlement.Name, entitlement);
        }

        Set<Entitlement> entitlementRecordToUpsert = new Set<Entitlement>();

        for(Team__c config : teamMap.values()) {
            Entitlement entitlement;
            Id accountId = accountNameToIdMap.get(config.name);
            
            if(entitlementNameToEntitlementMap.containsKey(config.Name)) {
                entitlement = entitlementNameToEntitlementMap.get(config.Name); 
            } else {
                entitlement = new Entitlement();
                entitlement.Name = config.Name;
                entitlement.Type = PHONE_SUPPORT;
                entitlement.AccountId = accountId;
                entitlement.StartDate = Date.today();
            }
            entitlement.SlaProcessId = ENTITLEMENT_PROCESS_NAME_TO_ID_MAP.get(GENERIC_ENTITLEMENT_NAME);
            setBusinessHours(entitlement);
            entitlementRecordToUpsert.add(entitlement);
                
        }
        upsert new List<Entitlement>(entitlementRecordToUpsert);

        for(Entitlement entitlement : entitlementRecordToUpsert) {
            entitlementRecordNameToIdMap.put(entitlement.Name, entitlement.Id);
        }

        return entitlementRecordNameToIdMap;


    }
    
    private static void setBusinessHours(Entitlement ent) {
        String teamName = ent.Name;
        Contact_Centre__c contactCenter =  TeamUtils.getContactCentreForTeam(teamName) ;
        //Since we cannot create custom business hours in unit test, it always uses DEFAULT business hours
        String bhName = (Test.isRunningTest())? UnitTestDataFactory.DEFAULT_LITERAL : contactCenter.Business_Hours__c ;
        system.debug('Saurabh Yadav------->'+bhName);
        BusinessHours bh = CommonStaticUtils.getBusinessHours('24 Hours');
        ent.BusinessHoursId = (bh != null) ?  bh.Id : null;
    }
    
    
    private static Map<String, Id> getRoutingConfig() {
        Set<String> routingConfigNames = new Set<String>{FAILED_ROUTING_CONFIG, WARNING_ROUTING_CONFIG , DEFAULT_ROUTING_CONFIG };
        List<QueueRoutingConfig> routingConfigList = [SELECT Id, DeveloperName FROM QueueRoutingConfig WHERE DeveloperName IN :routingConfigNames];
        Map<String, Id> routingConfigNameToIdMap = new Map<String, Id>();
        for(QueueRoutingConfig qRoutingConfig : routingConfigList){
            routingConfigNameToIdMap.put(qRoutingConfig.DeveloperName, qRoutingConfig.Id);
        }
        return routingConfigNameToIdMap;
    }
    
    private static Map<String, Id> getEntitlementProcesses() {

        Set<String> entitlementProcessNames = new Set<String>{GENERIC_ENTITLEMENT_NAME};

        List<SlaProcess> entitlementProcesses = [SELECT Id, Name FROM SlaProcess WHERE isVersionDefault = TRUE and isActive = TRUE and Name IN :entitlementProcessNames];

        Map<String, Id> entitlementNameToIdMap = new Map<String, Id>();

        for(SlaProcess entitlementProcess : entitlementProcesses) {
            entitlementNameToIdMap.put(entitlementProcess.Name, entitlementProcess.Id);
        }

        return entitlementNameToIdMap;
    }

    private static void setStandardGroupValues(Group newGroup) {
        newGroup.Email = DEFAULT_QUEUE_EMAIL;
        newGroup.DoesSendEmailToMembers = SEND_EMAIL_TO_QUEUE_MEMBERS;
    }

    private static Group createRoutingConfigForGroup(Group newGroup, String queueRouting) {
        newGroup.QueueRoutingConfigId = ROUTING_CONFIG.get(queueRouting);
        return newGroup;
    }

    private static QueueOwnerFilter__c createQueueFilterList(Group queueRecord) {
        QueueOwnerFilter__c newQueueFilter = new QueueOwnerFilter__c();

        newQueueFilter.Name = (queueRecord.Name.length() > CommonStaticUtils.MAX_SEARCH_TEXT_LENGTH) ? queueRecord.Name.left(CommonStaticUtils.MAX_SEARCH_TEXT_LENGTH) : queueRecord.Name;
        newQueueFilter.Filter_from_Bulk_Transfer__c = true;
        newQueueFilter.Filter_from_Change_Owner__c = true;
        
        return newQueueFilter;
    }

    private static void retrieveRolesAssignedToGroup(Set<Id> defaultGroups){
        Set<Id> roleIds = new Set<Id>();
        Set<Id> rolesAssignedToGroup = new Set<Id>();   
        for(GroupMember gm : [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId IN :defaultGroups]) {
            rolesAssignedToGroup.add(gm.UserOrGroupId);
        }
        for(Group grp : [SELECT relatedId FROM Group WHERE Id IN :rolesAssignedToGroup]){
            roleIds.add(grp.relatedId);
        }
        AutoAssignPermissionSetHelper.upsertAutoAssignPermSetsForQueueMembers(roleIds);
        assignRelatedQueuesToUsers(roleIds);
    }

    @Future
    @TestVisible
    private static void assignRelatedQueuesToUsers(Set<Id> roleIds){
        List<User> usersToBeUpdated = [SELECT Id, Name FROM User WHERE UserRole.Id IN :roleIds AND IsActive = TRUE AND Team__c != NULL];
        update usersToBeUpdated;
    }
}