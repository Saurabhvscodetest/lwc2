global class PersonalTemplateJobsScheduledClass implements Schedulable {
   global void execute(SchedulableContext SC) {
      PersonalTemplateJobs.SweepPersonalTemplates();       
   }
}