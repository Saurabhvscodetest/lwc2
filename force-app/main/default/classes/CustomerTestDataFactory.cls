@isTest
public with sharing class CustomerTestDataFactory {

	public static Account createAccount(String accountName) {
		
		return new Account(Name = accountName);
	}

	public static Contact createContact() {
		
		Account acc = createAccount('Test Account');
		insert acc;

		Contact testContact = new Contact();

		testContact.AccountId = acc.Id;
		testContact.FirstName = 'Test';
		testContact.LastName = 'User';
		testContact.Email = 'test_user@unittest.com';

		return testContact;
	}

	public static Contact_Profile__c createContactProfile(Contact c){

		return new Contact_Profile__c(Contact__c = c.Id);
	}

	public static Loyalty_Account__c createLoyaltyAccount(Contact_Profile__c cp) {
		
		Loyalty_Account__c la = new Loyalty_Account__c(Customer_Profile__c = cp.Id);

		return la;
	}

	public static Loyalty_Card__c createLoyaltyCard(Loyalty_Account__c la, String cardNumber) {
		
		Loyalty_Card__c lc = new Loyalty_Card__c(Loyalty_Account__c = la.Id, 
												 Name = cardNumber, 
												 Card_Number__c = cardNumber);
		return lc;
	}
	
	 public static Potential_Customer__c createPotentialCustomer(String email) {
		
		Potential_Customer__c pc = new Potential_Customer__c(email__c=email);

		return pc;
	} 	
}