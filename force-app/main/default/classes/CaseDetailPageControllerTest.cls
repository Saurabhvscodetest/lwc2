@isTest
public class CaseDetailPageControllerTest {
    
    private static LiveChatVisitor createVisitor() { 
        LiveChatVisitor lcv = new LiveChatVisitor();
        
        insert lcv;
        return lcv;
    }

    private static LiveChatTranscript createTranscript(String chatKey) {
        LiveChatTranscript lct = new LiveChatTranscript();
        if (chatKey != null) {
            lct.chatKey = chatKey;
            //lct.Client_GUID__c = chatKey;
        }
        LiveChatVisitor lcv = createVisitor();
        lct.LiveChatVisitorId = lcv.Id; 
        
        return lct;
    }
    public static testMethod void fetchRelatedCaseForLCTRecord () {
       TestChatCreateCustomSettings.createCustomSettings(true);
    
        // create two transcripts
        // one has a survey and one does not
        LiveChatTranscript lct1 = createTranscript(null);
        insert lct1;
        
        String chatKey = 'TESTXX-CHAT_KEY';
        String q1 = 'favourite colour?';
        String a1 = 'red';
        Decimal r1 = 1;
        
        LiveChatTranscript lct2 = createTranscript(chatKey);

        
        insert lct2;
        Case newCase = UnitTestDataFactory.createCase(UnitTestDataFactory.createContact().Id);
        lct2.CaseId = newCase.Id;
        try{
          update lct2;
        }catch(Exception e){
          System.debug(' Exception Occured ' + e.getMessage());
        }
        CaseDetailPageController.fetchRelatedCaseRecordId ( ''+lct2.Id);
        
    }
}