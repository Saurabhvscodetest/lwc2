public class CustomTimelineLEXController {
    
    @AuraEnabled
    public static List<CustomTimeline> futurePromises {get;set;} { futurePromises = new List<CustomTimeline>(); }
    @AuraEnabled
    public static List<CustomTimeline> activePromises {get;set;} { activePromises = new List<CustomTimeline>(); }
    @AuraEnabled
    public static List<CustomTimeline> completedPromises {get;set;} { completedPromises = new List<CustomTimeline>(); }
    @AuraEnabled
    public static List<CustomTimeline> topThreeActivePromises {get;set;} { topThreeActivePromises = new List<CustomTimeline>(); }
    
    @AuraEnabled
    public static Map<String,List<CustomTimeline>> initializeTimeline(String caseId){
        Map<String,List<CustomTimeline>> timelineMap = new Map<String,List<CustomTimeline>>();
        futurePromises = new List<CustomTimeline>();
        activePromises = new List<CustomTimeline>();
        completedPromises = new List<CustomTimeline>();
        topThreeActivePromises = new List<CustomTimeline>();
        
        //Fetch values from custom metadata and pass to custom timeline class
        Map<String,List<String>> customerPromiseResolutionMap = new Map<String,List<String>>();
        customerPromiseResolutionMap = fetchResolutionOptionsInSortedOrder();

        for(Case_Activity__c obj : [SELECT Id,Slot_Queue_Name__c, Add_Comment__c, RecordTypeId, Resolution_Method__c, Actual_Activity_Completed_Date__c,Status__c,Customer_Promise_Type__c, System_Time__c ,Last_Activity_End_Date__c ,Created_Date_Time__c, Case__c, Activity_Start_Time__c, Activity_End_Time__c, Activity_Start_Date_Time__c, Activity_End_Date_Time__c, Name,Activity_Description__c, Activity_Completed_Date_Time__c, Activity_Type__c, Date__c FROM Case_Activity__c where Case__c =: caseId]){
            CustomTimeline tempObject = new CustomTimeline(obj,customerPromiseResolutionMap);
            if(tempObject.category == 'Completed'){
                completedPromises.add(tempObject);
            }
            else{
                activePromises.add(tempObject);
            }
        }
        //Rearrange timeline items
        completedPromises = rearrangeTimelineItems(completedPromises,'Completed');
        activePromises = rearrangeTimelineItems(activePromises,'Active');
        timelineMap.put('activePromises',activePromises);
        timelineMap.put('completedPromises',completedPromises);
        return timelineMap;
    }
    
    @AuraEnabled
    public static void completePromise(String promiseId, String resolutionOption){
        update new Case_Activity__c(Id=promiseId,Resolution_Method__c=resolutionOption,Activity_Completed_Date_Time__c=system.now());
    }
    
    @AuraEnabled
    public static void createCaseComment(String caseId, String commentText){
        insert new CaseComment(Parentid = caseId, CommentBody = commentText);
    }
    @AuraEnabled
    public static List<CustomTimeline> rearrangeTimelineItems(List<CustomTimeline> promises, String category){
        Map<DateTime,List<CustomTimeline>> timelineMap = new Map<DateTime,List<CustomTimeline>>();
        List<DateTime> dateList = new List<DateTime>();
        List<CustomTimeline> returnValue = new List<CustomTimeline>();
        for(CustomTimeline obj : promises){
            DateTime key = category == 'Completed' ? obj.promise.Activity_Completed_Date_Time__c : obj.promise.Activity_End_Date_Time__c;
            if(timelineMap.containsKey(key)){
                timelineMap.get(key).add(obj);
            }
            else{
                timelineMap.put(key,new List<CustomTimeline>{obj});
            }
        }
        dateList.addAll(timelineMap.keySet());
        dateList.sort();
        for(DateTime key : dateList){
            if(timelineMap.containsKey(key)){
                returnValue.addAll(timelineMap.get(key));
            }            
        }
        return returnValue;
    }

    @AuraEnabled
    public static Map<String,List<String>> fetchResolutionOptionsInSortedOrder(){
        Map<String,List<String>> customerPromiseResolutionMap = new Map<String,List<String>>();
        Map<String,List<Customer_Promise_Resolution_Dependency__mdt>> customerPromiseRecordTypeMap = new Map<String,List<Customer_Promise_Resolution_Dependency__mdt>>();
        for(Customer_Promise_Resolution_Dependency__mdt obj : [SELECT Id, Label, Resolution__c, Record_Type_Name__c, Promise_Status__c FROM Customer_Promise_Resolution_Dependency__mdt order by Label]){
            if(customerPromiseRecordTypeMap.containsKey(obj.Record_Type_Name__c)){
                customerPromiseRecordTypeMap.get(obj.Record_Type_Name__c).add(obj);
            }
            else{
                customerPromiseRecordTypeMap.put(obj.Record_Type_Name__c,new List<Customer_Promise_Resolution_Dependency__mdt>{obj});
            }
        }
        
        for(String key : customerPromiseRecordTypeMap.keySet()){
            Map<String,String> LabelToStatusMap = new Map<String,String>();
            for(Customer_Promise_Resolution_Dependency__mdt obj : customerPromiseRecordTypeMap.get(key)){
                LabelToStatusMap.put(obj.Label,obj.Promise_Status__c);
            }
            List<String> sortedLabels = new List<String>();
            sortedLabels.addAll(LabelToStatusMap.keySet());
            sortedLabels.sort();
            for(String label : sortedLabels){
                String value = LabelToStatusMap.get(label);
                if(customerPromiseResolutionMap.containsKey(key)){
                    customerPromiseResolutionMap.get(key).add(value);
                }
                else{
                    customerPromiseResolutionMap.put(key,new List<String>{value});
                }
            }
        }
        return customerPromiseResolutionMap;
    }
    @AuraEnabled
    public static Integer getCountOfOpenActivities(String caseId){
        return Integer.valueOf([select count(Id) from Case_Activity__c where Case__c = :caseId AND Activity_Completed_Date_Time__c = NULL].get(0).get('expr0'));
    }

    @AuraEnabled
    public static Boolean checkTransferCaseFromPencil(String caseActivityNewId) {
        
        List<Case_Activity__c> caseActList = [ SELECT Case__c, Activity_End_Date_Time__c FROM Case_Activity__c WHERE Id =: caseActivityNewId ];
        DateTime currentActivityEndTime = caseActList[0].Activity_End_Date_Time__c;
        Id caseId = caseActList[0].Case__c;
        
        List<Case_Activity__c> pastCaseActivityList = NEW List<Case_Activity__c>();
        List<Case_Activity__c> futureCaseActivityList = NEW List<Case_Activity__c>();   
        List<Case_Activity__c> caseActivityPastFutureList = [ SELECT Id, Activity_End_Date_Time__c FROM Case_Activity__c WHERE Case__c =: caseId AND Activity_Completed_Date_Time__c = NULL];
        
        for(Case_Activity__c ca : caseActivityPastFutureList) {
            if(ca.Activity_End_Date_Time__c < System.now()) {
                pastCaseActivityList.add(ca);
            }
            if(ca.Activity_End_Date_Time__c > System.now()) {
                futureCaseActivityList.add(ca);
            }
        }

        if(pastCaseActivityList.size()==1 && futureCaseActivityList.size()==1 && currentActivityEndTime>System.now()) {
            return True;
        }
        else if(pastCaseActivityList.size()==1 && futureCaseActivityList.size()>1 && currentActivityEndTime>System.now()) {
            return True;
        }
        else if(pastCaseActivityList.size()>1 && futureCaseActivityList.size()>1) {
            return True;
        }
        else if(pastCaseActivityList.size()>1 && futureCaseActivityList.size()==1) {
            return True;
        }
        else if(pastCaseActivityList.size()>1) {
            return True;
        }
        else {            
            return False;
        }
    }
    
    /* COPT-5415
    @AuraEnabled
    public static Boolean checkPSETransfer(String caseActNewId) {
        
        List<Case_Activity__c> cWithCase = [ SELECT Case__c FROM Case_Activity__c WHERE Id =: caseActNewId ];
        Id caseId = cWithCase[0].Case__c;
        List<Case> caseList = [ SELECT RecordType.Name FROM Case WHERE Id =: caseId ];
        String pseRecordTypeName = caseList[0].RecordType.Name;
        
        if(pseRecordTypeName=='Product Stock Enquiry') {
            return True;
        }
        else {
            return false;
        }
    }
    COPT-5415 */
}