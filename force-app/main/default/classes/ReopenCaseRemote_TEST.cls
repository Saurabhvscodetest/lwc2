/**
 * Unit tests for ReopenCaseRemote class methods.
 */
@isTest
private class ReopenCaseRemote_TEST {	
	
	@testSetup static void setup() {
         UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
    }

	@isTest
	static void testReopenCase()
	{
		
		Account acc = UnitTestDataFactory.createAccount(1, true);
    	Contact ctc = UnitTestDataFactory.createContact(1, acc.id, true);
    	Case c = UnitTestDataFactory.createQueryCase(ctc.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, true);    	
    	c = [select id,contactid,jl_Branch_master__c,jl_Query_type_detail__c, jl_Assign_query_to_queue__c, jl_Transfer_case__c, description, ownerid from Case where id = :c.id limit 1];        	
        c.Status = 'Closed - Resolved';
        c.jl_Action_Taken__c = clsCaseTriggerHandler.CUSTOMER_CONTACTED;
        c.jl_Case_Resolution__c = 'TEST';
        update c;                                		
		clsCaseTriggerHandler.resetCaseTriggerStaticVariables();
		MilestoneHandler.caseMilestoneListToBeUpdated.clear();
		Test.startTest();
		string returnVal = ReopenCaseRemote.ReopenCase(c);
		System.assertEquals('',returnVal);
		Test.stopTest();  				
	}
}