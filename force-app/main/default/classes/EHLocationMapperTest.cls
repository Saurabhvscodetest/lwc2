@isTest
public class EHLocationMapperTest {
	
	@isTest
    static  void testMappedLocation() {
    	String KEY = '14 - Peterborough';
    	String VALUE = 'Peter borough';
    	String INVALID_KEY = 'XXXXX';
    	EHCDHLocation__c location = new EHCDHLocation__c(name =KEY, Value__c =VALUE);
    	insert location;
    	Test.startTest();
	    	system.assertEquals(VALUE, EHLocationMapper.getMappedLocation(KEY));
	    	system.assertEquals(INVALID_KEY, EHLocationMapper.getMappedLocation(INVALID_KEY));
    	Test.stopTest();
    }
    
    
	@isTest
    static  void getLocation_withValidLocationId() {
    	String LOCATION_ID = '14';
    	String LOCATION_CDH = 'Peterborough';
    	String KEY = LOCATION_ID +'-'+ LOCATION_CDH;
    	String VALUE = 'Peter borough';
    	EHCDHLocation__c location = new EHCDHLocation__c(name =KEY, Value__c =VALUE);
    	insert location;
    	Test.startTest();
	    	system.assertEquals(LOCATION_CDH, EHLocationMapper.getLocation(LOCATION_ID ));
    	Test.stopTest();
    }
    
	@isTest
    static  void getLocation_withValidInValidLocationId() {
    	String LOCATION_ID = '14';
    	String INVALID_LOCATION_ID = '99';
    	String LOCATION_CDH = 'Peterborough';
    	String KEY = LOCATION_ID +'-'+ LOCATION_CDH;
    	String VALUE = 'Peter borough';
    	EHCDHLocation__c location = new EHCDHLocation__c(name =KEY, Value__c =VALUE);
    	insert location;
    	Test.startTest();
	    	system.assertEquals(null, EHLocationMapper.getLocation(INVALID_LOCATION_ID));
    	Test.stopTest();
    }
}