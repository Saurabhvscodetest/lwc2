/**
 *  @author: Andrey Gavrikov (westbrook)
 *  @date: 2014-06-30 13:58:38
 *  @description:
 *      Test methods for MyJL_JoinLoyaltyAccountHandler
 *
 *  Version History :
 *  2014-07-21 - MJL-569 - AG
 *  added test for MJL-569
 *
 *  001     13/11/14    NTJ     MJL-1294    Unit test needs to create the custom settings when SeeAllData not set
 *  002     24/04/15    NTJ     MJL-1823    Performance fixes
 *  003     11/05/15    NTJ                 Fix unit tests for 15.5 release
 *  004     18/05/15    NTJ     Add more unit tests
 *  005     29/07/15    NA      Added test.starttest and stoptest to test the future methods
 *  006     08/07/16    NA      Decommission Contact Profile
 */
@isTest
public class MyJL_JoinLoyaltyAccountHandlerTest  {
    private static String SOURCE_SYSTEM = System.Label.MyJL_JL_comSourceSystem;

    static {
        BaseTriggerHandler.addSkipReason(CalloutHandler.SKIP_ALL_NOTIFICATIONS_CODE, 'disable outbound events in this test class');
        CustomSettings.setTestMyJLSetting(UserInfo.getUserId(), new CustomSettings.TestRecord(new Map<String, Object> {'Barcode_Number_Prefix__c' => 'test'}));
    }

    public static SFDCMyJLCustomerTypes.ResultStatus getResponseStatus(SFDCMyJLCustomerTypes.CustomerResponseType response) {
        return response.ResponseHeader.ResultStatus;
    }
    public static SFDCMyJLCustomerTypes.CustomerRecordResultType getCustomerResponseStatus(SFDCMyJLCustomerTypes.CustomerResponseType response, Integer index) {
        System.assert(response.ActionRecordResults.size() > index, 'Number of Customer Result records is lower than provided index');
        system.debug('response: '+response.ActionRecordResults[index]);
        return response.ActionRecordResults[index];
    }

    /**
     * check various Key errors
     * 1. Both Shopper_ID__c & Email + Barcode provided = Expect Error
     * 2. Neither Shopper_ID__c nor Email + Barcode Provided
     * 3. Shopper_ID__c not provided,  Email provided, Barcode NOT Provided
     * 3. Shopper_ID__c not provided,  Email NOT provided, Barcode Provided
     */

    static testMethod void testVariousKeyErrors () {
        // temp out
        //return;
        SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
        SFDCMyJLCustomerTypes.Customer customer1 = MyJL_TestUtil.getCustomer('shopper-id-provided', new Map<String, Object>{'EmailAddress' => 'some1@example.com', 'CustomerLoyaltyAccountCardId' => 'test-partial-card-1'});
        SFDCMyJLCustomerTypes.Customer customer2 = MyJL_TestUtil.getCustomer(null, new Map<String, Object>());
        SFDCMyJLCustomerTypes.Customer customer3 = MyJL_TestUtil.getCustomer(null, new Map<String, Object>{'EmailAddress' => 'some2@example.com'});
        SFDCMyJLCustomerTypes.Customer customer4 = MyJL_TestUtil.getCustomer(null, new Map<String, Object>{'CustomerLoyaltyAccountCardId' => 'test-partial-card-2'});

        request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer1, customer2, customer3, customer4};

        SFDCMyJLCustomerTypes.CustomerResponseType response = MyJL_JoinLoyaltyAccountHandler.process(request);
        //check main response OK
        System.assert(getResponseStatus(response).Success, 'expected main request to succeed');
        //check individual response
        //1
        System.assertEquals(false, getCustomerResponseStatus(response, 0).Success, 'Both Shopper_ID__c & Email + Barcode provided, Expect Success = FALSE');
        System.assertEquals(MyJL_Const.ERRORCODE.INCORRECT_JOIN_REQUEST.name(), getCustomerResponseStatus(response, 0).code, 'incorrect error code returned');
        //2
        System.assertEquals(false, getCustomerResponseStatus(response, 1).Success, 'Neither Shopper_ID__c nor Email + Barcode Provided, Expect Success = FALSE');
        System.assertEquals(MyJL_Const.ERRORCODE.INCORRECT_JOIN_REQUEST.name(), getCustomerResponseStatus(response, 1).code, 'incorrect error code returned');
        //3
        System.assertEquals(false, getCustomerResponseStatus(response, 2).Success, 'Shopper_ID__c not provided,  Email provided, Barcode NOT Provided, Expect Success = FALSE');
        System.assertEquals(MyJL_Const.ERRORCODE.INCORRECT_JOIN_REQUEST.name(), getCustomerResponseStatus(response, 2).code, 'incorrect error code returned');
        //3
        System.assertEquals(false, getCustomerResponseStatus(response, 3).Success, 'Shopper_ID__c not provided,  Email NOT provided, Barcode Provided, Expect Success = FALSE');
        System.assertEquals(MyJL_Const.ERRORCODE.INCORRECT_JOIN_REQUEST.name(), getCustomerResponseStatus(response, 3).code, 'incorrect error code returned');
    }

    /**
     * Partial - success
     */
    static testMethod void testPartialSuccess () {
        // temp out
        //return;
        // Ensure custom setting exists
        MyJL_TestUtil.createCustomSettings();

        SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
        SFDCMyJLCustomerTypes.Customer customer1 = MyJL_TestUtil.getCustomer(null, new Map<String, Object>{'EmailAddress' => 'some1@example.com', 'CustomerLoyaltyAccountCardId' => 'test-partial-card-1'});
        SFDCMyJLCustomerTypes.Customer customer2 = MyJL_TestUtil.getCustomer(null, new Map<String, Object>{'EmailAddress' => 'some2@example.com', 'CustomerLoyaltyAccountCardId' => 'test-partial-card-2'});
        //deliberate duplicate by email - see if this causes an exception
        SFDCMyJLCustomerTypes.Customer customer3 = MyJL_TestUtil.getCustomer(null, new Map<String, Object>{'EmailAddress' => 'some2@example.com', 'CustomerLoyaltyAccountCardId' => 'test-partial-card-2'});
        //email is TOO long
        SFDCMyJLCustomerTypes.Customer customer4 = MyJL_TestUtil.getCustomer(null, new Map<String, Object>{'EmailAddress' => 'a123456789012345678901234567890@example.com', 'CustomerLoyaltyAccountCardId' => 'test-partial-card-3'});
        request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer1, customer2, customer3, customer4};

        // turn loggin ON
        CustomSettings.setTestLogHeaderConfig(true, -1);
        CustomSettings.setTestLogDetailConfig(true, -1);
        Test.starttest();
        SFDCMyJLCustomerTypes.CustomerResponseType response = SFDCMyJLCustomerServices.JoinCustomerLoyaltyAccount(request.RequestHeader, request.Customer);
        Test.stoptest();
        //check main response OK
        System.assert(getResponseStatus(response).Success, 'expected main request to succeed');
        //check individual response
        System.assertEquals(true, getCustomerResponseStatus(response, 0).Success, 'expected first customer record to succeed');
        System.assertEquals(true, getCustomerResponseStatus(response, 1).Success, 'expected second customer record to succeed');
        //Email Too long - customer4
        System.assertEquals(false, getCustomerResponseStatus(response, 3).Success, 'expected last customer record to fail because the email is too long');
        System.assertEquals(MyJL_Const.ERRORCODE.EMAIL_TOO_LONG.name(), getCustomerResponseStatus(response, 3).code, 'incorrect error code returned');
        //check rejection object
        //MJL-302, SCENARIO 6
        MyJL_Rejection__c rejection = [select Id, Name, Rejection_Code__c, Membership_Number__c from MyJL_Rejection__c where EmailAddress__c = 'a123456789012345678901234567890@example.com'];
        System.assertEquals(MyJL_Const.ERRORCODE.EMAIL_TOO_LONG.name(), rejection.Rejection_Code__c, 'Expected rejection code EMAIL_TOO_LONG');

        //check that relevant fields have been persisted
        System.assertEquals(1, [select count() from Loyalty_Account__c where Email_Address__c = 'some1@example.com'],
                                'Expected exactly 1 Loyalty_Account__c with given email address');
        System.assertEquals(1, [select count() from Loyalty_Card__c where Loyalty_Account__r.Email_Address__c = 'some1@example.com'],
                                'Expected exactly 1 Loyalty_Card__c associated with Loyalty_Account__c with given email address');


    }

    /**
     * Partial - check duplicate handling
     */

    static testMethod void testPartial_Duplicates () {
        // temp out
        //return;
        Loyalty_Account__c acc1 = new Loyalty_Account__c(Email_Address__c = 'some1@example.com');
        Loyalty_Account__c acc2 = new Loyalty_Account__c(Email_Address__c = 'somex@example.com');
        Database.insert(new Loyalty_Account__c[] {acc1, acc2});

        Loyalty_Card__c card2 = new Loyalty_Card__c(Name = 'test-partial-card-2', Loyalty_Account__c = acc2.Id);
        Database.insert(new Loyalty_Card__c[] {card2});

        SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
        SFDCMyJLCustomerTypes.Customer customer1 = MyJL_TestUtil.getCustomer(null, new Map<String, Object>{'EmailAddress' => 'some1@example.com', 'CustomerLoyaltyAccountCardId' => 'test-partial-card-1'});
        SFDCMyJLCustomerTypes.Customer customer2 = MyJL_TestUtil.getCustomer(null, new Map<String, Object>{'EmailAddress' => 'some2@example.com', 'CustomerLoyaltyAccountCardId' => 'test-partial-card-2'});
        request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer1, customer2};
        Test.starttest();
        SFDCMyJLCustomerTypes.CustomerResponseType response = MyJL_JoinLoyaltyAccountHandler.process(request);
        Test.stoptest();
        //check main response OK
        System.assert(getResponseStatus(response).Success, 'expected main request to succeed');
        //check individual response = OK
        System.assertEquals(false, getCustomerResponseStatus(response, 0).Success, 'expected first customer record to fail, because email already exists');
        System.assertEquals(false, getCustomerResponseStatus(response, 1).Success, 'expected second customer record to fail, because card number already exists');
        //check rejection object
        MyJL_Rejection__c rejection = [select Id, Name, Rejection_Code__c, Membership_Number__c from MyJL_Rejection__c where EmailAddress__c = 'some1@example.com'];
        System.assertEquals(MyJL_Const.ERRORCODE.EMAIL_ALREADY_USED.name(), rejection.Rejection_Code__c, 'Expected rejection code EMAIL_ALREADY_USED');
        System.assert(!String.isBlank(rejection.Membership_Number__c), 'Expected not blank Membership_Number__c');

    }

    /**
     * Full - check duplicate handling and missing Shopper Ids
     */

    static testMethod void testFull_Errors () {
        // temp out
        //return;
        Contact contact = new Contact(LastName = 'shopper1');
        Database.insert(contact);

        Contact_Profile__c profile_AlreadyJoined = new Contact_Profile__c(Shopper_ID__c = 'shopper1-already-joined', Email__c = 'some1@example.com',
                                                                            Contact__c = contact.Id, SourceSystem__c = SOURCE_SYSTEM);
        Database.insert(profile_AlreadyJoined);

        Loyalty_Account__c acc_AlreadyJoined = new Loyalty_Account__c(
                                                        Email_Address__c = 'some1@example.com',
                                                        Customer_Profile__c = profile_AlreadyJoined.Id,
                                                        ShopperID__c = 'shopper1-already-joined',
                                                        Voucher_Preference__c = 'Paper',
                                                        IsActive__c = true);
        Database.insert(acc_AlreadyJoined);

        SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
        SFDCMyJLCustomerTypes.Customer customer1_AlreadyJoined = MyJL_TestUtil.getCustomer('shopper1-already-joined', new Map<String, Object>());
        SFDCMyJLCustomerTypes.Customer customer2_Missing_Shopper_Id = MyJL_TestUtil.getCustomer('test_missing-shopper-Id', new Map<String, Object>());
        request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer1_AlreadyJoined, customer2_Missing_Shopper_Id};

        Test.startTest();
        SFDCMyJLCustomerTypes.CustomerResponseType response = MyJL_JoinLoyaltyAccountHandler.process(request);
        Test.stopTest();

        //check main response OK
        System.assert(getResponseStatus(response).Success, 'expected main request to succeed');
        //check individual response = OK
        // No longer care about this as long as the activateAccount has to handle async update
        System.assertEquals(false, getCustomerResponseStatus(response, 0).Success);
        // MJL-1823 now it works as we look at Potential Customer
        System.assertEquals(false, getCustomerResponseStatus(response, 1).Success, 'expected second customer record to fail, because Shopper_ID__c doe NOT exists');
        //System.assertEquals(true, getCustomerResponseStatus(response, 1).Success, 'expected second customer record to fail, because Shopper_ID__c doe NOT exists');
    }

    /**
     * By Shopper Id
     * 0. Shopper_ID__c does not exist - expect error CUSTOMERID_NOT_FOUND
     * 1. Loyalty_Account__c exists (inactive) and NOT linked to Profile
     * 2. Loyalty_Account__c exists (Active) and NOT linked to Profile
     * 3. Loyalty_Account__c does NOT exist - will be created with new unique Loyalty_Card__c.Name
     */

    static testMethod void testMixed () {
        // temp out
        //return;
        Contact contact1 = new Contact(LastName = 'shopper1',Shopper_ID__c = 'shopper1', email = 'some1@example.com',  SourceSystem__c = SOURCE_SYSTEM);  //<<006>>
        Contact contact2 = new Contact(LastName = 'shopper2',Shopper_ID__c = 'shopper2', email = 'some2@example.com',  SourceSystem__c = SOURCE_SYSTEM);  //<<006>>
        Contact contact3 = new Contact(LastName = 'shopper3',Shopper_ID__c = 'shopper3', email = 'some3@example.com',  SourceSystem__c = SOURCE_SYSTEM);  //<<006>>
        Database.insert(new Contact[] {contact1, contact2, contact3});

        //Contact_Profile__c profile1 = new Contact_Profile__c(Shopper_ID__c = 'shopper1', Email__c = 'some1@example.com', Contact__c = contact1.Id, SourceSystem__c = SOURCE_SYSTEM);  //<<006>>
        //Contact_Profile__c profile2 = new Contact_Profile__c(Shopper_ID__c = 'shopper2', Email__c = 'some2@example.com', Contact__c = contact2.Id, SourceSystem__c = SOURCE_SYSTEM);  //<<006>>
        //Contact_Profile__c profile3 = new Contact_Profile__c(Shopper_ID__c = 'shopper3', Email__c = 'some3@example.com', Contact__c = contact3.Id, SourceSystem__c = SOURCE_SYSTEM);  //<<006>>
        //Database.insert(new Contact_Profile__c[] {profile1, profile2, profile3});

        Loyalty_Account__c acc1 = new Loyalty_Account__c(Name='00000000012', Email_Address__c = 'some1@example.com', IsActive__c = false, ShopperId__c='shopper1', Voucher_Preference__c = 'Paper');
        Loyalty_Account__c acc2 = new Loyalty_Account__c(Name='00000000029', Email_Address__c = 'some2@example.com', IsActive__c = true, ShopperId__c='shopper2', Voucher_Preference__c = 'Paper');
        Database.insert(new Loyalty_Account__c[] {acc1, acc2});

        SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
        SFDCMyJLCustomerTypes.Customer customer0 = MyJL_TestUtil.getCustomer('shopper0-not-found', new Map<String, Object>());
        SFDCMyJLCustomerTypes.Customer customer1 = MyJL_TestUtil.getCustomer('shopper1', new Map<String, Object>());
        SFDCMyJLCustomerTypes.Customer customer2 = MyJL_TestUtil.getCustomer('shopper2', new Map<String, Object>());
        SFDCMyJLCustomerTypes.Customer customer3 = MyJL_TestUtil.getCustomer('shopper3', new Map<String, Object>());
        request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer0, customer1, customer2, customer3};

        Test.startTest();
        SFDCMyJLCustomerTypes.CustomerResponseType response = MyJL_JoinLoyaltyAccountHandler.process(request);
        Test.stopTest();

        //check main response OK
        System.assert(getResponseStatus(response).Success, 'expected main request to succeed');

        //0. Shopper_ID__c does not exist - expect error CUSTOMERID_NOT_FOUND
        // MJL-1823 behaviour has changed, errors don't happen know, the account gets created
        System.assertEquals(false, getCustomerResponseStatus(response, 0).Success);
        //System.assertEquals(true, getCustomerResponseStatus(response, 0).Success);
        //System.assertEquals(MyJL_Const.ERRORCODE.CUSTOMERID_NOT_FOUND.name(), getCustomerResponseStatus(response, 0).code, 'incorrect error code');

        //1. Loyalty_Account__c exists (inactive) and NOT linked to Profile
        System.assertEquals(true, getCustomerResponseStatus(response, 1).Success);
        //check that this Loyalty_Account__c is now Active and linked to profile
        System.assertEquals(1, [select count() from Loyalty_Account__c where Email_Address__c = 'some1@example.com'
                                                                        and contact__c = :contact1.Id and IsActive__c = true]);

        //2. Loyalty_Account__c exists (Active) and NOT linked to Profile, but as active we return 'customer already joined'
        System.assertEquals(false, getCustomerResponseStatus(response, 2).Success);
        //check that this Loyalty_Account__c is now linked to profile and still Active
        //old code - removed the contact profile check as the flag said we had already joined
        //System.assertEquals(1, [select count() from Loyalty_Account__c where Email_Address__c = 'some2@example.com' and Customer_Profile__c = :profile2.Id and IsActive__c = true]);
        System.assertEquals(1, [select count() from Loyalty_Account__c where Email_Address__c = 'some2@example.com' and IsActive__c = true]);

        //3. Loyalty_Account__c does NOT exist - will be created with new unique Loyalty_Card__c.Name
        System.assertEquals(true, getCustomerResponseStatus(response, 3).Success);
        //check that this Loyalty_Account__c is Created and linked to profile and is Active
        System.assertEquals(1, [select count() from Loyalty_Account__c where Email_Address__c = 'some3@example.com'
                                                                            and contact__c = :contact3.Id and IsActive__c = true]);
        //check that Loyalty_Card__c is also created and linked to the created Loyalty_Account__c (test by email)
        System.assertEquals(1, [select count() from Loyalty_Card__c where Loyalty_Account__r.Email_Address__c = 'some3@example.com'
                                                                            and Loyalty_Account__r.contact__c = :contact3.Id]);

        Loyalty_Account__c account = [select id, Voucher_Preference__c from Loyalty_Account__c where Email_Address__c = 'some2@example.com' and IsActive__c = true];
        		System.assertEquals('Paper', account.Voucher_Preference__c, 'Expected paper as voucher preference');
    }

    /**
     * MJL-569 - full join - with duplicate keys
     * passing two records with the same shopper id and email in a single full join request generates duplicates
     */
    static testMethod void testDuplicateIncomingRecordsFull () {
        // temp out
        //return;
        Contact contact1 = new Contact(LastName = 'shopper1',Shopper_ID__c = 'shopper1',email = 'some1@example.com',  SourceSystem__c = SOURCE_SYSTEM);  //<<006>>
        Database.insert(new Contact[] {contact1});  //<<006>>

        //Contact_Profile__c profile1 = new Contact_Profile__c(Shopper_ID__c = 'shopper1', Email__c = 'some1@example.com', Contact__c = contact1.Id, SourceSystem__c = SOURCE_SYSTEM);  //<<006>>
        //Database.insert(new Contact_Profile__c[] {profile1});   //<<006>>

        SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
        SFDCMyJLCustomerTypes.Customer customer0 = MyJL_TestUtil.getCustomer('shopper1', new Map<String, Object>());
        SFDCMyJLCustomerTypes.Customer customer1 = MyJL_TestUtil.getCustomer('shopper1', new Map<String, Object>());
        request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer0, customer1};

        SFDCMyJLCustomerTypes.CustomerResponseType response = MyJL_JoinLoyaltyAccountHandler.process(request);
        //check main response OK
        System.assert(getResponseStatus(response).Success, 'expected main request to succeed');

        //check that exactly 2 Loyalty_Account__c records have been created
        final List<Loyalty_Account__c> shoppers = [select Id, contact__c, IsActive__c, Email_Address__c from Loyalty_Account__c];
        System.assertEquals(1, shoppers.size(), shoppers);

        System.assertEquals(true, getCustomerResponseStatus(response, 0).Success, 'Expected first record to succeed');
        System.assertEquals(false, getCustomerResponseStatus(response, 1).Success, 'Expected second (duplicate) record to fail');
    }

    /**
     * MJL-569 - Partial Join - with duplicate keys
     * passing two records with the same shopper id and email in a single full join request generates duplicates
     */
     /* temp
    static testMethod void testDuplicateIncomingRecordsPartial () {
        Contact contact2 = new Contact(LastName = 'shopper2');
        Database.insert(new Contact[] {contact2});

        Contact_Profile__c profile2 = new Contact_Profile__c(Shopper_ID__c = 'shopper2', Email__c = 'some2@example.com', Contact__c = contact2.Id, SourceSystem__c = SOURCE_SYSTEM);
        Database.insert(new Contact_Profile__c[] {profile2});

        SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
        SFDCMyJLCustomerTypes.Customer customer2 = MyJL_TestUtil.getCustomer('', new Map<String, Object>{'EmailAddress' => 'some2@example.com', 'CustomerLoyaltyAccountCardId' => 'card2'});
        SFDCMyJLCustomerTypes.Customer customer3 = MyJL_TestUtil.getCustomer('', new Map<String, Object>{'EmailAddress' => 'some2@example.com', 'CustomerLoyaltyAccountCardId' => 'card2'});
        request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer2, customer3};

        SFDCMyJLCustomerTypes.CustomerResponseType response = MyJL_JoinLoyaltyAccountHandler.process(request);
        //check main response OK
        System.assert(getResponseStatus(response).Success, 'expected main request to succeed');

        //check that exactly 2 Loyalty_Account__c records have been created
        final List<Loyalty_Account__c> shoppers = [select Id, Customer_Profile__c, IsActive__c, Email_Address__c from Loyalty_Account__c];
        System.assertEquals(1, shoppers.size(), shoppers);

        System.assertEquals(true, getCustomerResponseStatus(response, 0).Success, 'Expected first record to succeed');
        System.assertEquals(false, getCustomerResponseStatus(response, 1).Success, 'Expected second (duplicate) record to fail');
    }

    /**
     * MJL-604
     * JOIN - Membership number gets over written when activating a partial joined ePOS user as well when activating a deactivated user
     */

    static testMethod void testMembershipNumberOverwrite () {
        // temp out
        //return;
        // Ensure custom setting exists
        MyJL_TestUtil.createCustomSettings();
        //partial join to initiate membership number
        SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
        SFDCMyJLCustomerTypes.Customer customer1 = MyJL_TestUtil.getCustomer(null, new Map<String, Object>{'EmailAddress' => 'some1@example.com', 'CustomerLoyaltyAccountCardId' => 'test-partial-card-1'});
        request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer1};

        SFDCMyJLCustomerTypes.CustomerResponseType response = SFDCMyJLCustomerServices.JoinCustomerLoyaltyAccount(request.RequestHeader, request.Customer);
        //check main response OK
        System.assert(getResponseStatus(response).Success, 'expected main request to succeed');
        //check individual response
        System.assertEquals(true, getCustomerResponseStatus(response, 0).Success, 'expected first customer record to succeed');

        //get original Membership number
        Loyalty_Account__c accountOriginal = [select Id, Name from Loyalty_Account__c where Email_Address__c = 'some1@example.com'] ;

        //FULL join - to check that membership number does not get overridden
        //prepare data
        Contact contact1 = new Contact(LastName = 'shopper1',Shopper_ID__c = 'shopper1', Email = 'some1@example.com', SourceSystem__c = SOURCE_SYSTEM);  //<<006>>
        Database.insert(new Contact[] {contact1});  //<<006>>

        //Contact_Profile__c profile1 = new Contact_Profile__c(Shopper_ID__c = 'shopper1', Email__c = 'some1@example.com', Contact__c = contact1.Id, SourceSystem__c = SOURCE_SYSTEM);  //<<006>>
        //Database.insert(new Contact_Profile__c[] {profile1});  //<<006>>
        //run actual request
        request = MyJL_TestUtil.prepareBlankRequest();
		customer1 = MyJL_TestUtil.getCustomer('shopper1', new Map<String, Object>{'VoucherPreference' => 'Digital'});
		request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer1};

        response = MyJL_JoinLoyaltyAccountHandler.process(request);
        //check main response OK
        System.assert(getResponseStatus(response).Success, 'expected main request to succeed');
        System.assertEquals(true, getCustomerResponseStatus(response, 0).Success, 'Expected first record to succeed. ' + response);

        Loyalty_Account__c accountLoaded = [select Id, Name from Loyalty_Account__c where Email_Address__c = 'some1@example.com'] ;

        System.assertEquals(accountOriginal.Name, accountLoaded.Name, 'Membership numbers do not match');

    }

    private static testmethod void testPostTasks() {
        Contact c = UnitTestDataFactory.createContact();
        insert c;
        Contact_Profile__c cp = UnitTestDataFactory.createContactProfile(c);
        insert cp;
        Loyalty_Account__c la = UnitTestDataFactory.createLoyaltyAccount(cp);
        insert la;
        Loyalty_Card__c lc = UnitTestDataFactory.createLoyaltyCard(la, 'crad-number1');
        insert lc;

        List<Loyalty_Card__c> lcList = new List<Loyalty_Card__c>();
        lcList.add(lc);

        Test.startTest();
            MyJL_JoinLoyaltyAccountHandler.schedulePostProcessing(lcList);
        Test.stopTest();

        // Test account got card number
        la = [SELECT Customer_Loyalty_Account_Card_ID__c FROM Loyalty_Account__c WHERE Id = :la.Id];
        system.assertEquals(la.Customer_Loyalty_Account_Card_ID__c, lc.Name);

        // TODO - test adoption of orphaned transactions
    }
}