/**
* @description  Class to handle issuing 'VOC' survey to customers after they have had a Case or Task ('Customer Contact')
* @author       @tomcarman
* @created      2017-03-07
*/
public class VocEmailHandler {    
    
    private static final String TASK_CALL_LOG_SUBJECT = 'Call log'; // Task type eligible to receive VOC survey
    private static final String SEND_EMAIL_FIELD_NAME = 'Send_VOC_Survey__c';
    
    private static String contactRelationshipName; // Relationship between entity and contact eg. WhoId or ContactId
    private static VOC_Survey_Settings__c vocCustomSettings; // Custom setting that defines grace period and users this functionality is active for
    private static Set<Id> caseExclusionRecordTypeIds; // List of Case RecordTypes to exclude
    private static final Id queryCaseRTId = CommonStaticUtils.getRecordTypeID('Case','Query');
    
    private static final String BRANCHCST = Label.VOC_Branch_CST;
    private static final String BRANCHCCL = Label.VOC_Branch_CCL;
    private static final String BRANCHVALUE = 'Branch';
    Private static final String Voc_Delay_Time = Label.VOC_Delay_Time;
    Private static final String VOC_DELAY_WORK = Label.VOC_DELAY_WORK;
    Private static final String BRANCHEMAIL = Label.VOC_BRANCH_EMAIL;
    
    /**
* @description  Entry point for class. Typically called with Trigger.New            
* @author       @tomcarman
* @param        incomingRecords     List of SObjects to be processed
*/    
    public static List<Sobject> processRecords(List<SObject> incomingRecords) {
        CommonStaticUtils.allowEditOfOrderingCustomerForVocUpdate();
        return processRecords(null, incomingRecords);
    }
    
    /**
* @description  Entry point for class. Typically called with Trigger.OldMap and Trigger.New         
* @author       @tomcarman
* @param        oldIncomingRecordsMap   Map of SObjects from trigger.oldMap to be processed
* @param        incomingRecords         List of SObjects to be processed
*/    
    public static List<Sobject> processRecords(Map<Id, SObject> oldIncomingRecordsMap, List<SObject> incomingRecords) {
        
        CommonStaticUtils.allowEditOfOrderingCustomerForVocUpdate();
        vocCustomSettings = VOC_Survey_Settings__c.getInstance('Default');
        
        if(vocCustomSettings == null || !vocCustomSettings.Enabled__c) {
            return null;
        }        
        if(!isCurrentUserCustomerServiceTeam()) {
            return null;
        }
        
        List<SObject> filteredRecords = new List<SObject>();
        
        /* Task - Task specific filtering */
        if(!incomingRecords.isEmpty() && incomingRecords[0].getSObjectType() == Task.SObjectType) {
            contactRelationshipName = 'WhoId';
            filteredRecords = filterCustomerContactTasks(incomingRecords);
            /* Case - Case specific filtering */
        } else if(!incomingRecords.isEmpty() && incomingRecords[0].getSObjectType() == Case.SObjectType) {
            contactRelationshipName = 'ContactId';
            filteredRecords = filterCustomerContactCases(oldIncomingRecordsMap, incomingRecords);
        }
        
        if(filteredRecords.isEmpty()) {
            return null;
        }
        Map<Id, Contact> relatedContacts = getRelatedEligibleContacts(filteredRecords);
        List<SObject> recordsToEmail = filterRecordsWithoutEligibleContact(filteredRecords, relatedContacts);
        Map<Id, Integer> NumberOfCustomerContactPerCase = getNumberOfCustomerContactsPerCase(recordsToEmail);
        
        if(VOC_DELAY_WORK != 'Yes') {
            updateRecordsToEmail(recordsToEmail, NumberOfCustomerContactPerCase);     
        }        
        
        //updateRecordsToEmail(recordsToEmail, NumberOfCustomerContactPerCase);        
        updateContactsWithLastSurveySendDate(recordsToEmail, NumberOfCustomerContactPerCase);
        return recordsToEmail;
    }
    
    /**
* @description  Custom logic to filter Tasks eligible for VoC survey        
* @author       @tomcarman
* @param        incomingTasks   List of Tasks from trigger.new
* @return       A list of Tasks which meet the criteria to be sent a VoC survey
*/    
    private static List<Task> filterCustomerContactTasks(List<Task> incomingTasks) {
        
        List<Task> customerContactTasks = new List<Task>();
        
        for(Task aTask : incomingTasks) {
            if(aTask.WhoId != null && aTask.WhoId.getSObjectType() == Contact.SObjectType && // task related to a contact
               aTask.Level_1__c != null && aTask.Subject == TASK_CALL_LOG_SUBJECT) { // indicates this task was most likely generated from Customer Contact screen
                   customerContactTasks.add(aTask);
               }
        }        
        return customerContactTasks;
    }
    
    /**
* @description  Custom logic to filter Cases eligible for VoC survey    
* @author       @tomcarman
* @param        incomingOldCaseMap  List of Cases from trigger.oldMap
* @param        incomingNewCaseList List of Cases from trigger.new
* @return       A list of Cases which meet the criteria to be sent a VoC survey
*/    
    private static List<Case> filterCustomerContactCases(Map<Id, SObject> incomingOldCaseMap, List<SObject> incomingNewCaseList) {
        
        List<Case> customerContactCases = new List<Case>();     
        
        for(SObject caseSObject : incomingNewCaseList) {
            Case newCase = (Case)caseSObject;
            Case oldCase =  (incomingOldCaseMap != null) ? (Case)incomingOldCaseMap.get(newCase.Id) : null;            
            //if(newCase.Origin != 'EventHub' && !getCaseExclusionRecordTypeIds().contains(newCase.RecordTypeId)) {
            //if( !getCaseExclusionRecordTypeIds().contains(newCase.RecordTypeId)) {
            if(newCase.jl_Branch_master__c != 'John Lewis Finance(Escalation Only)' && !getCaseExclusionRecordTypeIds().contains(newCase.RecordTypeId)) {  //COPT-5874 RV
                customerContactCases.add(newCase);
            }
        }
        return customerContactCases;
    }   
    
    /**
* @description  Return Contacts related to supplied records which are eligible to receive VoC survey (eg. not opted out, not sent survey in past 90 days)           
* @author       @tomcarman
* @param        incomingRecords     List of SObjects, currently supports Task and Case
* @return       Map of Contacts
*/    
    private static Map<Id, Contact> getRelatedEligibleContacts(List<SObject> incomingRecords) {
        
        Set<Id> relatedContactIds = new Set<Id>();        
        for(SObject record : incomingRecords) {
            if(record.get(contactRelationshipName) != null){
                relatedContactIds.add((Id)record.get(contactRelationshipName));
            }
        }
        
        Date todayMinusGracePeriod = Date.today().addDays(getGracePeriod()*-1);        
        Map<Id, Contact> returnMap = new Map<Id, Contact>();
        
        
        // Return Contacts which have an email address, have not opted out of survey, and have not been sent a
        // survey in past 90 days.
        if(incomingRecords[0].getSObjectType() == Case.SObjectType) {
            if(!(relatedContactIds.isEmpty())){
                
                returnMap =  new Map<Id, Contact>([SELECT Id, Name, Email
                                                   FROM Contact 
                                                   WHERE Survey_Opt_Out__c = false
                                                   AND VoC_Opt_Out__c = false
                                                   AND Email != NUll
                                                   AND (Last_Survey_Send_Date__c = null OR Last_Survey_Send_Date__c < :todayMinusGracePeriod)
                                                   AND Id IN :relatedContactIds]);
                //Logic to check if the contact is having any other active cases
                if(returnMap !=null){
                    if(returnMap.keySet().Size()>0){
                        List<Case> allActiveCases=new List<Case>();
                        allActiveCases=new List<case>([Select ContactId,Id 
                                                       From Case 
                                                       Where IsClosed = false
                                                       AND ContactId IN : returnMap.keySet()]);
                        
                        if(allActiveCases.size()>0){
                            Map<Id,List<Case>> allActiveCaseMap=New Map<Id,List<Case>>();
                            for(Case c:allActiveCases){
                                if(allActiveCaseMap.get(c.ContactId) !=null){
                                    allActiveCaseMap.get(c.ContactId).add(c);
                                }
                                else{
                                    allActiveCaseMap.put(c.ContactId,new List<case>{c});
                                }
                            }
                            for(Id contactId:allActiveCaseMap.keySet()){
                                if(allActiveCaseMap.get(contactId).size()>1){
                                    returnMap.remove(contactId);
                                }
                            }
                        }
                    }
                }
            }
            //End of Logic
            
            // Return Contacts which don't have a related open Case (survey from Case-close take precedence over ones triggered by Task), 
            // have an email address, have not opted out of survey, and have not been sent a
            // survey in past 90 days.
            
        } else if(incomingRecords[0].getSObjectType() == Task.SObjectType) {           
            
            returnMap = new Map<Id, Contact>([SELECT Id, Name, Email
                                              FROM Contact 
                                              WHERE Id NOT IN (
                                                  SELECT ContactId 
                                                  FROM Case 
                                                  WHERE IsClosed = false 
                                                  AND ContactId IN :relatedContactIds
                                              )
                                              AND Email != null
                                              AND Survey_Opt_Out__c = false
                                              AND VoC_Opt_Out__c = false
                                              AND (Last_Survey_Send_Date__c = null OR Last_Survey_Send_Date__c < :todayMinusGracePeriod)
                                              AND Id IN :relatedContactIds]);
        }
        
        return returnMap;
    }
    
    /**
* @description  Filter out any records which dont are not related to an VoC survey eligible contact         
* @author       @tomcarman
* @param        records     List of SObjects to filter
* @param        contactMap  List of Contacts to filter against
* @return       List of filtered SObjects
*/    
    private static List<SObject> filterRecordsWithoutEligibleContact(List<SObject> records, Map<Id,Contact> contactMap) {
        List<SObject> returnRecords = new List<SObject>();
        
        for(SObject record : records) {
            if(contactMap.keySet().contains((Id)record.get(contactRelationshipName))) {
                returnRecords.add(record);
            }
        }
        return returnRecords;
    }
    
    private static void updateRecordsToEmail(List<SObject> recordsToEmail, Map<Id, Integer> NumberOfCustomerContactPerCase) {
        
        for(SObject recordToEmail : recordsToEmail) {
            Id caseId = (Id)recordToEmail.get('Id');
            if(recordToEmail.getSObjectType() == Task.SObjectType || 
               (recordToEmail.getSObjectType() == Case.SObjectType 
                && recordToEmail.get('status') == 'Closed - Resolved'
                && (NumberOfCustomerContactPerCase.get(caseId) >0  
                    || recordToEmail.get('Final_Case_Activity_Resolution_Method__c') == 'Customer Contacted'
                    || recordToEmail.get('Final_Case_Activity_Resolution_Method__c') == 'Email sent to customer'))) {
                        recordToEmail.put(SEND_EMAIL_FIELD_NAME, true);
                    }
        }
    }
    
    private static void updateContactsWithLastSurveySendDate(List<SObject> recordsToEmail, Map<Id, Integer> NumberOfCustomerContactPerCase) {
        // Update Contact.Last_Survey_Send_Date__c with current date
        Integer vDelay = Integer.valueOf(Voc_Delay_Time);
        Set<Contact> contactsToUpdateSet = new Set<Contact>();
        for(SObject recordToEmail : recordsToEmail) {
            Id caseId = (Id)recordToEmail.get('Id');
            if( recordToEmail.getSObjectType() == Task.SObjectType || 
               ( recordToEmail.getSObjectType() == Case.SObjectType 
                && recordToEmail.get('status') == 'Closed - Resolved'
                && ( NumberOfCustomerContactPerCase.get(caseId) >0  
                    || recordToEmail.get('Final_Case_Activity_Resolution_Method__c') == 'Customer Contacted'
                    || recordToEmail.get('Final_Case_Activity_Resolution_Method__c') == 'Email sent to customer' ) ) ){
                        Contact relatedContact = new Contact();
                        relatedContact.Id = (Id)recordToEmail.get(contactRelationshipName);
                        relatedContact.Last_Survey_Send_Date__c = Date.today();
                        relatedContact.Actual_Survey_Send_Time__c = system.now().addHours(vDelay);
                        relatedContact.Skip_Batch_Validation__c = true;
                        relatedContact.IsSurveyCheck__c = 'Closed';
                        contactsToUpdateSet.add(relatedContact);
                    }  
        }
        
        List<Contact> contactsToUpdate = new List<Contact>(contactsToUpdateSet);        
        /* COPT-5415
* update contactsToUpdate;
* COPT-5415 */
        /* COPT-5415 */
        if(!contactsToUpdate.isEmpty()) {
            Database.update(contactsToUpdate,false);
        }
        /* COPT-5415 */
    }   
    
    /* HELPER METHODS */
    
    /**
* @description  Checks if the current user is eligible to cause a VoC survey email to be trigger. This is based on the two letter prefix
*               of the User.Net_Dom_Id__c. The allowed values are defined in a custom setting - VOC_Survey_Settings__c
* @author       @tomcarman
* @return    

Boolean of if the user is eligible or not
*/    
    private static Boolean isCurrentUserCustomerServiceTeam() {
        
        List<User> userList = [SELECT Id, Net_Dom_ID__c FROM User WHERE Id = :UserInfo.getUserId()];
        
        if(userList.size()==0) {
            return false;
        }        
        User runningUser = userList[0];        
        if(runningUser.Net_Dom_ID__c != null && isBranchUser() == False) {
            return getTeamPrefixes().contains(runningUser.Net_Dom_ID__c.subString(0,2));
        }
        else if(isBranchUser() == true ) {            
            if(isBranchUserWithVOC() == true ) {
                return true;
            }
            else {
                return false;
            }
        }        
        else {
            return false;
        }       
    }
    
    public static Boolean isBranchUser() {        
        List<User> userList = [SELECT jl_Primary_Team_Text__c FROM User WHERE Id = :UserInfo.getUserId()];
        
        if(userList.size()==0) {
            return false;
        }
        User runningUser = userList[0];
        
        if( runningUser.jl_Primary_Team_Text__c != NULL) {
            if(runningUser.jl_Primary_Team_Text__c.containsIgnoreCase(BRANCHVALUE)) {
                return True;
            }
            else {
                return false; 
            } 
        }
        else {
            return false;   
        }
    }
    
    public static Boolean isBranchUserWithVOC() {
        
        List<User> userList = [SELECT jl_Primary_Team_Text__c FROM User WHERE Id = :UserInfo.getUserId()];
        
        if(userList.size()==0) {
            return false;
        }        
        User runningUser = userList[0];
        
        if(runningUser.jl_Primary_Team_Text__c != NULL) {
            
            String userBranch = runningUser.jl_Primary_Team_Text__c;            
            if(userBranch.containsIgnoreCase(BRANCHCST)||userBranch.containsIgnoreCase(BRANCHCCL)||userBranch.containsIgnoreCase(BRANCHEMAIL)){
                return true;
            }
            else {
                return false;
            }
        }
        else { 
            return false;
        }
    }   
    
    /**
* @description  Retrieve the eligible User.Net_Dom_Id__c prefixes from the VOC_Survey_Settings__c custom setting            
* @author       @tomcarman
* @return       Set of elibile prefixes
*/    
    private static Set<String> getTeamPrefixes() {
        
        Set<String> teamPrefixes = new Set<String>();
        
        if(vocCustomSettings == null) {
            vocCustomSettings = VOC_Survey_Settings__c.getInstance('Default');
        }
        
        if(vocCustomSettings != null) {
            if(!String.isEmpty(vocCustomSettings.Team_Prefixes__c)) {
                teamPrefixes.addAll(vocCustomSettings.Team_Prefixes__c.split(','));
            }
        }        
        return teamPrefixes;
    }    
    
    /**
* @description  Retrieve the grace period (amount of days to supress sending a survey after one has been issued) from the VOC_Survey_Settings__c custom setting         
* @author       @tomcarman
* @return       Number of days as an integer
*/
    
    private static Integer getGracePeriod() {
        
        if(vocCustomSettings == null) {
            vocCustomSettings = VOC_Survey_Settings__c.getInstance('Default');
        }        
        return Integer.valueOf(vocCustomSettings.Survey_Grace_Period__c);
    }
    
    /**
* @description  Returns a set of Case RecordTypeIds which should not cause a VoC survey to be triggered     
* @author       @tomcarman
* @return       Set of RecordTypeIds
*/    
    private static Set<Id> getCaseExclusionRecordTypeIds() {
        
        // TODO: check if there is a util doing this with schema describes        
        /* COPT-5415
Set<String> recordTypesToExclude = new Set<String>();
recordTypesToExclude.add('PSE Sub-case');
recordTypesToExclude.add('myJL Request');
recordTypesToExclude.add('Order Exception');
recordTypesToExclude.add('Product Stock Enquiry');
COPT-5415 */
        
        /* COPT-5415 */
        Set<String> recordTypesToExclude = new Set<String>{'PSE Sub-case','myJL Request','Order Exception','Product Stock Enquiry'};
            /* COPT-5415 */
            
            if(caseExclusionRecordTypeIds == null) {            
                caseExclusionRecordTypeIds = new Set<Id>();            
                for(RecordType recordType :  [SELECT Id FROM RecordType WHERE SObjectType = 'Case' AND Name IN :recordTypesToExclude]) {
                    caseExclusionRecordTypeIds.add(recordType.Id);
                }
            }
        return caseExclusionRecordTypeIds;        
    }
    
    /**
* @description  Identify if a Case has been closed during this transaction          
* @author       @tomcarman
* @param        newCase     Case from trigger.new
* @param        oldCase     Case from trigger.old
* @return       Boolean of if the case has been closed during this transaction
*/    
    private static Boolean caseHasBeenClosed(Case newCase, Case oldCase) {
        
        Boolean returnValue = false;
        if(newCase.IsClosed && !oldCase.IsClosed) {
            returnValue = true;            
        } else if (oldCase!= null && newCase.Status != null && oldCase.Status != null) {            
            if(oldCase.Status != 'Close Case' && oldCase.Status != 'Closed - Resolved' && (newCase.Status == 'Close Case' || newCase.Status == 'Closed - Resolved')) {
                returnValue = true;
            }
        } else if(newCase.RecordTypeId == queryCaseRTId 
                  && (newCase.Status.equals('Closed - Resolved') || newCase.Status.equals('Close Case')) ) {
                      returnValue = true;
                  }
        return returnValue;
    }
    
    public static Map<Id, Integer> getNumberOfCustomerContactsPerCase(List<SObject> recordsToEmail) {
        Set<Id> caseIds =  new Set<Id>();
        if(recordsToEmail.size()>0 && recordsToEmail[0].getSObjectType() == Case.SObjectType ){
            for(SObject c : recordsToEmail){
                caseIds.add((Id)c.get('Id'));
            }
        }
        
        Map<Id, Integer> NumberOfCustomerContactPerCase = new Map<Id, Integer>();
        for (AggregateResult aggRes : [
            SELECT COUNT(ID) numContacts, Case__c caseId
            FROM Case_Activity__c
            WHERE Case__c in :caseIds  AND (Resolution_Method__c = 'Customer Contacted' OR Resolution_Method__c = 'Email sent to customer' )
            GROUP BY Case__c
        ]) {
            Id caseId = (Id) aggRes.get('caseId');
            Integer numContacts = (Integer) aggRes.get('numContacts');
            NumberOfCustomerContactPerCase.put(caseId, numContacts);
        }
        return NumberOfCustomerContactPerCase;
    }
}