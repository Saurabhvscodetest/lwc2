/**
 *	@author: Amit Kumar
 *	@date: 2014-07-04 
 *	@description:
 *	    unit tests for MyJL_LeaveCustomerHandler
 *	
 *	Version History :   
 *  001     08/07/16    NA      Decommission Contact Profile 	
 */
@isTest
private class MyJL_LeaveLoyaltyAccountHandlerTest {
    private static final String SHOPPER_ID = 'shopper1';
    
    private static final String PARTNERSHIP_CARD_NUMBER = '3456789456123';
    private static final boolean PHYSICAL_WELCOME_PACK = true;
    private static final String ORIGIN = 'Unit Test';
    private static final DateTime DATE_CREATED = DateTime.NOW();   
    
	static {
		BaseTriggerHandler.addSkipReason(CalloutHandler.SKIP_ALL_NOTIFICATIONS_CODE, 'disable outbound events in this test class');
	}

    /**
	 * check empty messageId
	 */
	static testMethod void testMessageIdProblems () {
		
		//check duplicate messageId
		SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
		/*
		 request.RequestHeader.MessageId = 'messageId123';
		SFDCMyJLCustomerTypes.Customer customer1 = MyJL_TestUtil.getCustomer('1234444', new Map<String, Object>{'EmailAddress' => 'some124567@test.com'});
		request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer1};
		SFDCMyJLCustomerTypes.CustomerResponseType response = MyJL_CreateCustomerHandler.process(request);
		System.assertEquals(1,[SELECT COUNT() FROM Log_Header__c WHERE Message_ID__c != null AND Message_ID__c = '89891779'],'expected exactly 1 record for this messageid');
		
		SFDCMyJLCustomerTypes.CustomerRequestType request1 = MyJL_TestUtil.prepareBlankRequest();
		request1.RequestHeader.MessageId = '89891779';
		//SFDCMyJLCustomerTypes.Customer customer2 = MyJL_TestUtil.getCustomer('1235555', new Map<String, Object>{'EmailAddress' => 'some124567@test.com'});
		//request1.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer2};
		SFDCMyJLCustomerTypes.CustomerResponseType response1 = MyJL_LeaveLoyaltyAccountHandler.process(request1);
		//System.assertEquals(false, MyJL_TestUtil.getResponseStatus(response1).Success, 'expected main request to fail because of duplicate message Id ');
		
		System.assertEquals(false, MyJL_TestUtil.getResponseStatus(response1).Success, 'expected customer record to fail because it is having duplicate MessageId');
		System.assertEquals(MyJL_TestUtil.getResponseStatus(response1).Code,'MESSAGEID_DUPLICATE');
		//System.assertEquals(MyJL_TestUtil.getResponseStatus(response1, 0).Code,'MESSAGEID_DUPLICATE');
		
		//System.assertEquals((response1.ActionRecordResults[0]).Code,'MESSAGEID_DUPLICATE');
		
		//System.assertEquals(MyJL_TestUtil.getResponseStatus(response1).Code,'MESSAGEID_DUPLICATE');
		*/
		//Empty Message Id
		request = MyJL_TestUtil.prepareBlankRequest();
		request.RequestHeader.MessageId = '';
		SFDCMyJLCustomerTypes.CustomerResponseType response = MyJL_LeaveLoyaltyAccountHandler.process(request);
		//check main response FAIL
		System.assertEquals(false, MyJL_TestUtil.getResponseStatus(response).Success, 'expected main request to fail because message Id is blank');

		//null message Id
		request = MyJL_TestUtil.prepareBlankRequest();
		request.RequestHeader.MessageId = null;
		response = MyJL_LeaveLoyaltyAccountHandler.process(request);
		//check main response FAIL
		System.assertEquals(false, MyJL_TestUtil.getResponseStatus(response).Success, 'expected main request to fail because message Id is null');

		//'NULL' message Id
		request = MyJL_TestUtil.prepareBlankRequest();
		request.RequestHeader.MessageId = 'NULL';
		SFDCMyJLCustomerTypes.Customer customer = MyJL_TestUtil.getCustomer('shopper1', new Map<String, Object>{'EmailAddress' => 'some@test.com'});
		request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer};
		response = MyJL_LeaveLoyaltyAccountHandler.process(request);
		//check main response FAIL
		System.assertEquals(false, MyJL_TestUtil.getResponseStatus(response).Success, 'expected main request to fail because message Id is "NULL"');
	}
	/**
	 * check request without customers
	 */
	static testMethod void testMessageWithoutCustomers () {
		//'NULL' message Id
		SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
		//try {
			SFDCMyJLCustomerTypes.CustomerResponseType response = MyJL_LeaveLoyaltyAccountHandler.process(request);
			
		//} catch (Exception e) {
			//System.assert(false, 'Exception must not be thrown. Result: ' + e);
		//}
		System.assertEquals(false, MyJL_TestUtil.getResponseStatus(response).Success, 'expected main request to fail because customer array is blank');
		System.assertEquals(MyJL_TestUtil.getResponseStatus(response).Code,'NO_CUSTOMER_DATA');
		
		/*check customer Id Blank or empty in request*/
		SFDCMyJLCustomerTypes.CustomerRequestType request1 = MyJL_TestUtil.prepareBlankRequest();
		
		final String email = 'testSingleRecordInsert@example.com';
		SFDCMyJLCustomerTypes.Customer customer = MyJL_TestUtil.getCustomer(null, new Map<String, Object>{'EmailAddress' => email});
		request1.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer};

		SFDCMyJLCustomerTypes.CustomerResponseType response1 = MyJL_LeaveLoyaltyAccountHandler.process(request1);
		System.assertEquals((response1.ActionRecordResults[0]).Code,'CUSTOMERID_NOT_PROVIDED');
		
	}
	
	/* check if no customer profile found exists*/
	
	static testmethod void testNoContactProfilerecordExists(){
		
		SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
		
		final String email = 'testSingleRecordInsert@example.com';
		SFDCMyJLCustomerTypes.Customer customer = MyJL_TestUtil.getCustomer('545', new Map<String, Object>{'EmailAddress' => email});
		request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer};

		SFDCMyJLCustomerTypes.CustomerResponseType response = MyJL_LeaveLoyaltyAccountHandler.process(request);
		System.assertEquals(true, MyJL_TestUtil.getResponseStatus(response).Success, 'expected main request to fail because customer array is blank');
		
		//System.assertEquals(MyJL_TestUtil.getResponseStatus(response).Code,'CUSTOMERID_NOT_FOUND');
		System.assertEquals((response.ActionRecordResults[0]).Code,'LOYALTY_ACCOUNT_NOT_FOUND');
	}
	
	/* check if no loyalty account exists*/
	
	static testmethod void testNoLoyaltyAccountExists(){
		Contact contact = new Contact();
		contact.firstname='JL';
		contact.LastName='.com';
		insert contact;
		
		Contact_Profile__c cprofile = new Contact_Profile__c();
		cprofile.Contact__c = contact.id;
		cprofile.Shopper_ID__c = '3545';
		cprofile.Name = 'testCP';
		cprofile.SourceSystem__c = MyJL_Util.SOURCE_SYSTEM;
		insert cprofile;
		
		SFDCMyJLCustomerTypes.CustomerRequestType request1 = MyJL_TestUtil.prepareBlankRequest();
		
		final String email = 'testSingleRecordInsert@example.com';
		SFDCMyJLCustomerTypes.Customer customer = MyJL_TestUtil.getCustomer('3545', new Map<String, Object>{'EmailAddress' => email});
		request1.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer};

		SFDCMyJLCustomerTypes.CustomerResponseType response1 = MyJL_LeaveLoyaltyAccountHandler.process(request1);
		System.assertEquals(true, MyJL_TestUtil.getResponseStatus(response1).Success, 'expected main request to fail because customer array is blank');
		
		//System.assertEquals(MyJL_TestUtil.getResponseStatus(response1).Code,'LOYALTY_ACCOUNT_NOT_FOUND');
		System.assertEquals((response1.ActionRecordResults[0]).Code,'LOYALTY_ACCOUNT_NOT_FOUND');
	}

	/* check if no Active loyalty account exists*/
	static testmethod void testNoActiveLoyaltyAccountExists(){
		Contact contact = new Contact();
		contact.firstname='JL';
		contact.LastName='.com';  //<<001>>
		contact.Shopper_ID__c = '3212';  //<<001>>
		contact.SourceSystem__c = MyJL_Util.SOURCE_SYSTEM;  //<<001>>
		insert contact;
		
		//Contact_Profile__c cprofile = new Contact_Profile__c();  <<001>>
		//cprofile.Contact__c = contact.id;  <<001>>
		//cprofile.Shopper_ID__c = '3212';  <<001>>
		//cprofile.Name = 'testCP';  <<001>>
		//cprofile.SourceSystem__c = MyJL_Util.SOURCE_SYSTEM;  <<001>>
		//insert cprofile;  <<001>>
		
		Loyalty_Account__c lAccount = new Loyalty_Account__c();
		lAccount.contact__c = contact.id;  //<<001>>
		lAccount.IsActive__c = false;
		insert lAccount;
		
		SFDCMyJLCustomerTypes.CustomerRequestType request1 = MyJL_TestUtil.prepareBlankRequest();
		
		final String email = 'testSingleRecordInsert@example.com';
		final String email1 = 'testSingleRecordInsert@example1.com';
		SFDCMyJLCustomerTypes.Customer customer = MyJL_TestUtil.getCustomer('3212', new Map<String, Object>{'EmailAddress' => email});
		SFDCMyJLCustomerTypes.Customer customer1 = MyJL_TestUtil.getCustomer('3212', new Map<String, Object>{'EmailAddress' => email1});
		
		request1.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer,customer1};

		SFDCMyJLCustomerTypes.CustomerResponseType response1 = MyJL_LeaveLoyaltyAccountHandler.process(request1);
		System.assertEquals(true, MyJL_TestUtil.getResponseStatus(response1).Success, 'expected main request to fail because customer array is blank');
		
		System.assertEquals((response1.ActionRecordResults[0]).Code,'NO_ACTIVE_LOYALTY_ACCOUNT_FOUND');
		System.assertEquals((response1.ActionRecordResults[1]).Code,'CUSTOMERID_NOT_UNIQUE');
		//System.assertEquals(MyJL_TestUtil.getResponseStatus(response1).Code,'NO_ACTIVE_LOYALTY_ACCOUNT_FOUND');
	}

	/**
	* check Leave web service
	
	*/
	static testmethod void testLeaveLoyaltyAccount(){
		
		Contact contact = new Contact();
		contact.firstname='JL';
		contact.LastName='.com';
		contact.Shopper_ID__c = '321';
		contact.SourceSystem__c = MyJL_Util.SOURCE_SYSTEM;
		insert contact;
		
		//Contact_Profile__c cprofile = new Contact_Profile__c();  <<001>>
		//cprofile.Contact__c = contact.id;  <<001>>
		//cprofile.Shopper_ID__c = '321';  <<001>>
		//cprofile.Name = 'testCP';  <<001>>
		//cprofile.SourceSystem__c = MyJL_Util.SOURCE_SYSTEM;  <<001>>
		//insert cprofile;  <<001>>
		
		Loyalty_Account__c lAccount = new Loyalty_Account__c();
		lAccount.contact__c = contact.id;
		lAccount.IsActive__c = true;
		insert lAccount;
		
		System.assertEquals(1, [select count() from Loyalty_Account__c where contact__c = :contact.id and IsActive__c = true], 
								'Expected exactly 1 record of Loyalty_Account__c to be created ' );
		
		/* check for valid deactivation */
		
		SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
		
		final String email = 'testSingleRecordInsert@example.com';
		SFDCMyJLCustomerTypes.Customer customer = MyJL_TestUtil.getCustomer('321', new Map<String, Object>{'EmailAddress' => email});
		request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer};

		MyJL_LeaveLoyaltyAccountHandler.process(request);
		//check main response PASS
		
		System.assertEquals(1, [select count() from Loyalty_Account__c where contact__c = :contact.id and IsActive__c = false], 
								'Expected exactly 1 record of Loyalty_Account__c to be deactivated ' );
		
		/* check for duplicate email address */
		
	}
	
	/**
	 * check what happens if a request with existing ShopperId comes in
	 */
	static testMethod void testSingleDuplicateShopperIdInsert () {
		
		testLeaveLoyaltyAccount();
		SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
		
		final String email = 'testSingleRecordInsert@example.com';
		SFDCMyJLCustomerTypes.Customer customer = MyJL_TestUtil.getCustomer('321', new Map<String, Object>{'EmailAddress' => email});
		request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer};

		
		//check main response PASS
		SFDCMyJLCustomerTypes.CustomerResponseType response = MyJL_LeaveLoyaltyAccountHandler.process(request);
		//check main response OK
		System.assert(MyJL_TestUtil.getResponseStatus(response).Success, 'expected main request to succeed');
		//check individual response = FAIL because email is a duplicate
		System.assertEquals(false, MyJL_TestUtil.getCustomerResponseStatus(response, 0).Success, 'expected second customer record to fail because it is a duplicate');
		
		
		
	}
	
    static testmethod void testLeavePartnershipScenario(){
		
		Contact contact = new Contact();
		contact.firstname='JL';
		contact.LastName='.com';
		contact.Shopper_ID__c = '321';
		contact.SourceSystem__c = MyJL_Util.SOURCE_SYSTEM;
		insert contact;
			
		
		Loyalty_Account__c lAccount = new Loyalty_Account__c();
		lAccount.contact__c = contact.id;
		lAccount.IsActive__c = true;
        lAccount.Customer_Loyalty_Account_Card_ID__c = '12345687891011121345';
        lAccount.Membership_Number__c = '452136789521';
		insert lAccount;
        
        Loyalty_Card__c myJLCard = new Loyalty_Card__c(Loyalty_Account__c = lAccount.Id, Name = 'myJL-card-id', Card_Type__c = MyJL_Const.MY_JL_CARD_TYPE);
        insert(myJLCard);
        
        Loyalty_Card__c partnershipCard = new Loyalty_Card__c(Loyalty_Account__c = lAccount.Id, Name = 'partnershipCard', Card_Type__c = MyJL_Const.MY_PARTNERSHIP_CARD_TYPE);
        insert(partnershipCard);
                
        
        Test.startTest();
      
		System.assertEquals(1, [select count() from Loyalty_Account__c where contact__c = :contact.id and IsActive__c = true], 
								'Expected exactly 1 record of Loyalty_Account__c to be created ' );
		
		
		SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
		
		final String email = 'testSingleRecordInsert@example.com';
		SFDCMyJLCustomerTypes.Customer customer = MyJL_TestUtil.getCustomer('321', new Map<String, Object>{'EmailAddress' => email});
		request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer};

		MyJL_LeaveLoyaltyAccountHandler.process(request);
		
		System.assertEquals(1, [select count() from Loyalty_Account__c where contact__c = :contact.id and IsActive__c = false], 
								'Expected exactly 1 record of Loyalty_Account__c to be deactivated ' );
        
		Loyalty_Account__c resultedAccount = [SELECT Id, Number_Of_Partnership_Cards__c, MyJL_Old_Membership_Number__c, MyJL_Old_Barcode__c, Membership_Number__c, Name, Customer_Loyalty_Account_Card_ID__c
                                              FROM Loyalty_Account__c
                                              WHERE contact__c = :contact.id];
        System.assertEquals(resultedAccount.MyJL_Old_Membership_Number__c, resultedAccount.Name);
        System.assertEquals(resultedAccount.MyJL_Old_Membership_Number__c, resultedAccount.Membership_Number__c);
        System.assertEquals(resultedAccount.MyJL_Old_Barcode__c, resultedAccount.Customer_Loyalty_Account_Card_ID__c);
		
        Loyalty_Card__c resultedPartnershipCard = [SELECT Id, Disabled__c 
                                                   FROM Loyalty_Card__c
                                                   WHERE Card_Type__c = : MyJL_Const.MY_PARTNERSHIP_CARD_TYPE];
        System.assertEquals(true, resultedPartnershipCard.Disabled__c);
        Test.stopTest();
		
	}
}