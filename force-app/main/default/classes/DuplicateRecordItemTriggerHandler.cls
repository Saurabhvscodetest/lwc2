/******************************************************************************
* @author       Matt Povey
* @date         10/12/2015
* @description  Handler class containing all logic relating to DuplicateRecordItem 
*				updates from triggers.
*
* EDIT RECORD
*
* LOG     DATE        Author	JIRA		COMMENT      
* 001     10/12/2015  MP		COPT-485		Original code
*
*******************************************************************************
*/
public without sharing class DuplicateRecordItemTriggerHandler {
	@testVisible private static DateTime dbxCreateDateTime = DateTime.now().addSeconds(-20); // This 20 second window is completely arbitrary
    /**
     * mainEntry method called from the Trigger.
	 * @params:	Standard Trigger context variables. 
     */
    public static void mainEntry(Boolean isBefore, 
                               Boolean isDelete, Boolean isAfter, 
                               Boolean isInsert, Boolean isUpdate, 
                               Boolean isExecuting, List<DuplicateRecordItem> newList, 
                               Map<ID, Object> inNewMap, List<DuplicateRecordItem> oldList, 
                               Map<Id, Object> inOldMap) {

		Map<ID, DuplicateRecordItem> newMap = (Map<ID, DuplicateRecordItem>) inNewMap;
		Map<ID, DuplicateRecordItem> oldMap = (Map<ID, DuplicateRecordItem>) inOldMap;
	
		if (isInsert && isBefore) {
			Map<Id, DuplicateRecordItem> contactToDuplicateRecordItemXrefMap = new Map<Id, DuplicateRecordItem>();
			for (DuplicateRecordItem dri : newList) {
				String recordIdString = dri.RecordId;
				if (recordIdString.startsWith('003')) {
					contactToDuplicateRecordItemXrefMap.put(dri.RecordId, dri);
				}
			}
			if (!contactToDuplicateRecordItemXrefMap.isEmpty()) {
				List<Contact> contactList = [SELECT Id, CreatedDate, SystemModstamp FROM Contact WHERE Id IN :contactToDuplicateRecordItemXrefMap.keySet()];
				for (Contact c : contactList) {
					if (c.CreatedDate > dbxCreateDateTime) {
						contactToDuplicateRecordItemXrefMap.get(c.Id).Create_Record_Duplicate__c = true;
					} else if (c.SystemModstamp > dbxCreateDateTime) {
						contactToDuplicateRecordItemXrefMap.get(c.Id).Edit_Record_Duplicate__c = true;
					}
				}
			}
		}
	}
}