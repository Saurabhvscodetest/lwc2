//  Edit    Date        Author      Comment
//  001     19/02/15    NTJ         MJL-1570 - Add method to detect whether migration in progress
//  002     19/02/15    NTJ         MJL-1572 - Salesforce Maintenance Window support
//  003     09/03/15    NTJ         MJL-1610 - ShopperId case must be preserved
//  004     15/04/15    NTJ         MJL-1790 - Do not do a SELECT WHERE x IN set, if we have an empty set
//  005     20/04/15    ntj         Remove Debug.log
//  006     20/04/15    NTJ         MJL-1957 - add method to get ms for a date time
//  007     27/06/16    NA          Decommissioning contact profile
//  008     21/09/16    YM			OCCO-20055 - Adding Alternative Telephone to the Update Customer Service.


public with sharing class MyJL_Util {

    public static final String SHOPPER_IDENTITY = 'Shopper';

    public static String SOURCE_SYSTEM = System.Label.MyJL_JL_comSourceSystem;

    /*
    ##############################################################################################################
    ## Base abstract EBO Customer wrapper - each MyJL service implements this specific for the needs in context
    ##############################################################################################################
    */ 
    
    /**
     * base class for all MyJL Customer Wrappers
     * AG note: moved it here from SFDCMyJLCustomerTypes because SFDCMyJLCustomerTypes is generated based on WSDL
     * and can be re-generated at any time
     */
    public abstract class CustomerWrapper extends CustomerManagement.CustomerWrapper {
        public String messageId;

        public abstract SFDCMyJLCustomerTypes.Customer getCustomerInput();
        public abstract SFDCMyJLCustomerTypes.CustomerRecordResultType getCustomerResult();
        public virtual String getStackTrace() { return null; }

        /**
         * @return: lower case Shopper Id, or null if Id is not defined or has value 'NULL'
         */
        public virtual override String getCustomerId() {
            return MyJL_Util.getCustomerId(getCustomerInput());
        }

        /**
         * @return: lower case Email, or null if email is not defined
         */
        public virtual override String getEmailAddress() {
            return MyJL_Util.getEmailAddress(getCustomerInput());
        }

        public virtual override String getFirstName() {
            final SFDCMyJLCustomerTypes.Customer requestCustomer = this.getCustomerInput();
            return null != requestCustomer.Person? requestCustomer.Person.FirstName : null;
        }

        public virtual override String getLastName() {
            final SFDCMyJLCustomerTypes.Customer requestCustomer = this.getCustomerInput();
            return null != requestCustomer.Person? requestCustomer.Person.LastName : null;
        }

        public SFDCMyJLCustomerTypes.Address getBillingAddress() {
            final SFDCMyJLCustomerTypes.Customer requestCustomer = this.getCustomerInput();
            return (SFDCMyJLCustomerTypes.Address)getCustomerValue(requestCustomer, 'Address.Billing');
        }

        public virtual override String getAddressLine1() {
            SFDCMyJLCustomerTypes.Address address = this.getBillingAddress();
            return null == address ? null : address.AddressLine1;
        }

        public virtual override String getPostalCode() {
            SFDCMyJLCustomerTypes.Address address = this.getBillingAddress();
            return null == address ? null : address.PostalCode;
        }

        // MJL-1693
        public virtual override void setMatchMethod(final String matchMethod) {
            // implement if you want
        }

        public virtual override Date getBirthDate() {
            final SFDCMyJLCustomerTypes.Customer requestCustomer = this.getCustomerInput();
            return null != requestCustomer.Person? requestCustomer.Person.DateOfBirth : null;
        }
        public virtual override String getCity() {
            SFDCMyJLCustomerTypes.Address address = this.getBillingAddress();
            return null == address ? null : address.City;
        }
        public virtual override String getCountry() {
            SFDCMyJLCustomerTypes.Address address = this.getBillingAddress();
            return null == address ? null : address.ISOCountry;
        }
        public virtual override String getState() {
            SFDCMyJLCustomerTypes.Address address = this.getBillingAddress();
            return null == address ? null : address.County;
        }
        public virtual override String getInitials() {
            final SFDCMyJLCustomerTypes.Customer requestCustomer = this.getCustomerInput();
            return null != requestCustomer.Person? requestCustomer.Person.MiddleNames : null;
        }
        public virtual override String getPhone() {
            final SFDCMyJLCustomerTypes.Customer requestCustomer = this.getCustomerInput();
            String telNumber = getCustomerValue(requestCustomer, 'Telephone.TelephoneNumber') == null ? '' : String.valueOf(getCustomerValue(requestCustomer, 'Telephone.TelephoneNumber'));
            return telNumber;
        }
        //This Method is to the Alternative Telephone from the Customer node.
        public virtual String getAlternativePhone()	{
           final SFDCMyJLCustomerTypes.Customer requestCustomer = this.getCustomerInput();
           String altPhone = String.valueOf(getCustomerValue(requestCustomer, 'Telephone.Alternative'));
           return altPhone==null ? '' : altPhone;
        }
        public virtual override String getSalutation() {
            final SFDCMyJLCustomerTypes.Customer requestCustomer = this.getCustomerInput();
            return null != requestCustomer.Person? requestCustomer.Person.Salutation : null;
        }

    }
    
      public abstract class TransactionWrapper  {
        public String messageId;

        public abstract SFDCKitchenDrawerTypes.KDTransaction getKDTransactionInput();
        public abstract SFDCKitchenDrawerTypes.ActionResult getActionResult();

        /**
         * @return: lower case Shopper Id, or null if Id is not defined or has value 'NULL'
         */
        public virtual String getTransactionId() {
            return MyJL_Util.getTransactionId(getKDTransactionInput());
        }

        public virtual String getStackTrace() {
            return null;
        }

    }


    /*
    ##############################################################################################################
    ## Initialise WS response MyJL objects
    ##############################################################################################################
    */ 

    /* Return default instantiated CustomerRecordResultType (Customer level) with Success set to true */
    public static SFDCMyJLCustomerTypes.CustomerRecordResultType getSuccessResult() {
        SFDCMyJLCustomerTypes.CustomerRecordResultType customerRecordResultType = new SFDCMyJLCustomerTypes.CustomerRecordResultType();

        customerRecordResultType.Code = null;
        customerRecordResultType.Message = null;
        customerRecordResultType.Type_x = null;
        customerRecordResultType.Success = true;

        return customerRecordResultType;
    }
    
    /* Return default instantiated ResultStatus (Header level) with Success set to true */
    public static SFDCMyJLCustomerTypes.ResultStatus getSuccessResultStatus() {
        SFDCMyJLCustomerTypes.ResultStatus resultStatus = new SFDCMyJLCustomerTypes.ResultStatus();

        resultStatus.Code = null;
        resultStatus.Message = null;
        resultStatus.Type_x = null;
        resultStatus.Success = true;

        return resultStatus;
    }
    
    /* Return default instantiated ActionResponse (Response level) with Success set to true */
    public static SFDCKitchenDrawerTypes.ActionResponse getSuccessActionResponse() {
        SFDCKitchenDrawerTypes.ActionResponse actionResponse = new SFDCKitchenDrawerTypes.ActionResponse();

        actionResponse.messageId = null;
        actionResponse.success = true;
        actionResponse.resultType = null;
        actionResponse.message = null;
        actionResponse.results = null;

        return actionResponse;
    }
    
    /* Return default instantiated ActionResult (Header level) with Success set to true */
    public static SFDCKitchenDrawerTypes.ActionResult getSuccessActionResultStatus() {
        SFDCKitchenDrawerTypes.ActionResult resultStatus = new SFDCKitchenDrawerTypes.ActionResult();

        resultStatus.code = null;
        resultStatus.resultType = null;
        resultStatus.message = null;
        resultStatus.success = true;
        resultStatus.kdTransaction = null;
        return resultStatus;
    }
    
    /*
    ##############################################################################################################
    ## Error Message helpers
    ##############################################################################################################
    */ 
    
   public static SFDCMyJLCustomerTypes.CustomerRecordResultType populateErrorDetails(final MyJL_Const.ERRORCODE code, final SFDCMyJLCustomerTypes.CustomerRecordResultType customerRecordResultType) {
        //system.debug('populateErrorDetails start [code: ' + code + ', customerRecordResultType: ' + customerRecordResultType + ']');
        
        customerRecordResultType.Code = code.name();
        customerRecordResultType.Success = false;        
        
        try {
            //Get the error message & type from Custom Settings Object
            MyJL_Error_Messages__c em = getErrorMessage(code);
            customerRecordResultType.Message = em.Error_Message__c;
            customerRecordResultType.Type_x = em.Error_Type__c;
        } catch(Exception e) {
            customerRecordResultType.Message = MyJL_Const.ERROR_NOT_CONFIGURED_MESSAGE;
            customerRecordResultType.Type_x = MyJL_Const.ERROR_NOT_CONFIGURED_TYPE;
        }
        
        //system.debug('populateErrorDetails return [customerRecordResultType: ' + customerRecordResultType + ']');
        
        return customerRecordResultType;
    }

    public static MyJL_Error_Messages__c getErrorMessage(final MyJL_Const.ERRORCODE code) {
        final Map<String, MyJL_Error_Messages__c> errorMessages = MyJL_Error_Messages__c.getAll();
        if(null != errorMessages && errorMessages.containsKey(code.name())) {
            return errorMessages.get(code.name());
        } else {
            return new MyJL_Error_Messages__c(Error_Message__c = MyJL_Const.ERROR_NOT_CONFIGURED_MESSAGE, Name = null, Error_Type__c = MyJL_Const.ERROR_NOT_CONFIGURED_TYPE);
        }
    }
    
    public static SFDCMyJLCustomerTypes.ResultStatus populateErrorDetailsHeader(final MyJL_Const.ERRORCODE code, final SFDCMyJLCustomerTypes.ResultStatus resultStatus) {
        //system.debug('populateErrorDetailsHeader start [code: ' + code + ', resultStatus: ' + resultStatus + ']');
        
        resultStatus.Code = code.name();
        resultStatus.Success = false;        
        
        try {
            //Get the error message & type from Custom Settings Object
            Map<String, MyJL_Error_Messages__c> errorMessages = MyJL_Error_Messages__c.getAll();
            if(errorMessages.containsKey(code.name())) {
                MyJL_Error_Messages__c em = errorMessages.get(code.name());
                resultStatus.Message = em.Error_Message__c;
                resultStatus.Type_x = em.Error_Type__c;
            } else {
                resultStatus.Message = MyJL_Const.ERROR_NOT_CONFIGURED_MESSAGE;
                resultStatus.Type_x = MyJL_Const.ERROR_NOT_CONFIGURED_TYPE;
            }
        } catch(Exception e) {
            resultStatus.Message = MyJL_Const.ERROR_NOT_CONFIGURED_MESSAGE;
            resultStatus.Type_x = MyJL_Const.ERROR_NOT_CONFIGURED_TYPE;
        }
        
        //system.debug('populateErrorDetailsHeader return [resultStatus: ' + resultStatus + ']');
        
        return resultStatus;
    }
    
    public static MyJL_Rejection__c logRejection(MyJL_Const.ERRORCODE code, String email, String shopperId, String service) {
        MyJL_Rejection__c rec = new MyJL_Rejection__c();
        MyJL_Util.setNotBlank(rec, 'Id__c', shopperId); //wrapper.getCustomerId()
        MyJL_Util.setNotBlank(rec, 'EmailAddress__c', email); // email
        MyJL_Util.setNotBlank(rec, 'Rejection_Code__c', code.name());
        //get description from Custom Setting
        String description = service + ': ' + MyJL_Util.getErrorMessage(code).Error_Message__c;
        description = description.abbreviate(255);
        MyJL_Util.setNotBlank(rec, 'Rejection_Description__c', description);
        insert rec;
        return rec;
    }

    public static SFDCKitchenDrawerTypes.ActionResponse populateKDErrorDetailsHeader(final MyJL_Const.ERRORCODE code, final SFDCKitchenDrawerTypes.ActionResponse resultStatus) {
        //system.debug('populateErrorDetailsHeader start [code: ' + code + ', resultStatus: ' + resultStatus + ']');
        
        resultStatus.code = code.name();
        //resultStatus.message = code.name();
        resultStatus.Success = false;         
        
        try {
            //Get the error message & type from Custom Settings Object
            Map<String, MyJL_Error_Messages__c> errorMessages = MyJL_Error_Messages__c.getAll();
            if(errorMessages.containsKey(code.name())) {
                MyJL_Error_Messages__c em = errorMessages.get(code.name());
                resultStatus.Message = em.Error_Message__c;
                resultStatus.resultType = em.Error_Type__c;
            } else {
                resultStatus.Message = MyJL_Const.ERROR_NOT_CONFIGURED_MESSAGE;
                resultStatus.resultType = MyJL_Const.ERROR_NOT_CONFIGURED_TYPE;
            }
        } catch(Exception e) {
            resultStatus.Message = MyJL_Const.ERROR_NOT_CONFIGURED_MESSAGE;
            resultStatus.resultType = MyJL_Const.ERROR_NOT_CONFIGURED_TYPE;
        }
        
        //system.debug('populateErrorDetailsHeader return [resultStatus: ' + resultStatus + ']');
        
        return resultStatus;
    }
    
    public static SFDCKitchenDrawerTypes.ActionResult populateKDErrorDetails(final MyJL_Const.ERRORCODE code, final SFDCKitchenDrawerTypes.ActionResult actionResult) {
        //system.debug('populateErrorDetails start [code: ' + code + ', customerRecordResultType: ' + actionResult + ']');
        
        actionResult.code = code.name();
        actionResult.success = false;        
        
        try {
            //Get the error message & type from Custom Settings Object
            MyJL_Error_Messages__c em = getErrorMessage(code);
            actionResult.message = em.Error_Message__c;
            actionResult.resultType = em.Error_Type__c;
        } catch(Exception e) {
            actionResult.message = MyJL_Const.ERROR_NOT_CONFIGURED_MESSAGE;
            actionResult.resultType = MyJL_Const.ERROR_NOT_CONFIGURED_TYPE;
        }
        
        //system.debug('populateKDErrorDetails return [actionResult: ' + actionResult + ']');
        
        return actionResult;
    }
    

    /*
    ##############################################################################################################
    ## Service idempotency check helper
    ##############################################################################################################
    */ 

    /*  
    Check if the messageId already exists in combination with the CURRENT API USER
    Assumption here is that each external consumer of the services (be it ATG/ESB or someone else) will use a different set of SFDC Credentials.
    Message ID's are expected to be unique only within the context of a single API User
    */
    public static boolean isMessageIdUnique(final String messageId) {
        //system.debug('isMessageIdUnique start [messageId: ' + messageId + ']');

        try {
            Integer i = 0;
            // MJL-1790
            if (String.isNotBlank(messageId)) {
                i = [SELECT COUNT() FROM Log_Header__c WHERE Message_ID__c = :messageId];
            }

            //system.debug('isMessageIdUnique COUNT() = ' + i);

            if(i <= LogUtil.headerLimit()) {
                return true;
            }
        } catch(Exception e) {
            System.debug('isMessageIdUnique ERROR: ' + e);
        }

        return false;
    }
    
    
    /*
    ##############################################################################################################
    ## Actual utility methods
    ##############################################################################################################
    */ 

    public static Boolean isNull(Object obj) {
        if (null == obj) {
            return true;
        }
        if (obj instanceof String) {
            return 'NULL' == ((String)obj).toUpperCase();
        }
        return false;
    }

    public static Boolean isBlank(String val) {
        return String.isBlank(val) || isNull(val);
    }

    public static String getOrElse(String val, String defaultVal) {
        return String.isBlank(val)? defaultVal : val;
    }

    public static Decimal getOrElse(Decimal val, Decimal defaultVal) {
        return null == val? defaultVal : val;
    }

    /**
      * @return: lower case Customer Id, or null if Id is not defined or has value 'NULL'
      */
    public static String getCustomerId(final SFDCMyJLCustomerTypes.Customer customer) {
        if (null == customer) {
            return null;
        }
        return getShopperId(customer.CustomerID);
    }


    /**
      * @return: lower case Customer Id, or null if Id is not defined or has value 'NULL'
      *
      * MJL-1610 - ShopperId case must be preserved, do not lower case
      */
    public static String getShopperId(final SFDCMyJLCustomerTypes.Identity customerId) {
        final SFDCMyJLCustomerTypes.Identity customerIdObj = customerId;
        if (null != customerIdObj && null != customerIdObj.xrefIdentity && !customerIdObj.xrefIdentity.isEmpty()) {
            //find identity with identityType = 'Shopper'
            SFDCMyJLCustomerTypes.Identity identity = getIdentity(customerId, SHOPPER_IDENTITY);
            // MJL-1610 - don't lower case the value
            // Pre 003 code return null == identity || MyJL_Util.isBlank(identity.ID)? null : identity.ID.toLowerCase().trim();
            return null == identity || MyJL_Util.isBlank(identity.ID)? null : identity.ID.trim();
        }
        return null;
    }
    /**
     * MJL-729
     * find and return xrefIdentity with identityType = 'Shopper'
     */
    public static SFDCMyJLCustomerTypes.Identity getIdentity(final SFDCMyJLCustomerTypes.Identity customerId, final String identityType) {
        final SFDCMyJLCustomerTypes.Identity customerIdObj = customerId;
        if (null != customerIdObj && null != customerIdObj.xrefIdentity && !customerIdObj.xrefIdentity.isEmpty()) {
            //find identity with identityType = 'Shopper'
            for(SFDCMyJLCustomerTypes.Identity identity : customerIdObj.xrefIdentity) {
               if (SHOPPER_IDENTITY == identity.identityType) {
                   return identity;
               }
            }
        }
        return null;
    }

    /**
     * MJL-784
     * set Id value to xrefIdentity with identityType = 'Shopper'
     *
     * <sfd:Customer>
     *     <sfd1:CustomerID>
     *       <sfd1:xrefIdentity>
     *           <sfd1:ID>Join-positive-2</sfd1:ID>
     *           <sfd1:identityType>Shopper</sfd1:identityType>
     *       </sfd1:xrefIdentity>
     *     </sfd1:CustomerID>
     * </sfd:Customer>
     *
     */
    public static void setShopperId(final SFDCMyJLCustomerTypes.Customer customer, final String customerIdString) {
        if (null == customer.CustomerID) {
            customer.CustomerID = new SFDCMyJLCustomerTypes.Identity();
        }
        setShopperId(customer.CustomerID, customerIdString);
    }

    // MJL-1610 - ShopperId case must be preserved
    public static void setShopperId(final SFDCMyJLCustomerTypes.Identity customerId, final String customerIdString) {
        SFDCMyJLCustomerTypes.Identity shopperIdentity = getIdentity(customerId, SHOPPER_IDENTITY);
        if (null == shopperIdentity) {
            shopperIdentity = new SFDCMyJLCustomerTypes.Identity();
            shopperIdentity.identityType = MyJL_Util.SHOPPER_IDENTITY;
            if (null == customerId.xrefIdentity) {
                customerId.xrefIdentity = new List<SFDCMyJLCustomerTypes.Identity>();
            }
            customerId.xrefIdentity.add(shopperIdentity);

        }
        // pre-003 code
        // shopperIdentity.ID = MyJL_Util.getOrElse(customerIdString, '').toLowerCase();
        shopperIdentity.ID = MyJL_Util.getOrElse(customerIdString, '');
    }

    /**
      * @return: lower case Customer Id, or null if not defined or has value 'NULL'
      */
    // MJL-1610 - ShopperId case must be preserved
    public static String getCustomerId(final Contact contactProfile) {    //<<007>>
        if(contactProfile.Shopper_ID__c == null || MyJL_Util.isBlank(contactProfile.Shopper_ID__c)) {
            return null;
        }
        // pre-003 code
        //return contactProfile.Shopper_ID__c.toLowerCase().trim();
        return contactProfile.Shopper_ID__c.trim();
    }

    /**
      * @return: lower case Customer Id, or null if not defined or has value 'NULL'
      */
    public static String getCustomerId(final Potential_Customer__c limitedCustomer) {
        if(limitedCustomer.Shopper_ID__c == null || MyJL_Util.isBlank(limitedCustomer.Shopper_ID__c)) {
            return null;
        }
        // pre-003 code
        //return limitedCustomer.Shopper_ID__c.toLowerCase();
        return limitedCustomer.Shopper_ID__c;
    }

    public static void setNotBlank(final SObject obj, final String fName, final Object val) {
        //system.debug('setNotBlank: fName: '+fName);
        if (null != val) {
            if (val instanceof String) {
                String str = (String)val;
                //system.debug('setNotBlank: val: '+str);
                if ('NULL' == str.toUpperCase()) {
                    obj.put(fName, null);
                } else if (!String.isBlank(str)){
                    obj.put(fName, val);
                }
            } else {
                obj.put(fName, val);
            }
        }
    }

    public static void setNotBlankIfTargetNull(final SObject obj, final String fName, final Object val) {
        Object target = obj.get(fName);
        if (target == null) {
            setNotBlank(obj, fName, val);
        }
    }

    /**
     * @return: lower case Email, or null if email is not defined
     */
    public static String getEmailAddress(final SFDCMyJLCustomerTypes.Customer customer) {
        string emailAddress = (String) getCustomerValue(customer, 'PartyContactMethod.EmailAddress');
        return (emailAddress != null ? emailAddress.trim() : emailAddress);
    }

    public static String getTelephone(final SFDCMyJLCustomerTypes.Customer customer) {
        string telephone = (String) getCustomerValue(customer, 'PartyContactMethod.Telephone');
        return (telephone != null ? telephone.trim() : telephone);
    }

    public static Object getCustomerValue(final SFDCMyJLCustomerTypes.Customer customer, final String path) {
        if (null == customer) {
            return null;
        }
        //<YM008>:Start - This code is for retrieving Alternative Phonenumber from the Customer Object of Response.
        if(path.equalsIgnoreCase('Telephone.Alternative') && customer.PartyContactMethod!=null ) {
        	for(SFDCMyJLCustomerTypes.PartyContactMethod partyContactMethod: customer.PartyContactMethod){
        		SFDCMyJLCustomerTypes.Code purposeTypeCode = partyContactMethod.ContactPurposeTypeCode;
        		SFDCMyJLCustomerTypes.Telephone          telephone   = partyContactMethod.Telephone;
        		if(purposeTypeCode!=null && purposeTypeCode.Code!=null &&telephone!=null && telephone.TelephoneNumber!=null && purposeTypeCode.Code.equalsIgnoreCase('Alternative')) {
        			return telephone.TelephoneNumber;
        		}
        	}
        	return null;
        } else if ('PartyContactMethod.EmailAddress' == path && customer.PartyContactMethod!=null) {
        	for(SFDCMyJLCustomerTypes.PartyContactMethod partyContactMethod: customer.PartyContactMethod){
            	if(partyContactMethod.EmailAddress!=null && 
            	   partyContactMethod.EmailAddress.EmailAddress!=null) 
            	{
            		return partyContactMethod.EmailAddress.EmailAddress.toLowerCase().trim();
            	}
            }
        }
        else if (path.startsWith('Address.') && customer.PartyContactMethod!=null ) {//'Address.Billing'; 'Address.Other' ('PartyContactMethod.EmailAddress' == path && customer.PartyContactMethod!=null) {
        	String requiredAddressType = path.split('\\.')[1];
        	for (SFDCMyJLCustomerTypes.PartyContactMethod partyContactMethod : customer.PartyContactMethod) {
                    if(partyContactMethod.ContactMethodTypeCode!=null &&
            	   		partyContactMethod.ContactMethodTypeCode.Code!=null &&
            	   		partyContactMethod.ContactPurposeTypeCode!=null &&
            	   		partyContactMethod.ContactPurposeTypeCode.Code!=null && 
            	   		partyContactMethod.Address!=null && 
            	   		partyContactMethod.ContactPurposeTypeCode.Code.equalsIgnoreCase(requiredAddressType)&&
            	   		partyContactMethod.ContactMethodTypeCode.Code.equalsIgnoreCase('Address'))
	            	{
	            		return  partyContactMethod.Address; 
	            	}
            }
        	//system.debug('create new');
            return new SFDCMyJLCustomerTypes.Address();	
         }

        //<YM008>:End
        else if ('CustomerGroup.Name' == path) {
            if (null != customer.CustomerAffiliation && null != customer.CustomerAffiliation) {
                if (customer.CustomerAffiliation.size() > 0 && null != customer.CustomerAffiliation[0]
                        && null != customer.CustomerAffiliation[0].CustomerGroup && customer.CustomerAffiliation[0].CustomerGroup.size() > 0)
                    return customer.CustomerAffiliation[0].CustomerGroup[0].Name;
            }
        } else if ('Telephone.TelephoneNumber' == path && null != customer.PartyContactMethod ) {
                for (SFDCMyJLCustomerTypes.PartyContactMethod method : customer.PartyContactMethod) {
					SFDCMyJLCustomerTypes.Code purposeTypeCode = method.ContactPurposeTypeCode;
					SFDCMyJLCustomerTypes.Telephone telephone = method.Telephone;

					if(purposeTypeCode!=null && purposeTypeCode.Code!=null && telephone !=null && telephone.TelephoneNumber !=null && purposeTypeCode.Code.equalsIgnoreCase('Primary')) {
						return telephone.TelephoneNumber;
					}
                }
        }
        else if ('CustomerAccount.CustomerLoyaltyAccountCard' == path) {
            if (null != customer.CustomerAccount) {
                for (SFDCMyJLCustomerTypes.CustomerAccount account : customer.CustomerAccount) {
                    if (null != account && null != account.CustomerLoyaltyAccountCard) {
                        for(SFDCMyJLCustomerTypes.CustomerLoyaltyAccountCard card : account.CustomerLoyaltyAccountCard) {
                            return card;
                        }
                    }
                }
            }
            return new SFDCMyJLCustomerTypes.CustomerLoyaltyAccountCard();
        }
		else if ('CustomerPreference.VoucherPreference' == path && customer.CustomerPreference!=null) {

			for(SFDCMyJLCustomerTypes.CustomerPreference  preference : customer.CustomerPreference ){
        		if( preference != null && preference.PreferenceTypeCode != null){
        			SFDCMyJLCustomerTypes.Code prefTypeCode = preference.PreferenceTypeCode;
        			for(SFDCMyJLCustomerTypes.Code code : prefTypeCode.xrefCode){
        				if(code.ID == MyJL_Const.MY_JL_VOUCHER_PREFERENCE_KEY){
        					return preference.Value;
        				}
        			}
        		}
        	}
		}

        return null;
    }

    /**
     * @return: the checksum value for an input integer, Bulkified version
     */
    public static map<long, integer> calculateChecksum(set<long>  inputSet){

        map<long, integer> returnMap = new map<long, integer>();

        map<integer, integer> indexMultipliersMap = new map<integer, integer>{

            1 => 1,
            2 => 3,
            3 => 1,
            4 => 3,
            5 => 1,
            6 => 3,
            7 => 1,
            8 => 3,
            9 => 1,
            10 => 3
        };


        for(long inputInteger : inputSet){

            integer checkSum;

            String stringEquiv = String.valueOf(inputInteger);
            list<string> stringEquivSplits = stringEquiv.split('');
            integer runningTotal = 55;
            integer runningCounter = 0;
            for(integer i = 0; i < stringEquivSplits.size() ; i++) {

                if(stringEquivSplits[i] != null && stringEquivSplits[i] != '' && stringEquivSplits[i] != ' '){

                    runningCounter++;
                    try{

                        integer inputIntegerSplit = Integer.valueOf(stringEquivSplits[i]);
                        integer indexMultiplier = indexMultipliersMap.get(runningCounter);
                        runningTotal += inputIntegerSplit * indexMultiplier;

                    } catch(Exception e){

                        System.debug(e.getMessage());
                    }
                }
            }

            integer modOf10 = math.mod(runningTotal, 10);
            if(modOf10 > 0)
                checkSum = 10-modOf10;
            else
                checkSum = 0;

            returnMap.put(inputInteger, checkSum);

        }

        return returnMap;
    }

    /**
     * @return: the checksum value for an input integer, Singleton version
     */
    public static integer calculateChecksum(string  inputValue){
        //system.debug('inputValue: '+inputValue);
        map<integer, integer> indexMultipliersMap = new map<integer, integer>{

            1 => 1,
            2 => 3,
            3 => 1,
            4 => 3,
            5 => 1,
            6 => 3,
            7 => 1,
            8 => 3,
            9 => 1,
            10 => 3
        };

        integer checkSum;

        list<string> inputValueSplits = inputValue.split('');
        //system.debug('inputValueSplits: '+inputValueSplits);
        integer runningTotal = 55;
        integer runningCounter = 0;
        for(integer i = 0; i < inputValueSplits.size() ; i++) {

            if(inputValueSplits[i] != null && inputValueSplits[i] != '' && inputValueSplits[i] != ' '){

                runningCounter++;
                //system.debug('runningCounter: '+runningCounter);
                try{
                    Integer inputIntegerSplit = Integer.valueOf(inputValueSplits[i]);
                    //system.debug('inputIntegerSplit: '+inputIntegerSplit);
                    Integer indexMultiplier = indexMultipliersMap.get(runningCounter);
                    if (indexMultiplier != null) {
                        //system.debug('indexMultiplier: '+indexMultiplier);
                        runningTotal += inputIntegerSplit * indexMultiplier;
                        //system.debug('runningTotal: '+runningTotal);
                    }
                } catch(Exception e){
                    System.debug(e.getMessage());
                }
            }
        }

        integer modOf10 = math.mod(runningTotal, 10);
        //system.debug('modOf10: '+modOf10);
        if(modOf10 > 0)
            checkSum = 10-modOf10;
        else
            checkSum = 0;

        //system.debug('checkSum: '+checkSum);
        return checkSum;
    }

    /**
      * @return: lower case Transaction Id, or null if Id is not defined or has value 'NULL'
      */
    public static String getTransactionId(final SFDCKitchenDrawerTypes.KDTransaction kdTransaction) {
        if (null == kdTransaction) {
            return null;
        }
        final String transactionIdObj = kdTransaction.transactionId;
        return MyJL_Util.isBlank(transactionIdObj) ? null : transactionIdObj.toLowerCase().trim();
    }

    /**
      * @return: lower case Transaction Id, or null if not defined or has value 'NULL'
      */
    public static String getTransactionId(final Transaction_Header__c kdTransaction) {
        return MyJL_Util.isBlank(kdTransaction.Receipt_Barcode_Number__c) ? null : kdTransaction.Receipt_Barcode_Number__c.toLowerCase().trim();
    }

    public static String getFullXmlName() {
        String name = CustomSettings.getMyJLSetting().getString('KD_Full_XML_File_Name__c');
        return String.isBlank(name)? 'Transaction Full XML' : name;
    }

    public static String getShortXmlName() {
        String name = CustomSettings.getMyJLSetting().getString('KD_Short_XML_File_Name__c');
        return String.isBlank(name)? 'Transaction Short XML' : name;
    }

    public static Boolean isMigrationMode() {
        return true == CustomSettings.getMyJLSetting().getBoolean('Migration_Mode__c');
    }

    public static Boolean lastModifiedIsLater(DateTime oldDTM, DateTime newDTM) {
        // Assume we will update
        // Only do this check if we are recording last modified
        // true means that we will write the change to Salesforce
        Boolean isLater = MyJL_Util.isMigrationMode();

        // if config is set then we need to investigate the date times on the record, otherwise we just accept the update (so need to set the isLater flag to true)
        if (isLater) {
            if ( oldDTM != null) {
                if (newDTM != null) {
                    //system.debug('new: '+newDTM+' old: '+oldDTM);
                    isLater = (newDTM > oldDTM);
                }
            }
            //system.debug('lastModifiedIsLater: '+isLater);
        } else isLater = true;

        return isLater;
    }

    // MJL-1572 Test whether salesforce is in read-only mode
    public static Boolean inReadOnlyMode() {
        ApplicationReadWriteMode mode = System.getApplicationReadWriteMode();

        return (mode == ApplicationReadWriteMode.READ_ONLY);
    }

    public static List<Loyalty_Account__c> accountsToNotify(List<Loyalty_Account__c> accounts) {
        List<Loyalty_Account__c> accountsToSendOutboundNotifs = new List<Loyalty_Account__c>();
        for (Loyalty_Account__c la:accounts) {
            if (la.contact__c != null) {  //<<007>>
                //system.debug('add: '+la.Id);
                accountsToSendOutboundNotifs.add(la);
            } else {
                //system.debug('defer: '+la.Id);
            }
        }
        return accountsToSendOutboundNotifs;
    }

    //
    // Convert DateTime string with milliseconds into a DateTime with milliseconds
    //
    // params:
    // strDTM of the form yyyy-mm-ddThh:mm:ss.cccZ
    public static DateTime dateTimeString2DateTimeMsecs(String strDTM) {
        //system.debug('strDTM: '+strDTM);
        // need double quotes around the datetime string
        strDTM = '\"'+strDTM+'\"';
        // Now use JSON desrialize to convert the string into Apex class instance, this preserves the milliseconds
        DateTime dt = (DateTime)JSON.deserialize(strDTM, DateTime.class);
        //system.debug('dt: '+dt);
        //system.debug(dt.millisecond());
        return dt;
    }
    
    /**
     * reusable method to remove nulls from a string
     * @params: string value
     * @rtnval: value blanked, if it is null
     */ 
    public static string removeNulls(String inputText) {
        string outputtext = inputText;
        if(inputText == null){
            outputtext = '';
                
        }
        return outputtext.toLowerCase();
    }       

}