public with sharing class CasePrintViewExt 
{
	/*
	Name: 		CasePrintViewExt
	Author:		Shion Abdillah
	Date:		10/07/2015
	Desc: 		
	*/
	
	public Case CaseRec {get;set;}
	public string CaseRecordTypeName {get;set;}
	public List<RecordType> CaseRecordTypes {get;set;}
    public List<CaseComment> MasterCaseComments {get;set;}
    public List<CaseComment> UserCaseComments {get;set;}
    public List<CaseComment> SystemCaseComments {get;set;}
    public Boolean ShowDisplayList {get;set;}
    public Boolean ShowCaseDetails {get;set;}    
    public Boolean ShowRelatedCases {get;set;}
    public Boolean ShowCaseSystemComments {get;set;}    
    public Boolean ShowCaseComments {get;set;}
    public Boolean ShowOpenActivities {get;set;}
    public Boolean ShowActivityHistory {get;set;}
    public Boolean ShowAttachments {get;set;}   
    public Boolean ShowPrintScript {get;set;}   


	
	public CasePrintViewExt(ApexPages.StandardController stdController) 
	{
        ShowDisplayList = true;
        ShowCaseDetails = true;
    	CaseRec = (Case)stdController.getRecord();        
        FilterCaseComments();    	    	
    }
	
    @TestVisible
    private void FilterCaseComments()
    {
        MasterCaseComments = [SELECT Id, CommentBody, ParentId, CreatedDate, CreatedBy.Name FROM CaseComment WHERE ParentId = :CaseRec.Id ORDER BY CreatedDate DESC];
        SystemCaseComments = new List<CaseComment>();
        UserCaseComments = new List<CaseComment>();
        if(MasterCaseComments.size() > 0)
        {
            System.debug('CaseRec.CaseComments.size():' + MasterCaseComments.size());
            for(CaseComment item : MasterCaseComments)
            {
                if(IsSystemMessage(item.CommentBody))
                {
                    SystemCaseComments.add(item);
                }
                else
                {
                    UserCaseComments.add(item);
                }
            }
        }
    }
    

    public PageReference RenderPreview()
    {
        ShowDisplayList = true;
        FilterCaseComments();
        return null;
    }

    public PageReference PrintInvoke()
    {
        ShowDisplayList = false;
        ShowPrintScript = true;
        return null;
    }

    public PageReference Cancel()
    {
        return null;
    }


    private Boolean IsSystemMessage(string pContainsString)
    {
        Boolean result = false;
        if(pContainsString.contains('ApiUser'))result = true;
        if(pContainsString.contains('Task status change for related task Email: '))result = true;
        if(pContainsString.contains('Case Re-opened'))result = true;
        return result;
    }

}