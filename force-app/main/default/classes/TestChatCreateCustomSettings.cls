/**
 */
@isTest
public class TestChatCreateCustomSettings {

	public static final String A1 = 'Strongly Agree';
	public static final String A2 = 'Agree';
	public static final String A3 = 'Disagree';
	public static final String A4 = 'Strongly Disagree';
	public static final Integer A1_RATING = 4;
	public static final Integer A2_RATING = 3;
	public static final Integer A3_RATING = 2;
	public static final Integer A4_RATING = 1;
	public static final String Q1 = 'Chat is a great way to talk to Customer Services';
	public static final String TEST_CHAT_KEY = 'test-chat-key';
	public static final String REVISION = 'RevX';
	public static final Decimal SHOW_THE_SURVEY = 40.0;
	
	static public void createCustomSettings(Boolean enableFlag) {
		// old style
		/*
		Chat_Survey__c cs = new Chat_Survey__c();
		cs.Name = 'Current';
		cs.Revision__c = REVISION;
		cs.Show_the_Survey__c = SHOW_THE_SURVEY;
		cs.enabled__c = enableFlag;
		insert cs;
		*/
		
		// new style
		Live_Agent_Control_Panel__c cs = new Live_Agent_Control_Panel__c();
		cs.Live_Agent_Survey_Revision__c = REVISION;
		cs.Live_Agent_Show_The_Survey__c = SHOW_THE_SURVEY;
		cs.Live_Agent_Survey_Enabled__c = enableFlag;
		insert cs;
		
		Chat_Survey_Question__c csq = new Chat_Survey_Question__c();	
		csq.Name = 'RevX';
		csq.Answer_1__c = A1;
		csq.Answer_2__c = A2;
		csq.Answer_3__c = A3;
		csq.Answer_4__c = A4;
		csq.Question__c = Q1;
		insert csq;

		Id anyUserID = [SELECT Id FROM User WHERE isActive = true LIMIT 1].id;
		Config_Settings__c c = new Config_Settings__c(Name = 'EMAIL_TO_CASE_USER_ID', Text_Value__c = anyUserID);
		insert c;
	}
	
	static public void createControlPanel(Boolean enableFlag, Boolean triggerFlag, Boolean vfPageOverridesFlag, Decimal showSurveyPercent, Boolean assignmentRulesFlag,
										  Boolean validationRulesFlag, Boolean workflowFlag) {
		Live_Agent_Control_Panel__c cs = new Live_Agent_Control_Panel__c();
		cs.Enable_Live_Agent_VF_Page_Overrides__c = vfPageOverridesFlag;
		cs.Live_Agent_Show_The_Survey__c = showSurveyPercent;
		cs.Live_Agent_Survey_Enabled__c = enableFlag;
		cs.Live_Agent_Survey_Revision__c = REVISION;
		cs.Run_Live_Agent_Assignment_Rules__c = assignmentRulesFlag;
		cs.Run_Live_Agent_Triggers__c = triggerFlag;
		cs.Run_Live_Agent_Validation_Rules__c = validationRulesFlag;
		cs.Run_Live_Agent_Workflow__c = workflowFlag;
		insert cs;
	}
	
}