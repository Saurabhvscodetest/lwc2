global class ScheduleBatchdeleteSecondaryCustomers implements Schedulable {
    global void execute(SchedulableContext sc){
        BAT_DeleteSecondaryCustomer obj = new BAT_DeleteSecondaryCustomer();
        Database.executebatch(obj);
    }    
}