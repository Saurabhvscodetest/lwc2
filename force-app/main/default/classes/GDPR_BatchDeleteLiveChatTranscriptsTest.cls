@isTest
Public class GDPR_BatchDeleteLiveChatTranscriptsTest {
    
    static testmethod void testDeleteLiveChatTranscript()
    {
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        Case testCase;
        Contact testContact;
        System.runAs(runningUser) 
        {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;       
            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        
        System.runAs(testUser) 
        {
            
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;
            
            testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            insert testCase;
                       
            LiveChatVisitor testvisitor=new LiveChatVisitor();
            testvisitor.CreatedDate=System.today();
            insert testvisitor;
            
            Test.startTest();
            List<LiveChatTranscript> livechattranscriptlist=new  List<LiveChatTranscript>();
            for(integer i=0; i<99; i++){
                livechattranscriptlist.add(new LiveChatTranscript(Answer_1__c = 'Test'+i, CreatedDate=System.now().addYears(-6), CaseId= testCase.id, LiveChatVisitorId=testvisitor.id));
            }       
            insert livechattranscriptlist;   
            Database.BatchableContext BC;
            GDPR_BatchDeleteLiveChatTranscripts obj=new GDPR_BatchDeleteLiveChatTranscripts();
            Database.DeleteResult[] Delete_Result = Database.delete(livechattranscriptlist, false);
            obj.start(BC);
            obj.execute(BC,livechattranscriptlist);
            obj.finish(BC);
            DmlException expectedException;
            Test.stopTest();
        }  
    }

 @isTest
    Public static void testSchedule(){
        Test.startTest();
        ScheduleBatchDeleteLiveChatTranscripts obj = NEW ScheduleBatchDeleteLiveChatTranscripts();
        obj.execute(null);  
        Test.stopTest();
    } 
}