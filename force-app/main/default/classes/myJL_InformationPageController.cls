public with sharing class myJL_InformationPageController {
    public static final String ADDRESS_LINE_1 = 'Add1';
    public static final String ADDRESS_LINE_2 = 'Add2';
    public static final String ADDRESS_LINE_3 = 'Add3';
    public static final String ADDRESS_LINE_4 = 'Add4';
    public static final String LOYALTY_ACC_ID = 'laId';
    
    public Contact sourceContact { get; set; }
    public string memNumber {get;set;}
    public string cardNumber {get;set;}
    public string activeMem {get;set;}
    public string myJLActiveTime {get;set;}
    public string myPartnershipActiveTime {get;set;}
    public string deactivtime {get;set;}
    public string sourceconemail {get;set;}
    public string mailingAddress {get;set;}
    public string voucherPreference {get;set;}
    public boolean isPartnershipAccount {get;set;}
    public string partnershipCardPack {get;set;}
    Public String UPDATE_MY_VOUCHER_PREFERENCE = Label.UPDATE_MY_VOUCHER_PREFERENCE;
    
    public string BarcodeLabel {
        get{
            return (isPartnershipAccount)? 'My Partnership Barcode Number' : 'myJL Barcode Number';
        }
        private set;
    }
    
    public string ScreenLabel{
        get{
            return (isPartnershipAccount)? 'My Partnership Information' : 'my John Lewis Infromation';
        }
        private set;
    }
    
    public string title{
        get{
            return (isPartnershipAccount)? 'my Partnership Requests' : 'my John Lewis Requests';
        }
        private set;
    }
    public string buttonLabel{
        get{
            return (isPartnershipAccount)? 'New my Partnership Request' : 'New my John Lewis Request';
        }
        private set;
    }
    
    public list<Case> caseDetails { get; private set;}
    public list<Contact> getCp = new list<contact>();
    public list<Loyalty_Account__c> loyaltyAccounts = new list<Loyalty_Account__c>();
    public id loyactid;
    public id contactid;
    
    private static final String DEFAULT_MAIL_RANGE = 'default';
    public List<SelectOption> mailDateRangeList {get; private set;}
    public List<Direct_Mail_Update__c> directMailList {get; private set;}
    public String selectedMailDateRange {get; set;}
    private Date rangeStartDate;
    private Date rangeEndDate;
    
    public myJL_InformationPageController (ApexPages.StandardController con) {
        
        sourceContact = (Contact) addFieldsToController(con).getRecord();
        loyactid = ApexPages.currentPage().getParameters().get('laId');
        contactid = ApexPages.currentPage().getParameters().get('conId');
        
        mailDateRangeList = new List<SelectOption>();
        mailDateRangeList.add(new selectOption(DEFAULT_MAIL_RANGE,'Issued in last 8 months'));
        Integer thisYear = system.today().year();
        for (Integer i = 0; i < 5; i++) {
            Integer tempYear = thisYear - i;
            mailDateRangeList.add(new selectOption(String.valueOf(tempYear),String.valueOf(tempYear)));
        }
        selectedMailDateRange = DEFAULT_MAIL_RANGE;
        
        processData();
    }
    
    
    public void processData(){
        
        //Retrieve the Contact Profile
        
        getCp = [select id,email,sourcesystem__c,mailingstreet,mailingcountry,mailingstate,mailingcity,Mailing_Address_Line2__c,
                 Mailing_Address_Line3__c,Mailing_Address_Line4__c,Mailing_Street__c, mailingpostalcode from contact where id = :sourceContact.id limit 1];
        
        //Retreive Loyalty Account
        loyaltyAccounts = [select id,Activation_Date_Time__c,Customer_Loyalty_Account_Card_ID__c,Scheme_Joined_Date_Time__c,Membership_Number__c,Deactivation_Date_Time__c,Name,IsActive__c,channel_id__c , voucher_preference__c,
                           (select id, Date_Created__c, Pack_Type__c, Card_Number__c  from MyJL_Cards__r where Card_Type__c =: MyJL_Const.MY_PARTNERSHIP_CARD_TYPE AND Disabled__c = FALSE)
                           from Loyalty_Account__c where id =:ApexPages.currentPage().getParameters().get(LOYALTY_ACC_ID) limit 1];
        
        //Get record type id for myl request
        id myjlrecordtypeid = [select id from recordtype where developername ='myJL_Request' and SobjectType ='Case' limit 1].id;                                   
        
        
        isPartnershipAccount = (loyaltyAccounts[0].MyJL_Cards__r.size() > 0)? true : false;
        
        if(isPartnershipAccount){
            
            Loyalty_Card__c card = loyaltyAccounts[0].MyJL_Cards__r[0];
            myPartnershipActiveTime = card.Date_Created__c.format('dd MMMM, yyyy HH:mm');
            partnershipCardPack = card.Pack_Type__c;
        }
        
        deactivtime = (loyaltyAccounts[0].Deactivation_Date_Time__c != null) ? loyaltyAccounts[0].Deactivation_Date_Time__c.format('dd MMMM, yyyy HH:mm') : '';         
        
        
        if(loyaltyAccounts[0].Channel_ID__c == 'EPOS'){
            myJLActiveTime  = (loyaltyAccounts[0].Scheme_Joined_Date_Time__c!=null) ? loyaltyAccounts[0].Scheme_Joined_Date_Time__c.format('dd MMMM, yyyy HH:mm') : '';           
        }
        else{
            myJLActiveTime  = (loyaltyAccounts[0].Activation_Date_Time__c != null) ? loyaltyAccounts[0].Activation_Date_Time__c.format('dd MMMM, yyyy HH:mm') : '';         
        }
        
        
        memNumber = loyaltyAccounts[0].name;
        
        if(getcp[0].email != null){ 
            sourceconemail = getcp[0].email;     
        }
        
        if(loyaltyAccounts[0].voucher_preference__c != null){
            voucherPreference = loyaltyAccounts[0].voucher_preference__c;
        }
        else{
            voucherPreference = UPDATE_MY_VOUCHER_PREFERENCE;
        }
        
        if(loyaltyAccounts[0].IsActive__c==true){
            activemem = 'Yes';
        }
        else{
            activemem = 'No';
        }
        
        
        cardNumber =loyaltyAccounts[0].Customer_Loyalty_Account_Card_ID__c;
        
        
        //Retreive the Case Details
        caseDetails = [select id,CaseNumber,request_type__c,fast_track__c,myJL_Membership_Number__c,Integration_Ref__c ,Despatch_Status__c,Tracking_Number__c,Despatch_Date__c,status,createdbyid,createddate, Case_Request_Cancelled__c from case 
                       where recordtypeid =:myjlrecordtypeid and myJL_Membership_Number__c=:memNumber order by casenumber desc];                            
        
        getMailingData();               
        
    }
    
    /**
* Method to retrieve and display mailing data.  Date range set using select list.
* @params: String selectedMailDateRange        This will be set in the VF page and passed in.
*/
    public void getMailingData() {
        if (String.isBlank(selectedMailDateRange)) {
            selectedMailDateRange = DEFAULT_MAIL_RANGE;
        }
        Date toDate;
        Date fromDate;
        if (selectedMailDateRange == DEFAULT_MAIL_RANGE) {
            fromDate = system.today().addMonths(-8).toStartOfMonth();
            toDate = system.today();
        } else {
            Integer year = Integer.valueOf(selectedMailDateRange);
            fromDate = Date.newInstance(year, 1, 1);
            toDate = Date.newInstance(year, 12, 31);
        }
        directMailList = [SELECT Id, MYJL_Membership_Number__c, Despatched__c, Mailing_Type__c,Mailing_Type_F__c,
                          (SELECT Id, Reward_Type__c, Valid_From__c, Valid_To__c, Required_Spend__c, Value__c FROM Rewards__r order by Valid_From__c) 
                          FROM Direct_Mail_Update__c
                          WHERE MYJL_Membership_Number__c = :memNumber
                          AND ((Despatched__c >= :fromDate
                                AND Despatched__c <= :toDate) OR (despatched__c=null and Mailing_Type__c='New Welcome Pack'))
                          ORDER BY Despatched__c DESC];
    }
    
    public pagereference goback(){
        pagereference pagecancel = new pagereference('/apex/myJL_MembershipDetails');
        pagecancel.getParameters().put('contactId',sourcecontact.Id);
        pagecancel.setredirect(true);
        return pagecancel;
    }
    
    public pagereference myjlnewrequest(){
        id myjlrecordtypeid = [select id from recordtype where developername ='myJL_Request' and SobjectType ='Case' limit 1].id;
        
        string caseurl = '/apex/myJLRequestCreateCase?retURL=%2F/apex/myJL_Information?id='+sourceContact.id+'&scontrolCaching=1';
        
        //Pass the parameters to page reference to create case
        pagereference pr = new PageReference(caseurl);
        pr.getParameters().put('jlMemNumber',memNumber ); 
        pr.getParameters().put('jlCarNumber',cardNumber );
        pr.getParameters().put(ADDRESS_LINE_1,getcp[0].Mailing_Street__c); 
        pr.getParameters().put(ADDRESS_LINE_2,getcp[0].Mailing_Address_Line2__c  ); 
        pr.getParameters().put(ADDRESS_LINE_3,getcp[0].Mailing_Address_Line3__c  ); 
        pr.getParameters().put(ADDRESS_LINE_4,getcp[0].Mailing_Address_Line4__c  ); 
        pr.getParameters().put('postcode',getcp[0].mailingpostalcode  ); 
        pr.getParameters().put('cname',getcp[0].mailingcity  ); 
        pr.getParameters().put('ccountry',getcp[0].mailingcountry  ); 
        pr.getParameters().put('ccounty',getcp[0].mailingstate  ); 
        pr.getParameters().put('conId',contactid  ); 
        pr.getParameters().put('laid',loyactid ); 
        pr.getParameters().put('sourcecontactid',sourceContact.id );
        pr.getParameters().put('sourcecontactname',sourceContact.name );  
        pr.getParameters().put('sourcecontactemail',sourceconemail );  
        return pr;
    }
    
    private ApexPages.StandardController addFieldsToController (ApexPages.StandardController con) {
        
        List<String> fieldNames = new List<String>{
            'Id', 
                'Name','email'
                };
                    
                    
                    return con;
    }
    
    /**
* Method to update staging request records when a request has been cancelled
* @params: Id requestCaseId            This will be set in the VF page and passed in.
*/
    public void cancelRequest() {
        Id requestCaseId = ApexPages.currentPage().getParameters().get('requestCaseId');
        Case requestCase = new Case(Id = requestCaseId, Case_Request_Cancelled__c = true, Despatch_Status__c = 'Not Required',status = 'Closed - Resolved',MyJL_Request_Record_Locked__c =true);
        try {
            system.debug('beforecaseupdate'+requestcase);
            update requestCase;
            system.debug('aftercaseupdate'+requestcase);
            processData();
        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error cancelling request: ' + ex.getMessage()));
        }
    }
}