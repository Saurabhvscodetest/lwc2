public with sharing class myJL_Marshall {
    public static String prepare(SFDCMyJLCustomerTypes.CustomerRequestType request) {
        try {
            return JSON.serialize(request);
        } catch (Exception e) {
            System.debug('Error in SObject serialization');
        }
        return null;    	
    }
    
    public static SFDCMyJLCustomerTypes.CustomerRequestType unprepareCustomerRequestType(String jsonString) {
        try {
    		return (SFDCMyJLCustomerTypes.CustomerRequestType) JSON.deserialize(jsonString, SFDCMyJLCustomerTypes.CustomerRequestType.class);    	
        } catch (Exception e) {
            System.debug('Error in SObject serialization');
        }
        return null;    
    }
}