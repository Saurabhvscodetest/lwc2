/*
** TestPostChatController
**
** Test class for LiveChatTranscript trigger
**
**  Edit    Date        Author          Comment
**  001     10/09/14    NTJ             Original
**  002     11/09/14    NTJ             Reinstate use of LiveTranscript chatKey
*/
/**
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 *
 *  001     21/08/14    NTJ     Added test of the 4 out of 10 code
 */
@isTest
private class TestLiveChatTranscriptTrigger {

    private static LiveChatVisitor createVisitor() { 
        LiveChatVisitor lcv = new LiveChatVisitor();
        
        insert lcv;
        return lcv;
    }

    private static LiveChatTranscript createTranscript(String chatKey) {
        LiveChatTranscript lct = new LiveChatTranscript();
        if (chatKey != null) {
            lct.chatKey = chatKey;
            //lct.Client_GUID__c = chatKey;
        }
        LiveChatVisitor lcv = createVisitor();
        lct.LiveChatVisitorId = lcv.Id; 
        
        return lct;
    }
    
    private static Survey__c createSurvey(String chatKey, String q1, String a1, Decimal r1) {
        Survey__c s = new Survey__c();
        s.chat_question_1__c = q1;
        s.Chat_Answer_1__c = a1;
        s.chat_rating_1__c = r1;
        s.chat_Key__c = chatKey;
        //s.Chat_GUID__c = chatKey;
        s.type__c = 'Live Agent';
        s.Revision__c = TestChatCreateCustomSettings.REVISION;
        insert s;
        system.debug('s: '+s);
        
        return s;
    }

    private static testmethod void test() {
        TestChatCreateCustomSettings.createCustomSettings(true);
    
        // create two transcripts
        // one has a survey and one does not
        LiveChatTranscript lct1 = createTranscript(null);
        insert lct1;
        
        String chatKey = 'TESTXX-CHAT_KEY';
        String q1 = 'favourite colour?';
        String a1 = 'red';
        Decimal r1 = 1;
        
        LiveChatTranscript lct2 = createTranscript(chatKey);

        Survey__c s = createSurvey(chatKey, q1, a1, r1);
        
        system.debug('before insert: lct2: '+lct2);
        insert lct2;
        
        // requery
        lct1 = [SELECT rating_1__c, answer_1__c, survey_revision__c FROM LiveChatTranscript WHERE Id=:lct1.Id];
        system.assertNotEquals(null, lct1.Id);
        system.assertEquals(null, lct1.answer_1__c);
        system.assertEquals(null, lct1.rating_1__c);
        system.assertEquals(null, lct1.survey_revision__c);
        
        lct2 = [SELECT rating_1__c, answer_1__c, survey_revision__c FROM LiveChatTranscript WHERE Id=:lct2.Id];
        system.debug('lct2: '+lct2);
        system.assertNotEquals(null, lct2.Id);
        system.assertEquals(a1, lct2.answer_1__c);
        system.assertEquals(r1, lct2.rating_1__c);
        system.assertNotEquals(null, lct2.survey_revision__c);
    }
    
    static testMethod void testFetchCustomerRecordMethod() {
        TestChatCreateCustomSettings.createCustomSettings(false);
    
        LiveChatVisitor lcv = new LiveChatVisitor();    
        insert lcv;
        
        LiveChatTranscript lct = new LiveChatTranscript();
        system.assertNotEquals(null, lct);
        
        lct.Body = 'hello mr customer';
        lct.LiveChatVisitorId = lcv.Id;
        
        lct.FirstName__c = 'Customer';
        lct.LastName__c = 'Test';
        lct.Email__c = 'customertest@test.com';
        
        Contact contactRecord = new Contact ();
        contactRecord.LastName = 'Test';
        contactRecord.FirstName = 'Customer';
        contactRecord.Email = 'customertest@test.com';
        contactRecord.recordTypeId = '012b0000000M9U4';
        insert contactRecord;
                
        insert lct;
        system.assertNotEquals(null, lct.Id);
    
        LiveChatTranscriptEvent lcte = new LiveChatTranscriptEvent();
        lcte.LiveChatTranscriptId = lct.Id;
        lcte.type = 'Other';
        lcte.Time = system.now();
        
        insert lcte;
        
        try {
          delete lcte;
          system.assertEquals('Error as cannot delete transcript event', 'deleted transcript event');    
        } catch (Exception e) {
          system.assertEquals(true, true);
        }
        
        Contact contactRecord1 = new Contact ();
        contactRecord1.LastName = 'TestCus1';
        contactRecord1.FirstName = 'CustomerTest1';
        contactRecord1.Email = 'customertestuser1@test.com';
        contactRecord1.recordTypeId = '012b0000000kKhm';
        insert contactRecord1;
            
        
        LiveChatTranscript lct1 = new LiveChatTranscript();
        
        lct1.Body = 'hello mr customer';
        lct1.LiveChatVisitorId = lcv.Id;
        
        lct1.FirstName__c = 'Customer22';
        lct1.LastName__c = 'Test';
        lct1.Email__c = 'customertest@test.com';
        
        insert lct1;
            
        LiveChatTranscript lct2 = new LiveChatTranscript();
        
        lct2.Body = 'hello mr customer';
        lct2.LiveChatVisitorId = lcv.Id;
        
        lct2.FirstName__c = 'Customer22';
        lct2.LastName__c = 'Test11';
        lct2.Email__c = 'customertest@test.com';
        
        insert lct2;
            
        LiveChatTranscript lct3 = new LiveChatTranscript();
        
        lct3.Body = 'hello mr customer';
        lct3.LiveChatVisitorId = lcv.Id;
        
        lct3.FirstName__c = 'CustomerTest1';
        lct3.LastName__c = 'Test111';
        lct3.Email__c = 'customertest@test111.com';
        
        insert lct3;
        
        LiveChatTranscript lct4 = new LiveChatTranscript();
        
        lct4.Body = 'hello mr customer';
        lct4.LiveChatVisitorId = lcv.Id;
        
        lct4.FirstName__c = 'CustomerTest1';
        lct4.LastName__c = 'Test111';
        lct4.Email__c =  'customertestuser1@test.com';
        insert lct4;    
            
        
        LiveChatTranscript lct5 = new LiveChatTranscript();
        
        lct5.Body = 'hello mr customer';
        lct5.LiveChatVisitorId = lcv.Id;
        
        lct5.FirstName__c = 'CustomerTest1';
        lct5.LastName__c = 'TestCus1';
        lct5.Email__c =  'customertestuser1@test.com';
        insert lct5;
            
        LiveChatTranscript lct6 = new LiveChatTranscript();
        
        lct6.Body = 'hello mr customer';
        lct6.LiveChatVisitorId = lcv.Id;
        
        lct6.FirstName__c = 'CustomerTest2';
        lct6.LastName__c = 'TestCus1';
        lct6.Email__c =  'customertestuser1@test.com';
        insert lct6;
        
    }
}