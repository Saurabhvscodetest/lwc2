global class ScheduleBatchDeleteCustomerSurveys implements Schedulable{

     global void execute(SchedulableContext sc){
        GDPR_BatchDeleteCustomerSurveys obj = new GDPR_BatchDeleteCustomerSurveys();
        Database.executebatch(obj);
    }
    
}