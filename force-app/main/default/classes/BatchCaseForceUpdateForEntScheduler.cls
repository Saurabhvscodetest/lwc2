global class BatchCaseForceUpdateForEntScheduler implements Schedulable {

	global void execute(SchedulableContext sc) {

		BatchCaseForceUpdateForEntitlements batchRun = new BatchCaseForceUpdateForEntitlements ();
	
		// Set scope size to 50;
		database.executebatch(batchRun, 50);
	
	}

}