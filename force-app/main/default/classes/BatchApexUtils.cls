/**
 *	@author: Andrey Gavrikov (westbrook)
 *	@date: 2014-12-02 12:36:36 
 *	@description:
 *	    utility methods for batch apex classes
 *	
 *	Version History :   
 *	2014-12-02 - AG
 *	initial version
 *		
 */
public with sharing class BatchApexUtils {
	private final static Set<String> IN_PROGRESS_JOB_STATUSES = new Set<String>{
		'Processing'
		,'Preparing'
		,'Queued'
		,'Holding'
	};
	private static Integer DEFAULT_STALE_JOB_TIMEOUT_MIN = 30;//consider job to be stuck if its start date is this many minutes or more in the past

	public static Boolean isJobAlreadyRunning(final String batchClassName, final Integer staleJobTimeoutMin) {
		final Integer jobStaleMin = null != staleJobTimeoutMin? staleJobTimeoutMin : DEFAULT_STALE_JOB_TIMEOUT_MIN;
		final List<ApexClass> recs = [select Id, Name from ApexClass where Name = :batchClassName limit 1];
		if (!recs.isEmpty()) {
			final Id apexClassId =  recs[0].Id;
			final List<AsyncApexJob> jobs = [select Id, Status, CreatedDate, CompletedDate from AsyncApexJob 
											where ApexClassId = :apexClassId and Status in :IN_PROGRESS_JOB_STATUSES 
												and JobType = 'BatchApex'
											order by CreatedDate desc
											limit 1];
			//check if this is a stale/stuck job
			if (!jobs.isEmpty() && null == jobs[0].CompletedDate) {
				final Datetime cutOffDatetime = System.now().addMinutes(-1 * jobStaleMin);
				if (jobs[0].CreatedDate < cutOffDatetime) {
					return true;//job is still running
				}
			}
		}
		return false;
	}
}