/* Description  : Delete case Records As per the cirteria given in COPT-4293
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   20/02/2019        Vignesh Kotha                   Created               COPT-4293
*
*/
global class BAT_DeleteCaseRecords implements Database.Batchable<sObject>,Database.Stateful{
    global Map<Id,String> ErrorMap = new Map<Id,String>(); 
    global integer result=0;
    global integer TaskListsize=0;
    global integer EmailMessagelistsize=0;
    global integer caseActivityListsize=0;
    global integer Interactionlistsize=0;
    private static final DateTime Prior_Date_Months = System.now().addMonths(-12);
    private static final DateTime Prior_Date_Years = System.now().addyears(-6);
    private static final DateTime Prior_Date_Years_10 = System.now().addyears(-10);
    global Database.QueryLocator start(Database.BatchableContext BC){
        GDPR_Record_Limit__c gDPRLimit = GDPR_Record_Limit__c.getValues('BAT_DeleteCaseRecords');
        String recordLimit = gDPRLimit.Record_Limit__c;
        Integer qLimit = Integer.valueOf(recordLimit);
        
        return Database.getQueryLocator([select id,recordtypeid,Number_of_Email_Contacts__c,ClosedDate,CreatedDate from case where
                                         (recordtype.name ='myJL Request' and ClosedDate < : Prior_Date_Months) OR 
                                         (recordtype.name ='Product Stock Enquiry' AND ClosedDate < : Prior_Date_Months AND Number_of_Email_Contacts__c = 0) OR
                                         (recordtype.name ='Product Stock Enquiry' AND ClosedDate < : Prior_Date_Years AND Number_of_Email_Contacts__c >= 1) OR
                                         (recordtype.name ='Order Exception' AND ClosedDate < : Prior_Date_Months AND Number_of_Email_Contacts__c = 0) OR
                                         (recordtype.name ='Order Exception' AND ClosedDate < : Prior_Date_Years AND Number_of_Email_Contacts__c >= 1) OR
                                         /* COPT-5272 
                                         (recordtype.name ='PSE Sub-case' AND ClosedDate < : Prior_Date_Months AND Number_of_Email_Contacts__c = 0) OR
                                         (recordtype.name ='PSE Sub-case' AND ClosedDate < : Prior_Date_Years AND Number_of_Email_Contacts__c >= 1) OR
                                         (recordtype.name ='Email Triage' and ClosedDate < : Prior_Date_Years) OR
                                         COPT-5272 */
										 (recordtype.name ='New Case' and ClosedDate < : Prior_Date_Years) OR
                                         (recordtype.name ='NKU' and ClosedDate < : Prior_Date_Years) OR 
                                         (recordtype.name ='Query' and ClosedDate < : Prior_Date_Years) OR
                                         /* COPT-5272
                                         (recordtype.name ='Web Triage' and ClosedDate < : Prior_Date_Years) OR
                                         COPT-5272 */
										 (recordtype.name ='Complaint' and ClosedDate < : Prior_Date_Years_10) LIMIT : qLimit
                                        ]);
    }
    global void execute(Database.BatchableContext BC, list<Case> scope){
        List<id> recordstoDelete = new List<id>();
        if(scope.size()>0){
            try{
                for(Case recordScope : scope){
                    recordstoDelete.add(recordScope.id);
                }
                TaskListsize = [select id,whatid from task where whatid IN : recordstoDelete].size();
                EmailMessagelistsize = [select id,parentid from Emailmessage where parentid IN : recordstoDelete].size();
                Interactionlistsize = [select id,case__c from Interaction__c where case__c IN : recordstoDelete].size();
                caseActivityListsize = [select id,case__c from Case_Activity__c where case__c IN : recordstoDelete].size();
                list<Database.deleteResult> srList = Database.delete(recordstoDelete, false);
                for(Integer counter = 0; counter < srList.size(); counter++){
                    if (srList[counter].isSuccess()){
                        result++;
                    }else{
                        for(Database.Error err : srList[counter].getErrors()){
                            ErrorMap.put(srList.get(counter).id,err.getMessage());
                        }
                    }
                }
                /* Delete records from Recyclebin */
                Database.emptyRecycleBin(scope);
            }Catch(exception e){
                EmailNotifier.sendNotificationEmail('Exception from BAT_DeleteCaseRecords ', e.getMessage());
            }
        }
    }
    global void finish(Database.BatchableContext BC){
        String textBody ='';
        set<Id> allIds = new set<Id>();
        textBody+= result +' records deleted in object Case.'+'\n';
        textBody+= TaskListsize +' records deleted in object Task.'+'\n';
        textBody+= EmailMessagelistsize +' records deleted in object Email message.'+'\n';
        textBody+= caseActivityListsize +' records deleted in object Case Activity.'+'\n';
        textBody+= Interactionlistsize +' records deleted in object Interaction.'+'\n';
        if(!ErrorMap.isEmpty()){
            for (Id recordids : ErrorMap.KeySet()){
                allIds.add(recordids);
            } 
            textBody+= 'Error log: '+allIds+'\n';
        }
        EmailNotifier.sendNotificationEmail('GDPR Connex Deletion Log', textBody);
        BAT_DeleteCaseFeedback casefeedbatch = new BAT_DeleteCaseFeedback();
        Database.executeBatch(casefeedbatch);
    }    
}