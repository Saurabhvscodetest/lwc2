/******************************************************************************
* @author       Ranjan Samal             
* @date         21/09/2017
* @description  VF Controller for Team Manage page 

EDIT RECORD

LOG     DATE        Author  COMMENT      
001     04/05/2015  ES      V.1.0 working version ready for Demo
002     06/05/2015  ES      Feedback FROM KW - V.1.1 Team members moved to another team will retain their original team manager
003     16/05/2015  ES      CMP-405. Allow In-house CC team managers to move agents between in-house CC teams
004     21/09/2017  RS      Entire functionality was changed.
                            Removal of Team Managers
                            Addition of Generic Search across 3 fields of user object:team__c,Net_Dom_ID__c and name.
                            Ability to multi select users and assign to New Team and Public Groups
******************************************************************************/
public without sharing class JLManageTeamMembershipController {
    
    public  String              teamName                              {get;set;}
    public  String              newTeamName                           {get;set;}
    public  String              teamID                                {get;set;}
    public  String              newPublicGrpID                        {get;set;}
    public  String              newTeamID                             {get;set;}
    public  String              chosenUserIDs                         {get;set;}
    public  boolean             showTeamMembers                       {get;private set;}
    public  boolean             showNewTeams                          {get;private set;}
    public  boolean             showNewGroups                         {get;private set;}
    public  boolean             showMoveButton                        {get;private set;}
    public  boolean             hasCapitaDutyManagerPermissionSet     {get;set;}
    public  boolean             hasInHouseTeamManagerPermissionSet    {get;set;}
    public  boolean             showMessage                           {get;private set;}
    public  List<User>          users                                 {get;set;}
    public  List<SelectOption>  teamMembers                           {get; private set;}//Wrapper class which stores User data and a boolean flag.
    public  List<JLUserWrapper> JLUserWrappers                        {get; set;}        
    public  List<JLUserWrapper> lstSetController                      {get;set;}
    private String soql                                               {get;set;} 
    
    private static final string CAPITA_DUTY_MANAGER_PERMSETNAME       = 'Duty_Manager';
    private static final string IN_HOUSE_CC_TEAM_MANAGER_PERMSETNAME  = 'In_House_CC_Team_Manager';
    private static final string SOQL_BASE_QUERY                       = 'SELECT Id,Name,team__c,Temporary_Public_Group__c,Net_Dom_ID__c FROM User WHERE IsActive = True ';
    private static final string USER                                  = 'usr';
    private static final string NONE                                  = 'NONE';
    private static final string ERROR_MESSAGE                         = 'An Error Occured!';
    private static final string SELECT_A_TEAM                         = 'Please select a Team';
    private static final string SELECT_A_PUBLIC_GROUP                 = 'Please select a Public Group';
    private static final string SUCCESS_MESSAGE                       = 'Selected Users added successfully to Public Group: ';
    private static final string ERROR_UPDATING                        = '--Error Updating--';
    private SET<Id>  ids;
    public  List<User> selectedusers = new List<User>();                                       
    private integer LimitSize= 2000;
   
    //Instantiating the JLCustomIterable (Custom Iterable) class - Used for Pagination
    JLCustomIterable obj;
    
    /**
    * @description  Method for returning the contact search results to the UI
    * @author       @rsamal 
    * @return       List of Wrapper JLUserWrapper
    */
    public List<JLUserWrapper> getusers(){   
        return JLUserWrappers;  
    }
        
    /**
    * @description  Constructor
    * @author       @rsamal 
    */
    public JLManageTeamMembershipController () {
        
       showTeamMembers                    = false;
       showNewTeams                       = false;
       showNewGroups                      = false;
       showMoveButton                     = false;
       hasCapitaDutyManagerPermissionSet  = false;
       hasInHouseTeamManagerPermissionSet = false;

       Map <Id, permissionset> manageTeamPermsetMap = new Map <Id, permissionset> ([select Id, 
                                                                                           Name 
                                                                                      FROM permissionset 
                                                                                     WHERE name= :CAPITA_DUTY_MANAGER_PERMSETNAME 
                                                                                        OR name = :IN_HOUSE_CC_TEAM_MANAGER_PERMSETNAME]);
       
        if (manageTeamPermsetMap!=null && manageTeamPermsetMap.size() > 0) {
            
            for (PermissionsetAssignment pa : [select Id, 
                                                      AssigneeId, 
                                                      PermissionSetId 
                                                 FROM PermissionSetAssignment 
                                                WHERE PermissionSetId = :manageTeamPermsetMap.keySet() 
                                            AND AssigneeId = :UserInfo.getUserId()]) {
                                                
                PermissionSet p = manageTeamPermsetMap.get(pa.PermissionSetId);
                if (p!=null && p.name==CAPITA_DUTY_MANAGER_PERMSETNAME) {
                    hasCapitaDutyManagerPermissionSet = true;
                    break;
                }
                if (p!=null && p.name==IN_HOUSE_CC_TEAM_MANAGER_PERMSETNAME) {
                    hasInHouseTeamManagerPermissionSet = true;
                    break;
                }
            }
        } 
    }
    
    /**
    * @description  Pulling the teamID and querying to fetch the Team Name selected
    * @author       @rsamal     
    */
    public PageReference chooseNewTeam() {
        
        if (!String.IsBlank(newTeamId) && newTeamId.toUpperCase()!='NONE') {
            newTeamName = [select Id,
                                  Name 
                             FROM Team__c 
                            WHERE Id = :newTeamId limit 1].name;
        }         
        return null;
    }

    /**
    * @description  Fetch All teams based on the permission set assigned to the user
    * @author       @rsamal
    */
    public List<SelectOption> getAllTeams () {
        
        List<SelectOption> options = new List<SelectOption>();
        List<Team__c> allTeamsCS   = new List<Team__c> ();

        if (hasCapitaDutyManagerPermissionSet) 
            allTeamsCS = [SELECT Id, 
                                 Name 
                            FROM Team__c 
                           WHERE Available_on_Team_Management_Page__c = true 
                        ORDER BY Name asc];
        
        if (hasInHouseTeamManagerPermissionSet) 
            allTeamsCS = [SELECT Id, 
                                 Name 
                            FROM Team__c 
                           WHERE Available_In_House_Team_Management_Page__c = true 
                        ORDER BY Name asc];
            
        options.add(new SelectOption(NONE,SELECT_A_TEAM,true));
        for (Team__c t: allTeamsCS) {
            
            options.add(new SelectOption(t.id,t.name));
        }

        return options;
    }
    
    /**
    * @description  Runs the actual dynamic query to fetch the user list searched
    * @author       @rsamal
    */
    public void runQuery() {

        try {
            users = Database.query(soql + ' limit ' + LimitSize);
            system.debug('users ---->' + users);
            JLUserWrappers = new List<JLUserWrapper>();
            lstSetController = new List<JLUserWrapper>();
            for(User u: users){
                JLUserWrappers.add(new JLUserWrapper(u,false));
            }
        } catch (Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ERROR_MESSAGE));
        }
        obj = new JLCustomIterable (JLUserWrappers); 
        obj.setPageSize = 10;
        next(); 
        /**Setting the Booleans to make the components visible */
        showTeamMembers = true;
        showMoveButton  = true;
        showNewTeams    = true;
        showNewGroups   = true;
    }
    
    /**
    * @description  runs the search with parameters passed via Javascript
                    Does a dynamic search (Contains) across 3 fields of user object:
                    team__c,Net_Dom_ID__c and name.
    * @author       @rsamal
    */
    public PageReference runSearch() {
        
        String userQueryString = Apexpages.currentPage().getParameters().get(USER);
        
        //CHECK WHICH PROFILES HAVE BEEN ENABLED FOR AUTO RETURN TO PERMANENT TEAM
        Set <String> profileNames = new Set<String> ();
        List<Config_Settings__c> allSettings = Config_Settings__c.getall().values();
        for (Config_Settings__c s : allSettings) {
            if (s.name!=null && s.name.contains('AUTO_RETURN_TO_TEAM_PROFILE') && !String.IsBlank(s.Text_Value__c)) {
                profileNames.add(s.Text_Value__c);
            }
        }

        Map<Id,Profile> profilesMap = new Map<Id,Profile> ([SELECT ID 
                                                          FROM Profile 
                                                         WHERE Name IN :profileNames]);
        ids = profilesMap.keyset();
        
        soql = SOQL_BASE_QUERY;
        
        if (!userQueryString.equals(''))
            soql += ' AND profileid IN :ids'
                  + ' AND (team__c LIKE \''+'%'+String.escapeSingleQuotes(userQueryString)+'%\'' 
                  + ' or Net_Dom_ID__c LIKE \''+'%'+String.escapeSingleQuotes(userQueryString)+'%\''
                  + ' or name LIKE \''+'%'+String.escapeSingleQuotes(userQueryString)+'%\')';
        system.debug('SOQL_BASE_QUERY ---->' + soql);
        // run the query again
        runQuery();

        return null;
    }
      
    /**
    * @description  Method to fetch the selected records 
                    and assign the users to the selected Team or Public Group
    * @author       @rsamal
    */
    public pageReference processSelected() {
        
        selectedUsers.clear();
        ApexPages.getMessages().clear();  
        if (JLUserWrappers!= null) {
            
            for(JLUserWrapper cUsr : getusers()) {
                
                if(cUsr.selected == true){                  
                    selectedUsers.add(cUsr.usr);
                }
            }
        }
        List<GroupMember> groupMembers = new List<GroupMember>();
        
        if ((selectedUsers!=null) && (selectedUsers.size() > 0) 
                                  && (!String.IsBlank(newTeamName)) 
                                  && (newTeamName.toUpperCase()!= NONE)) {
            for (User u : selectedUsers) {          
                u.Team__c = newTeamName;              
            }
            try{        
                update selectedUsers;
                }
            catch (Exception e) {
                
                //during exception displaying the error message against the records by assigning the wrapper
                for (User u : selectedUsers) {          
                    u.Team__c = ERROR_UPDATING;            
                } 
                ApexPages.addMessages(e);
            }
        }       
        if((!String.IsBlank(newPublicGrpID)) && (newPublicGrpID.toUpperCase()!= NONE)) {
            for (User u : selectedUsers) {  
                      
                GroupMember GM = new GroupMember();
                GM.GroupId     = newPublicGrpID;
                if(u.Temporary_Public_Group__c != null) {
                    u.Temporary_Public_Group__c = u.Temporary_Public_Group__c + ',' + newPublicGrpID;
                } else{
                    u.Temporary_Public_Group__c = newPublicGrpID;
                }
                GM.UserOrGroupId = U.Id;
                groupMembers.add(GM);            
            }
        
            try{
                insert groupMembers;
                update selectedUsers;
                String groupName = [SELECT Id,
                                           Name 
                                      FROM Group 
                                     WHERE ID =: newPublicGrpID LIMIT 1].name;
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, SUCCESS_MESSAGE + groupName));
            }catch (Exception e) {          
                //ApexPages.addMessages(e);
            }
        }
        //Resetting the Selected Team and User Group variables
        newTeamName = '';
        newPublicGrpID = '';
        
        return null;
    }
    
     /**
    * @description  Fetch all the public Groups
    * @author       @rsamal
    */
    public List<SelectOption> getPublicGroups () {
        
        List<SelectOption> options = new List<SelectOption>();
        options.add (new SelectOption('NONE',SELECT_A_PUBLIC_GROUP));
        for (Group g : [SELECT Id,
                               DeveloperName,
                               Name,
                               OwnerId 
                          FROM Group 
                         WHERE Name LIKE 'CPM%']) {
            options.add(new SelectOption(g.id,g.Name));
        }
        return options;
    }
    
    /**
    * @description  Set the selected group ID.
    * @author       @rsamal
    */
    public PageReference chooseNewPublicGrp() {
        return null;
    }
    
    public Boolean hasNext {
        get 
        {
            if(obj != null)
            return obj.hasNext();
            else
            return null;
            
        }
        set;
    }
        
    public Boolean hasPrevious {
        get 
        {
            if(obj != null)
            return obj.hasPrevious();
            else
            return null;
        }
        set;
    }
        
    public void next() 
    {
        lstSetController = obj.next();
    }
    
    public void previous() 
    {
        lstSetController = obj.previous();
    }
}