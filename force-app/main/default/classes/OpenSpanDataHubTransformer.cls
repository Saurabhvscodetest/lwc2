public class OpenSpanDataHubTransformer {
    
    //static final String OPENSPAN_ACTIVE_CASE = 'Active';
    public static final String OPENSPAN_FAIL_CASE = 'Fail';
    public static final String OPENSPAN_REJECTED_CASE = 'Rejected';
    public static final String OPENSPAN_SUCCESS_CASE = 'Success';
    public static final String DUPLICATE_KEY_ERROR_MSG = 'more than one record found for external id field';
    
    List<Data_Hub__c> OpenSpanCases;
    Map<Id, Data_Hub__c> OpenSpanCasesMap;
    Map<Id, Data_Hub__c> OpenSpanCasesNewMap;
    Map<Id, Case> contactProfileCases = new Map<Id, Case>();
    
    public OpenSpanDataHubTransformer (List<Data_Hub__c> scope, Map<Id, Data_Hub__c> newMap){
        
        OpenSpanCases = scope;
        OpenSpanCasesMap = new Map<Id, Data_Hub__c>(scope);
        OpenSpanCasesNewMap = newMap;
    }
         
    public void transformToSFDC(){
        
        //The agreed on matching logic for OpenSpan Cases is the following
        //Match by (FirstName+LastName+Email)
        //if no match try (FirstName+LastName+FirstLineOfAddress+PostalCode)
        //if no match and case is Open, create with no contact
        //if no match and case is closed, reject with error message 
        
        List<Data_Hub__c> rejectedOpenSpanCases = new List<Data_Hub__c>();
        
        List<Data_Hub__c> acceptedOpenSpanCasesKey1 = new List<Data_Hub__c>();
        List<Data_Hub__c> acceptedOpenSpanCasesKey2 = new List<Data_Hub__c>();
        List<Data_Hub__c> acceptedOpenSpanCasesKey3 = new List<Data_Hub__c>();
        List<Data_Hub__c> acceptedOpenSpanCasesKey4 = new List<Data_Hub__c>();
                
        List<Data_Hub__c> acceptedOpenSpanOrphanCases = new List<Data_Hub__c>();
        
        Map<Id, Data_Hub__c> dataHubRecordsToUpdate = new Map<Id, Data_Hub__c>();
        List<CaseComment> caseCommentsToBeInserted = new List<CaseComment>(); 
        
        for(Data_Hub__c OpenSpanCase : OpenSpanCases){
            
            OpenSpanCase = OpenSpanUtilities.validateCase(OpenSpanCase);
        
            if(OpenSpanCase.Status__c == OPENSPAN_REJECTED_CASE){
                rejectedOpenSpanCases.add(new Data_Hub__c(Id = OpenSpanCase.Id, status__c = OpenSpanCase.Status__c, Error_Messages__c = OpenSpanCase.Error_Messages__c));
            } else {
                
                // if there are some fields missing that make the matching keys, skip the matching process and try to create orphan cases
                
                //MJS: 03Oct2014 - changed to test each key individually, and to use the central ContactUtils functions
                //so that Openspan uses the same code to validate the keys as the rest of the implementations
                
                if(ContactUtils.validateCaseMatchKey1(OpenSpanCase.First_Name__c, OpenSpanCase.Surname__c, OpenSpanCase.Email__c)){
                    acceptedOpenSpanCasesKey1.add(OpenSpanCase);
                } else if(ContactUtils.validateCaseMatchKey2(OpenSpanCase.First_Name__c, OpenSpanCase.Surname__c, OpenSpanCase.Address1__c, OpenSpanCase.Post_Code__c)){
                    acceptedOpenSpanCasesKey2.add(OpenSpanCase);
                } else {
                    // log the message in Data Hub Error message the reason of creating Orphan Case
                    Data_Hub__c runUpdate = new Data_Hub__c(Id=OpenSpanCase.Id);
                    runUpdate.Error_Messages__c = 'Incomplete matching keys\n\n';
                    dataHubRecordsToUpdate.put(runUpdate.Id, runUpdate);
                    acceptedOpenSpanOrphanCases.add(OpenSpanCase);
                    
                }
            }
            
        }
        
        //This first run will try to match failing Cases using the first Key against contact object
        if(acceptedOpenSpanCasesKey1.size() > 0){
            
            List<Case> sfdcCases = new List<Case>();
            //List<Data_Hub__c> run1UpdateList = new List<Data_Hub__c>();
            
            for(Data_Hub__c acceptedOpenSpanCase : acceptedOpenSpanCasesKey1){
                try{
                    sfdcCases.add(OpenSpanUtilities.matchCaseByKey1(acceptedOpenSpanCase));
                } catch(Exception e){
                    system.debug('===Exception===' + e.getStackTraceString());
                    rejectedOpenSpanCases.add(new Data_Hub__c(Id = acceptedOpenSpanCase.Id, status__c = OPENSPAN_FAIL_CASE, Error_Messages__c = acceptedOpenSpanCase.Error_Messages__c + '\nException: ' + e.getStackTraceString()));
                }
                
                
            }
             
            Database.SaveResult[] caseResults = Database.insert(sfdcCases, false);
            for(Integer i=0; i < CaseResults.size() ; i++){
                Data_Hub__c run1Update;
                if(dataHubRecordsToUpdate.get(acceptedOpenSpanCasesKey1[i].Id) != null){
                    run1Update = dataHubRecordsToUpdate.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c);
                }else{
                    run1Update = new Data_Hub__c(Id = acceptedOpenSpanCasesKey1[i].Id);
                    dataHubRecordsToUpdate.put(acceptedOpenSpanCasesKey1[i].Id, run1Update);
                }
                system.debug('==caseResults===' + caseResults); 
                //run1UpdateList.add(run1Update);
                
                if(caseResults[i].Success){
                    
                    run1Update.Status__c = OPENSPAN_SUCCESS_CASE;
                    run1Update.Case__c = caseResults[i].Id;
                    //run1Update.Error_Messages__c = '';
                    run1Update.Case_Number__c = sfdcCases[i].CaseNumber;
                    caseCommentsToBeInserted.add(OpenSpanUtilities.createCaseComment(sfdcCases[i].Id, OpenSpanCasesNewMap.get(run1Update.Id)));
                } else{ 
                    
                    run1Update.Status__c = OPENSPAN_FAIL_CASE;
                    //run1Update.Error_Messages__c = '';
                    
                    for(Database.Error error : caseResults[i].errors){
                        
                        if(error.message != null && error.message != '')
                            run1Update.Error_Messages__c = error.message + '\n\n';
                    }
                    
                    if((run1Update.Error_Messages__c).contains('not found for field Case_Matching_Key_1__c in entity Contact')
                            || (run1Update.Error_Messages__c).contains(DUPLICATE_KEY_ERROR_MSG)){
                        
                        //MJS: 03Oct2014 - added double check that the matching key2 is valid
                        //if it is not then added the record to acceptedOpenSpanCasesKey3 in order 
                        //to try against the contact profiles with key1, because if we got here then key1 must be valid
                        if(ContactUtils.validateCaseMatchKey2(OpenSpanCasesMap.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c).First_Name__c,
                            OpenSpanCasesMap.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c).Surname__c,
                             OpenSpanCasesMap.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c).Address1__c,
                               OpenSpanCasesMap.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c).Post_Code__c)){
                            system.debug('MJS: in run1 key2 Valid');
                            acceptedOpenSpanCasesKey2.add(OpenSpanCasesMap.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c));
                        } else {
                            system.debug('MJS: in run1 key2 NOTvalid:');
                            system.debug('fname: ' + OpenSpanCasesMap.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c).First_Name__c);
                            system.debug('lname: ' + OpenSpanCasesMap.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c).Surname__c);
                            system.debug('addl1: ' + OpenSpanCasesMap.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c).Address1__c);
                            system.debug('pcode: ' + OpenSpanCasesMap.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c).Post_Code__c);
                            
                            acceptedOpenSpanCasesKey3.add(OpenSpanCasesMap.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c));
                        }
                    }
                    
                }
                
            }
            //update run1UpdateList;
        }
        system.debug('====dataHubRecordsToUpdate===' + dataHubRecordsToUpdate);
        //This second run will try to match failing Cases using the second Key against contact object
        if(acceptedOpenSpanCasesKey2.size() > 0){
            
            List<Case> sfdcCases = new List<Case>();
            //List<Data_Hub__c> run2UpdateList = new List<Data_Hub__c>();
            
            for(Data_Hub__c acceptedOpenSpanCase : acceptedOpenSpanCasesKey2){
                        
                try{
                    sfdcCases.add(OpenSpanUtilities.matchCaseByKey2(acceptedOpenSpanCase));
                } catch (Exception e){
                    rejectedOpenSpanCases.add(new Data_Hub__c(Id = acceptedOpenSpanCase.Id, status__c = OPENSPAN_FAIL_CASE, Error_Messages__c = acceptedOpenSpanCase.Error_Messages__c + '\nException: ' + e.getStackTraceString()));
                }
                
            }
             
            Database.SaveResult[] caseResults = Database.insert(sfdcCases, false);
            for(Integer i=0; i < CaseResults.size() ; i++){
                
                Data_Hub__c run2Update;
                if(dataHubRecordsToUpdate.get(acceptedOpenSpanCasesKey2[i].Id) != null){
                    run2Update = dataHubRecordsToUpdate.get(acceptedOpenSpanCasesKey2[i].Id);
                }else{
                    run2Update = new Data_Hub__c(Id = acceptedOpenSpanCasesKey2[i].Id);
                    dataHubRecordsToUpdate.put(acceptedOpenSpanCasesKey2[i].Id, run2Update);
                }
                
                if(caseResults[i].Success){
                    
                    run2Update.Status__c = OPENSPAN_SUCCESS_CASE;
                    run2Update.Case__c = caseResults[i].Id;
                    //run2Update.Error_Messages__c = '';
                    run2Update.Case_Number__c = sfdcCases[i].CaseNumber;
                    caseCommentsToBeInserted.add(OpenSpanUtilities.createCaseComment(sfdcCases[i].Id, OpenSpanCasesNewMap.get(run2Update.Id)));
                      
                } else{
                    
                    run2Update.Status__c = OPENSPAN_FAIL_CASE;
                    //run2Update.Error_Messages__c = '';
                    
                    for(Database.Error error : caseResults[i].errors){
                        
                        if(error.message != null && error.message != '')
                            run2Update.Error_Messages__c += error.message + '\n\n';
                    }
                    
                    if((run2Update.Error_Messages__c).contains('not found for field Case_Matching_Key_2__c in entity Contact')
                            || (run2Update.Error_Messages__c).contains(DUPLICATE_KEY_ERROR_MSG)){
                        
                        //MJS: 03Oct2014 - added check to see if the matching key1 is valid
                        //if it is then add the case to acceptedOpenSpanCasesKey3 to try to match agains
                        //contact profile with key1, if not then to get here key2 must be valid so add to
                        //acceptedOpenSpanCasesKey4 to try with key2 against contactProfile
                        
                        if(ContactUtils.validateCaseMatchKey1(OpenSpanCasesMap.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c).First_Name__c,
                             OpenSpanCasesMap.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c).Surname__c,
                              OpenSpanCasesMap.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c).Email__c)){
                            acceptedOpenSpanCasesKey3.add(OpenSpanCasesMap.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c));
                        } else {
                            acceptedOpenSpanCasesKey4.add(OpenSpanCasesMap.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c));
                        }

                    }
                    
                }
                
            }
            //update run2UpdateList;
        }
        system.debug('====dataHubRecordsToUpdate2===' + dataHubRecordsToUpdate);
        //This third run will try to match failing Cases using the first Key against contact profile object
        if(acceptedOpenSpanCasesKey3.size() > 0){
            
            List<Case> sfdcCases = new List<Case>();
            //List<Data_Hub__c> run3UpdateList = new List<Data_Hub__c>();
            
            for(Data_Hub__c acceptedOpenSpanCase : acceptedOpenSpanCasesKey3){
                
                try{
                    sfdcCases.add(OpenSpanUtilities.matchCaseByKey3(acceptedOpenSpanCase));
                } catch (Exception e){
                    rejectedOpenSpanCases.add(new Data_Hub__c(Id = acceptedOpenSpanCase.Id, status__c = OPENSPAN_FAIL_CASE, Error_Messages__c = acceptedOpenSpanCase.Error_Messages__c + '\nException: ' + e.getStackTraceString()));
                }
                
                
            }
             
            Database.SaveResult[] caseResults = Database.insert(sfdcCases, false);
            for(Integer i=0; i < CaseResults.size() ; i++){
                
                Data_Hub__c run3Update;
                if(dataHubRecordsToUpdate.get(acceptedOpenSpanCasesKey3[i].Id) != null){
                    run3Update = dataHubRecordsToUpdate.get(acceptedOpenSpanCasesKey3[i].Id);
                }else{
                    run3Update = new Data_Hub__c(Id = acceptedOpenSpanCasesKey3[i].Id);
                    dataHubRecordsToUpdate.put(acceptedOpenSpanCasesKey3[i].Id, run3Update);
                }
                
                if(caseResults[i].Success){
                    
                    run3Update.Status__c = OPENSPAN_SUCCESS_CASE;
                    run3Update.Case__c = caseResults[i].Id;
                    //run3Update.Error_Messages__c = '';
                    run3Update.Case_Number__c = sfdcCases[i].CaseNumber;
                    contactProfileCases.put(caseResults[i].Id, sfdcCases[i]);
                    caseCommentsToBeInserted.add(OpenSpanUtilities.createCaseComment(sfdcCases[i].Id, OpenSpanCasesNewMap.get(run3Update.Id)));
                      
                } else{
                    
                    run3Update.Status__c = OPENSPAN_FAIL_CASE;
                    //run3Update.Error_Messages__c = '';
                    
                    for(Database.Error error : caseResults[i].errors){
                        
                        if(error.message != null && error.message != '')
                            run3Update.Error_Messages__c += error.message + '\n\n';
                    }
                    
                    if((run3Update.Error_Messages__c).contains('not found for field Case_Matching_Key_1__c in entity Contact_Profile__c')
                            || (run3Update.Error_Messages__c).contains(DUPLICATE_KEY_ERROR_MSG)){
                        
                        //MJS: 03Oct2014 - added double check that the matching key2 is valid
                        //if it is then add the case to acceptedOpenSpanCasesKey4 to try to match against
                        //contactProfile using Key2;  if not then add to the OrphanCases
                        if(ContactUtils.validateCaseMatchKey2(OpenSpanCasesMap.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c).First_Name__c,
                            OpenSpanCasesMap.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c).Surname__c,
                             OpenSpanCasesMap.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c).Address1__c,
                               OpenSpanCasesMap.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c).Post_Code__c)){
                            acceptedOpenSpanCasesKey4.add(OpenSpanCasesMap.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c));
                        } else {
                            acceptedOpenSpanOrphanCases.add(OpenSpanCasesMap.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c));
                        }
                        
                    }
                    
                }
                
            }           
            //update run3UpdateList;
        }
        
        //This fourth run will try to match failing Cases using the second Key against contact profile object
        if(acceptedOpenSpanCasesKey4.size() > 0){
            
            List<Case> sfdcCases = new List<Case>();
            //List<Data_Hub__c> run4UpdateList = new List<Data_Hub__c>();
            
            for(Data_Hub__c acceptedOpenSpanCase : acceptedOpenSpanCasesKey4){
                
                try{
                    sfdcCases.add(OpenSpanUtilities.matchCaseByKey4(acceptedOpenSpanCase));
                } catch (Exception e){
                    rejectedOpenSpanCases.add(new Data_Hub__c(Id = acceptedOpenSpanCase.Id, status__c = OPENSPAN_FAIL_CASE, Error_Messages__c = acceptedOpenSpanCase.Error_Messages__c + '\nException: ' + e.getStackTraceString()));
                }
                
                
            }
             
            Database.SaveResult[] caseResults = Database.insert(sfdcCases, false);
            for(Integer i=0; i < CaseResults.size() ; i++){
                
                Data_Hub__c run4Update;
                if(dataHubRecordsToUpdate.get(acceptedOpenSpanCasesKey4[i].Id) != null){
                    run4Update = dataHubRecordsToUpdate.get(acceptedOpenSpanCasesKey4[i].Id);
                }else{
                    run4Update = new Data_Hub__c(Id = acceptedOpenSpanCasesKey4[i].Id);
                    dataHubRecordsToUpdate.put(acceptedOpenSpanCasesKey4[i].Id, run4Update);
                }
                
                if(caseResults[i].Success){
                    
                    run4Update.Status__c = OPENSPAN_SUCCESS_CASE;
                    run4Update.Case__c = caseResults[i].Id;
                    //run4Update.Error_Messages__c = '';
                    run4Update.Case_Number__c = sfdcCases[i].CaseNumber;
                    contactProfileCases.put(caseResults[i].Id, sfdcCases[i]);
                    caseCommentsToBeInserted.add(OpenSpanUtilities.createCaseComment(sfdcCases[i].Id, OpenSpanCasesNewMap.get(run4Update.Id)));
                      
                } else{
                    
                    run4Update.Status__c = OPENSPAN_FAIL_CASE;
                    //run4Update.Error_Messages__c = '';
                    
                    for(Database.Error error : caseResults[i].errors){
                        
                        if(error.message != null && error.message != '')
                            run4Update.Error_Messages__c += error.message + '\n\n';
                    }
                    
                    if((run4Update.Error_Messages__c).contains('not found for field Case_Matching_Key_2__c in entity Contact_Profile__c')
                            || (run4Update.Error_Messages__c).contains(DUPLICATE_KEY_ERROR_MSG)){
                        
                        //if(OpenSpanCasesMap.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c).Status__c == OPENSPAN_ACTIVE_CASE){
                            acceptedOpenSpanOrphanCases.add(OpenSpanCasesMap.get(sfdcCases[i].jl_Case_Inbound_Data_Id__c));
                        /*}
                        else{
                            
                            run4Update.Status__c = OPENSPAN_REJECTED_CASE;
                            run4Update.Error_Messages__c = 'Closed Case did not match customer';
                            
                            rejectedOpenSpanCases.add(run4Update);               
                            
                        }*/
                    }
                    
                }
                
            }           
            //update run4UpdateList;
        }  
        
        //This fifth run will try to created the Open Cases that failed all matching as Orphan cases
        if(acceptedOpenSpanOrphanCases.size() > 0){
            
            List<Case> sfdcCases = new List<Case>();
            //List<Data_Hub__c> run5UpdateList = new List<Data_Hub__c>();
            
            for(Data_Hub__c acceptedOpenSpanOrphanCase : acceptedOpenSpanOrphanCases){
                
                try{
                    sfdcCases.add(OpenSpanUtilities.createOrphanCase(acceptedOpenSpanOrphanCase));
                } catch (Exception e){
                    rejectedOpenSpanCases.add(new Data_Hub__c(Id = acceptedOpenSpanOrphanCase.Id, status__c = OPENSPAN_FAIL_CASE, Error_Messages__c = acceptedOpenSpanOrphanCase.Error_Messages__c + '\nException: ' + e.getStackTraceString()));
                }
                
                
            }
             
            Database.SaveResult[] caseResults = Database.insert(sfdcCases, false);
            for(Integer i=0; i < CaseResults.size() ; i++){
                
                Data_Hub__c run5Update;
                if(dataHubRecordsToUpdate.get(acceptedOpenSpanOrphanCases[i].Id) != null){
                    run5Update = dataHubRecordsToUpdate.get(acceptedOpenSpanOrphanCases[i].Id);
                }else{
                    run5Update = new Data_Hub__c(Id = acceptedOpenSpanOrphanCases[i].Id);
                }
                
                if(caseResults[i].Success){
                    
                    run5Update.Status__c = OPENSPAN_SUCCESS_CASE;
                    run5Update.Case__c = caseResults[i].Id;
                    run5Update.Error_Messages__c += 'Orphan Open Case';
                    run5Update.Case_Number__c = sfdcCases[i].CaseNumber;
                    caseCommentsToBeInserted.add(OpenSpanUtilities.createCaseComment(sfdcCases[i].Id, OpenSpanCasesNewMap.get(run5Update.Id)));
                      
                } else{

                    run5Update.Status__c = OPENSPAN_FAIL_CASE;
                    //run5Update.Error_Messages__c = '';
                    
                    for(Database.Error error : caseResults[i].errors){
                        
                        if(error.message != null && error.message != '')
                            run5Update.Error_Messages__c += error.message + '\n';
                    }
                }
                 
            }           
            //update run5UpdateList;
        }
        
        if(rejectedOpenSpanCases.size() > 0)
            update rejectedOpenSpanCases;
            
        // set the Case Number field and update the Data Hub records
        Set<id> caseIds = new Set<id>();
        for(Data_Hub__c dhrec : dataHubRecordsToUpdate.values()){
            if(dhrec.Case__c != null){
                caseIds.add(dhrec.Case__c);
            }
        }
        Map<Id, Case> newCases = new Map<Id, Case>([SELECT Id, CaseNumber FROM Case WHERE Id IN:caseIds]);
        for(Data_Hub__c dhrec : dataHubRecordsToUpdate.values()){
            if(dhrec.Case__c != null){
                dhrec.Case_Number__c = newCases.get(dhrec.Case__c).CaseNumber;
            }
        }
        system.debug('====dataHubRecordsToUpdate==='+dataHubRecordsToUpdate);
        update dataHubRecordsToUpdate.values();
        
        // link case with customer for all linked with customer profile
        if(contactProfileCases.keyset().size() > 0){
            List<Case> casesWithContactIds = [SELECT Id, Contact_Profile__c, Contact_Profile__r.Contact__c FROM Case WHERE Id IN:contactProfileCases.keyset()];
            if(casesWithContactIds.size() > 0){
                for(Case c: casesWithContactIds){
                    c.ContactId = c.Contact_Profile__r.Contact__c;
                }
                update casesWithContactIds; 
            }
        }
        
        // Create case comments for Initiating User
        if(caseCommentsToBeInserted.size() > 0)
            insert caseCommentsToBeInserted;
    }

}