/* Description  : Delete Emaildata related to the customers must be deleted after 18 months from created date.
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   11/02/2019        Vignesh Kotha                   Created                COPT-4300
*
*/
global class BAT_DeleteEmailMessages implements Database.Batchable<sObject>,Database.Stateful {
    global Map<Id,String> ErrorMap = new Map<Id,String>();
    global integer result=0;
    private static final DateTime Prior_Date = System.now().addMonths(-18);
    global String query;
    global Database.QueryLocator start(Database.BatchableContext BC){
	GDPR_Record_Limit__c gDPRLimit = GDPR_Record_Limit__c.getValues('BAT_DeleteEmailMessages');
    if(gDPRLimit.Record_Limit__c != NULL){
	String recordLimit = gDPRLimit.Record_Limit__c;
    String limitSpace = ' ' + 'Limit' + ' '; 
	query = 'SELECT Id,Subject,WhatId,WhoId,CreatedDate FROM Task where CreatedDate < : Prior_Date and Subject !=NULL' + limitSpace + recordLimit;
        }
        else{
            query = 'SELECT Id,Subject,WhatId,WhoId,CreatedDate FROM Task where CreatedDate < : Prior_Date and Subject !=NULL';
        }
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, list<Task> scope){
        List<id> recordstoDelete = new List<id>();
            if(scope.size()>0){
            try{
                for(Task recordScope : scope){
                    String whatidval = recordScope.Whatid;
                    String sub = recordScope.Subject;
                   if(recordScope.Subject.startsWith('Email:') && (whatidval==NULL || !whatidval.startsWith('500'))){
                        recordstoDelete.add(recordScope.id);
                   }                   
                }
                list<Database.deleteResult> srList = Database.delete(recordstoDelete, false);
                for(Integer counter = 0; counter < srList.size(); counter++) {
                    if (srList[counter].isSuccess()) {
                        result++;
                    }else {
                        for(Database.Error err : srList[counter].getErrors()) {
                            ErrorMap.put(srList.get(counter).id,err.getMessage());
                        }
                    }
                }
                /* Delete records from Recyclebin */
                  Database.emptyRecycleBin(scope);
            }Catch(exception e){
                EmailNotifier.sendNotificationEmail('Exception from BAT_DeleteEmailMessages ', e.getMessage());
            }
        }
    }
    global void finish(Database.BatchableContext BC) {
        String textBody ='';
        set<Id> allIds = new set<Id>();
        textBody+= result +' records deleted in object Email Messages'+'\n';
        if (!ErrorMap.isEmpty()){
            for (Id recordids : ErrorMap.KeySet())
            {
                allIds.add(recordids);
            } 
            textBody+= 'Error log: '+allIds+'\n';
        }
        EmailNotifier.sendNotificationEmail('GDPR Delete Email Messages Records', textBody);
    }
    
}