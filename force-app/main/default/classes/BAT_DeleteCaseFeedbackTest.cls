/* Description  : Delete case feed records when cases deleted from COPT-4293
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   20/02/2019        Vignesh Kotha                   Created               COPT-4293
*
*/
@IsTest
Public class BAT_DeleteCaseFeedbackTest {
    
    @IsTest
    Public static void testCreateCase(){
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        Case testCase;
        Contact testContact;
        System.runAs(runningUser) 
        {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;       
            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        System.runAs(testUser) 
        {
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;                                    
            Case ccCase = new Case(Bypass_Standard_Assignment_Rules__c = True,ContactId = testContact.Id);            
            Insert ccCase;            
            List<Case_Feedback__c> cList = new List<Case_Feedback__c>(); 
            for(integer i=0;i<10;i++){  
                cList.add(new Case_Feedback__c(Case__c = ccCase.Id));
            }
            Insert cList;     
            Database.BatchableContext BC;
            BAT_DeleteCaseFeedback obj = new BAT_DeleteCaseFeedback();
            Test.startTest();
            
            obj.start(BC);
            
            obj.execute(BC,cList); 
            
            obj.finish(BC);
            Test.stopTest();   
        }
    }
    
    @IsTest
    Public static void testExceptionMethod(){
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        Case testCase;
        Contact testContact;
        System.runAs(runningUser) 
        {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;       
            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        System.runAs(testUser) 
        {
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;
            List<Case> cList = new List<Case>(); 
            for(integer i=0;i<10;i++){                
                cList.add(new Case(Bypass_Standard_Assignment_Rules__c = True,ContactId = testContact.Id));
            }
            insert cList;
            List<Case_Feedback__c> cfList = new List<Case_Feedback__c>(); 
            for(integer i=0;i<10;i++){  
                cfList.add(new Case_Feedback__c(Case__c = cList[i].Id));
            }
            insert cfList;
            GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
            gDPR.GDPR_Class_Name__c = 'BAT_DeleteCaseFeedback';
            gDPR.Name = 'BAT_DeleteCaseFeedback';
            gDPR.Record_Limit__c = '';
            
            try{
                Insert gDPR;
                system.assertEquals(10, cList.size());
            }
            Catch(Exception e){
                system.debug('Exception Caught:'+e.getmessage());
            }
            Database.BatchableContext BC;
            BAT_DeleteCaseFeedback obj = new BAT_DeleteCaseFeedback();
            Test.startTest();
            obj.start(BC);
            Database.DeleteResult[] Delete_Result = Database.delete(cfList, false);
            obj.execute(BC,cfList); 
            obj.finish(BC);
            Test.stopTest();  
            
        }        
    }
    
}