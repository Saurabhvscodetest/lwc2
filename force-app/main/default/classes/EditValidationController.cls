/******************************************************************************
* @author       Shion Abdillah
* @date         20/10/2015
* @description  Case Edit Page override controller 
*
* EDIT RECORD
*
* LOG     DATE        Author	JIRA		COMMENT      
* 001     20/10/2015  SA		COPT-327	Original code
* 002     30/10/2015  MP		COPT-414	Add 'retURL' param to stop window closing after case edit
* 003     02/11/2015  SA		COPT-???	Do not display screen after an insert
* 004     02/11/2015  MP		COPT-418	Skip timestamp update in the event of DML update error
*
*******************************************************************************
*/
public with sharing class EditValidationController {
	public String RecordId {get;set;}
	public Id CaseId {get;set;}
	public Case CaseRec {get;set;} 
	public boolean HasRecordId {get;set;}
	public boolean IsClosedRecord {get;set;}
	public boolean IsConsole {get;set;}	
	public boolean IsLoadView {get;set;}	
	public boolean IsTimer = false;   
    public string RecordOwner {get;set;}
	public integer IntervalMins {get;set;}
	public Long ElapsedMins {get;set;}
	public Long ElapsedSeconds {get;set;}
    private Boolean IsSystemUser;
	  
	 public EditValidationController(ApexPages.StandardController stdController)
	 {	 
	 	String pageUrl = ApexPages.currentPage().getURL();
		System.debug('pageUrl =' + pageUrl);	 		 	
	 	IsSystemUser = (UserInfo.getName().contains('Automated') || UserInfo.getName().contains('System')) ? true : false;
		
		if(ApexPages.currentPage().getParameters().get('LoadView') != null){
			if(ApexPages.currentPage().getParameters().get('LoadView') == 'true')IsLoadView = true; 
		}    	
		if(ApexPages.currentPage().getParameters().get('Console') != null){
			if(ApexPages.currentPage().getParameters().get('Console') == 'true')IsConsole = true; 
		}		
	 	HasRecordId = false;
        CaseRec = (Case)stdController.getRecord();
        CaseId = CaseRec.Id;
        CaseRec = [select Id, Status, ContactId,LastModifiedDate, jl_Last_Edited__c, LastModifiedBy.Id, LastModifiedBy.Name, CreatedDate FROM Case WHERE Id = : CaseId];
        if(CaseRec.Status.contains('Closed')) {
        	IsClosedRecord = true;
        } else {
        	IsClosedRecord = false;
        }   
        System.debug('IsClosedRecord: ' + IsClosedRecord);     
        if (CaseId != null) {
        	HasRecordId = true;
        }        
        if(Config_Settings__c.getInstance('EditNotificationInterval') != null){
				IntervalMins = Integer.valueOf(Config_Settings__c.getInstance('EditNotificationInterval').Number_Value__c);
		}else{		
			IntervalMins = 5;
		}        		
     }
			
	public PageReference ViewRecord()  
	{
		Boolean IsContactId = CaseRec.ContactId != null ? true : false;
		PageReference pageRef = new PageReference('/apex/ServiceConsoleRedirect?Id=' + CaseId + '&IsContact=' + IsContactId); // <<002>>		
		return pageRef;		
	}
	
	public PageReference ForceEditRecord() 
	{
		
		PageReference pageRef;
		Boolean IsContactId = CaseRec.ContactId != null ? true : false;		
		pageRef = new PageReference('/' + CaseId + '/e?nooverride=1&retURL=%2Fapex%2FServiceConsoleRedirect%3FId%3D'+ CaseId + '%3FIsContact%3D' + IsContactId + '%3Fisdtp%3Dvw'); // <<002>>		
		pageRef.setRedirect(true); 			
		//CaseRec.Current_Editor__c = UserInfo.getUserId();					
		// <<004>> Add try/catch to stop fallover if case fails a validation rule
		try{
			CaseRec.jl_Last_Edited__c = DateTime.now();
			update CaseRec;
		} catch (Exception ex) { 
			system.debug('UNABLE TO STAMP EDITOR ON CASE DUE TO EXCEPTION: ' + ex.getMessage());
		}
		return pageRef;		
	}
	
	public PageReference CheckForEditable()
	{			
		if(IsSystemUser)
		{
			PageReference pageRef = new PageReference('/' + CaseId + '/e?isdtp=vw&nooverride=1&retURL=%2F' + CaseId); // <<002>>
			pageRef.setRedirect(false);
			return pageRef;			
		}
		
		if(!getIsWithinRecentlyEditedTimeRange() && IsClosedRecord == false && IsLoadView==null){						
			return ForceEditRecord();			
		} else {
			return null;			
		}				
	}
		
	
	public boolean getIsWithinRecentlyEditedTimeRange()
	{
		System.debug('CaseRec: ' + CaseRec);
		if(CaseRec.jl_Last_Edited__c == null)return false; 
		Long LastEditModeLong = CaseRec.jl_Last_Edited__c.getTime(); 		 
		Long LastModLong = CaseRec.LastModifiedDate.getTime();		
		Long CurrentTime = System.now().getTime();		
		Long milliseconds = CurrentTime - LastEditModeLong;
		ElapsedSeconds = milliseconds/1000;
		ElapsedMins = ElapsedSeconds/60;	
		System.debug('getIsWithinRecentlyEditedTimeRange mins: ' + ElapsedMins);	
		System.debug('CaseRec.LastModifiedDate: ' + CaseRec.LastModifiedDate);
		System.debug('CaseRec.CreatedDate: ' + CaseRec.CreatedDate);
		// <<003>>
		if(CaseRec.LastModifiedBy.Id != UserInfo.getUserId() && ElapsedMins < IntervalMins) {
			return true;
		}
		return false;
	}
	
	
}