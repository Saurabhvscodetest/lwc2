@isTest
private class OpenSpanDataHubTransformer_TEST {

    static testMethod void testCaseCreationByKey1() {
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        //UnitTestDataFactory.createTeamAndContactCentre();
        insert UnitTestDataFactory.createOpenSpan2CaseManagementMapping();
        insert UnitTestDataFactory.createOpenSpanBranchMapping();
        
        Data_Hub__c dataHubRecord = UnitTestDataFactory.createValidDataHubRecord();
        
        Config_Settings__c setting1 = new Config_Settings__c();
        setting1.Name = 'WS_Endpoint_ViewCustomerOrdersV1';
        setting1.Checkbox_Value__c = True;
        setting1.Text_Value__c = 'Test1';
        setting1.Description__c = 'Inserting customsettings1';
        setting1.Number_Value__c = 1;
        insert setting1;
        
        Config_Settings__c setting2 = new Config_Settings__c();
        setting2.Name = 'WS_Endpoint_EmailContent';
        setting2.Checkbox_Value__c = False;
        setting2.Text_Value__c = 'Test2';
        setting2.Description__c = 'Inserting customsettings2';
        setting2.Number_Value__c = 2;
        insert setting2;
        
        Config_Settings__c setting3 = new Config_Settings__c();
        setting3.Name = 'WS_Endpoint_ManageCustomerComments';
        setting3.Checkbox_Value__c = True;
        setting3.Text_Value__c = 'Test3';
        setting3.Description__c = 'Inserting customsettings3';
        setting3.Number_Value__c = 3;
        insert setting3;
        
        Config_Settings__c setting4 = new Config_Settings__c();
        setting4.Name = 'WS_Endpoint_SearchCustomer';
        setting4.Checkbox_Value__c = False;
        setting4.Text_Value__c = 'Test4';
        setting4.Description__c = 'Inserting customsettings4';
        setting4.Number_Value__c = 4;
        insert setting4;
        
        Config_Settings__c setting5 = new Config_Settings__c();
        setting5.Name = 'WS_Endpoint_ViewCustomerOrdersV2';
        setting5.Checkbox_Value__c = False;
        setting5.Text_Value__c = 'Test5';
        setting5.Description__c = 'Inserting customsettings5';
        setting5.Number_Value__c = 5;
        insert setting5;
        
        
        /*Data_Hub__c dataHubRecord = new Data_Hub__c(
            First_name__c = 'fName',
            Surname__c = 'lName',
            Email__c = 'email@address.com',
            Status__c = 'Active',
            Data_Hub_Origin__c = clsDataHubHandler.ORIGIN_OPENSPAN,
            Data_Hub_Action__c = clsDataHubHandler.OPENSPAN_INBOUND_ACTION);*/
            
        Contact c = new Contact(
            FirstName = 'fName',
            LastName = 'lName',
            Email = 'email@address.com'
            //Case_Matching_Key_1__c = 'fnamelnameemail@address.com'
            );
        insert c;
        c = [select id, firstname, lastname, Case_Matching_Key_1__c, Case_Matching_Key_2__c from contact where id =:c.Id];
        System.debug(JSON.serialize(c));
        
        insert dataHubRecord;
        dataHubRecord = [SELECT Id, Name, status__c, case__c FROM Data_Hub__c where id=:dataHubRecord.Id];
        
        Case result = [SELECT Id, ContactId, Contact_Profile__c FROM Case WHERE jl_Case_Inbound_Data_Id__c =: dataHubRecord.Id];
        System.debug(JSON.serialize(result));
        
        
        System.assert(result.ContactId != null);
        System.assert(result.Contact_Profile__c == null);
        
    }
    
    static testMethod void testCaseCreationByKey2(){
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        //UnitTestDataFactory.createTeamAndContactCentre();
        insert UnitTestDataFactory.createOpenSpan2CaseManagementMapping();
        insert UnitTestDataFactory.createOpenSpanBranchMapping();
        
        Data_Hub__c dataHubRecord = UnitTestDataFactory.createValidDataHubRecord();
        // dataHubRecord.Email__c = null;
        
        /*Data_Hub__c dataHubRecord = new Data_Hub__c(
            First_name__c = 'fName',
            Surname__c = 'lName',
            Address1__c = 'addr',
            Post_Code__c = 'qwe 123',
            Status__c = 'Active',
            Data_Hub_Origin__c = clsDataHubHandler.ORIGIN_OPENSPAN,
            Data_Hub_Action__c = clsDataHubHandler.OPENSPAN_INBOUND_ACTION);*/
        
        Config_Settings__c setting1 = new Config_Settings__c();
        setting1.Name = 'WS_Endpoint_ViewCustomerOrdersV1';
        setting1.Checkbox_Value__c = True;
        setting1.Text_Value__c = 'Test1';
        setting1.Description__c = 'Inserting customsettings1';
        setting1.Number_Value__c = 1;
        insert setting1;
        
        Config_Settings__c setting2 = new Config_Settings__c();
        setting2.Name = 'WS_Endpoint_EmailContent';
        setting2.Checkbox_Value__c = False;
        setting2.Text_Value__c = 'Test2';
        setting2.Description__c = 'Inserting customsettings2';
        setting2.Number_Value__c = 2;
        insert setting2;
        
        Config_Settings__c setting3 = new Config_Settings__c();
        setting3.Name = 'WS_Endpoint_ManageCustomerComments';
        setting3.Checkbox_Value__c = True;
        setting3.Text_Value__c = 'Test3';
        setting3.Description__c = 'Inserting customsettings3';
        setting3.Number_Value__c = 3;
        insert setting3;
        
        Config_Settings__c setting4 = new Config_Settings__c();
        setting4.Name = 'WS_Endpoint_SearchCustomer';
        setting4.Checkbox_Value__c = False;
        setting4.Text_Value__c = 'Test4';
        setting4.Description__c = 'Inserting customsettings4';
        setting4.Number_Value__c = 4;
        insert setting4;
        
        Config_Settings__c setting5 = new Config_Settings__c();
        setting5.Name = 'WS_Endpoint_ViewCustomerOrdersV2';
        setting5.Checkbox_Value__c = False;
        setting5.Text_Value__c = 'Test5';
        setting5.Description__c = 'Inserting customsettings5';
        setting5.Number_Value__c = 5;
        insert setting5;
            
        Contact c = new Contact(
            FirstName = 'fName',
            LastName = 'lName',
            MailingStreet = 'addr',
            MailingPostalCode = 'qwe 123'
            //Case_Matching_Key_2__c = 'fnamelnameaddrqwe123'
            );
        insert c;
        c = [select id, firstname, lastname, Case_Matching_Key_1__c, Case_Matching_Key_2__c from contact where id =:c.Id];
        System.debug(JSON.serialize(c));
        
        system.assertEquals('fnamelnameaddrqwe123',c.Case_Matching_Key_2__c);
        
        insert dataHubRecord;
        dataHubRecord = [SELECT Id, Name, status__c, case__c, Error_Messages__c FROM Data_Hub__c where id=:dataHubRecord.Id];
        
        Case result = [SELECT Id, ContactId, Contact_Profile__c FROM Case WHERE jl_Case_Inbound_Data_Id__c =: dataHubRecord.Id];
        System.debug('MJS:');
        system.debug(dataHubRecord.Error_Messages__c);
        System.debug(JSON.serialize(result));
        
        
        System.assert(result.ContactId != null);
        System.assert(result.Contact_Profile__c == null);
        
        
    }
    
    static testMethod void testCaseCreationByKey3(){
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        //UnitTestDataFactory.createTeamAndContactCentre();
        insert UnitTestDataFactory.createOpenSpan2CaseManagementMapping();
        insert UnitTestDataFactory.createOpenSpanBranchMapping();
        
        Data_Hub__c dataHubRecord = UnitTestDataFactory.createValidDataHubRecord();
        
        Config_Settings__c setting1 = new Config_Settings__c();
        setting1.Name = 'WS_Endpoint_ViewCustomerOrdersV1';
        setting1.Checkbox_Value__c = True;
        setting1.Text_Value__c = 'Test1';
        setting1.Description__c = 'Inserting customsettings1';
        setting1.Number_Value__c = 1;
        insert setting1;
        
        Config_Settings__c setting2 = new Config_Settings__c();
        setting2.Name = 'WS_Endpoint_EmailContent';
        setting2.Checkbox_Value__c = False;
        setting2.Text_Value__c = 'Test2';
        setting2.Description__c = 'Inserting customsettings2';
        setting2.Number_Value__c = 2;
        insert setting2;
        
        Config_Settings__c setting3 = new Config_Settings__c();
        setting3.Name = 'WS_Endpoint_ManageCustomerComments';
        setting3.Checkbox_Value__c = True;
        setting3.Text_Value__c = 'Test3';
        setting3.Description__c = 'Inserting customsettings3';
        setting3.Number_Value__c = 3;
        insert setting3;
        
        Config_Settings__c setting4 = new Config_Settings__c();
        setting4.Name = 'WS_Endpoint_SearchCustomer';
        setting4.Checkbox_Value__c = False;
        setting4.Text_Value__c = 'Test4';
        setting4.Description__c = 'Inserting customsettings4';
        setting4.Number_Value__c = 4;
        insert setting4;
        
        Config_Settings__c setting5 = new Config_Settings__c();
        setting5.Name = 'WS_Endpoint_ViewCustomerOrdersV2';
        setting5.Checkbox_Value__c = False;
        setting5.Text_Value__c = 'Test5';
        setting5.Description__c = 'Inserting customsettings5';
        setting5.Number_Value__c = 5;
        insert setting5;
        
        /*Data_Hub__c dataHubRecord = new Data_Hub__c(
            First_name__c = 'fName',
            Surname__c = 'lName',
            Email__c = 'email@address.com',
            Status__c = 'Active',
            Data_Hub_Origin__c = clsDataHubHandler.ORIGIN_OPENSPAN,
            Data_Hub_Action__c = clsDataHubHandler.OPENSPAN_INBOUND_ACTION);*/
            
        Contact c = new Contact(
            FirstName = 'fName',
            LastName = 'lName'
            );
        insert c;
        
        Contact_Profile__c cp = new Contact_Profile__c(
            Contact__c = c.Id,
            FirstName__c = 'fName',
            LastName__c = 'lName',
            Email__c = 'email@address.com'
            //Case_Matching_Key_1__c = 'fnamelnameemail@address.com'
            );
        insert cp;
        
        insert dataHubRecord;
        dataHubRecord = [SELECT Id, Name, status__c, case__c FROM Data_Hub__c where id=:dataHubRecord.Id];
        
        Case result = [SELECT Id, ContactId, Contact_Profile__c FROM Case WHERE jl_Case_Inbound_Data_Id__c =:dataHubRecord.Id];
        
        // System.assert(result.ContactId == null);
        System.assert(result.Contact_Profile__c != null);
    }
    
    static testMethod void testCaseCreationByKey4(){
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        //UnitTestDataFactory.createTeamAndContactCentre();
        insert UnitTestDataFactory.createOpenSpan2CaseManagementMapping();
        insert UnitTestDataFactory.createOpenSpanBranchMapping();
        
        Data_Hub__c dataHubRecord = UnitTestDataFactory.createValidDataHubRecord();
        //dataHubRecord.Email__c = null;
        
        /*Data_Hub__c dataHubRecord = new Data_Hub__c(
            First_name__c = 'fName',
            Surname__c = 'lName',
            Address1__c = 'addr',
            Post_Code__c = 'qwe 123',
            Status__c = 'Active',
            Data_Hub_Origin__c = clsDataHubHandler.ORIGIN_OPENSPAN,
            Data_Hub_Action__c = clsDataHubHandler.OPENSPAN_INBOUND_ACTION);*/
            
        Contact c = new Contact(
            FirstName = 'fName',
            LastName = 'lName'
            );
        insert c;
        
        Config_Settings__c setting1 = new Config_Settings__c();
        setting1.Name = 'WS_Endpoint_ViewCustomerOrdersV1';
        setting1.Checkbox_Value__c = True;
        setting1.Text_Value__c = 'Test1';
        setting1.Description__c = 'Inserting customsettings1';
        setting1.Number_Value__c = 1;
        insert setting1;
        
        Config_Settings__c setting2 = new Config_Settings__c();
        setting2.Name = 'WS_Endpoint_EmailContent';
        setting2.Checkbox_Value__c = False;
        setting2.Text_Value__c = 'Test2';
        setting2.Description__c = 'Inserting customsettings2';
        setting2.Number_Value__c = 2;
        insert setting2;
        
        Config_Settings__c setting3 = new Config_Settings__c();
        setting3.Name = 'WS_Endpoint_ManageCustomerComments';
        setting3.Checkbox_Value__c = True;
        setting3.Text_Value__c = 'Test3';
        setting3.Description__c = 'Inserting customsettings3';
        setting3.Number_Value__c = 3;
        insert setting3;
        
        Config_Settings__c setting4 = new Config_Settings__c();
        setting4.Name = 'WS_Endpoint_SearchCustomer';
        setting4.Checkbox_Value__c = False;
        setting4.Text_Value__c = 'Test4';
        setting4.Description__c = 'Inserting customsettings4';
        setting4.Number_Value__c = 4;
        insert setting4;
        
        Config_Settings__c setting5 = new Config_Settings__c();
        setting5.Name = 'WS_Endpoint_ViewCustomerOrdersV2';
        setting5.Checkbox_Value__c = False;
        setting5.Text_Value__c = 'Test5';
        setting5.Description__c = 'Inserting customsettings5';
        setting5.Number_Value__c = 5;
        insert setting5;
        
        Contact_Profile__c cp = new Contact_Profile__c(
            Contact__c = c.Id,
            FirstName__c = 'fName',
            LastName__c = 'lName',
            Mailing_Street__c = 'addr',
            MailingPostCode__c = 'qwe 123'
            //Case_Matching_Key_2__c = 'fnamelnameaddrqwe123'
            );
        insert cp;
        
        insert dataHubRecord;
        dataHubRecord = [SELECT Id, Name, status__c, case__c FROM Data_Hub__c where id=:dataHubRecord.Id];
        
        Case result = [SELECT Id, ContactId, Contact_Profile__c FROM Case WHERE jl_Case_Inbound_Data_Id__c =:dataHubRecord.Id];
        
        // System.assert(result.ContactId == null);
        System.assert(result.Contact_Profile__c != null);
    }
    
    static testMethod void testCaseCreationNoMatchFound(){
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        //UnitTestDataFactory.createTeamAndContactCentre();
        insert UnitTestDataFactory.createOpenSpan2CaseManagementMapping();
        insert UnitTestDataFactory.createOpenSpanBranchMapping();
        
        Data_Hub__c dataHubRecord = UnitTestDataFactory.createValidDataHubRecord();
        
        /*Data_Hub__c dataHubRecord = new Data_Hub__c(
            First_name__c = 'fName',
            Surname__c = 'lName',
            Address1__c = 'addr',
            Post_Code__c = 'qwe 123',
            Status__c = 'Active',
            Data_Hub_Origin__c = clsDataHubHandler.ORIGIN_OPENSPAN,
            Data_Hub_Action__c = clsDataHubHandler.OPENSPAN_INBOUND_ACTION);*/
        
        insert dataHubRecord;
        dataHubRecord = [SELECT Id, Name, status__c, case__c FROM Data_Hub__c where id=:dataHubRecord.Id];
        
        Config_Settings__c setting1 = new Config_Settings__c();
        setting1.Name = 'WS_Endpoint_ViewCustomerOrdersV1';
        setting1.Checkbox_Value__c = True;
        setting1.Text_Value__c = 'Test1';
        setting1.Description__c = 'Inserting customsettings1';
        setting1.Number_Value__c = 1;
        insert setting1;
        
        Config_Settings__c setting2 = new Config_Settings__c();
        setting2.Name = 'WS_Endpoint_EmailContent';
        setting2.Checkbox_Value__c = False;
        setting2.Text_Value__c = 'Test2';
        setting2.Description__c = 'Inserting customsettings2';
        setting2.Number_Value__c = 2;
        insert setting2;
        
        Config_Settings__c setting3 = new Config_Settings__c();
        setting3.Name = 'WS_Endpoint_ManageCustomerComments';
        setting3.Checkbox_Value__c = True;
        setting3.Text_Value__c = 'Test3';
        setting3.Description__c = 'Inserting customsettings3';
        setting3.Number_Value__c = 3;
        insert setting3;
        
        Config_Settings__c setting4 = new Config_Settings__c();
        setting4.Name = 'WS_Endpoint_SearchCustomer';
        setting4.Checkbox_Value__c = False;
        setting4.Text_Value__c = 'Test4';
        setting4.Description__c = 'Inserting customsettings4';
        setting4.Number_Value__c = 4;
        insert setting4;
        
        Config_Settings__c setting5 = new Config_Settings__c();
        setting5.Name = 'WS_Endpoint_ViewCustomerOrdersV2';
        setting5.Checkbox_Value__c = False;
        setting5.Text_Value__c = 'Test5';
        setting5.Description__c = 'Inserting customsettings5';
        setting5.Number_Value__c = 5;
        insert setting5;
        
        Case result = [SELECT Id, ContactId, Contact_Profile__c FROM Case WHERE jl_Case_Inbound_Data_Id__c =:dataHubRecord.Id];
        
        System.assert(result.ContactId == null);
        System.assert(result.Contact_Profile__c == null);
        
        
    }
    
    static testMethod void testCaseCreationRejected(){
        
        insert UnitTestDataFactory.createOpenSpan2CaseManagementMapping();
        insert UnitTestDataFactory.createOpenSpanBranchMapping();
        
        Data_Hub__c dataHubRecord = UnitTestDataFactory.createValidDataHubRecord();
        dataHubRecord.Email__c = null;
        dataHubRecord.Status__c = 'Rejected';
        
        /*Data_Hub__c dataHubRecord = new Data_Hub__c(
            First_name__c = 'fName',
            Surname__c = 'lName',
            Address1__c = 'addr',
            Post_Code__c = 'qwe 123',
            Status__c = 'Rejected',
            Data_Hub_Origin__c = clsDataHubHandler.ORIGIN_OPENSPAN,
            Data_Hub_Action__c = clsDataHubHandler.OPENSPAN_INBOUND_ACTION);*/
        
        insert dataHubRecord;
        
        Config_Settings__c setting1 = new Config_Settings__c();
        setting1.Name = 'WS_Endpoint_ViewCustomerOrdersV1';
        setting1.Checkbox_Value__c = True;
        setting1.Text_Value__c = 'Test1';
        setting1.Description__c = 'Inserting customsettings1';
        setting1.Number_Value__c = 1;
        insert setting1;
        
        Config_Settings__c setting2 = new Config_Settings__c();
        setting2.Name = 'WS_Endpoint_EmailContent';
        setting2.Checkbox_Value__c = False;
        setting2.Text_Value__c = 'Test2';
        setting2.Description__c = 'Inserting customsettings2';
        setting2.Number_Value__c = 2;
        insert setting2;
        
        Config_Settings__c setting3 = new Config_Settings__c();
        setting3.Name = 'WS_Endpoint_ManageCustomerComments';
        setting3.Checkbox_Value__c = True;
        setting3.Text_Value__c = 'Test3';
        setting3.Description__c = 'Inserting customsettings3';
        setting3.Number_Value__c = 3;
        insert setting3;
        
        Config_Settings__c setting4 = new Config_Settings__c();
        setting4.Name = 'WS_Endpoint_SearchCustomer';
        setting4.Checkbox_Value__c = False;
        setting4.Text_Value__c = 'Test4';
        setting4.Description__c = 'Inserting customsettings4';
        setting4.Number_Value__c = 4;
        insert setting4;
        
        Config_Settings__c setting5 = new Config_Settings__c();
        setting5.Name = 'WS_Endpoint_ViewCustomerOrdersV2';
        setting5.Checkbox_Value__c = False;
        setting5.Text_Value__c = 'Test5';
        setting5.Description__c = 'Inserting customsettings5';
        setting5.Number_Value__c = 5;
        insert setting5;
        
        List<Case> casesList = [SELECT Id FROM Case];
        System.assert(casesList.size() == 0);
    }
    
    static testMethod void testMissingMatchingFields(){
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        //UnitTestDataFactory.createTeamAndContactCentre();
        insert UnitTestDataFactory.createOpenSpan2CaseManagementMapping();
        insert UnitTestDataFactory.createOpenSpanBranchMapping();
        
        Config_Settings__c setting1 = new Config_Settings__c();
        setting1.Name = 'WS_Endpoint_ViewCustomerOrdersV1';
        setting1.Checkbox_Value__c = True;
        setting1.Text_Value__c = 'Test1';
        setting1.Description__c = 'Inserting customsettings1';
        setting1.Number_Value__c = 1;
        insert setting1;
        
        Config_Settings__c setting2 = new Config_Settings__c();
        setting2.Name = 'WS_Endpoint_EmailContent';
        setting2.Checkbox_Value__c = False;
        setting2.Text_Value__c = 'Test2';
        setting2.Description__c = 'Inserting customsettings2';
        setting2.Number_Value__c = 2;
        insert setting2;
        
        Config_Settings__c setting3 = new Config_Settings__c();
        setting3.Name = 'WS_Endpoint_ManageCustomerComments';
        setting3.Checkbox_Value__c = True;
        setting3.Text_Value__c = 'Test3';
        setting3.Description__c = 'Inserting customsettings3';
        setting3.Number_Value__c = 3;
        insert setting3;
        
        Config_Settings__c setting4 = new Config_Settings__c();
        setting4.Name = 'WS_Endpoint_SearchCustomer';
        setting4.Checkbox_Value__c = False;
        setting4.Text_Value__c = 'Test4';
        setting4.Description__c = 'Inserting customsettings4';
        setting4.Number_Value__c = 4;
        insert setting4;
        
        Config_Settings__c setting5 = new Config_Settings__c();
        setting5.Name = 'WS_Endpoint_ViewCustomerOrdersV2';
        setting5.Checkbox_Value__c = False;
        setting5.Text_Value__c = 'Test5';
        setting5.Description__c = 'Inserting customsettings5';
        setting5.Number_Value__c = 5;
        insert setting5;
        
        Data_Hub__c dataHubRecord = UnitTestDataFactory.createValidDataHubRecord();
        dataHubRecord.Email__c = null;
        dataHubRecord.First_Name__c = null;
        
        insert dataHubRecord;
        dataHubRecord = [SELECT Id, Name, status__c, case__c FROM Data_Hub__c where id=:dataHubRecord.Id];
        
        Case result = [SELECT Id, ContactId, Contact_Profile__c FROM Case WHERE jl_Case_Inbound_Data_Id__c =:dataHubRecord.Id];
        
        System.assert(result.ContactId == null);
        System.assert(result.Contact_Profile__c == null);
        
    }
}