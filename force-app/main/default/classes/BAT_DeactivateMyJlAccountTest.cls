/* Description  : Delete Customer records as per given criteria in COPT-4283
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   29/03/2019        Vignesh Kotha                   Created                COPT-4283
*
*/
@isTest
Public class BAT_DeactivateMyJlAccountTest{
    static testmethod void testDeleteMyJLAcc(){        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        // Running user Context
        System.runAs(runningUser){
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;       
            insert CustomSettingTestDataFactory.createConfigSettings();
        }        
        contact testContact = CustomerTestDataFactory.createContact();
        testContact.Email = 'Tobepurged@gmail.com';
        insert testContact; 
        
        Test.startTest();
        List<contact> ConList=new  List<contact>();
         Id contactRecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Contact' AND DeveloperName = 'Ordering_Customer' LIMIT 1].Id;
        for(integer i=0; i<99; i++){
            ConList.add(new contact(FirstName='test',LastName='test1',Email = 'Tobepurged@gmail.com',Recordtypeid=contactRecordTypeId));
        }       
        insert ConList;
        System.debug('Connnss'+ConList);
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.GDPR_Class_Name__c = 'BAT_DeactivateMyJlAccount';
        gDPR.Name = 'BAT_DeactivateMyJlAccount';
        gDPR.Record_Limit__c = '5000';
        Insert gDPR;
        GDPR_Record_Limit__c gDPR1 = new GDPR_Record_Limit__c();
        gDPR1.GDPR_Class_Name__c = 'BAT_DeletePrimaryCustomer';
        gDPR1.Name = 'BAT_DeletePrimaryCustomer';
        gDPR1.Record_Limit__c = '5000'; 
        try{
           Insert gDPR1;
        }
        Catch(Exception e)
        {
            System.debug('Exception Caught:'+e.getmessage());
        }
        GDPR_Record_Limit__c gDPR2 = new GDPR_Record_Limit__c();
        gDPR2.GDPR_Class_Name__c = 'BAT_DeleteTransactionHeaders';
        gDPR2.Name = 'BAT_DeleteTransactionHeaders';
        gDPR2.Record_Limit__c = '5000'; 
        insert gDPR2;
        GDPR_Record_Limit__c gDPR3 = new GDPR_Record_Limit__c();
        gDPR3.GDPR_Class_Name__c = 'BAT_DeleteBulkLoyaltyAccount';
        gDPR3.Name = 'BAT_DeleteBulkLoyaltyAccount';
        gDPR3.Record_Limit__c = '5000'; 
        insert gDPR3;
        List<Loyalty_Account__c> laList = new List<Loyalty_Account__c>();
        Loyalty_Account__c la1 = new Loyalty_Account__c(contact__c = ConList[0].Id, IsActive__c = true,Email_Address__c = ConList[0].Email); 
		laList.add(la1);
        Loyalty_Account__c la2 = new Loyalty_Account__c(contact__c = ConList[0].Id, IsActive__c = false,Email_Address__c = ConList[0].Email); 
		laList.add(la2);
        insert laList;
        Database.BatchableContext BC;
        BAT_DeactivateMyJlAccount obj=new BAT_DeactivateMyJlAccount();
        obj.start(BC);
        obj.execute(BC,ConList);
        obj.finish(BC);
        DmlException expectedException;
        Test.stopTest();
    }
    @isTest
    Public static void testSchedule(){
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.GDPR_Class_Name__c = 'BAT_DeactivateMyJlAccount';
        gDPR.Name = 'BAT_DeactivateMyJlAccount';
        gDPR.Record_Limit__c = '5000'; 
        GDPR_Record_Limit__c gDPR1 = new GDPR_Record_Limit__c();
        gDPR1.GDPR_Class_Name__c = 'BAT_DeletePrimaryCustomer';
        gDPR1.Name = 'BAT_DeletePrimaryCustomer';
        gDPR1.Record_Limit__c = '5000'; 
        GDPR_Record_Limit__c gDPR2 = new GDPR_Record_Limit__c();
        gDPR2.GDPR_Class_Name__c = 'BAT_DeleteTransactionHeaders';
        gDPR2.Name = 'BAT_DeleteTransactionHeaders';
        gDPR2.Record_Limit__c = '5000'; 
        GDPR_Record_Limit__c gDPR3 = new GDPR_Record_Limit__c();
        gDPR3.GDPR_Class_Name__c = 'BAT_DeleteBulkLoyaltyAccount';
        gDPR3.Name = 'BAT_DeleteBulkLoyaltyAccount';
        gDPR3.Record_Limit__c = '5000'; 
        Test.startTest();
        Insert gDPR1;
        Insert gDPR;
        insert gDPR2;
        insert gDPR3;
        ScheduleBatchdeleteDeactivateMyJL obj = NEW ScheduleBatchdeleteDeactivateMyJL();
        obj.execute(null);  
        Test.stopTest();
    }  
}