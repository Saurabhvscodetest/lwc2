/**
* @description	Utility which helps retrieve lists of Queues which should be filtered from VF pages			
* @author		@tomcarman
*/

public class QueueOwnerFilterHelper {

	private static Map<String, QueueOwnerFilter__c> allQueueFilterRecords;

	private static final String BULK_FILTER = 'Filter_from_Bulk_Transfer__c';
	private static final String CHANGE_OWNER_FILTER = 'Filter_from_Change_Owner__c';


	/**
	* @description	Used to retrieve the list of Queues that should be filtered from the BulkCaseTransfer VF Page			
	* @author		@tomcarman	
	*/
	public static Set<String> getBulkTransferQueueFilter() {
		return filterQueueRecords(BULK_FILTER);
	}

	public static Set<String> getChangeOwnerQueueFilter() {
		return filterQueueRecords(CHANGE_OWNER_FILTER);
	}


	/**
	* @description	Used to retrieve the list of Queues that should be filtered from the ChangeOwner VF Page			
	* @author		@tomcarman	
	*/
    @TestVisible
	private static Set<String> filterQueueRecords(String filterType) {

		Set<String> returnSet = new Set<String>();

		for(QueueOwnerFilter__c filter : getAllQueueFilterRecords().values()) {
			if(filter.get(filterType) == true) {
				returnSet.add(filter.Name);
			}
		}

		return returnSet;

	}

	public static Set<String> getNotFilteredQueueRecords(String filterType) {

		Set<String> returnSet = new Set<String>();

		for(QueueOwnerFilter__c filter : getAllQueueFilterRecords().values()) {
			if(filter.get(filterType) == false) {
				returnSet.add(filter.Name);
			}
		}

		return returnSet;

	}

	/**
	* @description	Used to retrieve and cache all QueueOwnerFilter__c records.
	* @author		@tomcarman	
	*/
    @TestVisible
	private static Map<String, QueueOwnerFilter__c> getAllQueueFilterRecords() {

		if(allQueueFilterRecords == null) {
			allQueueFilterRecords = QueueOwnerFilter__c.getAll();
		}

		return allQueueFilterRecords;
	}


}