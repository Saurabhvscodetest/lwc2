@IsTest
Public class SF_Connex_LP_Connection_Utils_Test {

    @IsTest
    Public static void testcustomerNameCheck(){
        
        String test1 = 'vijay ambati';
        String test2 = 'VIJAY AMBATI';
        String test3 = 'Sekhar AMBATI';
        String test4 = 'e78eu';
        String test5 = 'E78EU';
        String test6 = 'E7 8EU';
        String test7 = ' ';
        String test8 = null;
        
        SF_Connex_LP_Connection_Utils.customerNameCheck(test1,test2);
        SF_Connex_LP_Connection_Utils.customerNameCheck(test1,test7);
        SF_Connex_LP_Connection_Utils.customerNameCheck(test1,test3);
        SF_Connex_LP_Connection_Utils.customerNameCheck(test1,test8);
        SF_Connex_LP_Connection_Utils.customerAddressCheck(test1,test2);
        SF_Connex_LP_Connection_Utils.customerAddressCheck(test1,test7);
        SF_Connex_LP_Connection_Utils.customerNameCheck(test1,test3);
        SF_Connex_LP_Connection_Utils.customerNameCheck(test1,test8);
        SF_Connex_LP_Connection_Utils.customerPostCodeCheck(test1,test3);
        SF_Connex_LP_Connection_Utils.customerPostCodeCheck(test4,test5);
        SF_Connex_LP_Connection_Utils.customerPostCodeCheck(test4,test6);
        SF_Connex_LP_Connection_Utils.customerPostCodeCheck(test4,test7);
        SF_Connex_LP_Connection_Utils.customerPostCodeCheck(test1,test8);
        
    }
    
}