/**
* @description  Unit tests for VocEmailHandler
* @author       @tomcarman
* @created      2017-03-07
*/


@isTest
private class VocEmailHandlerTest {
    
    
    /**
* @description  Set up dependent custom settings        
* @author       @tomcarman  
*/
    
    @testSetup static void initTestData() {
        
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        
        VOC_Survey_Settings__c vocSurveySettings = new VOC_Survey_Settings__c();
        vocSurveySettings.Name = 'Default';
        vocSurveySettings.Team_Prefixes__c = 'HM,DB,JC';
        vocSurveySettings.Survey_Grace_Period__c = 90;
        vocSurveySettings.Enabled__c = true;
        
        insert vocSurveySettings;
        
    }
    
    /**
* @description  Test that a VoC survey is sent when an eligible Case is closed      
*/
    
    @isTest static void sendSurveyWhenCustomerIsContactedAndCaseClosed() {
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        
        User frontOfficeUser;
        Contact testContact;
        Case testCase;
        Case_Activity__c testCaseActivity;
        
        System.runAs(runningUser) { // Avoid MIXED_DML
            
            frontOfficeUser = UnitTestDataFactory.getfrontOfficeUser('TEST_USER');
            frontOfficeUser.Team__c = 'Hamilton/Didsbury - CST';
            frontOfficeUser.Net_Dom_ID__c = 'DB000000';
            insert frontOfficeUser;
            String ps1 = 'JL_MYJL_Phase_3_Contact_Center_Permissions';
            UserTestDataFactory.assignPermissionSetToUser(frontOfficeUser.id,ps1);
        }
        
        User unrestrcitedUser =  UnitTestDataFactory.getUnrestrcitedUser('TEST123');
        insert unrestrcitedUser;
        Id orderingCustRecordTypeId =  null;
        System.runAs(unrestrcitedUser) {
            orderingCustRecordTypeId = CommonStaticUtils.getRecordType('Contact', CommonStaticUtils.CONTACT_RT_DEVELEPER_NAME_OC).Id ;
            testContact = UnitTestDataFactory.createContact();
            testContact.Email = 'tom.riddle@test.com';
            testContact.RecordTypeId = orderingCustRecordTypeId;
            insert testContact;
        }
        
        
        System.runAs(frontOfficeUser) {
            Test.startTest();
            testCase = UnitTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            testCase.Send_VOC_Survey__c = false;
            insert testCase;
            
            testCaseActivity = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            testCaseActivity.Resolution_Method__c = 'Customer Contacted';
            insert testCaseActivity;
            
            Contact relatedContact = [SELECT Id, Last_Survey_Send_Date__c FROM Contact WHERE Id = :testCase.ContactId LIMIT 1];
            Date dateBeforeSurveyBeingSent = relatedContact.Last_Survey_Send_Date__c ;
            System.assertEquals(null, dateBeforeSurveyBeingSent);
            
            // Requery and check Send VOC Survey checkbox is false
            testCase = [SELECT Id, Send_VOC_Survey__c FROM Case WHERE Id = :testCase.Id];
            System.assertEquals(false, testCase.Send_VOC_Survey__c);
            
            ClsCaseTriggerHandler.resetCaseTriggerStaticVariables(); // Reset recursion flags to mock a seperate execution from above insert
            
            
            testCase.Status = 'Closed - Resolved';
            update testCase;
            
            Test.stopTest();
            
            // Assert the Send VOC Survey checkbox is now true
            testCase = [SELECT Id, Send_VOC_Survey__c,Is_Customer_Ever_Been_Contacted__c,  ContactId FROM Case WHERE Id = :testCase.Id];
           // System.assertEquals(true, testCase.Send_VOC_Survey__c);
            
            // Assert Last_Survey_Send_Date__c was updated on Contact record
            relatedContact = [SELECT Id, Last_Survey_Send_Date__c FROM Contact WHERE Id = :testCase.ContactId LIMIT 1];
         // System.assertNotEquals(dateBeforeSurveyBeingSent, relatedContact.Last_Survey_Send_Date__c); 
            
        }
        
    }
    
    
    /**
* @description  Test that a VoC survey is sent when an eligible Case is closed      
* @author       @tomcarman  
*/
    
    @isTest static void surveyOnCaseClose_VocEmailShouldNotBeSentIfCustomerIsNotContacted() {
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User frontOfficeUser;
        Contact testContact;
        Case testCase;
        
        System.runAs(runningUser) { // Avoid MIXED_DML
            
            frontOfficeUser = UnitTestDataFactory.getfrontOfficeUser('TEST_USER');
            frontOfficeUser.Team__c = 'Hamilton/Didsbury - CST';
            frontOfficeUser.Net_Dom_ID__c = 'DB000000';
            insert frontOfficeUser;
            String ps1 = 'JL_MYJL_Phase_3_Contact_Center_Permissions';
            UserTestDataFactory.assignPermissionSetToUser(frontOfficeUser.id,ps1);
        }
        
        
        System.runAs(frontOfficeUser) {
            
            testContact = UnitTestDataFactory.createContact();
            testContact.Email = 'tom.riddle@test.com';
            insert testContact;
            
            testCase = UnitTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            insert testCase;
            
            
            // Requery and check Send VOC Survey checkbox is false
            Test.startTest();
            testCase = [SELECT Id, Send_VOC_Survey__c , Is_Customer_Ever_Been_Contacted__c FROM Case WHERE Id = :testCase.Id];
            System.assertEquals(false, testCase.Send_VOC_Survey__c);
            
            ClsCaseTriggerHandler.resetCaseTriggerStaticVariables(); // Reset recursion flags to mock a seperate execution from above insert
            testCase.Status = 'Closed - Resolved';
            
            update testCase;
            Test.stopTest();
            // Assert the Send VOC Survey checkbox is now true
            testCase = [SELECT Id, Send_VOC_Survey__c, Is_Customer_Ever_Been_Contacted__c, ContactId FROM Case WHERE Id = :testCase.Id];
            System.assertEquals(false, testCase.Send_VOC_Survey__c);
            System.assertEquals(false, testCase.Is_Customer_Ever_Been_Contacted__c);
            
            // Assert Last_Survey_Send_Date__c was updated on Contact record
            Contact relatedContact = [SELECT Id, Last_Survey_Send_Date__c FROM Contact WHERE Id = :testCase.ContactId LIMIT 1];
            //System.assertEquals(Date.today(), relatedContact.Last_Survey_Send_Date__c, 'Last Survey Date on Contact should be set to today'); 
            
        }
        
    }
    
    
    /**
* @description  Test that a VoC survey is issued when a Task (Customer Contact) is logged, and there are no open Cases          
* @author       @tomcarman  
*/
    
    @isTest static void surveyOnTaskLogged_success() {
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User frontOfficeUser;
        Contact testContact;
        
        System.runAs(runningUser) {
            
            frontOfficeUser = UnitTestDataFactory.getfrontOfficeUser('TEST_USER');
            frontOfficeUser.Team__c = 'Hamilton/Didsbury - CST';
            frontOfficeUser.Net_Dom_ID__c = 'DB000000';
            insert frontOfficeUser;
            String ps1 = 'JL_MYJL_Phase_3_Contact_Center_Permissions';
            UserTestDataFactory.assignPermissionSetToUser(frontOfficeUser.id,ps1);
        }
        
        User unrestrcitedUser =  UnitTestDataFactory.getUnrestrcitedUser('TEST123');
        insert unrestrcitedUser;
        Id orderingCustRecordTypeId =  null;
        System.runAs(unrestrcitedUser) {
            orderingCustRecordTypeId = CommonStaticUtils.getRecordType('Contact', CommonStaticUtils.CONTACT_RT_DEVELEPER_NAME_OC).Id ;
            testContact = UnitTestDataFactory.createContact();
            testContact.Email = 'tom.riddle@test.com';
            testContact.RecordTypeId = orderingCustRecordTypeId;
            insert testContact;
        }
        
        System.runAs(frontOfficeUser) {
            Test.startTest();
            Contact relatedContact = [SELECT Id, Last_Survey_Send_Date__c FROM Contact WHERE Id = :testContact.Id LIMIT 1];
            System.assert(relatedContact.Last_Survey_Send_Date__c == null , 'Last Survey Sent date was not populated before');
            
            // Log contact
            Task newCustomerContact = UnitTestDataFactory.createCustomerContact(testContact.Id);
            insert newCustomerContact;
            Test.stopTest();
            
            // Requery Task 
            newCustomerContact = [SELECT Id, WhoId, Send_VOC_Survey__c FROM Task WHERE Id = :newCustomerContact.Id];
            
           // System.assertEquals(true, newCustomerContact.Send_VOC_Survey__c);
            
            // Assert Last_Survey_Send_Date__c was updated on Contact record
            relatedContact = [SELECT Id, Last_Survey_Send_Date__c FROM Contact WHERE Id = :newCustomerContact.WhoId LIMIT 1];
          //  System.assert(relatedContact.Last_Survey_Send_Date__c != null , 'Last Survey Sent date is populated for Ordering Customer');
            //System.assertEquals(Date.today(), relatedContact.Last_Survey_Send_Date__c, 'Last Survey Date on Contact should be set to today'); 
            
        }
        
    }
    
    
    
    
    /**
* @description  Test that a survey is not sent when a Task (Customer Contact) is logged, and the Customer also has an open case.
*               (sending a VoC survey against a Case should always take precedence over a call log) 
* @author       @tomcarman  
*/
    
    @isTest static void surveyOnTaskLogged_CaseOpen_fail() {
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User frontOfficeUser;
        Contact testContact;
        Case testCase;
        
        System.runAs(runningUser) {
            frontOfficeUser = UnitTestDataFactory.getfrontOfficeUser('TEST_USER');
            frontOfficeUser.Team__c = 'Hamilton/Didsbury - CST';
            frontOfficeUser.Net_Dom_ID__c = 'DB000000';
            insert frontOfficeUser;
            String ps1 = 'JL_MYJL_Phase_3_Contact_Center_Permissions';
            UserTestDataFactory.assignPermissionSetToUser(frontOfficeUser.id,ps1);
        }
        
        System.runAs(frontOfficeUser) {
            
            testContact = UnitTestDataFactory.createContact();
            testContact.Email = 'tom.riddle@test.com';
            insert testContact;
            
            testCase = UnitTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            insert testCase;
            
            // Assert Case is Open and no emails have been sent yet
            testCase = [SELECT Id, IsClosed, Send_VOC_Survey__c FROM Case WHERE Id = :testCase.Id];
            
            System.assertEquals(false, testCase.IsClosed, 'The test case should be open');
            System.assertEquals(false, testCase.Send_VOC_Survey__c, 'Send VOC Survey should be false');
            
            
            // Log contact
            Task newCustomerContact = UnitTestDataFactory.createCustomerContact(testContact.Id);
            insert newCustomerContact;
            
            // Assert Send VOC Survey still false - because there is an open Case
            System.assertEquals(false, testCase.Send_VOC_Survey__c, 'Send VOC Survey should be false');
            
        }
        
    }
    
    
    /**
* @description  Test that a survey is not sent when the Contact.Survey_Opt_Out__c is true           
* @author       @tomcarman  
*/
    
    @isTest static void surveyOptedOut_fail() {
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User frontOfficeUser;
        Contact testContact;
        
        System.runAs(runningUser) {
            
            frontOfficeUser = UnitTestDataFactory.getFrontOfficeUser('TEST_USER');
            frontOfficeUser.Team__c = 'Hamilton/Didsbury - CST';
            frontOfficeUser.Net_Dom_ID__c = 'DB000000';
            insert frontOfficeUser;
            String ps1 = 'JL_MYJL_Phase_3_Contact_Center_Permissions';
            UserTestDataFactory.assignPermissionSetToUser(frontOfficeUser.id,ps1);
        }
        
        System.runAs(frontOfficeUser) {
            
            testContact = UnitTestDataFactory.createContact();
            testContact.Email = 'tom.riddle@test.com';
            testContact.Survey_Opt_Out__c = true; // Set survey opt-out to TRUE
            insert testContact;
            
            // Log contact
            Task newCustomerContact = UnitTestDataFactory.createCustomerContact(testContact.Id);
            insert newCustomerContact;
            
            // Requery Task
            newCustomerContact = [SELECT Id, Send_VOC_Survey__c FROM Task WHERE Id = :newCustomerContact.Id];
            
            System.assertEquals(false, newCustomerContact.Send_VOC_Survey__c, 'Send survey flag should be false, because Contact.Survey_Opt_Out__c is TRUE.');
            
        }
        
    }
    
    /**
* @description  Test that a survey is not sent when the Contact.Survey_Opt_Out__c is true           
* @author       @tomcarman  
*/
    
    @isTest static void vocSurveyOptedOut_fail() {
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User frontOfficeUser;
        Contact testContact;
        
        System.runAs(runningUser) {
            
            frontOfficeUser = UnitTestDataFactory.getFrontOfficeUser('TEST_USER');
            frontOfficeUser.Team__c = 'Hamilton/Didsbury - CST';
            frontOfficeUser.Net_Dom_ID__c = 'DB000000';
            insert frontOfficeUser;
            String ps1 = 'JL_MYJL_Phase_3_Contact_Center_Permissions';
            UserTestDataFactory.assignPermissionSetToUser(frontOfficeUser.id,ps1);
        }
        
        System.runAs(frontOfficeUser) {
            
            testContact = UnitTestDataFactory.createContact();
            testContact.Email = 'tom.riddle@test.com';
            testContact.VoC_Opt_Out__c = true; // Set VOC opt-out to TRUE
            insert testContact;
            
            // Log contact
            Task newCustomerContact = UnitTestDataFactory.createCustomerContact(testContact.Id);
            insert newCustomerContact;
            
            // Requery Task
            newCustomerContact = [SELECT Id, Send_VOC_Survey__c FROM Task WHERE Id = :newCustomerContact.Id];
            
            System.assertEquals(false, newCustomerContact.Send_VOC_Survey__c, 'Send survey flag should be false, because Contact.Survey_Opt_Out__c is TRUE.');
            
        }
        
    }
    
    /**
* @description  Test that a survey is not sent when the Contact.Last_Survey_Send_Date__c is inside the defined grace period
* @author       @tomcarman  
*/
    
    @isTest static void surveyInsideGracePeriod_fail() {
        
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User frontOfficeUser;
        Contact testContact;
        Case testCase;
        
        System.runAs(runningUser) {
            frontOfficeUser = UnitTestDataFactory.getFrontOfficeUser(('TEST_USER'));
            frontOfficeUser.Team__c = 'Hamilton/Didsbury - CST';
            frontOfficeUser.Net_Dom_ID__c = 'DB000000';
            insert frontOfficeUser;
            String ps1 = 'JL_MYJL_Phase_3_Contact_Center_Permissions';
            UserTestDataFactory.assignPermissionSetToUser(frontOfficeUser.id,ps1);
            
        }
        
        System.runAs(frontOfficeUser) {
            
            Test.startTest();
            testContact = UnitTestDataFactory.createContact();
            testContact.Email = 'tom.riddle@test.com';
            testContact.Last_Survey_Send_Date__c = Date.today().addDays(-78);
            
            insert testContact;
            
            testCase = UnitTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            
            insert testCase;
            
            ClsCaseTriggerHandler.resetCaseTriggerStaticVariables(); // Reset recursion flags to mock seperate execution from above insert;
            
            testCase.Status = 'Closed - Resolved';
            update testCase;
            
            // Requery Case
            testCase = [SELECT Id, Send_VOC_Survey__c, ContactId FROM Case WHERE Id = :testCase.Id];
            
            System.assertEquals(false, testCase.Send_VOC_Survey__c, 'Send email flag should still be false as Contact.Last_Survey_Send_Date__c is within grace period');
            
            // Assert Last_Survey_Send_Date__c was NOT updated on Contact record
            Contact relatedContact = [SELECT Id, Last_Survey_Send_Date__c FROM Contact WHERE Id = :testCase.ContactId LIMIT 1];
            System.assertNotEquals(Date.today(), relatedContact.Last_Survey_Send_Date__c, 'Last Survey Date on Contact should be set to today');    
            System.assertEquals(Date.today().addDays(-78), relatedContact.Last_Survey_Send_Date__c, 'Last survey date should still be 78 days ago, as setup in test');
            Test.stopTest();
        }
        
    }
    
    
    
    /** 
* @description  Test that a survey is not send when the user does not have an eligible Net_Dom_Id__c prefix
* @author       @tomcarman  
*/
    
    @isTest static void surveyIneligibleUser_fail() {
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User frontOfficeUser;
        Contact testContact;
        
        System.runAs(runningUser) {
            
            frontOfficeUser = UnitTestDataFactory.getFrontOfficeUser('TEST_USER');
            frontOfficeUser.Team__c = 'Hamilton/Didsbury - CST';
            frontOfficeUser.Net_Dom_ID__c = ''; // Make sure NET Dom ID is blank so that this user is ineligible to send VoC survey
            insert frontOfficeUser;
            String ps1 = 'JL_MYJL_Phase_3_Contact_Center_Permissions';
            UserTestDataFactory.assignPermissionSetToUser(frontOfficeUser.id,ps1);
        }
        
        System.runAs(frontOfficeUser) {
            
            testContact = UnitTestDataFactory.createContact();
            testContact.Email = 'tom.riddle@test.com';
            insert testContact;
            
            // Log contact
            Task newCustomerContact = UnitTestDataFactory.createCustomerContact(testContact.Id);
            insert newCustomerContact;
            
            // Requery Task
            newCustomerContact = [SELECT Id, Send_VOC_Survey__c FROM Task WHERE Id = :newCustomerContact.Id];
            
            System.assertEquals(false, newCustomerContact.Send_VOC_Survey__c, 'Send survey flag should be false, because User is not eligible to trigger survey.');
            
        }
        
    }
    
    @isTest static void setting_OPt_out_Should_Update_All_Users_WithSameEmail() {
        Id orderingCustRecordTypeId = CommonStaticUtils.getRecordType('Contact', 'Ordering_Customer').Id ;
        Id normalCustRecordTypeId = CommonStaticUtils.getRecordType('Contact', 'Normal').Id ;
        Id masterCustRecordTypeId = null ;
        Id superSeededCustRecordTypeId = CommonStaticUtils.getRecordType('Contact', 'Superseded').Id ;
        Account acc =  new Account(Name = 'Test Account');
        Contact con = new Contact(AccountId = acc.id, LastName = 'Test111' , FirstName = 'John' , Salutation = 'Mr',  Email = 'antony12341234@madeupemail1234test.com' );
        
        //create customers of diff records types but with same details 
        Contact orderingCust = new Contact(FirstName=con.FirstName, LastName=con.LastName,email=con.Email,  RecordTypeId = orderingCustRecordTypeId);        
        Contact normalCust = new Contact(FirstName=con.FirstName, LastName=con.LastName,email=con.Email,  RecordTypeId = normalCustRecordTypeId);        
        //Contact masterCust = new Contact(FirstName=con.FirstName, LastName=con.LastName,email=con.Email,  RecordTypeId = null);        
        Contact superSeededCust = new Contact(FirstName=con.FirstName, LastName=con.LastName,email=con.Email, RecordTypeId = superSeededCustRecordTypeId);
        
        
        List<Contact> contactList = new List<Contact>{ orderingCust,normalCust,superSeededCust};
            insert contactList;
        
        Set<Id> contactIds= new Set<Id> { orderingCust.id,normalCust.id,superSeededCust.id};
            
            //checking if the 3 contacts inserted, has VOC opt out field set not set to true 
            List<Contact> actualContactList = new List<Contact>([select id from Contact where Id in :contactIds and VoC_Opt_Out__c = true]);
        system.assertEquals(0, actualContactList.size(), 'Contact records VoC_Opt_Out__c is not set '); 
        
        
        Test.startTest();
        //update only       
        Contact cust = [select id, VoC_Opt_Out__c from Contact where Id=: orderingCust.Id];
        system.assertEquals(false, cust.VoC_Opt_Out__c, 'Contact records VoC_Opt_Out__c is NOT  set');
        cust.VoC_Opt_Out__c = true;
        update cust;
        Test.stopTest();
        
        actualContactList = [select id from Contact where Id in :contactIds and VoC_Opt_Out__c = true];
        system.assertEquals(3, actualContactList.size(), 'Contact records VoC_Opt_Out__c is  set ');    
        
    }
    
    
    
}