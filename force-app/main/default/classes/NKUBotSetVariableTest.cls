/*
* @File Name   : NKUBotSetVariableTest 
* @Description : Test class related to NKU Chatbot functionalities  
* @Copyright   : Zensar
* @Jira #      : #5174
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0    28-JAN-2020           Vijay A                      Created
*/

@IsTest
Public class NKUBotSetVariableTest {

    Public static List< NKUBotSetVariable.NKUBotSetVariableRequest > chatbotInputs = new List< NKUBotSetVariable.NKUBotSetVariableRequest > ();
    Public static List< NKUBotSetVariable.NKUBotSetVariableRequest > chatbotInputs1 = new List< NKUBotSetVariable.NKUBotSetVariableRequest > ();
    Public static List< NKUBotSetVariable.NKUBotSetVariableRequest > chatbotInputs2 = new List< NKUBotSetVariable.NKUBotSetVariableRequest > ();
    @IsTest
    Public static void testMethodForNKUChatBotProcess(){
        
        NKUBotSetVariable.NKUBotSetVariableRequest nkuInput = new NKUBotSetVariable.NKUBotSetVariableRequest();
        
        nkuInput.NKUInput = 'Check';
        chatbotInputs.add(nkuInput);
        
        NKUBotSetVariable.NKUBotSetVariableRequest nkuInput2 = new NKUBotSetVariable.NKUBotSetVariableRequest();
        
        nkuInput2.NKUInput = null;
        chatbotInputs2.add(nkuInput2);
        
        Test.startTest();
        NKUBotSetVariable.NKUChatBotProcess(chatbotInputs);
        NKUBotSetVariable.NKUChatBotProcess(chatbotInputs1);
        NKUBotSetVariable.NKUChatBotProcess(chatbotInputs2);
        Test.stopTest();
    }
    
    
}