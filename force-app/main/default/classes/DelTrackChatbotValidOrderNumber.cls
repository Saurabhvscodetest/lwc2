/*
* @File Name   : DelTrackChatbotValidOrderNumber 
* @Description : Apex Class to related to MyJLChatBot Delivery Tracking functionalities  
* @Copyright   : Zensar
* @Jira #      : #5174
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0      18-MAR-20          Vijay A                       Created
*/
Public class DelTrackChatbotValidOrderNumber {

  Public class DelTrackChatBotRequest {
     @InvocableVariable(required= true)
     public String orderNumberInput;
   }
        
  Public class DelTrackChatBotResponse {
     @InvocableVariable(required= true)
      public String ExceptionStatus; 
   }
    
    @Invocablemethod(Label ='Chatbot Valid Order Check' description='Check for Valid Reschedule Order Number')
    Public static List<DelTrackChatBotResponse> DelTrackChatBotProcess(List<DelTrackChatBotRequest > chatbotInputReqList){
        
        List<DelTrackChatBotResponse>   chatBotDelResponseList = new List<DelTrackChatBotResponse>();
        DelTrackChatBotResponse chatBotDelResponse = new DelTrackChatBotResponse();
        try{
           
             List<String> chatbotInputs = new List<String>();
           for( DelTrackChatBotRequest chatbotReq : chatbotInputReqList ){
               chatbotInputs.add ( chatbotReq.orderNumberInput );}
            
       	   Boolean orderNumberCheck = DelTrackConstants.orderNumberValidCheck(chatbotInputs[0]);
                
                if(orderNumberCheck != False){    
                chatBotDelResponse.ExceptionStatus = 'Valid Order';
                }else
                {
                    chatBotDelResponse.ExceptionStatus = 'InValid Order';
                }
        
                chatBotDelResponseList.add(chatBotDelResponse);
            //Exception Generator
            if(Test.isRunningTest())
			{
				Integer a = 1/0;
			}
                
        return chatBotDelResponseList;
            
        }
        
        catch(exception e){
            
           chatBotDelResponse.ExceptionStatus = 'Error'; 
           chatBotDelResponseList.add(chatBotDelResponse);
         
           return chatBotDelResponseList;
        }
       
    }
      


}