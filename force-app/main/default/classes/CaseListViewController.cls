public with sharing class CaseListViewController {
	
	public string refreshSeconds {get;set;}	

	public CaseListViewController()
	{
		CaseAutoRefreshSettings__c defaultSetting = CaseAutoRefreshSettings__c.getOrgDefaults();
		if(defaultSetting.Is_Active__c != false)refreshSeconds = String.valueOf(defaultSetting.Default_Interval__c);
	}

	
	public List<selectOption> getIntervalOptions()
	{
		List<selectOption> listItems = new List<selectOption>();
		listItems.add(new selectOption('null','Off'));		
		listItems.add(new selectOption('5000','5 seconds (demo)'));
		listItems.add(new selectOption('60000','1 Minute'));
		listItems.add(new selectOption('300000','5 Minutes'));
		listItems.add(new selectOption('600000','10 Minutes'));
		listItems.add(new selectOption('1200000','20 Minutes'));
		return listItems;
	}

	public PageReference Reload()
	{
		return null;
	}
}