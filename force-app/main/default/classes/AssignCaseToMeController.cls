public class AssignCaseToMeController {
	
    @AuraEnabled
    public static boolean checkCaseWithQueueOrUser(String caseId) {
        
        List<Case> caseList = [ SELECT Id, OwnerId FROM Case WHERE id = :caseId ];
        
        if(caseList.size()==0) {
            return false;
        }
        Case cas = caseList[0];
            
        if(String.valueOf(cas.OwnerId).startsWith('005')) {
            return true;
        }
        else {
            return false;
        }
    }
    
    @AuraEnabled
    public static void assignCaseToMe(String caseId) {
        
        List<Case> caseList = [ SELECT Id, OwnerId FROM Case WHERE Id = :caseId FOR UPDATE ];
        Case cas;
        if(caseList.size()>0) {
        	cas = caseList[0];
        }
        cas.OwnerId = UserInfo.getUserId();
        try {
        	update cas;
        }
        catch(Exception e) {
            System.debug('Error : '+e.getMessage());
        }
    }
}