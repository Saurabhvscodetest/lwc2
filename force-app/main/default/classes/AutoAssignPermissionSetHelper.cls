public class AutoAssignPermissionSetHelper {
	
    public static final String AAPS_NAME_PREFIX = 'ROLE: ';
    public static final String CASE_SERVICE_PRESENCE_STATUS_PERMSET = 'Case Service Presence Status';
    public static Map<String, Auto_Assign_Permission_Set__c> aapsMap = new Map<String, Auto_Assign_Permission_Set__c>();
    
    public static void upsertAutoAssignPermSetsForQueueMembers(Set<Id> roleIds) {		

		for (Auto_Assign_Permission_Set__c aaps : [SELECT Name, Permission_Sets_To_Assign__c FROM Auto_Assign_Permission_Set__c]){
			aapsMap.put(aaps.Name, aaps);	
		}		

		List<Auto_Assign_Permission_Set__c> aapsToBeUpserted = new List<Auto_Assign_Permission_Set__c>();
        
		for(UserRole role : [SELECT Name FROM UserRole WHERE Id IN :roleIds]){
			Auto_Assign_Permission_Set__c aap = null;
			String aapName = getAutoAssignPermissionSetName(role.Name); 
			if(!aapsMap.keySet().contains(aapName)){
				aap  = new Auto_Assign_Permission_Set__c(Name = aapName,  Permission_Sets_To_Assign__c = CASE_SERVICE_PRESENCE_STATUS_PERMSET);
			} else {
				aap = aapsMap.get(aapName);
				if(String.isEmpty(aap.Permission_Sets_To_Assign__c)) {
					aap.Permission_Sets_To_Assign__c = CASE_SERVICE_PRESENCE_STATUS_PERMSET;	
				} else {
					aap.Permission_Sets_To_Assign__c = aap.Permission_Sets_To_Assign__c + ';' + CASE_SERVICE_PRESENCE_STATUS_PERMSET;		
				}			
			}
			if(aap != null) {
				aapsToBeUpserted.add(aap);
			}
		}
		upsert aapsToBeUpserted;
	}
	
	@Testvisible
	private static String getAutoAssignPermissionSetName(String roleName){
		return (AutoAssignPermissionSetHandler.AAPS_ROLE_NAME_PREFIX + roleName);
	}
}