@isTest
private class OrderResponseLEX_Tests {
	
    @isTest static void testPropertiesAreSet(){

        OrderResponseLEX orderResponse = new OrderResponseLEX();

        coOrderHeader orderHeader = new coOrderHeader();
        List<coOrderHeader> orderHeaders = new List<coOrderHeader>();
        orderHeaders.Add(orderHeader);

        orderResponse.Status = 'Success';
        orderResponse.ErrorDescription = 'Some description';
        orderResponse.Orders = orderHeaders;

        System.assertEquals('Success', orderResponse.Status);
        System.assertEquals('Some description', orderResponse.ErrorDescription);
        System.assertEquals(orderResponse.Orders.size(), 1);
    }
	
}