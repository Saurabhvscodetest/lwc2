/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
  001  27/06/16    NA                  Decommissioning contact profile  
 */
@isTest
private class MyJL_UtilTest {

    static testMethod void test_calculateChecksum() { 
        set<long>  inputSet = new set<long>();
        
        // test with an empty map
        map<long, integer> myMapEmpty = MyJL_Util.calculateChecksum(inputSet);
    
        system.debug('myMapEmpty: '+myMapEmpty);
        system.assertEquals(0, myMapEmpty.size());

        // test with a non-empty map
        inputSet.add(42);
        inputSet.add(1001);
        inputSet.add(9999999);
        
        map<long, integer> myMap3 = MyJL_Util.calculateChecksum(inputSet);

        system.debug('myMap3: '+myMap3);
        system.assertEquals(3, myMap3.size());
        // Admittedly the following checksums have been experimentally found, but they must never change. If they do then the checksum generator has changed and that
        // will cause problems. 
        system.assertEquals(5, (Integer)myMap3.get(42));
        system.assertEquals(1, (Integer)myMap3.get(1001));
        system.assertEquals(8, (Integer)myMap3.get(9999999));
    }
    static testMethod void test_ReadOnlyMode() {    
        system.assertEquals(false, MyJL_Util.inReadOnlyMode());
    }
    
    // need to test we do phone number manipulation properly
    // null and '' should become null
    // anything less than the min (8) should become null
    // anything between the min and the max should be as is
    // anything over the max (19)should get truncated
    private static testMethod void test_PhoneFixUp() {
        List<contact> cps = new List<contact>();   //<<001>>
        cps.add(new contact(lastname='a'));   //<<001>>
        cps.add(new contact(lastname='b', phone=''));   //<<001>>
        cps.add(new contact(lastname='c',phone='0'));   //<<001>>
        cps.add(new contact(lastname='d',phone='1234'));   //<<001>>
        // valid range
        String TEST_5 = '12345678';
        String TEST_6 = '07941365983';
        String TEST_7 = '00447941365983';
        String TEST_8 = '1234567890123456789';
        String TEST_9 = '123456789012345678901234567890';
        
        cps.add(new contact(lastname='e',phone=TEST_5));   //<<001
        cps.add(new contact(lastname='f',phone=TEST_6));   //<<001
        cps.add(new contact(lastname='g',phone=TEST_7));   //<<001
        cps.add(new contact(lastname='h',phone=TEST_8));   //<<001
        // over the max
        cps.add(new contact(lastname='i',phone=TEST_9));   //<<001
        // illegal characters, alpha in string is not allowed
        cps.add(new contact(lastname='j',phone='447941nj365983'));   //<<001
        cps.add(new contact(lastname='k',phone='447941365983NJ'));   //<<001
        
        Test.StartTest();
        insert cps;
        Test.StopTest();
        
        // requery 
        List<contact> cpsAfter = [SELECT lastname, phone FROM contact ORDER BY lastname ASC];   //<<001
        
        system.assertEquals(11, cpsAfter.size());
        // the ones that should get blanked
        system.assertEquals(null, cpsAfter[0].phone);   //<<001
        system.assertEquals(null, cpsAfter[1].phone);   //<<001
        system.assertEquals(null, cpsAfter[2].phone);   //<<001
        system.assertEquals(null, cpsAfter[3].phone);   //<<001
        // the ones that should be preserved
        system.assertEquals(TEST_5, cpsAfter[4].phone);   //<<001
        system.assertEquals(TEST_6, cpsAfter[5].phone);   //<<001
        system.assertEquals(TEST_7, cpsAfter[6].phone);   //<<001
        system.assertEquals(TEST_8, cpsAfter[7].phone);   //<<001
        // the ones that should be truncated
        system.assertNotEquals(TEST_9, cpsAfter[8].phone);   //<<001
        system.assertEquals(TEST_8, cpsAfter[8].phone);    //<<001
        // these should be blanked as nj was present in string
        system.assertEquals(null, cpsAfter[9].phone);    //<<001
        system.assertEquals(null, cpsAfter[10].phone);      //<<001             
    } 
    
    private class TestTransactionWrapper extends MyJL_Util.TransactionWrapper { 
        private Integer                                         requestIndex = -1;
        private Boolean                                         transactionFound = false;
        private Boolean                                         isProcessed = false;
        
        private SFDCKitchenDrawerTypes.KDTransaction                    inputKDTransaction = null;
        private SFDCKitchenDrawerTypes.ActionResult     transactionResult = MyJL_Util.getSuccessActionResultStatus();
        private String                                          stackTraceDetails = null;
        
        /* Empty Constructor */
        public TestTransactionWrapper() {
        }
         
        public Integer getRequestIndex() {
            return this.requestIndex;
        }
        
        public void setRequestIndex(Integer requestIndex) {
            this.requestIndex = requestIndex;
        }
        
        public override SFDCKitchenDrawerTypes.KDTransaction getKDTransactionInput() {
            return this.inputKDTransaction;
        }
        
        public void setKDTransactionInput(SFDCKitchenDrawerTypes.KDTransaction inputKDTransaction) {
            this.inputKDTransaction = inputKDTransaction;
        }
        
        
        public override SFDCKitchenDrawerTypes.ActionResult getActionResult() {
            return this.transactionResult;
        }
        
        public void setActionResult(SFDCKitchenDrawerTypes.ActionResult transactionResult) {
            this.transactionResult = transactionResult;
        }
        
    
        public override String getStackTrace() {
            return this.stackTraceDetails;
        } 
        /*
        public void setStackTrace(String stackTraceDetails) {
            this.stackTraceDetails = stackTraceDetails;
        }*/
        
        /*public Boolean getTransactionFound() {
            return this.transactionFound;
        }
        
        public void setTransactionFound(Boolean transactionFound) {
            this.transactionFound = transactionFound;
        }*/
        
        public Boolean getIsProcessed() {
            return this.isProcessed;
        }
        
        public void setIsProcessed(Boolean isProcessed) {
            this.isProcessed = isProcessed;
        }

    }   
    
    private static testMethod void testIsNullWithInteger(){
    	
    	integer intValue = 1;
    	boolean result = MyJL_Util.isNull(intValue);
    	System.assertEquals(false, result, 'Result should be false as we are testing an integer and it\'s not a string');
    }
	
	private static testmethod void testGetCustomerIdWhenNull(){
		SFDCMyJLCustomerTypes.Customer customer;
    	System.assertEquals(null, MyJL_Util.getCustomerId(customer), 'Should have returned null');
	}

	private static testmethod void testGetCustomerIdFromPotentialCustomerWhenNull(){
		Potential_Customer__c customer = new Potential_Customer__c();
    	System.assertEquals(null, MyJL_Util.getCustomerId(customer), 'Should have returned null');
	}

	private static testmethod void testGetCustomerIdFromContactProfileWhenNull(){
		Contact customer = new Contact();
    	System.assertEquals(null, MyJL_Util.getCustomerId(customer), 'Should have returned null');
	} 
    
    private static testmethod void testKDLog() {
        CustomSettings.setTestLogHeaderConfig(true, -1);
        CustomSettings.setTestLogDetailConfig(true, -1);

        Log_Header__c lh = new Log_Header__c(Service__c='send');
        insert lh;
        
        List<TestTransactionWrapper> transactionWrappers = new List<TestTransactionWrapper>();
        TestTransactionWrapper tw = new TestTransactionWrapper();
        SFDCKitchenDrawerTypes.KDTransaction inputKDTransaction = new SFDCKitchenDrawerTypes.KDTransaction();
        tw.setKDTransactionInput(inputKDTransaction);
        
        
        transactionWrappers.add(tw);
        
        logUtil.logKDDetails(lh.Id, transactionWrappers);
    }
    
    private static testmethod void testMillisecondsDTM() {
        String dtm = '2015-02-20T05:06:44.160Z';
        
        DateTime dt = myjl_Util.dateTimeString2DateTimeMsecs(dtm);
        system.assertNotEquals(null, dt, 'expected non null dtm'); 
        
        system.assertEquals(2015, dt.year());
        system.assertEquals(2, dt.month());
        system.assertEquals(20, dt.day());
        system.assertEquals(5, dt.hour());
        system.assertEquals(6, dt.minute());
        system.assertEquals(44, dt.second());
        system.assertEquals(160, dt.millisecond());
        
        
    }
}