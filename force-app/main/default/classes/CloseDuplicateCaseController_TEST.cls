@isTest private class CloseDuplicateCaseController_TEST {
	public CloseDuplicateCaseController_TEST() {}
		
	@testSetup static void setup() {
         UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
    }
	
	@isTest static void testSave()
    {

		UnitTestDataFactory.setRunValidationRules(false);

        Contact con = UnitTestDataFactory.createContact();
        con.Email = 'a@bb.com';
        con.Prefered_method_of_Contact__c = 'Facebook';
        con.Facebook_username__c = 'facebookuser@test.com';
        insert con;
        
        Case case1 = UnitTestDataFactory.createNormalCase(con);
        Case case2 = UnitTestDataFactory.createNormalCase(con);
        List<Case> cases = new List<Case>{case1,case2};
        insert cases;
        

        case2.ParentId = case1.id;
        case2.Additional_Information__c = 'Test Close Case';

        Test.setCurrentPage(Page.CloseDuplicateCase);
        ApexPages.currentPage().getParameters().put('Id', case2.Id);

        CloseDuplicateCaseController controller = new CloseDuplicateCaseController(new ApexPages.StandardController(case2) );
        
        PageReference pageRef = controller.Save();

      	System.assert(pageRef != null, 'Page Reference is null'); 
      	System.assertEquals(case2.Status, 'Closed - Duplicate', 'Incorrect status on Case.'); 
        System.assertEquals(case2.jl_Action_Taken__c, 'No response required', 'Incorrect action taken on Case.'); 
    }
}