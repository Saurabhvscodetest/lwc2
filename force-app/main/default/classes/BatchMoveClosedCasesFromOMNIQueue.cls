global class BatchMoveClosedCasesFromOMNIQueue implements Database.Batchable<SObject>, Database.stateful{
	@TestVisible public static final String EMAIL_ENABLE_CONFIG = 'Enable Email Logging for Batch';
	// @TestVisible public static final String ADMIN_USER_ID = '005b0000004L7TV';
	@TestVisible public static final String ADMIN_USER_ID = userinfo.getuserid();
	
	@TestVisible public static final String QUEUE_LITERAL = 'Queue';
	@TestVisible private Set<Id> successfulCasesIds;
    @TestVisible private Map<String, String> unsuccessfulMap;
    @TestVisible private Set<Id> omniQueueIds = new Set<Id>(); 
   
	global BatchMoveClosedCasesFromOMNIQueue() {
		this.successfulCasesIds = new Set<Id>();
        this.unsuccessfulMap = new Map<String, String>();	
	}	
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
		Set<Id> groupIds = new Set<Id>(new Map<Id, Group>([SELECT Id, Name, Type FROM Group WHERE Type =:QUEUE_LITERAL AND QueueRoutingConfigId != null]).keySet());
		System.debug('groupIds ::' + groupIds);
		List<QueuesObject> existingQueueSobjects = [select ID,queueid from  QueuesObject where queueid IN :groupIds];
		System.debug('existingQueueSobjects ::' + existingQueueSobjects);
		
		for(QueuesObject qSobj : existingQueueSobjects){
			omniQueueIds.add(qSobj.queueid);
		}
		if (!omniQueueIds.isEmpty()){
			return Database.getQueryLocator([Select Id from Case where OwnerId IN : omniQueueIds and isClosed = true]);    	
		} 
		return null;
    }
    
    
    global void execute(Database.BatchableContext BC, List<Case> caseList) {
    	System.debug('caseList ' + caseList);
    	if(caseList != null && !caseList.isEmpty()){
	    	for(Case caseObj : caseList) {
			    caseobj.Bypass_Standard_Assignment_Rules__c = true; 
	    		caseObj.ownerId = ADMIN_USER_ID;
	    	}
	    	
	    	List<Database.SaveResult> updateResults = Database.update(caseList, false);


	        for(Integer i = 0; i < updateResults.size(); i++) {
	            // If Success
	            if(updateResults[i].isSuccess()) {
	                successfulCasesIds.add(updateResults[i].getId());
	            
	            // If Error
	            } else {
	
	                String errorsConcatenated = '';
	
	                for(Database.Error error : updateResults[i].getErrors()) {
                        errorsConcatenated = errorsConcatenated + ' | ' + error.getMessage();
	                }
	
	                unsuccessfulMap.put((String)caseList[i].Id, errorsConcatenated);
	            }
	        }
	
	        System.debug('*** | BatchMoveClosedCasesFromOMNIQueue | successfulCasesIds: ' + successfulCasesIds);
	        System.debug('*** | BatchMoveClosedCasesFromOMNIQueue | unsuccessfulMap: ' + unsuccessfulMap);
	    	
    	}
    	
    }
    
    global void finish(Database.BatchableContext BC) {
        try {
            Config_Settings__c emailEnabledForBatch = Config_Settings__c.getInstance(EMAIL_ENABLE_CONFIG);
            if(emailEnabledForBatch != null && emailEnabledForBatch.Checkbox_Value__c) {
                Integer loggingLevel = 1; // default to 1;
                if(emailEnabledForBatch.Number_Value__c != null) {
                    loggingLevel = Integer.valueOf(emailEnabledForBatch.Number_Value__c);
                }
                if(loggingLevel == 1 || (loggingLevel == 2 && !unsuccessfulMap.isEmpty())) {
                    String emailBody = '';
                    emailBody += 'BatchMoveClosedCasesFromOMNIQueue job has run: \n\n\n';
                    emailBody += 'The following records were updated succesfully: \n\n';
                    emailBody += successfulCasesIds.size();
                    emailBody += '\n\n';
                    emailBody += 'The following records encountered errors: \n\n';
                    emailBody += unsuccessfulMap.size();

                    EmailNotifier.sendNotificationEmail('BatchMoveClosedCasesFromOMNIQueue Results', emailBody);
                }
            }

        } catch (Exception e) {

            System.debug('*** | BatchMoveClosedCasesFromOMNIQueue: EMAIL FAILED: ' + e); // ALlow to silently fail - some sandboxes maybe have reached emailsend limit
            System.debug('*** | BatchCaseForceUpdateForEntitlements.cls:221 | successfulMap: ' + successfulCasesIds);
            System.debug('*** | BatchCaseForceUpdateForEntitlements.cls:222 | unsuccessfulMap: ' + unsuccessfulMap);

        }
    }
    
     
}