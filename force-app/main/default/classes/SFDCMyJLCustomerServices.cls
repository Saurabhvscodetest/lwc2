//Generated by wsdl2apex
//
//	001		13/11/14	NTJ		MJL-1294		Add parenthesis to isEnabled code as order of precedence is ambiguous and && || reads Left to Right.
//	002		19/02/15	NTJ		MJL-1572 		Salesforce Maintenance Window support
//
global class SFDCMyJLCustomerServices {


	webservice static SFDCMyJLCustomerTypes.CustomerResponseType CreateCustomer(SFDCMyJLCustomerTypes.RequestHeaderType RequestHeader,SFDCMyJLCustomerTypes.Customer[] Customer) {
		if (!isEnabled()) return getInactiveServiceResponse();

		SFDCMyJLCustomerTypes.CustomerRequestType request = new SFDCMyJLCustomerTypes.CustomerRequestType();
		request.RequestHeader = RequestHeader;
		request.Customer = Customer;
		//SFDCMyJLCustomerTypes.CustomerResponseType response = MyJL_CreateCustomerHandler.process(request);

		MyJL_UpdateCustomerHandler.mode = MyJL_UpdateCustomerHandler.MODE_CREATE;
		SFDCMyJLCustomerTypes.UpdateCustomerResponseType responseTemp = MyJL_UpdateCustomerHandler.process(request);
		//convert UpdateCustomerResponseType into CustomerResponseType
		SFDCMyJLCustomerTypes.CustomerResponseType response = new SFDCMyJLCustomerTypes.CustomerResponseType();
		response.ActionRecordResults = responseTemp.ActionRecordResults;
		response.ResponseHeader = responseTemp.ResponseHeader;

		return response; 
	}

	webservice static SFDCMyJLCustomerTypes.CustomerResponseType JoinCustomerLoyaltyAccount(SFDCMyJLCustomerTypes.RequestHeaderType RequestHeader,
			SFDCMyJLCustomerTypes.Customer[] Customer) {
		
		if (!isEnabled()) return getInactiveServiceResponse();

		SFDCMyJLCustomerTypes.CustomerRequestType request = new SFDCMyJLCustomerTypes.CustomerRequestType();
		request.RequestHeader = RequestHeader;
		request.Customer = Customer;

		return MyJL_JoinLoyaltyAccountHandler.process(request);

	}

	webservice static SFDCMyJLCustomerTypes.CustomerResponseType LeaveCustomerLoyaltyAccount(SFDCMyJLCustomerTypes.RequestHeaderType RequestHeader,
				SFDCMyJLCustomerTypes.Customer[] Customer) {
		
		if (!isEnabled()) return getInactiveServiceResponse();

		SFDCMyJLCustomerTypes.CustomerRequestType request = new SFDCMyJLCustomerTypes.CustomerRequestType();
		request.RequestHeader = RequestHeader;
		request.Customer = Customer;

		return MyJL_LeaveLoyaltyAccountHandler.process(request);
	}

	webservice static SFDCMyJLCustomerTypes.UpdateCustomerResponseType UpdateCustomer(SFDCMyJLCustomerTypes.RequestHeaderType RequestHeader,
			SFDCMyJLCustomerTypes.Customer[] Customer) {
		
		if (!isEnabled()) return getInactiveServiceUpdateResponse();

		SFDCMyJLCustomerTypes.CustomerRequestType request = new SFDCMyJLCustomerTypes.CustomerRequestType();
		request.RequestHeader = RequestHeader;
		request.Customer = Customer;

		return MyJL_UpdateCustomerHandler.process(request);
	}

	webservice static SFDCMyJLCustomerTypes.GetCustomerResponseType GetCustomer(SFDCMyJLCustomerTypes.RequestHeaderType RequestHeader,
				SFDCMyJLCustomerTypes.Customer[] Customer) {

		if (!isEnabled()) return getInactiveServiceGetResponse();
		
		SFDCMyJLCustomerTypes.CustomerRequestType request = new SFDCMyJLCustomerTypes.CustomerRequestType();
		request.RequestHeader = RequestHeader;
		request.Customer = Customer; 

		//Call the specific handler to perform the actual implementation
		SFDCMyJLCustomerTypes.GetCustomerResponseType response = MyJL_GetCustomerHandler.process(request);
		return response; 
	}   

	// this variable is only used in unit test checking what happens when MyJL Settings__c.Activate_Account_Admin__c is FALSE
	@TestVisible private static Boolean TEST_ENABLE_SERVICES = Test.isRunningTest();
	/**
	 * MJL-1183 - only accept SFDCMyJLCustomerServices calls if MyJL Settings__c.Activate_Account_Admin__c == TRUE
	 * MJL-1294 - force order of precedence by using parenthesis
	 * MJL-1572 - check salesforce is in DEFAULT (Read/Write) mode
	 */
	private static Boolean isEnabled() {
		Boolean isReadWrite = !myJL_Util.inReadOnlyMode();
		return TEST_ENABLE_SERVICES || ( (true == CustomSettings.getMyJLSetting().getBoolean('Activate_Account_Admin__c')) && isReadWrite);
	}

	//default response for inactive service
	private static SFDCMyJLCustomerTypes.CustomerResponseType getInactiveServiceResponse() {
        SFDCMyJLCustomerTypes.CustomerResponseType response = new SFDCMyJLCustomerTypes.CustomerResponseType();
        SFDCMyJLCustomerTypes.ResponseHeaderType responseHeader = new SFDCMyJLCustomerTypes.ResponseHeaderType();
		SFDCMyJLCustomerTypes.ResultStatus resultStatus = MyJL_Util.getSuccessResultStatus();
		resultStatus = MyJL_Util.populateErrorDetailsHeader(MyJL_Const.ERRORCODE.INACTIVE_SERVICE, resultStatus);
		responseHeader.ResultStatus = resultStatus;
        response.ResponseHeader = responseHeader;
		return response;
	}

	//default response for inactive service
	private static SFDCMyJLCustomerTypes.GetCustomerResponseType getInactiveServiceGetResponse() {
		SFDCMyJLCustomerTypes.CustomerResponseType genericResponse = getInactiveServiceResponse();
		SFDCMyJLCustomerTypes.GetCustomerResponseType response = new SFDCMyJLCustomerTypes.GetCustomerResponseType();
		response.ResponseHeader = genericResponse.ResponseHeader;
		return response;
	}

	//default response for inactive service
	private static SFDCMyJLCustomerTypes.UpdateCustomerResponseType getInactiveServiceUpdateResponse() {
		SFDCMyJLCustomerTypes.CustomerResponseType genericResponse = getInactiveServiceResponse();
		SFDCMyJLCustomerTypes.UpdateCustomerResponseType response = new SFDCMyJLCustomerTypes.UpdateCustomerResponseType();
		response.ResponseHeader = genericResponse.ResponseHeader;
		return response;
	}
}