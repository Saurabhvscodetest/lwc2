@isTest
public with sharing class CasePrintViewExt_TEST {
	
    @testSetup static void setup() {
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();        
        User runningUser = UserTestDataFactory.getRunningUser();        
        User contactCentreUser;        
        Contact con;
        Case cas;
        CaseHistory ch;
        
        System.runAs(runningUser) {
            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
            con = CustomerTestDataFactory.createContact();
            insert con;
        }
        System.runAs(contactCentreUser) {             
            cas = CaseTestDataFactory.createPSECase(con.Id);
            insert cas;
        }
    }
    
    @isTest static void CasePrintViewExtTest() {
        List<Case> caseList = [ SELECT Id FROM Case LIMIT 1 ];
        Case cas = caseList[0];
        ApexPages.StandardController stdController = new ApexPages.StandardController(cas);
        Test.startTest();
        CasePrintViewExt obj = new CasePrintViewExt(stdController);
        obj.FilterCaseComments();
        obj.PrintInvoke();
        obj.Cancel();
        obj.RenderPreview();
        Test.stopTest();
    }
}