//***************************************************************************
//Edit    Date        Author      Comment
//001     ??/??/??    ??          Original Class
//002     12/06/19    RV          COPT-4681 Code Refactoring
//***************************************************************************
public with sharing class MilestoneTriggerTimeCalculator implements Support.MilestoneTriggerTimeCalculator {

    private static final String MILESTONE_NEXT_ACTION_NAME = MilestoneHandlerSupport.NEXT_ACTION_DATE;
    private static final String MILESTONE_NEXT_CASE_ACTIVITY_NAME = MilestoneHandlerSupport.NEXT_CASE_ACTIVITY;
    private static final String MILESTONE_WARNING = system.label.NextCaseActivityWarning; //<<002>> added 
    private static final String MILESTONE_VIOLATION = system.label.NextCaseActivityViolation; //<<002>> added

    private static Map<Id, MilestoneType> milestoneTypeMap; 

    public static Map<Id, Case> newCaseTriggerValues = new Map<Id, Case>();

    public Integer calculateMilestoneTriggerTime(String caseIdAsString, String milestoneTypeIdAsString) {
        // Cast Ids
        Id milestoneTypeId = (Id)milestoneTypeIdAsString;
        Id caseId = (Id)caseIdAsString;

        if(!newCaseTriggerValues.containsKey(caseId)) {
            return null;
        }

        // Return if cant find the MilestoneType which corresponds to provided Id
        if(!getMilestoneTypes().containsKey(milestoneTypeId)) {
            return null;
        }
        // Dispatch to method which caclulates milestone lenght for each type
        if(getMilestoneTypes().get(milestoneTypeId).Name == MILESTONE_NEXT_CASE_ACTIVITY_NAME) {
            return nextCaseActivityMilestoneCalculator(newCaseTriggerValues.get(caseId));
        }
        else 
        { 
            if(getMilestoneTypes().get(milestoneTypeId).Name == MILESTONE_NEXT_ACTION_NAME)
            {
                return nextActionDateMilestoneCalculator(newCaseTriggerValues.get(caseId));
            } 
            if(getMilestoneTypes().get(milestoneTypeId).Name == MILESTONE_WARNING)  //<<002>> added 
            {
                return nextCaseActivityWarningMilestoneCalculator(newCaseTriggerValues.get(caseId),milestoneTypeId);
            }
            if(getMilestoneTypes().get(milestoneTypeId).Name == MILESTONE_VIOLATION) //<<002>> added 
            {
               
               return nextCaseActivityViolationMilestoneCalculator(newCaseTriggerValues.get(caseId),milestoneTypeId);
            } 
            else 
            {
                return null;
            }
        }
    }

    private static Integer nextActionDateMilestoneCalculator(Case aCase) {
        Integer minutesBetween = DatetimeUtilities.getMinutesBetween(aCase.Next_Action_Date_Entry__c, aCase.JL_Next_Response_By__c);
        return minutesBetween;
    }

    private static Integer nextCaseActivityMilestoneCalculator(Case aCase) {
        Integer minutesBetween = (BusinessHours.diff(aCase.businessHoursId, adjustForGMTValues(BusinessHours.nextStartDate(aCase.businessHoursId, Datetime.now())), adjustForGMTValues(aCase.Next_Case_Activity_Due_Date_Time__c))/1000/60).intValue();
        return minutesBetween > 0 ? minutesBetween : 1;
    }
   
    //<<002>> This Method will return hours needed to run milestone based on business hours
    private static Integer nextCaseActivityWarningMilestoneCalculator(Case aCase,id milestoneTypeId)
    {        
        Datetime stdatetime = Datetime.now();
        List<CaseMilestone> cm_lst = new List<CaseMilestone>(); 
        // skiping for Google sheet or bulk case upload
        if(!(Limits.getQueries() >= Limits.getLimitQueries()-10))
        {
            if(aCase.Origin!='Google Sheet' && aCase.Origin!='IFC Queries Sheet' && aCase.Origin!='MP3 Exceptions' && aCase.Origin!='MP3 Queries' && aCase.Origin!='Incorrect Services' && aCase.Origin!='7 Day Report Complaints' && aCase.Origin!='All Directorate Exceptions' && aCase.Origin!='Backorder Exceptions') {
                cm_lst = [Select Id, case.createddate, StartDate,TargetDate from CaseMilestone where CaseId =: aCase.id and CompletionDate=null and MilestoneTypeId=:milestoneTypeId];
            }
        }
        if(cm_lst.isEmpty() == false && cm_lst[0].StartDate != null)
        {
            Datetime caseCreatedatePlusMin = cm_lst[0].Case.createddate.addMinutes(3);
            if(stdatetime>caseCreatedatePlusMin)
            {
                stdatetime = cm_lst[0].StartDate;
            }
        }
        Integer minutesBetween = (BusinessHours.diff(aCase.businessHoursId, (BusinessHours.nextStartDate(aCase.businessHoursId, stdatetime)), (aCase.jl_Customer_Response_By__c))/1000/60).intValue();
        
        return minutesBetween > 0 ? minutesBetween : 1; 
    }
    
    //<<002>> This Method will return hours needed to run milestone based on business hours
    private static Integer nextCaseActivityViolationMilestoneCalculator(Case aCase,id milestoneTypeId) 
    {        
        Datetime stdatetime = Datetime.now();
        List<CaseMilestone> cm_lst = new List<CaseMilestone>(); 
        Integer additionalMin = 1;

        // skiping for Google sheet or bulk case upload
        if(!(Limits.getQueries() >= Limits.getLimitQueries()-10))
        {
            if(aCase.Origin!='Google Sheet' && aCase.Origin!='IFC Queries Sheet' && aCase.Origin!='MP3 Exceptions' && aCase.Origin!='MP3 Queries' && aCase.Origin!='Incorrect Services' && aCase.Origin!='7 Day Report Complaints' && aCase.Origin!='All Directorate Exceptions' && aCase.Origin!='Backorder Exceptions') {
               cm_lst = [Select Id, CaseId, StartDate,case.createddate, TargetDate from CaseMilestone where CaseId =: aCase.id and CompletionDate=null and MilestoneTypeId=:milestoneTypeId];
            }
        }
        if(cm_lst.isEmpty() == false && cm_lst[0].StartDate != null)
        {
            Datetime caseCreatedatePlusMin = cm_lst[0].Case.createddate.addMinutes(3);
            if(stdatetime>caseCreatedatePlusMin)
            {
                stdatetime = cm_lst[0].StartDate;
            }
            else
            {
                additionalMin = 3;
            }               
        }
        Integer minutesBetween = (BusinessHours.diff(aCase.businessHoursId, (BusinessHours.nextStartDate(aCase.businessHoursId, stdatetime)), (aCase.Next_Case_Activity_Due_Date_Time__c))/1000/60).intValue();
        
        return minutesBetween > 0 ? minutesBetween+additionalMin : 1; 
    }
    private static Map<Id, MilestoneType> getMilestoneTypes() {
        if(milestoneTypeMap == null) {
            milestoneTypeMap = new Map<Id, MilestoneType>([SELECT Id, Name FROM MilestoneType]);
        }
        return milestoneTypeMap;
    }
    public static DateTime adjustForGMTValues(DateTime dt){
        Timezone tz = Timezone.getTimeZone('Europe/London');
        return dt.addHours(tz.getOffset(dt)/(60*60*1000));
    }

}