/// Handler Class that makes web service calls to 
//  Order management system and populates data into 
//	Customer Order class
//
// Edit    Date        By            Comments
//  001    28/05/14    Vikram Middha    Initial Version
public with sharing class CustomerOrderWSHandler {
	public CustomerOrderWSHandler() {
		
	}

	// WS call to get the customer ID using which Order will be queried
	public static String getCustomerId(){
		
		//TO DO

		return null;
	}

	public static List<CustomerOrder> getCustomerOrders(){

		// To Do

		return prepareTestData();
	}

	public static CustomerOrder getOrderLineItems(){

		// To Do

		//return null;
		return prepareTestOLI();
	}

	public static CustomerOrder getOrderComments(){

		// To Do

		return null;
	}

	public static Boolean postOrderComment(){

		// To Do

		return null;
	}

	public static Boolean setCaseFlag(){

		// To Do

		return null;
	}

	private static List<CustomerOrder> prepareTestData(){
		List<CustomerOrder> retList = new List<CustomerOrder>();

		CustomerOrder co1 = new CustomerOrder();
		co1.orderNumber = 'CO0-1001';
		co1.orderStatus = 'In Progress';
		co1.datePlaced = '01/05/2014';
		co1.totalOrderValue = '100.00';

		retList.add(co1);

		CustomerOrder co2 = new CustomerOrder();
		co2.orderNumber = 'CO0-1002';
		co2.orderStatus = 'On Hold';
		co2.datePlaced = '02/05/2014';
		co2.totalOrderValue = '999.00';

		retList.add(co2);
		return retList;

	}

	private static CustomerOrder prepareTestOLI(){
		CustomerOrder co1 = new CustomerOrder();
		co1.orderNumber = 'CO0-1001';
		co1.orderStatus = 'In Progress';
		co1.datePlaced = '01/05/2014';
		co1.totalOrderValue = '100.00';
		co1.shopperId = '11091';
		co1.paymentMethod = 'Credit Card';
		co1.cardLast4Digits = '4433';
		co1.cardExpiryDate = '10/05/2015';
		co1.billingName = 'Test Billing';
		co1.shippingMethod = 'Post';
		co1.expectedDeliveryDate = '10/05/2014';
		co1.mobilePhone = '9999999999';

		CustomerOrder.OrderLineItem oli = new CustomerOrder.OrderLineItem();
		oli.oItemId = '001';
		oli.fullfillmentSku = 'F-SKU';
		oli.sku = 'SKU-001';
		oli.isReturnable = 'Yes';
		oli.quantityOrdered = '5';
		oli.quantityReturned = '1';
		oli.quantityShipped = '5';
		oli.title = 'Shirts';
		oli.totalPriceIncVat = '500';
		oli.unitPartnerDiscount = '5';
		oli.unitPriceIncVat = '100';
		oli.carrier = 'Courier';
		oli.dateShipped = '10/05/2014';
		oli.trackingNumber = '99102134';
		oli.trackingUrl = 'http://www.johnlewis.com/';
		oli.deliveryCity = 'London';
		oli.deliveryCounty = 'UK';
		oli.deliveryPostcode = 'E1 005';
		oli.deliveryStreet1 = 'Com Street';
		oli.deliveryStreet2 = '555';
		oli.deliveryStreet3 = 'First Floor';
		oli.deliveryStreet4 = '';
		oli.deliveryForename = 'Vikram';
		oli.delivertSurname = 'Middha';
		oli.deliveryTitle = 'Mr';
		oli.bundleItems = '10';

		co1.addLineItem(oli);

		CustomerOrder.OrderComments oc = new CustomerOrder.OrderComments();
		oc.orderComments = 'Follow up with the Customer';
		oc.commentTime = '01/01/2014 10:00:00';
		oc.enteredByName = 'Vikram Middha';
		oc.enteredByPosition = 'Customer Service Rep';
		co1.addOrderComment(oc);

		CustomerOrder.OrderEmails oe = new CustomerOrder.OrderEmails();
		oe.content = 'A Test Email content';
		oe.subject = 'FW : Hello';
		oe.emailDate = '01/01/2014';
		co1.addOrderEmail(oe);
		return co1;
	}
}