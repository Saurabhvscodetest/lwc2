/* Description  : Test Class For GDPR - Potential Customer Data Deletion
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   26/12/2018        ChandraMouli Maddukuri          Created                COPT-4269
*
*/

@isTest
public class GDPR_BatchDeletePotentialCustomersTest {
    
    @isTest
    Public static void testPotentialCustomerDataDeletion() {
        List<Potential_Customer__c>  Potential_Customer = New List<Potential_Customer__c>();
        for(integer i=0; i<300; i++){
            Potential_Customer.add(new Potential_Customer__c(FirstName__c = 'Test', LastName__c = 'Customer'+i, Email__c = 'test@abc.com'));
        }
        try{
            insert Potential_Customer;
            system.assertEquals(300, Potential_Customer.size());
        }
        Catch(exception e){
            system.debug('Exception Caught:'+e.getmessage());
        }
        Database.BatchableContext BC;
        GDPR_BatchDeletePotentialCustomers obj = NEW GDPR_BatchDeletePotentialCustomers();
        Test.startTest();
        Database.DeleteResult[] Delete_Result = Database.delete(Potential_Customer, false);
        obj.start(BC);
        obj.execute(BC,Potential_Customer); 
        obj.finish(BC);
        Test.stopTest();
    }
    
    @isTest
    Public static void testDmlException_Delete()
    {
        List<Potential_Customer__c>  Potential_Customer = New List<Potential_Customer__c>();
        for(integer i=0; i<300; i++){
            Potential_Customer.add(new Potential_Customer__c(FirstName__c = 'Test', LastName__c = 'Customer'+i, Email__c = 'test@abc.com'));
        }
        
        insert Potential_Customer;
        system.assertEquals(300, Potential_Customer.size());
        Database.DeleteResult[] Delete_Result = Database.delete(Potential_Customer, false);
        DmlException expectedException;
        Test.startTest();
        try { 
            delete Potential_Customer;
        }
        catch (DmlException dException) {
            expectedException = dException; 
            system.assertNotEquals(NULL, expectedException);
        }
        Test.stopTest();
    }
    
     @isTest
    Public static void testSchedule(){
        Test.startTest();
        ScheduleBatchDeletePotentialCustomers obj = NEW ScheduleBatchDeletePotentialCustomers();
        obj.execute(null);  
        Test.stopTest();
    }
}