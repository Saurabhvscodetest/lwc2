/*------------------------------------------------------------
Author: 		AMIT KUMAR
Company: 		Titanium Fire Ltd
Description:   
Test Class: 	
    
History
<Edit>	<Date>      	<Authors Name>     	<Jira>		<Brief Description of Change>
001		2014-08-11    	AK                  xxxxxxxx 	Initial Release
002		23/10/14		NTJ					MJL-1210	Ensure transaction type returned to caller on Get
003		15/04/15		NTJ					MJL-1790 - Do not do a SELECT WHERE x IN set, if we have an empty set
004		20/04/15		ntj					Remove Debug.log

------------------------------------------------------------*/
public with sharing class MyJLKD_GetHandler {

	private static final String CONTENT_TYPE   = 'text/xml';
/* Wrapper object to group state with return object */
	private class GetTransactionWrapper extends MyJL_Util.TransactionWrapper { 
		private Integer 										requestIndex = -1;
		private Boolean											transactionFound = false;
		private Boolean											isProcessed = false;
		
		private SFDCKitchenDrawerTypes.KDTransaction 					inputKDTransaction = null;
		private SFDCKitchenDrawerTypes.ActionResult 	transactionResult = MyJL_Util.getSuccessActionResultStatus();
		private String											stackTraceDetails = null;
		
		/* Empty Constructor */
		public GetTransactionWrapper() {
		}
		 
		public Integer getRequestIndex() {
			return this.requestIndex;
		}
		
		public void setRequestIndex(Integer requestIndex) {
			this.requestIndex = requestIndex;
		}
		
		public override SFDCKitchenDrawerTypes.KDTransaction getKDTransactionInput() {
			return this.inputKDTransaction;
		}
		
		public void setKDTransactionInput(SFDCKitchenDrawerTypes.KDTransaction inputKDTransaction) {
			this.inputKDTransaction = inputKDTransaction;
		}
		
		
		public override SFDCKitchenDrawerTypes.ActionResult getActionResult() {
			return this.transactionResult;
		}
		
		public void setActionResult(SFDCKitchenDrawerTypes.ActionResult transactionResult) {
			this.transactionResult = transactionResult;
		}
		
	
	    public override String getStackTrace() {
			return this.stackTraceDetails;
		} 
		/*
		public void setStackTrace(String stackTraceDetails) {
			this.stackTraceDetails = stackTraceDetails;
		}*/
		
		/*public Boolean getTransactionFound() {
			return this.transactionFound;
		}
		
		public void setTransactionFound(Boolean transactionFound) {
			this.transactionFound = transactionFound;
		}*/
		
		public Boolean getIsProcessed() {
			return this.isProcessed;
		}
		
		public void setIsProcessed(Boolean isProcessed) {
			this.isProcessed = isProcessed;
		}

	}
	
	public static String getFullXmlName() {
		return MyJL_Util.getFullXmlName();
	}
	
	public static String getShortXmlName() {
		return MyJL_Util.getShortXmlName();
	}
    
public static SFDCKitchenDrawerTypes.ActionResponse process(SFDCKitchenDrawerTypes.RequestHeader request, String[] transIds) {	
		//Start Log
  	   ID 		logHeaderRecordId = LogUtil.startKDLogHeader(LogUtil.SERVICE.WS_KD_GET_CUSTOMER, request, null, transIds); 
  	   String 	logHeaderStackTrace = null;
  	   list<SFDCKitchenDrawerTypes.KDTransaction> KDtransactions = new list<SFDCKitchenDrawerTypes.KDTransaction>();
  	   SFDCKitchenDrawerTypes.ActionResponse response = new SFDCKitchenDrawerTypes.ActionResponse();
       List<GetTransactionWrapper> processTransactionWrapperList = new List<GetTransactionWrapper>();
       Map<String, GetTransactionWrapper> processTransactionWrapperMap = new Map<String, GetTransactionWrapper>();
       set<string> receivedTransactionIds = new set<string>();
       
       try{
       	
       	if(request == null || request.MessageId == null || String.IsBlank(request.MessageId)) {
        	//Return error indicating no Message ID was found
        	 response = MyJL_Util.getSuccessActionResponse();
        	 response = MyJL_Util.populateKDErrorDetailsHeader(MyJL_Const.ERRORCODE.MESSAGEID_NOT_PROVIDED, response);
			 
        }
        else if(! MyJL_Util.isMessageIdUnique(request.MessageId)) {
        	//Return error indicating the Message ID is a duplicate
        	response = MyJL_Util.getSuccessActionResponse();
        	response = MyJL_Util.populateKDErrorDetailsHeader(MyJL_Const.ERRORCODE.MESSAGEID_DUPLICATE, response);
			
        } else if(transIds == null || transIds.size() == 0) {
        	//Return error indicating we need at least 1 Transaction as input
        	response = MyJL_Util.getSuccessActionResponse();
        	response = MyJL_Util.populateKDErrorDetailsHeader(MyJL_Const.ERRORCODE.NO_TRANSACTION_DATA, response);
			
        }
        else {
        	
        	for(String str: transIds){
        		SFDCKitchenDrawerTypes.KDTransaction kdTransaction= new SFDCKitchenDrawerTypes.KDTransaction();
        		kdTransaction.transactionId = str;
        		KDtransactions.add(kdTransaction);
        	}
        	
        	//We've got a good header. Mirror messageId back in the response
        	response = MyJL_Util.getSuccessActionResponse();
        	response.MessageId = request.MessageId;
        	
        	//Populate Transaction objects in Response and error handling
    		Integer index = 0;
    		for(SFDCKitchenDrawerTypes.KDTransaction kdtransaction : KDtransactions) {
    			
    			GetTransactionWrapper processTransactionWrapper = new GetTransactionWrapper();
				processTransactionWrapper.setRequestIndex(index);  
				processTransactionWrapper.setKDTransactionInput(kdtransaction);	
				
				//Do we have a TransactionID
    			if(processTransactionWrapper.getTransactionId() == null ) {
    				//system.debug('inside TranactionID Null');
					//Set error indicating no CustomerID was found in this Customer record
    				processTransactionWrapper.setActionResult(MyJL_Util.populateKDErrorDetails(MyJL_Const.ERRORCODE.TRANSACTIONID_NOT_PROVIDED, processTransactionWrapper.getActionResult()));
					processTransactionWrapper.setIsProcessed(true);        				
    			} 
    			
    			else {
    			    //Add wrapper to map
    				receivedTransactionIds.add(processTransactionWrapper.getTransactionId());
    				processTransactionWrapperMap.put(processTransactionWrapper.getTransactionId(), processTransactionWrapper);
    				
    			}
    			
    			//Add wrapper object to list
    			processTransactionWrapperList.add(processTransactionWrapper);
				
				//Increment index counter
				index++;
    		}
    		
    		//Query Transaction Header
 			Transaction_Header__c[] transactionHeaders = queryTransactionHeaders(processTransactionWrapperMap.keySet());
 			//system.debug('AMIT transactionHeaders: '+ transactionHeaders);
 			final Set<Id> transactionIds = new Set<Id>();
 			for(Transaction_Header__c trans : transactionHeaders) {
				transactionIds.add(trans.Id);
			}
 			
 			final String xmlDocumentName = getFullXmlName();
			final Map<Id, String> xmlByTransactionId = new Map<Id, String>();
			if (!transactionIds.isEmpty()) {
				for(Attachment att : [select Id, Name, Body, ParentId from Attachment 
														where Name = :xmlDocumentName 
														and ParentId in :transactionIds 
																and ContentType =: CONTENT_TYPE]) {
					
					xmlByTransactionId.put(att.ParentId, att.Body.toString());
				}				
			}
 			/* Loop over query results & populate wrapper object */
    		for(Transaction_Header__c KDtransaction : transactionHeaders) {
				//Find the right wrapper object for this transaction 
				String xmlText = xmlByTransactionId.get(KDtransaction.Id);
				String fullXml = null;
				if (!String.isBlank(xmlText)) {
					fullXml = xmlText;
				}
				GetTransactionWrapper processTransactionWrapper = processTransactionWrapperMap.get(MyJL_Util.getTransactionId(KDtransaction));
				if(processTransactionWrapper != null) {
					processTransactionWrapper.setIsProcessed(true);
					//Create Transaction based on the Transaction Header instance
					SFDCKitchenDrawerTypes.KDTransaction returnTransaction = populateKDTransaction(KDtransaction,fullXml);
					processTransactionWrapper.getActionResult().kdTransaction = returnTransaction;
					
					
				}
    		}
 			
 			for(GetTransactionWrapper processTransactionWrapper : processTransactionWrapperList) {
    			if(! processTransactionWrapper.getIsProcessed()) {
    				processTransactionWrapper.setActionResult(MyJL_Util.populateKDErrorDetails(MyJL_Const.ERRORCODE.TRANSACTIONID_NOT_FOUND, processTransactionWrapper.getActionResult()));
    			}
    		
    		}
    		//response = MyJL_Util.getSuccessActionResponse();
        }
        
        //Set response payload and ensure it's in the same order as the inbound array of transactions (based on requestIndex)
        SFDCKitchenDrawerTypes.ActionResult[] responseTransactions = new SFDCKitchenDrawerTypes.ActionResult[processTransactionWrapperList.size()];
        for(GetTransactionWrapper processTransactionWrapper : processTransactionWrapperList) {
        	responseTransactions[processTransactionWrapper.getRequestIndex()] = processTransactionWrapper.getActionResult();
        	
        	//Echo input Transaction in the response in case of errors
        	if(! responseTransactions[processTransactionWrapper.getRequestIndex()].Success) {
        		
        		responseTransactions[processTransactionWrapper.getRequestIndex()].kdTransaction = processTransactionWrapper.getKDTransactionInput();
        		
        		if(receivedTransactionIds.contains(processTransactionWrapper.getTransactionId())) {
        			
        			responseTransactions[processTransactionWrapper.getRequestIndex()] = processTransactionWrapperMap.get(processTransactionWrapper.getTransactionId()).getActionResult();
        		}
        	}
        }	
        
        response.results = responseTransactions;  
        
        //Store log detail records
        LogUtil.logKDDetails(logHeaderRecordId, processTransactionWrapperMap.values());
        //Update log header record
        LogUtil.updateKDLogHeader(logHeaderRecordId, response, null);
        
		    } catch (Exception e) {
        	
        	response.Message = null;
        	response.resultType = null;
			response.Success = false;
			response.results = null;
        	response = MyJL_Util.populateKDErrorDetailsHeader(MyJL_Const.ERRORCODE.UNHANDLED_EXCEPTION, response);
			LogUtil.updateKDLogHeader(logHeaderRecordId, response, e.getStackTraceString());
			System.assertEquals(false, Test.isRunningTest(), 'exception thrown: ' + e + '; ' + e.getStackTraceString());
		}
	return response;
 }
 
 public static SFDCKitchenDrawerTypes.KDTransaction populateKDTransaction(final Transaction_Header__c transactionHeader, final String fullXml) {
		//system.debug('populateKDTransaction start [transactionHeader: ' + transactionHeader + ']');
		//Init return object
		SFDCKitchenDrawerTypes.KDTransaction kdTransaction = new SFDCKitchenDrawerTypes.KDTransaction();
		
		kdTransaction.transactionId = transactionHeader.Receipt_Barcode_Number__c;
		kdTransaction.fullXml = fullXml;
		kdTransaction.voidingTransactionId = transactionHeader.Voiding_Transaction_Id__c;
		// 002 set the transaction type
		kdTransaction.transactionType = transactionHeader.transaction_type__c; 
		//system.debug('AMIT tran=' + transactionHeader);
		return kdTransaction;
	}
     
    //#####################################################################################################################
    //## Queries
    //#####################################################################################################################
    
    private static Transaction_Header__c[] queryTransactionHeaders(Set<String> transIds) {
    	//system.debug('GetTransactionResponse-queryTransactionHeaders list of IDs for the Transaction_Header__c query: ' + transIds);
    	
    	List<Transaction_Header__c> transactionHeaders = new List<Transaction_Header__c>();
    	
    	if(transIds != null && transIds.size() > 0) {
			transactionHeaders = [
				SELECT Channel__c,
					Receipt_Barcode_Number__c, 
					Card_Number__r.Name,
					Customer_Email__c,
					Customer_Id__c,
					Membership_Number__c,
					MyJL_Barcode__c,
					MyJL_ESB_Message_ID__c,
					Store_Code__c,
					Transaction_Amount__c,
					Transaction_Date_Time__c,
					Voided_Transaction_Response__c,
					Voiding_Transaction_Id__c,
					Transaction_Type__c
				FROM 
					Transaction_Header__c
				WHERE
					Receipt_Barcode_Number__c IN :transIds
					
			];
    	}
		
		//system.debug('GetTransactionHeader-queryTransaction_Header__c query: ' + transactionHeaders);
    	
		return transactionHeaders;
    }
}