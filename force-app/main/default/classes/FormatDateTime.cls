public class FormatDateTime {
    public DateTime dateTimeValue { get; set; }
    public String getTimeZoneValue() {
        if( dateTimeValue != null) {
            return dateTimeValue.format(EHConstants.DATE_TIME_FORMAT);
        }
        return null;
    }   
    public String getDateValue() {
        if( dateTimeValue != null) {
            return dateTimeValue.format(EHConstants.DATE_FORMAT);
        }
        return null;
    }   
}