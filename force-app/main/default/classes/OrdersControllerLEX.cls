public with sharing class OrdersControllerLEX {
	public OrdersControllerLEX() {
		
	}

	@AuraEnabled
	public static OrderResponseLEX getOrdersForOrderNumber(String orderNumber){

		OrderResponseLEX response = new OrderResponseLEX();

		if(String.IsBlank(orderNumber)){
			response.Status = 'Error';
			response.ErrorDescription = 'Please provide order number';
			return response;
		}

		try{
			coSoapManager soapManger = new coSoapManager();
			response.orders = soapManger.getOrderHeaders(orderNumber);
			response.Status = soapManger.Status;
			response.ErrorDescription = soapManger.ErrorDescription;
		}
		catch (Exception e) {
			System.debug('getOrdersForCustomerDetails - Exception' + e);	
			response.Status = 'Error';
			response.ErrorDescription = 'There has been a problem, please contact support';
	    }
		return response; 
	}

	@AuraEnabled
	public static OrderResponseLEX getOrdersForCustomerDetails(String firstName, String lastName, string postcode, string email){

		OrderResponseLEX response = new OrderResponseLEX();


		if( String.IsBlank(email) && String.IsBlank(postcode) && String.IsBlank(lastName) && String.IsBlank(firstName)){
			response.Status = 'Error';
			response.ErrorDescription= 'Please provide customer details';

		}
		else if( String.IsBlank(lastName)  && String.IsBlank(firstName)){
			response.Status = 'Error';
			response.ErrorDescription = 'Firstname and lastName are required fields.';
		}

		else if( String.IsBlank(email) && String.IsBlank(postcode)){
			response.Status = 'Error';
			response.ErrorDescription = 'Email and Postcode cannot be blank at the same time.';
		}
		else{
			
			try{

				coSoapManager soapManger = new coSoapManager();
				coSoapManager.Customer customerSearchCriteria = new coSoapManager.Customer();
				customerSearchCriteria.FirstName = firstName;
				customerSearchCriteria.LastName = lastName;
				customerSearchCriteria.Postcode = postcode;
				customerSearchCriteria.Email = email;
			
				response.orders = new List<coOrderHeader>();
				response.orders = soapManger.getOrderHeaders(response.orders, 9, customerSearchCriteria);
				response.Status = soapManger.Status;
				response.ErrorDescription = soapManger.ErrorDescription;

				if (!response.orders.IsEmpty()) {
					response.orders.sort();
				}
			}
			catch (Exception e) {
				System.debug('getOrdersForCustomerDetails - Exception' + e);	
				response.Status = 'Error';
				response.ErrorDescription = 'There has been a problem, please contact support';
			}
		}

		return response;
	}
}