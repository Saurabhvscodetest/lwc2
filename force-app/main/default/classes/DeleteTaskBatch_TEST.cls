/******************************************************************************

* @author		Stuart Barber
* @date			06/03/2017
* @description	Test Class to test DeleteTaskBatch and DeleteTaskScheduler deletes tasks older than x amount of days

******************************************************************************/

@istest private class DeleteTaskBatch_TEST {

    private static User getBackOfficeUser(){
        User sysAdmin = UnitTestDataFactory.getSystemAdministrator('admin1');
        User backOfficeUser = UnitTestDataFactory.getBackOfficeUser('bo', 'JL.com - Bookings');
        List<User> uList = new List<User> {sysAdmin, backOfficeUser};
        insert uList;
    
        System.runAs(sysAdmin) {
            jl_runvalidations__c jrv = new jl_runvalidations__c(Name = backOfficeUser.Id, Run_Validations__c = true);
            insert jrv;     
        }
        return backOfficeUser;
    }
    
    public static String CRON_EXP = '0 0 12 1/1 * ? *';
    
    /**
	 * Code coverage for DeleteTaskBatch.apxc
	 * Deletes tasks that are outside retention period  
	 */
    @istest static void PerformTaskDeletionBatch_outsideRetentionPeriod() {
		
        User backOfficeUser = getBackOfficeUser();
        
        System.runAs(backOfficeUser) {
        	
        	string correctPermissionSet = 'Delete_Tasks';            
            PermissionSetAssignment assignCallLoggingPermissionSet = UnitTestDataFactory.assignPermissionSet(backOfficeUser.Id, correctPermissionSet);
            
            DeleteTaskBatch taskBatch = new DeleteTaskBatch();
        	taskBatch.daysToRetain = 30;
            Integer daysToRetain = taskBatch.daysToRetain;
            taskBatch.taskRecordType = 'MILESTONE';
            String taskRecordType = 'MILESTONE';
        
        	DateTime todaysDate = DateTime.now();
    		DateTime creationDate = todaysDate.addDays((daysToRetain + 1) * -1);
        
        	List<Task> createTask = UnitTestDataFactory.createTask(taskRecordType, creationDate, true);
        	
			system.assertEquals(3, createTask.size());
	
            test.startTest();
                
                Database.executeBatch(taskBatch);
           	test.stopTest();

			Integer expectedResult = 0; 
        	List<Task> actualResult = [SELECT Id FROM Task WHERE Id in :createTask];
            
            System.assertEquals(expectedResult, actualResult.size(), 'There should be zero Milestone Tasks');
        }
    }
    
    /**
	 * Code coverage for DeleteTaskBatch.apxc
	 * Retains tasks that are inside retention period (i.e. less than 30 days old) 
	 */
    @istest static void PerformTaskDeletionBatch_insideRetentionPeriod() {
			            
        User backOfficeUser = getBackOfficeUser();
        
        System.runAs(backOfficeUser) {
        	
        	string correctPermissionSet = 'Delete_Tasks';            
            PermissionSetAssignment assignCallLoggingPermissionSet = UnitTestDataFactory.assignPermissionSet(backOfficeUser.Id, correctPermissionSet);	
        
        	DeleteTaskBatch taskBatch = new DeleteTaskBatch();
            taskBatch.daysToRetain = 30;
            Integer daysToRetain = taskBatch.daysToRetain;
            taskBatch.taskRecordType = 'MILESTONE';
            String taskRecordType = 'MILESTONE';  		
        
        	DateTime todaysDate = DateTime.now();
    		DateTime creationDate = todaysDate.addDays((daysToRetain - 15) * -1);	
        
        	List<Task> createTask = UnitTestDataFactory.createTask(taskRecordType, creationDate, true);
			
        	system.assertEquals(3, createTask.size());
 	
            test.startTest();
            	Database.executeBatch(taskBatch);
           	test.stopTest();

			Integer expectedResult = 3; 
        	List<Task> actualResult = [SELECT Id FROM Task WHERE Id in :createTask];
            
            System.assertEquals(expectedResult, actualResult.size(), 'There should be three Tasks');   
        }
    }
    
     /**
	 * Code coverage for DeleteTaskSchedular.apxc
	 */
    @istest static void PerformTaskDeletionSchedular(){
                
        User backOfficeUser = getBackOfficeUser();
        
        System.runAs(backOfficeUser) {
        	
        	string correctPermissionSet = 'Delete_Tasks';            
            PermissionSetAssignment assignCallLoggingPermissionSet = UnitTestDataFactory.assignPermissionSet(backOfficeUser.Id, correctPermissionSet);
        
            List<AsyncApexJob> jobsBefore = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob];
            System.assertEquals(0, jobsBefore.size(), 'not expecting any asyncjobs');
            
            Test.startTest();
                DeleteTaskScheduler taskScheduler = new DeleteTaskScheduler();
                String jobId = System.schedule('Task Deletion Scheduler', CRON_EXP, taskScheduler);
            Test.stopTest();
            
            List<AsyncApexJob> jobsScheduled = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'ScheduledApex'];
            System.assertEquals(1, jobsScheduled.size(), 'expecting one scheduled job');
            System.assertEquals('DeleteTaskScheduler', jobsScheduled[0].ApexClass.Name, 'expecting DeleteTaskScheduler job');
        }
    }
}