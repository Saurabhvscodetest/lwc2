/**
* Author:       Nawaz Ahmed
* Date:         09/03/2016
* Description:  Data object Class for web service
*
* ******************* Change Log *******************
* Modified by       Change Date     Change
* Nawaz Ahmed	     09/03/2016     Initial Version
*/
public class DataExtensionObject_DO { 

   @AuraEnabled public String communication       {get; set;} 
    @AuraEnabled public String sentto              {get; set;}
    @AuraEnabled public String status              {get; set;}
    @AuraEnabled public String datetimesent        {get; set;}
    @AuraEnabled public String statusdatetime      {get; set;}
    @AuraEnabled public String ordernumber         {get; set;}
    @AuraEnabled public String deliveryNumber      {get; set;}
    @AuraEnabled public string msgCcopy            {get; set;}
    @AuraEnabled public string preview             {get; set;}
    @AuraEnabled public string statusDescription   {get; set;}
    @AuraEnabled public string channelInfo         {get; set;}
    @AuraEnabled public string displayText         {get; set;}
    @AuraEnabled public boolean shouldStatusHighlighted   {get; set;}
    @AuraEnabled public boolean isEmailChannel   {get; set;}
    @AuraEnabled public boolean isPreviewToBeDisplayed   {get; set;}
    @AuraEnabled public string vampUrl {get; set;}
    @AuraEnabled public string dateText {get; set;}
    @AuraEnabled public string timeText {get; set;}
    @AuraEnabled public string statusDateText {get; set;}
    @AuraEnabled public string statusTimeText {get; set;}
    
    Map<String,String> valuesMap {get;set;}
    public static final String VALID_STATUS = 'CG VALID STATUS';
    public static final String INVALID_PREVIEW_STATUS = 'INVALID_PREVIEW_STATUS';
    public static final string SMS = Comms_Gateway_Constants__c.getinstance('SMS').Value__c;
    public static final string EMAIL = Comms_Gateway_Constants__c.getinstance('EMAIL').Value__c;
    public static final string UNKNOWN_CHANNEL = Comms_Gateway_Constants__c.getinstance('UNKNOWN_CHANNEL').Value__c;
    public static final string VOICEMESSAGE = Comms_Gateway_Constants__c.getinstance('VOICEMESSAGE').Value__c;
    public static final string DESCRIPTION_NOT_AVAILABLE = Comms_Gateway_Constants__c.getinstance('DESCRIPTION_NOT_AVAILABLE').Value__c;
    public static final string CAMPAIGN_NAME = Comms_Gateway_Constants__c.getinstance('CAMPAIGN_NAME').Value__c;
    public static final string CHANNEL = Comms_Gateway_Constants__c.getinstance('CHANNEL').Value__c;
    public static final string EMAIL_ADDRESS = Comms_Gateway_Constants__c.getinstance('EMAIL_ADDRESS').Value__c;
    public static final string PHONE_NUMBER = Comms_Gateway_Constants__c.getinstance('PHONE_NUMBER').Value__c;
    public static final string DELIVERY_STATUS = Comms_Gateway_Constants__c.getinstance('DELIVERY_STATUS').Value__c;
    public static final string SEND_DT = Comms_Gateway_Constants__c.getinstance('SEND_DT').Value__c;
    public static final string DELIVERY_STATUS_DT = Comms_Gateway_Constants__c.getinstance('DELIVERY_STATUS_DT').Value__c;
    public static final string ORDER_NUMBER = Comms_Gateway_Constants__c.getinstance('ORDER_NUMBER').Value__c;
    public static final string DELIVERY_NUMBER = Comms_Gateway_Constants__c.getinstance('DELIVERY_NUMBER').Value__c;
    public static final string VAMP_URL = Comms_Gateway_Constants__c.getinstance('VAMP_URL').Value__c;
    public static final string MESSAGE_COPY = Comms_Gateway_Constants__c.getinstance('MESSAGE_COPY').Value__c;
    public static final string VIEW = Comms_Gateway_Constants__c.getinstance('VIEW').Value__c;
    public static final string CLICK = Comms_Gateway_Constants__c.getinstance('CLICK').Value__c;
    public static final string validStatuses = Comms_Gateway_Constants__c.getInstance(VALID_STATUS).Value__c;
    
    
    /**
	* Constructor
	* @params:	
	* @rtnval:	void
	*/
    public DataExtensionObject_DO(Dom.XmlNode properties,String targetNameSpace) { 
        valuesMap = new Map<String,String>();
        for(Dom.XmlNode propertyNode : properties.getChildren())    {
            updateValuesMap(propertyNode,targetNameSpace);
        }  
        updateVariables(); 
    }
    
    /**
	* Method to populate the data from Values Map
	* @params:	
	* @rtnval:	void
	*/    
    private void updateVariables()  {
        communication = valuesMap.get(CAMPAIGN_NAME);
        channelInfo = String.isNotBlank(valuesMap.get(CHANNEL))? valuesMap.get(CHANNEL) : '';
        isEmailChannel = false;
        //Check for the channel type and populate the preview column in salesforce, 
        //if its email then user should be able to click the link to open in new browser
        if(channelInfo==EMAIL){
            sentto = valuesMap.get(EMAIL_ADDRESS);
            preview = CLICK; 
            isEmailChannel = true;
        }
        else if (channelInfo==SMS || channelInfo==VOICEMESSAGE)
        {
            sentto = valuesMap.get(PHONE_NUMBER); 
            if(sentto!=null && sentto!=''){
                String subStr = sentto.substring(0,2);
                sentto = sentto.removeStart('44');
                sentto = '0' +sentto;
            }
            preview = VIEW;
        } else {
            channelInfo = UNKNOWN_CHANNEL;
        }
        status              = String.isNotBlank(valuesMap.get(DELIVERY_STATUS))? valuesMap.get(DELIVERY_STATUS) : '';
        datetimesent        = valuesMap.get(SEND_DT);
        statusdatetime      = valuesMap.get(DELIVERY_STATUS_DT);
        ordernumber         = valuesMap.get(ORDER_NUMBER);
        deliveryNumber      = valuesMap.get(DELIVERY_NUMBER);
        vampURL             = valuesMap.get(VAMP_URL);
        msgCcopy            = valuesMap.get(MESSAGE_COPY);
        //Transform the date to Connex supported format using the modifyDateTime method
        datetimesent =  String.isNotBlank(datetimesent) ? modifyDateTime(datetimesent) : '';
        statusdatetime = (String.isNotBlank(statusdatetime)) ? modifyDateTime(statusdatetime): '';
        if(String.isNotBlank(datetimesent)){
            dateText = datetimesent.substringBefore(' ');
            timeText = datetimesent.substringAfter(' ');
            //datetimesent = dateText +' - ' + timeText;
        }
        if(String.isNotBlank(statusdatetime)){
            statusDateText = statusdatetime.substringBefore(' ');
            statusTimeText = statusdatetime.substringAfter(' ');
            statusdatetime = statusDateText+' - ' +statusTimeText;
        }
        
        /* Set the default values for the Status information */
        displayText =  status;
        isPreviewToBeDisplayed = false;
        shouldStatusHighlighted = true;
        
        //Get the related description from custom setting, it is displayed on hover field in visual force page
        String configKey = String.isNotBlank(channelInfo+status) ? (channelInfo+status).toUpperCase() : '';
        Exact_Target_Status_Description__c statusDescriptionConfig  = Exact_Target_Status_Description__c.getinstance(configKey);
        
        //If the description not availabe in custom setting use standard message
        if(statusDescriptionConfig!=null) {
            shouldStatusHighlighted = statusDescriptionConfig.HighlightStatus__c;
            isPreviewToBeDisplayed = statusDescriptionConfig.DisplayPreview__c;
            preview = (isPreviewToBeDisplayed) ? preview : '';
            statusDescription = statusDescriptionConfig.Description__c;
            displayText = statusDescriptionConfig.DisplayText__c;
        } else {
            statusDescription = DESCRIPTION_NOT_AVAILABLE;
        }
    }
    
    /**
	* Method to update Values map
	* @params:	
	* @rtnval:	void
	*/       
    private void updateValuesMap(Dom.XmlNode propertyNode,String targetNameSpace)   { 
        Dom.XmlNode nameNode  = propertyNode.getChildElement('Name',targetNameSpace); 
        Dom.XmlNode valueNode = propertyNode.getChildElement('Value',targetNameSpace); 
        valuesMap.put(nameNode.getText(),valueNode.getText());
    }

    /**
	* Method to change dateformat to different type, the format returning from exact target is invalid date timeformat in CONNEX
	* so unable to convert to datetime
	* @params:	String date time (2/4/2016 3:35:33 PM)
	* @rtnval:	String date time (04/02/2016 15:35)
	*/        
    private string modifyDateTime(string inputDateTime){
        String[]splitDateTime = inputDateTime.split(' ');
        splitDateTime[1] = splitDateTime[1].removestart(' ');
        String[]splitDatePart = splitDateTime[0].split('/');
        String[] splitTimePart = splitDateTime[1].split(':');
        String dayofDate = splitDatePart[1];
        
        //check length of date and if it is 1, add 0 
        if(splitDatePart[1].length() ==1){
            dayofDate =  '0'+ dayofDate;   
        }
        String monthofDate = splitDatePart[0];
       
       //check length of month and if it is 1, add 0 
        if(splitDatePart[0].length() ==1){
            monthofDate =  '0'+ monthofDate ;   
        }
        String yearofDate = splitDatePart[2] ;
        
        //Transform timeinto24 format
        String hourofdate = splitTimePart[0];
        if(splitDateTime[2] == 'PM'){
            integer hourValue = integer.valueOf(hourofdate) + 12;
            hourofdate = string.valueOf(hourValue);
            if(hourValue==24){
                hourofdate = '12';
            }
        }
        if(hourofdate.length() ==1){
            hourofdate = '0'+hourofdate  ; 
        }
        hourofdate = '    '+hourofdate;
        string minValue = splitTimePart[1];
        
        //Construct the full date string and return
        string changedDate = dayofDate+'/'+monthofDate+'/'+yearofDate;
       // changedDate.rightPad(13);
        string changedTime = hourofdate+':'+minValue;
        string changedDateTime=changedDate+  ' '  +changedTime;
        return changedDateTime;
    }
    

}