global class ScheduleBatchdeleteTransactionHeader implements Schedulable{
     global void execute(SchedulableContext sc){
        BAT_DeleteTransactionHeaders obj = new BAT_DeleteTransactionHeaders();
        Database.executebatch(obj);
    }
}