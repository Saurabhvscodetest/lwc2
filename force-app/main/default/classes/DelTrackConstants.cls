/*
* @File Name   : DelTrackConstants 
* @Description : Constant class to related to MyJLChatBot Delivery Tracking functionalities  
* @Copyright   : Zensar
* @Jira #      : #5174
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0       18-OCT-19          Vijay A                       Created
*/

Public class DelTrackConstants {

    
    Public Static Final String Current ;
    
    Public Static List<Case> relatedCaseList;
    Public Static Final String TWOMANGVF = 'TwoManGVF';
    Public Static Final String EXIST = 'Exist';
    Public Static String currentTimeCompareWithEightPM;
    
    Public Static String orderNumberStart = System.Label.DeliveryBotOrderNumberCheck;
    
    Public Static List<Case> getCaseListOrderNumber(String orderNumber){
        
        List<Case> cList = new List<Case>();
		cList = [Select id,CaseNumber from case where jl_OrderManagementNumber__c =: OrderNumber and Isclosed = false limit 1];
       
        system.debug('Checking the case list' + cList);
        return cList;
        
    }
    
     Public Static List<Case> getCaseListDeliveryNumber(String deliveryNumber){
        return [Select id,CaseNumber from case where jl_DeliveryNumber__c =: deliveryNumber and Isclosed = false limit 1];
    }
    
    Public Static String currentTimeCheckWithEightPM(){
        
        Datetime CurrentDateTime = System.now();
	    String CurrentTimeOnly = string.valueOf(CurrentDateTime);
		List<String> cList = CurrentTimeOnly.split(' ');
		String cTimeonly = cList[1];
		List<String> cListTimeOnly = cTimeonly.split(':');
		String actualCurrentTime = cListTimeOnly[0] + ':' + cListTimeOnly[1];               
        if(actualCurrentTime != NULL){
        if(actualCurrentTime > '20:00'){
        currentTimeCompareWithEightPM = 'After';
           }else {
                 currentTimeCompareWithEightPM = 'Before';
                       }
                   }
        
        return currentTimeCompareWithEightPM;
        
    }
    
    Public Static String twoHourARTimeCompare(String twoHourARTime){
        try{
        Datetime CurrentDateTime = System.now();
	    String CurrentTimeOnly = string.valueOf(CurrentDateTime);
		List<String> cList = CurrentTimeOnly.split(' ');
		String cTimeonly = cList[1];
		List<String> cListTimeOnly = cTimeonly.split(':');
		String actualCurrentTime = cListTimeOnly[0] + ':' + cListTimeOnly[1]; 
        system.debug('@@@ actualCurrentTime' + actualCurrentTime);
        system.debug('@@@ twoHourARTime' + twoHourARTime);
        if(actualCurrentTime > twoHourARTime){
            return 'After';
        }else{
            return 'Before';
        }
        }catch(exception e){
            system.debug('@@@ Exception' + e.getMessage());
            return 'After';
        }
        
        
    }
    
     Public Static Boolean orderNumberRegexCheckReSchedule(String orderNumber){

        Boolean result = False;
        
        if(orderNumber != null && orderNumber != ''){
          
            try{
                Boolean orderNumberStartCheck = orderNumber.startsWith(orderNumberStart); 
                
                if(orderNumberStartCheck != False){
                    
            		
        	string orderNumberExpression = '^[0-9]{0,9}$';
        	Pattern myPattern = Pattern.compile(orderNumberExpression);        
        	Matcher myMatcher = myPattern.matcher(orderNumber);        
        	result = myMatcher.matches();
       System.debug('@@@ orderNumberRegexCheckReSchedule' + result);
                return result;   
                }
                else{
                    
                    return false;
                }
            }
            catch (exception e){
                System.debug('@@@ Error while validating the OrderNumber with Regex');
                return false;
            }
                        
        }
        
        else{
            
             return false;
        }        
       
        
    } 
    
    
     Public Static Boolean orderNumberValidCheck(String orderNumber){

        Boolean result = False;
        
        if(orderNumber != null && orderNumber != ''){
          
            try{
        	string orderNumberExpression = '^[0-9]{0,9}$';
        	Pattern myPattern = Pattern.compile(orderNumberExpression);        
        	Matcher myMatcher = myPattern.matcher(orderNumber);        
        	result = myMatcher.matches();
                
                System.debug('@@@ orderNumberValidCheck' + result);
                return result;   
            }
            catch (exception e){
                System.debug('@@@ Error while validating the OrderNumber with Regex');
                return false;
            }
            
        }
        
        else{
            
             return false;
        }        
       
        
    } 
   

Public Static Boolean orderNumberValidOnlyCharCheck(String orderNumber){

        Boolean result = False;
        
        if(orderNumber != null && orderNumber != ''){
          
            try{
                String orderNumberWithNoSpace = orderNumber.remove(' ');
                
        	string orderNumberExpression = '^[a-zA-Z]{0,255}$';
        	Pattern myPattern = Pattern.compile(orderNumberExpression);        
        	Matcher myMatcher = myPattern.matcher(orderNumberWithNoSpace);        
        	result = myMatcher.matches();
                
                System.debug('@@@ orderNumberValidCheck' + result);
                return result;   
            }
            catch (exception e){
                System.debug('@@@ Error while validating the OrderNumber with Regex');
                return false;
            }
            
        }
        
        else{
            
             return false;
        }        
       
        
    } 
   

    
}