/**
 *  @author : Yousuf Mohammad 
 *  @Date   : 10-06-2016
 *  @Purpose: This Service class supports POST operation to support the creation of case, when there is an 
 *            exception while creating the Order on External systems.
 *
 */
@RestResource(urlMapping='/ExceptionService/*')
global class ExceptionService { 

    @TestVisible private static final String ERROR_INFO = 'Error :';
    @TestVisible private static final String ERROR_EMAIL_SUBJECT = 'STERLING EXCEPTION FAILURE';
    @TestVisible private static final Integer BAD_REQUEST_STATUS_CODE = 400;

    @HttpPost
    /*
	 * POST - this method handles the incoming request and returns success/failure of Case creation.
     *  If system fail to save the case then throws error with error code. 
	 *
	 * @param  ExceptionServiceVo : value object holding case creation information
     * @return List<ServiceResponse> : List of Responses.
     */
    global static List<ServiceResponse> doPost(List<ExceptionServiceVo> exceptionCasesList)	{ 
        RestResponse 		  restResponse 			= RestContext.response;
        List<ServiceResponse> responseList 			= new List<ServiceResponse>(); 
        
        ExceptionServiceHelper.validateExceptions(exceptionCasesList); 
        
        List<QueryExceptionType__mdt> queryExceptionConfigList = ExceptionServiceHelper.getQueryExceptionConfigList(exceptionCasesList);
        for(ExceptionServiceVo orderException : exceptionCasesList)	{
        	try {
        		if(!((orderException.serviceResult != null) && (orderException.serviceResult.hasError))) {//Does not have an error 
        		ExceptionServiceHelper.loadExceptionServiceVoFromCustomMetaData(orderException,queryExceptionConfigList);
        		}
        	}catch(Exception e) { 
        		ExceptionServiceHelper.addError(ERROR_INFO, (e.getMessage() + '-' +e.getStackTraceString()) , orderException);
        	}
        }
        
        ExceptionServiceHelper.createCasesForValidExceptions(exceptionCasesList);
        responseList = ExceptionServiceHelper.getServiceResponses(exceptionCasesList);
        for(ServiceResponse resp : responseList)	{
        	if(resp instanceOf ExceptionServiceFailResponse) { 
        		restResponse.statusCode = BAD_REQUEST_STATUS_CODE; 
        		break;
        	}
        
        }
        //If the result contains error
        system.debug('========'+ restResponse.statusCode );
        if(restResponse.statusCode == BAD_REQUEST_STATUS_CODE){
			String jsonRequest = (exceptionCasesList != null) ? JSON.serializePretty(exceptionCasesList) : '';
			String jsonResponse = (responseList != null) ? JSON.serializePretty(responseList) : '';
        	sendEmail(jsonRequest, jsonResponse);
        }
        return responseList;
        
    }    
        
	@Future
	public static void sendEmail(String jsonRequest, String jsonResponse){
		EmailNotifier.sendNotificationEmail(ERROR_EMAIL_SUBJECT , 'ENVIRONMENT - '+ UserInfo.getOrganizationId() + '  \n\r'
                        +' REQUEST - ' + '  \n\r'
                        + jsonRequest + '  \n\r ================================================== \n\r'
                        + ' RESPONSE -' + '  \n\r' + jsonResponse);	
	}        
}