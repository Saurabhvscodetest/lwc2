/* 
EventHub Get Events Interface - Holds the Signature of the methods that an EventHub Service 
Needs to implement 
*/
public interface EHGetEventsInterface{
	 /* Gets the Delivery Tracking Information for the provided OrderId or the Delivery ID */
	 DeliveryTrackingVO getDeliveryTrackingInfo(String orderId, String deliveryId);
	 /* Gets the Returns Tracking Information for the provided OrderId or the Delivery ID */
	 DeliveryTrackingVO getReturnsTrackingInfo(String orderId, String trackingId);
}