/**
 * Unit tests for PCDUpdateHandler class methods.
 */
@isTest
public with sharing class PSETransferHandler_TEST{
	
	private static final Id pseCaseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonStaticUtils.CASE_RT_NAME_PRODUCT_STOCK_ENQUIRY).getRecordTypeId();

	@testSetup static void setup() {
         UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
    }

	@IsTest
	static void testSinglePSECaseWithCaseComments(){

		Contact con = UnitTestDataFactory.createContact(new Account());
		con.Email = 'test@test.com';
        insert con;
        
        Case c = UnitTestDataFactory.createNormalCase(con);                                
        c.RecordTypeId = pseCaseRTId;
        c.Assign_To_New_Branch__c = 'Aberdeen';                  
        insert c;
		
		Case testCase = [select Id, OwnerId,RecordTypeId,Owner.Name, Assign_To_New_Branch__c, jl_Last_Queue_Name__c from Case where Id = : c.Id];				

		List<CaseComment> caseComments = new List<CaseComment>(); 
		for (Integer i = 0; i < 20; i++){

			CaseComment item = new CaseComment();
			item.CommentBody = 'Comment #' + i;
			item.ParentId = testCase.Id;
			caseComments.add(item);
		}

		insert caseComments;
		
		test.startTest();

		System.assertNotEquals(null,testCase.OwnerId);
        //System.assertEquals('Branch - CCLA ' + testCase.Assign_To_New_Branch__c + ' Q',testCase.Owner.Name);
        System.assertEquals(pseCaseRTId,testCase.RecordTypeId);
        
        testCase.Bypass_Standard_Assignment_Rules__c = true;
        testCase.Assign_To_New_Branch__c = 'Bluewater';
        testCase.jl_Transfer_case__c = true;
        testCase.jl_Case_RestrictedAddComment__c = 'test';
        testCase.Status = 'Closed - Assigned To Another Branch';
	    testCase.jl_Action_Taken__c = 'Case resolved';
	    
	    clsCaseTriggerHandler.resetCaseTriggerStaticVariables();
	    
	    update testCase;
	    
	    testCase = [select Id, OwnerId,Owner.Name,RecordTypeId, Assign_To_New_Branch__c, Status,jl_Last_Queue_Name__c from Case where Id = : c.Id];
	    
	    //check that the status of the original case has been updated.
	    System.assertNotEquals(null,testCase.OwnerId);
        System.assertEquals('Closed - Assigned to Another Branch',testCase.Status);
        
	    /*Case cloneCase = [select Id,RecordTypeId,Status,Owner.Name,Assign_To_New_Branch__c FROM Case where PreviousPSECase__c = : testCase.Id];
	    System.assertNotEquals(null,cloneCase);
	    System.assertEquals('New',cloneCase.Status);
	    System.assertEquals(pseCaseRTId,cloneCase.RecordTypeId);
	    //System.assertEquals('Branch - CCLA ' + cloneCase.Assign_To_New_Branch__c + ' Q',cloneCase.Owner.Name);
		
		List<CaseComment> ccResults = [select id from CaseComment where ParentId = : cloneCase.Id];
		System.assertNotEquals(null,ccResults);
		System.assertEquals(21,ccResults.Size());*/
	    
	    test.stopTest();
	}
	
	@IsTest
	static void testBulkPSECasesWithCaseComments(){	

        Integer batchSize = 5;
		Account testAccount = UnitTestDataFactory.createAccount(1, true);
		List<Case> testCases = new List<case>(); 
		
		for (Integer i = 1; i <= batchSize; i++){

			Contact con = UnitTestDataFactory.createContact(i, testAccount.id, false);
			Case c = UnitTestDataFactory.createNormalCase(con);                                
        	c.RecordTypeId = pseCaseRTId;
        	c.Assign_To_New_Branch__c = 'Aberdeen';
        	testCases.add(c);	
		}

		insert testCases;
		
		test.startTest();					

		List<CaseComment> caseComments = new List<CaseComment>(); 		
		
		for(Case testCaseItem : testCases){
			
			for (Integer i = 0; i < 20; i++){
				CaseComment item = new CaseComment();
				item.CommentBody = 'Comment #' + i;
				item.ParentId = testCaseItem.Id;
				caseComments.add(item);
			}		
		}				
		insert caseComments;

		System.assertEquals(100,caseComments.Size());
		
		for(Case testCase : testCases){

			testCase.Assign_To_New_Branch__c = 'Bluewater';
	       	testCase.Status = 'Closed - Assigned To Another Branch';
		   	testCase.jl_Action_Taken__c = 'Customer contacted';
		   	testCase.jl_Transfer_case__c = true;
        	testCase.jl_Case_RestrictedAddComment__c = 'test';				
		}		
	               	    
		clsCaseTriggerHandler.resetCaseTriggerStaticVariables();	    
		update testCases;

		List<Case> updatedCases = [SELECT Id, Assign_To_New_Branch__c, Status, jl_Action_Taken__c, Owner.Name FROM Case WHERE Status = 'Closed - Assigned To Another Branch'];

		System.assertEquals(5, updatedCases.size());
	    test.stopTest();		
	}
}