/* Description  : GDPR - Decouple Accounts from Cases
*
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   28/03/2019      Vijay Ambati                        Created             COPT-4432
*
*/
global class BAT_DecoupleAccountsFromCases implements Database.Batchable<sObject>,Database.Stateful{
    global String query;
    global Database.QueryLocator start(Database.BatchableContext BC){
        GDPR_Record_Limit__c gDPRLimit = GDPR_Record_Limit__c.getValues('BAT_DecoupleAccountsFromCases');
        if(gDPRLimit.Record_Limit__c != NULL){
            String recordLimit = gDPRLimit.Record_Limit__c;
            String limitSpace = ' ' + 'Limit' + ' '; 
            query = 'SELECT Id from Case where AccountId != NULL order by createddate asc' + limitSpace + recordLimit;
        }
        else{
            query = 'SELECT Id from Case where AccountId != NULL order by createddate asc';
        }
        
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, list<Case> scope){
        try{ 
            for(Case con:scope){
                con.AccountId = NULL;
                con.Skip_Batch_Validation__c = True;
            }
            list<Database.SaveResult> srList = Database.Update(scope);
            
        }
        Catch(exception e){
            EmailNotifier.sendNotificationEmail('Exception from BAT_DecoupleAccountsFromCases', e.getMessage());
        }
    }
    global void finish(Database.BatchableContext BC) {
        
    }
}