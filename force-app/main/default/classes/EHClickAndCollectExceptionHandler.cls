/******************************************************************************
* @author       Antony John
* @date         10/10/2015
* @description  ClickAndCollect Exception Handler.
*
* LOG     DATE        Author    JIRA                            COMMENT
* ===     ==========  ======    ========================        ============ 
* 001     10/10/2015  AJ        CMP-39, CMP-42                  Initial code
*
*/
public with sharing class EHClickAndCollectExceptionHandler extends EHBaseExceptionHandler{
    public static final String DEFAULT_DESCRIPTION = 'Event Hub Exception Raised - Please review delivery tracking and contact customer';
    
     public override EventHubVO.EHExceptionResponse handleEventHubExceptions(EventHubVO.EHExceptionRequest ehRequest){
     	Savepoint sp = Database.setSavepoint(); 
        EventHubVO.EHExceptionResponse ehResponse = null;
        try {
            ehResponse = checkInputRequestForError(ehRequest);
            //Check  if the ehResponse is null which confirms that there are no validation errors.
            if(ehResponse == null){
                Case c =  getCase(ehRequest);
                Task t =  createTaskForCase(ehRequest, c);
                ehResponse = getSucessResponse(t, ehRequest.ehExceptionId);
            }
        } catch(Exception exp){
            Database.rollBack(sp);
            String expDetail = 'Exception occured ' + exp.getMessage()  + '\n'+ exp.getStackTraceString();
            System.debug(expDetail);
            if(exp.getMessage().containsIgnoreCase(EHConstants.DUPLICATE_VALUE)){
                ehResponse = addTaskToExistingcase(ehRequest);
            } else {
                ehResponse = getErrorResponse(expDetail,ehRequest.ehExceptionId) ;
            }
        }
        return ehResponse;
     }
     
     public override EventHubVO.EHExceptionResponse addTaskToExistingcase(EventHubVO.EHExceptionRequest ehRequest){
        EventHubVO.EHExceptionResponse ehResponse = null;
        try {
            String taskKey = getUniqueKey(ehRequest); 
            Case c =  getExistingCase(taskKey);
            Task t = createTaskForCase(ehRequest, c);
            ehResponse = getSucessResponse(t, ehRequest.ehExceptionId);
        } catch(Exception exp){
            String expDetail = 'Exception occured ' + exp.getMessage()  + '\n'+ exp.getStackTraceString();
            System.debug(expDetail);
            ehResponse = getErrorResponse(expDetail,ehRequest.ehExceptionId) ;
        }
        return ehResponse;
     }
     
     /*
      - Checks if there is a existing case for the Exception ehRequest which is either "closed" status and created within the last 28 days
        or is the case is not closed.
      - If not creates a new case with the provided information from the EvenetHub.
      - If the existing closed case then reopen the same
     */ 
     public Case getCase(EventHubVO.EHExceptionRequest ehRequest){
        Case c =  null;
        String orderId = ehRequest.clickAndCollectInfo.orderId;
        Datetime ndate = DateTime.Now().AddDays(-1 * Integer.valueOf(EHConstants.CASE_CREATED_DAYS_CRITERIA));
        List<Case> caseList =  [Select Id,contactid, isClosed from Case where jl_OrderManagementNumber__c =:orderId and RecordTypeId in(:EHBaseExceptionHandler.queryCaseRecordTypeId,:EHBaseExceptionHandler.complaintCaseRecordTypeId ) and Origin =: EHConstants.EVENT_HUB and ((isClosed = true and createdDate > : ndate) or (isClosed = false)) LIMIT 1];
        system.debug(LoggingLevel.INFO, 'caseList Size' + (caseList.size()));
        if(caseList.isEmpty()) {
            //Populate the details of the new case
            c = populateCase(ehRequest.ehExceptionName, EHConstants.CLICK_AND_COLLECT, ehRequest);
            String receivedPickValueRevise = '';
            String esbBranchValue = ehRequest.enrichmentBranchName;
            if(esbBranchValue!=NULL) {
                Set<String> picklistValues = EHGVFExceptionHandler.getActivePicklistValuesForField(Case.jl_Branch_master__c.getDescribe());
                for(String pValues : picklistValues) {
                    if(esbBranchValue.containsIgnoreCase(pValues)) {
                        receivedPickValueRevise = pValues;
                    }
                }
            }
            c.Reference_Id__c = ehRequest.clickAndCollectInfo.packageNo;
            c.jl_OrderManagementNumber__c = ehRequest.clickAndCollectInfo.orderId;
            c.CDH_location__c = EHConstants.CARRIER_TEAM;
            c.Status = EHConstants.STATUS_NEW;
            c.EH_KEY__c = getUniqueKey(ehRequest);
            if(c.RecordTypeId==complaintCaseRecordTypeId) {
                c.Description = DEFAULT_DESCRIPTION;
                if(receivedPickValueRevise != NULL && receivedPickValueRevise != '') {
                    c.jl_Branch_master__c = receivedPickValueRevise;
                }
                else if(receivedPickValueRevise == NULL || receivedPickValueRevise == '') {
                    EH_Field_Mapping__c branchMapForNULL = EH_Field_Mapping__c.getValues('EHOnlineCCNULLMap');
                    c.jl_Branch_master__c = branchMapForNULL.Branch__c;
                }
                EH_Field_Mapping__c allFieldsComplaintType = EH_Field_Mapping__c.getValues('EHClickAndCollectComplaintMap');
                c.Contact_Reason__c = allFieldsComplaintType.Contact_Reason__c;
                c.Reason_Detail__c = allFieldsComplaintType.Reason_Detail__c;
                c.CDH_Site__c = allFieldsComplaintType.CDH__c;
            }
            try {
                insert c;
            }
            catch(Exception e) {
                c.jl_Branch_master__c = NULL;
                c.Contact_Reason__c = NULL;
                c.Reason_Detail__c =  NULL;
                c.CDH_Site__c =  NULL;
                insert c;
            }
        } else {
            c = caseList[0];
            if(c.isClosed){
                c.Status = EHConstants.STATUS_NEW;
                update c;
            }
        }
        return c;
    }

    /*
      - Creates a new Task and then assciates with the Case.
    */ 
    private Task createTaskForCase(EventHubVO.EHExceptionRequest ehRequest , Case caseObj){
        Task t = getTask(caseObj, ehRequest);
        t.RecordTypeId = eventHubCCTaskRecordTypeId;
        //Exception Information.
        t.EH_Order_Status__c = ehRequest.clickAndCollectInfo.orderStatus;
        t.EH_Order_Id__c = ehRequest.clickAndCollectInfo.orderId;
        t.Mode_Of_Transport__c = ehRequest.clickAndCollectInfo.modeOfTransport;
        t.subject = EHConstants.EVENT_HUB + EHConstants.SPLITTER + ehRequest.ehExceptionName;
        t.EH_Current_Location__c =  ehRequest.clickAndCollectInfo.currentLocation;
        t.EH_Expected_Location__c = ehRequest.clickAndCollectInfo.expectedLocation;
        t.EH_Further_Information__c = ehRequest.clickAndCollectInfo.furtherInformation;
        t.EH_Extra_Information__c = ehRequest.clickAndCollectInfo.extraInformation;
        insert t;
        return t;
    }
    
    /*
      - Validates the ehRequest to check if it contains all the ClickandCollect information 
       to create Case/Task.
    */  
    private  EventHubVO.EHExceptionResponse checkInputRequestForError(EventHubVO.EHExceptionRequest ehRequest){
        EventHubVO.EHExceptionResponse ehResponse = null;
        String errorMessage = null;
        if (EHConstants.CLICK_AND_COLLECT.equalsIgnoreCase(ehRequest.deliveryType) && (ehRequest.clickAndCollectInfo == null)){
            errorMessage = EHConstants.MANDATORY_PARAMETER_MISSING + EHConstants.SPLITTER + EHConstants.CC_INFO ; 
        } else if (EHConstants.CLICK_AND_COLLECT.equalsIgnoreCase(ehRequest.deliveryType) && (String.isBlank(ehRequest.clickAndCollectInfo.orderId))){
            errorMessage = EHConstants.MANDATORY_PARAMETER_MISSING + EHConstants.SPLITTER 
                        + EHConstants.CC_INFO + EHConstants.SPLITTER + EHConstants.ORDER_ID ;
        } else if (String.isBlank(ehRequest.clickAndCollectInfo.modeOfTransport)){
            errorMessage = EHConstants.MANDATORY_PARAMETER_MISSING + EHConstants.SPLITTER 
                    + EHConstants.CC_INFO + EHConstants.SPLITTER + EHConstants.SOURCE ;
        }
        return getErrorResponse(errorMessage, ehRequest.ehExceptionId);
    }
    
    /* get the uniqueKey */
    public static String getUniqueKey(EventHubVO.EHExceptionRequest ehRequest){
        return '' + ehRequest.clickAndCollectInfo.orderId + EHConstants.SPLITTER + DateTime.now().date() + EHConstants.SPLITTER + DateTime.now().hour(); 
    }
}