/**
 *	@author: Andrey Gavrikov (westbrook)
 *	@date: 2014-12-02 14:13:40 
 *	@description:
 *	    Test methods for LogCleanerBatch
 *	
 *	Version History :   
 *	2014-12-02 - MJL-1350 - AG
 *	initial version
 *	2020-01-06 -          - VC
 *  Added testRecordLimit() and testNoConfig()   
 *  
 */
@isTest
public class LogCleanerBatchTest  {
	static {
		//prepare configuration
		Database.insert(new House_Keeping_Config__c(Name = 'LogCleanerBatch', Class_Name__c = 'LogCleanerBatch', Order__c = 0, Run_Frequency_Days__c = 1, Active__c = true));
	}
	/**
	 * check that log headers are deleted when custom setting is set
	 */
	static testMethod void testOK () {
		CustomSettings.setTestLogHeaderConfig(true, -1);

		Database.insert(new Log_Header__c(Name = 'LogCleanerBatchTest', Service__c = 'LogCleanerBatchTest'));

		Test.startTest();
 		Database.executeBatch(new HouseKeeperBatch(new LogCleanerBatch(), true));
		Test.stopTest();

		System.assertEquals(0, [select count() from Log_Header__c where Name = 'LogCleanerBatchTest'], 
				'Did not expect to find log header with name "LogCleanerBatchTest" after running cleaning job');

		//check that Last_Run_Time__c was set
		final Map<String, House_Keeping_Config__c> configMap = House_Keeping_Config__c.getAll();
		final House_Keeping_Config__c config = configMap.get('LogCleanerBatch');
		System.assert(config.Last_Run_Time__c >= System.today(), 'Expected Last_Run_Time__c to be set');

	}

	/**
	 * check that log headers do not get deleted when custom setting is NOT set
	 */
	static testMethod void testRecordLimit () {
		CustomSettings.setTestLogHeaderConfig(true, -1); //	Sets Batch size to 1 and limt to 3

		// Insert 5 log header records
		for  (Integer i = 0; i < 5; i++){
			Database.insert(new Log_Header__c(Name = 'LogCleanerBatchTest', Service__c = 'LogCleanerBatchTest'));
		}
		

		Test.startTest();
 		Database.executeBatch(new HouseKeeperBatch(new LogCleanerBatch(), true));
		Test.stopTest();

		Integer recCount = [select count() from Log_Header__c where Name = 'LogCleanerBatchTest'];

		System.assertEquals(2,recCount , 
			'Expected to find 2 log header records with name "LogCleanerBatchTest" after running cleaning job. Found ' + recCount );
	}

	/**
	 * check that log headers do not get deleted when custom setting is NOT set
	 */
	static testMethod void testNoConfig () {
		Database.insert(new Log_Header__c(Name = 'LogCleanerBatchTest', Service__c = 'LogCleanerBatchTest'));

		Test.startTest();
 		Database.executeBatch(new HouseKeeperBatch(new LogCleanerBatch(), true));
		Test.stopTest();

		System.assertEquals(1, [select count() from Log_Header__c where Name = 'LogCleanerBatchTest'], 
				'Did not expect to find log header with name "LogCleanerBatchTest" after running cleaning job');

	}
	/**
	 * check if we can detect a job which is stuck
	 */
	static testMethod void testStuckJob () {
		CustomSettings.setTestLogHeaderConfig(true, -1);
		//Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
		Datetime NOW_TIME = System.now();
		//String sch = '0 0 0 * * ? ' + System.today().year();
		String sch = NOW_TIME.second() + ' ' + NOW_TIME.minute() + ' ' + NOW_TIME.hour() + ' * * ? ' + System.today().year();
		Test.startTest();
		String jobID = System.schedule('Test Job', sch, new HouseKeeperBatch(new LogCleanerBatch(), true));
		Test.stopTest();
		final Boolean isStale = BatchApexUtils.isJobAlreadyRunning('HouseKeeperBatch', -10);
		System.assert(isStale, 'Expected to report job as stuck');

	}

	//check that next processor is initiated when previous completes
	private static Boolean SecondBatch_WAS_CALLED = false;
	public class SecondBatch implements HouseKeeperBatch.Processor {
		public SecondBatch() {
			SecondBatch_WAS_CALLED = true;
		}
		//Dummy class as no record limit set
		public void setMaxReordsToProcess(Integer num){
		}
		//name of the process specified in House_Keeping_Config__c custom setting
		public String getName() {
			return 'SecondBatch';
		}
		
		public Database.QueryLocator  start(Database.BatchableContext bc) { 
			return Database.getQueryLocator([select Name from House_Keeping_Config__c where Name = 'DOES_NOT_EXIST' limit 1]);
		}
		public void execute(Database.BatchableContext bc, List<SObject> scope) {}
		public void finish(Database.BatchableContext bc) {}

		public Integer getBatchSize() {
			return 1;
		}
	}

	/**
	 * check that when first process completes the next one is initiated
	 */
	static testMethod void testNextProcessorCall () {
		CustomSettings.setTestLogHeaderConfig(true, -1);
		//configure SecondBatch to be run on every execution
		Database.insert(new House_Keeping_Config__c(Name = 'SecondBatch', Class_Name__c = 'LogCleanerBatchTest.SecondBatch', Order__c = 1, Run_Frequency_Days__c = -1, Active__c = true));
		
		Test.startTest();
 		Database.executeBatch(new HouseKeeperBatch(new LogCleanerBatch(), false));
		Test.stopTest();
		System.assertEquals(true, SecondBatch_WAS_CALLED, 'SecondBatch has not been initiated');
	}

}