/*
** PostChatController
**
** This class provides the controller used in the Live Agent Chat.
**
** The name is historical in that it used to support a Post Chat but now it supports the Custom Chat Window and the following 'survey' question.
**
**	Edit	Date		Author			Comment
**	001		10/09/14	NTJ				Original
**	002		11/09/14	NTJ				Reinstate use of LiveTranscript chatKey
**	003		25/09/14	NTJ				House-keeping - retire old code
*/
public with sharing class PostChatController {

// Public Properties
	
    public String question1 {get; private set;}
    public String answer1 {get; set;}
	public String freeFormText {get; set;}
	public String permissionText {get; private set;}
	public String explanationText {get; private set;}
	public String thankYouText {get; private set;}

    public Id surveyId {get; set;}
    public Boolean surveyCompleted {get; set;}

	public String errorText {get; set;}
	public Boolean showTranscript {get; set;}

	public Integer surveyPhase {get; private set;}
	public Boolean surveyAccepted {get; set;}
	public Boolean showThanks {get; private set;}

    public Boolean showSurvey {
        get {
        	// The custom setting governs whether we show surveys
            if (null == showSurvey) {
				if (surveyEnabled == true) {
	            	if (String.isEmpty(errorText)) {
						showSurvey = doMaths();				            		
	            	} else {
	            		showSurvey = false;
	            	}
		            // start temp code
					//showSurvey = true;
					// end temp code								
				} else {
					showSurvey = false;
				}
            }
            return showSurvey;
        }
        private set;
    }

   	public String guidChatKey {get; set;}
		
// Private Properties
	
	private String A4 {get; set;}	//= 'Strongly Agree';
	private String A3 {get; set;}	//= 'Agree';
	private String A2 {get; set;}	//= 'Disagree';
	private String A1 {get; set;}	//= 'Strongly Disagree';
	
	private String surveyRevision {get;set;}	
    private String transcriptText {get; set;}
    private Boolean surveyEnabled {get; set;}

// Private Members
    private LiveChatTranscript chatTranscript;
	private LiveChatVisitor chatVisitor;
    private String chatKey;
	// retire next line
	private String chatDetails;

    private String visitorId;
    // new style custom settings
	private Live_Agent_Control_Panel__c controlPanel;
	// retire chatSurvey
    //private Chat_Survey__c chatSurvey;
    private Chat_Survey_Question__c chatSurveyQuestion;
	
// Public Methods
	
    public List<SelectOption> getAnswers() {
        List<SelectOption> options = new List<SelectOption>(); 
        system.debug('A4: '+A4);
        options.add(new SelectOption(A1,A1)); 
        options.add(new SelectOption(A2,A2)); 
        options.add(new SelectOption(A3,A3)); 
        options.add(new SelectOption(A4,A4)); 
        return options; 
    }    
    
    // controller
    public PostChatController () { 
    	system.debug('controller');      
        chatKey =  ApexPages.currentPage().getParameters().get('chatKey');    
	// retire next line
		chatDetails = ApexPages.currentPage().getParameters().get('chatDetails');
	// retire next line
		errorText = ApexPages.currentPage().getParameters().get('error');
		transcriptText = ApexPages.currentPage().getParameters().get('transcript');
        system.debug('chatKey: '+chatKey);
        system.debug('chatDetails: '+chatDetails);
		system.debug('errorText: '+errorText);
		system.debug('transcriptText: '+transcriptText);

		getQA();
        //visitorId = getVisitorId(chatDetails);
		showTranscript = false;
		
		guidChatKey = '';
		surveyAccepted = false;
		showThanks = false;
		surveyPhase = 0;
    }
    
    // currently 40% 
    // No guarantee that it will do 40% or that they will be evenly spaced.
    // Assuming that the random number generator produces a good distribution between 0.0 and 1.0
    // We cannot count something like number of transcripts (which is a per-chat artifact) as we cannot guarantee when the
    // agent closes (and hence commits) the transcript or when the cron job closes unclosed transcripts.
    public static final Decimal SURVEY_LIMIT = 40;
 	@TestVisible private Boolean doMaths() {
		Boolean showFlag = false;
		if (controlPanel != null) {
			if (controlPanel.Live_Agent_Show_The_Survey__c != null) {
				Double d = math.random() * 100;
				system.debug('d: '+d+' limit: '+controlPanel.Live_Agent_Show_The_Survey__c);
				showFlag = (d < controlPanel.Live_Agent_Show_The_Survey__c); 						
			}			
		}
	    // get a number between 0 and 100
		return showFlag;
 	}
    
    public PageReference submitSurvey() {
    	system.debug('submitSurvey');
    	
    	Survey__c s = updateSurvey1(true);
    	updateTranscript(s, true);
    	
		return Page.CloseWindow;
    }

    public PageReference submitSurvey2() {
    	system.debug('submitSurvey2: guid: '+guidChatKey);
     	system.debug('submitSurvey2: surveyId: '+surveyId);
     	system.debug('submitSurvey2: surveyCompleted: '+surveyCompleted);
    	chatKey = guidChatKey;
    	
    	Survey__c s = updateSurvey1(surveyCompleted);
    	updateTranscript(s, true);
    	// old code
    	//PageReference pr = Page.CloseWindow;
    	//pr.setRedirect(true);
		//return pr;
		// end old code
		// new code
		surveyPhase = 2;
		surveyAccepted = false;
		showThanks = true;
		system.debug('surveyPhase: '+surveyPhase);
		system.debug('surveyAccepted: '+surveyAccepted);
		system.debug('showThanks: '+showThanks);
		return null;
    }

    public PageReference declineSurvey() {
    	system.debug('declineSurvey');
    	
    	Survey__c s = updateSurvey1(false);
    	updateTranscript(s, false);
    	
		return Page.CloseWindow;
    }
    
    public PageReference yesToSurvey() {
    	surveyPhase = 1;
    	surveyAccepted = true;
    	return null;
    }
    
	public PageReference donePostChat() {
    	PageReference pr = Page.CloseWindow;
    	pr.setRedirect(true);
		return pr;
	}
    
	public PageReference saveChat() {
		// get ready to redirect
		// set the parameter on new page by adding a parameter
		PageReference savedPr = new PageReference('/apex/PostChatSave?transcript='+transcriptText);
		
		return savedPr;
	}
    
    public PageReference backToChatWindow() {
    	PageReference pr = Page.ChatWindow;
    	return pr;
    }
    
// Remote Actions

    @RemoteAction
	public static String createSurveyForUrl(String url) {
		Survey__c s = new Survey__c();
		insert s;
		return url;
	}
	
	@RemoteAction
	public static String createSurveyWithGUID(String guid) {
		Survey__c s = new Survey__c();
		s.Chat_Key__c = guid;
		//s.Chat_GUID__c = guid;
		s.Status__c = 'Not Surveyed';
		s.Type__c = 'Live Agent';
 		s.Revision__c = getRevision();
		insert s;			
		return s.Id;
	}		

	@TestVisible private Survey__c updateSurvey1(Boolean completed) {
        Survey__c s = new Survey__c();
		s.Type__c = 'Live Agent';
        s.chat_question_1__c = question1;
        s.revision__c = surveyRevision;
        s.chat_Key__c = chatKey;
        //s.Chat_GUID__c = chatKey;
        s.chat_rating_1__c = answerString2Decimal(answer1);
        s.chat_answer_1__c = answer1;
        s.status__c = (completed) ? 'Survey Completed' : 'Survey Declined';
        system.debug('s: '+s);
        try {
    		if (surveyId != null) {
    			s.Id = surveyId;
    			update s;
    		} else {
        		insert s;        			
    		}
    		system.debug('new survey: '+s.Id);
        } catch (Exception ex) {
        	system.debug('Warning: cannot update survey: '+ex);
			s = null;
        }
        return s;		
	}
	
	@TestVisible private void updateTranscript(Survey__c s, Boolean completed) {
		if (s == null) {
			return;
		}
		system.debug('chatKey: '+chatKey);
        if (!String.isEmpty(chatKey)) {
            List<LiveChatTranscript> lctList = [SELECT Id FROM LiveChatTranscript WHERE chatKey = :chatKey ORDER BY CreatedDate LIMIT 1];
            if (lctList.size() == 1) {
                system.debug('lctList: '+lctList);
                chatTranscript = lctList[0];
            }
        }

		// if the transcript was created before the survey record then we need to update the transcript (as the before insert on Transcript is long gone)
		if (chatTranscript != null) {
			if (completed) {
				chatTranscript.Question_1__c = question1;
				chatTranscript.Answer_1__c = answer1;
				chatTranscript.Rating_1__c = answerString2Decimal(answer1);
				chatTranscript.Survey__c = s.Id;
			}
			chatTranscript.Survey_Revision__c = surveyRevision; 
			chatTranscript.Survey_Status__c = s.status__c;				
			
			// do the update in Apex System mode
			PostChatUpdateTranscript.processRecords(chatTranscript); 
		}
	}

    @TestVisible private Decimal answerString2Decimal(String opt) {
    	Decimal d = 0;
    	
    	if (opt == A4) {
    		d = 1;
    	} else if (opt == A3) {
    		d = 2;
    	} else if (opt == A2) {
    		d = 3;
    	} else if (opt == A1) {
    		d = 4;
    	}
    	return d;
    }
    
    @TestVisible private void getQA() {
		controlPanel = Live_Agent_Control_Panel__c.getInstance();	
		system.debug('controlPanel: '+controlPanel);
		if (controlPanel != null) {
			surveyEnabled = controlPanel.Live_Agent_Survey_Enabled__c;
		    surveyRevision = controlPanel.Live_Agent_Survey_Revision__c;
		    if (!String.isEmpty(surveyRevision)) {		    	
		        chatSurveyQuestion = Chat_Survey_Question__c.getValues(surveyRevision);
		        if (chatSurveyQuestion != null) {
		            system.debug(chatSurveyQuestion);
		            question1 = chatSurveyQuestion.question__c;
		            A1 = chatSurveyQuestion.answer_1__c;
		            A2 = chatSurveyQuestion.answer_2__c;
		            A3 = chatSurveyQuestion.answer_3__c;
		            A4 = chatSurveyQuestion.answer_4__c;
		            permissionText = chatSurveyQuestion.permission_text__c;
					explanationText = chatSurveyQuestion.explanation_text__c;
					thankYouText = chatSurveyQuestion.thanks__c;
		        }
		    }
		}
    }
    
    @TestVisible static private String getRevision() {
    	String revision = '';

		/* old style
		Chat_Survey__c cs = Chat_Survey__c.getValues('Current');
		if (cs != null) {
			revision = cs.Revision__c; 
		}
		*/
		Live_Agent_Control_Panel__c controlPanel = Live_Agent_Control_Panel__c.getInstance();	
		system.debug('controlPanel: '+controlPanel);
			
		if (controlPanel != null) {
			revision = controlPanel.Live_Agent_Survey_Revision__c;
		}
		    	
    	return revision;
    }
}