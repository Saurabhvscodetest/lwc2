/**
* @description	Controller for the Case Change Owner Button
* @author		@tomcarman		
*/

public with sharing class ChangeOwnerButtonController {
	
	public Boolean isCaseClosed {get; set;}
	public Boolean needToSetNewNad {get; set;}

	@TestVisible private Case caseRecord;
	@TestVisible private ApexPages.StandardController standardController;
	@TestVisible private Id recordId;	
	@TestVisible private PageReference changeOwnerPage;

	@TestVisible private static String CHANGE_OWNER_PERMISSION = 'Change_Owner_Access';
	
    /* COPT-5272 
    @TestVisible private static Set<String> CASE_RECORDTYPE_TO_BE_OWNED_BY_QUEUE = new Set<String>{'Email Triage', 'Web Triage', 'NKU'};
	COPT-5272 */
    @TestVisible private static Set<String> CASE_RECORDTYPE_TO_BE_OWNED_BY_QUEUE = new Set<String>{'NKU'};
	@TestVisible private static User currentUser;


	public ChangeOwnerButtonController(ApexPages.StandardController standardController) {
		this.standardController = standardController;
	}
 

 	/**
 	* @description	Page level validation			
 	* @author		@tomcarman
 	*/

	public PageReference validate() {

		recordId = standardController.getId();
		caseRecord = [SELECT Id, IsClosed, RecordType.Name, JL_Last_Queue_Name__c, EntitlementId, JL_Next_Response_By__c FROM Case WHERE Id = :recordId];
		
		changeOwnerPage = new PageReference('/apex/ChangeOwner?Id='+recordId+'&isdtp=vw');

		if(userHasPermission()) {

			if(!caseRecord.IsClosed && !nextActionDateInPast()) { 
				return changeOwnerPage;

			} else if (nextActionDateInPast()) {

				ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, 'You cannot transfer a Case with a Next Action date and time in the past'));
				needToSetNewNad = true;
				return null;

			} else {

				ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.WARNING, 'You are trying to change the owner of a closed case, would you like to reopen it?'));
				isCaseClosed = true;
				return null; 
			}

		} else {

			ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, 'You do not have the required permission to change a record owner.'));
			return null;

		}
	}


	/**
	* @description	Reopens case, returns either Change Owner page, or error		
	* @author		@tomcarman
	*/

	public PageReference reopenCase () {

		caseRecord = CaseReopenUtilities.reopenCase(caseRecord.Id);

		if(CaseReopenUtilities.errorMessages.isEmpty()) {
			update caseRecord;
			return changeOwnerPage;
		} else {
			for(String errorMessage : CaseReopenUtilities.errorMessages) {
				ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, errorMessage));
			}

			return null;
		}

	}



	/**
	* @description	Checks the the current User has permission to reopen a Case			
	* @author		@tomcarman
	*/

	private Boolean userHasPermission() {
		return PermissionSetLookup.checkPermissionSetForUserByName(UserInfo.getUserId(), CHANGE_OWNER_PERMISSION);
	}


	private Boolean nextActionDateInPast() {

		return  caseRecord.EntitlementId == null &&
				caseRecord.JL_Next_Response_By__c != null && 
				caseRecord.JL_Next_Response_By__c < Datetime.now();

	}

	

}