@isTest
public with sharing class EntitlementsTestDataFactory {
	
	public static final String GENERIC_ENTITLEMENT_PROCESS = 'Generic';

	/**
    * @description   Logic to setup Entitlement for a given Team, inc. associating it to an Entitlement record and Business Hours      
    * @author        @stuartbarber
    */
	public static Entitlement createEntitlements(String entitlementName){

		Account acc = createEntitlementAccounts(entitlementName);
		insert acc;

		BusinessHours busHrs = UnitTestDataFactory.getBusinessHours(entitlementName);

		SlaProcess entitlementProcess = [SELECT Id, Name FROM SlaProcess 
										 WHERE Name = :GENERIC_ENTITLEMENT_PROCESS 
										 AND IsActive = TRUE 
										 AND IsVersionDefault = TRUE LIMIT 1];
	

		Entitlement entitlement = new Entitlement();

		entitlement.SlaProcessId = entitlementProcess.Id;
		entitlement.Name = entitlementName;
		entitlement.AccountId = acc.Id;
		entitlement.BusinessHoursId = busHrs.Id;

		return entitlement;							 
	}

	/**
    * @description   Logic to setup Account to be associated to the Entitlement       
    * @author        @stuartbarber
    */
	public static Account createEntitlementAccounts(String entitlementName){

		return CustomerTestDataFactory.createAccount(entitlementName);
	}
	
}