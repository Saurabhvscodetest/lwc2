@isTest
public with sharing class CaseActivityTestDataFactory {
	
	public static Case_Activity__c createCustomerContactPromiseCaseActivity(Id caseId){
		Case_Activity__c newCaseActivity = new Case_Activity__c();

		newCaseActivity.Case__c = caseId;
		newCaseActivity.RecordTypeId = CaseActivityUtils.customerPromiseRTId;
		newCaseActivity.Activity_Description__c = 'test description';
		newCaseActivity.Activity_Date__c = Date.today().addDays(2);
		newCaseActivity.Activity_Start_Time__c = '10:00';
		newCaseActivity.Activity_End_Time__c = '12:00';
		newCaseActivity.Customer_Promise_Type__c = 'Customer Contact Promise';

		return newCaseActivity;
	}
	
	 public static Case_Activity__c createCustomerContactPromiseCaseActivityInsert(Id caseId){
        
        try{
          Case_Activity__c newCaseActivity = new Case_Activity__c();

		newCaseActivity.Case__c = caseId;
		newCaseActivity.RecordTypeId = CaseActivityUtils.customerPromiseRTId;
		newCaseActivity.Activity_Description__c = 'test description';
		newCaseActivity.Activity_Date__c = Date.today().addDays(2);
		newCaseActivity.Activity_Start_Time__c = '10:00';
		newCaseActivity.Activity_End_Time__c = '12:00';
        newCaseActivity.Activity_End_Date_Time__c = System.now();
		newCaseActivity.Customer_Promise_Type__c = 'Customer Contact Promise';

		Insert newCaseActivity;  
            
            
        return newCaseActivity;
            
        }catch(Exception e){
            
            System.debug('Error While Processing Insert');
            
            return NULL;
        }
		
	}
	
	public static Case_Activity__c createInternalActionCaseActivity(Id caseId){
		Case_Activity__c newCaseActivity = new Case_Activity__c();
		newCaseActivity.Case__c = caseId;
		newCaseActivity.RecordTypeId = CaseActivityUtils.internalActionRTId;
		newCaseActivity.Activity_Description__c = 'internal action case activity';
		newCaseActivity.Activity_Date__c = Date.today().addDays(2);
		newCaseActivity.Activity_Start_Time__c = '10:00';
		newCaseActivity.Activity_End_Time__c = '12:00';
		newCaseActivity.Activity_Type__c = 'Carrier';
		return newCaseActivity;
	}
}