public interface Serialisable {
	String serialise (final Object obj);
	Object deserialise (final String val);
}