/* Description  : Test Class For GDPR - Call Log Task Data Deletion
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   28/12/2018        ChandraMouli Maddukuri          Created                COPT-4276
*
*/

@isTest
public class GDPR_BatchDeleteCallLogsonTaskTest {
    
    @isTest
    Public static void testDeleteCallLogsonTask() {
        List<Task> taskList = New List<Task>();
        for(integer i=0; i<300; i++){
            taskList.add(new Task(Subject = 'Call log'+i, Priority = 'Normal', Status = 'New'));
        }
        try{
            insert taskList;
            system.assertEquals(300, taskList.size());
        }
        Catch(exception e){
            system.debug('Exception Caught:'+e.getmessage());
        }
        Database.BatchableContext BC;
        GDPR_BatchDeleteCallLogsonTask obj = NEW GDPR_BatchDeleteCallLogsonTask();
        Test.startTest();
        Database.DeleteResult[] Delete_Result = Database.delete(taskList, false);
        obj.start(BC);
        obj.execute(BC,taskList);
        obj.finish(BC);
        Test.stopTest(); 
    }
    
    @isTest
    Public static void testExceptionforDelete()
    {
       List<Task> taskList = New List<Task>();
        for(integer i=0; i<300; i++){
            taskList.add(new Task(Subject = 'Call log'+i, Priority = 'Normal', Status = 'New'));
        }
        
        insert taskList;
        system.assertEquals(300, taskList.size());
        Database.DeleteResult[] Delete_Result = Database.delete(taskList, true);
        DmlException expectedException;
        Test.startTest();
        try { 
            delete taskList;
        }
        catch (DmlException dException) {
            expectedException = dException; 
            system.assertNotEquals(NULL , expectedException); 
        }
        Test.stopTest();
    }
    
    @isTest
    Public static void testScheduledbatch(){
        Test.startTest();
        ScheduleBatchDeleteCallLogsonTask obj = NEW ScheduleBatchDeleteCallLogsonTask();
        obj.execute(null);  
        Test.stopTest(); 
    }
}