@RestResource(urlMapping='/MyLoyaltyService/Subscribe/*')

global class MyLoyaltyService {
	
	@HttpPut
	global static void joinShopper(String partnershipCardNumber, String origin, DateTime dateCreated) {

	 	string SERVICE_NAME_FOR_ERROR = 'My Partnership Card Rest Service';
		RestRequest req = RestContext.request;
		List<Loyalty_Card__c> loyaltyCardsToInsert =  new List<Loyalty_Card__c>();


		try{

			string shopperId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);

			if( String.isEmpty(partnershipCardNumber)){
				SetResponse('partnershipCardNumber: is not provided', 400);
				MyJL_Util.logRejection(MyJL_Const.ERRORCODE.MY_PARTNERSHIP_MISSING_DATA, '', '', SERVICE_NAME_FOR_ERROR);
			}

			else if( String.isEmpty(origin)){
				SetResponse('origin: is not provided', 400);
				MyJL_Util.logRejection(MyJL_Const.ERRORCODE.MY_PARTNERSHIP_MISSING_DATA, '', shopperId, SERVICE_NAME_FOR_ERROR);
			}

			else if( dateCreated == null){
				SetResponse('dateCreated: is not provided', 400);
				MyJL_Util.logRejection(MyJL_Const.ERRORCODE.MY_PARTNERSHIP_MISSING_DATA, '', shopperId, SERVICE_NAME_FOR_ERROR);
			}

			else if( String.isEmpty(shopperId)){
				SetResponse('shopperId: is not provided', 400);
				MyJL_Util.logRejection(MyJL_Const.ERRORCODE.CUSTOMERID_NOT_FOUND, '', shopperId, SERVICE_NAME_FOR_ERROR);
			}
			else{

				List<Loyalty_Account__c> accounts = MyLoyaltyServiceHelper.getLoyaltyAccountsWithShopperId(shopperId);

				if(accounts.isEmpty())
				{
					SetResponse('No Loyalty accounts found for shopperId', 404);	
					MyJL_Util.logRejection(MyJL_Const.ERRORCODE.LOYALTY_ACCOUNT_NOT_FOUND, '', shopperId,SERVICE_NAME_FOR_ERROR);
				}  
				else{

					for(Loyalty_Account__c a : accounts){
						if ( a.MyJL_Cards__r.size() > 0 ){
							SetResponse('Account is already a Partnership Account',404);	
							MyJL_Util.logRejection(MyJL_Const.ERRORCODE.CUSTOMER_ALREADY_JOINED, '', shopperId,SERVICE_NAME_FOR_ERROR);
							return;
						}
						else if(!a.IsActive__c){
							SetResponse('Account is not active',404);	
							MyJL_Util.logRejection(MyJL_Const.ERRORCODE.NO_ACTIVE_LOYALTY_ACCOUNT_FOUND, '', shopperId,SERVICE_NAME_FOR_ERROR);
							return;
						}
						else{
			     			Loyalty_Card__c card = new Loyalty_Card__c();
		        			card.Name = partnershipCardNumber;
		        			card.Card_Number__c = partnershipCardNumber;
		        			card.Loyalty_Account__c = a.Id;
		        			card.Card_Type__c = MyJL_Const.MY_PARTNERSHIP_CARD_TYPE;
		        			card.Date_Created__c = dateCreated;
		        			card.Origin__c = origin;
		        			card.Pack_Type__c =  MyJL_Const.PACK_TYPE_DIGITAL;
		        			loyaltyCardsToInsert.add(card);
		        		}
		        	}

		        	Database.insert(loyaltyCardsToInsert);
	
					RestResponse res = RestContext.response;
					res.statusCode = 200;
				}
			}
		}
		catch(Exception e){ //something else happened
			SetResponse(e.getMessage(),500);
			MyJL_Util.logRejection(MyJL_Const.ERRORCODE.UNHANDLED_EXCEPTION, '', '',SERVICE_NAME_FOR_ERROR);
		}
	}

	private static void SetResponse(String message, Integer statusCode){
		RestResponse res = RestContext.response;
		res.statusCode = statusCode;
		res.addHeader('Content-Type', 'application/json');
		res.responseBody = Blob.valueOf(JSON.serialize(message));	
	}
}