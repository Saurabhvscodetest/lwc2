global class CaseInteractionLogController {

	private ApexPages.StandardController standardController;
	private Case caseRecord;
	private Id currentUserId;

	public CaseInteractionLogController(ApexPages.StandardController standardController) {
		
		this.standardController = standardController;

	}

	public void logInteractionFromCasePageLoad() {

		Id caseId = standardController.getId();
		CaseInteractionLogger.logInteraction(Datetime.now(), CaseInteractionLogger.START_TIME, caseId, UserInfo.getUserId());
	}


	webService static Id logInteractionFromCloseTab(String caseId) { 
		
		CaseInteractionLogger.logInteraction(Datetime.now(), CaseInteractionLogger.END_TIME, caseId, UserInfo.getUserId());
		return (Id)caseId;
		
	}


}