/*
* @File Name   : SF_ConnexToTeleopti
* @Description : Rest API class to build the response   
* @Copyright   : Zensar
* @Jira #      : #5025
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0      01-Oct-19         Vijay Ambati                  Created
*/

@RestResource(urlMapping='/SF_ConnexToTeleoptiService/*')

Global class SF_ConnexToTeleopti {
    
    public static String findAgentWork() {
        String agQuery = 'SELECT Id,HandleTime,AcceptDateTime,WorkItem.casenumber,OriginalQueue.Name,Owner.Name FROM AgentWork';
        return agQuery;
    }
    
    @HttpGet
    global static SF_ConnexToTeleoptiWrapper fetchData() {
        
        String Teleopti_Limit = System.Label.Teleopti_Limit_Check;
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String from_TimeStamp;
        String to_TimeStamp;
        String findAgentWorks;
        String jsonBody;
        List<AgentWork> agList;
        SF_ConnexToTeleoptiWrapper teleoptiResponse = NEW SF_ConnexToTeleoptiWrapper();
        
        try {
            from_TimeStamp = req.params.get('fromTimeStamp');
            to_TimeStamp = req.params.get('toTimeStamp');
            
            if (!String.isEmpty(from_TimeStamp) && !String.isEmpty(to_TimeStamp)) {
                Long fromTimeStamp = Long.valueOf(from_TimeStamp);
                Long toTimeStamp = Long.valueOf(to_TimeStamp);
                Integer from_Time_Stamp = Integer.valueOf(fromTimeStamp/1000);
                Integer to_Time_Stamp = Integer.valueOf(toTimeStamp/1000);
                
                DateTime dateInstance = datetime.newInstanceGmt(1970, 1, 1, 0, 0, 0);
                String fromDateTimeValue = dateInstance.addSeconds(from_Time_Stamp).format('yyyy-MM-dd HH:mm:ss');
                String toDateTimeValue = dateInstance.addSeconds(to_Time_Stamp).format('yyyy-MM-dd HH:mm:ss');
                
                if(!String.isEmpty(fromDateTimeValue) && !String.isEmpty(toDateTimeValue)) {
                   
                    findAgentWorks = findAgentWork();
                    if(findAgentWorks!='' && findAgentWorks!=NULL) {
                        DateTime fromTs = DateTime.valueOf(String.escapeSingleQuotes(fromDateTimeValue));
                        DateTime fTs = fromTs.addHours(1);
                        DateTime toTs = DateTime.valueOf(String.escapeSingleQuotes(toDateTimeValue));
                        DateTime tTs = toTs.addHours(1);
                        findAgentWorks += ' WHERE AcceptDateTime >=: fTs';
                        findAgentWorks += ' AND AcceptDateTime <=: tTs';
                        findAgentWorks += ' ' + 'Limit' + ' ' + Teleopti_Limit; //Use Custom Label
                        agList = Database.query(findAgentWorks);
                    }
                }
                
                if(!agList.isEmpty()) {
                    res.statusCode = 200; 
                    teleoptiResponse.statusCode = String.valueOf(res.statusCode);
                    teleoptiResponse.errorCode = 'AGENT_WORK_DATA';
                    teleoptiResponse.agData = NEW List<AgentWork>();                    
                    teleoptiResponse.agData.addAll(agList);
                }
                else {
                    res.statusCode = 404;
                    teleoptiResponse.statusCode = String.valueOf(res.statusCode);
                    teleoptiResponse.errorCode = 'NO_DATA_FOUND';
					return teleoptiResponse;
                }
            }
            return teleoptiResponse;
        }
        catch(Exception e) {
            res.statusCode = 500;
			teleoptiResponse.statusCode = String.valueOf(res.statusCode);
			teleoptiResponse.errorCode = 'CONNEX_SERVER_ERROR';
			return teleoptiResponse;
           
        }
    }    
    
    global class SF_ConnexToTeleoptiWrapper { 
        
        global List<AgentWork> agData {get;set;}
        global String statusCode{get;set;}
        global String errorCode{get;set;}
    }
    
}