/* Description  : Delete LiveChat Visitor records which don't have Livechat Transcript records associated with it.
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   12/02/2019        Vignesh Kotha                   Created                COPT-4425  
*
*/
global class BAT_DeleteLiveChatVisitors implements Database.Batchable<sObject>,Database.Stateful {
    global Map<Id,String> ErrorMap = new Map<Id,String>();
    global integer result=0;
     global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Id,CreatedDate FROM LiveChatVisitor';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, list<LiveChatVisitor> scope){
      Map<id,LiveChatVisitor> visitorMap = new Map<id,LiveChatVisitor>();
      List<id> recordstoDelete = new List<id>();
         if(scope.size()>0){
            try{
                for(LiveChatVisitor recordScope : scope){
                   visitorMap.put(recordScope.id, recordScope);
                }
                for(LiveChatTranscript tansset : [select id,LiveChatVisitorId from LiveChatTranscript where LiveChatVisitorId In :visitorMap.keySet() and LiveChatVisitorId!=NULL]){
                    if(visitorMap.containsKey(tansset.LiveChatVisitorId)){
                        visitorMap.remove(tansset.LiveChatVisitorId);
                    }
                }
                for(ID data : visitorMap.keySet()){
                    recordstoDelete.add(data);
                }
                list<Database.deleteResult> srList = Database.delete(recordstoDelete, false);
                for(Integer counter = 0; counter < srList.size(); counter++){
                    if (srList[counter].isSuccess()) {
                        result++;
                    }else{
                        for(Database.Error err : srList[counter].getErrors()){
                            ErrorMap.put(srList.get(counter).id,err.getMessage());
                        }
                    }
                } 
                Database.emptyRecycleBin(scope);
            }Catch(exception e){
                EmailNotifier.sendNotificationEmail('Exception from BAT_DeleteLiveChatVisitors ', e.getMessage());
            }
        }
    }
    global void finish(Database.BatchableContext BC) {
        String textBody ='';
        set<Id> allIds = new set<Id>();
        textBody+= result +' records deleted in LiveChat Visitor object'+'\n';
        if (!ErrorMap.isEmpty()){
            for (Id recordids : ErrorMap.KeySet()){
                allIds.add(recordids);
            } 
            textBody+= 'Error log: '+allIds+'\n';
        }
        EmailNotifier.sendNotificationEmail('GDPR Delete LiveChat Visitor Records', textBody);
    }    
}