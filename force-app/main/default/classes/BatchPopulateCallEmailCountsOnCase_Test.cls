@isTest
private class BatchPopulateCallEmailCountsOnCase_Test
{
    @isTest
    static void validate_BatchPopulateCallEmailCountsOnCase()
    {
        UnitTestDataFactory.setRunValidationRules(false);
    
        
        Contact con = UnitTestDataFactory.createContact();
        con.Email = 'a@bb.com';
        insert con;
        
        Contact_Centre__c contactCenter= new Contact_Centre__c();
        contactCenter.Name='Default';
        contactCenter.Business_Hours__c='Default';
        contactCenter.SLA_Initial_Minutes__c=120;
        contactCenter.SLA_Initial_Minutes_Warning__c=90;
        insert contactCenter;

        Config_Settings__c configsetting= new Config_Settings__c();
        configsetting.Name='NEW_CASE_FROM_EMAIL_CLOSED_DAYS';
        configsetting.Description__c='When a new email arrives on a case closed this many days ago - create a new case instead';
        configsetting.Number_Value__c=0;
        insert configsetting;
        
        Config_Settings__c setting1 = new Config_Settings__c();
        setting1.Name = 'WS_Endpoint_ViewCustomerOrdersV1';
        setting1.Checkbox_Value__c = True;
        setting1.Text_Value__c = 'Test1';
        setting1.Description__c = 'Inserting customsettings1';
        setting1.Number_Value__c = 1;
        insert setting1;
        
        Config_Settings__c setting2 = new Config_Settings__c();
        setting2.Name = 'WS_Endpoint_EmailContent';
        setting2.Checkbox_Value__c = False;
        setting2.Text_Value__c = 'Test2';
        setting2.Description__c = 'Inserting customsettings2';
        setting2.Number_Value__c = 2;
        insert setting2;
        
        Config_Settings__c setting3 = new Config_Settings__c();
        setting3.Name = 'WS_Endpoint_ManageCustomerComments';
        setting3.Checkbox_Value__c = True;
        setting3.Text_Value__c = 'Test3';
        setting3.Description__c = 'Inserting customsettings3';
        setting3.Number_Value__c = 3;
        insert setting3;
        
        Config_Settings__c setting4 = new Config_Settings__c();
        setting4.Name = 'WS_Endpoint_SearchCustomer';
        setting4.Checkbox_Value__c = False;
        setting4.Text_Value__c = 'Test4';
        setting4.Description__c = 'Inserting customsettings4';
        setting4.Number_Value__c = 4;
        insert setting4;
        
        Config_Settings__c setting5 = new Config_Settings__c();
        setting5.Name = 'WS_Endpoint_ViewCustomerOrdersV2';
        setting5.Checkbox_Value__c = False;
        setting5.Text_Value__c = 'Test5';
        setting5.Description__c = 'Inserting customsettings5';
        setting5.Number_Value__c = 5;
        insert setting5;
        
		Id anyUserID = [select id from user where isActive=true limit 1].id;
		Config_Settings__c csets = new Config_Settings__c(name='EMAIL_TO_CASE_USER_ID',text_value__c = anyUserID);
		insert csets;

        Case c;
        List<Case> cases = new List<Case>();
        for(Integer i=0; i<8;i++){
            c = new Case();
            c = UnitTestDataFactory.createNormalCase(con);
            c.SuppliedEmail = 'a'+i+'@bb.co.uk';
            c.Origin = 'Email2Case';
            cases.add(c);
        }

        insert cases;

        List<EmailMessage> emails = new List<EmailMessage>();
        List<Task> tasks= new List<Task>();
        for(case cRec:cases){   
            tasks.add(new Task( Subject='Call log',WhatId=cRec.Id,WhoId=con.Id));
            emails.add(new EmailMessage(ParentId = cRec.Id,TextBody = 'Body of email', status = '1'));
        }

        insert emails; 
        insert tasks;

        Test.startTest();
        
        BatchPopulateCallEmailCountsOnCase bPCECOC = new BatchPopulateCallEmailCountsOnCase('Select id from case limit 8');
        ID batchprocessid = Database.executeBatch(bPCECOC);
        System.debug('Returned batch process ID: ' + batchProcessId);
        
        Test.stopTest();

        cases=[select id,Number_of_Call_Contacts__c ,Last_Call_Contact__c,Number_of_Email_Contacts__c,Last_Email_Contact__c from case where id IN: cases];
        for(Case caseRec: cases){
           // system.assertEquals(caseRec.Number_of_Email_Contacts__c,1);
           // system.assertEquals(caseRec.Number_of_Call_Contacts__c,1);

        }
    } 
}