// Edit     Date        Who         Comment
//  001     02/10/14    NTJ         Ensure that the comment body never exceeds 4000 bytes
//  002     09/10/14    NTJ         Set CaseComment limit to 3800. Added constant.
//  003     09/10/14    NTJ         Create a case comment on the parent if it was created on the child case
//  004     12/10/14    JA          Modify the truncation logic to calculate using byte size rather than string length
//  005     20/04/15    KG          Add update comments method to postcasecomments to OM - CMP147
//  006     08/11/16    LLJ         Renamed class
//  007     09/11/16    MP          Copy in code from clsUtils as it is only used by this class.

public without sharing class CaseCommentTriggerHandler {

	//002 Constant to ensure CaseComment and Task triggers agree.
	//004 Increase limit to 4000 bytes
 
	public static final Integer MAX_COMMENT_SIZE = 4000;
	
	//006 Copied constants from clsUtils
	private static final Integer TRUNCATE_FACTOR = 100;
	private static final Integer MINIMUM_ABBREVIATION_LENGTH = 5;
    private static final Double ORDER_NUMBER_MINIMUM_VALUE = 200000000;
	
	public static void mainEntry(Boolean isBefore, 
							   Boolean isDelete, Boolean isAfter, 
							   Boolean isInsert, Boolean isUpdate, 
							   Boolean isExecuting, List<CaseComment> newlist, 
							   Map<ID,Sobject> newmap, List<CaseComment> oldlist, 
							   Map<Id,Sobject> oldmap) {
    
	System.debug('@@entering clsCaseCommentTriggerHandler');

	if (isBefore && isInsert) {
		System.debug('isBefore && isInsert');

		// 003 - check for recursion and exit if true
		if (RecursionHandler.CaseCommentTriggerFired == true) {
			system.debug('Recursion - exiting function');
			return;
		} 
			
			String rolename = null;
			Id roleid = UserInfo.getUserRoleId(); 
		
			if (roleid != null) {
				List<UserRole> userRoleList = [SELECT Name FROM UserRole WHERE Id = :roleid];
				if (!userRoleList.isEmpty()) {
					roleName = userRoleList[0].Name;
				}   
			}

			for (CaseComment cc: newlist) {
				// test we got a comment body
				if (cc.commentbody != null) {

					// 001 - Flag where we probably truncated in caller in order to insert a truncated message in the CommentBody
					//       (Small possibility that the data fitted perfectly into MAX_COMMENT_SIZE bytes but unlikely)
					// 004 - Test using byte size not length and allow for a spread of up to 4 bytes (max byte size for a single character)
					Boolean commentTruncated = ((Blob.valueOf(cc.commentbody).size() >= (MAX_COMMENT_SIZE - 3)) && (Blob.valueOf(cc.commentbody).size() <= (MAX_COMMENT_SIZE)));
					
					system.debug('CommentBody byte size is: '+ Blob.valueOf(cc.commentbody).size());
					
					// 001 - Add the role name to the comment before truncation check
					if (!JLHelperMethods.valueIsNull(roleName)) {
                        
                        
                        cc.commentbody = rolename + '  //   ' +  cc.commentbody.removeEnd('null') ;                             
					 //cc.commentbody = rolename + '  //   ' +  cc.commentbody.removeEnd('null') +'Created';                             
             		  //cc.commentbody = rolename + '  //   ' + cc.commentbody; 
                        
						system.debug('Role added. CommentBody byte size now: '+ Blob.valueOf(cc.commentbody).size());
					}

					// 001/002 - now test again whether we broke the byte limit.
					// 004 - test using byte size not length
					if (Blob.valueOf(cc.commentbody).size() > MAX_COMMENT_SIZE) {
						commentTruncated = true;
					}                   
	
					// if we are just about to truncate the comment (at the bottom of the loop) then add the truncate message
					if (commentTruncated) {
						// 001: Do not truncate the commentbody, this happens right at the end, but we tell the user what we have done
						cc.commentbody = '** Truncated comment ** \r\n' + cc.commentbody;
						system.debug('Truncated message added. CommentBody byte size now: '+ Blob.valueOf(cc.commentbody).size());
					}                 
				}   

				// 001/002 - the very last line of code (bar the debug log) is ensure length does not exceed maximum.
				// 004 - test and abbreviate using byte size not length
				if (Blob.valueOf(cc.commentbody).size() > MAX_COMMENT_SIZE) {
					cc.commentbody = byteAbbreviate(cc.commentbody, MAX_COMMENT_SIZE);
				}
				
				System.debug('Final cc.commentbody length: ' + cc.commentbody.length());
				System.debug('Final cc.commentbody byte size: ' + Blob.valueOf(cc.commentbody).size());
			}       
		}

		if (isUpdate && isBefore) {
			System.debug('isUpdate && isBefore');
			
			for (CaseComment cc: newlist) {
				// 001/002 - Increase the limit to byte limit
				// 004 - test using byte size not length            
				if (cc.commentbody != null && Blob.valueOf(cc.commentbody).size() >= MAX_COMMENT_SIZE) {
					system.debug('CommentBody byte size is: '+ Blob.valueOf(cc.commentbody).size());
		  
					cc.commentbody = '** Truncated comment ** \r\n' + cc.commentbody;
					system.debug('Truncated message added. CommentBody byte size now: '+ Blob.valueOf(cc.commentbody).size());
				}
				// 001/002 - the very last line of code is ensure length does not exceed maximum.
				// 004 - test and abbreviate using byte size not length
				if (Blob.valueOf(cc.commentbody).size() > MAX_COMMENT_SIZE) {
					cc.commentbody = byteAbbreviate(cc.commentbody, MAX_COMMENT_SIZE);
				}
				
				System.debug('Final cc.commentbody length: ' + cc.commentbody.length());
				System.debug('Final cc.commentbody byte size: ' + Blob.valueOf(cc.commentbody).size());
			}
		}
		
		if (isDelete && isBefore) {
			System.debug('isUpdate && isBefore');
			
			for (CaseComment cc: oldList) {
				cc.addError('You cannot delete a Case Comment');
			}
		}
		
	   // 005     20/04/15    KG          Add update comments method to postcasecomments to OM - CMP147
		if (isInsert && isAfter) {
			Set<Id> caseIdSet = new Set<Id>();
			Set<Id> setCommentIds = new Set<Id>();
			Map<Id,String> orderMap = new Map<Id,String>();
			
			for(CaseComment ct : newlist) {
				setCommentIds.add(ct.Id);
				caseIdSet.add(ct.parentId);
			}

			//Update Comments
			Boolean isMinimunOrderNumberReached = false;
			for (Case c : [SELECT id, jl_OrderManagementNumber__c FROM Case WHERE Id IN :caseIdSet]) {
				if(!String.isBlank(c.jl_OrderManagementNumber__c) && Double.valueOf(c.jl_OrderManagementNumber__c) <= ORDER_NUMBER_MINIMUM_VALUE){
					orderMap.put(c.Id, c.jl_OrderManagementNumber__c);
				}else{
					isMinimunOrderNumberReached = true;
				}
			}

			String userRole = '';
			if (String.isNotBlank(UserInfo.getUserRoleId())) {
				userRole = [SELECT Id, Name FROM UserRole WHERE Id = : UserInfo.getUserRoleId()].Name; 
			}
			
			if(isMinimunOrderNumberReached == false){
				coSoapManager.postCaseCommentsOnUpdate(setCommentIds,orderMap,UserInfo.getFirstName() + ' ' + UserInfo.getLastName(),userRole);
			}
		}
		   
		if (isUpdate && isAfter) {
			System.debug('isInsert && isAfter');
			
			// 003 - check for recursion and exit if true
			if (RecursionHandler.CaseCommentTriggerFired == true) {
				return;
			} else {
				RecursionHandler.CaseCommentTriggerFired = true;
			}    
	  
			//Invoke Future Method passing newList
			Set<Id> caseIds = new Set<Id>();
			for(CaseComment cc : newlist) {
				caseIds.add(cc.ParentId);
			}
			
			// 003 - Add System Debug statement
			system.debug('caseIds: '+caseIds);
			
			// 003 - get a map of cases referred to by the case comments
			Map<Id,Case> caseMap = new Map<Id, Case>([SELECT Id, ParentId, CaseNumber FROM Case WHERE Id IN :caseIds]);
			
			// 003 - Comment out Vikram's Order Manager code
			//Map<Id,Case> caseMap = new Map<Id, Case>([select Id,jl_OrderManagementNumber__c, ParentId 
			//                                        from Case where Id in :caseIds and jl_OrderManagementNumber__c != null]);
			Set<Id> ccSet = new Set<Id>();
			// 003 - Comment out Vikram's Order Manager code
			//Map<Id,String> caseOrderMap = new Map<Id,String>();
			
			// 003 - Map stores caseId to its CaseComment
			Map<Id, CaseComment> case2CaseCommentMap = new Map<Id, CaseComment>();
			
			if (!caseMap.isEmpty()) {
				for(CaseComment cc : newlist) {
					if(caseMap.get(cc.ParentId) != null){
						ccSet.add(cc.Id);
						// 003 - Comment out Vikram's Order Manager code
						//caseOrderMap.put(cc.ParentId, caseMap.get(cc.ParentId).jl_OrderManagementNumber__c);
						
						// 003 - add the case comment's case and the case comment
						case2CaseCommentMap.put(cc.ParentId, cc);
					}
				}
				String userRole = '';
				if (!String.isBlank(UserInfo.getUserRoleId())) {
					userRole = [SELECT Id, Name FROM UserRole WHERE Id = :UserInfo.getUserRoleId()].Name;
				} 
				
				// 003 - Comment out Vikram's Order Manager code
				//CustomerOrderWSHandler.postCaseComments(ccSet, caseOrderMap, userRole, UserInfo.getFirstName() + ' ' + UserInfo.getLastName());
				
				// 003 - Copy the Case Comments onto the Parent Case
				createCaseCommentOnParent(caseMap, case2CaseCommentMap);
			}
		}
		}

	// 003 - Add method to create Case Comments onto the Parent Case (if one exists)
	private static void createCaseCommentOnParent(Map<Id,Case> caseMap, Map<Id, CaseComment> case2CaseCommentMap) {
		List<CaseComment> caseCommentOnParentCaseList = new List<CaseComment> ();
		for (Case c:caseMap.values()) {
			system.debug('Case: '+c);
			// if the case has a parent then we need to add a case comment to the parent
			if (c.ParentId != null) {
				CaseComment oldCC = case2CaseCommentMap.get(c.Id);
				system.debug('oldCC: '+oldCC);
				if (oldCC != null) {
					CaseComment newCC = new CaseComment();
					// 003 - Add a message to the new Case Comment to highlight that it was copied from a child case
					newCC.CommentBody = '** Comment copied from child ' + c.CaseNumber + ' ** \r\n' + oldCC.CommentBody;
					// 004 - If the new CommentBody is greater than MAX_COMMENT_SIZE bytes
					//       add a truncated message (if not already there) and abbreviate
					system.debug('Copied message added. CommentBody byte size now: '+ Blob.valueOf(newCC.CommentBody).size());
					if (Blob.valueOf(newCC.CommentBody).size() > MAX_COMMENT_SIZE) {
						if (oldCC.CommentBody.left(23) != '** Truncated Comment **') {
							newCC.CommentBody = '** Comment copied from child ' + c.CaseNumber + ' ** \r\n' + '** Truncated Comment ** \r\n' + oldCC.CommentBody;
							system.debug('Truncated message added. CommentBody byte size now: ' + Blob.valueOf(newCC.CommentBody).size());
						}    
						newCC.CommentBody = byteAbbreviate(newCC.CommentBody, MAX_COMMENT_SIZE);
					}
					newCC.ParentId = c.ParentId;
					system.debug('newCC: '+newCC);
					caseCommentOnParentCaseList.add(newCC);                 
				}
			}
		}
		
		try {
			insert caseCommentOnParentCaseList;
		} catch (Exception e) {
			system.debug('Problem creating case comment: '+e);
		}
	}

	//006 Method copied from clsUtil
	@testVisible
	private static String byteAbbreviate(String stringToAbbreviate, Integer byteLength) {
		if ((stringToAbbreviate != null) && (byteLength != null) && (byteLength > MINIMUM_ABBREVIATION_LENGTH)) {
			system.debug('stringToAbbreviate.length: '+stringToAbbreviate.length());
			system.debug('desired byteLength: '+byteLength);

			//First abbreviate the character length of the string to the desired bytesize number. This will get us close
			//002 - Check the current length of the string - if it is already less than the bytelength don't bother 
			//      with the initial abbreviation.
			Integer abbLength;
			if (stringToAbbreviate.length() > byteLength) {
				// Set the initial abbLength value to the desired byte size
				abbLength = byteLength;
				stringToAbbreviate = stringToAbbreviate.abbreviate(abbLength);
			} else {
				// Set the initial abbLength value to the current string length
				abbLength = stringToAbbreviate.length(); 
			}

			//Use the Blob.size function to calculate the byte size of the string
			Integer blobsize = Blob.valueOf(stringToAbbreviate).size();
			system.debug('blobsize: '+blobsize);

			//If the byte size of the string is greater than the desired byte size, loop round reducing the character length 
			//of the string by the TRUNCATE_FACTOR each time until the byte size is less than the desired byte size
			while (blobsize > byteLength) {
				abbLength = abbLength - TRUNCATE_FACTOR;
				stringToAbbreviate = stringToAbbreviate.abbreviate(abbLength);
				blobsize = Blob.valueOf(stringToAbbreviate).size();
				system.debug('new: length: '+stringToAbbreviate.length());
				system.debug('new: blobsize: '+blobsize);
			}
		}
		return stringToAbbreviate;
	}
}