global class DeliveryTrackingVOUserInputMock  implements EHGetEventsInterface{
    public static final string INPUT_CONNECTION_ERROR = 'a';
    public static final Integer INVALID_HTTP_CODE = 524;
    public static final string INPUT_INVALID_DEATILS = 'b';
    public static final string STATIC_RESOURCE_NAME = 'DeliveryTrackingMockData';
    public static final string JSON_FILE_NAME = 'EventHub.json';
    
	
	
	public virtual  DeliveryTrackingVO getReturnsTrackingInfo(String orderId, String trackingId){
		return getDeliveryTrackingVO(orderId, trackingId);
	}
	
	public virtual  DeliveryTrackingVO getDeliveryTrackingInfo(String orderId, String deliveryId){
		DeliveryTrackingVO delVo= getDeliveryTrackingVO(orderId, deliveryId);
		//populate the given delivery Id in the repsonse 
		if(String.isNotBlank(deliveryId) && (delVo != null) && (delVo.gvfDelInfList != null)&& (!delVo.gvfDelInfList.isEmpty())){
			delVo.gvfDelInfList[0].deliveryId  = deliveryId;
		}
		delVo.caseList = EHGetEventsService.getRelatedCases(orderId, deliveryId);
		return delVo;
	}

	public static DeliveryTrackingVO getDeliveryTrackingVO(String orderId, String anotherId){
		String requestInput = '';
        DeliveryTrackingVO delVo = null;
		if(String.isNotBlank(orderId)){
			requestInput = orderId;
		} else if(String.isNotBlank(anotherId)){
			requestInput = anotherId;
		}
		try{
            if(requestInput.endsWithIgnoreCase(INPUT_CONNECTION_ERROR)){
            	delVo = EHGetEventsService.getErrorInfo(EHConstants.ERROR_TYPE_TECHNICAL);	
            } else if(requestInput.endsWithIgnoreCase(INPUT_INVALID_DEATILS)){
            	delVo = EHGetEventsService.getErrorInfo(EHConstants.ERROR_TYPE_BUSINESS);	
            }  else {
             	delVo =	(DeliveryTrackingVO)JSON.deserialize(getJsonResponse() , DeliveryTrackingVO.class);
            }
        } catch (Exception exp) {
            delVo = EHGetEventsService.handleException(null, EHConstants.ERROR_MESSAGE_UNEXPECTED +'\n '+ exp.getMessage()+ EHConstants.SPLITTER + exp.getStackTraceString());
        }   
        delVo.caseList = new List<Case>();
        delVo.orderId = orderId; 
        return delVo;
	}
	
	
	
	
	public static String getJsonResponse(){
		StaticResource sr = [SELECT Id, Body FROM StaticResource WHERE Name = :STATIC_RESOURCE_NAME LIMIT 1];
		if(sr!= null && sr.Body!= null){
			return sr.Body.toString();
		}
		return '';
	}
    
}