@isTest
private class EntitlementManagerControllerTest {

    @isTest
    static void buildAvailableTeams_shouldListAllteams() {
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
        // Function under test
        EntitlementManagerController.buildAvailableTeams();
        
        System.assert(!EntitlementManagerController.originalvalues.isEmpty(), 'Team Should not be empty');        
        System.assert(EntitlementManagerController.originalvalues.size() >  0, 'Should contain the set up team');        
        
    }

    @isTest
    static void buildAvailableTeams_shouldNOtListExistingEntitlementTeams_WhenConfigIsNotEnabled() {
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
        Account acc = UnitTestDataFactory.createAccount(randomNo, true);
        Entitlement ent = new Entitlement(Name = TEAM_NAME, AccountId = acc.Id);
        insert ent;
        // Function under test
        EntitlementManagerController.buildAvailableTeams();
        
        System.assert(EntitlementManagerController.originalvalues.isEmpty(), 'Team Should not be empty');        
        
    }

    @isTest
    static void buildAvailableTeams_shouldListExistingEntitlementTeams_WhenConfigIsEnabled() {
        Config_Settings__c showEntitlement = new Config_Settings__c(name= EntitlementManagerController.SHOW_EXISTING_ENTITLEMENT_TEAMS,Checkbox_Value__c = true);
        insert showEntitlement;
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
        Account acc = UnitTestDataFactory.createAccount(randomNo, true);
        Entitlement ent = new Entitlement(Name = TEAM_NAME, AccountId = acc.Id);
        insert ent;
        // Function under test
        EntitlementManagerController.buildAvailableTeams();
        
        System.assert(!EntitlementManagerController.originalvalues.isEmpty(), 'Team Should not be empty');        
        System.assert(EntitlementManagerController.originalvalues.size() >  0, 'Should contain the set up team');        
        
    }
    
    
    @isTest
    static void selectclick() {
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
        EntitlementManagerController entConfigController = new EntitlementManagerController();
        entConfigController.leftselected = new   List<string>{TEAM_NAME};
        
        // Function under test
        entConfigController.selectclick();
        
        System.assert(!entConfigController.leftvalues.contains(TEAM_NAME), 'Should not contain the team in the Available options');        
        System.assert(entConfigController.selectedTeamNames.contains(TEAM_NAME), 'Should contain the team in the Selected options');        
        
    }
    
    @isTest
    static void unselectclick() {
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
        EntitlementManagerController entConfigController = new EntitlementManagerController();
        entConfigController.rightselected = new   List<string>{TEAM_NAME};
        
        // Function under test
        entConfigController.unselectclick();
        
        System.assert(entConfigController.leftvalues.contains(TEAM_NAME), 'Should contain the team in the Available options');        
        System.assert(!entConfigController.selectedTeamNames.contains(TEAM_NAME), 'Should NOT contain the team in the Selected options');        
        
    }
    
    
    @isTest
    static void getunSelectedValues() {
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
        EntitlementManagerController entConfigController = new EntitlementManagerController();
        
        // Function under test
        List<SelectOption> unselectedTeamsList = entConfigController.getunSelectedValues();
        
        System.assert(unselectedTeamsList.size() > 0, 'Should contain the team in the Available options');        
        
    }
    
    @isTest
    static void getSelectedValues() {
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
        EntitlementManagerController entConfigController = new EntitlementManagerController();
        entConfigController.selectedTeamNames =  new   Set<string>{TEAM_NAME};
        
        // Function under test
        List<SelectOption> selectedTeamsList = entConfigController.getSelectedValues();
        
        System.assert(selectedTeamsList.size() > 0, 'Should contain the team in the Selected options');        
        
    }
    
    
    @isTest
    static void resetData() {
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
        EntitlementManagerController entConfigController = new EntitlementManagerController();
        
        // Function under test
        entConfigController.resetData();
        
        System.assert(entConfigController.leftselected.isEmpty(), 'leftselected should be empty');        
        System.assert(entConfigController.rightselected.isEmpty(), 'rightselected should be empty');        
        System.assert(entConfigController.selectedTeamNames.isEmpty(), 'selectedTeamNames should be empty');        
        System.assert(entConfigController.leftvalues.contains(TEAM_NAME), 'Should contain the set up team');        
        
    }
    
    
    @isTest
    static void assignEntitlementsAndCreateRealtedQueues_ShouldDisplayWarningMessageWhenNOteamsAreSelected() {
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
        EntitlementManagerController entConfigController = new EntitlementManagerController();
        
        // Function under test
        entConfigController.assignEntitlementsAndCreateRealtedQueues();
        
        System.assert(ApexPages.hasMessages(), 'Should have Apex messages');        
        System.assertEquals(ApexPages.getMessages()[0].getSummary(), EntitlementManagerController.ATLEAST_SELECT_ONE_TEAM, 'Apex message must match');        
        
    }

    

    @isTest
    static void assignEntitlementsAndCreateRealtedQueues_ShouldDisplayASucessMessage() {
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
        UnitTestDataFactory.createConfigSettings();

        EntitlementManagerController entConfigController = new EntitlementManagerController();
        entConfigController.selectedTeamNames =  new   Set<string>{TEAM_NAME};
        EntitlementManagerController.createRealtedQueues =  false;
        
        // Function under test
        entConfigController.assignEntitlementsAndCreateRealtedQueues();
        
        System.assert(ApexPages.hasMessages(), 'Should have Apex messages');        
        System.assertEquals(ApexPages.getMessages()[0].getSummary(), EntitlementManagerController.SUCCESS_MESSAGE, 'Apex message must match');   
    }


    @isTest
    static void updateExistingEntitlementRecords() {
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
        Account acc = UnitTestDataFactory.createAccount(randomNo, true);
        
        Entitlement ent = new Entitlement(Name = TEAM_NAME, AccountId = acc.Id);
        insert ent;
        Entitlement entitlementObj =  [SELECT Id, Name, SlaProcessId FROM Entitlement WHERE Id =:ent.Id LIMIT 1]; 
        System.assert(entitlementObj.SlaProcessId == null, 'Entitlement process must NOT be associated');
        EntitlementManagerController entConfigController = new EntitlementManagerController();
        
        // Function under test
        entConfigController.updateExistingEntitlementRecords();
        //since a single entitlement is present
        entitlementObj =  [SELECT Id, Name, SlaProcessId FROM Entitlement WHERE Id =:ent.Id LIMIT 1]; 
        System.assert(entitlementObj.SlaProcessId != null, 'Entitlement process must be associated');
    }
    
}