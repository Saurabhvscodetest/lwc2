public class EntitlementConfigScript {
        
    List<EntitlementConfig> entitlementConfigs;
    Map<String, Id> queueNameToIdMap;
    Map<String, Id> entitlementProcesstNameToIdMap;
    Map<String, Id> accountNameToIdMap;
    Map<String, Id> entitlementRecordNameToIdMap;

    public void clearUnusedEntitlements(){
        Map<Id, Entitlement> mapEntitlement = new Map<Id, Entitlement>([Select Id, (select id from cases), name from Entitlement]);
        List<Id> entIds = new List<Id>();
        for(Entitlement ent: mapEntitlement.values()){
           //collate the entitlements that are not associated with the cases.
            if(ent.cases.isempty()){
                system.debug( ent.Name + '=====' + ent.cases);
                entIds.add(ent.Id);
            }
        }
        //delete the unwanted entitlements that is not associated with the cases
        Database.delete(entIds, false);
    }
    
    public void getData() {

        entitlementConfigs = new List<EntitlementConfig>();

        entitlementConfigs.add(new EntitlementConfig('Hamilton - CRD', 'Hamilton - CRD', 'CRD_Work_Allocation_Hamilton', true));
        entitlementConfigs.add(new EntitlementConfig('Hamilton/Didsbury - CST', 'Hamilton/Didsbury - CST', 'CST_Hamilton_Didsbury', true));
        entitlementConfigs.add(new EntitlementConfig('Branch - CCLA Peter Jones', 'Branch PETER JONES', 'Branch_PETER_JONES', true));
        entitlementConfigs.add(new EntitlementConfig('Branch - CST Peter Jones', 'Branch PETER JONES', 'Branch_PETER_JONES', true));
        entitlementConfigs.add(new EntitlementConfig('Customer Delivery Resolution', 'Customer Delivery Resolution', 'Customer_Delivery_Resolution', false));

        queueNameToIdMap = getQueues();
        entitlementProcesstNameToIdMap = getEntitlementProcesses();

    }



    public void build() {

        // Build Accounts
        accountNameToIdMap = buildEntitlementAccounts();

        // Build Entitlement Records
        entitlementRecordNameToIdMap = buildEntitlementRecords(); 

        // Build Custom Setting
        buildTeamCustomSetting();

    } 



    private void buildTeamCustomSetting() {

        List<Team__c> teamCustomSettingsToUpsert = new List<Team__c>();

        Set<String> teamNames = new Set<String>();
        
        for(EntitlementConfig config : entitlementConfigs) {
            teamNames.add(config.Name);
        }

        Map<String, Id> teamNameToRecordId = new Map<String, Id>();
        
        for(Team__c team : [SELECT Id, Name FROM Team__c]) {
            teamNameToRecordId.put(team.Name, team.Id);
        }

        for(EntitlementConfig config : entitlementConfigs) {

            Team__c teamCustomSetting = new Team__c();

            if(teamNameToRecordId.containsKey(config.Name)) {
                teamCustomSetting.Id = teamNameToRecordId.get(config.Name);
            }
            teamCustomSetting.Name = config.Name;
            teamCustomSetting.Contact_Centre__c = config.contactCentreName;
            teamCustomSetting.Queue_Name__c = config.queueName;
            
            String baseQueueName = config.queueName.replace('_WARNING','').replace('_FAILED','');

            //teamCustomSetting.Queue_Id__c = queueNameToIdMap.get(baseQueueName);
            

            teamCustomSetting.Task_Assignee__c = 'to.be.assigned@johnlewis.com';

            teamCustomSettingsToUpsert.add(teamCustomSetting);
        }

        upsert teamCustomSettingsToUpsert;

    }



    private Map<String, Id> buildEntitlementRecords() {

        Map<String, Id> entitlementRecordNameToIdMap = new Map<String, Id>();

        Set<String> entitlementNames = new Set<String>();

        for(EntitlementConfig config : entitlementConfigs) {
            entitlementNames.add(config.name);
        }

        List<Entitlement> existingEntitlementRecords = [SELECT Id, Name FROM Entitlement WHERE Name IN :entitlementNames];

        Map<String, Entitlement> entitlementNameToEntitlementMap = new Map<String, Entitlement>();

        for(Entitlement entitlement : existingEntitlementRecords) {
            entitlementNameToEntitlementMap.put(entitlement.Name, entitlement);
        }


        Set<Entitlement> entitlementRecordToUpsert = new Set<Entitlement>();


        for(EntitlementConfig config : entitlementConfigs) {

            if(!config.Name.endsWithIgnoreCase('WARNING') && !config.Name.endsWithIgnoreCase('FAILED')) {
                
                Entitlement entitlement;
               
                if(entitlementNameToEntitlementMap.containsKey(config.Name)) {
                    entitlement = entitlementNameToEntitlementMap.get(config.Name); 
                } else {
                    entitlement = new Entitlement();
                    entitlement.Name = config.Name;
                    entitlement.Type = 'Phone Support';
                    entitlement.AccountId = accountNameToIdMap.get(config.name);
                    entitlement.StartDate = Date.today();
                }

                entitlement.SlaProcessId = entitlementProcesstNameToIdMap.get(config.Name);
                entitlementRecordToUpsert.add(entitlement);
            }
        }

        upsert new List<Entitlement>(entitlementRecordToUpsert);

        for(Entitlement entitlement : entitlementRecordToUpsert) {
            entitlementRecordNameToIdMap.put(entitlement.Name, entitlement.Id);
        }

        return entitlementRecordNameToIdMap;


    }


    private Map<String, Id> buildEntitlementAccounts() {

        Map<String, Id> nameToAccountIdMap = new Map<String, Id>();

        Set<String> accountNames = new Set<String>();

        for(EntitlementConfig config : entitlementConfigs) {
            accountNames.add(config.name);
        }


        // Get existing Accounts
        for(Account account : [SELECT Id, Name FROM Account WHERE Name IN :accountNames]) {
            nameToAccountIdMap.put(account.Name, account.Id);
        }

        // Build others
        List<Account> accountsToInsert = new List<Account>();

        accountNames.removeAll(nameToAccountIdMap.keySet());

        for(String remainingAccountName : accountNames) {
            Account account = new Account();
            account.Name = remainingAccountName;
            accountsToInsert.add(account);
        }

        insert accountsToInsert;

        for(Account account : accountsToInsert) {
            nameToAccountIdMap.put(account.Name, account.Id);
        }


        return nameToAccountIdMap;

    }



    private Map<String, Id> getEntitlementProcesses() {

        Set<String> entitlementProcessNames = new Set<String>();

        for(EntitlementConfig config :  entitlementConfigs) {
            entitlementProcessNames.add(config.name);
        }

        List<SlaProcess> entitlementProcesses = [SELECT Id, Name FROM SlaProcess WHERE isVersionDefault = TRUE and isActive = TRUE and Name IN :entitlementProcessNames];


        Map<String, Id> entitlementNameToIdMap = new Map<String, Id>();

        for(SlaProcess entitlementProcess : entitlementProcesses) {
            entitlementNameToIdMap.put(entitlementProcess.Name, entitlementProcess.Id);
        }


        return entitlementNameToIdMap;

    }




    private Map<String, Id> getQueues() {

        Set<String> queueNames = new Set<String>();

        for(EntitlementConfig config : entitlementConfigs) {
            queueNames.add(config.queueName);
        }

        List<Group> queues = [SELECT Id, DeveloperName FROM Group WHERE DeveloperName IN :queueNames AND Type = 'Queue'];
        Map<String, Id> queueNameToIdMap = new Map<String, Id>();

        for(Group queue : queues) {
            queueNameToIdMap.put(queue.DeveloperName, queue.Id);
        }

        return queueNameToIdMap;

    }


    private class EntitlementConfig {

        String name;
        String contactCentreName;
        String queueName;
    
        // Constructor
        EntitlementConfig(String name, String contactCentreName, String queueName, Boolean addBolt) {
            this.name = name;
            this.contactCentreName = contactCentreName;
            this.queueName = queueName;
        }
    }


}