global class BatchUpdateMyJlCardType implements Database.Batchable<sobject>,Database.stateful {
    @TestVisible private Set<Id> masterSetOfCardTypeUpdated = new Set<Id>();
    @TestVisible private Map<Id, String> successfulCardTypeMap = new Map<Id, String>();
    @TestVisible private Map<Id, String> unsuccessfulCardTypeMap = new Map<Id, String>();
    private String CardType ;
    private boolean RollbackBatch ;
    
    public BatchUpdateMyJlCardType(String CardType, boolean RollbackBatch){
        this.CardType = CardType;
        this.RollbackBatch = RollbackBatch;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('Select id,card_type__c from Loyalty_Card__c where card_type__c =: CardType' );
    }
    
    global void execute(Database.BatchableContext BC, List<Loyalty_Card__c> scope){
        if(!scope.isEmpty()){  
            for(Loyalty_Card__c card : scope){
                if(!masterSetOfCardTypeUpdated.contains(card.Id)){
                    masterSetOfCardTypeUpdated.add(card.Id);
                } 
                
                if(RollbackBatch == false){
                card.Card_Type__c = 'My John Lewis';
                }
                else{
                    card.Card_Type__c = '';
                }
            }
            List<Database.SaveResult> updateCardTypeResults = Database.update(scope, false);
            System.debug('::::: updateCardTypeResults : '+updateCardTypeResults);
            
            for(Integer i = 0;i < updateCardTypeResults.size(); i++){
                // If card type is successfully updated
                if(updateCardTypeResults[i].isSuccess()){
                    successfulCardTypeMap.put(updateCardTypeResults[i].getId(), 'Successully Updated Card Type');
                }else{ //if error
                    String errorsConcatenated = '';
                    
                    for(Database.Error error : updateCardTypeResults[i].getErrors()) {
                        if(String.isEmpty(errorsConcatenated)) {
                            errorsConcatenated = error.getMessage();
                        } else {
                            errorsConcatenated = errorsConcatenated + ' | ' + error.getMessage();
                        }
                    }
                    
                    unsuccessfulCardTypeMap.put((String)scope[i].Id, errorsConcatenated);
                }
            }
            
            
        }
    }
    
    global void finish(Database.BatchableContext BC){
        System.debug('@@@ | BatchCreateCaseActivityForCase | masterSetOfCardTypeUpdated: ' + masterSetOfCardTypeUpdated);
        System.debug('@@@ | BatchCreateCaseActivityForCase | successfulCardTypeMap: ' + successfulCardTypeMap);
        System.debug('@@@ | BatchCreateCaseActivityForCase | Number of successfulUpdate: ' + successfulCardTypeMap.size());
        System.debug('@@@ | BatchCreateCaseActivityForCase | unsuccessfulCardTypeMap: ' + unsuccessfulCardTypeMap);
        System.debug('@@@ | BatchCreateCaseActivityForCase | Number of unsuccessfulUpdate : ' + unsuccessfulCardTypeMap.size());
        try{
            String emailBody = '';
            
            emailBody += 'BatchUpdateMyJlCardType job has run: \n\n\n';
            emailBody += 'Total number of records to update :\n';
            emailBody += masterSetOfCardTypeUpdated.size();
            emailBody += '\n\n';
            emailBody += 'The following Case Activity records were updated succesfully: \n\n';
            emailBody += successfulCardTypeMap.size();
            emailBody += '\n\n';
            emailBody += 'The following Case Activity records encountered errors: \n\n';
            emailBody += unsuccessfulCardTypeMap.size();
            system.debug('+++ emailBody: ' + emailBody);
            EmailNotifier.sendNotificationEmail('BatchUpdateMyJlCardType Results', emailBody);
        }catch(Exception e){
            System.debug('@@@ | BatchCreateCaseActivityForCase | masterSetOfCardTypeUpdated: ' + masterSetOfCardTypeUpdated);
            System.debug('@@@ | BatchCreateCaseActivityForCase | successfulCardTypeMap: ' + successfulCardTypeMap);
            System.debug('@@@ | BatchCreateCaseActivityForCase | Number of successfulUpdate: ' + successfulCardTypeMap.size());
            System.debug('@@@ | BatchCreateCaseActivityForCase | unsuccessfulCardTypeMap: ' + unsuccessfulCardTypeMap);
            System.debug('@@@ | BatchCreateCaseActivityForCase | Number of unsuccessfulUpdate : ' + unsuccessfulCardTypeMap.size());
            System.debug('@@@ | Error Message : ' +e.getMessage());
        }
    }
}