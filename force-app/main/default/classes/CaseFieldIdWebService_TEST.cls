@IsTest
public class CaseFieldIdWebService_TEST {

    @IsTest
    public static void testObjectKeyPrefix(){
        // case object prefix is the same in all orgs, so test with Case (similar to opportunity, account, contact etc)
        String casePrefix = CaseFieldIdWebService.getObjectKeyPrefix('Case');
        System.assertEquals(casePrefix, '/500/e?nooverride=1');
    }
    
    @IsTest
    public static void testFieldId(){
        String accountFieldId = CaseFieldIdWebService.getFieldId('Case', 'Description');
        System.assertEquals(accountFieldId, 'test_field_id');
    }

}