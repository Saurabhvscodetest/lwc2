/**
 * Unit tests for BatchPCDSupersededContactUpdate class methods.
 */
@isTest
private class BatchPCDSupersededContactUpdate_TEST {
	private static final Id SUPERSEDED_RECORD_TYPE = [SELECT Id FROM RecordType WHERE SobjectType = 'Contact' AND DeveloperName = 'Superseded' LIMIT 1].Id;

	/**
	 * Code coverage for BatchPCDSupersededContactUpdate.start
	 */
	static testMethod void testStartMethod() {
    	system.debug('TEST START: testStartMethod');

       	Contact masterCon = UnitTestDataFactory.createContact(1, false);
       	masterCon.PCD_Id__c = 'PCDMASTER123';
       	Contact supersededCon = UnitTestDataFactory.createContact(2, false);
       	supersededCon.PCD_Id__c = 'PCDTEST123';
       	List<Contact> conList = new List<Contact> {masterCon, supersededCon};
       	insert conList;
		
		Database.BatchableContext bc = null;
		BatchPCDSupersededContactUpdate pcdBatchUpdater = new BatchPCDSupersededContactUpdate();
		Test.startTest();
    	Database.QueryLocator dql = pcdBatchUpdater.start(bc);	
		Test.stopTest();

		system.assertEquals(false, PCDUpdateHandler.BATCH_MODE);
		system.assertEquals(false, PCDUpdateHandler.IS_RUNNING_PCD_UPDATE);
    	system.debug('TEST END: testStartMethod');
	}

	/**
	 * Test BatchPCDSupersededContactUpdate.execute
	 */
	static testMethod void testExecuteMethod() {
    	system.debug('TEST START: testExecuteMethod');

		initialiseSettings(true);
       	Contact masterCon = UnitTestDataFactory.createContact(1, false);
       	masterCon.PCD_Id__c = 'PCDMASTER123';
       	Contact supersededCon = UnitTestDataFactory.createContact(2, false);
       	supersededCon.PCD_Id__c = 'PCDTEST123';
       	List<Contact> conList = new List<Contact> {masterCon, supersededCon};
       	insert conList;
       	       	
       	Case testCase1 = UnitTestDataFactory.createQueryCase(supersededCon.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, true);
		Contact_Profile__c testContactProfile1 = new Contact_Profile__c(Contact__c = supersededCon.Id);
		insert testContactProfile1;
		Task testTask1 = new Task(WhatId = testCase1.Id, WhoId = supersededCon.Id, ActivityDate = Date.today(), Type = 'Case Assist', Subject = 'Assist',
								  Status = 'Completed', jl_created_from_case__c = true);
		Task testTask2 = new Task(WhoId = supersededCon.Id, ActivityDate = Date.today(), Type = 'Case Assist', Subject = 'Test Contact Task',
								  Status = 'Completed', jl_created_from_case__c = true);
		List<Task> taskList = new List<Task> {testTask1, testTask2};
		insert taskList;
		
		clsCaseTriggerHandler.resetCaseTriggerStaticVariables();
		
		supersededCon.Superseded_By_SCV_Id__c = masterCon.PCD_Id__c;
		update supersededCon;

		List<Contact> testConList1 = [SELECT Id, Name, Superseded_By_Customer__c, Superseded_By_SCV_Id__c, RecordTypeId, 
											(SELECT Id, Contact__c FROM ContactProfiles__r), 
											(SELECT Id, ContactId FROM Cases), 
											(SELECT Id, WhoId FROM Tasks) 
									  FROM Contact WHERE Id IN :conList];
		system.assertEquals(2, testConList1.size());
		
		for (Contact testCon : testConList1) {
			if (testCon.Id == masterCon.Id) {
				system.assertNotEquals(SUPERSEDED_RECORD_TYPE, testCon.RecordTypeId);
				system.assertEquals(null, testCon.Superseded_By_SCV_Id__c);
				system.assertEquals(null, testCon.Superseded_By_Customer__c);
				system.assertEquals(0, testCon.ContactProfiles__r.size());
				system.assertEquals(0, testCon.Cases.size());
				system.assertEquals(0, testCon.Tasks.size());
			} else if (testCon.Id == supersededCon.Id) {
				system.assertNotEquals(SUPERSEDED_RECORD_TYPE, testCon.RecordTypeId);
				system.assertNotEquals(null, testCon.Superseded_By_SCV_Id__c);
				system.assertEquals(null, testCon.Superseded_By_Customer__c);
				system.assertEquals(1, testCon.ContactProfiles__r.size());
				system.assertEquals(1, testCon.Cases.size());
				system.assertEquals(2, testCon.Tasks.size());
			} else {
				system.assert(false);
			}
		}

		Database.BatchableContext bc = null;
		BatchPCDSupersededContactUpdate pcdBatchUpdater = new BatchPCDSupersededContactUpdate();
		
		List<Contact> contactsToUpdate = [SELECT Id, Superseded_By_SCV_Id__c, Superseded_By_Customer__c, RecordTypeId
        								  FROM Contact
                                          WHERE Superseded_By_SCV_Id__c != null
                                          AND Superseded_By_Customer__c = null
                                          AND RecordTypeId != :SUPERSEDED_RECORD_TYPE];

		system.assertEquals(1, contactsToUpdate.size());
		
		test.startTest();
		pcdBatchUpdater.execute(bc, contactsToUpdate);
		test.stopTest();
		
		system.assertEquals(true, PCDUpdateHandler.BATCH_MODE);
		system.assertEquals(true, PCDUpdateHandler.IS_RUNNING_PCD_UPDATE);
		
		List<Contact> testConList2 = [SELECT Id, Name, Superseded_By_Customer__c, RecordTypeId, 
											(SELECT Id, Contact__c FROM ContactProfiles__r), 
											(SELECT Id, ContactId FROM Cases), 
											(SELECT Id, WhoId FROM Tasks) 
									  FROM Contact WHERE Id IN :conList];
		system.assertEquals(2, testConList2.size());
		
		for (Contact testCon : testConList2) {
			if (testCon.Id == masterCon.Id) {
				system.assertNotEquals(SUPERSEDED_RECORD_TYPE, testCon.RecordTypeId);
				system.assertEquals(null, testCon.Superseded_By_Customer__c);
				system.assertEquals(1, testCon.ContactProfiles__r.size());
				system.assertEquals(1, testCon.Cases.size());
				system.assertEquals(2, testCon.Tasks.size());
			} else if (testCon.Id == supersededCon.Id) {
				system.assertEquals(SUPERSEDED_RECORD_TYPE, testCon.RecordTypeId);
				system.assertNotEquals(null, testCon.Superseded_By_Customer__c);
				system.assertEquals(0, testCon.ContactProfiles__r.size());
				system.assertEquals(0, testCon.Cases.size());
				system.assertEquals(0, testCon.Tasks.size());
			} else {
				system.assert(false);
			}
		}
    	system.debug('TEST END: testExecuteMethod');
	}

    /**
     * Code coverage for BatchPCDSupersededContactUpdate.finish()
     */
    static testMethod void testFinishMethod() {
    	system.debug('TEST START: testFinishMethod');

    	Config_Settings__c defaultNotificationEmail = new Config_Settings__c(Name = 'EMAIL_NOTIFICATION_DEFAULT_ADDRESS', Text_Value__c = 'Matthew.Povey@johnlewis.co.uk');
		insert defaultNotificationEmail;
		Database.BatchableContext bc = null;
		BatchPCDSupersededContactUpdate pcdBatchUpdater = new BatchPCDSupersededContactUpdate();
		Test.startTest();
		pcdBatchUpdater.finish(bc);
		Test.stopTest();

		system.assertEquals(false, PCDUpdateHandler.BATCH_MODE);
		system.assertEquals(false, PCDUpdateHandler.IS_RUNNING_PCD_UPDATE);
    	system.debug('TEST END: testFinishMethod');
    }

	/**
	 * Custom setting initialisation
	 */
	private static void initialiseSettings(Boolean batchMode) {
       	UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
		Config_Settings__c c1 = new Config_Settings__c(Name = 'PCD_UPDATE_BATCH_MODE_ONLY', Checkbox_Value__c = batchMode);
		insert c1;
	}	
}