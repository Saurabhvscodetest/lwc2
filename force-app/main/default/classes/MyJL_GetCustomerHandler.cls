/*------------------------------------------------------------
Author:         Jan-Willem Brokamp
Company:        Salesforce.com
Description:   
Test Class:     MyJL_GetCustomerHandlerTest
    
History
<Date>      <Authors Name>     <Brief Description of Change>
//  001     2014-06-13  JWB     Initial Release
//  002     26/02/15    NTJ     MJL-1581 - Make House No a Text field so that it does not display as 999,999 (should be 999999)
//  003     20/04/15    ntj     Remove Debug.log
//  004     23/04/15    NTJ     MJL-1823 - GetCustomer may need to use Potential Customer if Contact Profile not yet created
//  005     20/05/15    NTJ     MJL-1957 - use LastModifiedDateMs__c as it has milliseconds 
//  006     29/05/15    NTJ     MJL-2016 - Optimisation of this code
//  007     29/06/15    NTJ     MJL-2100 - Use Contact_Profile__c.AnonymousFlag2__c
//  008     27/06/16    NA      Decommissioning contact profile 
------------------------------------------------------------*/

public class MyJL_GetCustomerHandler {
    
    // 006 - Optimised - Don't get Contact_Profile info
    private static final Boolean JUST_ACCOUNT_INFO = true;
    
    /* Wrapper object to group state with return object */
    @TestVisible private class GetCustomerWrapper extends MyJL_Util.CustomerWrapper { 
        private Integer                                         requestIndex = -1;
        private Boolean                                         customerFound = false;
        private Boolean                                         isProcessed = false;
        
        private SFDCMyJLCustomerTypes.Customer                  inputCustomer = null;
        private SFDCMyJLCustomerTypes.CustomerRecordResultType  customerResult = MyJL_Util.getSuccessResult();
        private String                                          stackTraceDetails = null;
        
        /* Empty Constructor */
        public GetCustomerWrapper() {
        }
        
        public Integer getRequestIndex() {
            return this.requestIndex;
        }
        
        public void setRequestIndex(Integer requestIndex) {
            this.requestIndex = requestIndex;
        }
        
        public override SFDCMyJLCustomerTypes.Customer getCustomerInput() {
            return this.inputCustomer;
        }
        
        public void setCustomerInput(SFDCMyJLCustomerTypes.Customer inputCustomer) {
            this.inputCustomer = inputCustomer;
        }
        
        public override SFDCMyJLCustomerTypes.CustomerRecordResultType getCustomerResult() {
            return this.customerResult;
        }
        
        public void setCustomerResult(SFDCMyJLCustomerTypes.CustomerRecordResultType customerResult) {
            this.customerResult = customerResult;
        }
        
        public override String getStackTrace() {
            return this.stackTraceDetails;
        }
        
        public void setStackTrace(String stackTraceDetails) {
            this.stackTraceDetails = stackTraceDetails;
        }
        
        public Boolean getCustomerFound() {
            return this.customerFound;
        }
        
        public void setCustomerFound(Boolean customerFound) {
            this.customerFound = customerFound;
        }
        
        public Boolean getIsProcessed() {
            return this.isProcessed;
        }
        
        public void setIsProcessed(Boolean isProcessed) {
            this.isProcessed = isProcessed;
        }

    }
    
    /* Empty Constructor */
    public MyJL_GetCustomerHandler() {
    }
    
    public static SFDCMyJLCustomerTypes.GetCustomerResponseType process(SFDCMyJLCustomerTypes.CustomerRequestType request) {
        final SFDCMyJLCustomerTypes.Customer[] customers = request.Customer;
        
        //Start Log
        ID      logHeaderRecordId = LogUtil.startLogHeader(LogUtil.SERVICE.WS_GET_CUSTOMER, request.RequestHeader, customers);
        String  logHeaderStackTrace = null;
            
        //Init return object
        SFDCMyJLCustomerTypes.GetCustomerResponseType response = new SFDCMyJLCustomerTypes.GetCustomerResponseType();
        
        //Init Header & response payload 
        SFDCMyJLCustomerTypes.ResponseHeaderType responseHeader = new SFDCMyJLCustomerTypes.ResponseHeaderType();
        
        
        //Initialize Wrapper List & Map
        //List is an exact match of the incoming array of Customers
        //Map is a CustomerID->GetCustomerWrapper structure used to quickly find a wrapper for a particular customer
        //  note: the map size can be < then the list size if multiple customers have the same (or empty) customerId
        List<GetCustomerWrapper> processCustomerWrapperList = new List<GetCustomerWrapper>();
        Map<String, GetCustomerWrapper> processCustomerWrapperMap = new Map<String, GetCustomerWrapper>();
        
       try{
            
         if(request.requestHeader == null || request.RequestHeader.MessageId == null || String.IsBlank(request.RequestHeader.MessageId)) {
            //Return error indicating no Message ID was found
            SFDCMyJLCustomerTypes.ResultStatus resultStatus = MyJL_Util.getSuccessResultStatus();
            resultStatus = MyJL_Util.populateErrorDetailsHeader(MyJL_Const.ERRORCODE.MESSAGEID_NOT_PROVIDED, resultStatus);
                        
            responseHeader.ResultStatus = resultStatus;
        } else if(! MyJL_Util.isMessageIdUnique(request.RequestHeader.MessageId)) {
            //Return error indicating the Message ID is a duplicate
            SFDCMyJLCustomerTypes.ResultStatus resultStatus = MyJL_Util.getSuccessResultStatus();
            resultStatus = MyJL_Util.populateErrorDetailsHeader(MyJL_Const.ERRORCODE.MESSAGEID_DUPLICATE, resultStatus);
                        
            responseHeader.ResultStatus = resultStatus;
        } else if(request.Customer == null || request.Customer.size() == 0) {
            //Return error indicating we need at least 1 Customer as input
            SFDCMyJLCustomerTypes.ResultStatus resultStatus = MyJL_Util.getSuccessResultStatus();
            resultStatus = MyJL_Util.populateErrorDetailsHeader(MyJL_Const.ERRORCODE.NO_CUSTOMER_DATA, resultStatus);
                        
            responseHeader.ResultStatus = resultStatus;
        } else {
            //We've got a good header. Mirror messageId back in the response
            responseHeader.MessageId = request.RequestHeader.MessageId;
            
            //system.debug('GetCustomerResponse echo back MessageID. ResponseHeader: ' + responseHeader);
        
            //Populate Customer objects in Response and error handling
            Integer index = 0;
            for(SFDCMyJLCustomerTypes.Customer customer : request.Customer) {
                //Wrap Customer Objects - the Customer Object in the wrapper>CustomerRecordResultType object is the _RETURN_ instance not the received instance
                GetCustomerWrapper processCustomerWrapper = new GetCustomerWrapper();
                processCustomerWrapper.setRequestIndex(index);  
                processCustomerWrapper.setCustomerInput(customer);  
                
                //Do we have a CustomerID
                if(processCustomerWrapper.getCustomerId() == null) {
                    //Set error indicating no CustomerID was found in this Customer record
                    processCustomerWrapper.setCustomerResult(MyJL_Util.populateErrorDetails(MyJL_Const.ERRORCODE.CUSTOMERID_NOT_PROVIDED, processCustomerWrapper.getCustomerResult()));
                    processCustomerWrapper.setIsProcessed(true);
                    // log a rejection
                    logRejection(MyJL_Const.ERRORCODE.CUSTOMERID_NOT_PROVIDED, processCustomerWrapper);                                             
                } else {
                    //Add wrapper to map
                    processCustomerWrapperMap.put(processCustomerWrapper.getCustomerId(), processCustomerWrapper);  
                }
                
                //system.debug('processCustomerWrapper: '+processCustomerWrapper);
                
                //Add wrapper object to list
                processCustomerWrapperList.add(processCustomerWrapper);
                
                //Increment index counter
                index++;
            }
 
            //Query Contact Profiles
            Contact[] contactProfiles;  //<<008>>
            MyJL_MappingUtil.DbWrapper dbw;

            if (JUST_ACCOUNT_INFO == false) {
                contactProfiles = queryContactProfiles(processCustomerWrapperMap.keySet());                 
            } else {
                dbw = queryContactProfiles2(processCustomerWrapperMap.keySet()); 
                contactProfiles = dbw.cps;
                // could be that we have a GET and we have never Joined. e.g. migrated user and a Leave occurs
                if (contactProfiles.isEmpty()) {
                    contactProfiles = queryContactProfiles(processCustomerWrapperMap.keySet());
                }
            }
            
            //Create map to list all SFDC_ID->CustomerAccounts we have located. These are used to query matching Loyalty Cards.
            Map<ID, SFDCMyJLCustomerTypes.CustomerAccount> allCustomerAccounts = new Map<ID, SFDCMyJLCustomerTypes.CustomerAccount>();
            
            /* Loop over query results & populate wrapper object */
            for(Contact contactProfile : contactProfiles) {  //<<008>>
                //Find the right wrapper object for this customer
                
                GetCustomerWrapper processCustomerWrapper = processCustomerWrapperMap.get(MyJL_Util.getCustomerId(contactProfile));
                if(processCustomerWrapper != null) {
                    processCustomerWrapper.setIsProcessed(true);
                    
                    /* Did we already find this record earlier in the loop? (duplicate records returned by query) */
                    if(processCustomerWrapper.getCustomerFound()) {
                        processCustomerWrapper.setCustomerResult(MyJL_Util.populateErrorDetails(MyJL_Const.ERRORCODE.CUSTOMERID_ALREADY_USED, processCustomerWrapper.getCustomerResult()));
                        // log a rejection
                        logRejection(MyJL_Const.ERRORCODE.CUSTOMERID_ALREADY_USED, processCustomerWrapper);                                             
                    } else {
                        //Create Customer EBO based on the Contact_Profile__c instance
                        SFDCMyJLCustomerTypes.Customer returnCustomer = MyJL_MappingUtil.populateCustomer(contactProfile, dbw);
                        processCustomerWrapper.getCustomerResult().Customer = returnCustomer;
                        processCustomerWrapper.setCustomerFound(true);
                        //system.debug('AMIT Customer values :' + processCustomerWrapper.getCustomerResult().Customer);
                            
                        //Keep track of all Loyalty Accounts that have at least one account - we need to add Card Details for this
                        //as well but in a seperate query/flow
                        if(processCustomerWrapper.getCustomerResult().Customer.CustomerAccount != null) {
                            SFDCMyJLCustomerTypes.CustomerAccount[] customerAccounts = processCustomerWrapper.getCustomerResult().Customer.CustomerAccount;
                            for(SFDCMyJLCustomerTypes.CustomerAccount customerAccount : customerAccounts) {
                                if(customerAccount.CustomerAccountID != null && customerAccount.CustomerAccountID.ID != null) {
                                    allCustomerAccounts.put(customerAccount.CustomerAccountID.ID, customerAccount);
                                    //system.debug('AMIT allCustomerAccounts values :' + allCustomerAccounts);
                                }
                            }
                        }
                    }
                }
            }
            
            /* For customers not found try and check the Limited Customer object as fallback */
            List<String> possibleLimitedCustomerIDs = new List<String>();
            
            for(GetCustomerWrapper processCustomerWrapper : processCustomerWrapperMap.values()) {
                if(! processCustomerWrapper.getCustomerFound()) {
                    possibleLimitedCustomerIDs.add(processCustomerWrapper.getCustomerId());
                }
            }
            
            //Query Limited Customer records that match the Customer IDs            
            Potential_Customer__c[] limitedCustomers = queryLimitedCustomers(possibleLimitedCustomerIDs);
            
            // MJL-1823 - collect emails
            //system.debug('allCustomerAccounts: 1: '+allCustomerAccounts);
            Set<String> emailSet = new Set<String>();
            Map<String, SFDCMyJLCustomerTypes.Customer> email2CustomerMap = new Map<String, SFDCMyJLCustomerTypes.Customer>();
            for(Potential_Customer__c limitedCustomer : limitedCustomers) {
                // MJL-1823 - store the email
                Boolean emailPresent = String.isNotBlank(limitedCustomer.email__c);
                if (emailPresent == true) {
                    emailSet.add(limitedCustomer.email__c); 
                }
                //Find the right wrapper
                GetCustomerWrapper processCustomerWrapper = processCustomerWrapperMap.get(MyJL_Util.getCustomerId(limitedCustomer));
                if(processCustomerWrapper != null) {
                    //Create Customer EBO based on the Potential_Customer__c instance
                    SFDCMyJLCustomerTypes.Customer returnCustomer = MyJL_MappingUtil.populateCustomer(limitedCustomer);
                    processCustomerWrapper.getCustomerResult().Customer = returnCustomer;
                    
                    if (emailPresent == true) {
                        email2CustomerMap.put(limitedCustomer.email__c, returnCustomer);
                    }
                    processCustomerWrapper.setIsProcessed(true);
                    processCustomerWrapper.setCustomerFound(true);
                }
            }
            //system.debug('allCustomerAccounts: 2: '+allCustomerAccounts);
            
            // MJL-1823 - process cards
            if(emailSet.size() > 0) {
                //Query all loyalty cards related to the Loyalty Accounts we found earlier          
                Loyalty_Account__c[] loyaltyAccounts = queryLoyaltyAccountCardsByEmail(emailSet);
                
                //Loop over results
                for(Loyalty_Account__c loyaltyAccount : loyaltyAccounts) {
                    // get the associated Customer
                    SFDCMyJLCustomerTypes.Customer customer = email2CustomerMap.get(loyaltyAccount.email_Address__c);
                    SFDCMyJLCustomerTypes.CustomerAccount customerAccount;

                    if (customer != null) {
                        // add list of accounts
                        if (customer.CustomerAccount == null) {
                            customer.CustomerAccount = new List<SFDCMyJLCustomerTypes.CustomerAccount>();
                        }
                        // Create the Account
                        customerAccount = MyJL_MappingUtil.populateLoyaltyAccount(loyaltyAccount);
                        customer.CustomerAccount.add(customerAccount);
                        // MJL-1584
                        if (loyaltyAccount.IsActive__c == true) {
                            customer.IsMyJLRegistered = true;           
                        }
                    }
                
                   if (customerAccount != null && customerAccount.CustomerLoyaltyAccountCard == null) {

                        for (Loyalty_Card__c loyaltyCard:loyaltyAccount.MyJL_Cards__r) {    
                            //Create CustomerLoyaltyAccountCard object
                            SFDCMyJLCustomerTypes.CustomerLoyaltyAccountCard customerLoyaltyAccountCard = MyJL_MappingUtil.populateCustomerLoyaltyAccountCard(loyaltyCard);                     
                            //Add to List
                            if (customerAccount.CustomerLoyaltyAccountCard == null) {
                                customerAccount.CustomerLoyaltyAccountCard = new List<SFDCMyJLCustomerTypes.CustomerLoyaltyAccountCard>();
                            }
                            if (!cardAlreadyAdded(customerAccount.CustomerLoyaltyAccountCard, customerLoyaltyAccountCard)) {
                                customerAccount.CustomerLoyaltyAccountCard.add(customerLoyaltyAccountCard);                             
                            }
                        }                                                                                   
                    }
                }
            }                           
            /* Any wrapper that does not have a CustomerResult did not return in the query - set ERROR */
            for(GetCustomerWrapper processCustomerWrapper : processCustomerWrapperList) {
                if(! processCustomerWrapper.getIsProcessed()) {
                    processCustomerWrapper.setCustomerResult(MyJL_Util.populateErrorDetails(MyJL_Const.ERRORCODE.CUSTOMERID_NOT_FOUND, processCustomerWrapper.getCustomerResult()));
                    // log a rejection
                    logRejection(MyJL_Const.ERRORCODE.CUSTOMERID_NOT_FOUND, processCustomerWrapper);                                            
                }
            }
            
            //No global exception thrown - set success state to true (TODO?)
            SFDCMyJLCustomerTypes.ResultStatus resultStatus = MyJL_Util.getSuccessResultStatus();
            responseHeader.ResultStatus = resultStatus;
        }
            
        //Set response header
        response.ResponseHeader = responseHeader;
        
        //Set response payload and ensure it's in the same order as the inbound array of customer identifiers (based on requestIndex)
        SFDCMyJLCustomerTypes.CustomerRecordResultType[] responseCustomers = new SFDCMyJLCustomerTypes.CustomerRecordResultType[processCustomerWrapperList.size()];
        for(GetCustomerWrapper processCustomerWrapper : processCustomerWrapperList) {
            responseCustomers[processCustomerWrapper.getRequestIndex()] = processCustomerWrapper.getCustomerResult();
            
            //Echo input Customer EBO in the response in case of errors
            if(! responseCustomers[processCustomerWrapper.getRequestIndex()].Success) {
                responseCustomers[processCustomerWrapper.getRequestIndex()].Customer = processCustomerWrapper.getCustomerInput();
            }
        }
        
        response.ActionRecordResults = responseCustomers;  
        
        //Store log detail records
        LogUtil.logDetails(logHeaderRecordId, processCustomerWrapperMap.values());
        
        //Update log header record
        LogUtil.updateLogHeader(logHeaderRecordId, responseHeader, null);
        }
        
        catch (Exception e) {
            SFDCMyJLCustomerTypes.ResultStatus resultStatus = new SFDCMyJLCustomerTypes.ResultStatus();
            resultStatus.Code = null;
            resultStatus.Message = null;
            resultStatus.Type_x = null;
            resultStatus.Success = false;

            //SFDCMyJLCustomerTypes.CustomerResponseType response = new SFDCMyJLCustomerTypes.CustomerResponseType();
            responseHeader.ResultStatus = resultStatus;
            response.ResponseHeader = responseHeader;
            
            //responseHeader.ResultStatus.Success = false;
            //we do not have the code for this situation
            //throw new TerminateProcessException(MyJL_Const.ERRORCODE.UNHANDLED_EXCEPTION, logHeaderId, responseHeader, e);
            responseHeader.ResultStatus = MyJL_Util.populateErrorDetailsHeader(MyJL_Const.ERRORCODE.UNHANDLED_EXCEPTION, responseHeader.ResultStatus);
            LogUtil.updateLogHeader(logHeaderRecordId, responseHeader, e.getStackTraceString());
            System.assertEquals(false, Test.isRunningTest(), 'exception thrown: ' + e);
            //system.debug('AMIT e=' + e);
            //system.debug('AMIT e.stack=' + e.getStackTraceString());
        }
        
        return response;
    }
    
    private static Boolean cardAlreadyAdded(List<SFDCMyJLCustomerTypes.CustomerLoyaltyAccountCard> cards, SFDCMyJLCustomerTypes.CustomerLoyaltyAccountCard card) {
        Boolean alreadyAdded = false;

        if (card != null && cards != null) {
            for (SFDCMyJLCustomerTypes.CustomerLoyaltyAccountCard thisCard:cards) {
                //system.debug('(thisCard.CustomerLoyaltyAccountCardId: '+thisCard.CustomerLoyaltyAccountCardId);
                //system.debug('(thisCard.card: '+card.CustomerLoyaltyAccountCardId);
                if (thisCard.CustomerLoyaltyAccountCardId == card.CustomerLoyaltyAccountCardId) {
                    //system.debug('found');
                    alreadyAdded = true;
                    break;
                }
            }           
        }
        
        return alreadyAdded;
    }
    
    //#####################################################################################################################
    //## Queries
    //#####################################################################################################################
 
    //
    // New optimised version. Just call Loyalty_Account__c and fabricate an empty contact profile 
    //
    private static MyJL_MappingUtil.DbWrapper queryContactProfiles2(Set<String> customerIds) {
        MyJL_MappingUtil.DbWrapper dbw = new MyJL_MappingUtil.DbWrapper();
        Map<String, Contact> shopper2CpMap = new Map<String, Contact>();   //<<008>>

        //system.debug('customerIds: '+customerIds);
        if (customerIds.isEmpty() == false) {
            for (Loyalty_Account__c la: [SELECT Id, Name, Email_Address__c, Scheme_Joined_Date_Time__c,My_Partnership_Activation_Date_Time__c, Activation_Date_Time__c, Deactivation_Date_Time__c,
                                         Welcome_Email_Sent_Flag__c, Registration_Workstation_ID__c, Registration_Business_Unit_ID__c, Channel_ID__c,
                                         MyJL_Old_Membership_Number__c,LastModifiedById, IsActive__c, LastModifiedDate, LastModifiedDateMs__c, ShopperID__c,Voucher_Preference__c, Number_Of_Partnership_Cards__c,
                                         (SELECT Loyalty_Account__c, Name FROM MyJL_Cards__r 
                                          where disabled__c = false 
                                          order by CreatedDate DESC)
                                         FROM Loyalty_Account__c
                                         WHERE ShopperID__c IN :customerIds]) {
                
                // create a dummy cp to satisfy the caller and add to the list                          
                if (String.isNotBlank(la.ShopperID__c)) {
                    // do we have an existing cp?
                    Contact cp = (Contact) shopper2CpMap.get(la.ShopperID__c);  //<<008>>
                    
                    if (cp == null) {
                        // create new and add to map
                        cp = new Contact(Shopper_ID__c=la.ShopperID__c, email=la.Email_Address__c);  //<<008>>
                        shopper2CpMap.put(la.ShopperID__c, cp);
                    }
                    
                    // add to the cp list
                    dbw.cps.add(cp);                                    
                }
                // store the account
                dbw.las.add(la);
            }
        }
        return dbw;
    }
    
    private static Contact[] queryContactProfiles(Set<String> customerIds) {  //<<008>>
        //system.debug('GetCustomerResponse-queryContactProfiles list of IDs for the Contact_Profile__c query: ' + customerIds);
        
        List<Contact> contactProfiles = new List<Contact>();
        
        if(customerIds != null && customerIds.size() > 0) {
            // MJL-1790
            for (Contact cp: [
                SELECT
                    name, 
                    Shopper_ID__c, 
                    birthdate,
                    salutation,
                    title,
                    firstname,
                    lastname,
                    email,
                    Mailing_House_No_Text__c,
                    Mailing_House_Name__c,
                    Mailing_Street__c,
                    Mailing_Address_Line2__c,
                    Mailing_Address_Line3__c,
                    Mailing_Address_Line4__c,
                    mailingstreet,
                    mailingcity,
                    mailingstate,
                    mailingcountry,
                    mailingcountrycode,
                    mailingpostalcode,
                    MobilePhone,
                    phone,
                     otherstreet,
                     otherstate,
                    othercity,
                    othercountry,
                    otherpostalcode,
                    MyJL_ESB_Message_ID__c,
                    AnonymousFlag2__c,
                    (Select 
                        Name,
                        Email_Address__c,
                        Scheme_Joined_Date_Time__c,
                        Activation_Date_Time__c,
                        Deactivation_Date_Time__c,
                        Welcome_Email_Sent_Flag__c,
                        Registration_Workstation_ID__c,
                        Registration_Business_Unit_ID__c,
                        Channel_ID__c,
                        LastModifiedById,
                        IsActive__c,
                        LastModifiedDate, LastModifiedDateMs__c,
                        ShopperID__c,
                        Number_Of_Partnership_Cards__c,
                        Voucher_Preference__c
                    From MyJL_Accounts__r)  //<<008>>
                FROM 
                    Contact
                WHERE
                    Shopper_ID__c IN :customerIds
                    AND SourceSystem__c = :Label.MyJL_JL_comSourceSystem
                    ORDER BY CreatedDate
            ]) {  //<<008>>
                if (String.isNotBlank(cp.Shopper_ID__c)) {
                    contactProfiles.add(cp);
                }               
            }
        }
        
        //system.debug('GetCustomerResponse-queryContactProfiles result of Contact_Profile__c query: ' + contactProfiles);
        
        return contactProfiles;
    }
    
    @TestVisible private static Loyalty_Account__c[] queryLoyaltyAccountCardsByEmail(Set<String> emailSet) {
        //system.debug('queryLoyaltyCardsByEmail: ' + emailSet);
        
        List<Loyalty_Account__c> loyaltyAccounts;
        
        if(emailSet != null && emailSet.size() > 0) {
            loyaltyAccounts = [
                SELECT Id, Name, Email_Address__c, Scheme_Joined_Date_Time__c, Activation_Date_Time__c, Deactivation_Date_Time__c,
                                                    Welcome_Email_Sent_Flag__c, Registration_Workstation_ID__c, Registration_Business_Unit_ID__c, Channel_ID__c,
                                                    LastModifiedById, IsActive__c, LastModifiedDate, LastModifiedDateMs__c,Voucher_Preference__c, Number_Of_Partnership_Cards__c,
                (SELECT
                    Loyalty_Account__c, 
                    Name
                 FROM MyJL_Cards__r)    
                FROM 
                    Loyalty_Account__c
                WHERE
                    Email_Address__c IN :emailSet
            ]; 
        } else {
             loyaltyAccounts = new List<Loyalty_Account__c>();
        }
        
        //system.debug('queryLoyaltyCardsByEmail: ' + loyaltyAccounts);
        
        return loyaltyAccounts;     
    }
    
    @TestVisible private static Loyalty_Card__c[] queryLoyaltyCards(Set<ID> loyaltyAccountIDs) {
        //system.debug('GetCustomerResponse-queryLoyaltyCards list of IDs for the Loyalty_Card__c query: ' + loyaltyAccountIDs);
        
        List<Loyalty_Card__c> loyaltyCards = new List<Loyalty_Card__c>();
        
        if(loyaltyAccountIDs != null && loyaltyAccountIDs.size() > 0) {
            loyaltyCards = [
                SELECT
                    Loyalty_Account__c, 
                    Card_Type__c,
                    Name
                FROM 
                    Loyalty_Card__c
                WHERE
                    Loyalty_Account__c IN :loyaltyAccountIDs and disabled__c = false
            ]; 
        }
        
        //system.debug('GetCustomerResponse-queryLoyaltyCards result of Loyalty_Card__c query: ' + loyaltyCards);
        
        return loyaltyCards;
    }
    
    private static Potential_Customer__c[] queryLimitedCustomers(List<String> customerIds) {
        //system.debug('GetCustomerResponse-queryLimitedCustomers list of IDs for the Potential_Customer__c query: ' + customerIds);
        
        List<Potential_Customer__c> limitedCustomers = new List<Potential_Customer__c>();
        
        if(customerIds != null && customerIds.size() > 0) {
            for (Potential_Customer__c pc :[
                SELECT
                    Shopper_ID__c,
                    Title__c,
                    Initials__c,
                    Salutation__c,
                    FirstName__c,
                    LastName__c,
                    Email__c, 
                    Telephone_Number__c,
                    Date_of_Birth__c,
                    AnonymousFlag__c
                FROM
                    Potential_Customer__c
                WHERE
                    Shopper_ID__c IN :customerIds
                    AND Source_System__c = :Label.MyJL_JL_comSourceSystem
            ]) {
                if (String.isNotBlank(pc.Shopper_ID__c)) {
                    limitedCustomers.add(pc);
                }
            }           
        }

        //system.debug('GetCustomerResponse-queryLimitedCustomers result of Potential_Customer__c query: ' + limitedCustomers);
        
        return limitedCustomers;
    }
    
    public static MyJL_Rejection__c logRejection(MyJL_Const.ERRORCODE code, GetCustomerWrapper wrapper) {
        return MyJL_Util.logRejection(code, wrapper.getEmailAddress(), wrapper.getCustomerId(), 'GetCustomer');
    }
}