/**
 */
@isTest
private class coSoapHelper_TEST {
    private static final String PRODCODE1 = '89140198';
    private static final String STATUS1 = 'Due for delivery on 25 Nov 2014';
	
	private static final String xml1 = 	
			'<ItemDetail>'+
				'<JLDWebSku>233598912</JLDWebSku>'+
				'<OrderItemTitle>3 years Added Care for your computing (accidental damage cover)</OrderItemTitle>'+
				'<PriceIncVAT>30</PriceIncVAT>'+
				'<TotalPriceIncVAT>30</TotalPriceIncVAT>'+
				'<TotalPartnerDiscount>0</TotalPartnerDiscount>'+
				'<UnitPartnerDiscount>0</UnitPartnerDiscount>'+
				'<ProductCode>'+PRODCODE1+'</ProductCode>'+
				'<EmptyTag/>'+
				'<OrderedQuantity>'+
					'<Status>'+STATUS1+'</Status>'+
					'<Quantity>1</Quantity>'+
				'</OrderedQuantity>'+
				'<ConsignmentTracking>'+
					'<CarrierDescription/>'+
				'</ConsignmentTracking>'+
			'</ItemDetail>';


	static testMethod void test_getChildStringGood() {
		Dom.XMLNode itemDetailNode = getNode();
		
		// Gets the Product Code as it is not empty
      	String s1 = coSoapHelper.getChildString(itemDetailNode, 'ProductCode', null);
      	system.assertEquals(PRODCODE1, s1);		
	}

	static testMethod void test_getChildStringBad() {
		Dom.XMLNode itemDetailNode = getNode();

		// Gets an empty string as it is empty
      	String s2 = coSoapHelper.getChildString(itemDetailNode, 'EmptyTag', null);
      	system.assertEquals('', s2);
	}
/* TODO	
	static testMethod void test_getGrandChildStringGood() {
		Dom.XMLNode itemDetailNode = getNode();

		// Get OrderedQuantity.Status as it is not null
		String s3 = coSoapHelper.getGrandChildString(itemDetailNode, 'OrderedQuantity', null, 'Status', null);
      	system.assertEquals(STATUS1, s3);
	}
*/	
	static testMethod void test_getGrandChildStringBad() {
		Dom.XMLNode itemDetailNode = getNode();

		// Get empty string as it is null
		String s4 = coSoapHelper.getGrandChildString(itemDetailNode, 'ConsignmentTracking', null, 'CarrierDescription', null);
      	system.assertEquals('', s4);
	}

	static testMethod void test_getGrandChildGood() {
		Dom.XMLNode itemDetailNode = getNode();

		// get grandchild node as it is not null
      	Dom.XMLNode xmlNode1 = coSoapHelper.getGrandChild(itemDetailNode, 'OrderedQuantity', null, 'Status', null);
      	system.assertNotEquals(null, xmlNode1);
	}

	static testMethod void test_getGrandChildBad() {
		Dom.XMLNode itemDetailNode = getNode();

		// get null as there is no such grandchild
      	Dom.XMLNode xmlNode2 = coSoapHelper.getGrandChild(itemDetailNode, 'OrderedQuantity', null, 'NONEXISTANT', null);
      	system.assertEquals(null, xmlNode2);
	}

    private static Dom.XMLNode getNode () {
    	
    	system.debug('xml1: '+xml1);
    	
		Dom.document doc = new Dom.document();
		system.assertNotEquals(null, doc);
		
      	doc.load(xml1);
      	
      	Dom.XMLNode itemDetailNode = doc.getRootElement();
		system.assertNotEquals(null, itemDetailNode);
		
      	system.debug('itemDetailNode: '+itemDetailNode);
      	
      	return itemDetailNode;
    }
}