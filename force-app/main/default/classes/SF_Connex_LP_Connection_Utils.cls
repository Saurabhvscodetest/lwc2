Public  class SF_Connex_LP_Connection_Utils {

    Public static boolean customerNameCheck( String customerGivenName, String connexCustomerName){
        
        try{
        String customerGivenNameLow = customerGivenName.toLowerCase();
        String connexCustomerNameLow = connexCustomerName.toLowerCase();

        if(connexCustomerNameLow.contains(customerGivenNameLow)){
            return true;
            
        }else if(connexCustomerNameLow == customerGivenNameLow){
            
            return true;
        }
        else if(customerGivenNameLow.containsWhitespace()){
            List<String> nameCheckList = customerGivenNameLow.split(' ');
            
            if(connexCustomerNameLow.contains(nameCheckList[0])){
                return true;
            }else if( connexCustomerNameLow.contains(nameCheckList[1]) ){
                return true;
            }else{
                return false;
            }            
        }
        else{
            return false;
        }
        }
        catch(Exception e){           
            return false;
        }
    }
    
     Public static boolean customerPostCodeCheck( String customerGivenPostCode, String connexCustomerPostCode){
         try{
         string customerGivenPostCodeLow = customerGivenPostCode.toLowerCase();
         string connexCustomerPostCodeLow = connexCustomerPostCode.toLowerCase();

         if(customerGivenPostCodeLow == connexCustomerPostCodeLow){
             return true;
         }else if(customerGivenPostCodeLow.remove(' ') == connexCustomerPostCodeLow.remove(' ')){
             return true;
         }

         else{
             
            return false;  
         }}catch(Exception e){
             
             return false;
         }
    }
    
     Public static boolean customerAddressCheck( string customerGivenAddress, String connexCustomerAddress){
         try{
         String customerGivenAddressLow = customerGivenAddress.toLowerCase();
         String connexCustomerAddressLow = connexCustomerAddress.toLowerCase();
         if(customerGivenAddressLow == connexCustomerAddressLow){
            return true;             
         }
         else if(connexCustomerAddressLow.contains(customerGivenAddressLow)){
             return true;
         }
         else if( customerGivenAddressLow.remove(' ') == connexCustomerAddressLow.remove(' ') ){
             return true;
         }
         else if( customerGivenAddressLow.remove(',') == connexCustomerAddressLow.remove(',') ){
             return true;
        }
         else{
            return false; 
                }         
         }
         catch(Exception e){   
             return false;
         }
     }
}