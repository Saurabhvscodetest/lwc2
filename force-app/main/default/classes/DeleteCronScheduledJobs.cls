/** Batch Job Used to Delete the Completed/ Errored Scheduler Jobs Relating to 
*   CopyEmailAttachments. This is required since because of errored jobs the 
*   Class clsScheduleCopyAttachments will not be allowed to edit.
*/
global class DeleteCronScheduledJobs implements Database.batchable<SObject>{
    // Query Where Cron Trigger is Fetched based on CronJobDetail
    global final String Query='select Id from CronTrigger where State = \'DELETED\' and nextfiretime = null and CronJobDetailId in (Select Id from CronJobDetail where name like \'%CopyEmailAttachments%\')';
    
    //The Start Method which is used to run the Query and return Subset in Batches
    global Database.QueryLocator start(Database.BatchableContext BC){
    if (!Test.isRunningTest())
    {
      return Database.getQueryLocator(query);
     }
     else{
         return Database.getQueryLocator([Select Id from CronTrigger Limit 2]);
     }
   }

    //Execute Method which runs in Every batch of the Query Result.
   global void execute(Database.BatchableContext BC, List<sObject> scope){
      List<CronTrigger> listCronTrigger = scope;
      for (CronTrigger ct:listCronTrigger)
      {
          System.abortJob(ct.id);
      }
    }
    // Finalized Action on the Batch Job.
   global void finish(Database.BatchableContext BC){
   }

}