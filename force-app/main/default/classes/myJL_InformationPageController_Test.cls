/**
* Author:       Nawaz Ahmed
* Date:         24/09/2015
* Description:  Test Class for myJL_InformationPageController
*/
@isTest
private class myJL_InformationPageController_Test {
    
    @testSetup
    static void init(){
        PageReference pageRef = Page.myJL_MembershipDetails;
        Test.setCurrentPage(pageRef); 
        
        Contact con =  UnitTestDataFactory.createContact();
        Insert con;
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        
        Loyalty_Account__c la = new Loyalty_Account__c(Name='00000000012', Email_Address__c = 'harry.hoppo@animals.org', IsActive__c = true, ShopperId__c='shopper1');
        la.Customer_Loyalty_Account_Card_ID__c = 'TestCard';
        la.Channel_ID__c = 'johnlewis.com';
        la.ShopperId__c = con.id;
        la.Voucher_Preference__c = MyJL_Const.PACK_TYPE_DIGITAL;
        la.Activation_Date_Time__c = datetime.now();
        Insert la;
        
        Loyalty_Card__c LC = new Loyalty_Card__c(Name = 'TestCard', Loyalty_Account__c = LA.Id);
        insert Lc;
        
    }
    
    
    @isTest
    static void InformationPageTest() {
        
        Contact con = [SELECT Id, Name, Email FROM Contact];
        Loyalty_Account__c la = [SELECT Id, Name, Customer_Loyalty_Account_Card_ID__c, Email_Address__c, Voucher_Preference__c, IsActive__c,
                                 Channel_ID__c, Activation_Date_Time__c, Scheme_Joined_Date_Time__c, Deactivation_Date_Time__c
                                 FROM Loyalty_Account__c];
        
        ApexPages.standardController controller = new ApexPages.standardController(con);
        myJL_MembershipDetailsExtension pge = new myJL_MembershipDetailsExtension(controller);
        myJL_MembershipDetailsExtension.ProfileWithLoyaltyAccount myJlReq = new myJL_MembershipDetailsExtension.ProfileWithLoyaltyAccount(con.id,con,LA);
        myJLReq.getMyJLNewRequest();
        
        Test.setCurrentPage(myJLReq.getMyJLNewRequest()); 
        
        ApexPages.standardController controller1 = new ApexPages.standardController(con);
        myJL_InformationPageController pge1 = new myJL_InformationPageController(controller1);
        
        
        System.assertEquals('/apex/myJL_MembershipDetails?contactId='+con.Id, pge1.goback().getUrl());
        System.assertEquals(true, pge1.goback().getRedirect());    
        pge1.cancelrequest();
        System.assertEquals(1, pge1.getCp.size());
        
        
        System.assertEquals('myJL Barcode Number', pge1.BarcodeLabel);
        System.assertEquals(MyJL_Const.PACK_TYPE_DIGITAL, pge1.voucherPreference);
        
        
        String returnedURL = '/apex/myJLRequestCreateCase?ccountry=United+Kingdom&conId=' + con.Id;
        returnedURL += '&jlCarNumber=TestCard&jlMemNumber=00000000012&laid=' + la.Id;
        returnedURL += '&retURL=%2F%2Fapex%2FmyJL_Information%3Fid%3D'+con.Id+'&scontrolCaching=1&sourcecontactid=' + con.Id;
        returnedURL += '&sourcecontactname=Last+Name';
        
        System.assertEquals(returnedURL, pge1.myjlnewrequest().getUrl());
    }
    
    
    @isTest
    static void testPartnershipCard() {
        
        Contact con = [SELECT Id, Name, Email FROM Contact];
        
        Loyalty_Account__c la = [SELECT Id, Name, Customer_Loyalty_Account_Card_ID__c, Email_Address__c, Voucher_Preference__c, IsActive__c,
                                 Channel_ID__c, Activation_Date_Time__c, Scheme_Joined_Date_Time__c, Deactivation_Date_Time__c
                                 FROM Loyalty_Account__c];
        
        CalloutHandlerTest.initCalloutSettings(MyJL_LoyaltyAccountHandler.FULL_JOIN_NOTIFY, 'MyJL_LoyaltyAccountHandler.JoinLeaveCallout', 10);
        
        Loyalty_Card__c card = new Loyalty_Card__c();
        card.Name = '1234567890123456';
        card.Card_Number__c = '1234567890123456';
        card.Loyalty_Account__c = la.Id;
        card.Card_Type__c = MyJL_Const.MY_PARTNERSHIP_CARD_TYPE;
        card.Date_Created__c = DateTime.now();
        card.Origin__c = 'TEST';
        card.Pack_Type__c = MyJL_Const.PACK_TYPE_DIGITAL;        
        insert card;
        
        ApexPages.standardController controller = new ApexPages.standardController(con);
        myJL_MembershipDetailsExtension pge = new myJL_MembershipDetailsExtension(controller);
        
        myJL_MembershipDetailsExtension.ProfileWithLoyaltyAccount myJlReq = new myJL_MembershipDetailsExtension.ProfileWithLoyaltyAccount(con.id,con,la);
        myJLReq.getMyJLNewRequest();
        
        Test.setCurrentPage(myJLReq.getMyJLNewRequest()); 
        
        ApexPages.standardController controller1 = new ApexPages.standardController(con);
        myJL_InformationPageController page = new myJL_InformationPageController(controller1);
        
        System.assertEquals('My Partnership Barcode Number', page.BarcodeLabel);
        System.assertEquals('1234567890123456', page.memNumber);  
        System.assertEquals(true, page.isPartnershipAccount);
    }
    
    @isTest
    static void CancelRequest() {
        Contact con = [SELECT Id, Name, Email FROM Contact];
        Loyalty_Account__c LA = [SELECT Id, Name, Customer_Loyalty_Account_Card_ID__c, Email_Address__c, Voucher_Preference__c, IsActive__c,
                                 Channel_ID__c, Activation_Date_Time__c, Scheme_Joined_Date_Time__c, Deactivation_Date_Time__c
                                 FROM Loyalty_Account__c];
        
        ApexPages.standardController controller = new ApexPages.standardController(con);
        myJL_MembershipDetailsExtension pge = new myJL_MembershipDetailsExtension(controller);
        myJL_MembershipDetailsExtension.ProfileWithLoyaltyAccount myJlReq = new myJL_MembershipDetailsExtension.ProfileWithLoyaltyAccount(con.id,con,LA);
        myJLReq.getMyJLNewRequest();
        
        Test.setCurrentPage(myJLReq.getMyJLNewRequest()); 
        
        ApexPages.standardController controller1 = new ApexPages.standardController(con);
        myJL_InformationPageController pge1 = new myJL_InformationPageController(controller1);
        
        pge1.goback();
        pge1.cancelrequest();
        
        Case ca =  UnitTestDataFactory.createNormalCase(con);
        
        
        Test.setCurrentPage(pge1.myjlnewrequest());
        ApexPages.standardController controller2 = new ApexPages.standardController(ca);
        myJLRequestCreateCaseExtension pge2 = new myJLRequestCreateCaseExtension(controller2);
        pge2.save();
        
        Test.startTest();
        List<Case> getcase = new list<Case>([SELECT id,despatch_status__c,status FROM Case]);
        List<MyJL_Outbound_Staging__c> getOutst = new list<MyJL_Outbound_Staging__c>([select id,Case_Reference_ID__c from MyJL_Outbound_Staging__c where Case_Reference_ID__c =:getcase[0].Id]);
        system.assertequals(1,getOutst.size());
        system.assertequals('Closed - No Response Required',getcase[0].status); 
        system.assertequals('Processing',getcase[0].despatch_status__c); 
        
        //cancel request
        ApexPages.standardController controller3 = new ApexPages.standardController(con);
        myJL_MembershipDetailsExtension pge3 = new myJL_MembershipDetailsExtension(controller3);
        myJL_MembershipDetailsExtension.ProfileWithLoyaltyAccount myJlReq3 = new myJL_MembershipDetailsExtension.ProfileWithLoyaltyAccount(con.id,con,LA);
        myJLReq3.getMyJLNewRequest();
        
        Test.setCurrentPage(myJLReq3.getMyJLNewRequest()); 
        
        ApexPages.standardController controller4 = new ApexPages.standardController(con);
        myJL_InformationPageController pge4 = new myJL_InformationPageController(controller4);
        ApexPages.currentPage().getParameters().put('requestCaseId',getcase[0].Id);
        pge4.cancelrequest();
        
        List<case> getcase1 = new list<case>([select id,despatch_status__c,status,case_request_cancelled__c from case where id =:getcase[0].Id]);
        
        List<MyJL_Outbound_Staging__c> getOutst1 = new list<MyJL_Outbound_Staging__c>([select id,Case_Reference_ID__c,case_request_cancelled__c from MyJL_Outbound_Staging__c where Case_Reference_ID__c =:getcase[0].Id]);
        
        system.assertequals(1,getOutst1.size());
        system.assertequals('Closed - Resolved',getcase1[0].status); 
        system.assertequals('Not Required',getcase1[0].despatch_status__c); 
        system.assertequals(true,getcase1[0].case_request_cancelled__c); 
        system.assertequals(true,getOutst1[0].case_request_cancelled__c); 
        Test.stopTest();
    }
    
    @isTest
    static void testMembershipDetailsForPartnershipCard(){
        
    }
    
    @isTest
    static void checkParametersInThePageRequest() {
        Contact customer = MyJL_TestUtil.createCustomer('Ordering_Customer');
        Loyalty_Account__c LA = new Loyalty_Account__c(Name='00000000012', Email_Address__c = customer.email, IsActive__c = true, ShopperId__c='shopper1');
        LA.Customer_Loyalty_Account_Card_ID__c = 'TestCard';
        LA.Channel_ID__c = 'johnlewis.com';
        LA.Contact__c = customer.id;
        LA.Activation_Date_Time__c = datetime.now();
        Insert LA;
        
        Loyalty_Card__c LC = new Loyalty_Card__c(Name = 'TestCard', Loyalty_Account__c = LA.Id);
        insert Lc;
        ApexPages.standardController controller = new ApexPages.standardController(customer);
        ApexPages.currentPage().getParameters().put(myJL_InformationPageController.LOYALTY_ACC_ID,LA.Id);
        
        myJL_InformationPageController pge = new myJL_InformationPageController(controller);
        
        PageReference pr= pge.myjlnewrequest();
        System.assertEquals(customer.Mailing_Street__c, pr.getParameters().get(myJL_InformationPageController.ADDRESS_LINE_1), 'Address line 1 shoould match ');
        System.assertEquals(customer.Mailing_Address_Line2__c, pr.getParameters().get(myJL_InformationPageController.ADDRESS_LINE_2), 'Address line 2 should match ');
        System.assertEquals(customer.Mailing_Address_Line3__c, pr.getParameters().get(myJL_InformationPageController.ADDRESS_LINE_3), 'Address line 3 should match ');
        System.assertEquals(customer.Mailing_Address_Line4__c, pr.getParameters().get(myJL_InformationPageController.ADDRESS_LINE_4), 'Address line 4 should match ');
    }
    
}