@istest
public class CustomTimelineLEXControllerTest {
    @isTest static void testCaseActivityTimelineFeatureBasicCheck() {
		
		User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User testUser;
        Case testCase;
        List<Case_Activity__c> testCaseActivity;
        Contact testContact;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;

            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        
        System.runAs(testUser) {

            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;

            testCase = CaseTestDataFactory.createCase(testContact.Id);
			testCase.Bypass_Standard_Assignment_Rules__c = true;
			insert testCase;
			system.assert([select Id from Case_Activity__c where Case__c = :testCase.Id].size() == 1);
            system.assert(CustomTimelineLEXController.getCountOfOpenActivities(testCase.Id) == 1);
			Test.startTest();
			
			testCaseActivity = new List<Case_Activity__c>();
            Case_Activity__c tempObj = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);

            tempObj.RecordTypeId = CaseActivityUtils.customerPromiseRTId;
            tempObj.Activity_Start_Date_Time__c = System.now().addHours(-5);
            tempObj.Activity_End_Date_Time__c = System.now().addHours(-3);
            tempObj.Activity_Completed_Date_Time__c = System.now().addHours(-1);
            tempObj.Resolution_Method__c = 'Customer contacted';
            testCaseActivity.add(tempObj);
            
            tempObj = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            tempObj.RecordTypeId = CaseActivityUtils.customerPromiseRTId;
            tempObj.Activity_Start_Date_Time__c = System.now();
            tempObj.Activity_End_Date_Time__c = System.now();
            tempObj.Activity_Completed_Date_Time__c = System.now();
            tempObj.Resolution_Method__c = 'Email sent to customer';
            testCaseActivity.add(tempObj);
            
            tempObj = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            tempObj.Activity_Date__c = System.today();
            tempObj.Activity_Start_Time__c = '08:00';
            tempObj.Activity_End_Date_Time__c = System.now() - 1;
            testCaseActivity.add(tempObj);
            
            tempObj = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            tempObj.Activity_Start_Date_Time__c = System.now() + 1;
            tempObj.Activity_End_Date_Time__c = System.now() + 1;
            testCaseActivity.add(tempObj);
            
            tempObj = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            tempObj.RecordTypeId = CaseActivityUtils.internalActionRTId;
            tempObj.Activity_Start_Date_Time__c = System.now();
            tempObj.Activity_End_Date_Time__c = System.now();
            tempObj.Activity_Completed_Date_Time__c = System.now();
            tempObj.Resolution_Method__c = 'No action required';
            testCaseActivity.add(tempObj);
            
            tempObj = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            tempObj.RecordTypeId = CaseActivityUtils.internalActionRTId;
            tempObj.Activity_Start_Date_Time__c = System.now() - 1;
            tempObj.Activity_End_Date_Time__c = System.now() - 1;
            testCaseActivity.add(tempObj);
            
            tempObj = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            tempObj.RecordTypeId = CaseActivityUtils.internalActionRTId;
            tempObj.Activity_Start_Date_Time__c = System.now() + 1;
            tempObj.Activity_End_Date_Time__c = System.now() + 1;
            testCaseActivity.add(tempObj);
            
            tempObj = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            tempObj.RecordTypeId = CaseActivityUtils.internalActionRTId;
            tempObj.Activity_Start_Date_Time__c = System.today();
            tempObj.Activity_End_Date_Time__c = System.today();
            testCaseActivity.add(tempObj);
            
            insert testCaseActivity;

			system.assert([select Id from Case_Activity__c].size() == 9);
            
            Map<String,List<CustomTimeline>> customTimelineMap = CustomTimelineLEXController.initializeTimeline(testCase.Id);
            system.assert(customTimelineMap.containsKey('activePromises') && customTimelineMap.get('activePromises').size()>0);
            
            system.assert(customTimelineMap.containsKey('completedPromises') && customTimelineMap.get('completedPromises').size()>0);
            
            
            CustomTimelineLEXController.completePromise(testCaseActivity[0].Id, 'Customer contacted');
            system.assert([select Id,Activity_Completed_Date_Time__c from Case_Activity__c where Id =:testCaseActivity[0].Id].get(0).Activity_Completed_Date_Time__c != NULL);
			Test.stopTest();

        }
	}
	
    @isTest static void testCaseReopenPromise() {
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User testUser;
        Case testCase;
        List<Case_Activity__c> testCaseActivity;
        Contact testContact;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;

            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        
        System.runAs(testUser) {
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;

            testCase = CaseTestDataFactory.createCase(testContact.Id);
			testCase.Bypass_Standard_Assignment_Rules__c = true;
			insert testCase;
            List<Case_Activity__c> caseActivityList = new List<Case_Activity__c>();
            caseActivityList = [select Id from Case_Activity__c where Case__c = :testCase.Id];
			system.assert(caseActivityList.size() == 1);
            system.assert(CustomTimelineLEXController.getCountOfOpenActivities(testCase.Id) == 1);
            update new Case_Activity__c(Id=caseActivityList[0].Id, Activity_Completed_Date_Time__c = System.now());
            system.assert(CustomTimelineLEXController.getCountOfOpenActivities(testCase.Id) == 0);
            testCase.Status = 'Closed - Resolved';
            testCase.jl_Case_RestrictedAddComment__c = 'Closing comments';
            update testCase;
            
			Test.startTest();
            Case caseToReopen = CaseReopenUtilities.reopenCase(testCase.Id);
            caseToReopen.jl_Case_RestrictedAddComment__c = 'Reopen comments';
            //update caseToReopen;
            caseActivityList = [select Id from Case_Activity__c where Case__c = :testCase.Id AND Customer_Promise_Type__c = 'Reopen Promise'];
			//system.assert(caseActivityList.size() == 1);
            //system.assert(CustomTimelineLEXController.getCountOfOpenActivities(testCase.Id) == 1);
            Test.stopTest();
        }
    }
    
    @isTest static void checkTransferCaseFromPencilTest() {
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User testUser;
        Case testCase;
        List<Case_Activity__c> testCaseActivity;
        Contact testContact;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;

            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        
        System.runAs(testUser) {
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;

            testCase = CaseTestDataFactory.createCase(testContact.Id);
			testCase.Bypass_Standard_Assignment_Rules__c = true;
			insert testCase;
            
            Case_Activity__c caseActivity = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.id);
            insert caseActivity;
            
			Test.startTest();
            List<Case_Activity__c> caseActivityList = [select Id from Case_Activity__c LIMIT 1];
            String caseActivityId = caseActivityList[0].Id;
            CustomTimelineLEXController.checkTransferCaseFromPencil(caseActivityId);
            Test.stopTest();
        }
    }
    @isTest static void testCaseCommentIsCreated() {
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User testUser;
        Case testCase;
        Case_Activity__c testCaseActivity;
        Contact testContact;
        String caseCommentText = 'test comment';
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;

            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        
        System.runAs(testUser) {

            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;

            testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            insert testCase;

            Test.startTest();

            CustomTimelineLEXController.createCaseComment(testCase.Id, caseCommentText);

            CaseComment createdCaseComment = [SELECT Id, ParentId, CommentBody FROM CaseComment WHERE ParentId = :testCase.Id][0];
            
            System.assertEquals(testCase.Id, createdCaseComment.ParentId, 'Ids should match');
            System.assert(createdCaseComment.CommentBody.contains(caseCommentText));

            Test.stopTest();

        }
    }
}