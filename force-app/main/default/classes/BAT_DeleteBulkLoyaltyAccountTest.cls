/* Description  : Test Class to Delete Loyalty Account records
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   29/03/2019        Vignesh Kotha                    Created                COPT-4282
*
*/
@IsTest
Public class BAT_DeleteBulkLoyaltyAccountTest {
    
    @IsTest
    Public static void testLoyaltyAccount() {
        contact c = new contact();
        c.FirstName = 'Vignesh';
        c.LastName='kotha';
        insert c;
        list<Loyalty_Account__c> loyaltylist = new list<Loyalty_Account__c>();
        for(integer counter =0; counter<300 ; counter++){
            Loyalty_Account__c la = new Loyalty_Account__c();
            la.Name = 'test'+counter;
            la.Contact__c = c.id;
            loyaltylist.add(la);
        }
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.Name = 'BAT_DeleteBulkLoyaltyAccount';   
        gDPR.GDPR_Class_Name__c = 'BAT_DeleteBulkLoyaltyAccount';
        gDPR.Record_Limit__c = '5000';
        try{
            insert loyaltylist;
            insert gDPR;
            system.assertEquals(300, loyaltylist.size());
        }
        Catch(exception e){
            system.debug('Exception Caught:'+e.getmessage());
        }
        Database.BatchableContext BC;
        BAT_DeleteBulkLoyaltyAccount obj = NEW BAT_DeleteBulkLoyaltyAccount();
        Test.startTest();
        Database.DeleteResult[] Delete_Result = Database.delete(loyaltylist, false);
        obj.start(BC);
        obj.execute(BC,loyaltylist); 
        obj.finish(BC);
        Test.stopTest();
    }
    
    @IsTest
    Public static void testDmlException_Delete()
    {
        contact c = new contact();
        c.FirstName = 'Vignesh';
        c.LastName='kotha';
        insert c;
        list<Loyalty_Account__c> conprofdata = new list<Loyalty_Account__c>();
        for(integer counter =0; counter<300 ; counter++){
            Loyalty_Account__c cp = new Loyalty_Account__c();
            cp.Name = 'test'+counter;
            cp.Contact__c = c.id;
            conprofdata.add(cp);
        }
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.Name = 'BAT_DeleteBulkLoyaltyAccount';   
        gDPR.GDPR_Class_Name__c = 'BAT_DeleteBulkLoyaltyAccount';
        gDPR.Record_Limit__c = '';
        try{
            insert conprofdata;
            insert gDPR;
            system.assertEquals(300, conprofdata.size());
        }
        Catch(exception e){
            system.debug('Exception Caught:'+e.getmessage());
        }
        Database.DeleteResult[] Delete_Result = Database.delete(conprofdata, false);
        DmlException expectedException;
        Test.startTest();
        try { 
            delete conprofdata;
        }
        catch (DmlException dException) {
            expectedException = dException; 
            system.assertNotEquals(NULL, expectedException);
        }
        Test.stopTest();
    }
    
    @IsTest
    Public static void testSchedule(){
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.Name = 'BAT_DeleteBulkLoyaltyAccount';   
        gDPR.GDPR_Class_Name__c = 'BAT_DeleteBulkLoyaltyAccount';
        gDPR.Record_Limit__c = '5000';
        Test.startTest();
        Insert gDPR;
        /* ScheduleBatchDeleteLoyaltyAccount obj = NEW ScheduleBatchDeleteLoyaltyAccount();
obj.execute(null);  */
        Test.stopTest();
    }
    @IsTest
    Public static void testTransactionDelete1(){
        List<Loyalty_Account__c> Loyaltydata = new List<Loyalty_Account__c>();
        for(Integer counter =0; counter<300 ; counter++){
            Loyalty_Account__c Lobject = new Loyalty_Account__c();
            Lobject.Name = 'test'+counter;
            Lobject.IsActive__c= true;
            Lobject.Activation_Date_Time__c = NULL;
            Lobject.createddate = System.now().addYears(-9);
            Lobject.Deactivation_Date_Time__c = System.now().addYears(-9);
            Lobject.Source_CreatedDate__c=System.now().addYears(-9); 
            Lobject.Contact__c = NULL; 
            Loyaltydata.add(Lobject);
        }
        insert Loyaltydata;        
        Loyalty_Card__c lCard = new Loyalty_Card__c();
        lCard.Card_Number__c = '9623683951828338';
        lCard.Loyalty_Account__c = Loyaltydata[0].Id;
        Insert lCard;              
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.GDPR_Class_Name__c = 'BAT_DeleteBulkLoyaltyAccount';
        gDPR.Name = 'BAT_DeleteBulkLoyaltyAccount';
        gDPR.Record_Limit__c = NULL; 
        try{
            Insert gDPR;
        }
        Catch(Exception e)
        {
            System.debug('Exception Caught:'+e.getmessage());
        }
        Test.startTest();
        Database.BatchableContext BC;
        BAT_DeleteBulkLoyaltyAccount obj=new BAT_DeleteBulkLoyaltyAccount();
        Database.DeleteResult[] Delete_Result = Database.delete(Loyaltydata, false);
        obj.start(BC);
        obj.execute(BC,Loyaltydata);
        obj.finish(BC);
        DmlException expectedException;
        Test.stopTest();
    }
}