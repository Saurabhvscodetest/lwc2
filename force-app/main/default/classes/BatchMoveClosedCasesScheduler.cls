global class BatchMoveClosedCasesScheduler implements Schedulable {

	global void execute(SchedulableContext sc) {

		BatchMoveClosedCasesFromOMNIQueue batchRun = new BatchMoveClosedCasesFromOMNIQueue ();
	
		// Set scope size to 50;
		database.executebatch(batchRun, 50);
	
	}

}