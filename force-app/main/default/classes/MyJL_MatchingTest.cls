//
// This is a exhaustive test of the Contact Profile matching code.
// It attempts all permutations of the matching keys against an existing contact. 
//  001  27/06/16    NA                  Decommissioning contact profile
//  002  26/0716     YM					 Changing the Matching Rules with respect to OCCO-19247  
@isTest
private class MyJL_MatchingTest {
    /*
	private static final String EXIST_FIRSTNAME = 'Gordon';
	private static final String EXIST_LASTNAME = 'Green';
	private static final String EXIST_EMAIL = 'gordon.green@kokua.co.edu';
	private static final String EXIST_ADDRESS1 = '1 Green Street, Hampton Green';
	private static final String EXIST_POSTCODE = 'GG34 8TR';
    private static final String EXIST_MOBILE   = '0123456789'; //<<002>>
	private static final String NEW_POSTFIX = 'new';
	private static final String NEW_FIRSTNAME = EXIST_FIRSTNAME + NEW_POSTFIX;
	private static final String NEW_LASTNAME = EXIST_LASTNAME + NEW_POSTFIX;
	private static final String NEW_EMAIL = EXIST_EMAIL + NEW_POSTFIX;
	private static final String NEW_ADDRESS1 = EXIST_ADDRESS1 + NEW_POSTFIX;
	private static final String NEW_POSTCODE = EXIST_POSTCODE + NEW_POSTFIX;

	private static void test(Integer testNum, Boolean firstName, Boolean lastName, Boolean email, Boolean address1, Boolean postCode, Boolean expectedMatch) {
		system.debug('test: '+testNum+ ' fn='+firstName+' ln='+lastName+' em='+email+' a1='+address1+' pc='+postCode+' em='+expectedMatch);
        // Create the Contact 
		account c = new account();
		c.name = EXIST_FIRSTNAME+' '+EXIST_LASTNAME;     //<<001>>
		c.email__c = EXIST_EMAIL;     //<<001>>
		c.Contact_Matching_Key_1__c = EXIST_FIRSTNAME.toLowerCase()+EXIST_LASTNAME.toLowerCase()+EXIST_EMAIL.toLowerCase();     //<<001>>
		//Start - <<002>>
		
		String initial = (EXIST_FIRSTNAME.substring(0,1)).toLowerCase();
		String emailString = (email == true) ? EXIST_EMAIL : NEW_EMAIL; 
		c.Contact_Matching_Key_2__c = initial +EXIST_LASTNAME.toLowerCase()+emailString.toLowerCase() +EXIST_MOBILE+ EXIST_POSTCODE.toLowerCase();     //<<001>>
		//End - <<>002>
		insert c;
		
		// Create the Contact Profile
		contact cp = new contact();     //<<001>>
		cp.firstname = (firstName == true) ? EXIST_FIRSTNAME : NEW_FIRSTNAME;     //<<001>>
		cp.lastname = (lastName == true) ? EXIST_LASTNAME : NEW_LASTNAME;     //<<001>>
		cp.email = (email == true) ? EXIST_EMAIL : NEW_EMAIL;     //<<001>>
		cp.mailingstreet = (address1 == true) ? EXIST_ADDRESS1 : NEW_ADDRESS1;     //<<001>>
		cp.mailingpostalcode = (postCode == true) ? EXIST_POSTCODE : NEW_POSTCODE;     //<<001>>

		system.debug('cp: '+cp);
		insert cp;
		
		// Now check wether you got a new contact
		List<account> accountlist = [SELECT Id FROM account];     //<<001>>
		List<contact> contactProfileList = [SELECT Id, accountid FROM contact];     //<<001>>
		system.assertEquals(1, contactProfileList.size());

		if (expectedMatch) {
			// should attach Contact Profile to existing Contact
			system.assertEquals(1, accountlist.size());     //<<001>>
			system.assertEquals(c.Id, contactProfileList[0].accountid);     //<<001>>
		} else {
			// should create a new contact
			system.assertEquals(2, accountlist.size());     //<<001>>
			// contact profile should not lookup to initial contact
			system.assertNotEquals(c.Id, cp.accountid);     //<<001>>
			// should point at the other contact
			account c1 = accountlist[0];     //<<001>>
			account c2 = accountlist[1];
			if (c1.Id == c.Id) {     //<<001>>
				system.assertEquals(contactProfileList[0].accountid, c2.Id);     //<<001>>
			} else {
				system.assertEquals(contactProfileList[0].accountid, c1.Id);		     //<<001>>		
			}
		}
	}

	private static void testX(Integer testNum, Boolean expectedMatch) {
		Integer x = 0;
		
		Boolean firstName = false;
		Boolean lastName = false;
		Boolean email = false;
		Boolean address1 = false;
		Boolean postCode = false;
		
		system.debug('testNum1: '+testNum);
		if (testNum > 15) {
			postCode = true;
			//-- <<002>>
			if(testNum==27) {
				email = true;
			}
			//-- <<002>>
			testNum -= 16;
		}
		system.debug('testNum2: '+testNum);
		if (testNum > 7) {
			address1 = true;
			testNum -= 8;
		}
		system.debug('testNum3: '+testNum);
		if (testNum > 3) {
			email = true;
			testNum -= 4;
		}
		system.debug('testNum4: '+testNum);
		if (testNum > 1) {
			lastName = true;
			testNum -= 2;
		}
		system.debug('testNum5: '+testNum);
		if (testNum > 0) {
			firstName = true;
		}

		test(testNum, firstName, lastName, email, address1, postCode, expectedMatch);
	}

	//
	// Tests. Should only match when FirstName, LastName, Email match or
	// FirstName, LastName, Address1m Post Code match
	//
	// Flags are test FLEAP (least significant bit to most significant bit)
	// F(irstName), L(astName) E(mail) A(ddress) P(ostCode)
	//
	// So masks that should map are xx111 and 11x11 
	//
	// testX() method needs the testNum and whether you think a match will be found
	// 
	// Most should NOT match!
    private static testMethod void test00000() {
		testX(0, false);
    }
    private static testMethod void test00001() {
		testX(1, false);
    }
    private static testMethod void test00010() {
		testX(2, false);
    }
    private static testMethod void test00011() {
		testX(3, false);
    }
    private static testMethod void test00100() {
		testX(4, false);
    }
    private static testMethod void test00101() {
		testX(5, false);
    }
    private static testMethod void test00110() {
		testX(6, false);
    }
    private static testMethod void test00111() {
    	// FirstName, LastName and email match
		testX(7, true);
    }
    private static testMethod void test01000() {
		testX(8, false);
    }
    private static testMethod void test01001() {
		testX(9, false);
    }
    private static testMethod void test01010() {
		testX(10, false);
    }
    private static testMethod void test01011() {
		testX(11, false);
    }
    private static testMethod void test01100() {
		testX(12, false);
    }
    private static testMethod void test01101() {
		testX(13, false);
    }
    private static testMethod void test01110() {
		testX(14, false);
    }
    private static testMethod void test01111() {
    	// FirstName, LastName and email match
		testX(15, true);
    }
    private static testMethod void test10000() {
		testX(16, false);
    }
    private static testMethod void test10001() {
		testX(17, false);
    }
    private static testMethod void test10010() {
		testX(18, false);
    }
    private static testMethod void test10011() {
		testX(19, false);
    }
    private static testMethod void test10100() {
		testX(20, false);
    }
    private static testMethod void test10101() {
		testX(21, false);
    }
    private static testMethod void test10110() {
		testX(22, false);
    }
    private static testMethod void test10111() {
    	// FirstName, LastName and email match
		testX(23, true);
    }
    private static testMethod void test11000() {
		testX(24, false);
    }
    private static testMethod void test11001() {
		testX(25, false);
    }
    private static testMethod void test11010() {
		testX(26, false);
    }
    private static testMethod void test11011() {
		// FirstName, LastName, Address1,PostCode match
		testX(27, true);
    }
    private static testMethod void test11100() {
		testX(28, false);
    }
    private static testMethod void test11101() {
		testX(29, false);
    }
    private static testMethod void test11110() {
		testX(30, false);
    }
    private static testMethod void test11111() {
    	// FirstName, LastName and email match or FirstName, LastName, Address1,PostCode match
		testX(31, true);
    }
    */
}