global class ScheduleBatchDeleteSupersededCustomers implements Schedulable{
   global void execute(SchedulableContext sc){
        BAT_DeleteSupersededCustomers obj = new BAT_DeleteSupersededCustomers();
        Database.executebatch(obj);
    }   
}