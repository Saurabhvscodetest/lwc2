public class AssignCaseBackToQueue {

    @AuraEnabled
    public static boolean assignQueueServer(String artId) {
        try {
            List<Case> caseList= [ SELECT OwnerId FROM Case WHERE Id =: artId ];
            String ownerIdCheck = caseList[0].OwnerId;
        
        	if(ownerIdCheck.startsWithIgnoreCase('005')) {
            	return false;
        	}
        	else {
            	return true;
        	}  
        }
        catch(Exception e) {
            System.debug('@@@ Error Inside assignQueueServer' + e.getMessage());
            return false;
        }
    }
    
    @AuraEnabled
    public static boolean assignQueueToCase(String artNewId) {
        
        List<String> oldQueueName = new List<String>();
        List<String> oldQueueNameNew = new List<String>();
        
        for(CaseHistory ch: [ SELECT NewValue, OldValue FROM CaseHistory WHERE CaseId =: artNewId AND field = 'owner' ORDER BY CreatedDate Desc ]) { 
            oldQueueName.add(String.valueOf(ch.OldValue));
        }        
        for(String str : oldQueueName) {
            if(str.startsWithIgnoreCase('00G')) {
                oldQueueNameNew.add(str);
            }
        }
        
        String assignQueue = oldQueueNameNew[0];
        Case cas = new Case();
        List<Case> updateCaseList = new List<Case>();
        
        try {    
        	cas.Id = artNewId;
        	if(assignQueue.startsWithIgnoreCase('00G')) {
                cas.OwnerId = assignQueue;
                updateCaseList.add(cas);
                update updateCaseList;    
            }
            else {
                return false;
            }
        	return true;
        }
        catch(Exception e) {
            System.debug('@@@ Error Inside assignQueueToCase' + e.getMessage());
        	return false;
        }
    }
}