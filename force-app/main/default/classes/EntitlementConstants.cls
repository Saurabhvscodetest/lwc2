public class EntitlementConstants {

	public static final String BOLT_CUSTOMER_SEGMENTATION = 'A';
	public static final String BOLT_ENTITLEMENT_SUFFIX = ' - BOLT';
	public static final String SEPARATOR = '_';

	public static final String DEFAULT_QUEUE = 'DEFAULT';
	public static final String ACTIONED_QUEUE = 'ACTIONED';
    //Commented as a part of COPT-3585
	//public static final String WARNING_QUEUE = 'WARNING';
	public static final String FAILED_QUEUE = 'FAILED';

	public static final String NORMAL_PRIORITY = 'Normal';
	public static final String ACTIONED_PRIORITY = 'Actioned';
	public static final String MEDIUM_PRIORITY = 'Medium';
	public static final String HIGH_PRIORITY = 'High';
	public static final String IN_FLIGHT_CASES_UPDATE_PERMISSION_SET = 'In_Flight_Cases_Update';
    public static final Boolean USER_HAS_IN_FLIGHT_CASES_UPDATE_PERMISSION_SET =  CommonStaticUtils.isPermissionSetAssignedToSingleUserByName(UserInfo.getUserId(), IN_FLIGHT_CASES_UPDATE_PERMISSION_SET);	

	//Commented as a part of COPT-3585
	//public static final Set<String> QUEUE_TYPES = new Set<String>{DEFAULT_QUEUE,ACTIONED_QUEUE, WARNING_QUEUE, FAILED_QUEUE};
    public static final Set<String> QUEUE_TYPES = new Set<String>{DEFAULT_QUEUE,ACTIONED_QUEUE,FAILED_QUEUE};
    //Commented as a part of COPT-3585
	//public static final Set<String> RELATED_QUEUE_TYPES = new Set<String>{ACTIONED_QUEUE, WARNING_QUEUE, FAILED_QUEUE};
    public static final Set<String> RELATED_QUEUE_TYPES = new Set<String>{ACTIONED_QUEUE, FAILED_QUEUE};
	public static final Set<String> PRIORITIES = new Set<String>{NORMAL_PRIORITY, ACTIONED_PRIORITY, MEDIUM_PRIORITY, HIGH_PRIORITY};


	public static final Map<String, String> QUEUE_TO_PRIORITY_MAP = new Map<String, String> { 
																		DEFAULT_QUEUE 	=> MEDIUM_PRIORITY,
																		ACTIONED_QUEUE 	=> ACTIONED_PRIORITY,
																		FAILED_QUEUE	=> HIGH_PRIORITY
																		};


	public static final Map<String, String> PRIORITY_TO_QUEUE_MAP = new Map<String, String> { 
																		MEDIUM_PRIORITY 	=>	DEFAULT_QUEUE, 
																		ACTIONED_PRIORITY 	=>	ACTIONED_QUEUE,
																		HIGH_PRIORITY 		=>	FAILED_QUEUE
																		};


	public static final String CUSTOMER_CONTACTED = 'Customer contacted';
	public static final String CUSTOMER_NOT_CONTACTED_STATUS = 'No customer contact required';


}