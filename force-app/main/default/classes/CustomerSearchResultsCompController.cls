/******************************************************************************
* @author       Shion Abdillah
* @date         14 Jul 2016
* @description  Controller class for Customer Search Results Components.
*
*
* LOG	DATE			AUTHOR	JIRA		COMMENT      
* 001	28/06/2016		SA			        Original code

*
*******************************************************************************
*/
public class CustomerSearchResultsCompController extends DataTablePaginator { 

	//Pagination Data members		
	private static final String DEFAULT_SORT_FIELD = 'LastName';	
	private static final Integer DEFAULT_PAGE_SIZE = 50;
	private Set<Id> selectedContactIds = new Set<Id>();		
	public List<RowItem> ContactRows {get;set;}	
	public Integer totalRecordCount {get; private set;}		
    public String searchCriteria {get; set;}     
    private String previousSearchCriteria = null;
	
	//Collection Data members 
	private List<Contact> contactCollectionLoc;
	
	public List<Contact> ContactCollection{
		set{			
                        
			contactCollectionLoc = value;
            if (String.isEmpty(previousSearchCriteria) || String.isEmpty(searchCriteria) || !searchCriteria.equals(previousSearchCriteria)) {
                system.debug('==============================ContactCollection');
                previousSearchCriteria  = searchCriteria;
            setCon = null;
			if(contactCollectionLoc != null){				
            	fullDataList = contactCollectionLoc;
            }
            else{
                fullDataList =  new List<SObject>();
            }
			pageSize = 20;
			totalRecordCount = fullDataList.size();	
			System.debug('fullDataList: ' + fullDataList);				
		}
        }
		get{
			return contactCollectionLoc;
		}
	}
	
    /**
     * No Args Constructor
     * @params: None
     * @rtnval: N/A
     */
	public CustomerSearchResultsCompController() {
        pageSize = 5;
        noOfRecords = 0;

	}



	public class RowItem {
		public Contact ContactRec {get; private set;}
		
		
		public RowItem (Contact c) {
			this.ContactRec = c;
		}		
			
				
	}
	
	
	public List<RowItem> getContacts() {
		if(ContactCollection != null){
			
			ContactRows = new List<RowItem>();
			for (Contact c : (List<Contact>)setCon.getRecords()) {
                RowItem row = new RowItem(c);
				ContactRows.add(row);
			}
			
		}
		noOfRecords = ContactRows.size();
		System.debug('ContactCollection is : ' + ContactCollection);
		return ContactRows;
    }


    /**
     * Return an integer value of the number of pages needed to display all selected records
     * based on page size.
     * @params: None
     * @rtnval: Integer     Total number of pages
     */
    public Integer getTotalPages(){
        Decimal totalSize = setCon.getResultSize();
        Decimal pageSize = setCon.getPageSize();
        Decimal pages = totalSize/pageSize;
        return (Integer)pages.round(System.RoundingMode.CEILING);
    }

   
}