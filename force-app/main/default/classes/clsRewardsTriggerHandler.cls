/**
* Author:       Nawaz Ahmed
* Date:         15/09/2015
* Description:  Trigger handler class for  Rewards Object
*
* ******************* Change Log *******************
* Modified by       Change Date     Change
* Nawaz Ahmed	     13/08/2015     Initial Version (MJLP-331)
* 
*
*/
public with sharing class clsRewardsTriggerHandler {
	
		//Variable to hold the caseids
	public static set<id> caseids = new set<id>();
	public static set<string> uniqIds = new set<string>();
	public static map<string,id> parentidtouniqid = new map<string,id>();
	public static list<Rewards__c> updaterew = new list<Rewards__c>();
	
	public static void mainEntry(Boolean isBefore, 
                               Boolean isDelete, Boolean isAfter, 
                               Boolean isInsert, Boolean isUpdate, 
                               Boolean isExecuting, List<Rewards__c> newlist, 
                               Map<ID,SObject> newmap, List<Rewards__c> oldlist, 
                               Map<Id,SObject> oldmap){
                               	
     	if(isBefore && isInsert){
     		
     		//if case reference is null set te Mailing type to New welcome pack
     		for(Rewards__c getrew:newlist){
     			if(getrew.Case_Reference_ID__c == null){
     				getrew.Mailing_Type__c = 'New Welcome Pack';
     				if(getrew.MYJL_Membership_Number__c!=null){
     					getrew.Unique_Identifier__c = getrew.MYJL_Membership_Number__c+'|'+'New Welcome Pack';
     				}
     			}
     			else{
     				caseids.add(getrew.Case_Reference_ID__c);	
     				
     			}
     		}
     		
     		//Get the case details with request type
     		Map<id,case> casemap = new map<id,case>( [select id,request_type__c from case where id in:caseids]);
     		
     		//If Case reference is not null get the mailing type from the case
     		for(Rewards__c getrew:newlist){
     			if(getrew.Case_Reference_ID__c != null){
     				getrew.Mailing_Type__c = casemap.get(getrew.Case_Reference_ID__c).request_type__c;	
     				getrew.Unique_Identifier__c = getrew.Case_Reference_ID__c+'|'+getrew.MYJL_Membership_Number__c+'|'+casemap.get(getrew.Case_Reference_ID__c).request_type__c;
     			}	
     		}
     	}
     	
     	else if(isafter && isinsert){
     		
     		//Retreive the unique identifiers
     		for(Rewards__c getrew:newlist){
     			if(getrew.Unique_Identifier__c!=null){
     				uniqIds.add(getrew.Unique_Identifier__c);	
     			}	
     		}
     		
     		//Retreive the parent objects
     		for(Direct_Mail_Update__c getdm:[select id,unique_identifier__c from Direct_Mail_Update__c where unique_identifier__c in:uniqIds and isdeleted=false]){
     			parentidtouniqid.put(getdm.unique_identifier__c,getdm.id);	
     		}
     		
     		
     		//update the parent for rewards object
     		for(Rewards__c getrew:[select id,unique_identifier__c,Direct_Mail_Update__c from Rewards__c where id in:newmap.keyset()]){
     			if(parentidtouniqid.containskey(getrew.unique_identifier__c)){
     				getrew.Direct_Mail_Update__c = 	parentidtouniqid.get(getrew.unique_identifier__c);
     				updaterew.add(getrew);
     			}
     		}
     		
     		//Update the rewards with parent attached.
     		if(updaterew.size() > 0){
     			update updaterew;
     		}
     		
     	}    	                           	
                               	
    }	

}