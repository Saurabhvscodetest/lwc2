public class EntitlementManagerController {

    public static Set<String> originalvalues = new Set<String>();
    public static final String SHOW_EXISTING_ENTITLEMENT_TEAMS = 'SHOW EXISTING ENTITLEMENT TEAMS';
    public static final String ATLEAST_SELECT_ONE_TEAM = 'At least one team must be selected to perfrom action';
    public static final String SUCCESS_MESSAGE = 'The Selected teams actions were success';
    public static final String FAILURE_MESSAGE = 'The Selected teams actions had failed ,  Please check the logs for the errors';
    public static final String UPDATE_MESSAGE = 'All entitlements have been successfully updated';
    public static Boolean createRealtedQueues { get; set; }
    public List<string> leftselected{get;set;}
    public List<string> rightselected{get;set;}
    @TestVisible Set<string> leftvalues = new Set<string>();
    @TestVisible Set<string> selectedTeamNames = new Set<string>();
    
    static{
        buildAvailableTeams();
    }
    
    public static void buildAvailableTeams(){
        originalvalues.addall(Team__c.getAll().keySet());
        Config_Settings__c config = Config_Settings__c.getInstance(SHOW_EXISTING_ENTITLEMENT_TEAMS);
        boolean showExistingEntitlementTeams = (config != null && config.Checkbox_Value__c) ? true: false;
        List<Entitlement> entitlementList = new List<Entitlement> ([SELECT Name FROM Entitlement]);
        system.debug('entitlementList' + entitlementList);
        Set<String> existingEntitlementSet = new Set<String>();
        for(Entitlement ent: entitlementList) {
            existingEntitlementSet.add(ent.Name);
        }
        if(!showExistingEntitlementTeams){
            originalvalues.removeAll(existingEntitlementSet);
        }
    }
    
    public EntitlementManagerController(){
        leftselected = new List<String>();
        rightselected = new List<String>();
        leftvalues.addAll(originalValues);
    }
      
    public PageReference selectclick(){
        rightselected.clear();
        for(String s : leftselected){
            leftvalues.remove(s);
            selectedTeamNames.add(s);
        }
        return null;
    }
     
    public PageReference unselectclick(){
        leftselected.clear();
        for(String s : rightselected){
            selectedTeamNames.remove(s);
            leftvalues.add(s);
        }
        return null;
    }
 
    public List<SelectOption> getunSelectedValues(){
        List<SelectOption> options = new List<SelectOption>();
        List<string> tempList = new List<String>();
        tempList.addAll(leftvalues);
        tempList.sort();
        for(string s : tempList) {
            options.add(new SelectOption(s,s));
        }
        return options;
    }
 
    public List<SelectOption> getSelectedValues(){
        List<SelectOption> options1 = new List<SelectOption>();
        List<string> tempList = new List<String>();
        tempList.addAll(selectedTeamNames);
        tempList.sort();
        for(String s : tempList) {
            options1.add(new SelectOption(s,s));
        }
        return options1;
    }
    
    public void assignEntitlementsAndCreateRealtedQueues() {
        if(selectedTeamNames.isempty()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, ATLEAST_SELECT_ONE_TEAM));
            return;
        }
        try{
            EntitlementConfigManager.buildEntitlementsAndRelatedQueues(selectedTeamNames, createRealtedQueues);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,SUCCESS_MESSAGE ));
        } catch ( Exception exp){
            System.debug(exp.getMessage() + '- ' + exp.getStackTraceString());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, FAILURE_MESSAGE ));
        } 
        
    }

    public void resetData() {
        ApexPages.getMessages().clear();
        buildAvailableTeams();
        
        leftselected = new List<String>();
        rightselected = new List<String>();
        leftvalues.addAll(originalValues);
        selectedTeamNames = new Set<string>();
    }

    public void updateExistingEntitlementRecords(){
        
        List<Entitlement> entitlementList = new List<Entitlement> ([SELECT Name FROM Entitlement]);
        Set<String> existingEntitlementSet = new Set<String>();
        
        for(Entitlement ent: entitlementList) {
            existingEntitlementSet.add(ent.Name);
        }
        
        EntitlementConfigManager.updateEntitlements(existingEntitlementSet);
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, UPDATE_MESSAGE));
        resetData(); 
    }
    

}