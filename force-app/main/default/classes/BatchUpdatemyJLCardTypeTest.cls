@isTest
private class BatchUpdatemyJLCardTypeTest {
    @testSetup 
    static void setup() {
        List<Loyalty_Account__c> Laccount = new List<Loyalty_Account__c>();
        List<Loyalty_Card__c> Lcard = new List<Loyalty_Card__c>();
        
        Contact con =  UnitTestDataFactory.createContact();
        Insert con;
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        
        for(Integer i = 0 ; i < 20 ; i++){
            
            Loyalty_Account__c la = new Loyalty_Account__c(Name='00000000012', Email_Address__c = 'harry.hoppo@animals.org', IsActive__c = true, ShopperId__c='shopper1');
            la.Customer_Loyalty_Account_Card_ID__c = 'TestCard';
            la.Channel_ID__c = 'johnlewis.com';
            la.ShopperId__c = con.id;
            la.Voucher_Preference__c = MyJL_Const.PACK_TYPE_DIGITAL;
            la.Activation_Date_Time__c = datetime.now();
            Laccount.add(la);
        } 
        insert Laccount;
        
        for (Loyalty_Account__c Lacc : [select id from Loyalty_Account__c]) {
            Loyalty_Card__c LC = new Loyalty_Card__c(Name = 'TestCard', Loyalty_Account__c = Lacc.Id);
            Lcard.add(Lc);    
        }
        insert Lcard;
    }

    @isTest
    static void StartBatch() {        
        Test.startTest();
        BatchUpdateMyJlCardType ucard = new BatchUpdateMyJlCardType('',false);
        Id batchId = Database.executeBatch(ucard);
        Test.stopTest();
        
        System.assertEquals(20, [select count() from Loyalty_Card__c where Card_type__c = 'My John Lewis']);

    }
    
     @isTest
    static void RollbackBatch() {        
        Test.startTest();
        BatchUpdateMyJlCardType ucard1 = new BatchUpdateMyJlCardType('My John Lewis',true);
        Id batchId1 = Database.executeBatch(ucard1);
        Test.stopTest();
        
        System.assertEquals(20, [select count() from Loyalty_Card__c where Card_type__c = '']);

    }
    
}