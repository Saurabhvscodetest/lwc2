/*
** TestPostChatController
**
** Test class for PostChatController
**
**	Edit	Date		Author			Comment
**	001		10/09/14	NTJ				Original
**	002		11/09/14	NTJ				Reinstate use of LiveTranscript chatKey
**	003		30/10/14	NTJ				Remove PostChat page reference. Use ChatWindow
*/
@isTest
private class TestPostChatController {
	
	private static LiveChatVisitor createVisitor() { 
		LiveChatVisitor lcv = new LiveChatVisitor();
		
		insert lcv;
		return lcv;
	}

    private static LiveChatTranscript createTranscript(String chatKey) {
    	LiveChatTranscript lct = new LiveChatTranscript();
    	if (chatKey != null) {
    		//lct.Client_GUID__c = chatKey;
    		lct.ChatKey = chatKey;
    	}
    	LiveChatVisitor lcv = createVisitor();
    	lct.LiveChatVisitorId = lcv.Id; 
    	
    	return lct;
    }


    static testMethod void test() {
		TestChatCreateCustomSettings.createCustomSettings(true);
		LiveChatTranscript lct = createTranscript(TestChatCreateCustomSettings.TEST_CHAT_KEY);
		insert lct;
		
		// set page and parameters
		Test.setCurrentPage(Page.ChatWindow);
		ApexPages.currentPage().getParameters().put('chatKey',TestChatCreateCustomSettings.TEST_CHAT_KEY);
		
		PostChatController pcc = new PostChatController();
		system.assertNotEquals(null, pcc);		

		List<SelectOption> answers = pcc.getAnswers();
		system.assertNotEquals(null, answers);
		system.assertEquals(4, answers.size());
		system.assertEquals(TestChatCreateCustomSettings.A4, answers[3].getValue());
		system.assertEquals(TestChatCreateCustomSettings.A3, answers[2].getValue());
		system.assertEquals(TestChatCreateCustomSettings.A2, answers[1].getValue());
		system.assertEquals(TestChatCreateCustomSettings.A1, answers[0].getValue());
		system.assertEquals(TestChatCreateCustomSettings.A4, answers[3].getLabel());
		system.assertEquals(TestChatCreateCustomSettings.A3, answers[2].getLabel());
		system.assertEquals(TestChatCreateCustomSettings.A2, answers[1].getLabel());
		system.assertEquals(TestChatCreateCustomSettings.A1, answers[0].getLabel());		

		// try some answers
		pcc.answer1 = TestChatCreateCustomSettings.A4;

		// difficult to test the result as it's random (literally)
		Boolean bOk = pcc.showSurvey;
		pcc.errorText = 'No agents available.';
		bOk = pcc.showSurvey;
		//system.assertEquals(false, bOk);
		
		pcc.submitSurvey();
		
		pcc.surveyCompleted = true;
		pcc.submitSurvey2();
	}

	static testMethod void doDone() {
		TestChatCreateCustomSettings.createCustomSettings(true);
		
		PostChatController pcc = new PostChatController();
		pcc.donePostChat();
	}

    static testMethod void test40Percent() {
    	TestChatCreateCustomSettings.createCustomSettings(true);
    	
		PostChatController pcc = new PostChatController();

		// how many times we want to do the test
		Double iterations = 10;
		// the test needs 100 calls as we are doing a percentage test
		Double loops = iterations * 100;
		// The SURVEY_LIMIT is something like 33% or 40% so if we have 10 iterations we expect 400 TRUE results
    	Double expected = PostChatController.SURVEY_LIMIT * iterations;
		// We need some tolerance as this test is not going to work, let's say 10% of the expected, e.g. 40
		Double twentyPercent = expected * 0.2;
		// lower bound will be 400 - 40 = 36
		Double lb = expected - twentyPercent;
		// upper bound will be 400 + 40 = 440
		Double ub = expected + twentyPercent;
		// This is the counter that records how many we see
    	Double achieved = 0;
    	
    	for (Integer i=0; i < loops; i++) {
    		if (pcc.doMaths()) {
    			achieved++;
    		}
    	}
    	
    	system.debug('loops: '+loops+' achieved: '+achieved+' lb: '+lb+' ub: '+ub);
    	// the answer should be somewhere between + or - 10%
    	system.assert(lb < achieved);
    	system.assert(ub > achieved);
    }
    
    static testmethod void testShowSurveyDisabled() {
    	// disable the survey
    	TestChatCreateCustomSettings.createCustomSettings(false);
		// create a controller
		PostChatController pcc = new PostChatController();
		// the show survey should be false, as custom setting says so
		system.assertEquals(false, pcc.showSurvey);
		
    }

    static testmethod void testShowSurveyEnabledButError() {
    	// enable the survey
    	TestChatCreateCustomSettings.createCustomSettings(true);
		// create a controller
		PostChatController pcc = new PostChatController();
		// set an error
		pcc.errorText = 'Error!';
		// the show survey should be false, as error
		system.assertEquals(false, pcc.showSurvey);		
    }
    
    static testmethod void testMisc() {
    	TestChatCreateCustomSettings.createCustomSettings(true);
		// create a controller
		PostChatController pcc = new PostChatController();
		
		system.assertEquals(null, pcc.yesToSurvey());
		
		system.assertNotEquals(null, pcc.declineSurvey());		    	
		system.assertNotEquals(null, pcc.saveChat());		    	
		system.assertNotEquals(null, pcc.backToChatWindow());		    	
    }
    
    static testmethod void testCreateSurveyForUrl() {
    	TestChatCreateCustomSettings.createCustomSettings(true);
		// create a survey
		PostChatController.createSurveyForUrl('url');
		List<Survey__c> sList = [SELECT Id FROM Survey__c];
		system.assertEquals(1, sList.size());
    }
    
    static testmethod void testCreateSurveyWithGUID() {
    	TestChatCreateCustomSettings.createCustomSettings(true);
		// create a survey
		PostChatController.createSurveyWithGUID('guid-guid-guid');
		List<Survey__c> sList = [SELECT Id,Status__c FROM Survey__c];
		system.assertEquals(1, sList.size());
		system.assertEquals('Not Surveyed', sList[0].Status__c);
		
		// create a controller
		PostChatController pcc = new PostChatController();
		// set the surveyId
		pcc.surveyId = sList[0].Id;
		// update
		pcc.updateSurvey1(true);
		List<Survey__c> sList2 = [SELECT Id,Status__c FROM Survey__c];
		system.assertEquals(1, sList2.size());
		system.assertEquals('Survey Completed', sList2[0].Status__c);
    }
    
    // now some scenario tests    
	static testmethod void testSurveyCreatedFirstSurveyCompleted() {
		final String GUID = 'abcd-efgh-ijkl-mnop-1234';

		// add the custom settings
    	TestChatCreateCustomSettings.createCustomSettings(true);

		// create a survey via a remote action
		PostChatController.createSurveyWithGUID(GUID);
		List<Survey__c> sList = [SELECT Id,Status__c FROM Survey__c];
		system.assertEquals(1, sList.size());

		// create a controller
		PostChatController pcc = new PostChatController();
		
		// complete the survey
		pcc.surveyId = sList[0].Id;
		pcc.guidChatKey = GUID;
		pcc.surveyCompleted = true;
		pcc.answer1 = TestChatCreateCustomSettings.A1;
		pcc.submitSurvey2();
		
		// now create the transcript
		LiveChatTranscript lct = createTranscript(GUID);
		insert lct;
		system.assertNotEquals(null, lct.Id);
		
		// this should mean that the Survey is connected to the Transcript and the answer is present in the Transcript
		List<LiveChatTranscript> lctList = [SELECT Id, Survey__c, Rating_1__c, Answer_1__c, Survey_Status__c FROM LiveChatTranscript];
		system.assertEquals(lctList.size(),1);
		system.assertEquals(sList[0].Id, lctList[0].Survey__c);
		system.assertEquals(TestChatCreateCustomSettings.A1_RATING, lctList[0].Rating_1__c);
		system.assertEquals(TestChatCreateCustomSettings.A1, lctList[0].Answer_1__c);
		system.assertEquals('Survey Completed', lctList[0].Survey_Status__c);
				
	}
	
	static testmethod void testSurveyCreatedFirstSurveyDeclined() {
		final String GUID = 'abcd-efgh-ijkl-mnop-1234';

		// add the custom settings
    	TestChatCreateCustomSettings.createCustomSettings(true);

		// create a survey via a remote action
		PostChatController.createSurveyWithGUID(GUID);
		List<Survey__c> sList = [SELECT Id,Status__c FROM Survey__c];
		system.assertEquals(1, sList.size());

		// create a controller
		PostChatController pcc = new PostChatController();
		
		// complete the survey
		pcc.surveyId = sList[0].Id;
		pcc.guidChatKey = GUID;
		pcc.surveyCompleted = false;
		pcc.submitSurvey2();
		
		// now create the transcript
		LiveChatTranscript lct = createTranscript(GUID);
		insert lct;
		system.assertNotEquals(null, lct.Id);
		
		// this should mean that the Survey is connected to the Transcript and the answer is present in the Transcript
		List<LiveChatTranscript> lctList = [SELECT Id, Survey__c, Rating_1__c, Answer_1__c, Survey_Status__c FROM LiveChatTranscript];
		system.assertEquals(lctList.size(),1);
		system.assertEquals(sList[0].Id, lctList[0].Survey__c);
		system.assertEquals(0, lctList[0].Rating_1__c);
		system.assertEquals(null, lctList[0].Answer_1__c);
		system.assertEquals('Survey Declined', lctList[0].Survey_Status__c);
	}
	
	static testmethod void testTranscriptCreatedFirstSurveyCompleted() {
		final String GUID = 'abcd-efgh-ijkl-mnop-1234';

		// add the custom settings
    	TestChatCreateCustomSettings.createCustomSettings(true);

		// create a survey via a remote action
		PostChatController.createSurveyWithGUID(GUID);
		List<Survey__c> sList = [SELECT Id,Status__c FROM Survey__c];
		system.assertEquals(1, sList.size());

		// create a controller
		PostChatController pcc = new PostChatController();
		
		// create the transcript
		LiveChatTranscript lct = createTranscript(GUID);
		insert lct;
		system.assertNotEquals(null, lct.Id);

		// now complete the survey
		pcc.surveyId = sList[0].Id;
		pcc.guidChatKey = GUID;
		pcc.surveyCompleted = true;
		pcc.answer1 = TestChatCreateCustomSettings.A1;
		pcc.submitSurvey2();
				
		// this should mean that the Survey is connected to the Transcript and the answer is present in the Transcript
		List<LiveChatTranscript> lctList = [SELECT Id, Survey__c, Rating_1__c, Answer_1__c, Survey_Status__c FROM LiveChatTranscript];
		system.assertEquals(lctList.size(),1);
		system.assertEquals(sList[0].Id, lctList[0].Survey__c);
		system.assertEquals(TestChatCreateCustomSettings.A1_RATING, lctList[0].Rating_1__c);
		system.assertEquals(TestChatCreateCustomSettings.A1, lctList[0].Answer_1__c);
		system.assertEquals('Survey Completed', lctList[0].Survey_Status__c);
	}

	static testmethod void testTranscriptCreatedFirstSurveyDeclined() {
		final String GUID = 'abcd-efgh-ijkl-mnop-1234';

		// add the custom settings
    	TestChatCreateCustomSettings.createCustomSettings(true);

		// create a survey via a remote action
		PostChatController.createSurveyWithGUID(GUID);
		List<Survey__c> sList = [SELECT Id,Status__c FROM Survey__c];
		system.assertEquals(1, sList.size());

		// create a controller
		PostChatController pcc = new PostChatController();
		
		//  create the transcript
		LiveChatTranscript lct = createTranscript(GUID);
		insert lct;
		system.assertNotEquals(null, lct.Id);

		// now complete the survey
		pcc.surveyId = sList[0].Id;
		pcc.guidChatKey = GUID;
		pcc.surveyCompleted = false;
		pcc.submitSurvey2();
		
		
		// this should mean that the Survey is connected to the Transcript and the answer is present in the Transcript
		List<LiveChatTranscript> lctList = [SELECT Id, Survey__c, Rating_1__c, Answer_1__c, Survey_Status__c FROM LiveChatTranscript];
		system.assertEquals(lctList.size(),1);
		system.assertEquals(sList[0].Id, lctList[0].Survey__c);
		system.assertEquals(0, lctList[0].Rating_1__c);
		system.assertEquals(null, lctList[0].Answer_1__c);
		system.assertEquals('Survey Declined', lctList[0].Survey_Status__c);
	}          
}