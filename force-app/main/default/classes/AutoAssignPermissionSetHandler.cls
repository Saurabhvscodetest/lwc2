/******************************************************************************
* @author       Matt Povey
* @date         07/12/2015
* @description  Handler class containing all logic relating to Auto_Assign_Permission_Set__c 
*				updates from triggers.
*
* EDIT RECORD
*
* LOG     DATE        Author	JIRA		COMMENT      
* 001     07/12/2015  MP		COPT-466		Original code
*
*******************************************************************************
*/
public without sharing class AutoAssignPermissionSetHandler {

    public static final String AAPS_ROLE_NAME_PREFIX = 'ROLE: ';
    public static final String AAPS_PROFILE_NAME_PREFIX = 'PROFILE: ';

    /**
     * mainEntry method called from the Trigger.
	 * @params:	Standard Trigger context variables. 
     */
    public static void mainEntry(Boolean isBefore, 
                               Boolean isDelete, Boolean isAfter, 
                               Boolean isInsert, Boolean isUpdate, 
                               Boolean isExecuting, List<Auto_Assign_Permission_Set__c> newList, 
                               Map<ID, Object> inNewMap, List<Auto_Assign_Permission_Set__c> oldList, 
                               Map<Id, Object> inOldMap) {

		Map<ID, Auto_Assign_Permission_Set__c> newMap = (Map<ID, Auto_Assign_Permission_Set__c>) inNewMap;
		Map<ID, Auto_Assign_Permission_Set__c> oldMap = (Map<ID, Auto_Assign_Permission_Set__c>) inOldMap;
		
		if (isBefore && (isInsert || isUpdate)) {
			Map<String, Auto_Assign_Permission_Set__c> aapsNameXrefMap = new Map<String, Auto_Assign_Permission_Set__c>();
			for (Auto_Assign_Permission_Set__c aaps : newList) {
				if (aapsNameXrefMap.containsKey(aaps.Name)) {
					aaps.addError('Duplicate name found in DML list: ' + aaps.Name);
				} else {
					aapsNameXrefMap.put(aaps.Name, aaps);
				}
			}
			List<Auto_Assign_Permission_Set__c> existingAAPSNameDuplicates = [SELECT Id, Name FROM Auto_Assign_Permission_Set__c WHERE Name IN :aapsNameXrefMap.keySet()];
			if (!existingAAPSNameDuplicates.isEmpty()) {
				for (Auto_Assign_Permission_Set__c aaps : existingAAPSNameDuplicates) {
					if (aapsNameXrefMap.containsKey(aaps.Name) && (isInsert || aaps.Id != aapsNameXrefMap.get(aaps.Name).Id)) {
						aapsNameXrefMap.get(aaps.Name).Name.addError('An Auto Assign Permission Set record with this name already exists');
					}
				}
			}
		}
		
		if (isAfter && (isInsert || isUpdate || isDelete)) {
        	Set<String> roleNamesToSelect = new Set<String>();
        	Set<String> profileNamesToSelect = new Set<String>();
			Map<String, String> dbxPermissionsToRemoveMap = new Map<String, String>();
			List<Auto_Assign_Permission_Set__c> aapsListToUse = newList;

			if (isDelete) {
				aapsListToUse = oldList;
			}

			for (Auto_Assign_Permission_Set__c aaps : aapsListToUse) {
				if (isInsert) {
					// If inserting a new record, simple update on all users will suffice.
					addAutoAssignProfileOrRoleToSelectSets(roleNamesToSelect, profileNamesToSelect, aaps.Name);
				} else if (isUpdate) {
					// For updates, need to establish permissions to remove.  Update will add any newly inserted permissions.
					Auto_Assign_Permission_Set__c oldAAPS = oldMap.get(aaps.Id);
					if (oldAAPS.Permission_Sets_To_Assign__c != aaps.Permission_Sets_To_Assign__c) {
						addAutoAssignProfileOrRoleToSelectSets(roleNamesToSelect, profileNamesToSelect, aaps.Name);
						if (oldAAPS.Permission_Sets_To_Assign__c != null) {
							List<String> oldPSNames = oldAAPS.Permission_Sets_To_Assign__c.split(';');
							Set<String> newPSNames;
							if (aaps.Permission_Sets_To_Assign__c != null) {
								newPSNames = new Set<String> (aaps.Permission_Sets_To_Assign__c.split(';'));
							} else {
								newPSNames = new Set<String>();
							}
							for (String oldPSName : oldPSNames) {
								if (!newPSNames.contains(oldPSName)) {
									// add to aapsPermissionsToRemoveMap
									if (dbxPermissionsToRemoveMap.containsKey(aaps.Name)) {
										String dbxString = dbxPermissionsToRemoveMap.get(aaps.Name);
										dbxString += ';' + oldPSName;
										dbxPermissionsToRemoveMap.put(aaps.Name, dbxString);
									} else {
										dbxPermissionsToRemoveMap.put(aaps.Name, oldPSName);
									}
								}
							}
						}
					}
				} else if (isDelete) {
					// For deletes, add all permissions to remove list and perform update (only with doing if Permission_Sets_To_Assign__c not blank).
					if (aaps.Permission_Sets_To_Assign__c != null) {
						addAutoAssignProfileOrRoleToSelectSets(roleNamesToSelect, profileNamesToSelect, aaps.Name);
						dbxPermissionsToRemoveMap.put(aaps.Name, aaps.Permission_Sets_To_Assign__c);
					}
				}
			}
			
			// In the event of any inserts or changes to Permission Set lists update users
			if (!roleNamesToSelect.isEmpty() || !profileNamesToSelect.isEmpty()) {
				if (Test.isRunningTest()) {
					clsUserTriggerHandler.aapsAssignPermissionSets(roleNamesToSelect, profileNamesToSelect, dbxPermissionsToRemoveMap);
				} else {
					clsUserTriggerHandler.aapsFutureAssignPermissionSets(roleNamesToSelect, profileNamesToSelect, dbxPermissionsToRemoveMap);
				}
			}
		}   
	}
	
    /**
     * Method to set a single permission set to Add or Keep depending on whether already assigned.
	 * @params:	Set<Id> roleNamesToSelect		Set of Role names to be used in User SOQL 
	 *			Set<Id> profileNamesToSelect	Set of Profile names to be used in User SOQL
	 *			String aapsName					The Auto_Assign_Permission_Set__c record Name (containing the Role/Profile name)
     */
	private static void addAutoAssignProfileOrRoleToSelectSets(Set<String> roleNamesToSelect, Set<String> profileNamesToSelect, String aapsName) {
		if (aapsName.startsWithIgnoreCase(AAPS_PROFILE_NAME_PREFIX)) {
			String profileName = aapsName.substringAfter(AAPS_PROFILE_NAME_PREFIX);
			profileNamesToSelect.add(profileName.trim());
		} else if (aapsName.startsWithIgnoreCase(AAPS_ROLE_NAME_PREFIX)) {
			String roleName = aapsName.substringAfter(AAPS_ROLE_NAME_PREFIX);
			roleNamesToSelect.add(roleName.trim());
		}
	}
}