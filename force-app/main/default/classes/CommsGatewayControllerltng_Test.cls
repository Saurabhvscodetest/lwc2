@isTest
public class CommsGatewayControllerltng_Test {
    @isTest
    Public static void TestMethod1(){
        // insert contact
        UnitTestDataFactory.createConfigSettings();
        Contact testContact;
        testContact = UnitTestDataFactory.createContact();
        insert testContact;
        // insert case
        Case newCSTCase;
        newCSTCase = UnitTestDataFactory.createCase(testContact.Id);
        newCSTCase.Bypass_Standard_Assignment_Rules__c = true; 
        newCSTCase.JL_Branch_master__c = 'Oxford Street';
        newCSTCase.Contact_Reason__c = 'Pre-Sales';
        newCSTCase.Reason_Detail__c = 'Buying Office Enquiry';
        newCSTCase.Status = 'Active - Follow up required';
        newCSTCase.JL_Action_Taken__c = 'Customer contacted';
        insert newCSTCase;
        // insert Order Number
        Comms_Gateway_Constants__c ordernum = new Comms_Gateway_Constants__c();
        ordernum.Name='ORDER_NUMBER';
        ordernum.Value__c = '';
                
        Comms_Gateway_Constants__c CusSet1 = new Comms_Gateway_Constants__c();
        CusSet1.Name = 'MESSAGE_EMAIL_ADDRESS_LENGTH_INVALID';
        CusSet1.Value__c = 'Email Address must not contain more than';
        insert CusSet1;
        
        Comms_Gateway_Constants__c CusSet2 = new Comms_Gateway_Constants__c();
        CusSet2.Name = 'EMAIL_ADDRESS_LENGTH';
        CusSet2.Value__c = '255';
        insert CusSet2;
        
        Comms_Gateway_Constants__c CusSet3 = new Comms_Gateway_Constants__c();
        CusSet3.Name = 'MESSAGE_PHONE_NO_LENGTH_INVALID_PREFIX';
        CusSet3.Value__c = 'MESSAGE_PHONE_NO_LENGTH_INVALID_PREFIX';
        insert CusSet3;
        
        Comms_Gateway_Constants__c CusSet4 = new Comms_Gateway_Constants__c();
        CusSet4.Name = 'MESSAGE_PHONE_NO_LENGTH_INVALID_SUFFIX';
        CusSet4.Value__c = 'MESSAGE_PHONE_NO_LENGTH_INVALID_SUFFIX';
        insert CusSet4;
        
        Comms_Gateway_Constants__c CusSet5 = new Comms_Gateway_Constants__c();
        CusSet5.Name = 'PHONE_NUMBER_MIN_LENGTH';
        CusSet5.Value__c = '10';
        insert CusSet5;
        
        Comms_Gateway_Constants__c CusSet6 = new Comms_Gateway_Constants__c();
        CusSet6.Name = 'PHONE_NUMBER_MAX_LENGTH';
        CusSet6.Value__c = '15';
        insert CusSet6;
        
        Comms_Gateway_Constants__c CusSet7 = new Comms_Gateway_Constants__c();
        CusSet7.Name = 'NO_COMMUNICATION_FOUND_ARCHIVE';
        CusSet7.Value__c = 'NO_COMMUNICATION_FOUND_ARCHIVE';
        insert CusSet7;
        
        Comms_Gateway_Constants__c CusSet8 = new Comms_Gateway_Constants__c();
        CusSet8.Name = 'MESSAGE_PROVIDE_ONLY_ONE_INPUT';
        CusSet8.Value__c = 'MESSAGE_PROVIDE_ONLY_ONE_INPUT';
        insert CusSet8;
        
        Comms_Gateway_Constants__c CusSet9 = new Comms_Gateway_Constants__c();
        CusSet9.Name = 'NO_COMMUNICATION_FOUND';
        CusSet9.Value__c = 'NO_COMMUNICATION_FOUND';
        insert CusSet9;
        
        Comms_Gateway_Constants__c CusSet10 = new Comms_Gateway_Constants__c();
        CusSet10.Name = 'STATUS_MORE_RECORDS';
        CusSet10.Value__c = 'STATUS_MORE_RECORDS';
        insert CusSet10;
        
        Comms_Gateway_Constants__c CusSet11 = new Comms_Gateway_Constants__c();
        CusSet11.Name = 'TAG_BODY';
        CusSet11.Value__c = 'TAG_BODY';
        insert CusSet11;
        
        Comms_Gateway_Constants__c CusSet12 = new Comms_Gateway_Constants__c();
        CusSet12.Name = 'TAG_RETRIEVE_RESPONSE';
        CusSet12.Value__c = 'RetrieveResponseMsg';
        insert CusSet12;
        
        Comms_Gateway_Constants__c CusSet13 = new Comms_Gateway_Constants__c();
        CusSet13.Name = 'TAG_OVERALL_STATUS';
        CusSet13.Value__c = 'TAG_OVERALL_STATUS';
        insert CusSet13;
        
        Comms_Gateway_Constants__c CusSet14 = new Comms_Gateway_Constants__c();
        CusSet14.Name = 'TAG_RESULTS';
        CusSet14.Value__c = 'TAG_RESULTS';
        insert CusSet14;
        
        Comms_Gateway_Constants__c CusSet15 = new Comms_Gateway_Constants__c();
        CusSet15.Name = 'TAG_PROPERTIES';
        CusSet15.Value__c = 'TAG_PROPERTIES';
        insert CusSet15;
        
        Comms_Gateway_Constants__c CusSet16 = new Comms_Gateway_Constants__c();
        CusSet16.Name = 'SEND_LOG_ARCHIVE_DATA_EXTENSION';
        CusSet16.Value__c = 'SEND_LOG_ARCHIVE_DATA_EXTENSION';
        insert CusSet16;
        
        Comms_Gateway_Constants__c CusSet17 = new Comms_Gateway_Constants__c();
        CusSet17.Name = 'SEND_LOG_DATA_EXTENSION';
        CusSet17.Value__c = 'SEND_LOG_DATA_EXTENSION';
        insert CusSet17;
        
        Exact_Target_Callout_Settings__c CusSet18 = new Exact_Target_Callout_Settings__c();
        CusSet18.Endpoint__c = 'https://webservice.s6.exacttarget.com/Service.asmx';
        CusSet18.Username__c='CONNEX_API_USER';
        CusSet18.Content_Type__c='Content-type';
        CusSet18.Content_Type_Text_or_XML__c='text/xml';
        CusSet18.Http_Method__c='POST';
        CusSet18.Password__c='3Nq?F^8yr5G@7*jS$D';
        CusSet18.Security_Token__c='0e5e13d4-0c7d-4cab-b296-2f5a6c1e98a2';
        CusSet18.Target_Namespace__c='http://exacttarget.com/wsdl/partnerAPI';
        
        insert CusSet18;
        
        List<DataExtensionObject_DO> lst = new   List<DataExtensionObject_DO>();
        
         ExactTargetServiceHelper ext =  new ExactTargetServiceHelper();
          ExactTargetServiceHelper.ResponseInfo ext1 =  new ExactTargetServiceHelper.ResponseInfo();
          ExactTargetServiceMock exactTargetMock = new ExactTargetServiceMock(ExactTargetServiceMock.INPUT_ORDER_NUMBER);
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, exactTargetMock);
        CommsGatewayControllerltng.fetchCustomerData(testContact.id);
        CommsGatewayControllerltng.fetchCustomerData(newCSTCase.id);
        List<DataExtensionObject_DO> WebSer = CommsGatewayControllerltng.searchDelivery('','','0123456789','');
        Test.stopTest();
    }
}