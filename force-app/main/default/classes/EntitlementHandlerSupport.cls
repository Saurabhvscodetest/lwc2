public class EntitlementHandlerSupport {

	public static Boolean ENTITLEMENTS_DISABLED; 
	public static Boolean BOLT_ENTITLEMENTS_DISABLED;
	public static Map<String, Entitlement> entitlementNameMap;
	public static Map<Id, Contact> relatedContacts;
	public static Map<Id, Id> caseIdToContactIdMap;
    public static Set<Id> casesWithClosedNADInThisExecution = new Set<Id>(); // TODO: Can we remove with proper execution control?

	/*** FILTER HELPERS ***/

	/**
	* @description	Business logic to filter Cases which need to have their Milestones recalculated with an @future call			
	* @author		@tomcarman
	* @param		oldCases  Map of Trigger.Old Cases (optional)
	* @param		newCases  List of Trigger.New Cases
	*/

	public static void filterCasesWhichNeedToHaveMilestonsRecalculated(Map<Id, Case> oldCases, List<Case> newCases) {

		addCasesToHaveMilestonesRecalculated(newCases);
	}

	/**
	* @description	Business logic to filter Cases which need to have their Entitlement reevaluated (eg. assign one, change existing to a different one)			
	* @author		@tomcarman
	* @param		oldCases  Map of Trigger.Old Cases (optional)
	* @param		newCases  List of Trigger.New Cases			
	*/

	public static List<Case> filterCasesWhichNeedEntitlementEvaluated(Map<Id, Case> oldCases, List<Case> newCases) {

		List<Case> filteredCases = new List<Case>();

		// Rule 1: This is an Insert, all Cases need to be evaluated
		if(oldCases == null) {
			filteredCases.addAll(newCases); 
		// Rule 2: If the Owner has changed, need to re-evaluate Entitlement
		} else {
			for(Case newCase : newCases) {
				if(oldCases.containsKey(newCase.Id)) {
					boolean  userVal = EntitlementConstants.USER_HAS_IN_FLIGHT_CASES_UPDATE_PERMISSION_SET;
					Case oldCase = oldCases.get(newCase.Id);
					boolean isOwnerNotChanged = (oldCase.OwnerId ==  newCase.OwnerId);
					//if the user has the in flight cases update add the case to the filtered list
					//Also for inflight case if its reopened and the if its assigned to the same owner , we need to apply the entitlement if it not got one already
					if((EntitlementConstants.USER_HAS_IN_FLIGHT_CASES_UPDATE_PERMISSION_SET) || (oldCase.OwnerId != newCase.OwnerId) 
							|| (isOwnerNotChanged && newCase.jl_Reopen_As_User__c && (oldcase.EntitlementId != null))) {
						filteredCases.add(newCase);
					}
				}
			}
		}

		return filteredCases;
	}

	/*** LOGIC HELPERS ***/

	/**
	* @description	Business logic to identify if this Case should be considered as BOLT			
	* @author		@tomcarman
	* @param		newCase  A case to be evaluated
	* @return		Boolean
	*/
	public static Boolean isBoltCase(Case newCase) {

		Boolean isBoltCustomer = false;

		if(newCase.ContactId != null && relatedContacts.containsKey(newCase.ContactId)) { // If they have a related Contact
			Contact relatedContact = relatedContacts.get(newCase.ContactId);
			if(relatedContact.Customer_Segmentation__c != null &&
				relatedContact.Customer_Segmentation__c.equalsIgnoreCase(EntitlementConstants.BOLT_CUSTOMER_SEGMENTATION)) { // And the Contact.Customer_Segmentation__c is Bolt
				isBoltCustomer = true;
			}
		}

		return isBoltCustomer;

	}

	/**
	* @description	Business logic to identify if this Case is being updated from a Bulk Transfer context			
	* @author		@tomcarman
	* @param		oldCase  the old Case
	* @param		oldCase  the new Case
	* @return		Boolean of it is a bulk transfer
	*/

	public static Boolean isBulkTransfer(Case oldCase, Case newCase) {
		return oldCase.Bulk_Transfer_Date__c != newCase.Bulk_Transfer_Date__c;
	}


	/**
	* @description	Business logic to determine if the First Contact has been made			
	* @author		@tomcarman
	* @param		newCase  A Case to check
	* @return		Boolean
	*/

	public static Boolean customerHasHadContact(Case newCase) {
		if(!newCase.Status.contains('Closed') && 	
				(EntitlementConstants.CUSTOMER_CONTACTED.equalsIgnoreCase(newCase.jl_Action_Taken__c) 
				|| EntitlementConstants.CUSTOMER_NOT_CONTACTED_STATUS.equalsIgnoreCase(newCase.jl_Action_Taken__c))) {

			return true;

		} else {

			return false;
		}
	}

	/**
	* @description	Business logic to determine if a new NAD has been set, and the old NAD Milestone should be closed			
	* @author		@tomcarman
	* @param		oldCase  The Case from Trigger.Old
	* @param 		newCase  The Case from Trigger.New
	* @return	 	Boolean of if a new NAD has been set
	*/

	public static Boolean newNextActionDateHasBeenSet(Case oldCase, Case newCase) {

		Boolean returnVal = false;
		if(!newCase.status.contains('Closed')) { // If Case is still open
			if(oldCase.JL_Next_Response_By__c != newCase.JL_Next_Response_By__c) { // If the NAD has been changed
				if(!casesWithClosedNADInThisExecution.contains(newCase.Id)) { // If hasnt been processed in this transaction TODO: Can we remove?
					returnVal = true;
				}
			}
		}

		return returnVal;

	}

	/** READABILITY HELPERS **/

	/**
	* @description	Overloaded method to add the Id of either a Case, List of Cases to the set of Ids which will have their Milestones recalculated by future call.
	* @author		@tomcarman
	* @param		aCase  The case to evaluate
	* @param		cases  List of cases to evaluate
	*/

	private static void addCasesToHaveMilestonesRecalculated(Case aCase) {
		addCasesToHaveMilestonesRecalculated(new List<Case>{aCase});
	}

	private static void addCasesToHaveMilestonesRecalculated(List<Case> cases) {
	
		for(Case aCase : cases) {
			if(EntitlementHandler.FIRST_EXECUTION) {
				EntitlementHandler.casesForRecalculating_first.add(aCase.Id);
			} else {
				if (!EntitlementHandler.casesForRecalculating_first.contains(aCase.Id)) {
					EntitlementHandler.casesForRecalculating_second.add(aCase.Id);
				}
			}
		}
	}

	/*** DATA RETRIEVE HELPERS ***/

	/**
	* @description	Business logic to check if Entitlements are globally enabled		
	* @author		@tomcarman
	* @return		Boolean
	*/

	public static Boolean entitlementsDisabled() {

		if(ENTITLEMENTS_DISABLED == null) {
			Config_Settings__c entitlementDisabledCustomSetting = Config_Settings__c.getInstance('ENTITLEMENTS_DISABLED') ;

			if(entitlementDisabledCustomSetting != null) {
				ENTITLEMENTS_DISABLED = entitlementDisabledCustomSetting.Checkbox_Value__c;
			} else {
				ENTITLEMENTS_DISABLED = false;
			}
		}

		return ENTITLEMENTS_DISABLED;

	}

	/**
	* @description	Business logic to check if BOLT Entitlements are globally enabled		
	* @author		@tomcarman
	* @return		Boolean
	*/

	public static Boolean boltEntitlementsDisabled() {

		if(BOLT_ENTITLEMENTS_DISABLED == null) {

			Config_Settings__c boltEntitlementDisabledCustomSetting = Config_Settings__c.getInstance('ENTITLEMENTS_BOLT_DISABLED') ;

			if(boltEntitlementDisabledCustomSetting != null) {
				BOLT_ENTITLEMENTS_DISABLED = boltEntitlementDisabledCustomSetting.Checkbox_Value__c;
			} else {
				BOLT_ENTITLEMENTS_DISABLED = false;
			}
		}

		return BOLT_ENTITLEMENTS_DISABLED;

	}

	/**
	* @description	Creates a map of the Case OwnerId (could be User or Queue), to its Team__c object (if User, User.Primary_Team__c, if Queue, Queue.Name)
	* @author		@tomcarman
	* @param		newCases List of Cases
	* @return		Map of Id to Team__c
	*/

	public static Map<Id, Team__c> getOwnerToTeamMap(List<Case> newCases) {

		Map<Id, Team__c> ownerToTeamMap = new Map<Id, Team__c>();

		Set<Id> queueOwnerIdSet = new Set<Id>();
		Set<Id> userOwnerIdSet = new Set<Id>();

		for(Case newCase : newCases) {

			// Create set of Queue Owner and User Owner Ids
			if(CommonStaticUtils.isGroupId(newCase.OwnerId)) {
				queueOwnerIdSet.add(newCase.OwnerId);
			} else if (CommonStaticUtils.isUserId(newCase.OwnerId)) {
				userOwnerIdSet.add(newCase.OwnerId);
			}
		}

		ownerToTeamMap.putAll(TeamUtils.getTeamForQueueIds(queueOwnerIdSet));
		ownerToTeamMap.putAll(TeamUtils.getTeamForUserIds(userOwnerIdSet));

		return ownerToTeamMap;

	}
	
	/**
	* @description	Retrieves the Contact which is related to the supplied Cases (Case.ContactId) and stores in class level static map relatedContacts
	* @author		@tomcarman
	* @param		newCases List of Cases
	*/

	public static void getReleatedContacts(List<Case> newCases) {

		caseIdToContactIdMap = new Map<Id, Id>();

		for(Case aCase : newCases) {
			if(aCase.ContactId != null) {
				caseIdToContactIdMap.put(aCase.Id, aCase.ContactId);
			}
		}

		relatedContacts = new Map<Id, Contact>([SELECT Id, Customer_Segmentation__c FROM Contact WHERE Id IN :caseIdToContactIdMap.values()]);

	}

	/**
	* @description	Retrieves the Entitlement.Id based on supplied Team name
	* @author		@tomcarman
	* @param		teamName String of Team name
	* @return		Id of Entitlement record
	*/

	public static Id getEntitlementId(String teamName) {

		if(getEntitlement(teamName) != null) {
			return getEntitlement(teamName).Id;
		} else {
			return null;
		}
	}

	/**
	* @description	Retrieves the Entitlement based on supplied Team name
	* @author		@tomcarman
	* @param		teamName String of Team name
	* @return		Entitlement record
	*/
	
	public static Entitlement getEntitlement(String teamName) {

		if(entitlementNameMap == null) {

			entitlementNameMap = new Map<String, Entitlement>();
			for(Entitlement entitlement : [SELECT Id, Name, BusinessHoursId FROM Entitlement]) {
				entitlementNameMap.put(entitlement.Name.toLowerCase(), entitlement);
			}
		}
		if(entitlementNameMap.containsKey(teamName.toLowerCase())) {
            system.debug('Saurabh Yadav----------->'+entitlementNameMap);
			return entitlementNameMap.get(teamName.toLowerCase());
		} else {
			return null;
		}
	}

	/* OTHER */

	/**
	* @description	Method to store the Trigger.NewMap Case values in a static variable in the MilestoneTriggerTimeCalculator (otherwise only oldValues available)
	* @author		@tomcarman
	* @param		newCases 	Map of Trigger.newMap Cases
	*/

	public static void cacheNewCaseTriggerValuesForMilestoneCalculator(Map<Id, Case> newCases) {
		if(newCases != null) {
			MilestoneTriggerTimeCalculator.newCaseTriggerValues.putAll(newCases);
		}
	}
}