/*
* @File Name   : BAT_UpdateClienTellingData
* @Description : Batch class used to fetch the contact records from the database based on the field - GDPR_Data_Deletion__c
* @Copyright   : Zensar
* @Jira #      : #4277
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0       16-April-19        Ragesh G                     Created
*/
global class ScheduleBatchToUpdateCTData implements Schedulable{
   global void execute(SchedulableContext sc){
        BAT_UpdateClienTellingData obj = new BAT_UpdateClienTellingData();
        Database.executebatch(obj);
    }   
}