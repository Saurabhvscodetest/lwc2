@isTest
private class BatchMoveClosedCasesFromOMNIQueueTest {
	
	// Constants
	private static Integer NUMBER_OF_CASES_TO_CREATE = 5;


	@testSetup static void initTestData() {
		MilestoneUtilsTest.initEntitlementTestData();
		// Turn on email logging
		Config_Settings__c enableEmailLogging = new Config_Settings__c();
		enableEmailLogging.Name = 'Enable Email Logging for Batch';
		enableEmailLogging.Checkbox_Value__c = true;
		enableEmailLogging.Number_Value__c = 1;
		insert enableEmailLogging;


	}

/**
	* @description	Main test - Moves the closed cases from OMNI queue to Admin user
	*/

	@isTest static void testClosedCasesGetsClearedFromOMNI_Queue() {

		Config_Settings__c loggingSettings = [SELECT Id FROM Config_Settings__c WHERE Name = 'Enable Email Logging for Batch'];
		loggingSettings.Number_Value__c = 2;
		update loggingSettings;

		User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		User cstUser;
		Contact testContact;
		List<Case> casesList = new List<Case>();

		System.runAs(runningUser) {

			cstUser = UnitTestDataFactory.getFrontOfficeUser('CSTUSER');
			cstUser.Team__c = MilestoneUtilsTest.HAMILTON_CST_ENTITLEMENT;
			insert cstUser;

			testContact = UnitTestDataFactory.createContact();
			insert testContact;

		}

		System.runAs(cstUser) {

			for(Integer i = 0; i < NUMBER_OF_CASES_TO_CREATE; i++) {
				casesList.add(createCase());
			}

			insert casesList;
			
			for(case caseObj : casesList) {
				caseObj.description = 'DUMMY UPDATE';
				caseObj.jl_Transfer_case__c = true;
			}
			
			update casesList;
			
		}

		BatchMoveClosedCasesFromOMNIQueue batchJob = null;
		Test.startTest(); 
			batchJob = new BatchMoveClosedCasesFromOMNIQueue();
			Database.executeBatch(batchJob);
		Test.stopTest();

		/*for(Case aCase : [SELECT Id, OwnerId FROM Case]) {
			System.assertEquals(BatchMoveClosedCasesFromOMNIQueue.ADMIN_USER_ID, aCase.OwnerId, 'Case must be assigned to Admin User');
		}*/
	}


	@isTest static void testScheduler() {

		Test.startTest();

			String CRON_EXP = '0 0 0 15 3 ? 2022';
			String jobId = System.schedule('Test', CRON_EXP, new BatchMoveClosedCasesScheduler());

		Test.stopTest();

		System.assertNotEquals(null, jobId);
	}

	private static Case createCase() {

		Case newCase = UnitTestDataFactory.createCase(UnitTestDataFactory.createContact().Id);
		newCase.Bypass_Standard_Assignment_Rules__c = true;
		newCase.JL_Branch_master__c = 'Oxford Street';
		newCase.Contact_Reason__c = 'Pre-Sales';
		newCase.Reason_Detail__c = 'Buying Office Enquiry';
		newCase.Status = 'Closed - Resolved';
		
		return newCase;

	}
    
}