public class NewCustomerContactLExController {
    @AuraEnabled 
    public static Map<String,List<String>> fieldDependencyMap { get; set; }
	@AuraEnabled 
	public static List<Contact> getCustomerResults(String searchTerm){
        system.debug('search Term'+searchTerm);
		String escapedSearchString = String.escapeSingleQuotes(searchTerm);
        String searchStringWithWildcards = '*' + escapedSearchString + '*';
	    List<Contact> contactCollection = new List<Contact>();
	        string DynamicSOSL = 'FIND \'' + searchStringWithWildcards + '\' IN ALL FIELDS RETURNING Contact(Customer_Record_Type_Lex__c,RecordType.DeveloperName,Name,Salutation,Email,HomePhone,MobilePhone,MailingPostalCode,MailingStreet,MailingCity,LastModifiedDate WHERE Primary_Record__c = \'Yes\' ORDER BY Customer_Record_Type_Lex__c ASC LIMIT 200)';
	        List<List<sObject>> queryResults = search.query(DynamicSOSL);
	        if (queryResults.size() > 0){
	            List<sObject> results = queryResults[0];
	            for(sObject item : results){
	                Contact castItem = (Contact)item;
	                contactCollection.add(castItem);
	            }
	        }
        return contactCollection;
    }
    
    @AuraEnabled
    public static List<String> initiateLevelOnePicklist(){
        List<String> returnList = new List<String>();
        List<Schema.PicklistEntry> level_1_ple = Schema.getGlobalDescribe().get('Task').getDescribe().fields.getMap().get('Level_1__c').getDescribe().getPicklistValues();
        for(Schema.PicklistEntry obj : level_1_ple){
            returnList.add((String)obj.getValue());
        }
        return returnList;
    }
    
    @AuraEnabled
    public static Map<String,Map<String,List<String>>> generateTaskDependentFieldMap(){
        PicklistUtility pick = new PicklistUtility();
        Map<String,Map<String,List<String>>> mastermap = new Map<String,Map<String,List<String>>>();
        mastermap.put('Level-2',pick.GetDependentOptions('Task','Level_1__c','Level_2__c'));
        mastermap.put('Level-3',pick.GetDependentOptions('Task','Level_2__c','Level_3__c'));
        mastermap.put('Level-4',pick.GetDependentOptions('Task','Level_3__c','Level_4__c'));
        mastermap.put('Level-5',pick.GetDependentOptions('Task','Level_4__c','Level_5__c'));
        return mastermap;
    }
    
    /*Generate Picklists for Case Assignment Lightning Component*/ 
    @AuraEnabled(cacheable=true)
    public static List<String> initiateCaseAssignmentLevelOnePicklist(){
        fieldDependencyMap = new Map<String,List<String>>();
        for(New_Case_Assignment_Picklist_Dependency__mdt obj : [SELECT Id, Case_Picklist_Name__c, Case_Picklist_Value__c FROM New_Case_Assignment_Picklist_Dependency__mdt]){
            if(fieldDependencyMap.containsKey(obj.Case_Picklist_Name__c)){
                fieldDependencyMap.get(obj.Case_Picklist_Name__c).add(obj.Case_Picklist_Value__c);
            }
            else{
                fieldDependencyMap.put(obj.Case_Picklist_Name__c,new List<String>{obj.Case_Picklist_Value__c});
            }
        }

        List<String> returnList = new List<String>();
        List<Schema.PicklistEntry> level_1_ple = Schema.getGlobalDescribe().get('Case').getDescribe().fields.getMap().get('jl_Branch_master__c').getDescribe().getPicklistValues();
        for(Schema.PicklistEntry obj : level_1_ple){
            String value = (String)obj.getValue();
            if(fieldDependencyMap.containsKey('Branch') && fieldDependencyMap.get('Branch').contains(value)){
                returnList.add(value);
            }
        }
        return returnList;
    }

    @AuraEnabled
    public static List<String> initiateCaseAssignmentLevelOnePicklistPSE(){
        fieldDependencyMap = new Map<String,List<String>>();
        for(New_Case_Assignment_Picklist_Dependency__mdt obj : [SELECT Id, Case_Picklist_Name__c, Case_Picklist_Value__c FROM New_Case_Assignment_Picklist_Dependency__mdt]){
            if(fieldDependencyMap.containsKey(obj.Case_Picklist_Name__c)){
                fieldDependencyMap.get(obj.Case_Picklist_Name__c).add(obj.Case_Picklist_Value__c);
            }
            else{
                fieldDependencyMap.put(obj.Case_Picklist_Name__c,new List<String>{obj.Case_Picklist_Value__c});
            }
        }

        List<String> returnList = new List<String>();
        List<Schema.PicklistEntry> level_1_ple = Schema.getGlobalDescribe().get('Case').getDescribe().fields.getMap().get('Assign_To_New_Branch__c').getDescribe().getPicklistValues();
        for(Schema.PicklistEntry obj : level_1_ple){
            String value = (String)obj.getValue();
            if(fieldDependencyMap.containsKey('Branch') && fieldDependencyMap.get('Branch').contains(value)){
                returnList.add(value);
            }
        }
        return returnList;
    }
    
    @AuraEnabled
    public static Map<String,List<String>> filterResultsByRecordType(Map<String,List<String>> fieldDependencyMap, Map<String,List<String>> dependentInformationMap, String keyValue){
        Map<String,List<String>> filteredMap = new Map<String,List<String>>();
        for(String parentValue : dependentInformationMap.keySet()){
            for(String childValue : dependentInformationMap.get(parentValue)){
                if(fieldDependencyMap.containsKey(keyValue) && fieldDependencyMap.get(keyValue).contains(childValue)){ 
                    if(filteredMap.containsKey(parentValue)){
                        filteredMap.get(parentValue).add(childValue);
                    }
                    else{
                        filteredMap.put(parentValue,new List<String>{childValue});
                    }
                }
            }
        }
        return filteredMap;
    }
    
    @AuraEnabled (cacheable=true)
    public static Map<String,Map<String,List<String>>> generateCaseAssignmentFieldMap(){
        fieldDependencyMap = new Map<String,List<String>>();
        for(New_Case_Assignment_Picklist_Dependency__mdt obj : [SELECT Id, Case_Picklist_Name__c, Case_Picklist_Value__c FROM New_Case_Assignment_Picklist_Dependency__mdt]){
            if(fieldDependencyMap.containsKey(obj.Case_Picklist_Name__c)){
                fieldDependencyMap.get(obj.Case_Picklist_Name__c).add(obj.Case_Picklist_Value__c);
            }
            else{
                fieldDependencyMap.put(obj.Case_Picklist_Name__c,new List<String>{obj.Case_Picklist_Value__c});
            }
        }
        PicklistUtility pick = new PicklistUtility();
        Map<String,Map<String,List<String>>> mastermap = new Map<String,Map<String,List<String>>>();
        
        mastermap.put('ContactReason',filterResultsByRecordType(fieldDependencyMap,pick.GetDependentOptions('Case','jl_Branch_master__c','Contact_Reason__c'), 'Contact Reason'));
        mastermap.put('ReasonDetail',filterResultsByRecordType(fieldDependencyMap,pick.GetDependentOptions('Case','Contact_Reason__c','Reason_Detail__c'), 'Reason Detail'));
        mastermap.put('CDH',filterResultsByRecordType(fieldDependencyMap,pick.GetDependentOptions('Case','Reason_Detail__c','CDH_Site__c'), 'CDH'));
        mastermap.put('BranchDepartment',filterResultsByRecordType(fieldDependencyMap,pick.GetDependentOptions('Case','Reason_Detail__c','Branch_Department__c'), 'Branch Department'));
        system.debug(mastermap.get('ContactReason').get('Aberdeen'));
        return mastermap;
    }

    @AuraEnabled
	public static Boolean isPermSetAssignedToUser(String permSetName){
        User currentUser = CommonStaticUtils.getCurrentUser();
        return CommonStaticUtils.isPermissionSetAssignedToSingleUserByName(currentUser.Id, permSetName);
	}
    
    /* COPT-5780
    @AuraEnabled
    public static Boolean isCapitaProfileUser(){
        return CommonStaticUtils.UserHasProfileNamed('Capita');
    }
    COPT-5780 */
    
    // Changes for COPT-5780- Ragesh 
    @AuraEnabled
    public static Boolean isCreatePSETabEnabled(){
        return CommonStaticUtils.isCreatePSETabEnabledUser();
    }
    // End Changes for COPT-5780- Ragesh
	
    @AuraEnabled
    public static Boolean UserHasKnowledgeAccessCheck() {        
        User runningUser = getCurrentUserDetails();
        String userProfile = runningUser.Profile.Name;
        List<Knowledge_Access_Profiles__mdt> knowledgeProfList = [ SELECT Profile_Name__c FROM Knowledge_Access_Profiles__mdt WHERE Profile_Name__c =: userProfile];
        if(knowledgeProfList.size()>0) {  
            return true; 
        }
        else {
        	return false;  
        }
    }

    @AuraEnabled
    public static User getCurrentUserDetails(){
        return CommonStaticUtils.getCurrentUser();
    }

    @AuraEnabled
    public static Id getUsersPrimaryTeamQueueId(String usersTeam){
        return TeamUtils.getQueueIdForTeam(usersTeam);
    }


    @AuraEnabled
    public static Map<String,String> createCase(String caseJSON, String taskJSON, Boolean UserHasCallLoggingPermission){
        system.debug('Block2'+caseJSON);
        system.debug('Block3'+taskJSON);
        Id CALL_LOG_TASK_RECORD_TYPE_ID = Schema.SObjectType.Task.getRecordTypeInfosByName().get(CommonStaticUtils.TASK_RT_NAME_CALL_LOG).getRecordTypeId();
        Case caseObject = (Case)JSON.deserialize(caseJSON, Case.class);
        Task taskObject = (Task)JSON.deserialize(taskJSON, Task.class);
        taskObject.Subject = CommonStaticUtils.TASK_SUBJECT_CALL_LOG;
        taskObject.Status = CommonStaticUtils.TASK_STATUS_CLOSED_RESOLVED;
        taskObject.RecordTypeId = CALL_LOG_TASK_RECORD_TYPE_ID;
        taskObject.ActivityDate = System.today();
        Savepoint sp = Database.setSavepoint();
		Map<String,String> returnMap = new Map<String,String>();
        try{
            insert caseObject;
            taskObject.Case__c = caseObject.Id;
            if(UserHasCallLoggingPermission){
                createCallLogTask(taskObject);
            }
    
            
            returnMap.put('CaseId',String.valueOf(caseObject.Id));
            returnMap.put('redirectURL',String.format(Label.CaseRedirectionLEXURL,new List<String>{String.valueOf(caseObject.Id)}));
        }catch(Exception e){
            Database.rollback( sp );
            returnMap.put('Error','ERROR : '+e.getMessage());
            system.debug('ERROR : '+e.getMessage());
        }
        

        return returnMap;
    }
    
    @AuraEnabled
    public static List<String> createContact(String contactJSON, String recordTypeId){
        List<String> returnList = new List<String>();
        Contact contactObject = (Contact)JSON.deserialize(contactJSON, Contact.class);
        contactObject.RecordTypeId = recordTypeId;
        if([select Id from Contact where FirstName =:contactObject.FirstName AND LastName =:contactObject.LastName AND MailingPostalCode =:contactObject.MailingPostalCode AND Email =: contactObject.Email].size()>0){
            returnList.add('Duplicate customer already exists in the system. ');
        }
        else{
            try{
                insert contactObject;
            }catch(Exception e){
                system.debug('ERROR : '+e.getMessage());
            }
            returnList.add('Success');
            returnList.add(contactObject.Id);
            returnList.add((!String.isBlank(contactObject.FirstName) ? contactObject.FirstName+' ' : '')+contactObject.LastName);
        }

        return returnList;
    }

    @AuraEnabled
    public static List<Id> getRecordTypeInformation(){
        List<Id> RecordTypeList = new List<Id>();
        RecordTypeList.add( CommonStaticUtils.getRecordTypeID('Case','New Case'));
        RecordTypeList.add( CommonStaticUtils.getRecordTypeID('Case','Product Stock Enquiry'));
        RecordTypeList.add( CommonStaticUtils.getRecordTypeID('Contact','Normal'));
        return RecordTypeList;
    }
    
    @AuraEnabled
    public static void createCallLog(String taskJSON, String parentID,String description ){
        system.debug('@@@ createCallLog' + taskJSON);
        Task taskobj = (Task)JSON.deserialize(taskJSON, Task.class);
        Id CALL_LOG_TASK_RECORD_TYPE_ID = Schema.SObjectType.Task.getRecordTypeInfosByName().get(CommonStaticUtils.TASK_RT_NAME_CALL_LOG).getRecordTypeId();
       system.debug('@@@ CALL_LOG_TASK_RECORD_TYPE_ID'+ CALL_LOG_TASK_RECORD_TYPE_ID);
        taskobj.Subject = !String.isBlank(taskobj.Subject) ? taskobj.Subject : CommonStaticUtils.TASK_SUBJECT_CALL_LOG;
        taskobj.Status = !String.isBlank(taskobj.Status) ? taskobj.Status : CommonStaticUtils.TASK_STATUS_CLOSED_RESOLVED;
        taskobj.RecordTypeId = taskobj.RecordTypeId != null ? taskobj.RecordTypeId : CALL_LOG_TASK_RECORD_TYPE_ID;
        taskobj.ActivityDate = taskobj.ActivityDate != null ? taskobj.ActivityDate : System.today();
   /* COPT-4114
    * Below Changes have been implemented to log a comment under CaseComments
    * Desp: Case Comment has to be included when a description is being added while logging a call 
    */
        if(parentID!=NULL){
        casecomment cd = new casecomment();
        cd.ParentId = parentID;
        cd.CommentBody = description;
        insert cd;
        }
        createCallLogTask(taskobj);
    }
    
	@AuraEnabled
    public static void createCallLogTask(Task taskobj){

        try{
            upsert taskobj;
        }catch(Exception e){
            system.debug('ERROR : '+e.getMessage());
        }
    }
    
    @AuraEnabled
    public static Id fetchCustomerDetails(String caseId){
        Id customerId = [select ContactId from Case where Id = :caseId LIMIT 1].get(0).ContactId;
        return customerId;
    }
    
    @AuraEnabled
    public static Case initializeRelatedCaseComponent(String caseId){
        Case currentCase = [select Id, jl_CRNumber__c, jl_DeliveryNumber__c, jl_OrderManagementNumber__c, jl_OtherSystemRef__c, ContactId from Case where Id = :caseId LIMIT 1].get(0);
        currentCase.Id = null;
        currentCase.jl_Action_Taken__c = 'New case created';
        currentCase.Create_Case_Activity__c = false;
        currentCase.jl_InitiatingSource__c = 'Contact Centre';
        currentCase.Origin = 'Phone';
        currentCase.Status = 'New';
        currentCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('New Case').getRecordTypeId();
        currentCase.jl_Branch_master__c = '';
        currentCase.Contact_Reason__c = '';
        currentCase.Reason_Detail__c = '';
        currentCase.CDH_Site__c = '';
        currentCase.Branch_Department__c = '';
        currentCase.ParentId = caseId;
        currentCase.jl_Transfer_case__c = true;
        currentCase.Description = '';
        return currentCase;
    }
    
    @AuraEnabled
    public static Case createRelatedCase(String caseJSON){
        Case caseObject = (Case)JSON.deserialize(caseJSON, Case.class);
        insert caseObject;
        return [select Id, CaseNumber from Case where Id = :caseObject.Id].get(0);
    }
    
    @AuraEnabled
    public static Case initializeAssignmentQuickActionComponent(String caseId){
        Case currentCase = [select Id, CaseNumber, jl_Branch_master__c, Contact_Reason__c, Reason_Detail__c, CDH_Site__c, Branch_Department__c, Next_Case_Activity_Due_Date_Time__c,jl_Case_RestrictedAddComment__c from Case where Id = :caseId LIMIT 1].get(0);
        currentCase.jl_Transfer_case__c = true;
        currentCase.jl_Case_RestrictedAddComment__c = '';
        return currentCase.Next_Case_Activity_Due_Date_Time__c >= DateTime.now() ? currentCase : null;
    }
    
    @AuraEnabled
     public static Case updateCaseAssignment(String caseJSON){
         system.debug('Block3'+caseJSON);
        Case caseObject = (Case)JSON.deserialize(caseJSON, Case.class);
        caseObject.jl_Branch_master__c = caseObject.jl_Branch_master__c != '-None-' ? caseObject.jl_Branch_master__c : '';
        caseObject.Contact_Reason__c = caseObject.Contact_Reason__c != '-None-' ? caseObject.Contact_Reason__c : '';
        caseObject.Reason_Detail__c = caseObject.Reason_Detail__c != '-None-' ? caseObject.Reason_Detail__c : '';
        caseObject.CDH_Site__c = caseObject.CDH_Site__c != '-None-' ? caseObject.CDH_Site__c : '';
        caseObject.Branch_Department__c = caseObject.Branch_Department__c != '-None-' ? caseObject.Branch_Department__c : '';
        update caseObject;
        // call case activity method
        String caseContactReason =  caseObject.Contact_Reason__c;
        String reasonDetail =  caseObject.Reason_Detail__c;
        String Branch_master = caseObject.jl_Branch_master__c;
        String CDH_Site = caseObject.CDH_Site__c;
        String Branch_Department = caseObject.Branch_Department__c;
        string Add_Comment = caseObject.jl_Case_RestrictedAddComment__c;
        Id caseId = caseObject.id;
        updatecaseactivity(caseId, caseContactReason, reasonDetail,Branch_master,CDH_Site,Branch_Department, Add_Comment);
        return caseObject;
    }
	
	
	public static void updatecaseactivity(Id caseId, String caseContactReason,String reasonDetail,String Branch_master,string CDH_Site, String  Branch_Department, String Add_Comment){
        List<Case_Activity__c> caseactivityrecords = new List<Case_Activity__c>();     
        for(Case_Activity__c cActivity: [Select id, Case_Contact_Reason__c,Activity_Description__c,CDH_Site__c,Case_Branch_Department__c,
                                         jl_Branch_master__c,Queue_Completion__c,Case_Reason_Detail__c,Skip_NextActionIfOpen_Validation__c,Case__c, Resolution_Method__c FROM Case_Activity__c WHERE Case__c =: caseId AND Resolution_Method__c = NULL ORDER BY CreatedDate DESC LIMIT 1]){                                            
                                             cActivity.Case__c = caseId;
                                             cActivity.Case_Contact_Reason__c = caseContactReason;
                                             cActivity.Case_Reason_Detail__c = reasonDetail;
                                             cActivity.Case_Branch_Department__c = Branch_Department;
                                             cActivity.CDH_Site__c = CDH_Site;
                                             cActivity.jl_Branch_master__c = Branch_master;
                                             cActivity.Activity_Description__c = Add_Comment;
                                             cActivity.Skip_NextActionIfOpen_Validation__c = true;
											 cActivity.Update_From_Assignment__c = true;
                                             caseactivityrecords.add(cActivity);  
                                         }
        
        if(caseactivityrecords.size()>0){
            try{
                update caseactivityrecords;
            } 
            catch(Exception Ex){
                system.debug('Error while updating case activity record'+ Ex.getMessage());
            }
        }   
    }
    
	
	
     @AuraEnabled
    public static LiveChatTranscript linkChatWithCase(Id chatId,Id caserId){
        LiveChatTranscript ChatList = [Select Name,CaseID from LiveChatTranscript where id =: chatId];
        system.debug('chatlist'+ChatList);
        ChatList.CaseId = caserId;
        try{
 		      update ChatList;
            }catch(Exception e){
                system.debug('ERROR : '+e.getMessage());
            }
        
        return ChatList;
    }
	@AuraEnabled
    public static String getRecordType(id caseId){ 
        List<Case> cs1 = [Select recordtype.name from Case where id =:caseId]; 
        String rc =  cs1[0].recordtype.name;
        return rc;
    }
    
    @AuraEnabled
    public static Map<String, SObject> fetchCaseAssignmentDetails(String caseId){
        Map<String, SObject> returnMap = new Map<String, SObject>();
        returnMap.put('CaseAssignment',[select Id, Assign_To_New_Branch__c, jl_Branch_master__c,Contact_Reason__c,Reason_Detail__c,CDH_Site__c,Branch_Department__c,Next_Case_Activity_Due_Date_Time__c from Case where Id = :caseId LIMIT 1].get(0));
        returnMap.put('CurrentUserDetail',CommonStaticUtils.getCurrentUser());
        System.debug('returnMapreturnMapreturnMapreturnMap'+returnMap);
        return returnMap;
    }
    
    
    @AuraEnabled    
    public static List<String> fetchAvailableSlots( String queueName ) {
        List<String> availableDates = new List<String>();
        Map < String, Map < String, Integer > > availableQueueSlotsMap = QueueCapacityUtils.fetchAllAvailableQueueSlots ( queueName );
        String lastDate = '';
        for ( String dateString : availableQueueSlotsMap.keySet() ) {
            availableDates.add( dateString);
            lastDate = dateString;
        }  
        
        
        return availableDates;
    }
    
    //QueueCapacity: Author: KM
    /* COPT-4807
    * Below Changes have been implemented to get the queue of the logged in user
    * Desc: Based on the logged in User Id the queue value will be populated
    */
   @AuraEnabled
    public static String getQueue()
    {
       
         String queueName = 'Queue not found';
        for ( User currentUser : [ SELECT Id, Queues__c FROM User WHERE Id =:UserInfo.getUserId()] ) {
            List<String> qname = currentUser.Queues__c.split(',');
            queueName = qname[0];                    
        }
        return queueName;
        
    /*QueueCapacity: Author: KM
     COPT-4807
    * Below Changes have been implemented to fetch the queue based on contactReason and ReasonDetail values
    */
    }
     @AuraEnabled
    public static String fetchQueue(String BranchValue, String contactReasonValue, String reasonDetailValue, String thisCase)
    {
        String queueNameValue = '';
        
       list<Case_Routing_Categorisation__c> lstcrc = [select Assign_To__c from Case_Routing_Categorisation__c where 
                                        Contact_Reason__c =: contactReasonValue and Reason_Detail__c =: reasonDetailValue
                                         Limit 1];  
        String queueAssignTo  = 'Queue does not exist';
		if(lstcrc != null && lstcrc.size() > 0)
        	queueAssignTo = lstcrc[0].Assign_To__c;
       
		List<Team__c> listTeam = [select Queue_Name__c from Team__c where name =: queueAssignTo];
             if(listTeam != null && listTeam.size()>0){
       		  queueNameValue = listTeam[0].Queue_Name__c ;
                 
              //return queueNameValue;
        }
        if (queueAssignTo.startsWith('Branch')){
            
                String branchMaster = queueAssignTo + ' ' +BranchValue;
                List<Team__c> listTeam1 = [select Queue_Name__c from Team__c where name =:branchMaster ];
             if(listTeam1 != null && listTeam1.size()>0){
       		  queueNameValue = listTeam1[0].Queue_Name__c ;
            }
        
        }
            return queueNameValue; 
          
          
    }
    
    /*QueueCapacity: Author: KM
     COPT-4807
    * Below Changes have been implemented to fetch the queue for PSE cases
    */
     @AuraEnabled
    public static String fetchPSEQueue(String branchValue)
    {
        String queuename = '';
      	queuename = QueueCapacityUtils.getQueueNameForTeam(branchValue);
        system.debug(queuename);
        return queuename;
    
	}

    //Changes for COPT-5315- Ragesh
    @AuraEnabled 
    public static Contact fetchRelatedCustomerDetails ( String LCTRecordId ) {  
        System.debug(' record Id value ' +LCTRecordId);
        LiveChatTranscript lctRecord = new LiveChatTranscript ();
        List < Contact > contactRecordList = new List < Contact > () ;
        Contact contactRecord ;
        for ( LiveChatTranscript  lctRecordVal : [ SELECT Id, Name, ChatKey, ContactId 
                                                  FROM LiveChatTranscript 
                                                  WHERE Id =: LCTRecordId ] ) {
                                                      lctRecord = lctRecordVal;
                                                  }
        if ( lctRecord.ContactId != null ) {
            System.debug(' lctRecord.ContactId ' + lctRecord.ContactId);
            contactRecordList =  [ SELECT Id, Name FROM Contact WHERE Id =: lctRecord.ContactId];
            if ( contactRecordList.size() > 0  ) {
                contactRecord = contactRecordList[0];
            }
        }
        return contactRecord;
    }
}