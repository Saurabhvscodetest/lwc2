/* Description : Delete SocialPersona Records
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0  29/03/2019        Vijay Ambati                      Created              COPT-4362
*
*/
@IsTest
Public class BAT_DeleteSocialPersonaTest {

    @isTest
    Public Static Void testDeleteSocialPersona(){
        
        contact con = new contact();
        con.FirstName = 'Vijay';
        con.LastName  = 'Ambati';
        Insert con;
        List<SocialPersona> sP = new List<SocialPersona>();
        
        for(Integer counter =0; counter<300 ; counter++){
            SocialPersona spd = new SocialPersona();
            spd.Name = 'Vijay' + counter;
            spd.ParentId = con.Id;
            spd.Provider = 'twitter';
            sP.add(spd);
        }
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.Name = 'BAT_DeleteSocialPersona';
		gDPR.GDPR_Class_Name__c = 'BAT_DeleteSocialPersona';
		gDPR.Record_Limit__c = '5000';                
        try{
            Insert gDPR;
            Insert sP;
        }
        catch(Exception e){
            System.debug('Exception Caught:'+e.getmessage());
        }
        Database.BatchableContext BC;
		BAT_DeleteSocialPersona obj = new BAT_DeleteSocialPersona();
        Test.startTest();
        Database.DeleteResult[] Delete_Result = Database.delete(sP, false);
        obj.start(BC);
        obj.execute(BC,sP); 
        obj.finish(BC);
        Test.stopTest();          
    }
    @isTest
		Public static void testSchedule(){
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.Name = 'BAT_DeleteSocialPersona';
		gDPR.GDPR_Class_Name__c = 'BAT_DeleteSocialPersona';
		gDPR.Record_Limit__c = '5000';
        Test.startTest();
        Insert gDPR;
        ScheduleDeleteSocialPersona obj = NEW ScheduleDeleteSocialPersona();
        obj.execute(null);  
        Test.stopTest();
    }  
}