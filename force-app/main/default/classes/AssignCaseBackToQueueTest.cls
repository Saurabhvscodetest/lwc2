@isTest
public class AssignCaseBackToQueueTest {
	
    @testSetup static void setup() {
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();        
        User runningUser = UserTestDataFactory.getRunningUser();        
        User contactCentreUser;        
        Contact con;
        Case cas;
        CaseHistory ch;
        
        System.runAs(runningUser) {
            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
            con = CustomerTestDataFactory.createContact();
            insert con;
        }
        System.runAs(contactCentreUser) {             
            cas = CaseTestDataFactory.createPSECase(con.Id);
            insert cas;
        }
    }
    
    @isTest static void assignQueueServerTestCaseQueue() {    
        List<Case> caseList = [ SELECT Id, OwnerId FROM Case WHERE RecordType.Name =: 'Product Stock Enquiry' LIMIT 1 ];
        Case cas = caseList[0];
        Test.startTest();
          AssignCaseBackToQueue.assignQueueServer(cas.Id);
        Test.stopTest();
    }
    
    @isTest static void assignQueueServerTestCaseOwner() {
        List<Case> caseList = [ SELECT Id, OwnerId FROM Case WHERE RecordType.Name =: 'Product Stock Enquiry' LIMIT 1 ];
        Case cas = caseList[0];
        cas.OwnerId = UserInfo.getUserId();        
        Test.startTest();
          update cas;
          AssignCaseBackToQueue.assignQueueServer(cas.Id);
        Test.stopTest();
    }
    
    @isTest static void assignQueueServerTestNoCase() {
        Test.startTest();
          AssignCaseBackToQueue.assignQueueServer(null);
        Test.stopTest();
    }
    
    @isTest static void assignQueueToCaseTest() {
        
        List<Case> caseList = [ SELECT Id, OwnerId FROM Case WHERE RecordType.Name =: 'Product Stock Enquiry' LIMIT 1 ];
        Case cas = caseList[0];
        Test.startTest();
        try {
            AssignCaseBackToQueue.assignQueueToCase(cas.Id);
        }
        catch(Exception e) { }          
        Test.stopTest();
    }
}