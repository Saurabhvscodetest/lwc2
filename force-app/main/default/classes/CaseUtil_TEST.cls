/**
 *  @author: Deepak Pansari
 *  @date: 2 Sep 2019 
 *  @description: Test class for class CaseUtil 
 */
@isTest
public class CaseUtil_TEST 
{
    @testSetup static void setup() {
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
    }
    public static TestMethod void testContactMatchingLogic()
    {
    // Want to skip validation rules
    UnitTestDataFactory.setRunValidationRules(false);
    List<Contact> conToInsert = new List<Contact>();    
    // Primary contact 
    Contact con = new Contact();
    con.FirstName = 'fName';
    con.LastName = 'lName';
    con.Email = 'primaryemail@address.com';
    con.MailingStreet = 'first line ';
    con.MailingPostalCode = 'qwe rty';
    con.recordtypeid = '012b0000000kKhmAAE';
    
    // Secondry contact
    Contact con1 = new Contact();
    con1.FirstName = 'fName';
    con1.LastName = 'lName';
    con1.Email = 'secondryemail@address.com';
    con1.MailingStreet = 'first line ';
    con1.MailingPostalCode = 'qwe rty';
    con1.recordtypeid = '012b0000000M9U4AAK';
    
    conToInsert.add(con);
    conToInsert.add(con1);
    
    insert conToInsert;
    
    List<Case> caseToInsert = new List<Case>();     

    // Case 1
    Case case1 = new case();
    case1.jl_pse1_Item_Description__c = 'test';
    case1.jl_pse1_Item_Price__c = 1;
    case1.jl_pse1_Quantity__c = '1';
    case1.jl_pse1_Stock_Number__c = '12365578';
    case1.Assign_To_New_Branch__c = 'ABERDEEN';
    case1.Origin = 'Instagram';
    case1.SuppliedEmail = 'primaryemail@address.com';
    case1.SuppliedName ='Ravi';
    case1.SuppliedPhone = '07404852166';
    case1.Web_Postcode__c ='E6 9PF';
    case1.Level_1__c = 'No Call Case Creation';
    case1.RecordTypeId ='012b0000000M1FUAA0';
    case1.Bypass_Standard_Assignment_Rules__c = true;
   

    // Case 2
    Case case2 = new case();
    case2.jl_pse1_Item_Description__c = 'test';
    case2.jl_pse1_Item_Price__c= 1;
    case2.jl_pse1_Quantity__c = '1';
    case2.jl_pse1_Stock_Number__c = '12365578';
    case2.Assign_To_New_Branch__c = 'ABERDEEN';
    case2.Origin = 'Instagram';
    case2.SuppliedEmail = 'secondryemail@address.com';
    case2.SuppliedName ='Ravi';
    case2.SuppliedPhone = '07404852166';
    case2.Web_Postcode__c ='E6 9PF';
    case2.Level_1__c = 'No Call Case Creation';
    case2.RecordTypeId ='012b0000000M1FUAA0';
    case2.Bypass_Standard_Assignment_Rules__c = true;
    
    Test.startTest();
    
        caseToInsert.add(case1);
        caseToInsert.add(case2);
        insert caseToInsert;
        case1.Assign_To_New_Branch__c = 'Oxford';
        case2.Assign_To_New_Branch__c = 'Stratford City';
        CaseUtils.getContactForPSECase(caseToInsert);
        CaseUtils.correctPSEBranchMapping(caseToInsert);
    Test.stopTest();
  }
  
  @isTest static void testCaseOmniTimeReset()
    {
    
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User testUser;
        Case testCase;
        Contact testContact;
        Test.startTest();
        System.runAs(runningUser)
        {
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'deepak@tests.com';
            insert testContact;
            id defaultQId = null;
            id failedQId = null;
            
            List<Group> testQueueList = new List<Group>();
            testQueueList = [select Id,name from Group where  Type = 'Queue' AND NAME like 'Gift Card%'];
            for(Group objGroup : testQueueList)
            {
                if(objGroup.name=='Gift Card')
                {
                    defaultQId = objGroup.id;
                }
                else if(objGroup.name=='Gift Card FAILED')
                {
                    failedQId = objGroup.id;
                }
            }
            
            testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            if(defaultQId != null)
            {
                testCase.ownerid = defaultQId;
            }
            insert testCase;
            if(failedQId != null)
            {
                testCase.ownerid = failedQId;
                update testcase;
            }
           Test.stopTest();
        }
    }
    
    public static TestMethod void ownerChangeToUserForCloseCaseTest() {
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        // Want to skip validation rules
        UnitTestDataFactory.setRunValidationRules(false);
        List<Contact> conToInsert = new List<Contact>();    
        // Primary contact 
        Contact con = new Contact();
        con.FirstName = 'fName';
        con.LastName = 'lName';
        con.Email = 'primaryemail@address.com';
        con.MailingStreet = 'first line ';
        con.MailingPostalCode = 'qwe rty';
        con.recordtypeid = '012b0000000kKhmAAE';
        
        // Secondry contact
        Contact con1 = new Contact();
        con1.FirstName = 'fName';
        con1.LastName = 'lName';
        con1.Email = 'secondryemail@address.com';
        con1.MailingStreet = 'first line ';
        con1.MailingPostalCode = 'qwe rty';
        con1.recordtypeid = '012b0000000M9U4AAK';
        
        conToInsert.add(con);
        conToInsert.add(con1);
        
        insert conToInsert;
        
        List<Case> caseToInsert = new List<Case>();
            List<Id> caseIds = new List<Id>();
    
        // Case 1
        Case case1 = new case();
        case1.jl_pse1_Item_Description__c = 'test';
        case1.jl_pse1_Item_Price__c = 1;
        case1.jl_pse1_Quantity__c = '1';
        case1.jl_pse1_Stock_Number__c = '12365578';
        case1.Assign_To_New_Branch__c = 'ABERDEEN';
        case1.Origin = 'Instagram';
        case1.SuppliedEmail = 'primaryemail@address.com';
        case1.SuppliedName ='Ravi';
        case1.SuppliedPhone = '07404852166';
        case1.Web_Postcode__c ='E6 9PF';
        case1.Level_1__c = 'No Call Case Creation';
        case1.RecordTypeId ='012b0000000M1FUAA0';
        case1.Bypass_Standard_Assignment_Rules__c = true;
       
    
        // Case 2
        Case case2 = new case();
        case2.jl_pse1_Item_Description__c = 'test';
        case2.jl_pse1_Item_Price__c= 1;
        case2.jl_pse1_Quantity__c = '1';
        case2.jl_pse1_Stock_Number__c = '12365578';
        case2.Assign_To_New_Branch__c = 'ABERDEEN';
        case2.Origin = 'Instagram';
        case2.SuppliedEmail = 'secondryemail@address.com';
        case2.SuppliedName ='Ravi';
        case2.SuppliedPhone = '07404852166';
        case2.Web_Postcode__c ='E6 9PF';
        case2.Level_1__c = 'No Call Case Creation';
        case2.RecordTypeId ='012b0000000M1FUAA0';
        case2.Bypass_Standard_Assignment_Rules__c = true;
        
        Test.startTest();
            System.runAs(runningUser) {
                caseToInsert.add(case1);
                caseToInsert.add(case2);
                caseIds.add(case1.Id);
                caseIds.add(case2.Id);
                insert caseToInsert;
                case1.Assign_To_New_Branch__c = 'Oxford';
                case2.Assign_To_New_Branch__c = 'Stratford City';
                CaseUtils.ownerChangeToUserForCloseCase(runningUser.Id,caseIds);
            }
        Test.stopTest();
    }  
}