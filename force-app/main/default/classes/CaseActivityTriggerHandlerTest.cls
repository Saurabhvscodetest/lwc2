@isTest
public with sharing class CaseActivityTriggerHandlerTest {
    
    @isTest static void checkCaseActivityStartAndEndDateTimeIsSet() {
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User testUser;
        Case testCase;
        Case_Activity__c testCaseActivity;
        Contact testContact;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
            
            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        
        System.runAs(testUser) {
            
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;
            
            testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            insert testCase;
            
            Test.startTest();
            
            testCaseActivity = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            insert testCaseActivity;
            
            Case_Activity__c createdCaseActivity = [SELECT Id, Activity_End_Date_Time__c, Activity_Start_Date_Time__c 
                                                    FROM Case_Activity__c WHERE Id = :testCaseActivity.Id AND Customer_Promise_Type__c = 'Customer Contact Promise'][0];
            Id BusinessHoursId = [select BusinessHoursId from Case where Id = :testCase.Id].get(0).BusinessHoursId;
           
		    system.assert(BusinessHours.isWithin(BusinessHoursId,createdCaseActivity.Activity_Start_Date_Time__c),'Case Activity start date is not within business hours');
            system.assert(BusinessHours.isWithin(BusinessHoursId,createdCaseActivity.Activity_End_Date_Time__c),'Case Activity end date is not within business hours');         
		   
		   Test.stopTest();
            
        }
    }
    
    @isTest static void checkCaseActivityFieldsSetOnClosure(){
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User testUser;
        Case testCase;
        Case_Activity__c testCaseActivity;
        Contact testContact;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
            
            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        
        System.runAs(testUser) {
            
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;
            
            testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            insert testCase;
            
            testCaseActivity = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            insert testCaseActivity;
            
            Test.startTest();
            
            testCaseActivity.Resolution_Method__c = 'Customer contacted';
            update testCaseActivity;
            
            Case_Activity__c createdCaseActivity = [SELECT Id, Completed_By__c, Resolution_Method__c, Activity_Completed_Date_Time__c 
                                                    FROM Case_Activity__c WHERE Id = :testCaseActivity.Id][0];
            
            User u = [SELECT Id, Name FROM User WHERE Id = :testUser.Id][0];
            
            System.assertEquals(testCaseActivity.Resolution_Method__c, createdCaseActivity.Resolution_Method__c);
            System.assertEquals(u.Name, createdCaseActivity.Completed_By__c);
            System.assertNotEquals(NULL, createdCaseActivity.Activity_Completed_Date_Time__c);
            
            Test.stopTest();
            
        }
        
    }
    
    
    @isTest static void checkRelatedCaseFieldsAreSetOnCaseActivityClosure(){
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User testUser;
        Case testCase;
        Case_Activity__c testCaseActivity;
        Contact testContact;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
            
            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        
        System.runAs(testUser) {
            
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;
            
            testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.jl_Branch_master__c = 'Aberdeen';
            testCase.Contact_Reason__c = 'After Sales - NON CHS';
            testCase.Reason_Detail__c = 'Order Enquiry';
            testCase.jl_Last_Queue_Name__c = 'test_queue';
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            insert testCase;
            
            testCaseActivity = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            insert testCaseActivity;
            
            Test.startTest();
            
            testCaseActivity.Resolution_Method__c = 'Customer contacted';
            update testCaseActivity;
            
            Case_Activity__c createdCaseActivity = [SELECT Id, Queue_Completion__c, Case_Contact_Reason__c, Case_Reason_Detail__c, Case_Branch_Department__c 
                                                    FROM Case_Activity__c WHERE Id = :testCaseActivity.Id][0];
            Case createdCase = [SELECT Id, Contact_Reason__c, Reason_Detail__c, Branch_Department__c, jl_Last_Queue_Name__c
                                FROM Case WHERE Id = :testCase.Id][0];
            
            
            Test.stopTest();
            
        }
        
    }
    @isTest static void checkNumberOfFailedCustomerPromisesAreSetOnRelatedCase(){
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User testUser;
        Case testCase;
        Contact testContact;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
            
            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        
        System.runAs(testUser) {
            
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;
            
            testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            insert testCase;
            
            List<Case_Activity__c> testCaseActivities = new List<Case_Activity__c>();
            
            for(Integer i = 0; i < 3; i++){
                Case_Activity__c testCaseActivity = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
                testCaseActivities.add(testCaseActivity);
            }
            
            insert testCaseActivities;
            
            system.assertEquals(3, testCaseActivities.size());
            
            Test.startTest();
            
            List<Case_Activity__c> updatedCaseActivities = new List<Case_Activity__c>();
            
            for(Case_Activity__c testCaseActivity : testCaseActivities){
                testCaseActivity.Resolution_Method__c = 'Customer contacted';
                updatedCaseActivities.add(testCaseActivity);
            }
            
            update updatedCaseActivities;
            
            system.assertNotEquals(0, updatedCaseActivities.size());
            
            updatedCaseActivities = [SELECT Id, Activity_Completed_Date_Time__c, Activity_End_Date_Time__c 
                                     FROM Case_Activity__c WHERE Id IN :updatedCaseActivities];
            
            List<Case_Activity__c> failedCaseActivities = new List<Case_Activity__c>();
            
            for(Case_Activity__c testCaseActivity : updatedCaseActivities){
                testCaseActivity.Activity_Completed_Date_Time__c = testCaseActivity.Activity_End_Date_Time__c.addDays(1);
                failedCaseActivities.add(testCaseActivity);
            }
            
            update failedCaseActivities;
            
            Case relatedCase = [SELECT Id, Number_of_Failed_Customer_Promises__c FROM Case WHERE Id = :testCase.Id][0];
            
            system.assertEquals(3, relatedCase.Number_of_Failed_Customer_Promises__c, 'Number of failed customer promises should equal 3');
            
            Test.stopTest();
        }
    }
    
    @isTest static void checkNumberOfFailedInternalActionsAreSetOnRelatedCase(){
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User testUser;
        Case testCase;
        Contact testContact;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
            
            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        
        System.runAs(testUser) {
            
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;
            
            testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            insert testCase;
            
            List<Case_Activity__c> testCaseActivities = new List<Case_Activity__c>();
            
            for(Integer i = 0; i < 3; i++){
                Case_Activity__c testCaseActivity = CaseActivityTestDataFactory.createInternalActionCaseActivity(testCase.Id);
                testCaseActivities.add(testCaseActivity);
            }
            
            insert testCaseActivities;
            
            system.assertEquals(3, testCaseActivities.size());
            
            Test.startTest();
            
            List<Case_Activity__c> updatedCaseActivities = new List<Case_Activity__c>();
            
            for(Case_Activity__c testCaseActivity : testCaseActivities){
                testCaseActivity.Resolution_Method__c = 'Carrier contacted';
                updatedCaseActivities.add(testCaseActivity);
            }
            
            update updatedCaseActivities;
            
            system.assertNotEquals(0, updatedCaseActivities.size());
            
            updatedCaseActivities = [SELECT Id, Activity_Completed_Date_Time__c, Activity_End_Date_Time__c 
                                     FROM Case_Activity__c WHERE Id IN :updatedCaseActivities];
            
            List<Case_Activity__c> failedCaseActivities = new List<Case_Activity__c>();
            
            for(Case_Activity__c testCaseActivity : updatedCaseActivities){
                testCaseActivity.Activity_Completed_Date_Time__c = testCaseActivity.Activity_End_Date_Time__c.addDays(1);
                failedCaseActivities.add(testCaseActivity);
            }
            
            update failedCaseActivities;
            
            Case relatedCase = [SELECT Id, Number_of_Failed_Internal_Actions__c FROM Case WHERE Id = :testCase.Id][0];
            
            system.assertEquals(3, relatedCase.Number_of_Failed_Internal_Actions__c, 'Number of failed Internal Actions should equal 3');
            
            Test.stopTest();
        }
    }
    
    @isTest static void checkHighestPriorityCustomerPromiseStartAndEndDateTimeSetOnRelatedCase(){
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User testUser;
        Case testCase;
        Contact testContact;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
            
            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        
        System.runAs(testUser) {
            
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;
            
            testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            insert testCase;
            
            List<Case_Activity__c> testCaseActivities = new List<Case_Activity__c>();
            
            for(Integer i = 0; i < 3; i++){
                Case_Activity__c testCaseActivity = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
                testCaseActivities.add(testCaseActivity);
            }
            
            insert testCaseActivities;
            
            system.assertEquals(3, testCaseActivities.size());
            
            Test.startTest();
            
            Case_Activity__c testCaseActivity = [SELECT Id, Activity_Completed_Date_Time__c, Activity_End_Date_Time__c 
                                                 FROM Case_Activity__c WHERE Id IN :testCaseActivities][0];
            
            testCaseActivity.Activity_End_Date_Time__c = testCaseActivity.Activity_End_Date_Time__c.addDays(-1);
            update testCaseActivity;
            
            DateTime highPriorityCaseActivityDateTime = DateTime.valueOf([select MIN(Activity_End_Date_Time__c) from Case_Activity__c WHERE Case__c = :testCase.Id].get(0).get('expr0'));
            Case relatedCase = [SELECT Id, Next_Case_Activity_Due_Date_Time__c FROM Case WHERE Id = :testCase.Id][0];
            system.assertEquals(highPriorityCaseActivityDateTime, relatedCase.Next_Case_Activity_Due_Date_Time__c, 'Case field should match high priority Case Activity Due Date/Time');
            Test.stopTest();
        }
    }
    
    @isTest static void checkHighestPriorityInternalActionStartAndEndDateTimeSetOnRelatedCase(){
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User testUser;
        Case testCase;
        Contact testContact;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
            
            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        
        System.runAs(testUser) {
            
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;
            
            testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            testCase.Create_Case_Activity__c = true;
            insert testCase;
            
            List<Case_Activity__c> testCaseActivities = new List<Case_Activity__c>();
            
            for(Integer i = 0; i < 3; i++){
                Case_Activity__c testCaseActivity = CaseActivityTestDataFactory.createInternalActionCaseActivity(testCase.Id);
                testCaseActivities.add(testCaseActivity);
            }
            
            insert testCaseActivities;
            
            system.assertEquals(3, testCaseActivities.size());
            
            Test.startTest();
            
            Case_Activity__c testCaseActivity = [SELECT Id, Activity_Completed_Date_Time__c, Activity_End_Date_Time__c 
                                                 FROM Case_Activity__c WHERE Id IN :testCaseActivities][0];
            
            testCaseActivity.Activity_End_Date_Time__c = testCaseActivity.Activity_End_Date_Time__c.addDays(-1);
            update testCaseActivity;
            
            Case_Activity__c highPriorityCaseActivity = [SELECT Id, Activity_Completed_Date_Time__c, Activity_Start_Date_Time__c, Activity_End_Date_Time__c
                                                         FROM Case_Activity__c WHERE Id = :testCaseActivity.Id][0];
            
            Case relatedCase = [SELECT Id, Next_Case_Activity_Due_Date_Time__c FROM Case WHERE Id = :testCase.Id][0];
            
            system.assertEquals(highPriorityCaseActivity.Activity_End_Date_Time__c, relatedCase.Next_Case_Activity_Due_Date_Time__c, 'Case field should match high priority Case Activity Due Date/Time');
            
            Test.stopTest();
        }
    }
    
    @isTest static void testNextActionTransferCase() {
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User testUser;
        Case testCase;
        Case_Activity__c testCaseActivity;
        Contact testContact;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
            
            insert CustomSettingTestDataFactory.createConfigSettings();            
        }
        
        System.runAs(testUser) {
            
            
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'tester@testing.com';
            insert testContact;
            
            testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Assign_To__c = 'Hamilton/Didsbury - CST';
            insert testCase;
            Test.startTest();
            testCaseActivity = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            testCaseActivity.transfer_Case__c = true;
            /*insert testCaseActivity;
            
            Case createdCase = [SELECT Id, OwnerId
                                FROM Case WHERE Id = :testCaseActivity.Case__c];
            List<QueueSObject> queues = [SELECT Queue.Id FROM QueueSObject WHERE Queue.DeveloperName = 'CST_Hamilton_Didsbury'];
            Id userQueueId; 
            if(!queues.isEmpty() && queues[0].Id != null) {
                userQueueId = queues[0].QueueId;
            }
            //Note: TeamUtils.getTeamNameForAssigning is returning null
            System.assertNotEquals(userQueueId, createdCase.OwnerId);
            */
            Test.stopTest();
        }
    }
    
    @isTest static void testAssignRecordTypes() {
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User testUser;
        Case testCase;
        Case_Activity__c testCaseActivity;
        Contact testContact;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
            
            insert CustomSettingTestDataFactory.createConfigSettings();            
            insert CustomSettingTestDataFactory.createTeamCustomSetting('Hamilton/Didsbury - CST', 'Hamilton/Didsbury - CST');
        }
        
        System.runAs(testUser) {
            
            
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'tester@testing.com';
            insert testContact;
            
            
            testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            insert testCase;
            
            Test.startTest();
            
            testCaseActivity = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            testCaseActivity.transfer_Case__c = true;
            /*insert testCaseActivity;
            
            Case_Activity__c createdCaseActivity = [SELECT Id, RecordTypeId
                                                    FROM Case_Activity__c WHERE Id = :testCaseActivity.Id][0];
            
            System.assertEquals(CaseActivityUtils.customerPromiseRTId, createdCaseActivity.RecordTypeId);
            */
            Test.stopTest();
            
        }
    }
    
    @isTest static void testCreateCaseComments() {
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User testUser;
        Case testCase;
        Case_Activity__c testCaseActivity;
        Contact testContact;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();   
            insert testUser;
            
            insert CustomSettingTestDataFactory.createConfigSettings();            
        }
        
        System.runAs(testUser) {
            
            
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'tester@testing.com';
            insert testContact;
            
            
            testCase = CaseTestDataFactory.createCase(testContact.Id);
            insert testCase;
            
            Test.startTest();
            
            testCaseActivity = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            testCaseActivity.Add_Comment__c = 'Test Comment';
            insert testCaseActivity;
            system.assertEquals('Test Comment', testCaseActivity.Add_Comment__c); 
            
            CaseComment caseComment = [SELECT CommentBody, CreatedDate
                                       FROM CaseComment ORDER BY CreatedDate DESC LIMIT 1];
            
          //  System.assert(caseComment.CommentBody.contains('test description'));
            Test.stopTest();
            
        }
    }
    
    @isTest static void testInsertCustomerPromiseType() {
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User testUser;
        Case testCase;
        Case_Activity__c testCaseActivity;
        Contact testContact;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
            
            insert CustomSettingTestDataFactory.createConfigSettings();            
            insert CustomSettingTestDataFactory.createTeamCustomSetting('Hamilton/Didsbury - CST', 'Hamilton/Didsbury - CST');
        }
        
        System.runAs(testUser) {
            
            
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'tester@testing.com';
            insert testContact;
            
            
            testCase = CaseTestDataFactory.createCase(testContact.Id);
            insert testCase;
            
            Test.startTest();
            Id internalActionRTId = CommonStaticUtils.getRecordTypeId('Case_Activity__c', 'Internal Action');
            testCaseActivity = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            testCaseActivity.RecordTypeId = internalActionRTId;
            testCaseActivity.Activity_Type__c = 'Internal Action';
            insert testCaseActivity;
            
            Case_Activity__c createdCaseActivity = [SELECT Id, Customer_Promise_Type__c, recordTypeId
                                                    FROM Case_Activity__c 
                                                    WHERE Id = :testCaseActivity.Id AND Customer_Promise_Type__c = 'Internal Action'];
            
            System.assertEquals('Internal Action', createdCaseActivity.Customer_Promise_Type__c);
            System.assertEquals(internalActionRTId, createdCaseActivity.recordTypeId);
            Test.stopTest();
            
        }
    }
    
    @isTest static void testUpdateRelatedCaseFields() {
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User testUser;
        Case testCase;
        Case_Activity__c testCaseActivity;
        Contact testContact;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
            
            insert CustomSettingTestDataFactory.createConfigSettings();            
        }
        
        System.runAs(testUser) {
            
            
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'tester@testing.com';
            insert testContact;
            
            
            testCase = CaseTestDataFactory.createCase(testContact.Id);
            insert testCase;
            
            Test.startTest();
            
            
            testCaseActivity = CaseActivityTestDataFactory.createInternalActionCaseActivity(testCase.Id);                                    
            insert testCaseActivity;
            
            testCaseActivity.Resolution_Method__c = 'Internal team contacted'; 
            update testCaseActivity;
            
            Case updatedCase = [SELECT Id, Status, jl_Action_Taken__c, Send_VOC_Survey__c
                                FROM Case 
                                WHERE Id = :testCase.Id];
            
            
            System.assertEquals('Active - Follow up required', updatedCase.Status);
            System.assertEquals('No customer contact required', updatedCase.jl_Action_Taken__c);
            System.assertEquals(false, updatedCase.Send_VOC_Survey__c);
            
            Case_Activity__c testCaseActivity2 = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            insert testCaseActivity2;
            
            testCaseActivity2.Resolution_Method__c = 'Voicemail left with customer';
            update testCaseActivity2;
            
            Case resultedCase = [SELECT Id, Status, jl_Action_Taken__c, Send_VOC_Survey__c
                                 FROM Case 
                                 WHERE Id = :testCase.Id];
            
            System.assertEquals('Active - Follow up required', resultedCase.Status);
            System.assertEquals('Customer Contact Unsuccessful', resultedCase.jl_Action_Taken__c);
            System.assertEquals(false, resultedCase.Send_VOC_Survey__c);

            Test.stopTest();
            
        }
    }
}