public class InFlightCase implements Comparable {
	public String caseNo;
	public DateTime dateTimeWhenCaseFailed;
	public string dateType;
	
	public InFlightCase(String caseNo, Datetime dateTimeWhenCaseFailed, String dateType){
		this.caseNo = caseNo;
		this.dateType = dateType;
		this.dateTimeWhenCaseFailed = dateTimeWhenCaseFailed;
	}
	
	public Integer compareTo(Object obj) {
		InFlightCase inflightCase = (InFlightCase)obj;	
		if (this.dateTimeWhenCaseFailed > inflightCase.dateTimeWhenCaseFailed) {
       		return 1;
    	}
	
		if (this.dateTimeWhenCaseFailed == (inflightCase.dateTimeWhenCaseFailed)) {
	        return 0;
	    }
	
	    return -1;
	}
	
    
    public Boolean equals(Object obj) {
        if (obj instanceof InFlightCase) {
            InFlightCase p = (InFlightCase)obj;
            return (caseNo == (p.caseNo)) && (dateTimeWhenCaseFailed == (p.dateTimeWhenCaseFailed));
        }
        return false;
    }
	
}