/* Description  : Delete Loyalty Account records
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   25/12/2018        Vignesh Kotha                   Created                 COPT-4282
*
*/
global class GDPR_BatchDeleteLoyaltyAccount implements Database.Batchable<sObject>,Database.Stateful{
	global Map<Id,String> ErrorMap = new Map<Id,String>();
    global integer result=0;
    private static final DateTime Deactication_DT = System.now().addYears(-5);
    private static final DateTime Source_DT = System.now().addYears(-5);
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'SELECT Id,Deactivation_Date_Time__c,Source_CreatedDate__c FROM Loyalty_Account__c where (Deactivation_Date_Time__c <= : Deactication_DT and IsActive__c = false) OR (Source_CreatedDate__c <= : Source_DT and IsActive__c = false)';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, list<Sobject> scope){
         try{
                list<Database.deleteResult> srList = Database.delete(scope, false);
                for(Integer counter = 0; counter < srList.size(); counter++) {
                    if (srList[counter].isSuccess()) {
                        result++;
                    }else {
                        for(Database.Error err : srList[counter].getErrors()) {
                            ErrorMap.put(srList.get(counter).id,err.getMessage());
                        }
                    }
                }
                Database.emptyRecycleBin(scope);
            }Catch(exception e){
                EmailNotifier.sendNotificationEmail('Exception from GDPR_DeleteBulkLoyalityAccount ', e.getMessage());
            }
        }
    global void finish(Database.BatchableContext BC) {
        String textBody ='';
        set<Id> allIds = new set<Id>();
        textBody+= result +' records deleted in object Loyalty Account'+'\n';
        if (!ErrorMap.isEmpty()){
            for (Id recordids : ErrorMap.KeySet())
            {
             allIds.add(recordids);
            } 
             textBody+= 'Error log: '+allIds+'\n';
        }
        EmailNotifier.sendNotificationEmail('GDPR Delete Loyality Accounts', textBody);
    }
}