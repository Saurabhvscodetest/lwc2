/**
* @description  Unit tests for Agent Work Trigger Handler
* @author       @stuartbarber
* @created      2017-04-11
*/

@isTest
public class AgentWorkTriggerHandler_TEST {

	/**
	* @description	Set up dependent custom settings		
	* @author		@stuartbarber
	*/

	@testSetup static void initTestData() {

		UnitTestDataFactory.initialiseContactCentreAndTeamSettings();

	}

	/**
	* @description	Check that Agent Work records are updated 			
	* @author		@stuartbarber
	*/

	@isTest static void testHandleAgentWorkBeforeInsert(){
        
		User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		Contact testContact;
		Case testCase;
		List<AgentWork> testAgentWork;

		System.runAs(runningUser) {

	        testContact = UnitTestDataFactory.createContact();
			testContact.Email = 'terry.tibbs@test.com';
			insert testContact;

			testCase = UnitTestDataFactory.createCase(testContact.Id);
			testCase.jl_Branch_master__c = 'Aberdeen';
			testCase.Contact_Reason__c = 'Policy & NKU';
			testCase.Reason_Detail__c = 'Policy';
			insert testCase;

			Case newCase = [SELECT Id, CaseNumber, Contact_Reason__c, Reason_Detail__c FROM Case WHERE Id = :testCase.Id];

			try {

			ServiceChannel testServiceChannel = [SELECT Id
                             FROM ServiceChannel
                             WHERE DeveloperName = 'Case'
                             LIMIT 1];

			testAgentWork = new List<AgentWork>();
			testAgentWork.add(new AgentWork(ServiceChannelId = testServiceChannel.Id,WorkItemId = testCase.Id,UserId = runningUser.Id));
	        insert testAgentWork;

	        AgentWorkTriggerHandler agentTriggerHandler = new AgentWorkTriggerHandler();

	        Test.startTest();
           	agentTriggerHandler.handleAgentWorkBeforeInsert(testAgentWork);             
	        Test.stopTest();

	        AgentWork updatedAgentWork = [SELECT Id, Case_Number__c, Contact_Reason__c, Reason_Detail__c FROM AgentWork WHERE Id IN :testAgentWork];

	        System.AssertEquals(newCase.CaseNumber, updatedAgentWork.Case_Number__c);
	        System.AssertEquals(newCase.Contact_Reason__c, updatedAgentWork.Contact_Reason__c);
	        System.AssertEquals(newCase.Reason_Detail__c, updatedAgentWork.Reason_Detail__c);

	        } catch (Exception e) {
	        	
	        	System.debug('AgentWorkTriggerHandler_TEST - Exception' + e);	
	        }
    	
    	}
	
	}

}