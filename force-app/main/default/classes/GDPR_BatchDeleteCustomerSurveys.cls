/* Description  : GDPR - Customer Survey(VOC) Data Deletion
*
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   26/12/2018        Chanchal Ghodki                  Created                COPT-4278
*
*/

global class GDPR_BatchDeleteCustomerSurveys  implements Database.Batchable<sobject>,Database.Stateful
{
    global Map<Id,String> ErrorMap = new Map<Id,String>();
    global integer Counter=0;
    private static final DateTime  Prior_Months = System.now().addMonths(-13);
   
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id,CreatedDate,Case__c FROM Customer_Survey__c  WHERE CreatedDate <:Prior_Months AND Case__c=NULL' );
    }
    
    global void execute(Database.BatchableContext BC, List<Customer_Survey__c> records)
    {
        if(records.size()>0){
            try{
                list<Database.deleteResult> srList = Database.delete(records, false);
                for(Integer result = 0; result < srList.size(); result++) {
                    if (srList[result].isSuccess()) {
                        Counter++;
                    }else {
                        for(Database.Error err : srList[result].getErrors()) {
                            ErrorMap.put(srList.get(result).id,err.getMessage());
                        }
                    }
                }
                /* Delete records from Recyclebin */
                Database.emptyRecycleBin(records);
            }
            Catch(exception e){
                EmailNotifier.sendNotificationEmail('Exception from GDPR_DeleteCustomerSurveys ', e.getMessage()); 
            }
        }  
    }
    
    global void finish(Database.BatchableContext BC)
    {		  
        String textBody ='';
        set<Id> allIds = new set<Id>();
        textBody += Counter +' records deleted in object Customer Survey'+'\n';
        if (!ErrorMap.isEmpty()){
            for (Id recordids : ErrorMap.KeySet())
            {
                allIds.add(recordids);
            } 
            textBody+= 'Error log: '+allIds+'\n';
        }
        EmailNotifier.sendNotificationEmail('GDPR Delete Customer Surveys',textBody);   
    }
}