/******************************************************************************
* @author       Ranjan Samal
* @date         21/09/2017
* @description  Wrapper class to contain User record and a boolean flag
******************************************************************************/
global class JLUserWrapper{
    
    public User usr {get; set;}
    public Boolean selected {get; set;}

    /*This is the contructor method. When we create a new UserWrapper object we pass a
    User that is set to the usr property. We also set the selected value to false*/
    public JLUserWrapper(User us,Boolean isSelected){
        usr = us;
        selected= isSelected;
    }
}