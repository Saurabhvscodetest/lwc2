/******************************************************************************
* @author       Antony John
* @date         10/10/2015
* @description  Webservice to manage the exceptions that are raised by EventHub.
*
* LOG     DATE        Author    JIRA                            COMMENT
* ===     ==========  ======    ========================        ============ 
* 001     10/10/2015  AJ        CMP-39, CMP-42                  Initial code
*
*/

global with sharing class  SFDCEventHubExceptionsHandler {     
    /* Webservice for manging Eventhub Exceptions */
   
    webservice static EventHubVO.EHExceptionResponse manageEventHubExceptions(EventHubVO.EHExceptionRequest request){
        EventHubVO.EHExceptionResponse ehResponse = null;
        EHBaseExceptionHandler handler;
        String errorMessage = null;
        try {
            errorMessage = validateBasicInformation(request);

            //Check if error message is not populated. 
            if(String.isBlank(errorMessage)){
                if(EHExceptionMapper.isExceptionNeededToBeHandled(request.deliveryType, request.ehExceptionName)){
                    if(EHConstants.GVF.equalsIgnoreCase(request.deliveryType)) {
                        handler = new EHGVFExceptionHandler();
                    } else if(EHConstants.CLICK_AND_COLLECT.equalsIgnoreCase(request.deliveryType)) {
                        handler = new EHClickAndCollectExceptionHandler();
                    } else if(EHConstants.SUPPLIER_DIRECT.equalsIgnoreCase(request.deliveryType)) {
                        handler = new EHSupplierDirectExceptionHandler();
                    }
                    ehResponse = handler.handleEventHubExceptions(request);
                } else {
                    ehResponse =  EHBaseExceptionHandler.getSucessResponse(null, request.ehExceptionId);
                }
            } else {
                 ehResponse = EHBaseExceptionHandler.getErrorResponse(errorMessage,request.ehExceptionId );
            }
        } catch(Exception exp){
            String expId =(request != null) ? request.ehExceptionId : null;
            ehResponse = EHBaseExceptionHandler.getErrorResponse(exp.getMessage() + ' ' + exp.getStackTraceString(), expId) ;    
        }
        if(ehResponse != null && EHConstants.RESPONSE_ERROR.equalsIgnoreCase(ehResponse.status)){
            String jsonRequest = (request != null) ? JSON.serialize(request) : '';
            EmailNotifier.sendNotificationEmail('RAISE EXCEPTION REQUEST FAILURE' , 'ENVIRONMENT - '+ UserInfo.getOrganizationId() + '  \n\r'
                        +' REQUEST - ' + '  \n\r'
                        + jsonRequest + '  \n\r'  + ' FAIL REASON-' + '  \n\r' + ehResponse.errorDescription);
        }
        return ehResponse;
    }  
    
    /* Validate the basic information*/
    private static String validateBasicInformation(EventHubVO.EHExceptionRequest request){
        String errorMessage = null;
        if(request == null) {
            errorMessage = EHConstants.MANDATORY_PARAMETER_MISSING + EHConstants.EXCEPTION_REQUEST ;
        } else {
            //Check if mandatory params are populated.
            if(request.ehExceptionId == null){
                errorMessage = EHConstants.MANDATORY_PARAMETER_MISSING + EHConstants.SPLITTER + EHConstants.EXCEPTION_ID ; 
            } else if (request.ehExceptionName == null) {
                errorMessage = EHConstants.MANDATORY_PARAMETER_MISSING + EHConstants.SPLITTER + EHConstants.EXCEPTION_NAME;
            } else if (String.isBlank(request.deliveryType)) {
                errorMessage = EHConstants.MANDATORY_PARAMETER_MISSING + EHConstants.SPLITTER + EHConstants.DELIVERY_TYPE;
            } else if (String.isNotBlank(request.deliveryType) 
                        && (!(EHConstants.SUPPLIER_DIRECT.equalsIgnoreCase(request.deliveryType)))
                        && (!(EHConstants.CLICK_AND_COLLECT.equalsIgnoreCase(request.deliveryType)))
                        && (!(EHConstants.GVF.equalsIgnoreCase(request.deliveryType)))) {
                errorMessage = EHConstants.INVALID_PARAMETER + EHConstants.SPLITTER + EHConstants.DELIVERY_TYPE + EHConstants.SPLITTER +  request.deliveryType;
            } 
        }
        return errorMessage;
    }
}