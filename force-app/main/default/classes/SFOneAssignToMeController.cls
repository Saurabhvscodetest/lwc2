public class SFOneAssignToMeController {

    public Case cs {get;set;}
    public Case caseRec {get;set;}
    public string AssigntoUserMessage {get;set;}
    public DateTime dt {get;set;}
    public boolean IsError {get;set;}
    ApexPages.StandardController controller;    
    public SFOneAssignToMeController(ApexPages.StandardController con)
    {
        controller = con;
        this.cs = (Case) con.getRecord();       
        dt = Datetime.now();
        Case caseRec = [select Id,  OwnerId FROM Case where Id = : this.cs.Id FOR UPDATE];       
        if(caseRec.OwnerId == UserInfo.getUserId()) {
            AssigntoUserMessage = 'This case is already assigned to you.';
        }else
        {
            AssigntoUserMessage = 'Assigning you to this case.';
        }                       
    }   

    public PageReference assignOwnerToCurrentUser() {
        try {           
            Case caseRec = [select Id, jl_ListView_Is_My_Team__c, OwnerId FROM Case where Id = : this.cs.Id FOR UPDATE];       
            if(caseRec.OwnerId == UserInfo.getUserId()) {
                AssigntoUserMessage = 'This case is already assigned to you.';          
            //}else if(!caseRec.jl_ListView_Is_My_Team__c){
               // AssigntoUserMessage = 'You are not a member of the correct queue for this case to be assigned to you.';          
            }else if(String.valueOf(caseRec.OwnerId).startsWith('005')){
                IsError = true;
                AssigntoUserMessage = 'This Case has already been assigned to another user. Please refresh your Case list and choose a different Case.';
            } else {
                caseRec.OwnerId = UserInfo.getUserId();
                update caseRec;
            }
            return null;
        }
        catch (Exception ex){
            if (ex.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION, ')){
                AssigntoUserMessage = ex.getMessage().substring(ex.getMessage().lastIndexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION, ') + 35);

                if (AssigntoUserMessage.contains('close a case')){
                    AssigntoUserMessage = 'You cannot be assigned to a closed case.';
                }

            }else{
                AssigntoUserMessage = ex.getMessage();
            }           
            return null;
        }               
    }
    
}