@isTest
private class OrdersControllerLEX_Tests {
	
    @isTest public static void getOrderHeadersWithOrderNumber() {

        Config_Settings__c configSearch = new Config_Settings__c();
        configSearch.Name = CustomSettingsManager.WS_Endpoint_SearchCustomer;
        configSearch.Text_Value__c = 'SearchCustomer';
        insert configSearch;

        // set up the config settings
        Config_Settings__c configOrders = new Config_Settings__c();
        configOrders.Name = CustomSettingsManager.WS_Endpoint_ViewCustomerOrdersV1;
        configOrders.Text_Value__c = 'ViewCustomerOrdersV1';
        insert configOrders;

        Test.setMock(HttpCalloutMock.class, new coSoapGetOrderHeaders_MOCK());

        Test.startTest();
        OrderResponseLEX response = OrdersControllerLEX.getOrdersForOrderNumber('12345678');
        Test.stopTest();

      //  System.assertEquals('Success', response.Status, 'Should be success as this is a successfull test ');
      //  System.assertEquals(1, response.Orders.Size(), 'Order not found');
    }

    @isTest public static void getOrderHeadersWithNoOrderNumberError() {
        OrderResponseLEX response = OrdersControllerLEX.getOrdersForOrderNumber('');
        System.assertEquals('Error', response.Status, 'We should have an error as no order number was provided');
        System.assertEquals('Please provide order number', response.ErrorDescription, 'Description is incorrect');
    }


    @isTest public static void getOrdersForCustomerDetailsNoDetailsError() {
        OrderResponseLEX response = OrdersControllerLEX.getOrdersForCustomerDetails('', '','','' );
        System.assertEquals('Error', response.Status, 'We should have an error as no order number was provided');
        System.assertEquals('Please provide customer details', response.ErrorDescription, 'Description is incorrect');
    }

    @isTest public static void getOrdersForCustomerDetailsNoNamesError() {
        OrderResponseLEX response = OrdersControllerLEX.getOrdersForCustomerDetails('', '','test@nowhere.com','HX7 6DJ' );
        System.assertEquals('Error', response.Status, 'We should have an error as no order number was provided');
        System.assertEquals('Firstname and lastName are required fields.', response.ErrorDescription, 'Description is incorrect');
    }

    @isTest public static void getOrdersForCustomerDetailsNoEmailPostcodeError() {
        OrderResponseLEX response =  OrdersControllerLEX.getOrdersForCustomerDetails('Steve', 'Loftus','','' );
        System.assertEquals('Error', response.Status, 'We should have an error as no order number was provided');
        System.assertEquals('Email and Postcode cannot be blank at the same time.', response.ErrorDescription, 'Description is incorrect');
    }

    
    @isTest public static void getOrdersForCustomerDetails() {

        Config_Settings__c configSearch = new Config_Settings__c();
        configSearch.Name = CustomSettingsManager.WS_Endpoint_SearchCustomer;
        configSearch.Text_Value__c = 'SearchCustomer';
        insert configSearch;


        Config_Settings__c configSearchOrders = new Config_Settings__c();
        configSearchOrders.Name = CustomSettingsManager.WS_Endpoint_ViewCustomerOrdersV2;
        configSearchOrders.Text_Value__c = 'SearchCustomerOrders';
        insert configSearchOrders;

        // set up the config settings
        Config_Settings__c configOrders = new Config_Settings__c();
        configOrders.Name = CustomSettingsManager.WS_Endpoint_ViewCustomerOrdersV1;
        configOrders.Text_Value__c = 'ViewCustomerOrdersV1';
        insert configOrders;

        // insert a contact to hang the page off
        Contact searchContact = new Contact();
        searchContact.FirstName = 'Steve';
        searchContact.LastName = 'Loftus';
        searchContact.Email = 'test@nowhere.com';
        searchContact.MailingPostalCode = 'HX7 6DJ';
        insert searchContact;

        Test.setMock(HttpCalloutMock.class, new coSoapGetOrderHeaders_MOCK());

        OrdersControllerLEX.getOrdersForCustomerDetails('Steve', 'Loftus','test@nowhere.com','HX7 6DJ' );

        Test.startTest();
            OrderResponseLEX response = OrdersControllerLEX.getOrdersForCustomerDetails('Steve', 'Loftus','test@nowhere.com','HX7 6DJ' );
        Test.stopTest();        

       // System.assertEquals('Success', response.Status, 'Should be success as this is a successfull test ');
       // System.Debug('LLJ - RESULT '+ response.Orders);
       // System.assertEquals(18, response.Orders.Size(), 'Order not found');
    }
	


}