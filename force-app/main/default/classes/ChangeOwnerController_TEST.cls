@istest
private with sharing class ChangeOwnerController_TEST {
    
    static User sysAdmin;
    static User backOfficeUser;
    public static String TEST_GROUP_NAME = 'test group';

    public static  void AssignRec(){   
        SetUpUsers();

        QueueSobject queue;

        ChangeOwnerController changeOwnerController = new ChangeOwnerController();

        System.runAs(sysAdmin){
            queue = CreateQueues()[0];
        }

        System.runAs(backOfficeUser) {
            changeOwnerController.caseRec = CreateCase();
        }

        changeOwnerController.AssigneeId =  queue.Queue.Id;
        changeOwnerController.QueueName = queue.Queue.Name;
        changeOwnerController.AssignRec();

        System.assertEquals(queue.Queue.Id, changeOwnerController.caseRec.OwnerId);
        System.assertEquals(false, changeOwnerController.displayQueueResults);
        System.assertEquals(true, changeOwnerController.displayAdditionalComment);
        System.assertEquals(true, changeOwnerController.displaySearchPanel);  
    }

    public static testmethod void QueueWithLongName_ShouldBeAllowedToExcludedInOwnerRecords(){   
        SetUpUsers();
        QueueSobject queue;
        /* Please ENSURE that this queue name does not exist in the org */
        String QUEUE_NAME = 'ZZZZZZZZJL Branch supplier direct - exc';
        /* Since the custom settings Name Field can hold upto 38 characters only , So due to this we cannot add the full queue name. */
        String QUEUE_NAME_TRUNCATED = QUEUE_NAME.left(CommonStaticUtils.MAX_SEARCH_TEXT_LENGTH);
        String SEARCH_TEXT = QUEUE_NAME.substring(0, 10);
        
        User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        System.runAs(currentUser) {
        QueueOwnerFilter__c qof = new QueueOwnerFilter__c(Name= QUEUE_NAME_TRUNCATED);
            qof.Filter_from_Change_Owner__c = true;
		insert qof;
        }
        
        ChangeOwnerController changeOwnerController = new ChangeOwnerController();
        changeOwnerController.SearchText = SEARCH_TEXT;
        System.assert(changeOwnerController.OwnerRecords == null , 'Owner Records Must be Empty at the start');

        System.runAs(sysAdmin){
            queue = CreateQueues(QUEUE_NAME)[0];
        }
		
		Test.startTest();
	        changeOwnerController.search();
        Test.stopTest();
        
        Boolean queueNameFound = false;
        for(QueueSobject queueObj : changeOwnerController.OwnerRecords){
			if(queueObj.Queue.Name.equalsIgnoreCase(QUEUE_NAME)){
				queueNameFound = true;
			}        	
        }
        System.assert(!queueNameFound , 'Queue Name must not be found'+ changeOwnerController.OwnerRecords);
    }
    
    public static testmethod  void QueueWithShortName_ShouldBeAllowedToExcludedInOwnerRecords(){   
        SetUpUsers();
        QueueSobject queue;

        User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        System.runAs(currentUser) {
    	QueueOwnerFilter__c qof = new QueueOwnerFilter__c(Name= TEST_GROUP_NAME);
            qof.Filter_from_Change_Owner__c = true;
    	insert qof;
        }

        String SEARCH_TEXT = TEST_GROUP_NAME.substring(0, 2);

        ChangeOwnerController changeOwnerController = new ChangeOwnerController();
        changeOwnerController.SearchText = SEARCH_TEXT;
        System.assert(changeOwnerController.OwnerRecords == null , 'OwnerRecords Must be Empty At the start');

        System.runAs(sysAdmin){
            queue = CreateQueues()[0];
        }
        
		Test.startTest();
	        changeOwnerController.search();
        Test.stopTest();
        
        System.assert(!changeOwnerController.OwnerRecords.isEmpty(), 'OwnerRecords Must be NOT be Empty After Search' + changeOwnerController.OwnerRecords.size());
        Boolean queueNameFound = false;
        for(QueueSobject queueObj : changeOwnerController.OwnerRecords){
			if(queueObj.Queue.Name.equalsIgnoreCase(TEST_GROUP_NAME)){
				queueNameFound = true;
			}        	
        }
        System.assert(!queueNameFound, 'Queue Name must not be found');
    }

    public static testmethod void ReturnToConsole_CaseIsNull(){

        ChangeOwnerController changeOwnerController = new ChangeOwnerController();
        changeOwnerController.RecordId = 'REC12345';
        PageReference pageRef = new PageReference('/apex/serviceconsoleredirect?id=REC12345&IsContact=false&isdtp=vw'); 
        PageReference pageRefToTest = changeOwnerController.ReturnToConsole();
        System.assertEquals(pageRef.getUrl(), pageRefToTest.getUrl(), 'PageReference is differenet');
    }

    public static testmethod void ReturnToConsole_WithCase(){

        SetUpUsers();
        ChangeOwnerController changeOwnerController = new ChangeOwnerController();
        System.runAs(backOfficeUser) {
            changeOwnerController.caseRec = CreateCase();
            changeOwnerController.caseRec.Create_Case_Activity__c = true;
        }

        changeOwnerController.RecordId = 'REC12345';
        PageReference pageRef = new PageReference('/apex/serviceconsoleredirect?id=REC12345&IsContact=true&isdtp=vw'); 
        PageReference pageRefToTest = changeOwnerController.ReturnToConsole();
        System.assertEquals(pageRef.getUrl(), pageRefToTest.getUrl(), 'PageReference is differenet');
    }

    public static testmethod void Constructor_WithNoId(){

        PageReference pageRef = Page.ChangeOwner;
        Test.setCurrentPage(pageRef);
        ChangeOwnerController changeOwnerController = new ChangeOwnerController();
        System.assertEquals(null, changeOwnerController.caseRec.ContactId, 'Case should be null');
        System.assertEquals(1, ApexPages.getMessages().size(), 'Epexcted error message size to be 1');
    }

    public static testmethod void Constructor_WithId(){

        SetUpUsers();
        Case c;

        System.runAs(backOfficeUser) {
            c = CreateCase();
            c.Create_Case_Activity__c = true;
        }

        PageReference pageRef = Page.ChangeOwner;
        pageRef.getParameters().put('id',  c.id);
        Test.setCurrentPage(pageRef);
        ChangeOwnerController changeOwnerController = new ChangeOwnerController();
        System.assertEquals(c.ContactId, changeOwnerController.caseRec.ContactId, 'Case contact id is incorrect');
    }

    public static testmethod void Constructor_WithIncorrectId(){

        PageReference pageRef = Page.ChangeOwner;
        pageRef.getParameters().put('id', '123456');
        Test.setCurrentPage(pageRef);
        ChangeOwnerController changeOwnerController = new ChangeOwnerController();
        System.assertEquals(null, changeOwnerController.caseRec, 'Case should be null');
        System.assertEquals(1, ApexPages.getMessages().size(), 'Epexcted error message size to be 1');
    }



    public static testmethod void ShowSearchBox(){

        ChangeOwnerController changeOwnerController = new ChangeOwnerController();
        changeOwnerController.ShowSearchBox();
        System.assertEquals(false, changeOwnerController.displayQueueResults);
        System.assertEquals(false, changeOwnerController.displayAdditionalComment);
        System.assertEquals(true, changeOwnerController.displaySearchPanel);
    }

    public static testmethod void ShowSearchResults(){

        ChangeOwnerController changeOwnerController = new ChangeOwnerController();
        changeOwnerController.ShowSearchResults();
        System.assertEquals(true, changeOwnerController.displayQueueResults);
        System.assertEquals(false, changeOwnerController.displayAdditionalComment);
        System.assertEquals(true, changeOwnerController.displaySearchPanel);  
    }

    public static testmethod void ShowAdditionalComments(){

        ChangeOwnerController changeOwnerController = new ChangeOwnerController();
        changeOwnerController.ShowAdditionalComments();
        System.assertEquals(false, changeOwnerController.displayQueueResults);
        System.assertEquals(true, changeOwnerController.displayAdditionalComment);
        System.assertEquals(true, changeOwnerController.displaySearchPanel);  
    }

    public static testmethod void HideAllPanels(){

        ChangeOwnerController changeOwnerController = new ChangeOwnerController();
        changeOwnerController.HideAllPanels();
        System.assertEquals(false, changeOwnerController.displayQueueResults);
        System.assertEquals(false, changeOwnerController.displayAdditionalComment);
        System.assertEquals(false, changeOwnerController.displaySearchPanel);
    }

    public static testmethod void Search_WithResults(){

        SetUpUsers();
        List<QueueSobject> queues;
        System.runAs(sysAdmin){
            queues = CreateQueues();
        }

        ChangeOwnerController changeOwnerController = new ChangeOwnerController();
        changeOwnerController.SearchText = 'test';
        changeOwnerController.Search();
        System.assertEquals(2, changeOwnerController.OwnerRecords.size());
    }

    public static testmethod void Search_WithNoResults(){

        SetUpUsers();
        Case c;
        System.runAs(backOfficeUser) {
            c = CreateCase();
            c.Create_Case_Activity__c = true;
        }

        PageReference pageRef = Page.ChangeOwner;
        pageRef.getParameters().put('id',  c.id);
        Test.setCurrentPage(pageRef);   
        ChangeOwnerController changeOwnerController = new ChangeOwnerController();
        changeOwnerController.SearchText = 'noresutlsfound';
        changeOwnerController.Search();
        System.assert(changeOwnerController.OwnerRecords.isEmpty());
        System.assertEquals(1, ApexPages.getMessages().size(), 'Epexcted error message size to be 1');
    }

    public static testmethod void Search_WithNoSearchText(){
        SetUpUsers();

        Case c;
        System.runAs(backOfficeUser) {
            c = CreateCase();
            c.Create_Case_Activity__c = true;
        }

        PageReference pageRef = Page.ChangeOwner;
        pageRef.getParameters().put('id',  c.id);
        Test.setCurrentPage(pageRef);
        ChangeOwnerController changeOwnerController = new ChangeOwnerController();
        changeOwnerController.SearchText = null;
        changeOwnerController.Search();
        System.assert(changeOwnerController.OwnerRecords.isEmpty());
        System.assertEquals(1, ApexPages.getMessages().size(), 'Epexcted error message size to be 1');

    }


    public static testmethod void SaveNormalPath(){
        SetUpUsers();

        Case caseRec;
        QueueSobject queue;
        System.runAs(sysAdmin){
            queue = CreateQueues()[0];
        }

        System.runAs(backOfficeUser) {
            caseRec = CreateCase();
            caseRec.Create_Case_Activity__c = true;
            PageReference pageRef = Page.ChangeOwner;
            pageRef.getParameters().put('id',  caseRec.id);
            Test.setCurrentPage(pageRef); 
            ChangeOwnerController changeOwnerController = new ChangeOwnerController();
            changeOwnerController.AssigneeId =  queue.Queue.Id;
            changeOwnerController.caseRec.jl_Case_RestrictedAddComment__c = 'TestComment';
        
            changeOwnerController.Save();

            PageReference pageRefToTest = new PageReference('/apex/serviceconsoleredirect?id=' + caseRec.id + '&IsContact=true&isdtp=vw'); 
            PageReference pageRefResponseToTest = changeOwnerController.Save();

            System.debug('LLJ  pageRefToTest: ' + pageRefToTest);
            System.debug('LLJ  pageRefResponseToTest: ' + pageRefResponseToTest);

            System.assertEquals(pageRefToTest.getUrl(), pageRefResponseToTest.getUrl(), 'PageReference is differenet');
        }
    }


    public static testmethod void Save_WithNoCaseComment(){

        SetUpUsers();

        Case caseRec;

        QueueSobject queue;

        System.runAs(sysAdmin){
            queue = CreateQueues()[0];
        }

        System.runAs(backOfficeUser) {
            caseRec = CreateCase();
			caseRec.Create_Case_Activity__c = true;
            PageReference pageRef = Page.ChangeOwner;
            pageRef.getParameters().put('id',  caseRec.id);
            Test.setCurrentPage(pageRef); 

            ChangeOwnerController changeOwnerController = new ChangeOwnerController();
            changeOwnerController.AssigneeId =  queue.Queue.Id;
            changeOwnerController.QueueName = queue.Queue.Name;
            changeOwnerController.caseRec.jl_Case_RestrictedAddComment__c = '';
        
            changeOwnerController.Save();

            System.assertEquals(1, ApexPages.getMessages().size(), 'Epexcted error message size to be 1');
        }
    }

    public static testmethod void Save_WithException(){

        SetUpUsers();

        Case caseRec;
        QueueSobject queue;

        System.runAs(sysAdmin){
            queue = CreateQueues()[0];
        }

        System.runAs(backOfficeUser) {
            caseRec = CreateCase();
			caseRec.Create_Case_Activity__c = true;
            PageReference pageRef = Page.ChangeOwner;
            pageRef.getParameters().put('id',  caseRec.id);
            Test.setCurrentPage(pageRef); 

            ChangeOwnerController changeOwnerController = new ChangeOwnerController();
            changeOwnerController.AssigneeId =  queue.Queue.Id;
            changeOwnerController.caseRec = null;
        
            changeOwnerController.Save();

            System.assertEquals(1, ApexPages.getMessages().size(), 'Epexcted error message size to be 1');
        }
    }

    public static testmethod void Save_WithDMLException(){

        SetUpUsers();

        Case caseRec;
        QueueSobject queue;

        System.runAs(sysAdmin){
            queue = CreateQueues()[0];
        }

        System.runAs(backOfficeUser) {
            caseRec = CreateCase();
			caseRec.Create_Case_Activity__c = true;
            PageReference pageRef = Page.ChangeOwner;
            pageRef.getParameters().put('id',  caseRec.id);
            Test.setCurrentPage(pageRef); 

            ChangeOwnerController changeOwnerController = new ChangeOwnerController();
            changeOwnerController.caseRec = new Case();

            changeOwnerController.caseRec.jl_Case_RestrictedAddComment__c = 'TestComment';
        
            changeOwnerController.Save();

            System.assertEquals(1, ApexPages.getMessages().size(), 'Epexcted error message size to be 1');
        }
    }


    private static void SetUpUsers(){
        sysAdmin = UnitTestDataFactory.getSystemAdministrator('admin1');
        backOfficeUser     = UnitTestDataFactory.getBackOfficeUser('bo', 'JL.com - Bookings');

        System.runAs(sysAdmin){
            InitDataForUser(backOfficeUser);
        }
    }


    private static List<QueueSobject> CreateQueues(){       
        return CreateQueues(TEST_GROUP_NAME);  
    }


 	private static List<QueueSobject> CreateQueues(String queueName){       

        Group testGroup1 = new Group(Name = queueName, type='Queue');
        Group testGroup2 = new Group(Name = queueName, type='Queue');

        List<Group> groups = new List<Group>{testGroup1,testGroup2};

        insert groups;

        QueueSobject testQueue1 = new QueueSObject(QueueID = testGroup1.id, SobjectType = 'Case');
        QueueSobject testQueue2 = new QueueSObject(QueueID = testGroup2.id, SobjectType = 'Case');

        List<QueueSobject> queues = new List<QueuesObject>{new QueueSObject(QueueID = testGroup1.id, SobjectType = 'Case'),
                                                           new QueueSObject(QueueID = testGroup2.id, SobjectType = 'Case')};
        insert queues;
        return queues;  
    }

    private static void InitDataForUser(User user){

        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();   
        jl_runvalidations__c jrv = new jl_runvalidations__c(Name = user.Id, Run_Validations__c = true);
        insert jrv; 
    }

    private static Case CreateCase(){

        Case c;     
        Account acc = UnitTestDataFactory.createAccount(1, true);
        Contact ctc = UnitTestDataFactory.createContact(1, acc.id, true);
        c = UnitTestDataFactory.createQueryCase(ctc.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, false);   
        c.Create_Case_Activity__c = true;
        insert c;
        return c;              
    }
}