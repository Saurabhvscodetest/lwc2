@isTest
public with sharing class CustomSettingTestDataFactory {

    public static final String BRANCH_TEAM_CS_NAME = 'Branch - CST Aberdeen';
    public static final String BRANCH_CONTACT_CENTRE_CS_NAME = 'Branch ABERDEEN';
    public static final String BRANCH_QUEUE_NAME = 'Branch_ABERDEEN';
    public static final String CONTACT_CENTRE_TEAM_NAME = 'Hamilton/Didsbury - CST';
    public static final String CONTACT_CENTRE_QUEUE_NAME = 'CST_Hamilton_Didsbury';

    /**
    * @description   Logic to setup Custom Settings for a given Team, inc. Config Settings, Team CS and Contact Centre CS       
    * @author        @stuartbarber
    */
    public static void initialiseTeamCustomSettings(String teamName, String contactCentreName, String queueName){
        
        insert createConfigSettings();
        insert createTeamCustomSetting(teamName, queueName);
        insert createContactCentreCustomSetting(contactCentreName);
    }

    /**** CONTACT CENTRE CUSTOM SETTING CREATION ****/

    public static Contact_Centre__c createContactCentreCustomSetting(String contactCentreName, Integer slaInitialMins, Integer slaInitialMinsWarn){
    
        Contact_Centre__c contactCentreCS = new Contact_Centre__c();
        contactCentreCS.Name = contactCentreName;
        contactCentreCS.Business_Hours__c = contactCentreName;
        contactCentreCS.SLA_Initial_Minutes__c = slaInitialMins;
        contactCentreCS.SLA_Initial_Minutes_Warning__c = slaInitialMinsWarn;
        contactCentreCS.Ignore_Business_Hours_for_SLA_Minutes__c = FALSE;

        return contactCentreCS;
    }

    public static Contact_Centre__c createContactCentreCustomSetting(String contactCentreName){
        return createContactCentreCustomSetting(contactCentreName, 120, 90);                
    }

    public static Contact_Centre__c createContactCentreCustomSettingForBranch(){
        return createContactCentreCustomSetting(BRANCH_CONTACT_CENTRE_CS_NAME, 120, 90);                
    }

    public static Contact_Centre__c createContactCentreCustomSettingForContactCentre(){
        return createContactCentreCustomSetting(CONTACT_CENTRE_TEAM_NAME, 120, 90);             
    }

    /**** TEAM CUSTOM SETTING CREATION ****/

    public static Team__c createTeamCustomSetting(String teamName, String queueName){

        Team__c teamCS = new Team__c();
        teamCS.Name = teamName;
        teamCS.Contact_Centre__c = teamName;
        teamCS.Queue_Name__c = queueName;
        teamCS.Task_Assignee__c = 'null';

        return teamCS;
    }

    public static Team__c createTeamCustomSetting(){
        return createTeamCustomSetting(BRANCH_TEAM_CS_NAME, BRANCH_QUEUE_NAME);

    }

    public static Team__c createTeamCustomSettingForBranch(){
        return createTeamCustomSetting(BRANCH_TEAM_CS_NAME, BRANCH_QUEUE_NAME);
    }

    public static Team__c createTeamCustomSettingForContactCentre(){
        return createTeamCustomSetting(CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_QUEUE_NAME);
    }


    /**** CONFIGURATION SETTING CREATION ****/  

    public static List<Config_Settings__c> createConfigSettings() {

        Id anyUserID = [SELECT Id FROM User WHERE isActive = true LIMIT 1].id;
        Config_Settings__c c = new Config_Settings__c(Name = 'EMAIL_TO_CASE_USER_ID', Text_Value__c = anyUserID);
        Config_Settings__c c1 = new Config_Settings__c(Name = CustomSettingsManager.NEW_CASE_FROM_EMAIL_CLOSED_DAYS , Number_Value__c = 30);
        Config_Settings__c c2 = new Config_Settings__c(Name = CustomSettingsManager.ActSite_Maximum_Security_Trials , Number_Value__c = 5);
        Config_Settings__c c3 = new Config_Settings__c(Name = CustomSettingsManager.ActSite_Maximum_Warning_Trails , Number_Value__c = 2);
        Config_Settings__c c4 = new Config_Settings__c(Name = 'NEW_PSE_CASE_FROM_EMAIL_CLOSED_DAYS', Number_Value__c = 30);
        return new List<Config_Settings__c>{c , c1, c2, c3};
    }

   public static void createCaseActionsConfigSettings() {

       Id anyUserID = [SELECT Id FROM User WHERE isActive = true LIMIT 1].id;

       Config_Settings__c custContactconfig = new Config_Settings__c(Name = 'CASE_ACTION:Customer contacted', Text_Value__c = 'Customer contacted', Checkbox_Value__c = true);
       Config_Settings__c newCaseCreatedconfig = new Config_Settings__c(Name = 'CASE_ACTION:New case created', Text_Value__c = 'New case created', Checkbox_Value__c = false);

       insert custContactconfig;
       insert newCaseCreatedconfig;
   }
}