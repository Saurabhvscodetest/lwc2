/*****************************************************************
**@author Ramachandran 
**  Edit    Date            By          Comment
**  001     15/12/15       Ram          Created. Base Chained Batch Apex Code.Constructor code to accept the parameters.
**  002     17/12/15       Ram          Edited some sections of the code so that they do not execute under Test Execution     
*/


global class CopyCaseEmailMessages_SecondStage implements Database.batchable<SObject>,Database.stateful{

    // Variable used to store the case id in the current context so it is chained till last stage   
    global Set<id> allCaseId{get;set;}
    // Variable used to receive the emailMessageIdList which is processed in the current context
      global set<String> emailMessageIdList{get;set;}
      // Variable used to store the details of the EmailMessage and its corresponding Case, used in the subsequent third stage
       global Map<Id,Id> emailMessageCaseMap = new Map<Id,Id>();
       
       // Constructor used to receive parameters of the EmailMessageIds from First Stage
      global CopyCaseEmailMessages_SecondStage(Set<id> lcaseId, Set<String> emIdList)
      {
           allCaseId =   lcaseId;
            emailMessageIdList = emIdList;
      }
     
    // The Query Locator which returns the content of the EmailMessage and its parent once its id is passed.  
    global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator( [select Id, ParentId from EmailMessage where Id IN: emailMessageIdList]);
      }

    // The Execution stage used to store the EmailCase Map under batch/ successive iterations.
   global void execute(Database.BatchableContext BC, List<sObject> scope){
   List<EmailMessage> emessages= scope;
     for (EmailMessage em: emessages)
     {
          emailMessageCaseMap.put(em.Id, em.ParentId);
     }
        
    }

    //Finish method to call the next batch or third stage passing the values, where attachments are processed.
   global void finish(Database.BatchableContext BC){
       if(!Test.isRunningTest())
       {
           DataBase.ExecuteBatch(new CopyCaseEmailMessages_ThirdStage(allCaseId,emailMessageIdList,emailMessageCaseMap),2);
        }
   }

}