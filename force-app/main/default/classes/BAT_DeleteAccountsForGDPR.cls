/* Description  : GDPR - Delete Accounts from System
*
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   28/03/2019      Vijay Ambati                        Created             COPT-4521
*
*/

global class BAT_DeleteAccountsForGDPR implements Database.Batchable<sObject>,Database.Stateful{
    global Map<Id,String> ErrorMap = new Map<Id,String>();
    global integer result=0;
    global String query;
	global Database.QueryLocator start(Database.BatchableContext BC){
        GDPR_Record_Limit__c gDPRLimit = GDPR_Record_Limit__c.getValues('BAT_DeleteAccountsForGDPR');
        if(gDPRLimit.Record_Limit__c != NULL){
            String recordLimit = gDPRLimit.Record_Limit__c;
            String limitSpace = ' ' + 'Limit' + ' '; 
            query = 'SELECT Id FROM Account where id NOT IN (SELECT AccountId FROM Entitlement) AND id NOT IN (SELECT AccountId FROM Contact) order by createddate asc' + limitSpace + recordLimit;
        }
        else{
            query = 'SELECT Id FROM Account where id NOT IN (SELECT AccountId FROM Entitlement) AND id NOT IN (SELECT AccountId FROM Contact) order by createddate asc';
        }
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, list<Account> scope){
        List<id> recordstoDelete = new List<id>();
            if(scope.size()>0){
        try{
            for(Account recordtoDelete : scope){
                recordstoDelete.add(recordtoDelete.id);
            }
            list<Database.deleteResult> srList = Database.delete(recordstoDelete, false);
            for(Integer counter = 0; counter < srList.size(); counter++) {
                if (srList[counter].isSuccess()) {
                    result++;
                }else {
                    for(Database.Error err : srList[counter].getErrors()) {
                        ErrorMap.put(srList.get(counter).id,err.getMessage());
                    }
                }
            }
            Database.emptyRecycleBin(scope);
        }Catch(exception e){
            EmailNotifier.sendNotificationEmail('Exception from BAT_DeleteAccountsForGDPR ', e.getMessage());
        }}
    }
    global void finish(Database.BatchableContext BC) {
        String textBody ='';
        set<Id> allIds = new set<Id>();
        textBody+= result +'Delete Accounts'+'\n';
        if (!ErrorMap.isEmpty()){
            for (Id recordids : ErrorMap.KeySet())
            {
                allIds.add(recordids);
            } 
          //textBody+= 'Error log: '+allIds+'\n';
        }
        EmailNotifier.sendNotificationEmail('BAT_DeleteAccountsForGDPR', textBody);
    }
}