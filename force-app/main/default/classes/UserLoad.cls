/******************************************************************************
* @author       Kevin Burke
* @date         14.04.2014
* @description  Class that assists with loading of users
* there is a companion s/s to this called 
* dependent picklist in Excel outlined in http://www.excel-easy.com/examples/dependent-drop-down-lists.html
******************************************************************************/

public class UserLoad {

    public static String calculateProfile (Load_User__c lu)
    {
        if (lu.SiteType__c == 'Branch')
        {
            if (lu.function__c == 'CCLA')
            {
                return 'JL: Branch CCLA';
            }
            else
            {
                return 'JL: Case Profile';
            }
        }
        if (lu.SiteType__c == 'CDH')
        {
            return 'JL: Case Profile';
        }
        if (lu.SiteType__c == 'ContactCentre' || lu.SiteType__c == 'CC')
        {
            if (lu.Tier__c == '1')
            {
                return 'JL: ContactCentre/CDH Tier1';
            }
            else
            {
                return 'JL: ContactCentre Tier2-3';
            }
        }
        if (lu.SiteType__c == 'OPSCustomerDelivery')
        {
            return 'JL: Case Profile';
        }
        if (lu.SiteType__c == 'OPSDeliveryPerformance')
        {
            return 'JL: Case Profile';
        }
        return null;
    }

    public static String calculateTier (Load_User__c lu)
    {
        if (lu.SiteType__c == 'Branch')
        {
            if (lu.function__c == 'Management')
            {
                return '3';
            }
            else
            {
                return '2';
            }
        }
        if (lu.SiteType__c == 'CDH')
        {
            return '2';
        }
        if (lu.SiteType__c == 'ContactCentre' || lu.SiteType__c == 'CC')
        {
            if (lu.function__c == 'COT') return '2';
            if (lu.function__c == 'CRD:Allocation') return '3';
            if (lu.function__c == 'CRD:SocialMedia') return '3';
            if (lu.function__c == 'CSA') return '1';
            if (lu.function__c == 'CST') return '2';
            if (lu.function__c == 'EHT:POS') return '2';
            if (lu.function__c == 'EmailTeam') return '2';
            if (lu.function__c == 'GiftList') return '2';
            if (lu.function__c == 'JLTS:CRM') return '3';
            if (lu.function__c == 'Management') return '3';
            if (lu.function__c == 'Management:TeamManagers') return '3';
            if (lu.function__c == 'ProductSupportTeam') return '1'; // todo - this is awaiting confirmation.
            if (lu.function__c == 'JL.com - CRM') return '3'; 
            if (lu.function__c.startsWith('JL.com')) return '2';
        }
        if (lu.SiteType__c == 'OPSCustomerDelivery')
        {
            return '3';
        }
        if (lu.SiteType__c == 'OPSDeliveryPerformance')
        {
            return '3';
        }

        return null;
    }

    public static String calculateRole (Load_User__c lu)
    {

        String function = ':' + lu.function__c ;
        
        if (lu.function__c == null || lu.function__c.trim().length() == 0 || lu.function__c.trim() == 'Not Applicable')
        {
            function = '';
        }

        if (lu.SiteType__c == 'Branch')
        {
            String branchsiteformat = lu.site__c.replace(' ','').toUpperCase();
            return 'JLP:Branch:' + branchsiteformat + ':' + lu.function__c + ':T' + lu.tier__c;
        }
        if (lu.SiteType__c == 'CDH')
        {
            String cdhsiteformat = lu.site__c.replace(' ','');
            return 'JLP:CDH:' + cdhsiteformat + ':' + lu.function__c + ':T' + lu.tier__c;
        }
        if (lu.SiteType__c == 'ContactCentre' || lu.SiteType__c == 'CC')
        {
            if (lu.function__c.startsWith('JL.com'))
            {
                String strJLRole = 'JLP:CC:JLCOM:BackOffice:';
                
                if (lu.function__c.contains('Bookings'))
                {
                    strJLRole += 'Bookings';
                }
                if (lu.function__c.contains('Carrier'))
                {
                    strJLRole += 'Carrier';
                }
                if (lu.function__c.contains('CRM'))
                {
                    strJLRole += 'CRM';
                }
                if (lu.function__c.contains('CST'))
                {
                    strJLRole += 'CST';
                }
                if (lu.function__c.contains('D2C'))
                {
                    strJLRole += 'D2C';
                }
                if (lu.function__c.contains('Returns'))
                {
                    strJLRole += 'ReturnsRefunds';
                }
                if (lu.function__c.contains('Held'))
                {
                    strJLRole += 'HeldOrders';
                }
                if (lu.function__c.contains('Gift Card'))
                {
                    strJLRole += 'MyJL';
                }
                if (lu.function__c.contains('NKU'))
                {
                    strJLRole += 'NKU';
                }
                if (lu.function__c.contains('CO Sample Sales'))
                {
                    strJLRole += 'COSampleSales';
                }
                if (lu.function__c.contains('Email'))
                {
                    strJLRole = 'JLP:CC:JLCOM:EmailTeam';
                }
                
                strJLRole += ':T' + lu.tier__c;
                return strJLRole;
            }
            else
            {
                return 'JLP:CC:' + lu.function__c + ':T' + lu.tier__c;
            }
        }

        String oppssiteformat = '';

        if (lu.site__c != null)
        {
            oppssiteformat = lu.site__c.replace(' ','').trim();
            if (oppssiteformat.trim().length() == 0 || oppssiteformat == 'NotApplicable')
            {
                oppssiteformat = '';
            }
            else
            {
                oppssiteformat = ':' + oppssiteformat ;
            }
        }


        if (lu.SiteType__c == 'OPSCustomerDelivery')
        {
            return 'JLP:OPS:CustomerDelivery' + oppssiteformat + function + ':T' + lu.tier__c;
        }
        if (lu.SiteType__c == 'OPSDeliveryPerformance')
        {
            return 'JLP:OPS:DeliveryPerformance:' + lu.function__c + ':T' + lu.tier__c;
        }
        return null;
    }

    public static Date addweeks (Decimal w)
    {
        if (w != null && w > 0)
        {
            Integer addedweeks = (Integer) w;
            return Date.Today().addDays(7 * addedweeks);
        }
        return null;
    }

    
    public static String calculatePrimaryTeam (Load_User__c lu)
    {
        if (lu.SiteType__c == 'Branch')
        {
            if (lu.function__c == 'CCLA')
            {
                return 'Branch - CCLA ' + lu.site__c;
            }
            else
            {
                return 'Branch - CST '  + lu.site__c;
            }
        }
        if (lu.SiteType__c == 'CDH')
        {
            return 'CDH - '  + lu.site__c;
        }
        if (lu.SiteType__c == 'ContactCentre'  || lu.SiteType__c == 'CC')
        {
            if (lu.site__c == 'JLTS') return 'JL.com - CRM';
            if (lu.function__c == 'COT') return 'Hamilton - COT';
            if (lu.function__c == 'CRD:Allocation') return 'Hamilton - CRD';
            if (lu.function__c == 'CRD:SocialMedia') return 'Hamilton - CRD';
            if (lu.function__c == 'CSA') return 'Hamilton/Didsbury - CSA';
            if (lu.function__c == 'CST') return 'Hamilton/Didsbury - CST';
            if (lu.function__c == 'EHT:POS') return 'Hamilton - EHT POS';
            if (lu.function__c == 'GiftList') return 'Hamilton - Gift List team';
            if (lu.function__c == 'EmailTeam') return 'Didsbury Email Team';
            if (lu.function__c == 'ProductSupportTeam') return 'Hamilton - Product expert team';
            if (lu.function__c == 'JLTS:CRM') return 'JL.com - CRM';
            if (lu.function__c == 'Management' && lu.site__c == 'JL.com') return 'JL.com - CST'; // may need to be changed manually on load by users
            if (lu.function__c == 'Management') return 'Hamilton/Didsbury - CSA'; // may need to be changed manually on load by users
            if (lu.function__c == 'Management:TeamManagers') return 'Hamilton/Didsbury - CSA'; // may need to be changed manually on load by users
            if (lu.function__c.startsWith('JL.com')) return lu.function__c;
        }
        if (lu.SiteType__c == 'OPSDeliveryPerformance')
        {
            return 'Customer Services - Distribution';
        }
        if (lu.SiteType__c == 'OPSCustomerDelivery')
        {
            return 'Customer Services - Distribution';
        }
        return null;
    }

    
    public static string getUserDomain()
    {
        String strReturn = UserInfo.getUsername().split('@')[1];
        // if the user is .com then replace with .co.uk for John Lewis consistency
        return modifyUserDomain(strReturn);
    }

    public static string modifyUserDomain(String strInput)
    {
        // if the user is .com then replace with .co.uk for John Lewis consistency
        string strReturn = strInput.replaceAll('.com','.co.uk');
        return strReturn;
    }
 

    public static string getJLLicenseType(String strRole, String strContract)
    {
        String strReturn = null;
        
        if (strRole != null)
        {
            if (strRole.startsWith('JLP:Branch'))
            {
                if ('PEAK TEMP'.equals(strContract))
                {
                    strReturn = 'Branch User (peak)';
                }
                else
                {
                    strReturn = 'Branch User';
                }
            }
            if (strRole.startsWith('JLP:CC'))
            {
                if ('PEAK TEMP'.equals(strContract))
                {
                    strReturn = 'Contact Centre (peak)';
                }
                else
                {
                    strReturn = 'Contact Centre';
                }
            }
            if (strRole.startsWith('JLP:CDH'))
            {
                strReturn = 'Distribution/CDH';
            }
            if (strRole.startsWith('JLP:OPS'))
            {
                strReturn = 'Distribution/CDH';
            }
            if (strRole.startsWith('JLP:HeadOffice'))
            {
                strReturn = 'Head Office';
            }
        }
        return strReturn;

    }



    public static string createUserName(Load_User__c l)
    {
        String lastname = l.Last_Name__c.replaceAll('\'','');
        lastname = lastname.replaceAll('-','');
        lastname = lastname.replaceAll(' ','');
        
        if (lastname.length() > 7)
        {
            lastname = lastname.substring(0,7);
        }
        
        String strReturn = l.First_Name__c.substring(0,1) + lastname + l.Netdom__c + '@' + getUserDomain();
        
        System.debug('Username is ' + strReturn);
        
        return strReturn.toLowerCase();
    }


    public static void enrichLoadUsers (List<Load_User__c> listl)
    {
        for (Load_User__c l : listl)                
        {
            enrichLoadUser(l);
        }
    }

    public static void enrichLoadUser (Load_User__c l)
    {
        
    }


    public static List<User>  createUserObjects (List<Load_User__c> listl)
    {
        
        List<User> listu = new List<User>();
        
        List<Profile> listp = [select id, name from Profile];
        Map<String,Profile> mapProfile = new Map<String,Profile>();
        for (Profile p : listp)
        {
            mapProfile.put(p.name,p);
        }

        List<UserRole> listur = [select id, name from UserRole];
        Map<String,UserRole> mapUserRole = new Map<String,UserRole>();
        for (UserRole ur : listur)
        {   
            mapUserRole.put(ur.name,ur);
        }

        Set<Id> setLoadUserDoneIds = new Set<Id>();


        for (Load_User__c l : listl)
        {

            system.debug(l);
            User u = new User();

            u.Username=createUserName(l);

            u.CommunityNickname=l.First_Name__c + '.' + l.Last_Name__c + '.' + l.Netdom__c;
            u.Department = l.Department__c;
            u.De_activation_Date__c = addweeks(l.Access_Weeks__c);

            Integer intExistingCommunityNick = [select count() from User where CommunityNickname = :u.CommunityNickname];
            
            if (intExistingCommunityNick > 0)
            {
                u.CommunityNickname = u.CommunityNickname + '.' + l.First_Name__c.substring(0,1) + l.Last_Name__c.substring(0,1) ;  
            }

            // Names
            u.FirstName=l.First_Name__c;
            u.LastName=l.Last_Name__c;
            u.Alias=l.Netdom__c;
            u.Net_Dom_ID__c=l.Netdom__c;
        
            // TBD
            u.IsActive=true;
            u.SenderEmail=null;
        
            // Profile and Role  
            u.ProfileId = mapProfile.get(l.Profile__c).Id;
            u.UserRoleId = mapUserRole.get(l.Role__c).Id;
            
            // Team
            u.Team__c = l.Primary_Team__c;
            u.jl_Has_No_Team__c = (l.Primary_Team__c != null) ? false : true;
            u.jl_license_type__c = getJLLicenseType(l.Role__c,l.contract__c);
            u.Email = (l.email__c != null) ? l.email__c : 'salesforce_null@johnlewis.co.uk'; 
            u.Trusted_User__c = (l.sst__c) ? true : false;

            // Fixed values
        
            u.Country='United Kingdom';
            u.CountryCode='GB';
            u.DefaultGroupNotificationFrequency='N';
            u.EmailEncodingKey='ISO-8859-1';
            u.ForecastEnabled=false;
            u.LanguageLocaleKey='en_US';
            u.LocaleSidKey='en_GB';
            u.ReceivesAdminInfoEmails=false;
            u.ReceivesInfoEmails=false;
            u.TimeZoneSidKey='Europe/London';
            u.UserPermissionsCallCenterAutoLogin=false;
            
            //Replace the failing keys OCCO-27464
            u.put('UserPermissionsChatterAnswersUser',false);
            u.put('UserPermissionsInteractionUser',true);
            u.put('UserPermissionsMarketingUser',false);
            u.put('UserPermissionsMobileUser',false);
            u.put('UserPermissionsOfflineUser',false);
            u.put('UserPermissionsSFContentUser',false);
            u.put('UserPermissionsSupportUser',true);

            u.UserPreferencesActivityRemindersPopup=true;
            u.UserPreferencesApexPagesDeveloperMode=false;
            u.UserPreferencesDisableAllFeedsEmail=false;
            u.UserPreferencesDisableBookmarkEmail=false;
            u.UserPreferencesDisableChangeCommentEmail=false;
            u.UserPreferencesDisableFileShareNotificationsForApi=false;
            u.UserPreferencesDisableFollowersEmail=false;
            u.UserPreferencesDisableLaterCommentEmail=false;
            u.UserPreferencesDisableLikeEmail=true;
            u.UserPreferencesDisableMentionsPostEmail=false;
            // taken out when Private Messages disabled                u.UserPreferencesDisableMessageEmail=false;
            u.UserPreferencesDisableProfilePostEmail=false;
            u.UserPreferencesDisableSharePostEmail=false;
            u.UserPreferencesDisCommentAfterLikeEmail=false;
            u.UserPreferencesDisMentionsCommentEmail=false;
            u.UserPreferencesDisProfPostCommentEmail=false;
            u.UserPreferencesEnableAutoSubForFeeds=false;
            u.UserPreferencesEventRemindersCheckboxDefault=true;
            u.UserPreferencesHideCSNDesktopTask=false;
            u.UserPreferencesHideCSNGetChatterMobileTask=false; 
            u.UserPreferencesOptOutOfTouch=false;
            u.UserPreferencesShowPostalCodeToGuestUsers=false;
            u.UserPreferencesReminderSoundOff=false;
            u.UserPreferencesShowEmailToExternalUsers=false;
            u.UserPreferencesShowFaxToExternalUsers=false;
            u.UserPreferencesShowManagerToExternalUsers=false;
            u.UserPreferencesShowMobilePhoneToExternalUsers=false;
            u.UserPreferencesShowPostalCodeToExternalUsers=false;
            u.UserPreferencesShowProfilePicToGuestUsers=false;
            u.UserPreferencesShowStateToExternalUsers=false;
            u.UserPreferencesShowStateToGuestUsers=false;
            u.UserPreferencesShowStreetAddressToExternalUsers=false;
            u.UserPreferencesShowTitleToExternalUsers=true;
            u.UserPreferencesShowTitleToGuestUsers=false;
            u.UserPreferencesShowWorkPhoneToExternalUsers=false;
            u.UserPreferencesTaskRemindersCheckboxDefault=true;
            listu.add(u);
            setLoadUserDoneIds.add(l.id);
                

        }
        
        return listu;
    }
    
    @future
    public static void setLoadUserDone (Set<Id> setIds)
    {
        List<Load_User__c> listlu = [select id, done__c from load_user__c where id IN :setids];
        
        for (Load_User__c lu : listlu)
        {
            lu.done__c = true;
        }
        
        update listlu;
    }
    
    public static String validateLoadUser (Load_User__c l)
    {

        String strReturn = null;
        
        Schema.DescribeFieldResult fieldResult = User.JL_License_Type__c.getDescribe();
        List<Schema.PicklistEntry> listple = fieldResult.getPicklistValues();
        
        Set<String> licensetypevalues = new Set<String>();
        for (Schema.Picklistentry ple : listple)
        {
            licensetypevalues.add(ple.getValue());
        }
        
        List<Team__c> listt = [select name from team__c];
        Set<String> setteams = new Set<String>();
        for (Team__c t : listt)
        {
            setteams.add(t.name);
        }

        List<Profile> listp = [select id, name from Profile];
        Set<String> setProfile = new Set<String>();
        for (Profile p : listp)
        {
            setProfile.add(p.name);
        }

        List<UserRole> listur = [select id, name from UserRole];
        Set<String> setUserRole = new Set<String>();
        for (UserRole ur : listur)
        {   
            setUserRole.add(ur.name);
        }
        
        System.debug ('Checking user: ' +  l.First_Name__c + ' ' +  l.Last_Name__c);
        
        if (l.netdom__c == null)
        {
            strReturn = 'Net Dom is null for user: ' +  l.First_Name__c + ' ' +  l.Last_Name__c;
        }
        else
        {
            String netdomcheck = validateNetDom (l.netdom__c);
            if (netdomcheck != null)
            {
                strReturn = netdomcheck;
            }
        }

        if (l.First_Name__c == null)
        {
            strReturn = 'First name is not provided';
        }

        if (l.Last_Name__c == null)
        {
            strReturn = 'Last name is not provided';
        }

        if (!setteams.contains(l.Primary_Team__c))
        {
            strReturn = 'No team named: ' + l.Primary_Team__c;
        }

        if (!setProfile.contains(l.Profile__c))  
        {
            strReturn = 'No profile named: ' + l.Profile__c + ' for Load User ' + l.name ;
        }

        if (!setUserRole.contains(l.Role__c))  
        {
            strReturn = 'No role named: ' + l.Role__c + ' for Load User ' + l.name ;
        }
        System.debug ('Validated settings for user: ' +  l.First_Name__c + ' ' +  l.Last_Name__c);
        return strReturn;
    }

    public static string validateNetDom (String s)
    {
        if (s == null)
        {
            return 'No NetDom id provided';
        }
        if (s.length() != 6)
        {
            return 'NetDom id must be 6 characters';
        }
        if (!s.isAlphanumeric())
        {
            return 'NetDom id must have format XX9999 (two letters, four numbers)';
        }
        String numeric = s.substring(2,6);
        if (!numeric.isNumeric())
        {
            return 'NetDom id must have format XX9999 (two letters, four numbers)';
        }
        String alpha = s.substring(0,2);
        if (!alpha.isAlpha())
        {
            return 'NetDom id must have format XX9999 (two letters, four numbers)';
        }
        return null;
    }


}