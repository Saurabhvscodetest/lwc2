/** 
 ** Test of the batch process for Contact Profiles
  * 001  27/06/16    NA                  Decommissioning contact profile  
 */
@isTest
private class myJL_ContactProfileHouseKeepingTest {

	static {
		//prepare configuration
		Database.insert(new House_Keeping_Config__c(Name = 'ContactProfileBatch', Class_Name__c = 'myJL_ContactProfileHouseKeeping', Order__c = 0, Run_Frequency_Days__c = 1));
	}
	
	/**
	 * check that log headers are deleted when custom setting is set
	 */
	private static testMethod void testOK () {
		CustomSettings.setTestLogHeaderConfig(true, -1);

		List<contact> cps = new List<contact>();
		cps.add(new contact(firstname='True', lastname='WillbeFalse', AnonymousFlag__c=true, Batch_Processed__c=false)); //<<001>>
		cps.add(new contact(firstname='False', lastname='WillbeTrue', AnonymousFlag__c=false, Batch_Processed__c=false)); //<<001>>
		cps.add(new contact(firstname='True', lastname='WasTrueWillStillbeFalse', AnonymousFlag__c=true, AnonymousFlag2__c=false, Batch_Processed__c=true)); //<<001>>
		cps.add(new contact(firstname='False', lastname='WasFalseWillStillbeTrue', AnonymousFlag__c=false, AnonymousFlag2__c=true, Batch_Processed__c=true)); //<<001>>
		insert cps;

		Test.startTest();
		myJL_ContactProfileHouseKeeping cpHK = new myJL_ContactProfileHouseKeeping();
		system.assertNotEquals(null, cpHK);
 		Database.executeBatch(new HouseKeeperBatch(cpHK, true));
		Test.stopTest();

		Map<Id, contact> cpMap = new Map<Id, contact>([SELECT Id, AnonymousFlag__c, AnonymousFlag2__c FROM contact]); //<<001>>
		system.assertEquals(4, cpMap.size());
		
		// get the contact profiles from the map so we can compare before and after
		contact cp0 = (contact)cpMap.get(cps[0].Id); //<<001>>
		system.assertNotEquals(null, cp0,'Did not get cp0');
		contact cp1 = (contact)cpMap.get(cps[1].Id); //<<001>>
		system.assertNotEquals(null, cp0,'Did not get cp1');
		contact cp2 = (contact)cpMap.get(cps[2].Id); //<<001>>
		system.assertNotEquals(null, cp0,'Did not get cp2');
		contact cp3 = (contact)cpMap.get(cps[3].Id); //<<001>>
		system.assertNotEquals(null, cp0,'Did not get cp3');
		
		// check the results the original flag should be as-is
		system.assertEquals(true, cp0.AnonymousFlag__c);
		system.assertEquals(false, cp1.AnonymousFlag__c);
		system.assertEquals(true, cp2.AnonymousFlag__c);
		system.assertEquals(false, cp3.AnonymousFlag__c);

		// check the expected results in the new field
		system.assertEquals(false, cp0.AnonymousFlag2__c);
		system.assertEquals(true, cp1.AnonymousFlag2__c);
		system.assertEquals(false, cp2.AnonymousFlag2__c);
		system.assertEquals(true, cp3.AnonymousFlag2__c);

		//check that Last_Run_Time__c was set
		final Map<String, House_Keeping_Config__c> configMap = House_Keeping_Config__c.getAll();
		final House_Keeping_Config__c config = configMap.get('ContactProfileBatch');
		System.assert(config.Last_Run_Time__c >= System.today(), 'Expected Last_Run_Time__c to be set');
	}
}