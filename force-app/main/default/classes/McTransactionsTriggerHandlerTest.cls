@isTest
public class McTransactionsTriggerHandlerTest {

    static {
        Config_Settings__c defaultNotificationEmail = 
        new Config_Settings__c(Name = 'EMAIL_NOTIFICATION_DEFAULT_ADDRESS', Text_Value__c = 'test@test.co.uk');
        insert defaultNotificationEmail;
        try{
            Test.loadData(Constants__c.sObjectType, 'Constants');
        }catch(Exception e){
          System.debug('Exception'+e.getMessage());
        }
    }
    
    @isTest
    public static void setTestCommentOnCaseActivity(){
      	Id anyUserID = [select id from user where isActive=true limit 1].id;
        Config_Settings__c emailToCase = new Config_Settings__c(name='EMAIL_TO_CASE_USER_ID',text_value__c = anyUserID);
        insert emailToCase;	
        
        Contact testContact;
        	Case testCase;
        	testContact = UnitTestDataFactory.createContact();
            testContact.Email = 'tester@testing.com';
            insert testContact;
            
            
            testCase = UnitTestDataFactory.createCase(testContact.Id);
            insert testCase;
        
        MC_Transactions__c mc = new MC_Transactions__c();
        List<Case> caseList = [select CaseNumber from Case where id =: testCase.Id];
        mc.Case_Number__c = caseList[0].CaseNumber;
        mc.Keyword__c = 'Phone';
        mc.Keyword_ID__c = 'abc';
        mc.Name = 'xyz';
        mc.Message__c='Phone after 8 PM';
        mc.Priority__c=1;
        test.startTest();
        insert mc;
        test.stopTest();
    }
}