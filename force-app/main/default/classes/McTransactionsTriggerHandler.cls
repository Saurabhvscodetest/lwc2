/* Description  : OFM
*
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   08/05/2019      Vignesh Kotha                      Modified              COPT-4535
*
*/
public class McTransactionsTriggerHandler {
    public static void mainEntry(Boolean isBefore, 
                                 Boolean isDelete, Boolean isAfter,
                                 Boolean isInsert, Boolean isUpdate,
                                 Boolean isExecuting, List<MC_Transactions__c> newList,
                                 Map<Id, SObject> newMap, List<MC_Transactions__c> oldList,
                                 Map<Id, SObject> oldMap) {                                     
                                     System.debug('@@entering MC_TransactionsTriggerHandler');                                     
                                     McTransactionsTriggerHandler handler = new McTransactionsTriggerHandler();
                                     if(isInsert && isAfter) {
                                         Map<ID,MC_Transactions__c> newValues = (Map<ID,MC_Transactions__c>)newMap;
                                         Map<ID,MC_Transactions__c> oldValues = (Map<ID,MC_Transactions__c>)oldMap;
                                         handler.handleAfterInsert(oldValues , newValues);
                                     }                                     
                                 }    
    /*
* Perform all After Insert trigger actions on Trigger.new
* @params: Map<ID,MC_Transactions__c> newValues      Map of new MC_Transactions__c (corresponds to Trigger.newMap).
*          Map<ID,MC_Transactions__c> oldValues      Map of old MC_Transactions__c (corresponds to Trigger.oldMap).
* @rtnval: void
*/    
    public void handleAfterInsert(Map<ID,MC_Transactions__c> oldValues, Map<ID,MC_Transactions__c> newValues){
        List<Case_Activity__c> relatedCaseActivities = new List<Case_Activity__c>();
        for(MC_Transactions__c mc : newValues.Values()){
            if(MC.Case_Number__c != null){
                List<Case> relatedCase = getCase(mc.Case_Number__c);
                //Storing Customer Message in a variable
                string commenttoinsert = mc.Message__c;
                // customer not give response for invalidmobile and smsfailure
                if(mc.Keyword__c =='InvalidMobile' || mc.Keyword__c =='SMSFailure'){
                    commenttoinsert = 'TXT';
                }
                //Generating Unique key
                String appendedKey = mc.Keyword__c+'KEYWORD'; 
                // passing parameters to insert message
                if(mc.Keyword__c!=NULL){
                    relatedCaseActivities =  getCaseActivityForCase(relatedCase , mc.Keyword__c,appendedKey,commenttoinsert);
                    addCaseCommentOnCase(relatedCase,getCommentFromKeyword(mc.Keyword__c),appendedKey,commenttoinsert,mc.Keyword__c);
                }
            }
        }
        if(relatedCaseActivities.size()>0){
            try{
                update relatedCaseActivities;
            } 
            catch(Exception Ex){
                system.debug('Error while updating case activity record'+ Ex.getMessage());
            }
        }
    }
    
    public static List<Case> getCase(string caseNumber){
        list<Case> foundCase = [select id,businessHoursId from Case where CaseNumber = :caseNumber];
        return foundCase;
    }
    
    public static List<Case_Activity__c> getCaseActivityForCase(List<Case> caseList , String keyWord,string appendedKey,string commenttoinsert){
        System.debug('getCaseActivityForCase###');
        List<Case_Activity__c> caseActivityRecords = new List<Case_Activity__c>();
        for(Case_Activity__c cActivity: [Select id, Activity_End_Date_Time__c,Activity_Start_Date_Time__c,CDH_Site__c,Case_Branch_Department__c,
                                         jl_Branch_master__c,Case_Reason_Detail__c,Skip_NextActionIfOpen_Validation__c,Case__c, Resolution_Method__c FROM Case_Activity__c WHERE Case__c IN :caseList AND Resolution_Method__c = NULL ORDER BY CreatedDate DESC LIMIT 1]){
                                             
                                             updateCaseActivityDate(cActivity);
                                             addCaseCommentOnActivity(cActivity , getCommentFromKeyword(keyWord),appendedKey,commenttoinsert,keyWord);
                                             caseActivityRecords.add(cActivity);
                                             
                                         }
        return caseActivityRecords;
    }
    
    public static Case_Activity__c addCaseCommentOnActivity(Case_Activity__c ca, String caseActivityComment,string appendedKey,string commenttoinsert,String keyWord ){
        String FinalComment = caseActivityComment;
        String keywordUpper = appendedKey.toUpperCase();
        String commenttoinsert1;
        if(commenttoinsert!=NULL){
            commenttoinsert1 = commenttoinsert;
            FinalComment = FinalComment.replaceAll(keywordUpper, commenttoinsert1);
        }else{
            FinalComment = FinalComment.replaceAll(keywordUpper, keyWord.toUpperCase());
        }
        ca.Add_Comment__c = FinalComment;
        return ca;
    }    
    public static void addCaseCommentOnCase(List<Case> caseList, String caseComment, string keyword, String commenttoinsert,String keywrd){
        List<CaseComment> childComment = new List<CaseComment>();
        for(Case c :caseList){
            CaseComment newComment = new CaseComment();
            String FinalComment = caseComment;
            String keywordUpper = keyword.toUpperCase();
            String commenttoinsert1;
            if(commenttoinsert!=NULL){
                commenttoinsert1 = commenttoinsert;
                FinalComment = FinalComment.replaceAll(keywordUpper, commenttoinsert1);
            }else{
                FinalComment = FinalComment.replaceAll(keywordUpper, keywrd.toUpperCase());
            }
            
            newComment.CommentBody = FinalComment;
            newComment.ParentId = c.id;
            childComment.add(newComment);
        }
        if(!childComment.isEmpty()){
            insert childComment;
        }
    }
    
    public static String getCommentFromKeyword(String keyword){
        if(keyword!=NULL){
            String keywordUpper = keyword.toUpperCase();
            List<EH_SMS_Keyword__mdt> keywordList = [SELECT Case_Comment__c FROM EH_SMS_Keyword__mdt WHERE name__c =: keywordUpper LIMIT 1];
            if(keywordList.size()>0){
                return keywordList[0].Case_Comment__c;  
            }
        }
        return null;
    }
    public static Case_Activity__c updateCaseActivityDate(Case_Activity__c cActivity){
        List<Case> caseList = [Select businessHoursId from Case where id =: cActivity.Case__c];
        cActivity.Activity_Start_Date_Time__c = BusinessHours.nextStartDate(caseList[0].businessHoursId ,System.now()); 
        cActivity.Activity_End_Date_Time__c =cActivity.Activity_Start_Date_Time__c.addHours(2);
        cActivity.Skip_NextActionIfOpen_Validation__c = true;
        String DateTimeValueStart = cActivity.Activity_Start_Date_Time__c.format('dd/MM/yyyy HH:mm');
        String DateTimeValueEnd = cActivity.Activity_Start_Date_Time__c.date().isSameDay(cActivity.Activity_End_Date_Time__c.date()) ? String.valueOf(cActivity.Activity_End_Date_Time__c.time()).left(5) : cActivity.Activity_End_Date_Time__c.format('dd/MM/yyyy HH:mm');
        cActivity.Date__c = DateTimeValueStart +' - '+ DateTimeValueEnd;
        return cActivity;
    }
}