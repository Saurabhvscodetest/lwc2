/******************************************************************************
* @author       SFDC for drop 1
* @date         Unknown
* @description  Utility class containing team assignment operations.
*
* RENAMED FROM ORIGINAL clsTeam CLASS
*
* EDIT RECORD
*
* LOG   DATE        Author  JIRA        COMMENT      
* 001   Unknown     SFDC                Original code.
* 002   07/08/16    MP      COPT-954    New routing code.
* 003   04/11/16    MP      COPT-656    Refactoring.

*******************************************************************************
*/
public class TeamUtils {
    @Testvisible private static String PSE_BRANCH_PREFIX = 'Branch - CCLA ';
    public static Map<Id, User> mapIdUser = new Map<Id, User>();
    private static Map<String, Id> mapUserNameId = new Map<String, Id>();
    @TestVisible private static map<Id,Team__c> mapUserIdToPrimaryTeam = new map<Id,Team__c>();
    private static final String CDH_PREFIX = 'CDH - ';
    private static final String DEFAULT_BUSINESS_HOURS_NAME = 'Default';
    private static final String DEFAULT_CONTACT_CENTRE_NAME = 'Default';
    public static final String DEFAULT_TEAM_NAME = 'Default';
    public static final String ACTIONED_QUEUE_NAME = '_ACTIONED';
    
    //ES:27/04/15 - CAPITA DROP 2 - LINKING TEAMS TO PUBLIC GROUPS
    private static Map<String, Id> primaryTeamNameToGroupMap = new Map<String, Id>();
    //ES:30/04/15 - CAPITA DROP 2 - Get Profiles 4 Teams
    private static Map<String, Profile> teamProfileNamesMap = new Map<String, Profile>();
    //ES:30/04/15 - CAPITA DROP 2 - Get Roles 4 Teams
    private static Map<String, UserRole> teamNames2RolesMap = new Map<String, UserRole>();
    
    public static final String SEPARATOR = '_';
    public static Map<String, Id> mapQueueNameId = new Map<String, Id>();
    public static Map<Id, QueueSobject> mapIdQueueSobject = new Map<Id, QueueSobject>();
    public static Map<String, Team__c> mapQueueNameTeam = new Map<String, Team__c>();
    
    //Commented and modified as a part of COPT-3585
    //public enum QUEUE_TYPES {WARNING, FAILED, ACTIONED, DEFAULT_QUEUE}
	public enum QUEUE_TYPES {FAILED, ACTIONED, DEFAULT_QUEUE}
    //Green Dot Modification  Saurabh Yadav
    Public static List<String> QueueType=new List<String> {'FAILED','ACTIONED','DEFAULT_QUEUE',''};
    Public static List<Team__c> getAllTeams=Team__c.getAll().Values();
	Public Static Map<String,Team__c> teamQ=new Map<string,Team__c>();    
    Public static Map<String,List<Team__c>> team=new Map<string,List<Team__c>>();
    
    static{
        buildTheStaticCache();
    }
    
    public static void test(){}
    
    public static void buildTheStaticCache() {
        system.debug('buildTheStaicCache == invoked');
        Map<Id, QueueSobject> queueObjMap = new Map<Id, QueueSobject>([SELECT Queue.Id, Queue.DeveloperName,Queue.Name  FROM QueueSobject WHERE SobjectType = 'Case']);
        system.debug('queueObjMap' + queueObjMap);
        
        for (Team__c t: getAllTeams){
   		for(String q: QueueType){	
                     if(q!=''){
                         if(!team.keySet().contains(t.Queue_Name__c+'_'+q)){
                             
                             team.put(t.Queue_Name__c+'_'+q,new List<Team__c>{t});
                             
                                 }else{
                                     team.get(t.Queue_Name__c+'_'+q).add(t);
                                     /*teamQ=team.get(t.Queue_Name__c+'_'+q);
                                     teamQ.add(t);
                                     team.put(t.Queue_Name__c+'_'+q,teamQ);*/
                                     
                                 }
                     
                     }else{
                         	
                             if(!team.keySet().contains(t.Queue_Name__c)){
                                 
                             team.put(t.Queue_Name__c,new List<Team__c>{t});
                                 
                                 }else{
                                     team.get(t.Queue_Name__c).add(t);
                                    /* teamQ=team.get(t.Queue_Name__c);
                                     teamQ.add(t);
                                     team.put(t.Queue_Name__c,teamQ);*/
                       }}}}
        for(QueueSobject qObj: queueObjMap.values() ){
            mapIdQueueSobject.put(qObj.Queue.Id, qObj);
            mapQueueNameId.put(qObj.Queue.DeveloperName, qObj.Queue.Id);
            Team__c teamObj = getTeamForQueue(qObj.Queue.DeveloperName);
            if(teamObj != null){
                mapQueueNameTeam.put(qObj.Queue.DeveloperName, teamObj);
            }               
        }
    }
    
    public static Id getQueueId(String queueName){
        return mapQueueNameId.get(queueName);
    }
    
    public static Id getQueueId(Id queueId, QUEUE_TYPES expectedQueueType){
        if(queueId != null || expectedQueueType != null ){
            QueueSobject queueSObj = mapIdQueueSobject.get(queueId);
            if(queueSObj != null) {
                String queueName = queueSObj.Queue.DeveloperName;
                //Replace the suffix on the queue Name to get the initail queuename
                for(QUEUE_TYPES qType : QUEUE_TYPES.values()) {
                    if(queueName.endsWithIgnoreCase(SEPARATOR + qType.name())){
                        queueName = queueName.substringBeforeLast(SEPARATOR + qType.name());
                        break;
                    }
                }
                queueName = (expectedQueueType == QUEUE_TYPES.DEFAULT_QUEUE) ? queueName : queueName + (SEPARATOR + expectedQueueType.name());            
                return mapQueueNameId.get(queueName);
            }
        }
        return null;    
    }
    
    public static QueueSobject getQueueObject(Id queueId) {
        return (mapIdQueueSobject.get(queueId));
    }
    
    /**
* Find and return the team linked to the given user id
* @params: Id userid       The user id to find the team for.
* @rtnval: Team__c         The user's team.
*/
    public static Team__c getTeamForUser(Id userid) {
        if (CommonStaticUtils.isUserId(userId)) {
            User u;
            Boolean userFound = false;
            Boolean teamFound = false;
            if (mapIdUser.containsKey(userid)) {
                u = mapIdUser.get(userid);
                userFound = true;
            } else {
                List<User> userList = [SELECT Id, Team__c FROM User WHERE Id = :userid LIMIT 1];
                if (!userList.isEmpty()) {
                    u = userList.get(0);
                    mapIdUser.put(u.id, u);
                    userFound = true;
                }
            }
            
            Team__c team;
            
            if (userFound) {
                String teamName = u.Team__c;
                System.debug('teamName: ' + teamName);
                if (String.isNotBlank(teamName)) {
                    team = Team__c.getValues(teamName);
                    System.debug('team: ' + team);
                    if (team != null) {
                        System.debug('Returning users team');
                        teamFound = true;
                    }
                }
            }
            if (!teamFound) {
                System.debug('Returning the default team details');
                team = Team__c.getValues(DEFAULT_TEAM_NAME);
            }
            return team;
        } else {
            System.debug('Case is owned by a queue');
            return getTeamForQueueId(userid);
        }
        
        System.debug('No user found for id: ' + userid);
        
        return null;
    }
    
    /**
* Return the queue id for the given user's team.
* @params: Id userid       The user id to find the queue id for.
* @rtnval: Id              The SF Id for the queue.
*/
    public static Id getQueueIdForUser(Id userid) {
        System.debug('userid****'+userid);
        Team__c t = getTeamForUser(userid);
        System.debug('t****'+t);
        if (t != null) {
            return getQueueIdForTeam(t.Name);
        }
        return null;
    }
    
    /**
* Return the Contact Centre record linked to the given Team.
* @params: String teamName     The team name that the contact centre is required for.
* @rtnval: Contact_Centre__c   The Contact Centre record linked to that team (custom setting).
*/
    public static Contact_Centre__c getContactCentreForTeam(String teamName) {
        if (String.isNotBlank(teamName)) {
            Team__c team = Team__c.getValues(teamName);
            
            if (team != null) {
                String contactCentreName = team.contact_centre__c;
                
                if (String.isNotBlank(contactCentreName)) {
                    return Contact_Centre__c.getValues(contactCentreName);
                }
            }
        }
        
        // Did not find contact centre - return the default one!
        return Contact_Centre__c.getValues(DEFAULT_CONTACT_CENTRE_NAME);
    }
    
    /**
* Return the Id of the Task Assignee user for the given team.
* @params: String teamName     The team name that the Assignee is required for.
* @rtnval: Id                  The SF Id for the Task Assignee user.
*/
    public static Id getAssigneeForTeam(String teamName) {
        if (String.isNotBlank(teamName)) {
            
            Team__c team = Team__c.getValues(teamName);
            
            if (team != null) {
                String username = team.Task_Assignee__c;
                if (String.isNotBlank(username)) {
                    
                    if (mapUserNameId.containsKey(username)) {
                        return mapUserNameId.get(username);
                    } else {
                        List<User> listu = [SELECT Id FROM User WHERE UserName = :username];
                        if (!listu.isEmpty()) {
                            User u = listu.get(0);
                            mapUserNameId.put(username, u.Id);
                            return u.Id;
                        }
                    }
                }
            }
        }
        return null;
    }
    
    /**
* Return the Queue Id linked to the given team.
* @params: String teamName     The team name that the Queue Id is required for.
* @rtnval: Id                  The SF Id for the Queue.
*/
   	public static Id getQueueIdForTeam(string teamName) {
        if (String.isNotBlank(teamName)) {
            Team__c team = new Team__c();
            team = Team__c.getValues(teamName);
			if (team != null) {
            String queuename;

                if(team.Queue_Name__c != null){
                    
                    
                   if( !(Test.isRunningTest())){
               
                if((team.Queue_Name__c != null) && !(team.Queue_Name__c.contains('Branch'))  && !(team.Queue_Name__c.contains('Robot')) ){
                    
                     queuename = team.Queue_Name__c+ ACTIONED_QUEUE_NAME;    
                }
                else{
               queuename = team.Queue_Name__c; 
            }
               
            
            }else{
               queuename = team.Queue_Name__c; 
            }  
                }
                else{
                    queuename = team.Queue_Name__c; 
                    
                }
            System.debug('queuename****'+queuename);         
                return mapQueueNameId.get(queuename);
            }
        }
        return null;
    }

    
    
    /**
* Find and return the team linked to the given queue id
* @params: Id userid       The user id to find the team for.
* @rtnval: Team__c         The user's team.
*/
    public static Team__c getTeamForQueueId(Id qid) {
        return (getTeamForQueueIds(new Set<Id>{qid}).get(qid));
    }
    
    public static map<Id,Team__c> getTeamForUserIds(set<Id> userIds) {
        map<Id,Team__c> userIdToTeamTempMap = new map<Id,Team__c>(); 
        Set<Id> newUserIdSet = userIds.clone();
        map<Id,User> newUserIdToUserMap = new map<Id,User>();
        newUserIdSet.removeAll(mapUserIdToPrimaryTeam.keySet());
        system.debug('newUserIdSet ==' + newUserIdSet);     
        //fetch the user info for the new userIDs
        if(!newUserIdSet.isEmpty()) {
            //Changes for COPT-5378
            newUserIdToUserMap = fetchUserRecordMap ( newUserIdSet );
            //End Changes for COPT-5378
        } 
        system.debug('newUserIdToUserMap ==' + newUserIdToUserMap);     
        //for the new user Ids check if the Team had already been populated as part of the static maps in this UTILS  by other utility methods 
        for(User userObj: newUserIdToUserMap.Values()){
            Team__c teamObj = (string.isempty(userObj.Team__c))?null:Team__c.getInstance(userObj.Team__c);
            //If there is not primary team for the user put the entry as Null since we do not need to search the same user again in SOQL
            mapUserIdToPrimaryTeam.put(userObj.Id,teamObj);
        }
        system.debug('mapUserIdToPrimaryTeam ==' + mapUserIdToPrimaryTeam);     
        //populate the previously populated user Id=Team into the result 
        for(Id userId: userIds){
            userIdToTeamTempMap.put(userId,mapUserIdToPrimaryTeam.get(userId));
        }
        system.debug('userIdToTeamTempMap ==' + userIdToTeamTempMap);       
        return userIdToTeamTempMap;
    }
    //Changes for COPT-5378
    public static Map<Id,User> fetchUserRecordMap ( Set < Id> newUserIdSet ) {
       Map<Id,User> newUserIdToUserMap = new map<Id,User>([select Id, Team__c from User where Id IN:newUserIdSet]);
        return newUserIdToUserMap;
    }
    //End Changes for COPT-5378
    
    @TestVisible
    public static Team__c getTeamForQueue(String queueName){
        Team__c teamObj = null;
        if(String.isNotEmpty(queueName)){
            // Green Dot Modification
            if(team.keySet().contains(queueName)){
                teamObj=team.get(queueName)[0];
            }
            
            /*for(Team__c tempTeam : Team__c.getAll().values()){
                for(QUEUE_TYPES qType : QUEUE_TYPES.values()) {
                    if(queueName.equalsIgnoreCase(tempTeam.Queue_Name__c + SEPARATOR + qType.name())){
                        teamObj = tempTeam;
                        break;
                    }
                }
                if(teamObj!= null){
                    break;
                } else if (queueName.equalsIgnoreCase(tempTeam.Queue_Name__c)){
                    teamObj = tempTeam;
                    break;
                }
                
            }*/
        }
        return  teamObj;
    }
    
    /*
for the given queue Ids get the appropriate Teams   
This method could be called from trigger context, So keeping the SOQL to 2 and handling it the logic via processing 
- this method also ammends the mapIdQueueSobject and mapQueueNameTeam Global maps with the newly values.    
*/
    public static map<Id,Team__c> getTeamForQueueIds(set<Id> qids) {
        Map<Id, Team__c> queueIDToTeamMap = new Map<Id, Team__c>();
        //prepare the queueName To queueId Map
        for(Id  qId:qids){
            String queueName =  mapIdQueueSobject.get(qId).Queue.DeveloperName;
            queueIDToTeamMap.put(qId,mapQueueNameTeam.get(queueName) ); 
        }
        system.debug('newQueueNameSet '+ ' = '+queueIDToTeamMap );      
        
        return queueIDToTeamMap;
    }
    
    
    
    /**
* Returns the name of the team to which a Case should be assigned.
* Refactored under <<002>>
* @params: Case thisCase   The case record to be assigned. 
* @rtnval: String          The team name to be used in the assignment logic.
*/
    public static String getTeamNameForAssigning (Case thisCase) {
        system.debug('@@entering getTeamNameForAssigning');
        string strReturn = '';
        
        // Legacy escalation code
        if (String.isNotBlank(thisCase.jl_Escalate_Case_To_Tier1__c)) {
            system.debug('jl_Escalate_Case_To_Tier1__c');
            return thisCase.jl_Escalate_Case_To_Tier1__c;
        }
        
        if (String.isNotBlank(thisCase.jl_Escalate_case_to__c)) {
            system.debug('jl_Escalate_case_to__c');
            return thisCase.jl_Escalate_case_to__c;
        }
        
        // Case transfer to team logic
        if (thisCase.jl_Transfer_case__c) {
            system.debug('jl_Transfer_case__c');
            system.debug('New Assign To: ' + thisCase.Assign_To__c);
            system.debug('CDH Site: ' + thisCase.CDH_Site__c);
            system.debug('Query: ' + thisCase.jl_Assign_query_to_queue__c);
            system.debug('Complaint: ' + thisCase.jl_Assign_complaint_to_queue__c );
            system.debug('PSE branch: ' + thisCase.Assign_To_New_Branch__c);
            Boolean teamAssigned = false;
            
            if (String.isNotBlank(thisCase.Assign_To__c)) {
                strReturn += thisCase.Assign_To__c;
                teamAssigned = true;
            }
            
            // Legacy complaint assignment code
            if (!teamAssigned && String.isNotBlank(thisCase.jl_Assign_complaint_to_queue__c)) {
                strReturn += thisCase.jl_Assign_complaint_to_queue__c;
                system.debug('Complaint: ' + strReturn);
                teamAssigned = true;
            }
            
            // Legacy query assignment code
            if (!teamAssigned && String.isNotBlank(thisCase.jl_Assign_query_to_queue__c)) {
                strReturn += thisCase.jl_Assign_query_to_queue__c;
                system.debug('Query: ' + strReturn);
                teamAssigned = true;
            }
            
            // <<COPT-429>> Start
            // PSE assignment code
            if (!teamAssigned && String.isNotBlank(thisCase.Assign_To_New_Branch__c)) {
                strReturn += 'Branch - CCLA ' + thisCase.Assign_To_New_Branch__c;
                system.debug('PSE branch: ' + strReturn);
                teamAssigned = true;
            }
            // <<COPT-429>> End
            
            if (!''.equals(strReturn)) {
                if (strReturn.startsWith('Branch') & thisCase.jl_Branch_master__c != null) {
                    strReturn += ' ' + thisCase.jl_Branch_master__c;
                }
            }
        }
        system.debug('strReturn: ' + strReturn);
        return strReturn;
    }
    
    
    /**
* Forms a single string from a list of string values, separated by the
* specified delimiter.
* @params: List<String> lists      List of strings to concatenate.
*          String delimiter        The delimiter string to use.
* @rtnval: String                  The concatenated string value.
*/
    public static String concatenateStrings(List<String> lists, String delimiter) {
        String strReturn = '';
        integer i = 0;
        for (String s : lists) {
            if (i > 0) { 
                strReturn += delimiter;
            }
            strReturn += s;
            i++;
        }
        return strReturn;
    }
    
    /**
* ES:27/04/15 - CAPITA DROP 2 - LINKING TEAMS TO PUBLIC GROUPS
* Returns a set containing all the Primary Team names obtained from the Team__c field
* picklist values on the User object.
* @params: None
* @rtnval: Set<String>     Set containing all primary teams.
*/
    public static Set<String> getAllPrimaryTeamNames() {
        Set <String> result = new Set <String>();
        Schema.DescribeFieldResult fieldResult = User.Team__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry primaryTeamName : ple) {
            result.add (primaryTeamName.getLabel());
        }
        return result;
    }
    
    /**
* ES:27/04/15 - CAPITA DROP 2 - LINKING TEAMS TO PUBLIC GROUPS
* Returns a map of Primary Team names to Queue Ids
* @params: None
* @rtnval: Map<String, Id>     Map of Primary Team names and corresponding Queue Ids.
*/
    public static Map<String, Id> getPrimaryTeamNamesToGroupMap() {
        if (primaryTeamNameToGroupMap.isEmpty()) {
            Set<String> allPrimaryTeamNames = getAllPrimaryTeamNames();
            for (Group g : [SELECT Id, Name FROM Group WHERE Type = 'Regular' AND Name IN :allPrimaryTeamNames]) {
                primaryTeamNameToGroupMap.put(g.Name, g.Id);
            }
        }
        return primaryTeamNameToGroupMap;
    }
    
    /**
* ES:30/04/15 - CAPITA DROP 2 - Get Profiles 4 Teams
* Returns a map of Profiles linked to Teams using the Profile_Name__c field on the Team__c Custom Setting.
* @params: None
* @rtnval: Map<String, Profile>    Map of Primary Team names and corresponding Profile records.
*/
    public static Map<String, Profile> getTeamProfileMap() {
        if (teamProfileNamesMap.isEmpty()) {
            Map<String, Profile> profileName2ProfileMap = new Map <String, Profile>();
            Set<String> teamProfileNames = new Set<String>();
            List<Team__c> teams = Team__c.getall().values();
            for (Team__c t : teams) {
                if (!String.IsBlank(t.Profile_Name__c)) {
                    teamProfileNames.add (t.Profile_Name__c);
                }
            }
            if (!teamProfileNames.isEmpty()) {
                for (Profile p: [SELECT Id, Name FROM Profile WHERE Name IN :teamProfileNames]) {
                    profileName2ProfileMap.put(p.name, p);
                }
            }
            if (!profileName2ProfileMap.isEmpty()) {
                for (Team__c t : teams) {
                    if (!String.IsBlank(t.Profile_Name__c) && profileName2ProfileMap.containsKey(t.Profile_Name__c)) {
                        teamProfileNamesMap.put(t.name, profileName2ProfileMap.get(t.Profile_Name__c));
                    }
                }
            }
        }
        return teamProfileNamesMap;
    }
    
    /**
* ES:30/04/15 - CAPITA DROP 2 - Get Roles 4 Teams
* Returns a map of Roles linked to Teams using the Role_Name__c field on the Team__c Custom Setting.
* @params: None
* @rtnval: Map<String, Profile>    Map of Primary Team names and corresponding Profile records.
*/
    public static Map<String, UserRole> getTeamRolesMap() {
        if (teamNames2RolesMap.isEmpty()) {
            Map<String, UserRole> roleName2RoleMap = new Map<String,UserRole> ();
            Set<String> teamRoleNames = new Set<String>();
            List<Team__c> teams = Team__c.getall().values();
            for (Team__c t : teams) {
                if (!String.IsBlank(t.Role_Name__c)) {
                    teamRoleNames.add(t.Role_Name__c);
                } 
            }
            if (!teamRoleNames.isEmpty()) {
                for (UserRole r: [SELECT Id, Name FROM UserRole WHERE Name IN :teamRoleNames]) {
                    roleName2RoleMap.put(r.Name, r);
                }
            }
            if (!roleName2RoleMap.isEmpty()) {
                for (Team__c t : teams) {
                    if (!String.IsBlank(t.Role_Name__c) && roleName2RoleMap.containsKey(t.Role_Name__c)) {
                        teamNames2RolesMap.put(t.name, roleName2RoleMap.get(t.Role_Name__c));
                    }
                }
            }
        }
        return teamNames2RolesMap;
    }
}