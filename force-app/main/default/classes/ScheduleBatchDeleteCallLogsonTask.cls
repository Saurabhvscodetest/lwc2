global class ScheduleBatchDeleteCallLogsonTask implements Schedulable{

    global void execute(SchedulableContext sc){
        GDPR_BatchDeleteCallLogsonTask obj = new GDPR_BatchDeleteCallLogsonTask();
        Database.executebatch(obj);
    }   
}