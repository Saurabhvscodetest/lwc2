/**
 *	@author: Andrey Gavrikov (westbrook)
 *	@date: 2014-11-19 15:33:06 
 *	@description:
 *	    this class is a scheduled batch which monitors Log_Header__c to see if there are any failed call-outs that need to be re-tried.
 *	    When such Log_Header__c-s found they are marked for retry.
 *	
 *	Version History :   
 *	2014-11-19 - MJL-1306 - AG
 *	initial version
 *	
 * 001 		17/04/15		NTJ		Change behaviour of RunNow command so that the Apex Page can force all Soft Failures to be retried	
 */
public with sharing class CalloutRetryBatch implements Database.Batchable<SObject>, Schedulable, Database.AllowsCallouts {
	private final static Set<String> IN_PROGRESS_JOB_STATUSES = new Set<String>{
		'Processing'
		,'Preparing'
		,'Queued'
		,'Holding'
	};
	private static Integer STALE_JOB_TIMEOUT_MIN = 30;//consider job to be stuck if its start date	is this many minutes or more in the past
	private static Integer BATCH_SIZE = 100;//number of items in apex batch job scope
	private static Integer MAX_CALLOUTS_PER_BATCH = 10;//number of items in apex batch job scope
	
	public void execute(SchedulableContext SC) {
		if (!BatchApexUtils.isJobAlreadyRunning('CalloutRetryBatch', 30)) {
			Database.executeBatch(new CalloutRetryBatch(), BATCH_SIZE);
		}
	}

	public Database.QueryLocator start(Database.BatchableContext bc) {
		//using order by Service__c to minimise number of services involved in a single batch
		//and also to minimise number of callouts, because each service requires a separate callout
		return Database.getQueryLocator(
				[select Id, Callout_Status__c, Do_Callout__c, Service__c from Log_Header__c 
					where 
						Activate_Callout_Retry_Workflow__c = true
						and Next_Callout_Date__c <= :System.now()
						and Callout_Status__c = :CalloutHandler.CALLOUT_STATUS_SOFT_FAIL 
					order by Service__c ]);
		// TODO - if we have a batch to send and one crashes the system then they all fail. Need to think about this.
		/* 
		return Database.getQueryLocator(
				[select Id, Callout_Status__c, Do_Callout__c, Service__c from Log_Header__c 
					where 
						Activate_Callout_Retry_Workflow__c = true
						and Next_Callout_Date__c <= :System.now()
						and Callout_Status__c = :CalloutHandler.CALLOUT_STATUS_SOFT_FAIL 
					order by LastModifiedDate LIMIT 1 ]);
		*/
	}
	public void execute(Database.BatchableContext bc, List<SObject> scope) {
		System.debug('agX about to trigger callout for: ' + scope);
		final Set<Id> headerIds = new Set<Id>();
		final Set<String> services = new Set<String>();
		for(Log_Header__c rec : (List<Log_Header__c>)scope) {
			if (services.size() < MAX_CALLOUTS_PER_BATCH) {
				headerIds.add(rec.Id);
				services.add(rec.Service__c);
			}
		}
		CalloutHandler.callout(headerIds, false);
		//Another option is Database.update(scope); but it will not work
		//because SFDC treats trigger execution caused by batch job as the same
		//context as batch job which caused this trigger, //so this will
		//cause @future from Batch Job which is currently not permitted
		//Database.update(scope); 

	}
	public void finish(Database.BatchableContext bc){ }

	////////////////////////////// Controller methods //////////////////
	private static Integer LOWEST_FREQUENCY = 1;
	private static Integer HIGHEST_FREQUENCY = 12;
	public Integer frequency {get; set;}
	public Boolean isScheduleSaved {get; set;}

	private Boolean validate() {
		if (frequency < LOWEST_FREQUENCY || frequency> HIGHEST_FREQUENCY) {
			//ApexPages.addMessage(ApexPages.)
			ApexPages.addMessage(
					new ApexPages.Message(ApexPages.Severity.ERROR, 'Frequency must be between ' + LOWEST_FREQUENCY + ' and ' + HIGHEST_FREQUENCY + ' times	per hour.'));
			return false;
		}
		return true;
	}

	public void setupNewSchedule() {
		if (validate()) {
			cleanSchedule();
			Integer minutes = 60 / frequency;
			Integer currentMinute = 0;
			for(Integer i =0; i < frequency; i++) {
				if (currentMinute < 60) {
					String cronStr = '0 ' + currentMinute + ' * * * ?';
					String minutesStr = '' + (i * minutes);
					minutesStr = minutesStr.leftPad(2).replaceAll(' ', '0');
					System.schedule('Callout retry @ X + ' + minutesStr + ' minutes', cronStr, new CalloutRetryBatch());
					currentMinute += minutes;
				}
			}
			isScheduleSaved = true;
		}
	}
	public void cleanSchedule() {
		isScheduleSaved = false;
		//Scheduled Apex = 7
		for (CronTrigger job : getScheduledList()) {
			System.abortJob(job.Id);
		}
	}
	public void runNow() {
		// 001 - Get all retriable Headers
		Set<String> serviceSet = makeOutboundServiceSet();
		DateTime pretendNextDateTime = getNextCallount();
		
		List<Log_Header__c> retriableNotifications = [SELECT Id, Name, Activate_Callout_Retry_Workflow__c, Do_Callout__c, Next_Callout_Date__c 
													  FROM Log_Header__c WHERE Callout_Status__c = :CalloutHandler.CALLOUT_STATUS_SOFT_FAIL AND
													  Service__c IN :serviceSet];

		system.debug('Outbound Notifications to be retried: '+retriableNotifications.size());
		Integer i = 1;
		for (Log_Header__c lh:retriableNotifications)		 {
			system.debug('-' +lh.Name);
			lh.Activate_Callout_Retry_Workflow__c = true;
			lh.Next_Callout_Date__c = pretendNextDateTime;
			i++;
		}																		  
		
		update retriableNotifications;
		
		Database.executeBatch(new CalloutRetryBatch(), BATCH_SIZE);
	}
	
	public List<CronTrigger> getScheduledList() {
		return [select Id, CronJobDetail.Name, CronExpression 
				from CronTrigger where CronJobDetail.JobType = '7' and CronJobDetail.Name like 'Callout retry%'
				order by CronJobDetail.Name];
	}

	private Set<String> makeOutboundServiceSet() {
		Set<String> serviceSet = new Set<String>();
		serviceSet.add(MyJL_LoyaltyAccountHandler.FULL_JOIN_NOTIFY);
		serviceSet.add(MyJL_LoyaltyAccountHandler.PARTIAL_JOIN_NOTIFY);
		serviceSet.add(MyJL_LoyaltyAccountHandler.LEAVE_NOTIFY);
		serviceSet.add(MyJL_LoyaltyAccountHandler.OUTBOUND_CONTACT_PROFILE_UPDATE_NOTIFY);
		return serviceSet;
	}

	// get a time that is 10 minutes in the past
	private DateTime getNextCallount() {
		DateTime dtm = system.Now();
		dtm.addMinutes(-1);
		return dtm;
	}
}