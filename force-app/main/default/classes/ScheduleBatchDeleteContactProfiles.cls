global class ScheduleBatchDeleteContactProfiles implements Schedulable{

     global void execute(SchedulableContext sc){
        GDPR_BatchDeleteContactProfiles obj = new GDPR_BatchDeleteContactProfiles();
        Database.executebatch(obj);
    }
    
}