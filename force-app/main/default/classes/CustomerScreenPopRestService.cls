//This Rest Resource has been created as per CCR (Screen Pop) release 1 requirement.
//Purpose: When a customer calls the CISCO IVR will send the customer Phone Number to Connex via ESB. Connex then need to send back Customer Detail/Customer Search url and some Customer data back to ESB.
@RestResource(urlmapping='/GetCustomerDetailsForScreenPop/*')
global class CustomerScreenPopRestService {

    public static List<List<sObject>> searchCustomers(String searchWord) {
        String searchQuery = 'FIND :searchWord IN ALL FIELDS RETURNING Contact(Name, Id, Customer_Segmentation__c, MailingCity, MailingState, MailingPostalCode, MailingStreet, MailingCountry, MobilePhone, HomePhone, Phone, OtherPhone, Email, Birthdate, Customer_Record_Type_Lex__c LIMIT 500), Case(jl_Case_Type__c, CreatedDate, ClosedDate, CaseNumber, Show_Case__c, JL_Cisco_Partner_Number__c, JL_CISCO_Telephone_Number__c, IsClosed, jl_CRNumber__c, jl_OrderManagementNumber__c, jl_DeliveryNumber__c, Case_Owner__c, Case_Owner_Role__c, Owner.Alias, jl_Last_Queue_Name__c, jl_Current_Owners_Queue__c, ContactRecordTypeName__c, View_Order__c, ContactId ORDER BY ClosedDate DESC LIMIT 500)';
        List<List<sObject>> searchCustomerList = search.query(searchQuery);
        return searchCustomerList;
    }
    public static String searchCaseCustomer() {
        String conQuery = 'SELECT Name, Id, Customer_Segmentation__c, MailingCity, MailingState, MailingPostalCode, MailingStreet, MailingCountry, MobilePhone, HomePhone, Phone, OtherPhone, Email, Birthdate, Customer_Record_Type_Lex__c FROM Contact';
        return conQuery;
    }
    public static String searchCases() {
        String caseQuery = 'SELECT jl_Case_Type__c, CreatedDate, ClosedDate, CaseNumber, Show_Case__c, JL_Cisco_Partner_Number__c, JL_CISCO_Telephone_Number__c, IsClosed, jl_CRNumber__c, jl_OrderManagementNumber__c, jl_DeliveryNumber__c, Case_Owner__c, Case_Owner_Role__c, Owner.Alias, jl_Last_Queue_Name__c, jl_Current_Owners_Queue__c, ContactRecordTypeName__c, View_Order__c, ContactId FROM Case';
        return caseQuery;
    }
    
    @HttpGet
    global static CustomerResponseWrapper getCustomerUrl() {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        List<Contact> custList; 
        List<Case> caseList;
        List<List<sObject>> searchList;
        CustomerResponseWrapper custResponse = NEW CustomerResponseWrapper();
        String contactNumber;
        String messageId;      
        String contactExtractNumber; 
        String caseNumber;
        String deliveryNumber;
        String orderNumber;
        String strSearchCases;
        String strSearchCaseCustomer;
        String strBaseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        final String caseKey = 'CASE-';
        
        try {
            //Extracting and storing Customer Phone Number from request.
            contactNumber = req.params.get('customerPhoneNumber');
            orderNumber = req.params.get('orderNumber');
            caseNumber = req.params.get('caseNumber');
            deliveryNumber = req.params.get('deliveryNumber');
            messageId = req.params.get('messageId');
            //Message Id is being used by ESB to track the complete journey, so returning the same Message Id that we received from request.
            custResponse.messageId = messageId;
            
            //caseNumber search
            if(!String.isEmpty(caseNumber)) {
                strSearchCases = searchCases();
                if(strSearchCases!='' && strSearchCases!=NULL) {
                    strSearchCases += ' WHERE CaseNumber = \'' +String.escapeSingleQuotes(caseKey) + String.escapeSingleQuotes(caseNumber)+ '\'';
                	strSearchCases += ' LIMIT 500';
                    caseList = Database.query(strSearchCases); 
                }               
                strSearchCaseCustomer = searchCaseCustomer();
                if(caseList.size()>0 && String.isNotBlank(caseList[0].ContactId)) {
                    strSearchCaseCustomer += ' WHERE Id = \'' +String.escapeSingleQuotes(caseList[0].ContactId) + '\'';
                	strSearchCaseCustomer += ' LIMIT 500';
                    custList = Database.query(strSearchCaseCustomer);
                }
                
                if(caseList!=NULL && !caseList.isEmpty()) {
                    res.statusCode = 200;
                    custResponse.statusCode = String.valueOf(res.statusCode);
                    custResponse.caseData = NEW List<Case>();
                    custResponse.customerData = NEW List<Contact>();
                                        
                    for(Case cas : caseList) {
                        if(String.isNotBlank(cas.ContactId)) {
                            custResponse.customerData.addAll(custList);
                            custResponse.caseData.addAll(caseList);
                            custResponse.customerUrl = strBaseUrl + '/' + custList[0].Id;
                        }
                        else if(String.isBlank(cas.ContactId)) {
                            custResponse.caseData.addAll(caseList);
                        }
                    }
                }
                else {
                    res.statusCode = 404;
                    custResponse.statusCode = String.valueOf(res.statusCode);
                    custResponse.errorCode = 'NO_CASE_FOUND';
                    custResponse.errorMessage = 'caseNumber: no case with this case number';
                    custResponse.caseUrl = strBaseUrl + '/apex/NoResultsFoundPage';
                }
            }
            //deliveryNumber search 
            else if(!String.isEmpty(deliveryNumber)) {
                strSearchCases = searchCases();
                if(strSearchCases!='' && strSearchCases!=NULL) {
                    strSearchCases += ' WHERE jl_DeliveryNumber__c = \'' +String.escapeSingleQuotes(deliveryNumber)+ '\'';
                    strSearchCases += ' ORDER BY ClosedDate DESC LIMIT 500';
                	caseList = Database.query(strSearchCases); 
                }               
                strSearchCaseCustomer = searchCaseCustomer();
                if(caseList.size()>0 && String.isNotBlank(caseList[0].ContactId)) {
                    strSearchCaseCustomer += ' WHERE Id = \'' +String.escapeSingleQuotes(caseList[0].ContactId) + '\'';
                	strSearchCaseCustomer += ' LIMIT 500';
                    custList = Database.query(strSearchCaseCustomer);
                }
                Map<Id,Case> openCaseDRMap = NEW Map<Id,Case>();
                Map<Id,Case> closeCaseDRMap = NEW Map<Id,Case>();
                
                if(caseList.size()>0) {
                    for(Case cas : caseList) {                        
                        if(cas.IsClosed==False && cas.jl_DeliveryNumber__c==String.escapeSingleQuotes(deliveryNumber)) {
                            openCaseDRMap.put(cas.Id,cas);
                        }
                        if(cas.IsClosed==True && cas.jl_DeliveryNumber__c==String.escapeSingleQuotes(deliveryNumber) && cas.ClosedDate.Date()>=System.today()-6) {
                            closeCaseDRMap.put(cas.Id,cas);
                        }
                    }
                }
                
                if(caseList!=NULL && !caseList.isEmpty()) {
                    res.statusCode = 200;
                    custResponse.statusCode = String.valueOf(res.statusCode);
                    custResponse.caseData = NEW List<Case>();
                    custResponse.customerData = NEW List<Contact>();
                    
                    if(openCaseDRMap.size()>0) {
                        if(openCaseDRMap.size()==1) {
                            for(Case cas : openCaseDRMap.values()) {
                                if(String.isNotBlank(cas.ContactId)) {
                                    custResponse.customerData.addAll(custList);
                                    custResponse.caseData.add(openCaseDRMap.values()[0]);
                                    custResponse.customerUrl = strBaseUrl + '/' + custList[0].Id;
                                }
                                else if(String.isBlank(cas.ContactId)) {
                                    custResponse.caseData.add(openCaseDRMap.values()[0]);
                                }
                            }    
                        }
                        else if(openCaseDRMap.size()>1) {
                            custResponse.caseUrl = strBaseUrl + '/_ui/search/ui/UnifiedSearchResults?searchType=2&str=' + String.escapeSingleQuotes(deliveryNumber);
                        }
                    }
                    else if(openCaseDRMap.size()==0) {
                        if(closeCaseDRMap.size()>0) {
                            if(closeCaseDRMap.size()==1) {
                                for(Case cas : closeCaseDRMap.values()) {
                                    if(String.isNotBlank(cas.ContactId)) {
                                        custResponse.customerData.addAll(custList);
                                        custResponse.caseData.add(closeCaseDRMap.values()[0]);
                                        custResponse.customerUrl = strBaseUrl + '/' + custList[0].Id;
                                    }
                                    else if(String.isBlank(cas.ContactId)) {
                                        custResponse.caseData.add(closeCaseDRMap.values()[0]);
                                    }
                                }    
                            }
                            else if(closeCaseDRMap.size()>1) {
                                res.statusCode = 404;
                                custResponse.statusCode = String.valueOf(res.statusCode);
                                custResponse.errorCode = 'NO_CASE_FOUND';
                                custResponse.errorMessage = 'deliveryNumber: no case with this delivery number';
                                custResponse.caseUrl = strBaseUrl + '/apex/NoResultsFoundPage';
                            }
                        }
                        else if(closeCaseDRMap.size()==0) {
                            res.statusCode = 404;
                            custResponse.statusCode = String.valueOf(res.statusCode);
                            custResponse.errorCode = 'NO_CASE_FOUND';
                            custResponse.errorMessage = 'deliveryNumber: no case with this delivery number';
                            custResponse.caseUrl = strBaseUrl + '/apex/NoResultsFoundPage';
                        }
                    }
                }
                else {
                    res.statusCode = 404;
                    custResponse.statusCode = String.valueOf(res.statusCode);
                    custResponse.errorCode = 'NO_CASE_FOUND';
                    custResponse.errorMessage = 'deliveryNumber: no case with this delivery number';
                    custResponse.caseUrl = strBaseUrl + '/apex/NoResultsFoundPage';
                }
            }
            //orderNumber search
            else if(!String.isEmpty(orderNumber)) {
                String strSearchCRCases = searchCases();
                String strSearchORCases = searchCases();
                String strSearchDRCases = searchCases();
                List<Case> caseCRList;
                List<Case> caseORList;
                List<Case> caseDRList;
                
                if(strSearchCRCases!='' && strSearchCRCases!=NULL) {
                    strSearchCRCases += ' WHERE jl_CRNumber__c = \'' +String.escapeSingleQuotes(orderNumber)+ '\'';
                    strSearchCRCases += ' ORDER BY ClosedDate DESC LIMIT 500';
                    caseCRList = Database.query(strSearchCRCases);
                }
                if(strSearchORCases!='' && strSearchORCases!=NULL) {
                    strSearchORCases += ' WHERE jl_OrderManagementNumber__c = \'' +String.escapeSingleQuotes(orderNumber)+ '\'';
                    strSearchORCases += ' ORDER BY ClosedDate DESC LIMIT 500';
                    caseORList = Database.query(strSearchORCases);
                }
                if(strSearchDRCases!='' && strSearchDRCases!=NULL) {
                    strSearchDRCases += ' WHERE jl_DeliveryNumber__c = \'' +String.escapeSingleQuotes(orderNumber)+ '\'';
                    strSearchDRCases += ' ORDER BY ClosedDate DESC LIMIT 500';
                    caseDRList = Database.query(strSearchDRCases);
                }
                
                String strSearchCaseCRCustomer = searchCaseCustomer();
                String strSearchCaseORCustomer = searchCaseCustomer();
                String strSearchCaseDRCustomer = searchCaseCustomer();                
                List<Contact> custCRList;
                List<Contact> custORList;
                List<Contact> custDRList;
                
                if(caseCRList.size()>0 && String.isNotBlank(caseCRList[0].ContactId)) {
                    strSearchCaseCRCustomer += ' WHERE Id = \'' +String.escapeSingleQuotes(caseCRList[0].ContactId) + '\'';
                    strSearchCaseCRCustomer += ' LIMIT 500';
                    custCRList = Database.query(strSearchCaseCRCustomer);             
                }
                if(caseORList.size()>0 && String.isNotBlank(caseORList[0].ContactId)) {
                    strSearchCaseORCustomer += ' WHERE Id = \'' +String.escapeSingleQuotes(caseORList[0].ContactId) + '\'';
                    strSearchCaseORCustomer += ' LIMIT 500';
                    custORList = Database.query(strSearchCaseORCustomer);
                }
                if(caseDRList.size()>0 && String.isNotBlank(caseDRList[0].ContactId)) {
                    strSearchCaseDRCustomer += ' WHERE Id = \'' +String.escapeSingleQuotes(caseDRList[0].ContactId) + '\'';
                    strSearchCaseDRCustomer += ' LIMIT 500';
                    custDRList = Database.query(strSearchCaseDRCustomer);
                }
                Map<Id,Case> openCaseCRMap = NEW Map<Id,Case>();
                Map<Id,Case> closeCaseCRMap = NEW Map<Id,Case>();
                Map<Id,Case> openCaseORMap = NEW Map<Id,Case>();
                Map<Id,Case> closeCaseORMap = NEW Map<Id,Case>();
                Map<Id,Case> openCaseDRMap = NEW Map<Id,Case>();
                Map<Id,Case> closeCaseDRMap = NEW Map<Id,Case>();
				
                if(caseCRList.size()>0) {
                    for(Case cas : caseCRList) {                      
                        if(cas.IsClosed==False && cas.jl_CRNumber__c==String.escapeSingleQuotes(orderNumber)) {
                            openCaseCRMap.put(cas.Id,cas);
                        }
                        if(cas.IsClosed==True && cas.jl_CRNumber__c==String.escapeSingleQuotes(orderNumber) && cas.ClosedDate.Date()>=System.today()-6) {
                            closeCaseCRMap.put(cas.Id,cas);
                        }                        
                    }
                }
                if(caseORList.size()>0) {
                    for(Case cas : caseORList) {                       
                        if(cas.IsClosed==False && cas.jl_OrderManagementNumber__c==String.escapeSingleQuotes(orderNumber)) {
                            openCaseORMap.put(cas.Id,cas);
                        }
                        if(cas.IsClosed==True && cas.jl_OrderManagementNumber__c==String.escapeSingleQuotes(orderNumber) && cas.ClosedDate.Date()>=System.today()-6) {
                            closeCaseORMap.put(cas.Id,cas);
                        }                        
                    }
                }
                if(caseDRList.size()>0) {
                    for(Case cas : caseDRList) {                      
                        if(cas.IsClosed==False && cas.jl_DeliveryNumber__c==String.escapeSingleQuotes(orderNumber)) {
                            openCaseDRMap.put(cas.Id,cas);
                        }
                        if(cas.IsClosed==True && cas.jl_DeliveryNumber__c==String.escapeSingleQuotes(orderNumber) && cas.ClosedDate.Date()>=System.today()-6) {
                            closeCaseDRMap.put(cas.Id,cas);
                        }                        
                    }
                }
                
                if((caseCRList!=NULL && !caseCRList.isEmpty()) || (caseORList!=NULL && !caseORList.isEmpty()) || (caseDRList!=NULL && !caseDRList.isEmpty())) { 
                    res.statusCode = 200;
                    custResponse.statusCode = String.valueOf(res.statusCode);
                    custResponse.caseData = NEW List<Case>();
                    custResponse.customerData = NEW List<Contact>();
                    
                    if(openCaseCRMap.size()>0) {
                        if(openCaseCRMap.size()==1) {
                            for(Case cas : openCaseCRMap.values()) {
                                if(String.isNotBlank(cas.ContactId)) {
                                    custResponse.customerData.addAll(custCRList);
                                    custResponse.caseData.add(openCaseCRMap.values()[0]);
                                    custResponse.customerUrl = strBaseUrl + '/' + custCRList[0].Id;
                                }
                                else if(String.isBlank(cas.ContactId)) {
                                    custResponse.caseData.add(openCaseCRMap.values()[0]);
                                }
                            }
                        }
                        else if(openCaseCRMap.size()>1) {
                            custResponse.caseUrl = strBaseUrl + '/_ui/search/ui/UnifiedSearchResults?searchType=2&str=' + String.escapeSingleQuotes(orderNumber);
                        }
                    }
                    else if(openCaseCRMap.size()==0) {
                        if(closeCaseCRMap.size()>0) {
                            if(closeCaseCRMap.size()==1) {
                                for(Case cas : closeCaseCRMap.values()) {
                                    if(String.isNotBlank(cas.ContactId)) {
                                        custResponse.customerData.addAll(custCRList);
                                        custResponse.caseData.add(closeCaseCRMap.values()[0]);
                                        custResponse.customerUrl = strBaseUrl + '/' + custCRList[0].Id;
                                    }
                                    else if(String.isBlank(cas.ContactId)) {
                                        custResponse.caseData.add(closeCaseCRMap.values()[0]);
                                    }
                                }
                            }
                            else if(closeCaseCRMap.size()>1) {
                                res.statusCode = 404;
                                custResponse.statusCode = String.valueOf(res.statusCode);
                                custResponse.errorCode = 'NO_CASE_FOUND';
                                custResponse.errorMessage = 'orderNumber: no case with this order number';
                                custResponse.caseUrl = strBaseUrl + '/apex/NoResultsFoundPage';
                            }
                        }
                        else if(closeCaseCRMap.size()==0) {
                            if(openCaseORMap.size()>0) {
                                if(openCaseORMap.size()==1) {
                                    for(Case cas : openCaseORMap.values()) {
                                        if(String.isNotBlank(cas.ContactId)) {
                                            custResponse.customerData.addAll(custORList);
                                            custResponse.caseData.add(openCaseORMap.values()[0]);
                                            custResponse.customerUrl = strBaseUrl + '/' + custORList[0].Id;
                                        }
                                        else if(String.isBlank(cas.ContactId)) {
                                            custResponse.caseData.add(openCaseORMap.values()[0]);
                                        }
                                    }
                                }
                                else if(openCaseORMap.size()>1) {
                                    custResponse.caseUrl = strBaseUrl + '/_ui/search/ui/UnifiedSearchResults?searchType=2&str=' + String.escapeSingleQuotes(orderNumber);
                                }
                            }
                            else if(openCaseORMap.size()==0) {
                                if(closeCaseORMap.size()>0) {
                                    if(closeCaseORMap.size()==1) {
                                        for(Case cas : closeCaseORMap.values()) {
                                            if(String.isNotBlank(cas.ContactId)) {
                                                custResponse.customerData.addAll(custORList);
                                                custResponse.caseData.add(closeCaseORMap.values()[0]);
                                                custResponse.customerUrl = strBaseUrl + '/' + custORList[0].Id;
                                            }
                                            else if(String.isBlank(cas.ContactId)) {
                                                custResponse.caseData.addAll(closeCaseORMap.values());
                                            }
                                        }
                                    }
                                    else if(closeCaseORMap.size()>1) {
                                        res.statusCode = 404;
                                        custResponse.statusCode = String.valueOf(res.statusCode);
                                        custResponse.errorCode = 'NO_CASE_FOUND';
                                        custResponse.errorMessage = 'orderNumber: no case with this order number';
                                        custResponse.caseUrl = strBaseUrl + '/apex/NoResultsFoundPage';
                                    }
                                }
                                else if(closeCaseORMap.size()==0) {
                                    if(openCaseDRMap.size()>0) {
                                        if(openCaseDRMap.size()==1) {
                                            for(Case cas : openCaseDRMap.values()) {
                                                if(String.isNotBlank(cas.ContactId)) {
                                                    custResponse.customerData.addAll(custDRList);
                                                    custResponse.caseData.add(openCaseDRMap.values()[0]);
                                                    custResponse.customerUrl = strBaseUrl + '/' + custDRList[0].Id;
                                                }
                                                else if(String.isBlank(cas.ContactId)) {
                                                    custResponse.caseData.add(openCaseDRMap.values()[0]);
                                                }
                                            }
                                        }
                                        else if(openCaseDRMap.size()>1) {
                                            custResponse.caseUrl = strBaseUrl + '/_ui/search/ui/UnifiedSearchResults?searchType=2&str=' + String.escapeSingleQuotes(orderNumber);
                                        }
                                    }
                                    else if(openCaseDRMap.size()==0) { 
                                        if(closeCaseDRMap.size()>0) {
                                            if(closeCaseDRMap.size()==1) {
                                                for(Case cas : closeCaseDRMap.values()) {
                                                    if(String.isNotBlank(cas.ContactId)) {
                                                        custResponse.customerData.addAll(custDRList);
                                                        custResponse.caseData.add(closeCaseDRMap.values()[0]);
                                                        custResponse.customerUrl = strBaseUrl + '/' + custDRList[0].Id;
                                                    }
                                                    else if(String.isBlank(cas.ContactId)) {
                                                        custResponse.caseData.add(closeCaseDRMap.values()[0]);
                                                    }
                                                }
                                            }
                                            else if(closeCaseDRMap.size()>1) {
                                                res.statusCode = 404;
                                                custResponse.statusCode = String.valueOf(res.statusCode);
                                                custResponse.errorCode = 'NO_CASE_FOUND';
                                                custResponse.errorMessage = 'orderNumber: no case with this order number';
                                                custResponse.caseUrl = strBaseUrl + '/apex/NoResultsFoundPage';
                                            }
                                        }
                                        else if(closeCaseDRMap.size()==0) {
                                            res.statusCode = 404;
                                            custResponse.statusCode = String.valueOf(res.statusCode);
                                            custResponse.errorCode = 'NO_CASE_FOUND';
                                            custResponse.errorMessage = 'orderNumber: no case with this order number';
                                            custResponse.caseUrl = strBaseUrl + '/apex/NoResultsFoundPage';
                                        }
                                    } 
                                } 
                            } 
                        } 
                    }
                }
                else {
                    res.statusCode = 404;
                    custResponse.statusCode = String.valueOf(res.statusCode);
                    custResponse.errorCode = 'NO_CASE_FOUND';
                    custResponse.errorMessage = 'orderNumber: no case with this order number';
                    custResponse.caseUrl = strBaseUrl + '/apex/NoResultsFoundPage';
                }
            }
            //customerPhoneNumber search
            else if(!String.isEmpty(contactNumber)) {
                contactExtractNumber = contactNumber.right(10);
                String searchTerm ='\'*'+contactExtractNumber+'\' OR \'0*'+contactExtractNumber+'\' OR \''+contactExtractNumber+'\' OR \'4*'+contactExtractNumber+'\'';
                searchList = searchCustomers(searchTerm); 
                
                custList = (Contact[])searchList[0];
                caseList = (Case[])searchList[1];
                
                Map<Id, Contact> primConMap = NEW Map<Id, Contact>();
                Map<Id, Contact> secConMap = NEW Map<Id, Contact>();
                                
                Map<Id,Case> openCasePrimMap = NEW Map<Id,Case>();
                Map<Id,Case> closeCasePrimMap = NEW Map<Id,Case>();
                Map<Id,Case> openCaseSecMap = NEW Map<Id,Case>();
                Map<Id,Case> closeCaseSecMap = NEW Map<Id,Case>();
                
                if(custList.size()>0) {
                    for(Contact con : custList) {
                        if(con.Customer_Record_Type_Lex__c=='Primary') {
                            primConMap.put(con.Id,con);
                        }
                        if(con.Customer_Record_Type_Lex__c=='Secondary') {
                            secConMap.put(con.Id,con); 
                        }
                    }
                }
                
                if(caseList.size()>0) {
                    for(Case cas : caseList) {
                        if(cas.IsClosed==False && cas.ContactRecordTypeName__c=='Primary' && cas.ContactId!=NULL) {
                            openCasePrimMap.put(cas.Id,cas);
                        }
                        if(cas.IsClosed==False && cas.ContactRecordTypeName__c=='Secondary' && cas.ContactId!=NULL) {
                            openCaseSecMap.put(cas.Id,cas);
                        }
                        if(cas.IsClosed==True && cas.ContactRecordTypeName__c=='Primary' && cas.ClosedDate.Date()>=System.today()-6 && cas.ContactId!=NULL) {
                            closeCasePrimMap.put(cas.Id,cas);
                        }
                        if(cas.IsClosed==True && cas.ContactRecordTypeName__c=='Secondary' && cas.ClosedDate.Date()>=System.today()-6 && cas.ContactId!=NULL) {
                            closeCaseSecMap.put(cas.Id,cas);
                        }
                    }
                }
                
                if(custList!=NULL && !custList.isEmpty()) {
                    res.statusCode = 200;
                    custResponse.statusCode = String.valueOf(res.statusCode);
                    custResponse.caseData = NEW List<Case>();
                    custResponse.customerData = NEW List<Contact>();
                    
                    //First - Primary Profile > 1
                    if(primConMap.size()>1) {
                        custResponse.customerData.addAll(primConMap.values());
                        custResponse.customerUrl = strBaseUrl + '/_ui/search/ui/UnifiedSearchResults?searchType=2&str=' + searchTerm;
                    }
                    
                    //Second - Primary Profile = 1 
                    else if(primConMap.size()==1) {
                        if(openCasePrimMap.size()==1) {
                            custResponse.customerData.add(primConMap.values());
                            custResponse.caseData.addAll(openCasePrimMap.values());
                            custResponse.customerUrl = strBaseUrl + '/' + primConMap.values()[0].Id; 
                        }
                        else if(openCasePrimMap.size()==0 || openCasePrimMap.size()>1) {
                            if(secConMap.size()>0) {
                                if(secConMap.size()==1) {
                                    if(openCaseSecMap.size()==1) {
                                        custResponse.customerData.add(secConMap.values());
                                        custResponse.caseData.addAll(openCaseSecMap.values());
                                        custResponse.customerUrl = strBaseUrl + '/' + secConMap.values()[0].Id;
                                    }
                                    else if(openCaseSecMap.size()==0 || openCaseSecMap.size()>1) {
                                        if(openCasePrimMap.size()>1) {
                                            custResponse.customerData.add(primConMap.values());
                                            custResponse.caseData.addAll(openCasePrimMap.values());
                                            custResponse.customerUrl = strBaseUrl + '/' + primConMap.values()[0].Id;
                                        }
                                        else if(openCasePrimMap.size()==0) {
                                            if(openCaseSecMap.size()>1) {
                                                custResponse.customerData.add(secConMap.values());
                                                custResponse.caseData.addAll(openCaseSecMap.values());
                                                custResponse.customerUrl = strBaseUrl + '/' + secConMap.values()[0].Id;
                                            }
                                            else if(openCaseSecMap.size()==0) {
                                                if(closeCasePrimMap.size()>0) {
                                                    custResponse.customerData.add(primConMap.values());
                                                    custResponse.caseData.add(closeCasePrimMap.values()[0]);
                                                    custResponse.customerUrl = strBaseUrl + '/' + primConMap.values()[0].Id;
                                                }
                                                else if(closeCasePrimMap.size()==0) {
                                                    if(closeCaseSecMap.size()>0) {
                                                        custResponse.customerData.add(secConMap.values());
                                                        custResponse.caseData.add(closeCaseSecMap.values()[0]);
                                                        custResponse.customerUrl = strBaseUrl + '/' + secConMap.values()[0].Id;
                                                    }
                                                    else if(closeCaseSecMap.size()==0) {
                                                        custResponse.customerData.add(primConMap.values());
                                                        custResponse.customerUrl = strBaseUrl + '/' + primConMap.values()[0].Id;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else if(secConMap.size()>1) {
                                    custResponse.customerData.addAll(secConMap.values());
                                    custResponse.customerUrl = strBaseUrl + '/_ui/search/ui/UnifiedSearchResults?searchType=2&str=' + searchTerm;
                                }
                            }
                            else if(secConMap.size()==0) {
                                if(openCasePrimMap.size()>1) {
                                    custResponse.customerData.add(primConMap.values());
                                    custResponse.caseData.addAll(openCasePrimMap.values());
                                    custResponse.customerUrl = strBaseUrl + '/' + primConMap.values()[0].Id;
                                }
                                else if(openCasePrimMap.size()==0) {
                                    if(closeCasePrimMap.size()>0) {
                                        custResponse.customerData.add(primConMap.values());
                                        custResponse.caseData.add(closeCasePrimMap.values()[0]);
                                        custResponse.customerUrl = strBaseUrl + '/' + primConMap.values()[0].Id;
                                    }
                                    else if(closeCasePrimMap.size()==0) {
                                        custResponse.customerData.add(primConMap.values());
                                        custResponse.customerUrl = strBaseUrl + '/' + primConMap.values()[0].Id;
                                    }                                                       
                                }
                            }
                        }
                    }
                    
                    //Third - Primary Profile = 0
                    else if(primConMap.size()==0) {
                        if(secConMap.size()>0) {
                            if(secConMap.size()==1) {
                                if(openCaseSecMap.size()>0) {
                                    custResponse.customerData.add(secConMap.values());
                                    custResponse.caseData.addAll(openCaseSecMap.values());
                                    custResponse.customerUrl = strBaseUrl + '/' + secConMap.values()[0].Id;
                                }
                                else if(openCaseSecMap.size()==0) {
                                    if(closeCaseSecMap.size()>0) {
                                        custResponse.customerData.add(secConMap.values());
                                        custResponse.caseData.add(closeCaseSecMap.values()[0]);
                                        custResponse.customerUrl = strBaseUrl + '/' + secConMap.values()[0].Id;                                        
                                    }
                                    else if(closeCaseSecMap.size()==0) {
                                        custResponse.customerData.add(secConMap.values());
                                        custResponse.customerUrl = strBaseUrl + '/' + secConMap.values()[0].Id;                                        
                                    }
                                }
                            }
                            else if(secConMap.size()>1) {
                                custResponse.customerData.addAll(secConMap.values());
                                custResponse.customerUrl = strBaseUrl + '/_ui/search/ui/UnifiedSearchResults?searchType=2&str=' + searchTerm;
                            }
                        }
                    }
                }
                else {
                    res.statusCode = 404;
                    custResponse.statusCode = String.valueOf(res.statusCode);
                    custResponse.errorCode = 'NO_CUSTOMER_FOUND';
                    custResponse.errorMessage = 'customerPhoneNumber: no customer with this phone number';
                    custResponse.customerUrl = strBaseUrl + '/apex/NoResultsFoundPage';
                } 
            }            
            return custResponse; 
        }
        catch(Exception e) { 
            messageId = req.params.get('messageId');
            res.statusCode = 500;
            custResponse.statusCode = String.valueOf(res.statusCode);
            custResponse.errorCode = 'CONNEX_SERVER_ERROR';
            custResponse.errorMessage = e.getMessage();
            custResponse.customerUrl = strBaseUrl + '/apex/NoResultsFoundPage';
            return custResponse;
        }
    }
    
    //Wrapper class to prepare the response
    global class CustomerResponseWrapper { 
        
        global String errorCode {get;set;} 
        global String errorMessage {get;set;}  
        global String messageId {get;set;}
        global String customerUrl {get;set;} 
        global String caseUrl {get;set;} 
        global List<Contact> customerData {get;set;}
        global List<Case> caseData {get;set;}
        global String statusCode { get; set; }
        
        global CustomerResponseWrapper() {
            this.errorCode = 'OK';
            this.errorMessage = 'Success';
        }
    }
}