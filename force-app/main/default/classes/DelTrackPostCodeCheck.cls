public class DelTrackPostCodeCheck {

     Public class DelTrackPostCodeRequest {
     @InvocableVariable(required= true)
     public String CustomerEnteredPostCode;
     @InvocableVariable(required= true)
     public String EventHubPostCode;
   }
    
    Public class DelTrackPostCodeResponse {
     @InvocableVariable(required= true)
     public String PostCodeExistResult;
   }
    @Invocablemethod(Label ='Chatbot PostCode Method' description='Invoke Post Code Check')
    Public static List<DelTrackPostCodeResponse> DelTrackChatBotProcess(List<DelTrackPostCodeRequest> chatbotInputReqList){
        List<DelTrackPostCodeResponse> chatBotDelResponseList = new List<DelTrackPostCodeResponse>();
        DelTrackPostCodeResponse chatBotDelResponse = new DelTrackPostCodeResponse();
         List<String> chatbotInputs = new List<String>();
         for( DelTrackPostCodeRequest chatbotReq : chatbotInputReqList ){
           		chatbotInputs.add ( chatbotReq.CustomerEnteredPostCode );
           		chatbotInputs.add ( chatbotReq.EventHubPostCode );
         }
        String resultPostCode = postCodeCheckMethod(chatbotInputs[0],chatbotInputs[1]);
        chatBotDelResponse.PostCodeExistResult = resultPostCode;
        chatBotDelResponseList.add(chatBotDelResponse);
        return chatBotDelResponseList;
    }
    
    Public Static String postCodeCheckMethod(String CustomerEnteredPostCode, String EventHubPostCode){
        
        if(CustomerEnteredPostCode != NULL && EventHubPostCode != NULL){
            
        if (CustomerEnteredPostCode == EventHubPostCode){
            return 'Exist';
        }
        else if(CustomerEnteredPostCode == EventHubPostCode.remove(' ')){
              return 'Exist';
        }
         else if(CustomerEnteredPostCode.remove(' ') == EventHubPostCode.remove(' ')){ 
              return 'Exist';
        }
        else if(CustomerEnteredPostCode.remove(' ') == EventHubPostCode){
              return 'Exist';
        }  
            
        }else {
           return 'Do Not Exist';   
        }
     
		 return 'Do Not Exist';       
    }  
}