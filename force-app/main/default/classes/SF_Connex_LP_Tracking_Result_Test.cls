@IsTest
Public class SF_Connex_LP_Tracking_Result_Test {

    @IsTest
    Public static void testTrackingResults(){
        
        List<String> customerCheck = new List<String>();
        List<String> customerCheck1 = new List<String>();
        List<String> customerCheck2 = new List<String>();
        
        Contact con = UnitTestDataFactory.createContact();
        con.Email = 'vijayreddyambati@gmail.com';
        Insert con;
        
        String conversationId = 'fe7ac93c-41e3-4004-9eea-6e713a4e3f9b';
        String customerPhoneNumber = '447409999999';
        String customerEmail = 'vijayreddyambati@gmail.com';
        String customerPostcode = 'SW1E 5NN';
        String customerName = 'vijay ambati';
        String customerAddress = '99, Test Road';
        String customerId = con.Id;
        String customerSearchResult = 'CUSTOMER_EXIST';
        String IDVStatus = 'Exist';
        String IDVType = 'Bot';
        
        customerCheck.add(conversationId);
        customerCheck.add(customerPhoneNumber);
        customerCheck.add(customerEmail);
        customerCheck.add(customerPostcode);
        customerCheck.add(customerName);
        customerCheck.add(customerAddress);
        customerCheck.add(customerId);
        customerCheck.add(customerSearchResult);
        customerCheck.add(IDVStatus);
        
        customerCheck1.add(conversationId);
        customerCheck1.add(customerPhoneNumber);
        customerCheck1.add(customerEmail);
        customerCheck1.add(customerPostcode);
        customerCheck1.add(customerName);
        customerCheck1.add(customerAddress);
        customerCheck1.add(customerSearchResult);
        customerCheck1.add(IDVStatus);
        
        Test.startTest();
        
        SF_Connex_LP_Tracking_Result.trackingResults(customerCheck);
        SF_Connex_LP_Tracking_Result.trackingResults(customerCheck1);
        SF_Connex_LP_Tracking_Result.trackingResults(customerCheck2);
        SF_Connex_LP_Tracking_Result.trackingResultsMissingParams();
        SF_Connex_LP_Tracking_Result.trackingResultsException();
        
        Test.stopTest();
        
    }

@IsTest
    
    Public static void testCheckMethod(){
        
        String test1 = 'test1';
        String test2 = 'test2';
        String test3 = 'test3';
        String test4 = 'test4';
        String test5 = 'test5';
        String test6 = 'test6';
        String test7 = 'test7';
        String test8 = 'test8';
        

		List<String> test1List =  new List<String>();
        test1List.add(test1);
        
        List<String> test2List =  new List<String>();
        test2List.add(test1);
        test2List.add(test2);
        
        List<String> test3List =  new List<String>();
        test3List.add(test1);
        test3List.add(test2);
        test3List.add(test3);
        
        List<String> test4List =  new List<String>();
        test4List.add(test1);
        test4List.add(test2);
        test4List.add(test3);
        test4List.add(test4);
        
        List<String> test5List =  new List<String>();
        test5List.add(test1);
        test5List.add(test2);
        test5List.add(test3);
        test5List.add(test4);
        test5List.add(test5);
        
        List<String> test6List =  new List<String>();
        test6List.add(test1);
        test6List.add(test2);
        test6List.add(test3);
        test6List.add(test4);
        test6List.add(test5);
        test6List.add(test6);
        
        List<String> test7List =  new List<String>();
        test7List.add(test1);
        test7List.add(test2);
        test7List.add(test3);
        test7List.add(test4);
        test7List.add(test5);
        test7List.add(test6);
        test7List.add(test7);
        
        List<String> test8List =  new List<String>();
        test8List.add(test1);
        test8List.add(test2);
        test8List.add(test3);
        test8List.add(test4);
        test8List.add(test5);
        test8List.add(test6);
        test8List.add(test7);
        test8List.add(test8);
        
		Test.startTest();
        
        SF_Connex_LP_Tracking_Result.saveCustomerChoosenOptions(test1List);
        SF_Connex_LP_Tracking_Result.saveCustomerChoosenOptions(test2List);
        SF_Connex_LP_Tracking_Result.saveCustomerChoosenOptions(test3List);
        SF_Connex_LP_Tracking_Result.saveCustomerChoosenOptions(test4List);
        SF_Connex_LP_Tracking_Result.saveCustomerChoosenOptions(test5List);
        SF_Connex_LP_Tracking_Result.saveCustomerChoosenOptions(test6List);
        SF_Connex_LP_Tracking_Result.saveCustomerChoosenOptions(test7List);
        SF_Connex_LP_Tracking_Result.saveCustomerChoosenOptions(test8List);
                
        Test.stopTest();

        
    }
    
    
}