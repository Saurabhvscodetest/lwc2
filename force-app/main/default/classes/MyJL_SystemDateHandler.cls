/**
 *	@author: Andrey Gavrikov (westbrook)
 *	@date: 2014-10-08 10:51:10 
 *	@description:
 *	The Audit Trail Fields in Salesforce (CreatedDate and LastModifiedDate) are
 *	createable and not updateable.  As we will be using upsert in Data
 *	Migration activities, we cannot guarantee if the upsert call will result in
 *	and insert or an update call.  We need a solution that ensure that the
 *	salesforce standard fields are populated only if this is an insert and not
 *	an update.
 *	    
 *	NOTE: this class will have 0 code coverage as soon as CreatedDate is made
 *	Read/Only again by SFDC Support, as there is no way to run this code when
 *	CreatedDate can not be written into
 *	
 *	Version History :   
 *	2014-10-08 - MJL-1158 - AG
 *	initial version copying on *create*
 *	- Source_LastModifiedDate__c -> LastModifiedDate
 *	- Source_CreatedDate__c -> CreatedDate
 *		
 */
public without sharing class MyJL_SystemDateHandler extends MyJL_TriggerHandler {
	override public void beforeInsert () {
		if (isEnabled()) {
			copyToSystemDates(Trigger.new);
		}
	}

	private static void copyToSystemDates(List<SObject> recs) {
		for(SObject rec : recs) {
			Datetime created = (Datetime)rec.get('Source_CreatedDate__c');
			Datetime modified = (Datetime)rec.get('Source_LastModifiedDate__c');

			if (null != created) {
				rec.put('CreatedDate', created);
			}
			if (null != modified) {
				rec.put('LastModifiedDate', modified);
			}
		    
		}

	}
	
	/**
	 * MJL-1158 - only run if MyJL Settings__c.Populate_Audit_fields__c == TRUE
	 */
	public static Boolean isEnabled() {
		return true == CustomSettings.getMyJLSetting().getBoolean('Populate_Audit_fields__c');
	}
}