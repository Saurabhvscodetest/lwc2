/*
* @File Name   : BAT_UpdateClienTellingDataTest
* @Description : Test class for the batch class used to update the contact records from the database and update the fields mentioned in 4277
* @Copyright   : Zensar
* @Jira #      : #4277
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0       22-April-19        Ragesh G                     Created
*/

@isTest
public class BAT_UpdateClienTellingDataTest {
    
    
     static testMethod void testBatchExecutionWithPositiveCases () {
         List< Contact > contactRecordList = new List< Contact >();
        
        for(Integer counter =0; counter<10 ; counter++){
            contact testContact = new contact();
            testContact.FirstName = 'John';
            testContact.LastName  = 'Smith' + counter;
            testContact.Clienteling_Permission__c = 'YES';
            testContact.GDPR_Data_Deletion__c = ConstantValues.FIVE_YEARS_AGO.addYears(-1);
            contactRecordList.add( testContact);
        }
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.Name = 'BatchUpdateClienTellingData';
        gDPR.GDPR_Class_Name__c = 'BatchUpdateClienTellingData';
        gDPR.Record_Limit__c = '5000';                
        try{
            insert gDPR;
            insert contactRecordList;
        }
        catch(Exception e){
            System.debug('Exception Caught:'+e.getmessage());
        }
         
          Test.startTest();
        DataBase.executeBatch( new BAT_UpdateClienTellingData());
        Test.stopTest();
     }
    
    static testMethod void testSchedulemethod () {
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.Name = 'BAT_UpdateClienTellingData';
		gDPR.GDPR_Class_Name__c = 'BAT_UpdateClienTellingData';
		gDPR.Record_Limit__c = '5000';
        Test.startTest();
        Insert gDPR;
        ScheduleBatchToUpdateCTData obj = NEW ScheduleBatchToUpdateCTData();
        obj.execute(null);  
        Test.stopTest();
    }

}