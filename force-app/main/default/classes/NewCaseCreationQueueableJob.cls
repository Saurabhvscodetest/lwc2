public class NewCaseCreationQueueableJob implements Queueable{
    public Map<Id,Case> parentCases {get;set;}
    public Map<Id,EmailMessage> oldCase2EmailMessageMap {get;set;}
    public Map<String,Id> compositeKeyMap {get;set;}
    public static final Id queryCaseRTId = CommonStaticUtils.getRecordTypeID('Case','Query');
    public static final Id nkuCaseRTId = CommonStaticUtils.getRecordTypeID('Case','NKU');
    public List<AssignmentRule> ARList {get;set;}
    public Database.DMLOptions dmlOpts {get;set;}
    
    public NewCaseCreationQueueableJob(List<Case> parentCaseList, List<EmailMessage> emailMessageList){
        ARList = new List<AssignmentRule>();
        ARList = [SELECT Id FROM AssignmentRule WHERE SobjectType = 'Case' AND Active = true LIMIT 1];
        //Creating the DMLOptions for "Assign using active assignment rules" checkbox
        dmlOpts = new Database.DMLOptions();
        
        if (ARList.size() > 0){
            dmlOpts.EmailHeader.triggerAutoResponseEmail = true;
            dmlOpts.assignmentRuleHeader.assignmentRuleId= ARList.get(0).id;
        }
        
        parentCases = new Map<Id,Case>();
        for(Case obj : parentCaseList){
            parentCases.put(obj.Id,obj);
        }
        oldCase2EmailMessageMap = new Map<Id,EmailMessage>();
        compositeKeyMap = new Map<String,Id>();
        for(EmailMessage obj : emailMessageList){
            if(obj.ParentId != NULL){
                oldCase2EmailMessageMap.put(obj.ParentId,obj);
                compositeKeyMap.put(generateCompositeKey(obj.Subject,obj.TextBody,obj.FromAddress),obj.ParentId);
            }
        }
    }
    
    public String generateCompositeKey(String subject, String textBody, String fromAddress){
        String compositeKey = '';
        compositeKey += !String.isBlank(subject) ? subject+'-' : '';
        compositeKey += !String.isBlank(textBody) ? textBody+'-' : '';
        compositeKey += !String.isBlank(fromAddress) ? fromAddress : '';
        return compositeKey;
    }
    
    public List<Case> cloneCase(){
        List<Case> cases2Create = new List<Case>();
        for(Case obj : parentCases.values()){
            Case newCase = new Case();        
            newCase = obj.clone();
            newCase.Status = 'New';
            newCase.jl_Action_Taken__c = 'New case created';
            newCase.JL_Email_Address__c = obj.JL_Email_Address__c;
            if(oldCase2EmailMessageMap.containsKey(obj.Id)){
                EmailMessage em = oldCase2EmailMessageMap.get(obj.Id);
                newCase.Subject = em.Subject;
                newCase.Description = em.TextBody;
                newCase.Origin = 'Email';
                newCase.SuppliedEmail = em.FromAddress;
                newCase.RecordTypeId = queryCaseRTId;
            }
            newCase.setOptions(dmlOpts);
            cases2Create.add(newCase);
        }
        return cases2Create;
    }
    
    public List<EmailMessage> copyEmailMessageToNewCases(Map<Id,Id> old2NewCaseMap){
        List<EmailMessage> emailMessagesToBeCopied = new List<EmailMessage>();
        for(EmailMessage em : [SELECT Id, ParentId, ActivityId, TextBody, HtmlBody, Headers, Subject, FromName, FromAddress, ValidatedFromAddress, ToAddress, CcAddress, BccAddress, Incoming, HasAttachment, Status, MessageDate, 
                               ReplyToEmailMessageId, IsExternallyVisible, IsClientManaged, Dummy_Email__c, Internal_Email__c, Case_Status__c, NKU_Resolution__c FROM EmailMessage where ParentId IN :old2NewCaseMap.keySet()]){
                                   if(old2NewCaseMap.containsKey(em.ParentId)){
                                       EmailMessage tempEmailMessage = new EmailMessage();
                                       tempEmailMessage = em.Clone(false,true,true,true);
                                       tempEmailMessage.ParentId=old2NewCaseMap.get(em.ParentId);
                                       tempEmailMessage.Case_Status__c = 'No change to Case Status required';
                                       emailMessagesToBeCopied.add(tempEmailMessage);
                                   }                            
                               }
        for(Id oldCaseId : old2NewCaseMap.keySet()){
            if(oldCase2EmailMessageMap.containsKey(oldCaseId)){
                EmailMessage tempEmailMessage = new EmailMessage();
                tempEmailMessage = oldCase2EmailMessageMap.get(oldCaseId).Clone(false,true,true,true);
                tempEmailMessage.ParentId=old2NewCaseMap.get(oldCaseId);
                tempEmailMessage.Case_Status__c = 'No change to Case Status required';
                emailMessagesToBeCopied.add(tempEmailMessage);
            }
        }
        return emailMessagesToBeCopied;
    }
    
    public Map<String,List<SObject>> handleTaskReassignmentAndCaseUpdate(Map<Id,Id> old2NewCaseMap){
        Map<String,List<SObject>> returnMap = new Map<String,List<SObject>>();
        List<Case> updateCaseOutstandingTasks = new List<Case>();
        List<Task> tasksToBeUpdated = new List<Task>();
        Map<Id,Task> taskMap = new Map<Id,Task>();
        for (Task t : [SELECT Id, WhatId FROM Task WHERE WhatId IN :parentCases.keySet() AND IsClosed = false order by createdDate asc]) {
            taskMap.put(t.WhatId, t);
        }
        for(Id oldCaseId : old2NewCaseMap.keySet()){
            if(parentCases.containsKey(oldCaseId)){
                Case oldCase = parentCases.get(oldCaseId);
                if(taskMap.containsKey(oldCaseId)){
                    Task t = taskMap.get(oldCaseId);
                    oldCase.jl_NumberOutstandingTasks__c = oldCase.jl_NumberOutstandingTasks__c - 1;
                    updateCaseOutstandingTasks.add(oldCase);
                    t.WhatId = old2NewCaseMap.get(oldCaseId);
                    tasksToBeUpdated.add(t);
                }
            }
        }
        returnMap.put('Task',tasksToBeUpdated);
        returnMap.put('Case',updateCaseOutstandingTasks);
        return returnMap;
    }
    
    public void execute(QueueableContext context){
        List<Case> cases2Create = new List<Case>();
        cases2Create = cloneCase();
        
        if(!cases2Create.isEmpty()){
            clsCaseTriggerHandler.CASE_OLDER_THAN_30_DAYS_REOPENED = true;
            Database.Saveresult[] results = Database.insert(cases2Create, false);
            
            List<Id> newCaseIdList = new List<Id>();
            Map<Id,Id> old2NewCaseMap = new Map<Id,Id>();
            for (Integer i = 0; i <  results.size(); i ++) {
                if (results[i].isSuccess()) {
                    newCaseIdList.add(results[i].getId());
                }
            }
            for(Case obj : [select Id,Subject,Description,SuppliedEmail from Case where Id IN :newCaseIdList]){
                String compositeKey = generateCompositeKey(obj.Subject,obj.Description,obj.SuppliedEmail);
                if(compositeKeyMap.containsKey(compositeKey)){
                    old2NewCaseMap.put(compositeKeyMap.get(compositeKey),obj.Id);
                }
            }
            
            List<EmailMessage> emailMessagesToBeCopied = new List<EmailMessage>();
            emailMessagesToBeCopied = copyEmailMessageToNewCases(old2NewCaseMap);
            
            Map<String,List<SObject>> taskAndCaseMap = new Map<String,List<SObject>>();
            taskAndCaseMap = handleTaskReassignmentAndCaseUpdate(old2NewCaseMap);
            List<Task> tasksToBeUpdated = taskAndCaseMap.containsKey('Task') ? taskAndCaseMap.get('Task') : new List<Task>();
            List<Case> updateCaseOutstandingTasks = taskAndCaseMap.containsKey('Case') ? taskAndCaseMap.get('Case') : new List<Case>();
            
            //Perform DML operations
            
            if(!emailMessagesToBeCopied.isEmpty()){
                Database.insert(emailMessagesToBeCopied, false);
            }
            
            if(!tasksToBeUpdated.isEmpty()){
                Database.update(tasksToBeUpdated, false);
            }
            
            if(!updateCaseOutstandingTasks.isEmpty()){
                Database.update(updateCaseOutstandingTasks, false);
            }
        }
    }

}