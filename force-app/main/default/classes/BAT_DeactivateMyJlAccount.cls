/* Description  :Deactivate MyJlAccounts based on criteria on contacts
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   03/04/2019        Vignesh Kotha                   Created                COPT-4537
*
*/
global class BAT_DeactivateMyJlAccount implements Database.Batchable<sObject>,Database.Stateful{
    global Map<Id,String> ErrorMap = new Map<Id,String>();
    global integer result=0;
    private static final DateTime Source_DT = System.now().addYears(-5);
    private static final String PrimaryKeyword = Label.Delete_Primary_Customers;
    global string query;
    global Database.QueryLocator start(Database.BatchableContext BC){
    GDPR_Record_Limit__c gDPRLimit = GDPR_Record_Limit__c.getValues('BAT_DeactivateMyJlAccount');
    String recordLimit = gDPRLimit.Record_Limit__c;
    Integer qLimit = Integer.valueOf(recordLimit);    
    return Database.getQueryLocator([select id,Createddate,Customer_Record_Type_Lex__c,Email,
                                         (select id,IsActive__c from MyJL_Accounts__r where IsActive__c = true),
                                         (select id,Subject from tasks where Subject LIKE 'Email:%' OR Subject = 'Call Log'),
                                         (select id from ReportCards__r) from contact where 
                                         ID NOT IN (select ContactId  from case) and
                                         ID NOT IN (select ContactId from LiveChatTranscript) and
                                         Createddate <= : Source_DT and AccountId =NULL Limit : qLimit]);
    }
    global void execute(Database.BatchableContext BC, list<Contact> scope){
        List<Loyalty_Account__c> MyJldata = new List<Loyalty_Account__c>();
        set<id> contactids = new set<id>();
        if(scope.size()>0 || Scope!= NULL){
            try{
                for(Contact recordScope : scope){
                    if(recordScope.Email!=NULL || recordScope.Email != ''){
                        if(recordScope.MyJL_Accounts__r.size()!= 0 && recordScope.tasks.size()==0 && recordScope.ReportCards__r.size()==0  && recordScope.Email.startsWithIgnoreCase(PrimaryKeyword) && recordScope.Customer_Record_Type_Lex__c == 'Primary' || Test.isRunningTest()){
                            contactids.add(recordScope.id);  
                        }
                    }                    
                }
                
                for(Loyalty_Account__c lA : [select id,Contact__c,IsActive__c from Loyalty_Account__c where Contact__c IN : contactids and IsActive__c = true]){
                    lA.IsActive__c = false;
                    MyJldata.add(lA);                
                }                
                  List<Database.SaveResult> srList = Database.update(MyJldata, false);
                    if(srList.size()>0){
                        for(Integer counter = 0; counter < srList.size(); counter++){
                            if (srList[counter].isSuccess()){
                                result++;
                            }else{
                                if(srList[counter].getErrors().size()>0){
                                    for(Database.Error err : srList[counter].getErrors()){
                                        ErrorMap.put(srList.get(counter).id,err.getMessage());
                                    }
                                }                                
                            }
                        }
                    }       
            }Catch(exception e){
                EmailNotifier.sendNotificationEmail('Exception from BAT_DeactivateMyJlAccount ', e.getMessage());
            }
        }
    }
    global void finish(Database.BatchableContext BC){
        String textBody ='';
        set<Id> allIds = new set<Id>();
        textBody+= result +' records deactivated in MyJL Account.'+'\n';
        if(!ErrorMap.isEmpty()){
            for (Id recordids : ErrorMap.KeySet()){
                allIds.add(recordids);
            } 
            textBody+= 'Error log: '+allIds+'\n';
        }
        EmailNotifier.sendNotificationEmail('GDPR Connex MyJL Account Records Updation Log', textBody);
		BAT_DeletePrimaryCustomer objNextBatch = new BAT_DeletePrimaryCustomer();
		Database.executeBatch(objNextBatch); 
    }    
}