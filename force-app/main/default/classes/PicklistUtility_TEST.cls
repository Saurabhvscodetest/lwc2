@Istest
public with sharing class PicklistUtility_TEST {
    @testSetup static void setup() {
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
    }
    @isTest
    public static void testPicklistUtilityForComplaintCase(){
        system.assert([SELECT Object_API__c, Record_Type_Name__c, Type__c, Controlling_Field__c, Dependent_Field__c FROM Field_Dependency_Information__mdt].size()>0,'Field Metadata Not Available.');
        insert new Case(Status='New',RecordTypeId=CommonStaticUtils.getRecordTypeId('Case', 'Complaint'));
        Case complaintCase = [select Id, Status, RecordTypeId from Case LIMIT 1].get(0);
        system.assertEquals('New', complaintCase.Status);
        test.startTest();
        complaintCase.Status = 'Closed - Resolved';
        //Try to mark the status of the case to "Closed - Resolved" and validate that an error message is thrown asking the user to fill in value for Primary Category
        try{
            update complaintCase;
        }
        catch(Exception e){
            system.assert(String.valueOf(e.getMessage()).contains('Please add values to these field(s) to proceed'));
            system.assert(String.valueOf(e.getMessage()).contains('Primary Category'));
        }
        
        //Add value to field : Primary Category
        //Try to mark the status of the case to "Closed - Resolved" and validate that an error message is thrown asking the user to fill in value for fields: Area of the Business (PR) & Primary Reason
        complaintCase.jl_Primary_Category__c = 'Branch';
        try{
            update complaintCase;
        }
        catch(Exception e){
            system.assert(String.valueOf(e.getMessage()).contains('Please add values to these field(s) to proceed'));
            system.assert(String.valueOf(e.getMessage()).contains('Area of the Business (PR)'));
            system.assert(String.valueOf(e.getMessage()).contains('Primary Reason'));
        }
        
        //Add value to field : Area of the Business (PR) & Primary Reason
        //Try to mark the status of the case to "Closed - Resolved" and validate that an error message is thrown asking the user to fill in value for fields: Primary Reason Detail
        complaintCase.jl_Primary_Category__c = 'Branch';
        complaintCase.jl_Primary_Reason__c = 'Accident/Incident';
        complaintCase.jl_AreaBusiness_pri__c = 'Aberdeen';
        try{
            update complaintCase;
        }
        catch(Exception e){
            system.assert(String.valueOf(e.getMessage()).contains('Please add values to these field(s) to proceed'));
            system.assert(String.valueOf(e.getMessage()).contains('Primary Reason Detail'));
        }
        
        //Add value to field : Primary Reason Detail
        //Try to mark the status of the case to "Closed - Resolved" and validate that an error message is thrown asking the user to fill in value for fields: Primary Detail Explanation
        complaintCase.jl_Primary_Category__c = 'Branch';
        complaintCase.jl_Primary_Reason__c = 'Customer Catering';
        complaintCase.jl_AreaBusiness_pri__c = 'Aberdeen';
        complaintCase.jl_Primary_Reason_Detail__c = 'Cleanliness';
        try{
            update complaintCase;
        }
        catch(Exception e){
            system.assert(String.valueOf(e.getMessage()).contains('Please add values to these field(s) to proceed'));
            system.assert(String.valueOf(e.getMessage()).contains('Primary Detail Explanation'));
        }
        
        //Add value to field : Primary Detail Explanation
        //Try to mark the status of the case to "Closed - Resolved" and validate that an error message is thrown asking the user to fill in value for fields: Primary Detail Explanation
        complaintCase.jl_Primary_Category__c = 'Branch';
        complaintCase.jl_Primary_Reason__c = 'Customer Catering';
        complaintCase.jl_AreaBusiness_pri__c = 'Aberdeen';
        complaintCase.jl_Primary_Reason_Detail__c = 'Cleanliness';
        complaintCase.jl_Primary_Detail_Explanation__c = 'Benugo';
        try{
            update complaintCase;
        }
        catch(Exception e){
        }
        complaintCase = [select Id, Status, RecordTypeId from Case LIMIT 1].get(0);
        system.assertEquals('Closed - Resolved', complaintCase.Status);
        PicklistUtility pUtility = new PicklistUtility();
        Map<String,List<String>> valueMap = new Map<String,List<String>>();
        valueMap = pUtility.GetDependentOptions(null,null,null);
        system.assertEquals(0,valueMap.size());
        valueMap = pUtility.GetDependentOptions('Case','Fake_Controlling_Field__c','Fake_Dependent_Field__c');
        system.assertEquals(0,valueMap.size());        
        test.stopTest();
    }
}