@isTest
public class LetterBuilderControllerTest {
	
	static testmethod void testInstantiation(){
		UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
		UnitTestDataFactory.setRunValidationRules(false);
		UnitTestDataFactory.createTeamAndContactCentre();
		
		Contact cnt = UnitTestDataFactory.createContact();
		cnt.MailingStreet = '5 Example Street';
		cnt.MailingCity = 'City';
		cnt.MailingCountry = 'United Kingdom';
		cnt.Salutation = 'Mr';
		insert cnt;
		Case c = UnitTestDataFactory.createNormalCase(cnt);
		insert c;
		
		ApexPages.Standardcontroller stdcntrl = new ApexPages.Standardcontroller(c);
		
		LetterBuilderController cntrl = new LetterBuilderController(stdcntrl);
		
		Test.startTest();
		
		PageReference previewPDF = cntrl.generateLetter();
		
		System.assert(previewPDF != null);
		System.assertEquals(c.Id, previewPDF.getParameters().get('id'));
		
		cntrl.attachPdfToCase();
		cntrl.getAddressLines();
		cntrl.getLetterLines();
		cntrl.cancel();
		
		Test.stopTest();
	
	}

}