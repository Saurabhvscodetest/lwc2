// Edit		When		Who		Comment
//	001		27/05/15	NTJ		Add sessionId for logging purposes
public with sharing class myJL_Queueable implements Queueable, Database.AllowsCallouts {
	
	private String mode;
	private String jsonRequest;
	private String sessionId;
	
	public myJL_Queueable(String mode, String jsonRequest, String sessionId) {
		this.mode = mode;
		this.jsonRequest = jsonRequest;
		this.sessionId = sessionId;
	}
	
	public void execute(QueueableContext context) { 
		MyJL_UpdateCustomerHandler.processAsynchronousImpl(mode, jsonRequest);	
	}
}