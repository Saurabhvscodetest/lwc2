/*
* @File Name   : BAT_UpdateCTellingDateOnContact
* @Description : Batch class used to update the contact record new date field with the last updated date value from the Contact
                 or Task object based on the recent date
* @Copyright   : Zensar
* @Jira #      : #4277
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0       16-April-19        Ragesh G                     Created
*/
global class ScheduleBatchToContactCTData implements Schedulable{
   global void execute(SchedulableContext sc){
        BAT_UpdateCTellingDateOnContact obj = new BAT_UpdateCTellingDateOnContact();
        Database.executebatch(obj);
    }   
}