/*
* @File Name   : QueueCapacityConstants
* @Description : Constants class to declare all the variables which is used  
* @Copyright   : Zensar
* @Jira #      : #4805
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0       10-July-19         Ragesh G                     Created
*/


public class QueueCapacityConstants {
    
    
    // The string lieral 'Slot' used to fetch the records from the system in the SOQL.
	public static String SLOT_STRING_VALUE   = System.Label.QC_SLOT_STRING;
	public static String USER_STRING_PREFIX  = '005';
	public static String QUEUE_STRING_PREFIX = '00G';
	public static String QUEUE_STRING_VALUE  = 'Queue';
	
	public static final Map < String, String > AVAILABLE_SLOT_MAP = new Map < String, String > {
                                              'Slot1_Available__c' => '08:00-12:00',
                                              'Slot2_Available__c' => '10:00-14:00',
                                              'Slot3_Available__c' => '12:00-16:00',
                                              'Slot4_Available__c' => '14:00-18:00',
                                              'Slot5_Available__c' => '16:00-20:00'
                                              };
    public static final Map < String, String > AVAILABLE_SLOT_LIMIT_MAP = new Map < String, String > {
                                              'Slot1__c' => '08:00-12:00',
                                              'Slot2__c' => '10:00-14:00',
                                              'Slot3__c' => '12:00-16:00',
                                              'Slot4__c' => '14:00-18:00',
                                              'Slot5__c' => '16:00-20:00'
                                              };
    public static Set < String > AVAILABLE_SLOT_FIELD_SET = new Set < String> {'Slot1_Available__c','Slot2_Available__c','Slot3_Available__c','Slot4_Available__c','Slot5_Available__c'};
	public static Set < String > AVAILABLE_SLOT_LIMIT_FIELD_SET = new Set < String> {'Slot1__c','Slot2__c','Slot3__c','Slot4__c','Slot5__c'};
	
	public static String SLOT1_AVAILABLE_FIELD = 'Slot1_Available__c';
	public static String SLOT2_AVAILABLE_FIELD = 'Slot2_Available__c';
	public static String SLOT3_AVAILABLE_FIELD = 'Slot3_Available__c';
	public static String SLOT4_AVAILABLE_FIELD = 'Slot4_Available__c';
	public static String SLOT5_AVAILABLE_FIELD = 'Slot5_Available__c';
	public static Integer NEXT_AVAILABLE_SLOT_LIMIT = Integer.valueOf ( System.Label.QC_NEXT_AVAILABLE_SLOT_LIMIT);
	public static Date NEXT_SLOT_LIMIT = System.today()+( NEXT_AVAILABLE_SLOT_LIMIT -1) ;
	public static String DEFAULT_STRING_VALUE = 'Default';
    public static String PSE_BRANCH_PREFIX = 'Branch - CCLA ';
    public static String ACTIONED_QUEUE_NAME = '_ACTIONED';
    
    public static String ACTIONED_STRING_VALUE = 'ACTIONED';
    public static String FAILED_STRING_VALUE = 'FAILED';
    
    
	

}