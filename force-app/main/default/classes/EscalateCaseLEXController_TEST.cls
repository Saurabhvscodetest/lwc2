@isTest
public class EscalateCaseLEXController_TEST {
    
    public static final String CONTACT_CENTRE_TEAM_NAME = ' Hamilton - CRD';
    public static final String CONTACT_CENTRE_QUEUE_NAME = 'CRD_Work_Allocation_Hamilton';

/**
* @description  Test the case escalate feature          
* @author       @ayanhore   
*/    
    @isTest static void testEscalateCaseFeature() {
        
        User runningUser = UserTestDataFactory.getRunningUser();
        
        User contactCentreUser;
        
        Contact testContact;
        Case testCase;
        
        System.runAs(runningUser) {
            
            CustomSettingTestDataFactory.initialiseTeamCustomSettings(CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_QUEUE_NAME);
            
            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
            system.assert(contactCentreUser.Id != NULL);
            List<QueueOwnerFilter__c> queueOwnerFilterCS = new List<QueueOwnerFilter__c>();
            queueOwnerFilterCS.add(new QueueOwnerFilter__c(Name='COT Hamilton',Filter_from_Bulk_Transfer__c=true,Filter_from_Change_Owner__c=true));
            queueOwnerFilterCS.add(new QueueOwnerFilter__c(Name='CSD',Filter_from_Bulk_Transfer__c=false,Filter_from_Change_Owner__c=false));
            insert queueOwnerFilterCS;
            Id changeOwnerPermissionSetId = [select Id, Name from PermissionSet where Name = 'Escalate_Access'].get(0).Id;
            system.assert(changeOwnerPermissionSetId != NULL);
            PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = changeOwnerPermissionSetId, AssigneeId = contactCentreUser.Id);
            insert psa;
            system.assert(psa.Id != NULL);
        }
        
        System.runAs(contactCentreUser) {
            test.startTest();
            testContact = CustomerTestDataFactory.createContact();
            insert testContact;
            
            testCase = CaseTestDataFactory.createComplaintCase(testContact.Id);
            insert testCase;
            system.assert(testCase.Id != NULL);
            
            List<String> queueList = EscalateCaseLEXController.getQueuesList(testCase.Id);
            system.assertEquals(1,queueList.size());
            boolean editPermission = EscalateCaseLEXController.checkChangeOwnerAccessPermission();
            EscalateCaseLEXController.initializeAssignmentQuickActionComponent(testCase.Id);
            system.assertEquals(true,editPermission);
            EscalateCaseLEXController.changeOwnerAction(testCase.Id,'CSD');
            test.stopTest();
        }
        system.assertEquals([select Id from Group where Name = 'CSD' LIMIT 1].get(0).Id,[select Id, OwnerId from Case LIMIT 1].get(0).OwnerId);
    }

}