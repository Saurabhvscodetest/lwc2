/* Description  : Test Class For GDPR - Superseded Customer Data Deletion
*
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   03/01/2019        Vijay Ambati                  Created                COPT-4270
*
*/

@isTest
public class BAT_DeleteSupersededCustomersTest {
    Public Static Id RecordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Superseded').getRecordTypeId();       
    static testmethod void testDeleteSupercededContacts() {
        List<Contact>  ccInsert = New List<Contact>();
        for(integer i=0; i<300; i++){
            ccInsert.add(new Contact(FirstName = 'Test', LastName = 'Customer'+i,RecordTypeId = RecordTypeIdContact,Email = 'test@abc.com'));
        }
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.GDPR_Class_Name__c = 'BAT_DeleteSupersededCustomers';
        gDPR.Name = 'BAT_DeleteSupersededCustomers';
        gDPR.Record_Limit__c = '5000';
        try{
            insert ccInsert;
			Insert gDPR;
            system.assertEquals(300, ccInsert.size());
        }
        Catch(exception e){
            system.debug('Exception Caught:'+e.getmessage());
        }
        Database.BatchableContext BC;
        BAT_DeleteSupersededCustomers obj = NEW BAT_DeleteSupersededCustomers();
        Test.startTest();
        Database.DeleteResult[] Delete_Result = Database.delete(ccInsert, false);
        obj.start(BC);
        obj.execute(BC,ccInsert); 
        obj.finish(BC);
        Test.stopTest();           
    }
    @isTest
    Public static void testDmlException_Delete()
    {
        List<Contact>  ccException = New List<Contact>();
        for(integer i=0; i<300; i++){
            ccException.add(new Contact(FirstName = 'Test', LastName = 'Customer'+i,RecordTypeId = RecordTypeIdContact,Email = 'test@abc.com'));
        }
        insert ccException;
        system.assertEquals(300, ccException.size());
        Database.DeleteResult[] Delete_Result = Database.delete(ccException, false);
        DmlException expectedException;
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.GDPR_Class_Name__c = 'BAT_DeleteSupersededCustomers';
        gDPR.Name = 'BAT_DeleteSupersededCustomers';
        gDPR.Record_Limit__c = '';
        Test.startTest();
        try { 
            Insert gDPR;
            delete ccException;
        }
        catch (DmlException dException) {
            expectedException = dException; 
            system.assertNotEquals(NULL, expectedException);
        }
        Test.stopTest();
    }
    @isTest
    Public static void testSchedule(){
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.GDPR_Class_Name__c = 'BAT_DeleteSupersededCustomers';
        gDPR.Name = 'BAT_DeleteSupersededCustomers';
        gDPR.Record_Limit__c = '5000';
        Test.startTest();
        Insert gDPR;
        ScheduleBatchDeleteSupersededCustomers obj = NEW ScheduleBatchDeleteSupersededCustomers();
        obj.execute(null);  
        Test.stopTest();
    }
}