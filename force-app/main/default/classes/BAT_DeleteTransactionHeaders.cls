/* Description : Delete Trasaction Header Records
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0  29/03/2019        Vijay Ambati                      Created              COPT-4296
* 1.1  18/04/2019        Vignesh kotha                     Modified             COPT-4578
*/
global class BAT_DeleteTransactionHeaders implements Database.Batchable<sObject>,Database.Stateful{
    global Map<Id,String> ErrorMap = new Map<Id,String>();
    global integer result=0;
    private static final DateTime CREATED_DATE = System.now().addYears(-5);
    global String query;
    global Database.QueryLocator start(Database.BatchableContext BC){
        GDPR_Record_Limit__c gDPRLimit = GDPR_Record_Limit__c.getValues('BAT_DeleteTransactionHeaders');
        if(gDPRLimit.Record_Limit__c != NULL){
            String recordLimit = gDPRLimit.Record_Limit__c;
            Integer qLimit = Integer.valueOf(recordLimit);
            return Database.getQueryLocator([Select id from Loyalty_Account__c where
                                             (Deactivation_Date_Time__c !=NULL and Deactivation_Date_Time__c <= :CREATED_DATE and IsActive__c=false) OR
                                             (Scheme_Joined_Date_Time__c != NULL and Scheme_Joined_Date_Time__c <= :CREATED_DATE and IsActive__c = false and Activation_Date_Time__c = NULL) OR
                                             (Contact__c = NULL and IsActive__c= false) LIMIT : qLimit]);
        }else{
            return Database.getQueryLocator([Select id from Loyalty_Account__c where
                                             (Deactivation_Date_Time__c!= NULL and Deactivation_Date_Time__c <= :CREATED_DATE and IsActive__c=false) OR
                                             (Scheme_Joined_Date_Time__c != NULL and Scheme_Joined_Date_Time__c <= :CREATED_DATE and IsActive__c = false and Activation_Date_Time__c = NULL) OR
                                             (Contact__c = NULL and IsActive__c= false)]);
        }
    }
    global void execute(Database.BatchableContext BC, list<Loyalty_Account__c> scope){
        List<id> recordstoDelete = new List<id>(); 
        Set<id> LoyaltyAccountrecords = new set<id>();
        if(scope.size()>0 && scope != NULL){
           try{ 
            for(Loyalty_Account__c recordScope : scope){
                LoyaltyAccountrecords.add(recordScope.id);
            }
            for(Transaction_Header__c Trashead : [select id,Card_Number__r.Loyalty_Account__c from Transaction_Header__c where Card_Number__r.Loyalty_Account__c=:LoyaltyAccountrecords]){
                recordstoDelete.add(Trashead.id);                
            } 
            list<Database.deleteResult> srList = Database.delete(recordstoDelete, false);
            for(Integer counter = 0; counter < srList.size(); counter++) {
                if (srList[counter].isSuccess()) {
                    result++;
                }else {
                    for(Database.Error err : srList[counter].getErrors()) {
                        ErrorMap.put(srList.get(counter).id,err.getMessage());
                    }
                }
            }
            Database.emptyRecycleBin(scope);
        }Catch(exception e){
            EmailNotifier.sendNotificationEmail('Exception from BAT_DeleteTransactionHeaders', e.getMessage());
        } 
        }
        
    }
    global void finish(Database.BatchableContext BC) {
        String textBody ='';
        set<Id> allIds = new set<Id>();
        textBody+= result +' records deleted in Transactional Headers'+'\n';
        if (!ErrorMap.isEmpty()){
            for (Id recordids : ErrorMap.KeySet())
            {
                allIds.add(recordids);
            } 
            textBody+= 'Error log: '+allIds+'\n';
        }
        EmailNotifier.sendNotificationEmail('GDPR Delete Transaction Headers Data', textBody);
        //Delete Loyalty Account once transaction headers are deleted.
        BAT_DeleteBulkLoyaltyAccount loyaltyobj = new BAT_DeleteBulkLoyaltyAccount();
        Database.executeBatch(loyaltyobj);
    }
    
}