/* Description  : 
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                         Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   29/01/2019        Vignesh Kotha                   Created               COPT-4259
*
*/
public class EventHubCaseExceptions {
     @AuraEnabled
     public static List<EventHubCaseExceptions.wrapperstoreExp> fetchtaskdata(string recordId) {
       List<EventHubCaseExceptions.wrapperstoreExp> updatedlist = new List<EventHubCaseExceptions.wrapperstoreExp>();
        for(Task tasklist : [SELECT Id, External_Exception_Name__c, Created_Date_Custom__c,status FROM Task where whatid =:recordId and status!='Closed - Resolved' ORDER BY createddate DESC]){
               EventHubCaseExceptions.wrapperstoreExp taskdata = new EventHubCaseExceptions.wrapperstoreExp();
               taskdata.ExceptionName = tasklist.External_Exception_Name__c;
               taskdata.createdinfo = tasklist.Created_Date_Custom__c;
               updatedlist.add(taskdata);
           }
        return updatedlist;
     }
    public class wrapperstoreExp{
       @AuraEnabled public string ExceptionName;
       @AuraEnabled public Datetime createdinfo;
    }
}