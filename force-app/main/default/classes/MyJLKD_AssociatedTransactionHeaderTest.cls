/**
 * Test to make sure that we can upsert an associated transaction
 */
@isTest
private class MyJLKD_AssociatedTransactionHeaderTest{

    static testMethod void basic() {
        // create card
        List<Loyalty_Card__c> cards = LoyaltyCardHandlerTest.generateCards(new List<String>{'Card_1'});
        system.assertEquals(1, cards.size());

        // Create three Transactions
        final List<Transaction_Header__c> trans = new List<Transaction_Header__c>{
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR1', Card_Number_Text_Version__c = 'card_1', Card_Number__c = cards[0].Id),
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR2', Card_Number_Text_Version__c = 'Card_1', Card_Number__c = cards[0].Id),
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR3', Card_Number_Text_Version__c = 'CarD_1', Card_Number__c = cards[0].Id)
        };
        Database.insert(trans);

        // Requery to get the Ids

        Test.StartTest();       
            List<Associated_Transaction__c> astInsertList = new List<Associated_Transaction__c>();
            
            Associated_Transaction__c ast1 = createAssociatedTransaction(cards[0], trans[0], trans[1]);
            astInsertList.add(ast1);
            Associated_Transaction__c ast2 = createAssociatedTransaction(cards[0], trans[0], trans[2]);
            astInsertList.add(ast2);
        
            upsert astInsertList External_Id__c;
            
            // id's should be added as part of the insert
            system.assertNotEquals(null, astInsertList[0].Id);
            system.assertNotEquals(null, astInsertList[1].Id);

            // now do it again and this time we should update as part of the upsert, so Id's should be same as before
            List<Associated_Transaction__c> astupdateList = new List<Associated_Transaction__c>();
            Associated_Transaction__c ast3 = createAssociatedTransaction(cards[0], trans[0], trans[1]);
            astupdateList.add(ast3);
            Associated_Transaction__c ast4 = createAssociatedTransaction(cards[0], trans[0], trans[2]);
            astupdateList.add(ast4);
            
            upsert astupdateList External_Id__c;

            // id's should be same as before
            system.assertEquals(astInsertList[0].Id, astupdateList[0].Id);
            system.assertEquals(astInsertList[1].Id, astupdateList[1].Id);                      
        
        Test.StopTest();
        
    }
        
    private static Associated_Transaction__c createAssociatedTransaction(Loyalty_Card__c card, Transaction_Header__c parentTH, Transaction_Header__c associatedTH) {
        Associated_Transaction__c ast = new Associated_Transaction__c();

        String externalId = parentTH.Receipt_Barcode_Number__c + '+' + associatedTH.Receipt_Barcode_Number__c;
            
        ast.Transaction__c = parentTH.Id;
        ast.Associated_Transaction__c = associatedTH.Id;
        ast.External_Id__c = externalId;
        
        return ast;
    }
}