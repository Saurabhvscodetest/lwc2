global class PermissionSetLookup {
	webservice static Boolean CheckPermissionSetForUserByName(Id userId, string permissionSetName){
		return CommonStaticUtils.isPermissionSetAssignedToSingleUserByName(userId,permissionSetName);
	}
}