@isTest
private class MyLoyaltyService_Test {

	private static final String SHOPPER_ID = '1234abcdefg';

	private static final String PARTNERSHIP_CARD_NUMBER = '3456789456123';
	private static final String ORIGIN = 'Unit Test';
	private static final DateTime DATE_CREATED = DateTime.NOW();

	@TestSetup
	static void initData(){

		Loyalty_Account__c myAccount = new Loyalty_Account__c(ShopperId__c = SHOPPER_ID, IsActive__c = true);
		insert myAccount;
	}

	@isTest static void testServiceWithNoPartnershipCardNumber() {

		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = '/MyLoyaltyService/Subscribe/' + SHOPPER_ID;
		req.httpMethod = 'PUT';

		Test.startTest();
		MyLoyaltyService.joinShopper( null, ORIGIN, DATE_CREATED);
		Test.stopTest();

		integer statusCode = res.statusCode;
		String message = res.responseBody.toString();
		
		System.assertEquals(400, statusCode);
		System.assertEquals('"partnershipCardNumber: is not provided"', message);
	}

	@isTest static void testServiceWithNoOrigin() {

		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = '/MyLoyaltyService/Subscribe/' + SHOPPER_ID;
		req.httpMethod = 'PUT';

		Test.startTest();
		MyLoyaltyService.joinShopper( PARTNERSHIP_CARD_NUMBER, null, DATE_CREATED);
		Test.stopTest();

		integer statusCode = res.statusCode;
		String message = res.responseBody.toString();
		
		System.assertEquals(400, statusCode);
		System.assertEquals('"origin: is not provided"', message);
	}


	@isTest static void testServiceWithNoDateCreated() {

		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = '/MyLoyaltyService/Subscribe/' + SHOPPER_ID;
		req.httpMethod = 'PUT';

		Test.startTest();
		MyLoyaltyService.joinShopper( PARTNERSHIP_CARD_NUMBER, ORIGIN, null);
		Test.stopTest();

		integer statusCode = res.statusCode;
		String message = res.responseBody.toString();
		
		System.assertEquals(400, statusCode);
		System.assertEquals('"dateCreated: is not provided"', message);
	}

	@isTest static void testServiceWithNoShopperProvided() {

		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = '/MyLoyaltyService/Subscribe/';
		req.httpMethod = 'PUT';

		Test.startTest();
		MyLoyaltyService.joinShopper( PARTNERSHIP_CARD_NUMBER, ORIGIN, DATE_CREATED);
		Test.stopTest();

		integer statusCode = res.statusCode;
		String message = res.responseBody.toString();
		
		System.assertEquals(400, statusCode);
		System.assertEquals('"shopperId: is not provided"', message);
	}

	@isTest static void testServiceWithNoShopperNotFound() {

		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = '/MyLoyaltyService/Subscribe/1234567';
		req.httpMethod = 'PUT';

		Test.startTest();
		MyLoyaltyService.joinShopper( PARTNERSHIP_CARD_NUMBER, ORIGIN, DATE_CREATED);
		Test.stopTest();

		integer statusCode = res.statusCode;
		String message = res.responseBody.toString();
		
		System.assertEquals(404, statusCode);
		System.assertEquals('"No Loyalty accounts found for shopperId"', message);
	}

	@isTest static void testServiceWithDeactivatedAccount() {

		Loyalty_Account__c account = [select IsActive__c From Loyalty_Account__c where ShopperId__c = :SHOPPER_ID Limit 1];

		if( account != null){
			account.IsActive__c = false;
			update account;
		}

		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = '/MyLoyaltyService/Subscribe/' + SHOPPER_ID;
		req.httpMethod = 'PUT';

		Test.startTest();
		MyLoyaltyService.joinShopper( PARTNERSHIP_CARD_NUMBER, ORIGIN, DATE_CREATED);
		Test.stopTest();

		integer statusCode = res.statusCode;
		String message = res.responseBody.toString();
		
		System.assertEquals(404, statusCode);
		System.assertEquals('"Account is not active"', message);
	}

	@isTest static void testServiceWithAccountAlreadyHasAPartnershipCard() {
        CalloutHandlerTest.initCalloutSettings(MyJL_LoyaltyAccountHandler.FULL_JOIN_NOTIFY, 'MyJL_LoyaltyAccountHandler.JoinLeaveCallout', 10);

		Loyalty_Account__c account = [select IsActive__c From Loyalty_Account__c where ShopperId__c = :SHOPPER_ID Limit 1];

		if( account != null){

			Loyalty_Card__c card = new Loyalty_Card__c();
	        card.Name = '1234567890123456';
	        card.Card_Number__c = '1234567890123456';
	        card.Loyalty_Account__c = account.Id;
	        card.Card_Type__c = MyJL_Const.MY_PARTNERSHIP_CARD_TYPE;
	        card.Date_Created__c = DateTime.Now();
	        card.Origin__c = 'TEST';

			insert card;
		}

		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = '/MyLoyaltyService/Subscribe/' + SHOPPER_ID;
		req.httpMethod = 'PUT';

		Test.startTest();
		MyLoyaltyService.joinShopper( PARTNERSHIP_CARD_NUMBER, ORIGIN, DATE_CREATED);
		Test.stopTest();

		integer statusCode = res.statusCode;
		String message = res.responseBody.toString();
		
		System.assertEquals(404, statusCode);
		System.assertEquals('"Account is already a Partnership Account"', message);
	}


	@isTest static void testServiceWithShopper() {
        CalloutHandlerTest.initCalloutSettings(MyJL_LoyaltyAccountHandler.FULL_JOIN_NOTIFY, 'MyJL_LoyaltyAccountHandler.JoinLeaveCallout', 10);

		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = '/MyLoyaltyService/Subscribe/' + SHOPPER_ID;
		req.httpMethod = 'PUT';

		Test.startTest();
		MyLoyaltyService.joinShopper( PARTNERSHIP_CARD_NUMBER, ORIGIN, DATE_CREATED);
		Test.stopTest();

		integer statusCode = res.statusCode;
		System.assertEquals(200, statusCode);
	}

	@isTest static void testServiceForCatchBlock() {

		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = null;
		req.httpMethod = 'PUT';

		Test.startTest();
		MyLoyaltyService.joinShopper( PARTNERSHIP_CARD_NUMBER, ORIGIN, DATE_CREATED);
		Test.stopTest();

		integer statusCode = res.statusCode;
		String message = res.responseBody.toString();	
		
		System.assertEquals(500, statusCode);
	}
	
}