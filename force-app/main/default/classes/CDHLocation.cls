public class CDHLocation {
    public String cdhKey {get; set;}
    
    public String getCDHLocation() {
        String cdhValue = '';
        if( String.isNotBlank(cdhKey)) {
            cdhValue  = EHLocationMapper.getMappedLocation(cdhKey);
        }
        if( String.isBlank(cdhValue)){
            cdhValue  = cdhKey;
        }
        return cdhValue;
    }      
   
}