@IsTest

Public class SFDeliveryBotOrderNumber_Test {

 @IsTest
    Public static void testMethod_OrderNumber(){
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        
        System.runAs(runningUser){
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;       
            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        
        System.runAs(testUser){
            
            contact testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'vijayambati@gmail.com';
            insert testContact;    
            
            List<Case> caseList = new List<Case>();
            case testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            caseList.add(testCase); 
            
            case testCase1 = CaseTestDataFactory.createCase(testContact.Id);
            testCase1.Bypass_Standard_Assignment_Rules__c = true;
            caseList.add(testCase1);   
            Insert caseList;
            
        LiveChatVisitor testvisitor=new LiveChatVisitor();
       // testvisitor.CreatedDate=System.today();
        insert testvisitor;  
            
        List<LiveChatTranscript> testTranscript = new List<LiveChatTranscript>();
        LiveChatTranscript lCT1 = new LiveChatTranscript();
       	lCT1.CreatedDate = system.now();
        lCT1.Email__c= 'vijayreddy@gmail.com';
        lCT1.FirstName__c= 'Vijay';
        lCT1.LastName__c= 'Ambati';
        lCT1.OrderNumber__c= '26789876';
        lCT1.LiveChatVisitorId= testvisitor.Id;
        lCT1.CaseId = caseList[0].id;
        
        testTranscript.add(lCT1);
            
        Test.startTest();
        Insert testTranscript; 
        List<CookbookBot_GetTranscriptVariables.TranscriptInput> inputList = new  List<CookbookBot_GetTranscriptVariables.TranscriptInput>();
        CookbookBot_GetTranscriptVariables.TranscriptInput  transInput = new CookbookBot_GetTranscriptVariables.TranscriptInput();
        
        transInput.routableID = testTranscript[0].Id;
        inputList.add(transInput);
        
        CookbookBot_GetTranscriptVariables.getUserName(inputList);

        Test.stopTest();
        }}
    
}