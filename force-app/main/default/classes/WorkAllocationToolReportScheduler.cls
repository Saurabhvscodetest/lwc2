/******************************************************************************
* @author       Matt Povey
* @date         09/06/2015
* @description  Class used for exporting data from Salesforce to Capita's Work Allocation Tool
*
* EDIT RECORD
*
* LOG     DATE        Author	JIRA		COMMENT      
* 001     09/06/2015  MP		CMP-98		Original code
*
*/
global without sharing class WorkAllocationToolReportScheduler implements Schedulable {
	private final String WORK_ALLOCATION_REPORT_NAME = 'Work Allocation Tool Report';
	
	/**
	 * Execute job scheduling
	 * Production (06:30:00 & 13:00:00)
	 * System.Schedule('Work Allocation Tool AM','0 30 6 ? * *', new WorkAllocationToolReportScheduler());
	 * System.Schedule('Work Allocation Tool PM','0 0 13 ? * *', new WorkAllocationToolReportScheduler());
	 * For Testing (08:21:00):
	 * System.Schedule('Work Allocation Tool','0 21 08 ? * *', new WorkAllocationToolReportScheduler());
	 */
	global void execute(SchedulableContext sc){
        // Ensure Report ID Custom Setting exists
        String WORK_ALLOCATION_REPORT_ID;
        String WORK_ALLOCATION_REPORT_RECIPIENTS;
        try {
    	    WORK_ALLOCATION_REPORT_ID = Config_Settings__c.getInstance('WORK_ALLOCATION_REPORT_ID').Text_Value__c;
	        // Ensure Report Recipients Custom Setting exists
    	    try {
	    	    WORK_ALLOCATION_REPORT_RECIPIENTS = Config_Settings__c.getInstance('WORK_ALLOCATION_REPORT_RECIPIENTS').Text_Value__c;
		        // Email the report
    		    try {
	    		   	system.debug('MPDEBUG SESSION 1: ' + UserInfo.getSessionId());
        		   	CSVReportEmailer.invokeEmailCSVReportService(WORK_ALLOCATION_REPORT_ID, WORK_ALLOCATION_REPORT_NAME, WORK_ALLOCATION_REPORT_RECIPIENTS, UserInfo.getSessionId());
	        	} catch (Exception e) {
    	    		sendErrorEmail('WorkAllocationToolReportScheduler has encountered the following error: ' + e.getMessage());
        		}
	        } catch (NullPointerException npe) {
        		sendErrorEmail('WorkAllocationToolReportScheduler WORK_ALLOCATION_REPORT_RECIPIENTS Custom Setting is missing');
	        }
        } catch (NullPointerException npe) {
        	sendErrorEmail('WorkAllocationToolReportScheduler WORK_ALLOCATION_REPORT_ID Custom Setting is missing');
        }
	}

	/**
	 * Send error email
	 */
	private static void sendErrorEmail(String errorMessage) {
        system.debug(errorMessage);
        EmailNotifier.sendNotificationEmail('WorkAllocationToolReport Scheduler Error', errorMessage);
	}
}