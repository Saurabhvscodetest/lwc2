/* Base class that Maps the XML Response to the VO object from the EH response */
// public abstract  class EHServiceResponseMapper {
public class EHServiceResponseMapper {
    /*
    public static final String GVF = '2ManGVF';
    public static final String CLICK_AND_COLLECT = 'ClickAndCollect';
    public static final string TIME_OUT = 'Timeout_Milliseconds__c';
    public static final string CERTIFICATE_NAME = 'Certificate_Name__c';
    public static final string POST_HTTP_METHOD = 'POST';
    public static final string EVENT_HUB_GET_EVENTS = 'EVENT_HUB_GET_EVENTS';
    public static final string UNEXP_ERROR_CONDITION =  'Unexpected Error In connection';
    public static final string ERROR_CODE = 'INTERNAL';
    public static final string ERROR_TYPE = 'TECHNICAL';
    public static final String PARCEL_KEY = 'PARCELNO';
    public static final String PHOTO = 'Photo';
    public static final String SIGNATURE = 'Signature';
    public static final String SUCCESS = 'Success';
    public static final String TRUE_VAL = 'TRUE';
    public static final String CONTENT_TYPE = 'Content-type';
    public static final String CONTENT_TYPE_TEXT_OR_XML = 'text/xml';
    public static final String CONNECTION = 'Connection';
    public static final String CONNECTION_VAL = 'close';
    public static final String SOAP_ENV_NS = 'http://schemas.xmlsoap.org/soap/envelope/';
    public static final String SERVER_END_POINT = 'http://soa.johnlewispartnership.com/';
    public static final String SERVICE_NS = SERVER_END_POINT + 'service/es/RetailTransactionShipment/ViewRetailTransactionShipment/v1';
    public static final String EBM_SCHEMA = SERVER_END_POINT  +'schema/ebm/CommonEBM/v1';
    public static final String DATE_REPLACE_LITERAL = 'T';
    public static final String DATE_REPLACE_LITERAL_VALUE = ' ';
    public static final String EH_ERROR_NO_MESSAGE = 'Unexpected Error Condition- No Error Message from EventHub';

    // XML Nodes Used in the  parsing of the Response
    public static final String NODE_SALE_RETURN_LINE_ITEM = 'SaleReturnLineItem';
    public static final String NODE_ITEM = 'Item';
    public static final String NODE_STOCK_ITEM = 'StockItem';
    public static final String NODE_ITEM_ID = 'ItemID';
    public static final String NODE_NAME = 'Name';
    public static final String NODE_BODY = 'Body';
    public static final String NODE_SHOW_RETAIL_SHIPMENT = 'ShowRetailTransactionShipment';
    public static final String NODE_DATA_AREA = 'DataArea';
    public static final String NODE_REATIL_SHIPMENT = 'RetailTransactionShipment';
    public static final String NODE_ACTUAL_SERVICE_LEVEL_CODE = 'ActualServiceLevelCode';
    public static final String NODE_SHOW = 'Show';
    public static final String NODE_HAS_ACTION_RECORDS = 'hasActionStatusRecords';
    public static final String NODE_ORGINAL_APP_AREA= 'OriginalApplicationArea';
    public static final String NODE_ACTION_STATUS_RECORD= 'ActionStatusRecord';
    public static final String NODE_SERVICE_ERROR= 'ServiceError';
    public static final String NODE_ERROR_DESCRIPTION= 'errorDescription';
    public static final String NODE_ERROR_CODE= 'errorCode';
    public static final String NODE_ERROR_TYPE= 'errorType';
    public static final String NODE_FAULT= 'Fault';
    public static final String NODE_FAULT_STRING= 'faultstring';
    public static final String NODE_RESPONSE_CODE= 'responseCode';
    public static final String NODE_REATIL_SHIPMENT_ITEM = 'RetailTransactionShipmentItem';
    public static final String NODE_FULLFILLMENT_EVENT = 'FulfilmentEvent';
    public static final String NODE_ACTUAL_DATE_TIME_STAMP = 'ActualEndDateTimestamp';
    public static final String NODE_PARCEL_FOR_DELIVERY = 'ParcelForDelivery';
    public static final String NODE_ROUTE_DROP_COLL_NODES = 'RouteDropCollectionNodes';
    public static final String NODE_PARCEL_STATUS = 'ParcelStatus';
    public static final String NODE_PARCEL_BAR_CODE = 'ParcelBarCode';
    public static final String NODE_DESCRIPTION = 'Description';
    public static final String NODE_REASON = 'Reason';
    public static final String NODE_LOCATION = 'Location';
    public static final String NODE_PARENT_LOCATION = 'ParentLocation';
    public static final String NODE_LOCATION_DESCRIPTION = 'LocationDescription';
    
    // Abstract methods which the child class implements to map the EH reponse to the VO object
    public abstract DeliveryTrackingVO mapResponse(Dom.XMLNode rootNode);
    
    public DateTime getDateTime(String dateTimeString) {
        DateTime dt = null;
        try{
             if(String.isNotBlank(dateTimeString)) {
                 dt = datetime.valueOfGMT(dateTimeString.replace(DATE_REPLACE_LITERAL, DATE_REPLACE_LITERAL_VALUE));
             }
        } catch (Exception exp) {
            system.debug(Logginglevel.ERROR, 'ERROR ==> Wrong DateFormat' + dateTimeString);
        }
        return dt;
    }
    
    
    public String getProductCode(Dom.XMLNode retailTransactionShipmentItemNode){
        String productCode = null;
        Dom.XMLNode saleReturnLineItemNode = retailTransactionShipmentItemNode.getChildElement(NODE_SALE_RETURN_LINE_ITEM, null) ;
        if(saleReturnLineItemNode != null) {
            Dom.XMLNode itemNode = saleReturnLineItemNode.getChildElement(NODE_ITEM, null) ;
            if(itemNode != null) {
                Dom.XMLNode stockItemNode = itemNode.getChildElement(NODE_STOCK_ITEM, null) ;
                if(stockItemNode != null) {
                    Dom.XMLNode itemIdNode = stockItemNode.getChildElement(NODE_ITEM_ID, null) ;
                    productCode = getValue(itemIdNode);
                }
            }
        }
        return productCode;
        
    }

    public String getProductDescription(Dom.XMLNode retailTransactionShipmentItemNode){
        String productDescription = null;
        Dom.XMLNode saleReturnLineItemNode = retailTransactionShipmentItemNode.getChildElement(NODE_SALE_RETURN_LINE_ITEM, null) ;
        if(saleReturnLineItemNode != null) {
            Dom.XMLNode itemNode = saleReturnLineItemNode.getChildElement(NODE_ITEM, null) ;
            if(itemNode != null) {
                Dom.XMLNode stockItemNode = itemNode.getChildElement(NODE_STOCK_ITEM, null) ;
                if(stockItemNode != null) {
                    Dom.XMLNode nameNode = stockItemNode.getChildElement(NODE_NAME, null) ;
                    productDescription = getValue(nameNode);
                }
            }
        }
        return productDescription;
        
    }


    public static String getDeliveryType(Dom.XMLNode rootNode){
        String deliveryType = null;
        if(rootNode != null) {
            Dom.XMLNode bodyNode= rootNode.getChildElement(NODE_BODY, SOAP_ENV_NS);
            if(bodyNode != null){
                Dom.XMLNode showRetailTransactionShipmentNode= bodyNode.getChildElement(NODE_SHOW_RETAIL_SHIPMENT, SERVICE_NS );
                if(showRetailTransactionShipmentNode != null) {
                    Dom.XMLNode dataAreaNode = showRetailTransactionShipmentNode.getChildElement(NODE_DATA_AREA, null);
                    if(dataAreaNode != null) {
                        List<Dom.XMLNode> dataAreaNodeChildList = dataAreaNode.getChildElements();
                        system.debug('deliveryType---1 [');
                        for(Dom.XMLNode dataAreaNodeChild: dataAreaNodeChildList){
                                                system.debug('deliveryType---2 [');
                            if(dataAreaNodeChild.getName().equalsIgnoreCase(NODE_REATIL_SHIPMENT)) {
                                deliveryType = dataAreaNodeChild.getChildElement(NODE_ACTUAL_SERVICE_LEVEL_CODE, null).getText();
                            }
                        }
                    }
                }
            }
        }
        system.debug('deliveryType [' + deliveryType + ']');
        return deliveryType;
    }
    
    public static DeliveryTrackingVO handleError(Dom.XMLNode rootNode){
        DeliveryTrackingVO  delTrackingVO = new DeliveryTrackingVO();
        DeliveryTrackingVO.ErrorInfo errorInfo = new DeliveryTrackingVO.ErrorInfo();
        Dom.XMLNode errorNode = null;
        if(rootNode != null) {
            Dom.XMLNode bodyNode= rootNode.getChildElement(NODE_BODY, SOAP_ENV_NS);
            if(bodyNode != null){
                Dom.XMLNode showRetailTransactionShipmentNode= bodyNode.getChildElement(NODE_SHOW_RETAIL_SHIPMENT, SERVICE_NS );
                if(showRetailTransactionShipmentNode != null) {
                    Dom.XMLNode dataAreaNode = showRetailTransactionShipmentNode.getChildElement(NODE_DATA_AREA, null);
                    if(dataAreaNode != null) {
                        Dom.XMLNode showNode = dataAreaNode.getChildElement(NODE_SHOW, null);
                        if(showNode !=null) {
                            Boolean hasActionStatusRecords = TRUE_VAL.equalsIgnoreCase(showNode.getAttributeValue(NODE_HAS_ACTION_RECORDS,''));
                            if(hasActionStatusRecords){
                                Dom.XMLNode originalApplicationAreaNode =  showNode.getChildElement(NODE_ORGINAL_APP_AREA, EBM_SCHEMA);
                                if(originalApplicationAreaNode != null){
                                    Dom.XMLNode actionStatusRecordNode =  showNode.getChildElement(NODE_ACTION_STATUS_RECORD, EBM_SCHEMA);
                                    if(actionStatusRecordNode != null){
                                        errorNode =  (actionStatusRecordNode.getChildElement(NODE_SERVICE_ERROR, EBM_SCHEMA));
                                    }
                                }
                            } else {
                                errorNode =  (showNode.getChildElement(NODE_SERVICE_ERROR, EBM_SCHEMA));
                            }

                        }
                    }
                }
            }
        }    
        system.debug('errorNode [' + errorNode + ']');
        if(errorNode != null) {
            errorInfo.errorText = errorNode.getAttributeValue(NODE_ERROR_DESCRIPTION, '');
            errorInfo.errorCode = errorNode.getAttributeValue(NODE_ERROR_CODE,'');
            errorInfo.errortype = errorNode.getAttributeValue(NODE_ERROR_TYPE, '');
            delTrackingVO.errorInfo = errorInfo;
        } else {
            delTrackingVO = getErrorInfo(EH_ERROR_NO_MESSAGE);
        }
        return delTrackingVO;
    }
    
    public static DeliveryTrackingVO handleSoapFault(Dom.XMLNode rootNode){
        DeliveryTrackingVO  delTrackingVO = new DeliveryTrackingVO();
        if(rootNode != null){
            Dom.XMLNode bodyNode = rootNode.getChildElement(NODE_BODY, SOAP_ENV_NS);
            Dom.XMLNode faultNode = bodyNode.getChildElement(NODE_FAULT, SOAP_ENV_NS);
            if(faultNode != null){
                delTrackingVO = getErrorInfo(faultNode.getChildElement(NODE_FAULT_STRING, null).getText());
            }
        }
        return delTrackingVO;
        
    }

    public static String getStatus(Dom.XMLNode rootNode){
        String status = null;
        if(rootNode != null) {
            Dom.XMLNode bodyNode= rootNode.getChildElement(NODE_BODY, SOAP_ENV_NS);
            if(bodyNode != null){
                Dom.XMLNode showRetailTransactionShipmentNode= bodyNode.getChildElement(NODE_SHOW_RETAIL_SHIPMENT, SERVICE_NS );
                if(showRetailTransactionShipmentNode != null) {
                    Dom.XMLNode dataAreaNode = showRetailTransactionShipmentNode.getChildElement(NODE_DATA_AREA, null);
                    if(dataAreaNode != null) {
                        Dom.XMLNode showNode = dataAreaNode.getChildElement(NODE_SHOW, null);
                        if(showNode !=null) {
                            status = showNode.getAttributeValue(NODE_RESPONSE_CODE, '');
                        }
                    }
                }
            }
        }    
        return status;
        
    }
    public static boolean isSoapFault(Dom.XMLNode rootNode){
        boolean isSoapFault = false;
        if(rootNode != null){
            Dom.XMLNode bodyNode = rootNode.getChildElement(NODE_BODY, SOAP_ENV_NS);
            Dom.XMLNode faultNode = bodyNode.getChildElement(NODE_FAULT, SOAP_ENV_NS);
            isSoapFault  = (faultNode != null)? true :false;
        }
        return isSoapFault;
        
    }
    public static Dom.XMLNode getRootNode(String soapResponseDocument){
        Dom.Document doc = new Dom.Document();
        doc.load(soapResponseDocument);
        return doc.getRootElement();
    }
    
    
    public String getValue(Dom.XMLNode node){
        return ((node != null) ? node.getText() : '') ;
    }
    */
    
   /* 
    Populate Error Information from the exception.    
    */
    /*
    public static DeliveryTrackingVO getErrorInfo(String errorDesc){
        DeliveryTrackingVO  delTrackingVO = new DeliveryTrackingVO();
        DeliveryTrackingVO.ErrorInfo errorInfo = new DeliveryTrackingVO.ErrorInfo();
        errorInfo.errorText = UNEXP_ERROR_CONDITION ;
        errorInfo.errorCode = ERROR_CODE;
        errorInfo.errorDescription = errorDesc;
        errorInfo.errortype = ERROR_TYPE;
        delTrackingVO.errorInfo = errorInfo;
        return delTrackingVO;
    }   
    */  
   
}