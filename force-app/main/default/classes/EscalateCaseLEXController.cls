public class EscalateCaseLEXController {

private static final String CHANGE_OWNER_FILTER = 'Filter_from_Change_Owner__c';
public static List<string> ReasonToEscalate=new List<string>();    
/**
* @description  Updates the case with the supplied queue name           
* @author       @ayanhore  
*/ 
    @AuraEnabled
    public static void changeOwnerAction(Id caseId, String queueName,string reason){
        string escalatedQueue;
        List<case> escalatedCase=[select Contact_Reason__c,Reason_Detail__c from case where id=: caseId];
        system.debug(escalatedCase[0].contact_reason__c);
        system.debug(escalatedCase[0].reason_detail__c);
        
        if(queueName=='Auto Escalation'){
            system.debug('---------->'+[select Escalated_Queue__c from case_routing_categorisation__c where contact_reason__c=: escalatedCase[0].contact_reason__c and reason_detail__c=:escalatedCase[0].reason_detail__c].Escalated_Queue__c);
			queueName=[select Escalated_Queue__c from case_routing_categorisation__c where contact_reason__c=: escalatedCase[0].contact_reason__c and reason_detail__c=:escalatedCase[0].reason_detail__c].Escalated_Queue__c;      
            system.debug('--------------------->'+queueName);
            if(queueName==null || queueName==''){
                queueName='Test Queue';
            }
        }
        List<Case> cases = [select Id, ContactId, OwnerId, jl_Case_RestrictedAddComment__c, CaseNumber  From Case where Id = : caseId LIMIT 1];
        List<Group> selectedQueue = [SELECT Id FROM Group WHERE Name = :queueName LIMIT 1];                                    
        cases.get(0).OwnerId = selectedQueue[0].Id;
        cases.get(0).Case_Escalated_by__c=[select id from user where id =:UserInfo.getUserId()].id;
        cases.get(0).Reason_for_Escalation__c=reason; 
        cases.get(0).Escalated_by_Primary_Team__c=[select Team__c from user where id =:UserInfo.getUserId()].Team__c;
        update cases;
		List<Case_Activity__c> cActivity = [Select id,Update_From_Assignment__c from Case_Activity__c where case__c =: caseId and Queue_Completion__c  = null order by LastModifiedDate];
        Case_Activity__c cNew = new Case_Activity__c();   
        cNew.Case__c = caseId;
        cNew.Update_From_Assignment__c = true;
        cNew.Skip_NextActionIfOpen_Validation__c = true;
        if(cActivity.size() > 0){
        cNew.id = cActivity[0].id;
        update cNew;   
        }
    }

/**
* @description  Returns the list of available queues to escalate to         
* @author       @ayanhore   
*/ 
    @AuraEnabled
    public static List<String> getQueuesList(Id caseId){
        List<String> resultedQueues = new List<String>();
        Set<String> includedQueues = QueueOwnerFilterHelper.getNotFilteredQueueRecords(CHANGE_OWNER_FILTER);
        List<QueueSobject> tempOwnerRecords = [SELECT Id, Queue.Name 
                                               FROM QueueSobject 
                                               WHERE Queue.Name  in :includedQueues order by Queue.Name LIMIT 100];
        for(QueueSobject q: tempOwnerRecords){
            resultedQueues.add(q.Queue.Name);
        }
        return resultedQueues;
    }

/**
* @description  Checks if the running user has permission to escalate the case to another queue         
* @author       @ayanhore   
*/     
    @AuraEnabled
    public static boolean checkChangeOwnerAccessPermission(){
        return [SELECT PermissionSet.Name FROM PermissionSetAssignment WHERE AssigneeId = :UserInfo.getUserId() AND PermissionSet.Name = 'Escalate_Access'].size()>0 ? true : false;
    }
    
    @AuraEnabled
    public static Case initializeAssignmentQuickActionComponent(String caseId){
        Case currentCase = [select Id, CaseNumber, jl_Branch_master__c, Contact_Reason__c, Reason_Detail__c, CDH_Site__c, Branch_Department__c, Next_Case_Activity_Due_Date_Time__c,jl_Case_RestrictedAddComment__c from Case where Id = :caseId LIMIT 1].get(0);
		return currentCase.Next_Case_Activity_Due_Date_Time__c >= DateTime.now() ? currentCase : null;
    }
    
    @AuraEnabled
    public static List<string> escaltedQueues(id caseid){
        List<string> queueName=new List<string>();
        for(Escalated_Queues__c e:[select Queue_Name__c from Escalated_Queues__c]){
            queueName.add(e.Queue_Name__c);
        }
        return queueName;
        
    }
    @AuraEnabled
    public static List<string> reasonforescalation(){
        
        String objectName = 'Case';
        String fieldName ='Reason_for_Escalation__c';
        Schema.SObjectType s = Schema.getGlobalDescribe().get(objectName) ;
        Schema.DescribeSObjectResult r = s.getDescribe() ;
        Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            ReasonToEscalate.add(pickListVal.getValue());
        }
        return ReasonToEscalate;
        
    }
    
}