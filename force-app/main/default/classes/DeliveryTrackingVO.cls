/* Value Object to hold the delivery tracking information */
public class DeliveryTrackingVO {
    public static final String SUPPLIER_DIRECT = 'Supplier Direct';
    public static final String GVF_SCREEN = 'GVF Delivery';
    public static final String CC_SCREEN = 'Click&Collect';
    public static final String SEPARATOR = '-';
    public static final String CARRIER_SCREEN = 'Carrier Delivery';
    /* Value Object to hold the delivery tracking request and response */
    public boolean success{get; set;}
    /* Display pannel on the Delivery Tracking screen */
    public static string getFullfillmentType(String key){
        String value = '';
        if(String.isNotBlank(key)){
            value =  key.subStringBefore(SEPARATOR);
        }
        return value;
    }
    
    public static string getFullfillmentKey(String fullfillmentType, Integer index){
        String value = '';
        if(String.isNotBlank(fullfillmentType)){
            value =  fullfillmentType + SEPARATOR + index;
        }
        return value;
    }
    
    public static Integer getFullfillmentTypeIndex(String key){
        Integer index = 0;
        String value = '';
        if(String.isNotBlank(key)){
            value =  key.subStringAfter(SEPARATOR);
        }
        try{
            index = Integer.valueOf(value);
        } catch (Exception exp){}
        return index;
    }
    
    public  Map<String, String> getFullfillmentInfo(){
        Map<String, String> fulfillmentTypeMap = new Map<String, String>();
        if(gvfDelInfList != null && !gvfDelInfList.isEmpty()){
            Integer i = 0;
            for(GVFDeliveryInformation delivery:gvfDelInfList){
                String key = getFullfillmentKey(GVF_SCREEN ,i);
                String value = GVF_SCREEN + SEPARATOR + delivery.deliveryId;
                fulfillmentTypeMap.put(key, value);
                i++;
            }
        } 
        if(sdOrderInfList != null && !sdOrderInfList.isEmpty()){
            Integer i = 0;
            String key = getFullfillmentKey(SUPPLIER_DIRECT ,  i);
            fulfillmentTypeMap.put(key, SUPPLIER_DIRECT);
        } 
        if(cdOrderInf != null) {
            Integer i = 0;
            String key = getFullfillmentKey(CARRIER_SCREEN ,  i);
            fulfillmentTypeMap.put(key, CARRIER_SCREEN);
        }
        if(ccOrderInf != null) {
            Integer i = 0;
            String key = CC_SCREEN + SEPARATOR + i;
            fulfillmentTypeMap.put(key, CC_SCREEN);
        } 
        return fulfillmentTypeMap;
    }
    
    @AuraEnabled public String requestTrackingId {get; set;}
    @AuraEnabled public String orderId {get; set;}
    @AuraEnabled public ErrorInfo errorInfo {get; set;}
    @AuraEnabled public List<GVFDeliveryInformation> gvfDelInfList{get; set;}
    @AuraEnabled public List<Case> caseList{get; set;}  
    @AuraEnabled public String gvfParcelEventHistoryInJSON {
        get{
            /* Populate the GVF Delivery List Information into a a serialized String */
            Map<String, List<ParcelEvent>> parcelEventHistoryMap = new Map<String, List<ParcelEvent>>();
            if(gvfDelInfList != null && !gvfDelInfList.isEmpty()) {
                for(GVFDeliveryInformation delivery:gvfDelInfList){
                    if(delivery.parcelInformationList != null && !delivery.parcelInformationList.isEmpty()) {
                        for(ParcelInformation parcel: delivery.parcelInformationList){
                            String key= delivery.deliveryKey + EHConstants.SPLITTER  + parcel.productKey;
                            parcelEventHistoryMap.put(key, parcel.parcelEvents);
                        }                        
                    }
                }
                //Serialize the Map into a String
                gvfParcelEventHistoryInJSON = JSON.serialize(parcelEventHistoryMap);
            }
            return gvfParcelEventHistoryInJSON;
        }
        set;
    }
    public CCOrderInformation ccOrderInf {get; set;}    
    public CarrierDeliveryInformation cdOrderInf {get; set;}    
    public String cdParcelEventHistoryInJSON {
        get{
            /* Populate the CC Order Information into a a serialized String */
            Integer deliveryIndex = 0;
            Map<String, List<ParcelEvent>> parcelEventHistoryMap = new Map<String, List<ParcelEvent>>();
            if(cdOrderInf != null) {
                if(cdOrderInf.parcelInformationList != null && !cdOrderInf.parcelInformationList.isEmpty()) {
                    for(CarrierParcelInformation parcel: cdOrderInf.parcelInformationList){
                        String key= cdOrderInf.deliveryKey + EHConstants.SPLITTER  + parcel.productKey;
                        parcelEventHistoryMap.put(key, parcel.parcelEvents);
                    }                        
                }
            }
            //Serialize the Map into a String
            return JSON.serialize(parcelEventHistoryMap); 
        } 
        set;
    }
    public String ccParcelEventHistoryInJSON {
        get{
            /* Populate the CC Order Information into a a serialized String */
            Integer deliveryIndex = 0;
            Map<String, List<ParcelEvent>> parcelEventHistoryMap = new Map<String, List<ParcelEvent>>();
            if(ccOrderInf != null) {
                if(ccOrderInf.parcelInformationList != null && !ccOrderInf.parcelInformationList.isEmpty()) {
                    for(ParcelInformation parcel: ccOrderInf.parcelInformationList){
                        String key= ccOrderInf.deliveryKey + EHConstants.SPLITTER  + parcel.productKey;
                        parcelEventHistoryMap.put(key, parcel.parcelEvents);
                    }                        
                }
            }
            //Serialize the Map into a String
            return JSON.serialize(parcelEventHistoryMap); 
        } 
        set;
    }
    public List<SDOrderInformation> sdOrderInfList {get; set;} 
    public String sdParcelEventHistoryInJSON {
        get{
            /* Populate the SD Order Information List into a a serialized String */
            Map<String, List<ParcelEvent>> parcelEventHistoryMap = new Map<String, List<ParcelEvent>>();
            if(sdOrderInfList != null && !sdOrderInfList.isEmpty()) {
                for(SDOrderInformation sdOrder:sdOrderInfList){
                    if(sdOrder.orderLineInformationList != null && !sdOrder.orderLineInformationList.isEmpty()) {
                        for(SDOrderLineItemInformation parcel: sdOrder.orderLineInformationList){
                            String key= sdOrder.deliveryKey + EHConstants.SPLITTER  + parcel.productKey;
                            parcelEventHistoryMap.put(key, parcel.parcelEvents);
                        }                        
                    }
                }
                sdParcelEventHistoryInJSON = JSON.serialize(parcelEventHistoryMap);
            }
            return sdParcelEventHistoryInJSON ;
        }
        set;
    }
    
    public class GVFDeliveryInformation {
        /** GVF fields */
        @AuraEnabled public String deliveryKey {get; set;}
        @AuraEnabled  public String deliveryId {get; set;}
        @AuraEnabled public String deliveryStatus {get; set;}
        @AuraEnabled public String deliveryCDHLocation {get; set;}
        @AuraEnabled public DateTime deliverySlotFrom {get; set;}
        @AuraEnabled  public DateTime deliverySlotTo {get; set;}
        @AuraEnabled public DateTime deliveryPlannedArrivalTime {get; set;}
        @AuraEnabled  public DateTime deliveryPlannedDepartureTime {get; set;}
        @AuraEnabled  public String recipientName {get; set;}
        @AuraEnabled  public String recipientPostcode {get; set;}
        @AuraEnabled  public DateTime deliveryTimeOnStatus {get; set;}
        /* ParcelNo -> to ParcelInformation*/
        @AuraEnabled public List<ParcelInformation> parcelInformationList{get; set;}
    }
    
    public class CarrierDeliveryInformation {
        /** Carrier fields */
        @AuraEnabled public String deliveryKey {get; set;}
        @AuraEnabled  public String orderStatus {get; set;}
        @AuraEnabled public DateTime timeOfTheStatus {get; set;}
        @AuraEnabled public String modeTransport {get; set;}
        @AuraEnabled public String customerName {get; set;}
        @AuraEnabled public String branchName {get; set;}
        
        /* ParcelNo -> to ParcelInformation*/
        @AuraEnabled public List<CarrierParcelInformation> parcelInformationList {get; set;}
    }
    
    public class CCOrderInformation {
        /** CC fields */
        @AuraEnabled  public String deliveryKey {get; set;}
        @AuraEnabled  public String orderNumber {get; set;}
        @AuraEnabled  public String customerName {get; set;}
        @AuraEnabled public DateTime availableForCollDate {get; set;}
        @AuraEnabled public String orderStatus {get; set;}
        @AuraEnabled public String modeTransport {get; set;}
        @AuraEnabled public String expectedLocation { 
            get{
                /* 
If the Product Status and the Latest Event Status are Equal
Get the Latest event current Information
*/
                String expLocation = expectedLocation;
                if(String.isNotBlank(modeTransport) 
                   && String.isNotBlank(expectedLocation)
                   && (EHConstants.JL_CLICK_AND_COLLECT.equalsIgnoreCase(modeTransport))) {
                       expLocation =  expectedLocation.substringAfter(EHConstants.SPLITTER);
                   }
                return expLocation;
            } 
            set;
        }
        @AuraEnabled public DateTime timeOfTheSatus {get; set;}
        
        /* ParcelNo -> to ParcelInformation*/
        @AuraEnabled  public List<ParcelInformation> parcelInformationList{get; set;}
    }
    
    /* Supplier Direct Order Information */
    public class SDOrderInformation {
        /** SD fields */
        @AuraEnabled public String deliveryKey {get; set;}
        @AuraEnabled public String customerName {get; set;}
        @AuraEnabled public String postCode {get; set;}
        @AuraEnabled public String branchName {get; set;}
        @AuraEnabled public String supplierName {get; set;}
        @AuraEnabled public String purchaseOrderNo {get; set;}
        
        /* ParcelNo -> to ParcelInformation*/
        @AuraEnabled public List<SDOrderLineItemInformation> orderLineInformationList {get; set;}
    }
    
    /*Holds the parcel Information which is common across delivery types*/
    public class ParcelInformation {
        @AuraEnabled public String productKey {get; set;}
        @AuraEnabled public String productCode {get; set;}
        @AuraEnabled public String productDescription {get; set;}
        @AuraEnabled public String productNo {get; set;}
        @AuraEnabled public String productStatus {get; set;}
        
        @AuraEnabled public String currentLocation {
            get{
                /* 
If the Product Status and the Latest Event Status are Equal
Get the Latest event current Information
*/ 
                if(String.isNotBlank(productStatus) && 
                   (this.parcelEvents !=null && !this.parcelEvents.isEmpty()) && 
                   (productStatus.equalsIgnoreCase(this.parcelEvents[0].eventStatus))) {
                       return this.parcelEvents[0].currentLocation;
                   }
                return null;
            } 
            set;
        }
        @AuraEnabled public String furtherInformation {
            get{
                /* 
If the Product Status and the Latest Event Status are Equal
Get the Latest event Further Information
*/ 
                if(String.isNotBlank(productStatus) && 
                   (this.parcelEvents !=null && !this.parcelEvents.isEmpty()) && 
                   (productStatus.equalsIgnoreCase(this.parcelEvents[0].eventStatus))) {
                       return this.parcelEvents[0].furtherInformation;
                   }
                return null;
            } 
            set;
        }
        @AuraEnabled public String additionalInformation {
            get{
                /* 
If the Product Status and the Latest Event Status are Equal
Get the Latest event Additional Information/Driver Comments
*/ 
                if(String.isNotBlank(productStatus) && 
                   (this.parcelEvents !=null && !this.parcelEvents.isEmpty() && this.parcelEvents[0].eventStatus !=null) && 
                   (productStatus.equalsIgnoreCase(this.parcelEvents[0].eventStatus))) {
                       return this.parcelEvents[0].additionalInformation;
                   }
                return null;
            } 
            set;
        }
        @AuraEnabled   public DateTime productStatusTime {
            get
            {
                /* Return the latest event time */
                if(this.parcelEvents !=null && !this.parcelEvents.isEmpty()) {
                    return this.parcelEvents[0].eventTime;
                }
                return null;
            } 	
            set;}
        @AuraEnabled  public List<ParcelEvent> parcelEvents{get; set;}
    }
    
    
    /*Holds the parcel Information for Suppier Direct*/
    public class SDOrderLineItemInformation {
        @AuraEnabled	public String productKey {get; set;}
        @AuraEnabled  public String purchaseOrderItemNo {get; set;}
        @AuraEnabled  public DateTime expectedDeliveryDate {get; set;}
        @AuraEnabled  public DateTime promisedDeliveryDate {get; set;}
        @AuraEnabled public String description {get; set;}
        @AuraEnabled public String productStatus {get; set;}
        @AuraEnabled public String fulfilmentType {get; set;}
        @AuraEnabled public String productReason {get; set;}
        @AuraEnabled public List<ParcelEvent> parcelEvents {get; set;}        
    }
    
    /*Holds the parcel Information for Carrier Parcel Information*/
    public class CarrierParcelInformation {
        @AuraEnabled public String productKey {get; set;}
        @AuraEnabled public String productNo {get; set;}
        @AuraEnabled  public String productStatus {get; set;}
        @AuraEnabled  public String orderEventCode {get; set;}
        @AuraEnabled  public String orderEventStatus {get; set;}
        @AuraEnabled public DateTime orderEventTime {get; set;}
        @AuraEnabled public String carrierName {get; set;}
        @AuraEnabled public String deliveryPostCode {get; set;}
        @AuraEnabled  public String destinationCountry {get; set;}
        @AuraEnabled  public String wareHouseName {get; set;}
        @AuraEnabled public String trackingLink {get; set;}
        
        @AuraEnabled  public String currentLocation {
            get{
                /* 
If the Product Status and the Latest Event Status are Equal
Get the Latest event current Information
*/ 
                if(String.isNotBlank(productStatus) && 
                   (this.parcelEvents !=null && !this.parcelEvents.isEmpty()) && 
                   (productStatus.equalsIgnoreCase(this.parcelEvents[0].eventStatus))) {
                       return this.parcelEvents[0].currentLocation;
                   }
                return null;
            } 
            set;
        }
        @AuraEnabled public String furtherInformation {
            get{
                /* 
If the Product Status and the Latest Event Status are Equal
Get the Latest event Further Information
*/ 
                if(String.isNotBlank(productStatus) && 
                   (this.parcelEvents !=null && !this.parcelEvents.isEmpty()) && 
                   (productStatus.equalsIgnoreCase(this.parcelEvents[0].eventStatus))) {
                       return this.parcelEvents[0].furtherInformation;
                   }
                return null;
            } 
            set;
        }
       @AuraEnabled public List<ParcelEvent> parcelEvents {get; set;}        
    }
    
    
    public class ParcelEvent {
        @AuraEnabled  public DateTime eventTime {get; set;}
        @AuraEnabled  public String eventStatus {get; set;}
        @AuraEnabled  public String currentLocation {get; set;}
        @AuraEnabled  public String furtherInformation {get; set;}
		@AuraEnabled  public String additionalInformation {get; set;}
        @AuraEnabled  public String signatureUrl {get; set;}
        @AuraEnabled  public List<String> photoEvidenceUrls {get; set;}
    }
    
    public class ErrorInfo{
        @AuraEnabled  public String errorDescription {get; set;}
        @AuraEnabled  public String errorType {get; set;}
    }
}