/******************************************************************************
* @author       Shion Abdillah
* @date         17/09/2015
* @description  Handler class containing all logic relating to PSE transfer cases. COPT-149
*
* EDIT RECORD
*
* LOG     DATE        Author	JIRA		COMMENT      
* 001     17/09/2015  SA		COPT-149	Original code
* 002     05/10/2015  MP		N/A			Refactor to use clsCaseTriggerHandler static vars.
* 003     05/10/2015  SA		COPT-429	Set Transfer Case flag.
* 004     02/03/2016  MP		COPT-654	Part of case trigger handler refactoring.
*
*******************************************************************************
*/
public with sharing class PSETransferHandler {
	@Testvisible private static String PSE_BRANCH_PREFIX = 'Branch - CCLA ';
	private static Map<Id,Id> casePairingMap = new Map<Id,Id>();
	/* COPT-5272
    private static final Set<Id> emailAndWebRecordTypes = new Set<Id> {Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonStaticUtils.CASE_RT_NAME_EMAIL_TRIAGE).getRecordTypeId(),  Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonStaticUtils.CASE_RT_NAME_WEB_TRIAGE).getRecordTypeId()};
	COPT-5272 */
    
	/**
	 * Set Case Owner and SLA for a PSE Case.
	 * <<004>> Changed from maps to a single case to be called from for loop in clsCaseTriggerHandler
	 * @params:	Case newCase				The new (changed) version of the Case record.
	 *			Case oldCase				The old (pre-change) version of the Case record.
	 *			List<CaseComment> ccList	List of case comments to be inserted.
	 * @rtnval:	void
	 */
	public static void handlePSETransferBeforeUpdate(Case newCase, Case oldCase) {
		system.debug('handlePSETransferBeforeUpdate entered!');
		
		String teamName =  PSE_BRANCH_PREFIX + newCase.Assign_To_New_Branch__c;
		
		if (oldCase.Assign_To_New_Branch__c != newCase.Assign_To_New_Branch__c) {
			/* COPT-5272
            if (emailAndWebRecordTypes.contains(newCase.RecordTypeId)) {
				newCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonStaticUtils.CASE_RT_NAME_PRODUCT_STOCK_ENQUIRY).getRecordTypeId();
				system.debug('TeamUtils.getQueueIdForTeam param: ' + TeamName);
				newCase.ownerid = TeamUtils.getQueueIdForTeam(TeamName);
			} else if (oldCase.Status != 'Closed - Assigned to Another Branch' && newCase.Status != 'Closed - Resolved') {
	                    //PSE change
	                    //newCase.Status = 'Closed - Assigned To Another Branch';
                    	//newCase.jl_Action_Taken__c = 'Closed resolved';
                	}	COPT-5272 */                               
            	SLAUtils.setSLA (newCase, TeamName, newCase.OwnerId, true, false);
            }            
        }      	 
	
	public static void handlePSETransferAfterUpdate(Map<Id,Case> newMap, Map<Id,Case> oldMap) 
	{		
        List<Case> clonedCases = new List<Case>();
        system.debug('handlePSETransferAfterUpdate entered!');            
        for (Case newCase : newMap.values())
        {   
        	Case oldCase = oldMap.get(newCase.Id);
        	if(oldCase.RecordTypeId != Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonStaticUtils.CASE_RT_NAME_PRODUCT_STOCK_ENQUIRY).getRecordTypeId()) {
	        	if((newCase.Origin !=  'Email' &&  newCase.Origin !=  'Web'))
	        	//if((newCase.Origin !=  'Email' &&  newCase.Origin !=  'Web') || oldCase.RecordTypeId == Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonStaticUtils.CASE_RT_NAME_PRODUCT_STOCK_ENQUIRY).getRecordTypeId())
	            {
	            	system.debug('handlePSETransferAfterUpdate Cloning Code entered!');
	            	system.debug('newCase.Assign_To_New_Branch__c: ' + newCase.Assign_To_New_Branch__c);            
		            string TeamName = PSE_BRANCH_PREFIX + newCase.Assign_To_New_Branch__c;                                               	        	          
					if(newCase.Assign_To_New_Branch__c != null)
					{
			            if (oldCase.Assign_To_New_Branch__c != newCase.Assign_To_New_Branch__c && !oldCase.Status.Contains('Closed'))
			            {              		            	                        	                    
			                Case newClonedCase = newCase.clone(false,false,false,false);  
			                system.debug('TeamUtils.getQueueIdForTeam param: ' + TeamName);                          
			                newClonedCase.ownerid = TeamUtils.getQueueIdForTeam(TeamName);
			                system.debug('newClonedCase.ownerid: ' + newClonedCase.ownerid);
			                newClonedCase.PreviousPSECase__c = newCase.Id;                 
			                newClonedCase.ClosedDate = null;                  
			                newClonedCase.Status = 'New';
			                newClonedCase.jl_Action_Taken__c = 'New case created'; 
			                newClonedCase.jl_First_Response_At__c = null;
		            		newClonedCase.jl_First_Response_By__c = null;
		            		newClonedCase.jl_First_Response_By_Warning__c = null;                	
							newClonedCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonStaticUtils.CASE_RT_NAME_PRODUCT_STOCK_ENQUIRY).getRecordTypeId();                  	                    	                                                          
			                System.debug('Closed old case and created a clone with new assigned branch.');	                
			                clonedCases.add(newClonedCase);	                 	                	                        		                            
			            }
					}
	            }
	        }
        }
        
        if(!clonedCases.isEmpty())
        {   
        	system.debug('clonedCases: '+ clonedCases.size());        	        	
            insert clonedCases;                       
            for (Case newClone : clonedCases)
            {
            	CasePairingMap.put(newClone.PreviousPSECase__c,newClone.Id);	
            }                  
            
        	system.debug('CasePairingMap: '+ CasePairingMap);   
        	TransferCaseCommentsToNewCases(CasePairingMap);                       
        }           
	}	
	
    /**
     * Reassign PSE Case Owners on transfer
	 * @params:	List<Case> newCases		List of cases (corresponds to Trigger.new).
	 * @rtnval:	void
     */
	public static void handlePSETransferBeforeInsert(Map<Id,Case> newMap) {
		system.debug('handlePSETransferBeforeInsert entered!');       
        for (Case newCase : newMap.values()) {
        	handlePSETransferBeforeInsert(newCase);            
        }
	}

    /**
     * Reassign PSE Case Owner on transfer
	 * @params:	List<Case> newCase		Case to check for PSE transfer.
	 * @rtnval:	void
     */
	public static void handlePSETransferBeforeInsert(Case newCase) {
		system.debug('handlePSETransferBeforeInsert entered!');       
            string TeamName = PSE_BRANCH_PREFIX + newCase.Assign_To_New_Branch__c;                                                       	                             
		if (newCase.Assign_To_New_Branch__c != null) {
            	system.debug('TeamUtils.getQueueIdForTeam param: ' + TeamName);
            	newCase.ownerid = TeamUtils.getQueueIdForTeam(TeamName);
				newCase.jl_Transfer_case__c = true; // <<03>>
            }                                                     	                                    
	}

    public static void TransferCaseCommentsToNewCases(Map<Id,Id> pCasePairingMap)
    {    	
    	system.debug('pCasePairingMap: '+ pCasePairingMap);
    	try
    	{    		    	
	    	List<CaseComment> newCaseComments = new List<CaseComment>();
	    	if(!pCasePairingMap.IsEmpty())
	    	{ 	    		
	    		List<CaseComment> caseComments = [Select c.SystemModstamp, c.ParentId, c.LastModifiedDate, c.LastModifiedById, c.IsPublished, c.IsDeleted, c.Id, c.CreatedDate, c.CreatedById, c.CommentBody From CaseComment c where c.ParentId IN : pCasePairingMap.keySet()];
	    		system.debug('caseComments: '+ caseComments.size());	    		
				for(Id casePairKey : pCasePairingMap.keySet())
				{
					for(CaseComment caseCommentRec : caseComments)
					{
						if(caseCommentRec.ParentId == casePairKey)
						{
							 CaseComment newClonedComment = caseCommentRec.clone(false,false,true,false);
							 newClonedComment.ParentId = pCasePairingMap.get(casePairKey);
							 newCaseComments.add(newClonedComment);				 
						}					
					}				
				}			
				if(newCaseComments.size() > 0) 
		    	{
		    		system.debug('newCaseComments: '+ newCaseComments.size());
		    		insert newCaseComments;
		    	}
	    	}
    	}
    	catch(Exception ex)
    	{
    		System.debug('Debug: ' + ex);
    	}    				    	
    }      
}