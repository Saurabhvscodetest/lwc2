/*************************************************
coCustomerOrdersController

controller extension for the coCustomerOrders page

Author: Steven Loftus (MakePositive)
Created Date: 13/11/2014
Modification Date: 
Modified By: 

**************************************************/
public with sharing class coEmailContentController {

    // main property holding the order details
    public coOrderDetail Order {get; set;}
    public coOrderDetail.EmailType SelectedEmail {get; set;}

    // private properties
    private String orderId {get; set;}
    private String emailQueueId {get; set;}

    private coSoapManager privateSoapManager;
    // soap manager
    public coSoapManager SoapManager {
        get {
            if (this.privateSoapManager == null) this.privateSoapManager = new coSoapManager();

            return this.privateSoapManager;
        }
    } 

    public coEmailContentController() {

        this.orderId = ApexPages.currentPage().getParameters().get('orderId');
        this.emailQueueId = ApexPages.currentPage().getParameters().get('emailQueueId');

        system.debug(this.orderId);

        getTheOrder();
        
        popualteEmailContent();
        
    }

    // make a copy of the selected email and populate the content
    // there is a limitation here that open subtabs of email content previous to this will break the others
    private void popualteEmailContent() {

        ApexPages.getMessages().clear();
        
        for (coOrderDetail.EmailType email : Order.Emails) {
            
            if (this.emailQueueId.equals(email.MessageQueueId)) {
                this.SelectedEmail = email;
                this.SelectedEmail.Content = this.SoapManager.getEmailContent(this.emailQueueId);

                // so we may have got a null back because of an error...make sure we show this on the email content page
                if (this.SoapManager.Status != 'Success') {

                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, SoapManager.ErrorDescription));
                }

                break;
            }
        }
    }

    private void getTheOrder() {

        ApexPages.getMessages().clear();

        this.Order = this.SoapManager.getOrder(this.orderId);

        // if we got an error back then tell the user
        if (soapManager.Status != 'Success') {

            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, SoapManager.ErrorDescription));
        }
    } 
}