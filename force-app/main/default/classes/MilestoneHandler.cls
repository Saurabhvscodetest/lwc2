/**
* @description  Class to orchestrate the opening and closing of Milestones, the setting of Case priority, 
*               and transitioning Cases between queues. Replaces MilestoneUtils         
* @author       @tomcarman  
*/
//***************************************************************************
//Edit    Date        Author      Comment
//001     ??/??/??    @tomcarman  Original Class
//002     12/06/19    RV          COPT-4681 Code Refactoring
//***************************************************************************

public class MilestoneHandler {
    private static final String EMAIL_FUTURE_UPADTE_RESULTS_LITERAL = 'EMAIL FUTURE UPADTE RESULTS';
    private static List<Case> casesWithMilestones;
    public static List<CaseMilestone> caseMilestoneListToBeUpdated = new List<CaseMilestone>();


    /** PRIORITY CALCULATION + QUEUE TRANSITION **/

    /**
    * @description  Business logic to determine the correct Case.Priority, based on the state of the current open Milestones
    *               Runs as a @future call, because Case Milestones are not committed to the database until after the execution
    *               they are created in.
    * @author       @tomcarman
    * @param        caseIds  A set of the Case Ids to be evaluated
    */

    @future
    public static void setCasePriorityAfterExecution(Set<Id> caseIds) {

        List<Case> casesToUpdate = new List<Case>();

        casesWithMilestones = MilestoneHandlerSupport.getCasesWithMilestones(caseIds);

        // Better structure
        Map<Case, Map<String, CaseMilestone>> caseToMilestoneTypeMap = MilestoneHandlerSupport.getCaseToMilestoneTypeMap(casesWithMilestones);

        for(Case aCase : caseToMilestoneTypeMap.keySet()) {

            Map<String, CaseMilestone> caseMilestoneMap = caseToMilestoneTypeMap.get(aCase);

            String entitlementProcessName = aCase.Entitlement.SlaProcess.Name;

            String newCasePriority;

            //if(caseMilestoneMap.size() == 1 && caseMilestoneMap.containsKey(MilestoneHandlerSupport.NEXT_ACTION_DATE)) { // If there is ONLY a NAD Milestone
            // <<002>>
            if(caseMilestoneMap.containsKey(MilestoneHandlerSupport.NEXT_CASE_ACTIVITY))  // If there is ONLY a NAD Milestone
            {
                newCasePriority = MilestoneHandlerSupport.calculatePriorityForCaseWithNCA(entitlementProcessName, caseMilestoneMap.get(MilestoneHandlerSupport.NEXT_CASE_ACTIVITY));
            } //<<002>>
            else if(caseMilestoneMap.containsKey(MilestoneHandlerSupport.MILESTONE_WARNING) && caseMilestoneMap.containsKey(MilestoneHandlerSupport.MILESTONE_VIOLATION))
            { 
                newCasePriority = MilestoneHandlerSupport.calculatePriorityForCaseWithNCAWV(entitlementProcessName, caseMilestoneMap.get(MilestoneHandlerSupport.MILESTONE_WARNING),caseMilestoneMap.get(MilestoneHandlerSupport.MILESTONE_VIOLATION));
            } 
            else
            {
                newCasePriority = MilestoneHandlerSupport.calculatePriorityForCaseWithoutNCA(entitlementProcessName, caseMilestoneMap);
            }

            if(newCasePriority != null) { // If new Case Priority is not null, set the new one
                If(CommonStaticUtils.isUserId(aCase.ownerId)){
                    aCase.Priority = EntitlementConstants.NORMAL_PRIORITY;
                } else {
                    aCase.Priority = newCasePriority;
                }
                casesToUpdate.add(aCase); // Always re-evaluate the Queue, as even if priority hasnt changed, could need to change queue - eg. TeamA - Actioned -> Reassign to TeamB -> Should get transitioned to TeamB - Actioned
            }

        }

        transitionPriorityCases(casesToUpdate);
        try{
            Database.SaveResult[] srList = Database.update(casesToUpdate,false);
            // Iterate through each returned result
            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('+++ Successfully completed action ' + sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('+++ The following error has occurred.');                    
                        System.debug('+++ '+err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('+++ Fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }
        catch(Exception e){
            system.debug('+++ ERROR : Exception occurred : '+e.getMessage());
        }
        System.debug('+++ MilestoneHandler: casesToUpdate ' + casesToUpdate);

    }


    public static List<Case> setCasePriorityAfterExecutionNonFuture(Set<Id> caseIds) {

        System.debug('***TC*** | MilestoneHandler.cls:68 | caseIds: ' + caseIds);

        List<Case> casesToUpdate = new List<Case>();

        casesWithMilestones = MilestoneHandlerSupport.getCasesWithMilestones(caseIds);

        // Better structure
        Map<Case, Map<String, CaseMilestone>> caseToMilestoneTypeMap = MilestoneHandlerSupport.getCaseToMilestoneTypeMap(casesWithMilestones);

        for(Case aCase : caseToMilestoneTypeMap.keySet()) {

            Map<String, CaseMilestone> caseMilestoneMap = caseToMilestoneTypeMap.get(aCase);

            String entitlementProcessName = aCase.Entitlement.SlaProcess.Name;

            String newCasePriority;

            //if(caseMilestoneMap.size() == 1 && caseMilestoneMap.containsKey(MilestoneHandlerSupport.NEXT_CASE_ACTIVITY)) { // If there is ONLY a NAD Milestone
            
            if(caseMilestoneMap.containsKey(MilestoneHandlerSupport.NEXT_CASE_ACTIVITY))  // If there is ONLY a NAD Milestone
            {    
                newCasePriority = MilestoneHandlerSupport.calculatePriorityForCaseWithNCA(entitlementProcessName, caseMilestoneMap.get(MilestoneHandlerSupport.NEXT_CASE_ACTIVITY));
            }else if(caseMilestoneMap.containsKey(MilestoneHandlerSupport.MILESTONE_WARNING) && caseMilestoneMap.containsKey(MilestoneHandlerSupport.MILESTONE_VIOLATION)) //<<002>>
            { 
                newCasePriority = MilestoneHandlerSupport.calculatePriorityForCaseWithNCAWV(entitlementProcessName, caseMilestoneMap.get(MilestoneHandlerSupport.MILESTONE_WARNING),caseMilestoneMap.get(MilestoneHandlerSupport.MILESTONE_VIOLATION));
            } 
            else
            {
                newCasePriority = MilestoneHandlerSupport.calculatePriorityForCaseWithoutNCA(entitlementProcessName, caseMilestoneMap);
            }

            if(newCasePriority != null && newCasePriority != aCase.Priority) { // If new Case Priority is not null, set the new one
                If(CommonStaticUtils.isUserId(aCase.ownerId)){
                    aCase.Priority = EntitlementConstants.NORMAL_PRIORITY;
                } else {
                    aCase.Priority = newCasePriority;
                }
                casesToUpdate.add(aCase); // Always re-evaluate the Queue, as even if priority hasnt changed, could need to change queue - eg. TeamA - Actioned -> Reassign to TeamB -> Should get transitioned to TeamB - Actioned
            }

        }

        transitionPriorityCases(casesToUpdate);
        
        return casesToUpdate;


    }


    /**
    * @description  Assign a Case to the correct subqueue (eg. WARNING, FAILED) based on the Case.Priority  
    * @author       @tomcarman
    * @param        newCasesList  list of Cases from Trigger.New 
    */

    private static void transitionPriorityCases(List<Case> casesToUpdate) { 

        if(MilestoneHandlerSupport.priorityAssignmentDisabled()) {
            return;
        }

        List<Case> casesOwnedByQueue = MilestoneHandlerSupport.filterCasesOwnedByQueue(casesToUpdate); // Filter Cases which are owned by a Queue

        Map<Id, Team__c> queueOwnerToTeam = EntitlementHandlerSupport.getOwnerToTeamMap(casesOwnedByQueue); // Retrieve the Team__c object for Cases owned by Queue

        for(Case newCase : casesOwnedByQueue) {

            Team__c ownerTeam = queueOwnerToTeam.get(newCase.OwnerId);

            Id queueId;

            if(String.isNotEmpty(newCase.Priority) && ownerTeam != null) { // TODO: Check why with Ant

                if(ownerTeam.Queue_Name__c != null && TeamUtils.mapQueueNameId.containsKey(ownerTeam.Queue_Name__c)) {
                    queueId = TeamUtils.mapQueueNameId.get(ownerTeam.Queue_Name__c); // Get the base-Queue Id (eg. the owner could currently be a WARNING or FAILED queue, but we need Id of the default one)
                }
            }

            // Convert base-Queue Id into the correct one for the Case's current priroty (eg. WARNING or FAILED queue)
            if(queueId != null) {

                if(EntitlementConstants.PRIORITY_TO_QUEUE_MAP.containsKey(newCase.Priority)) {
    
                    String qType = EntitlementConstants.PRIORITY_TO_QUEUE_MAP.get(newCase.Priority);
                    
                    Id relatedQueueId = MilestoneHandlerSupport.getQueueId(queueId, qType);

                    if(relatedQueueId != null) {
                        newCase.OwnerId = relatedQueueId;
                    } 
                }
            }
        }
    }




    /** CLOSING & REOPENING OF MILESTONES */

    /**
    * @description  This method takes a Map of MilestoneType to [Case Ids]. It finds any CaseMilestones (matching the type) related to the Case Ids, and closes them.       
    * @author       @tomcarman
    * @param        milestoneTypeToCases  Map of MilestoneType.Name (string) to Set<Id> of CaseIds
    */

    public static void closeMilestoneByType (Map<String, Set<Id>> milestoneTypeToCases) {

        Set<Id> caseIds = new Set<Id>();

        for(Set<Id> caseIdList : milestoneTypeToCases.values()){
            caseIds.addAll(caseIdList);
        }
        
        DateTime completionDate = Datetime.now();
        
        for(CaseMilestone cm : [SELECT Id, isViolated, CompletionDate,  Case.Id, Case.OwnerId, Case.jl_Last_Queue_Name__c , 
                                CaseId, MilestoneType.Name 
                                FROM CaseMilestone 
                                WHERE CaseId IN :caseIds AND MilestoneType.Name IN :milestoneTypeToCases.keySet() AND CompletionDate = null]) {
            
            if(milestoneTypeToCases.containsKey(cm.MilestoneType.Name)) {
                if(milestoneTypeToCases.get(cm.MilestoneType.Name).contains(cm.CaseId)) {
                    cm.completionDate = completionDate;
                    caseMilestoneListToBeUpdated.add(cm);
                }
            }

        }
    }


    /**
    * @description  Business logic to close all Case Milestones when a Case is closed           
    * @author       @tomcarman
    * @param        caseIds  A set of Case Ids which have Milestones that need closing  
    */

    public static void closeAllMilestones(Set<Id> caseIds) {

        Datetime completionDate = Datetime.now();

        for(CaseMilestone cms : [SELECT Id, isViolated, CompletionDate,  Case.Id, Case.Status, Case.IsClosed, Case.OwnerId, Case.jl_Last_Queue_Name__c, MileStoneType.Name FROM CaseMilestone 
                                 WHERE CaseId IN :caseIds AND CompletionDate = null]) {


            cms.CompletionDate = completionDate;
            caseMilestoneListToBeUpdated.add(cms);

        }
    }



    /**
    * @description  Future call made from EntitlementHandler.reOpenMilestons            
    * @author       @tomcarman
    * @param        caseIds  A Set of CaseIds which need their un-violated CaseMilestones re-opening
    */

    @future public static void reOpenMilestones(Set<Id> caseIds) {

        List<String> milestoneNames = new List<String>{MilestoneHandlerSupport.COMPLAINT_RESOLUTION_MILESTONE, 
                                                        MilestoneHandlerSupport.QUERY_RESOLUTION_MILESTONE,
                                                        MilestoneHandlerSupport.PSE_RESOLUTION_MILESTONE,
                                                        MilestoneHandlerSupport.NKU_RESOLUTION_MILESTONE,
                                                        MilestoneHandlerSupport.TRIAGE_RESOLUTION_MILESTONE,
                                                        MilestoneHandlerSupport.EXCEPTION_RESOLUTION_MILESTONE}; 

        List<CaseMilestone> cmsToUpdate = new List<CaseMilestone>();

        for(Case aCase : [SELECT Id, Is_Owner_A_Queue__c,
                                                    (SELECT Id, CompletionDate, MilestoneType.Name 
                                                     FROM CaseMilestones
                                                     WHERE MilestoneType.Name IN :milestoneNames 
                                                     ORDER BY CreatedDate DESC)
                                                FROM Case 
                                                WHERE Id IN :caseIds 
                                                AND EntitlementId != null]) {

            
            for(CaseMilestone cm : aCase.CaseMilestones){
                cm.completionDate = null;
                cmsToUpdate.add(cm);
            }
        }
    
        update cmsToUpdate;

    }

    
    /**
    * @description  DML to update the Milestones            
    * @author       @tomcarman
    */
    public static void commitAllMilestones() {
        
        if(!caseMilestoneListToBeUpdated.isEmpty()){
            Set<Id> milestoneIds = new Set<Id>();
            Map<Id,CaseMilestone> cmsMap = new Map<Id,CaseMilestone>();
            for(CaseMilestone cm:caseMilestoneListToBeUpdated){
                milestoneIds.add(cm.Id);
                cmsMap.put(cm.Id,cm);
            }
            update cmsMap.values();
            //Milestone Reporting task creation will not invoke any case updates 
            MilestoneReportingTaskHelper.createMilestoneReportingTask(milestoneIds);
        }
        

    }

}