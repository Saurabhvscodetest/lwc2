/**
*  @author: Ramesh Vasudevan
*  @date: 2019-11-20
*  @description:
*      Main logic for Leave MyJL function
*
*  Version history
*  
*
*  001     20/11/19    RV         Interface to update leave request for single customer using shopperId and loyaltyNumber

*/

@RestResource(urlmapping='/MyJL_LeaveRequest/customer/*')
global class MyJL_LeaveRequest{
    
    
    @Httppost
    global static ResponseWrapper postLeaveRequest(String messageId,String shopperId, String loyaltyNumber){
        return LeaveRequest(messageId,shopperId, loyaltyNumber);
    }
        
    global static ResponseWrapper leaveRequest(String messageId,String shopperId, String loyaltyNumber){
        String SERVICE_NAME_FOR_ERROR = 'MyJL Leave Scheme Service';  //Need to change error handling
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;  
        ResponseWrapper responseJSON = new ResponseWrapper();
        try{
            
            //string shopperId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
            //shopperId = 'RaviRamesh';
            //messageId = 'm1000000001';
            //string loyaltyNumber= 'l123456789';
            //ID logHeaderId = 
            LogUtil.startLogHeaderREST(messageId, '', 'WS_LEAVE_CUSTOMER', shopperId, 1);
        
            if( String.isEmpty(messageId)){
                res.statusCode = 400;
                //responseJSON.errorCode = 'MISSING_JL_MESSAGE_ID';
                //responseJSON.errorMessage = 'messageId: is not provided';
                responseJSON = new ResponseWrapper('BAD_REQUEST_PARAMETERS','Badly formatted messageId',messageId);
                MyJL_Util.logRejection(MyJL_Const.ERRORCODE.MESSAGEID_NOT_PROVIDED, '', messageId, SERVICE_NAME_FOR_ERROR);
            }
            else if( String.isEmpty(shopperId)){
                res.statusCode = 400;
                //responseJSON.errorCode = 'MISSING_JL_SHOPPER_ID';
                //responseJSON.errorMessage = 'shopperId: is not provided';
                responseJSON = new ResponseWrapper('BAD_REQUEST_PARAMETERS','Badly formatted shopperId',messageId);
                MyJL_Util.logRejection(MyJL_Const.ERRORCODE.CUSTOMERID_NOT_FOUND, '', shopperId, SERVICE_NAME_FOR_ERROR);
            }
            else if( String.isEmpty(loyaltyNumber)){
                res.statusCode = 400;
                //responseJSON.errorCode = 'MISSING_JL_LOYALTY_NUMBER';
                //responseJSON.errorMessage = 'loyaltyNumber: is not provided';
                responseJSON = new ResponseWrapper('BAD_REQUEST_PARAMETERS','Badly formatted loyaltyNumber',messageId);
                MyJL_Util.logRejection(MyJL_Const.ERRORCODE.LOYALTY_ACCOUNT_NOT_FOUND, '', loyaltyNumber, SERVICE_NAME_FOR_ERROR);
            }
            else {
                system.debug('ramesh both shopperid and message id exist');
                // processing
                List<Loyalty_Account__c> LoyaltyAccountsToBeDeactivatedList = new List<Loyalty_Account__c>();
                Map<String,Loyalty_Account__c> LoyaltyAccountsToBeDeactivatedMap = new Map<String,Loyalty_Account__c>(); 
                
                if(MyJL_Util.isMessageIdUnique(messageId)) {              

                    system.debug('ramesh unique message id exist');
                    //ResponseHeader.MessageId = request.requestHeader.MessageId; 
                    set<string> receivedCustomerIds = new set<string>();
                    if(shopperId != null && !String.isBlank(shopperId )) {
                       
                        receivedCustomerIds.add(shopperId);                                    
                        
                    }
                    set<string> existingCustomerProfiles = new set<string>();
                    map<string, contact> contactProfileToLoyaltyAccount = new map<string, contact>();  //<<006>>
                    
                    // fetch contacts with respective loyaltyAccounts for receivedCustomerIds/shopperIds
                    
                    contact[] contactProfiles = queryContactProfilesWithActiveLoyaltyAccounts(receivedCustomerIds); 
                    
                    system.debug('ramesh contact list '+contactProfiles );
                    Set<Loyalty_Account__c> loyaltyAccounts = new Set<Loyalty_Account__c>();
                    
                    if (contactProfiles.size() > 0){
                        for(contact contactProfile : contactProfiles) {  //<<006>>
                            String customerId = contactProfile.Shopper_Id__c;
                            existingCustomerProfiles.add(customerId);
                            if(contactProfile.MyJL_Accounts__r != null && contactProfile.MyJL_Accounts__r.size() > 0){
                            
                                contactProfileToLoyaltyAccount.put(customerId,contactProfile);
                                loyaltyAccounts.addAll(contactProfile.MyJL_Accounts__r);
                                  
                                
                            }
                           
                        } 
                        system.debug('ramesh Loyal account list '+loyaltyAccounts);
                        
                        
                        if(contactProfileToLoyaltyAccount.containsKey(shopperId)){
                            Set<string> loyaltyAccountNumbers = new Set<string>();
                            for (Loyalty_Account__c la : contactProfileToLoyaltyAccount.get(shopperId).MyJL_Accounts__r){
                                    if (la.name != null)
                                        loyaltyAccountNumbers.add(la.name);
                                }  
                            if(contactProfileToLoyaltyAccount.get(shopperId).MyJL_Accounts__r != null && contactProfileToLoyaltyAccount.get(shopperId).MyJL_Accounts__r.size() > 0 && loyaltyAccountNumbers.contains(loyaltyNumber)){
                                Integer counter=0;
                                for(Loyalty_Account__c la : contactProfileToLoyaltyAccount.get(shopperId).MyJL_Accounts__r){
                                    if(la.IsActive__c == true && !LoyaltyAccountsToBeDeactivatedMap.containsKey(shopperId) && la.name == loyaltyNumber){
                                        //re visit next 3 lines
                                        Loyalty_Account__c dla = deactivateLoyaltyAccounts(la, messageId);
                                        LoyaltyAccountsToBeDeactivatedMap.put(shopperId, dla);
                                        LoyaltyAccountsToBeDeactivatedList.add(dla);
                                        counter++;                                        
                                        
                                    }
                                    
                                }
                                
                                if(counter == 0){
                                    // error handling re visit  loyaltyAccount already disabled 
                                    res.statusCode = 409;
                                    //responseJSON.errorCode = 'NO_ACTIVE_LOYALTY_ACCOUNT_FOUND';
                                    //responseJSON.errorMessage = 'No active Loyaltyaccount found';
                                    responseJSON = new ResponseWrapper('INACTIVE_CUSTOMER_ACCOUNT','Customer account is inactive',messageId);
                                    MyJL_Util.logRejection(MyJL_Const.ERRORCODE.NO_ACTIVE_LOYALTY_ACCOUNT_FOUND, '', shopperId, SERVICE_NAME_FOR_ERROR);
                                                       
                                }
                                system.debug('ramesh LoyaltyAccountsToBeDeactivatedList: '+ LoyaltyAccountsToBeDeactivatedList);
                            }
                            else 
                            {
                                // error handling re visit loyaltyAccount does not exist
                                res.statusCode = 404;
                                //responseJSON.errorCode = 'LOYALTYACCOUNT_NOT_FOUND';
                                //responseJSON.errorMessage = 'Loyalty account not found for the customer';
                                responseJSON = new ResponseWrapper('NOT_FOUND','Customer with shopperId or membership with LoyaltyNumber does not exist',messageId);
                                MyJL_Util.logRejection(MyJL_Const.ERRORCODE.LOYALTY_ACCOUNT_NOT_FOUND, '', shopperId, SERVICE_NAME_FOR_ERROR);
                               
                            }  
                            
                        } else {
                        // error handling re visit contact does not exist
                        res.statusCode = 404;
                        //responseJSON.errorCode = 'NOT_FOUND';
                        //responseJSON.errorMessage = 'Customer does not exist';
                        responseJSON = new ResponseWrapper('NOT_FOUND','Customer with shopperId or membership with LoyaltyNumber does not exist',messageId);
                        MyJL_Util.logRejection(MyJL_Const.ERRORCODE.NO_CUSTOMER_DATA, '', shopperId, SERVICE_NAME_FOR_ERROR);
                    
                        }   
                        
                        
                    } else {
                        // error handling re visit contact does not exist
                        res.statusCode = 404;
                        //responseJSON.errorCode = 'CUSTOMER_NOT_FOUND';
                        //responseJSON.errorMessage = 'Customer does not exist';
                        responseJSON = new ResponseWrapper('NOT_FOUND','Customer with shopperId or membership with LoyaltyNumber does not exist',messageId);
                        MyJL_Util.logRejection(MyJL_Const.ERRORCODE.NO_CUSTOMER_DATA, '', shopperId, SERVICE_NAME_FOR_ERROR);
                    
                    }                                                
                       
                                 
                    if(LoyaltyAccountsToBeDeactivatedList.size()>0){
                        
                        LogUtil.startLogHeaderREST(messageId, '', 'LEAVE_DEACTIVATE_MYJL', shopperId, 1);
                        Database.Saveresult[] saveresults = Database.Update(LoyaltyAccountsToBeDeactivatedList, false);                            
                        system.debug('ramesh saveresult Sucess size'+saveresults.size());
                        
                        //responseJSON = new ResponseWrapper('OK','success',messageId);
                        
                        for(Integer i=0; i<saveresults.size(); i++) {
                                
                            if(saveresults[i].success) {
                                res.statusCode = 204;  
                                //responseJSON = new ResponseWrapper('OK','success',messageId);
                            } 
                            else {
                                for(Database.Error err : saveresults[i].getErrors()) {
                                    res.statusCode = 400;                                       
                                    responseJSON = new ResponseWrapper('FAILED_TO_PERSIST_DATA','Failed during data commit',messageId);
                                    MyJL_Util.logRejection(MyJL_Const.ERRORCODE.FAILED_TO_PERSIST_DATA, '', messageId, SERVICE_NAME_FOR_ERROR);
                                }
                            }
                        }
                        
                    }
                    
                    //Calling Logging Method to Log details 
                    //LogUtil.logDetails(logHeaderID, processCustomerWrapperList);
                    
                } 
                else 
                { //Message Id Unique Check ends here.
                    //to do: implement logging if message id already exists (duplicate)
                    res.statusCode = 400;
                    //responseJSON.errorCode = 'MESSAGEID_ALREADY_EXIST';
                    //responseJSON.errorMessage = 'Message id already exist';
                    responseJSON = new ResponseWrapper('BAD_REQUEST_PARAMETERS','Badly formatted messageId',messageId);
                    MyJL_Util.logRejection(MyJL_Const.ERRORCODE.MESSAGEID_DUPLICATE, '', messageId, SERVICE_NAME_FOR_ERROR);
                   
                }
                
                //update logheader for success
                //LogUtil.updateLogHeader(logHeaderID, responseHeader, null); 
            }
            
        }catch(Exception e){
            system.debug(e.getMessage());
            res.statusCode = 500;
            //responseJSON.errorCode = 'CONNEX_SERVER_ERROR';
            //responseJSON.errorMessage = e.getMessage();
            responseJSON = new ResponseWrapper('CONNEX_SERVER_ERROR','Error occurred when processing the preference update',messageId);
            MyJL_Util.logRejection(MyJL_Const.ERRORCODE.UNHANDLED_EXCEPTION, '', '',SERVICE_NAME_FOR_ERROR);
        }
        
        return responseJSON;
    }
    
    private static contact[] queryContactProfilesWithActiveLoyaltyAccounts(Set<String> customerIds) {  //<<006>>
        //system.debug('ramesh LeaveCustomerResponse-queryContactProfiles list of IDs for the Contact_Profile__c query: ' + customerIds);
        
        List<contact> contactProfiles = new List<contact>();  //<<006>>
        
        if(customerIds != null && customerIds.size() > 0) {
            // MJL-1790
            for (contact cp:[
                SELECT
                id,
                Shopper_ID__c, 
                (Select 
                 id, Name,
                 IsActive__c,
                 contact__c
                 From MyJL_Accounts__r )
                FROM 
                contact
                WHERE
                Shopper_ID__c IN :customerIds
                AND SourceSystem__c = :Label.MyJL_JL_comSourceSystem
            ]) {  //<<006>>
                if (String.isNotBlank(cp.Shopper_ID__c)) {
                    contactProfiles.add(cp);
                }
            }
        }
        
        return contactProfiles;
    }
    
    private static Loyalty_Account__c deactivateLoyaltyAccounts(Loyalty_Account__c la, final String messageId){
        Loyalty_Account__c loyaltyAccount = new Loyalty_Account__c();
        loyaltyAccount.IsActive__c = false;
        loyaltyAccount.Deactivation_Date_Time__c = datetime.now();
        loyaltyAccount.id = la.id;
        loyaltyAccount.MyJL_ESB_Message_ID__c = messageId;
        return loyaltyAccount;
    }
    
    
    global class ResponseWrapper {
        
        global String errorCode {get;set;} 
        global String errorMessage {get;set;}  
        global String messageId {get;set;}   
        
        global ResponseWrapper() {
            this.errorCode = 'OK';
            this.errorMessage = 'Success';
            this.messageId = '';
        }
        
        global ResponseWrapper(string errorCode , string errorMessage, string messageId ) {
            this.errorCode = errorCode ;
            this.errorMessage = errorMessage ;
            this.messageId = messageId ;
        }
    }
    
}