// Unit test to test the Post Join Handler.
// Basically we have Loyalty Accounts marked with a shopperId and Contact Profiles marked with the same shopperId 
// and they should be joined together (but they are not - because the Join started before the Update that created the 
// CP had not finished)
//
//	001		11/06/15	NTJ		Add extra parameter to the constructor
//  002     08/07/16    NA      Decommission Contact Profile      

@isTest
private class myJL_PostJoinHandlerTest {

    static testMethod void testSimple() {
		// create two cp with shopperid one and two
		List<contact> cps = new List<contact>();  //<<002>>
		cps.add(new contact(shopper_ID__c='one', LastName='Smith'));  //<<002>> 
		cps.add(new contact(shopper_ID__c='two', LastName='Jones'));  //<<002>>
		insert cps;

		// create a la with shopperid one
		List<Loyalty_Account__c> las = new List<Loyalty_Account__c>();
		las.add(new Loyalty_Account__c(shopperid__c='one'));
		insert las;
		
		// fire the batch job
		Test.startTest();
		       System.enqueueJob(new myJL_PostJoinHandler(las, 0)); 
		Test.stopTest();
		
		// check the result
		List<Loyalty_Account__c> las2 = [SELECT contact__c FROM Loyalty_Account__c];
		system.assertEquals(1, las2.size());
		system.assertEquals(cps[0].Id, las2[0].contact__c);
    }
}