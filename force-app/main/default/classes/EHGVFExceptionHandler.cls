/******************************************************************************
* @author       Antony John
* @date         10/10/2015
* @description  ClickAndCollect Exception Handler.
*
* LOG     DATE        Author    JIRA                            COMMENT
* ===     ==========  ======    ========================        ============ 
* 001     10/10/2015  AJ        CMP-39, CMP-42                  Initial code
*
*/
public with sharing class EHGVFExceptionHandler extends EHBaseExceptionHandler {
    
    public static Set<String> getActivePicklistValuesForField(Schema.DescribeFieldResult fieldResult) {
        Set<string> activeValues = new Set<string>();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple) {
            if(f.isActive()) {
                activeValues.add(f.getValue());
            }
        }       
        return activeValues;
    }
    
    public static final String CDH_PILOT_ENABLED = 'CDH PILOT ENABLED';    
    
    public static boolean isPilotCdhEnabled = false;
    Set<String> pilotCdhNames = null;
    public static final String DEFAULT_DESCRIPTION = 'Event Hub Exception Raised - Please review delivery tracking and contact customer';
    /* Constructor populate the list of Pilot CDH implementation Locations*/
    public EHGVFExceptionHandler(){
        Config_Settings__c configVal = Config_Settings__c.getInstance(CDH_PILOT_ENABLED);
        isPilotCdhEnabled = (configVal != null) ? configVal.Checkbox_Value__c : false; 
        pilotCdhNames = new Set<String>(EH_PILOT_CDH_Locations__c.getall().Keyset());
    }
    
    /* Handle the EventHub exceptions for GVF delivery type*/  
    public override EventHubVO.EHExceptionResponse handleEventHubExceptions(EventHubVO.EHExceptionRequest ehRequest){
    	Savepoint sp = Database.setSavepoint();
        EventHubVO.EHExceptionResponse ehResponse = null;
        try {
            ehResponse = validateRequest(ehRequest);
            //Check  if the response is null which confirms that there are no validation errors.
            if(ehResponse == null) {
                Task t = null;
                if((!isPilotCdhEnabled) || (isPilotCdhEnabled  && isCDHPilotNameConfigured(ehRequest.gvfInfo.deliveryCdh))) {
                    Case c =  getCase(ehRequest);
                    t = createTaskForCase(ehRequest, c);
                }
                ehResponse = getSucessResponse(t, ehRequest.ehExceptionId);
            }
        } catch(Exception exp){
            Database.rollBack(sp);
            System.debug('Exception occured ' + exp.getMessage()  + '\n'+ exp.getStackTraceString());
            if(exp.getMessage().containsIgnoreCase(EHConstants.DUPLICATE_VALUE)){
                ehResponse = addTaskToExistingcase(ehRequest);
            } else {
                ehResponse = getErrorResponse(exp.getMessage(),ehRequest.ehExceptionId) ;
            }
        }
        return ehResponse;
     }
     
     /* Handle subsequent Exception during concurrent request */
     public override EventHubVO.EHExceptionResponse addTaskToExistingcase(EventHubVO.EHExceptionRequest ehRequest){
         EventHubVO.EHExceptionResponse ehResponse = null;
         try {
            String taskKey = getUniqueKey(ehRequest);
            Case c =  getExistingCase(taskKey);
            Task t = createTaskForCase(ehRequest, c);
            ehResponse = getSucessResponse(t, ehRequest.ehExceptionId);
        } catch(Exception exp){
            System.debug('Exception occured ' + exp.getMessage()  + '\n'+ exp.getStackTraceString());
            ehResponse = getErrorResponse(exp.getMessage(),ehRequest.ehExceptionId) ;
        }
        return ehResponse;
     }
     
     /*
      - Checks if there is a existing case for the Exception ehRequest which is either "closed" status and created within the last 28 days
        or is the case is not closed.
      - If not creates a new case with the provided information from the EvenetHub.
      - If the existing closed case then reopen the same
    */ 
     public Case getCase(EventHubVO.EHExceptionRequest ehRequest){
        Case c =  null;
        String delId = ehRequest.gvfInfo.deliveryId;
        Datetime refDateTime = ehRequest.gvfInfo.deliverySlotTo;
        Datetime ndate = DateTime.Now().AddDays(-1 * Integer.valueOf(EHConstants.CASE_CREATED_DAYS_CRITERIA) );
        List<Case> caseList =  [Select Id,contactid, isClosed from Case where jl_DeliveryNumber__c =: delId  and Reference_DateTime__c =: refDateTime and RecordTypeId in(:EHBaseExceptionHandler.queryCaseRecordTypeId,:EHBaseExceptionHandler.complaintCaseRecordTypeId) and Origin =: EHConstants.EVENT_HUB and ((isClosed = true and createdDate > : ndate) or (isClosed = false)) LIMIT 1];
        
        if(!caseList.isEmpty()) {
            c = caseList[0];
            if(c.isClosed){
                c.Status = EHConstants.STATUS_NEW;
                update c;
            }
            return c;
        }
        
        //Populate the details of the new case
        c = populateCase(ehRequest.ehExceptionName, EHConstants.GVF, ehRequest);
        String receivedPickValueRevise = '';
        String esbBranchValue = ehRequest.enrichmentBranchName;
        if(esbBranchValue!=NULL) {
            Set<String> picklistValues = getActivePicklistValuesForField(Case.jl_Branch_master__c.getDescribe());
            for(String pValues : picklistValues) {
                if(esbBranchValue.containsIgnoreCase(pValues)) {
                    receivedPickValueRevise = pValues;
                }
            }
        } 
        String orderId = ehRequest.gvfInfo.orderId;
        c.Reference_DateTime__c  = ehRequest.gvfInfo.deliverySlotTo;
        c.CDH_location__c = ehRequest.gvfInfo.deliveryCdh.toUpperCase();
        c.jl_DeliveryNumber__c = ehRequest.gvfInfo.deliveryId;
        c.Description = DEFAULT_DESCRIPTION;
        
        //online GVF
        if(ehRequest.gvfInfo.isOnline) {
            c.jl_OrderManagementNumber__c = orderId;
            if(receivedPickValueRevise != NULL && receivedPickValueRevise != '') {
                c.jl_Branch_master__c = receivedPickValueRevise;
            }
            else if(receivedPickValueRevise == NULL || receivedPickValueRevise == '') {
                EH_Field_Mapping__c branchMapForNULL = EH_Field_Mapping__c.getValues('EHOnlineGVFNULLMap');
                c.jl_Branch_master__c = branchMapForNULL.Branch__c;
            }
            EH_Field_Mapping__c allFieldsQueryType = EH_Field_Mapping__c.getValues('EHOnlineGVFQueryMap');
            EH_Field_Mapping__c allFieldsComplaintType = EH_Field_Mapping__c.getValues('EHOnlineGVFComplaintMap');
            if(c.RecordTypeId==queryCaseRecordTypeId) {
                c.Contact_Reason__c = allFieldsQueryType.Contact_Reason__c;
                c.Reason_Detail__c = allFieldsQueryType.Reason_Detail__c;
                c.CDH_Site__c = ehRequest.gvfInfo.deliveryCdh.subStringAfter('- ');
            }
            else {
                c.Contact_Reason__c = allFieldsComplaintType.Contact_Reason__c;
                c.Reason_Detail__c = allFieldsComplaintType.Reason_Detail__c;
                c.CDH_Site__c = ehRequest.gvfInfo.deliveryCdh.subStringAfter('- ');
            }
        }
        //offline GVF
        if(!ehRequest.gvfInfo.isOnline) {
            c.jl_CRNumber__c = orderId;
            if(receivedPickValueRevise != NULL && receivedPickValueRevise != '') {
                EH_Field_Mapping__c allFieldsQueryType = EH_Field_Mapping__c.getValues('EHNotOnlineGVFQueryMap');
                EH_Field_Mapping__c allFieldsComplaintType = EH_Field_Mapping__c.getValues('EHNotOnlineGVFComplaintMap');
                c.jl_Branch_master__c = receivedPickValueRevise;
                if(c.RecordTypeId==queryCaseRecordTypeId) {
                    c.Contact_Reason__c = allFieldsQueryType.Contact_Reason__c;
                    c.Reason_Detail__c = allFieldsQueryType.Reason_Detail__c;
                    c.CDH_Site__c = ehRequest.gvfInfo.deliveryCdh.subStringAfter('- ');
                }
                else {
                    c.Contact_Reason__c = allFieldsComplaintType.Contact_Reason__c;
                    c.Reason_Detail__c = allFieldsComplaintType.Reason_Detail__c;
                    c.CDH_Site__c = ehRequest.gvfInfo.deliveryCdh.subStringAfter('- ');
                }
            }
        }
    	
        c.Status = EHConstants.STATUS_NEW;
        c.EH_KEY__c = getUniqueKey(ehRequest);
        try {
            insert c;
        }
        catch(Exception e) {
            c.jl_Branch_master__c = NULL;
            c.Contact_Reason__c = NULL;
            c.Reason_Detail__c =  NULL;
            c.CDH_Site__c =  NULL;
            insert c;
        }
        return c;
    }
    
    /*
      - Creates a new Task and then assciates with the Case.
    */ 
    private Task createTaskForCase(EventHubVO.EHExceptionRequest ehRequest , Case caseObj){
        Task t = getTask(caseObj, ehRequest);
        t.RecordTypeId = eventHubGVFTaskRecordTypeId;
        t.EH_Current_Location__c = EHLocationMapper.getMappedLocation(ehRequest.gvfInfo.deliveryCdh);
        t.EH_Order_Id__c = ehRequest.gvfInfo.orderId;
        t.EH_Delivery_Status__c = ehRequest.gvfInfo.deliveryStatus;
        t.EH_Delivery_Slot_From__c = ehRequest.gvfInfo.deliverySlotFrom;
        t.EH_Delivery_Slot_To__c = ehRequest.gvfInfo.deliverySlotTo;
        t.EH_Package_Status__c = ehRequest.gvfInfo.packageStatus;
        t.EH_Delivery_CDH__c = ehRequest.gvfInfo.deliveryCdh;
        t.EH_Further_Information__c = ehRequest.gvfInfo.furtherInformation;
        t.EH_Extra_Information__c = ehRequest.gvfInfo.extraInformation;
        t.EH_Delivery_Planned_Arrival_Time__c = ehRequest.gvfInfo.deliveryPlannedArrivalTime;
        t.EH_Delivery_Planned_Departure_Time__c  = ehRequest.gvfInfo.deliveryPlannedDepartureTime;

        String evidencesUrl = '';
        if(ehRequest.gvfInfo.photoEvidences != null) {
            for( EventHubVO.EHPhotoEvidences evindence  : ehRequest.gvfInfo.photoEvidences) {
                evidencesUrl = evidencesUrl + (String.isNotBlank(evindence.evidenceType) ? evindence.evidenceType :'')
                               + ' '
                               +  (String.isNotBlank(evindence.evidenceUrl) ? evindence.evidenceUrl :'') + EHConstants.NEW_LINE;
            }
        }
        t.EH_Photo_Evidences__c = evidencesUrl;
        insert t;
        return t;
    }
     
    /*validate the ehRequest */ 
    private EventHubVO.EHExceptionResponse validateRequest(EventHubVO.EHExceptionRequest ehRequest){
        EventHubVO.EHExceptionResponse response = null;
        String errorMessage = null;
        //check for invalid delivery type     
        if (EHConstants.GVF.equalsIgnoreCase(ehRequest.deliveryType) && (ehRequest.gvfInfo == null) ){
            errorMessage = EHConstants.MANDATORY_PARAMETER_MISSING + EHConstants.SPLITTER + EHConstants.GVF_INFO ; 
        } else if (EHConstants.GVF.equalsIgnoreCase(ehRequest.deliveryType) && (String.isBlank(ehRequest.gvfInfo.deliveryId))){
            errorMessage = EHConstants.MANDATORY_PARAMETER_MISSING + EHConstants.SPLITTER + EHConstants.GVF_INFO + EHConstants.SPLITTER +  EHConstants.DELIVERY_ID ; 
        } else if (EHConstants.GVF.equalsIgnoreCase(ehRequest.deliveryType) && (ehRequest.gvfInfo.deliverySlotto == null)){
            errorMessage = EHConstants.MANDATORY_PARAMETER_MISSING + EHConstants.SPLITTER + EHConstants.GVF_INFO + EHConstants.SPLITTER + EHConstants.DELIVERY_SLOT_TO ; 
        } else if (EHConstants.GVF.equalsIgnoreCase(ehRequest.deliveryType) && (String.isBlank(ehRequest.gvfInfo.deliveryCdh))){
            errorMessage = EHConstants.MANDATORY_PARAMETER_MISSING + EHConstants.SPLITTER + EHConstants.GVF_INFO + EHConstants.SPLITTER + EHConstants.DELIVERY_CDH ;
            //If the Order Id is provided then the online indentifier must be populated.
        } else if (EHConstants.GVF.equalsIgnoreCase(ehRequest.deliveryType) && (String.isNotBlank(ehRequest.gvfInfo.orderId)) &&  (ehRequest.gvfInfo.isOnline == null)){
            errorMessage = EHConstants.MANDATORY_PARAMETER_MISSING + EHConstants.SPLITTER + EHConstants.GVF_INFO + EHConstants.SPLITTER + '- isOnline'  ;
        }
        return getErrorResponse(errorMessage, ehRequest.ehExceptionId);
    }
    
    /* is the pilot implementation enabled for the provided CDH */ 
    private boolean isCDHPilotNameConfigured(String cdhname){
        return (pilotCdhNames.contains(cdhname));
    } 
    
    /* get the uniqueKey */
    public static String getUniqueKey(EventHubVO.EHExceptionRequest ehRequest){
        return '' + ehRequest.gvfInfo.deliveryId + EHConstants.SPLITTER + ehRequest.gvfInfo.deliverySlotTo + EHConstants.SPLITTER + DateTime.now().date() + EHConstants.SPLITTER + DateTime.now().hour(); 
    }
}