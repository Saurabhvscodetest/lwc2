//
// 001	24/03/15	NTJ		Remove comma from matching keys
//
public with sharing class ContactUtils {
    
    public static void updateContactCaseMatchKeys(Contact pContact){
        pContact.Case_Matching_Key_1__c = createMatchKey1(pContact.FirstName, pContact.LastName, pContact.Email);
        pContact.Case_Matching_Key_2__c = createMatchKey2(pContact.FirstName, pContact.LastName, pContact.MailingStreet, pContact.MailingPostalCode); 
    }
    
    
    public static void updateContactProfileCaseMatchKeys(Contact_Profile__c cp){
        cp.Case_Matching_Key_1__c = createMatchKey1(cp.FirstName__c, cp.LastName__c, cp.Email__c);
        cp.Case_Matching_Key_2__c = createMatchKey2(cp.FirstName__c, cp.LastName__c, cp.Mailing_Street__c, cp.MailingPostCode__c);
    }
    
    
    public static String createMatchKey1(String fName, String lName, String email){
        
        //MJS: 02 Oct 2014 - added logic to ensure that the keys are blanked out if any of the 
        //component fields used for the key are null or empty
        
        fName = cleanMatchingValue(fName);
        lName = cleanMatchingValue(lName);
        email = cleanMatchingValue(email);
        
        if(fName != null && fName != '' && lName != null && lName != '' && email != null && email != ''){
            system.debug('createMatchKey1: ' + fName + lName + email);
            return fName + lName + email;
        }
        else {
            return '';
        }
        
    }
    
    
    public static String createMatchKey2(String fName, String lName, String addr1, String postCode){
        
        //MJS: 02 Oct 2014 - added logic to ensure that the keys are blanked out if any of the 
        //component fields used for the key are null or empty    
        
        fName = cleanMatchingValue(fName);
        lName = cleanMatchingValue(lName);
        addr1 = cleanMatchingValue(addr1);
        postCode = cleanMatchingValue(postCode);
        
        if(fName != null && fName != '' && lName != null && lName != '' && addr1 != null & addr1 != '' && postCode != null && postCode != '' ){
            system.debug('createMatchKey2: ' + fName + lName + addr1 + postCode);
            return fName + lName + addr1 + postCode;
        }
        else {
            return '';
        }
        
    }
    
    
    public static String cleanMatchingValue(String theValue){
        
        if(theValue != null){
            
            theValue = theValue.replaceAll('\r\n', '');
            theValue = theValue.replaceAll('\n', '');
            theValue = theValue.replaceAll(',','');
            
            return (theValue).toLowerCase().remove(' ');
            
        } else
            return '';
        
    }
    
    
    public static String cleanNullValue(String theValue){
        
        if(theValue != null){           
            return theValue;            
        } else
            return '';
        
    }
    
    
    public static Boolean validateCaseMatchingKeys(String fName, String lName, String email, String addr1, String postCode){
        
        //MJS: 03 Oct 2014 - updated to check for empty string, clean the values, and positively check 
        //data is valid for at least one key
        //note: IMHO the mechanism should really be changed to validate and return the status of each key separately.
        
        fName = cleanMatchingValue(fName);
        lName = cleanMatchingValue(lName);
        email = cleanMatchingValue(email);
        addr1 = cleanMatchingValue(addr1);
        postCode = cleanMatchingValue(postCode);
        
        if ((fName != null && fName != '' && lName != null && lName != '' && email != null && email != '') ||
            (fName != null && fName != '' && lName != null && lName != '' && addr1 != null && addr1 != '' && postCode != null && postCode != '' )){
                //we can build at least one of the two keys
                return true;
            } else {
                //neither key would be valid
                return false;
            }
    }
    
    public static Boolean validateCaseMatchKey1(String fName, String lName, String email){
        
        //MJS: 03 Oct 2014 - added to specifically validate key1
        
        fName = cleanMatchingValue(fName);
        lName = cleanMatchingValue(lName);
        email = cleanMatchingValue(email);
        
        if (fName != null && fName != '' && lName != null && lName != '' && email != null && email != '') {
            //we can build key1
            return true;
        } else {
            return false;
        }
    }
    
    public static Boolean validateCaseMatchKey2(String fName, String lName, String addr1, String postCode){
        
        //MJS: 03 Oct 2014 - added to specifically validate key2
        
        fName = cleanMatchingValue(fName);
        lName = cleanMatchingValue(lName);
        addr1 = cleanMatchingValue(addr1);
        postCode = cleanMatchingValue(postCode);
        
        if (fName != null && fName != '' && lName != null && lName != '' && addr1 != null && addr1 != '' && postCode != null && postCode != '' ){
            //we can build key2
            return true;
        } else {
            return false;
        }
    }
    
    public static void handleLoyaltyAccountAfterContact(Map<id,Contact> newMaps)
    {
        try
        {
            
            
            List<Loyalty_Account__c> loyaltyAccountsToUpdate = new List<Loyalty_Account__c>();
            // MJL-1790 
            List<Loyalty_Account__c>listofacc= new List<Loyalty_Account__c>();
            if (!newMaps.keySet().isEmpty()) {
                listofacc= new List<Loyalty_Account__c>([Select Id, Email_Address__c,Contact__c ,Contact__r.Email from Loyalty_Account__c where Contact__r.SourceSystem__c =:Label.MyJL_JL_comSourceSystem and Contact__c in :newMaps.keySet()]); 			  	
            }
            Map<Id,Loyalty_Account__c> mapofaccounts = new Map<Id,Loyalty_Account__c>(listofacc);
            system.debug('------------mapofaccounts ---------'+mapofaccounts );
            system.debug('------------------listofacc------------'+listofacc);
            for(Loyalty_Account__c la: listofacc)
            {
                If(mapofaccounts.get(la.id).Contact__r.Email !=la.Email_Address__c)
                {
                    la.Email_Address__c =  mapofaccounts.get(la.id).Contact__r.Email;
                    loyaltyAccountsToUpdate.add(la );
                }
            }
            system.debug('-----------loyaltyAccountsToUpdate----------'+loyaltyAccountsToUpdate);
            if(!loyaltyAccountsToUpdate.isEmpty())
            {
                update loyaltyAccountsToUpdate;
            }   
            
        }
        catch(Exception e) {
            System.debug('An exception occurred: ' + e.getMessage());
            
        }
        
    }
    
    /*
Update VoC_Opt_Out__c on Customers.
*/
    @future
    public static void updateCustomers(Set<String> conMailIds){
        system.debug('updateCustomers   == >' +conMailIds);
        List<Contact> customers = [select ID,VoC_Opt_Out__c,VoC_Opt_Out_By__c from Contact where email IN: conMailIds and VoC_Opt_Out__c = false and VoC_Opt_Out_By__c = null];
        for(Contact customer : customers){
            customer.VoC_Opt_Out__c = true;       
        } 
        if(!customers.isEmpty()){
            update customers;       
        }      
        
    }    
    
    public static void UpdateCustomerOwner(Map<Id, Contact> oldMap, Map<Id, Contact> newMap){
        for (Contact newContact : newMap.values()) {
            Contact oldContact = oldMap.get(newContact.Id);
            if(newContact.Assign_Customer_to_Me__c != oldContact.Assign_Customer_to_Me__c && newContact.Assign_Customer_to_Me__c == true){
            	    newContact.OwnerId = userinfo.getUserId();
            }
        }
    }
}