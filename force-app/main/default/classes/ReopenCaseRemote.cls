//  Edit    Date        Author      Comment
//  001     12/12/15    SA          
//  002     14/11/16    LJ			Rename
global class ReopenCaseRemote {
    
    webService static String ReopenCase(Case caseObj) {
      
        try{
        		System.debug('caseObj: ' + caseObj);
            	Case c = [select id, ownerid, jl_ReOpened__c,jl_Reopen_As_User__c,Status from Case where id = :caseObj.Id];
           		c.OwnerId = caseObj.OwnerId;
            	c.jl_ReOpened__c = caseObj.jl_ReOpened__c;
            	c.jl_Reopen_As_User__c = caseObj.jl_Reopen_As_User__c;
            	c.Status = caseObj.Status;
                update c;
            }
        	catch(Exception e){
            	return 'Error : ' + e.getMessage();
        	} 
        return '';
    }
}