/* Description  : GDPR - Potential Customer Data Deletion
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   26/12/2018        ChandraMouli Maddukuri          Created                COPT-4269
*
*/

global class GDPR_BatchDeletePotentialCustomers implements Database.Batchable<sObject>,Database.Stateful
{
    global Map<Id,String> ErrorMap = new Map<Id,String>();
    global integer Counter = 0;
    private static final DateTime Prior_Date = System.now().addDays(-7);
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        //FInd all potential customer records where CreatedDate is more than one week ago
        String query = 'SELECT Id FROM Potential_Customer__c WHERE CreatedDate < : Prior_Date ' ;
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Potential_Customer__c> scope)
    {
        if(scope.size() > 0)
        {
            try{
                List<Database.deleteResult> srList = Database.delete(scope, false);
                for(Integer result = 0; result < srList.size(); result++) {
                    if (srList[result].isSuccess()) {
                        Counter++;
                    }
                    else {
                        for(Database.Error err : srList[result].getErrors()) {
                            ErrorMap.put(srList.get(result).id,err.getMessage());
                        }
                    }
                }
                /* Delete records from Recyclebin */
                Database.emptyRecycleBin(scope);
            }
            Catch(exception e){
                EmailNotifier.sendNotificationEmail('Exception from GDPR_BatchDeletePotentialCustomers ', e.getMessage()); 
            }
        } 
    }
    
    global void finish(Database.BatchableContext BC) 
    {
        String textBody ='';
        set<Id> allIds = new set<Id>();
        textBody += Counter +' records deleted in object Potential Customer'+'\n';
        if (!ErrorMap.isEmpty()){
            for (Id recordids : ErrorMap.KeySet())
            {
                allIds.add(recordids);
            } 
            textBody+= 'Error log: '+allIds+'\n';
        }
        EmailNotifier.sendNotificationEmail('GDPR - Potential Customer Data Deletion', textBody);
    }
    
}