/**
* Test class for CaseCommentTriggerHandler methods not covered in other tests.
* Currently it only features the test copied from clsUtils_Test
*/
@isTest
private class CaseCommentTriggerHandler_TEST {
    
    private static testmethod void test0() {
        String s = null;
        
        system.assertEquals(s, CaseCommentTriggerHandler.byteAbbreviate(s, 80));
    }
    
    private static testmethod void testLessThan() {
        String s = 'Hello, World';
        
        system.assertEquals(s, CaseCommentTriggerHandler.byteAbbreviate(s, 80));
    }
    
    private static final String TEST40 = '0123456789012345678901234567890123456789';
    private static final String TEST80 = '01234567890123456789012345678901234567890123456789012345678901234567890123456789';
    private static final String TEST50   = '01234567890123456789012345678901234567890123456789';
    
    private static final String TEST100 = TEST50 + TEST50;
    private static final String TEST1000 = TEST100 + TEST100 + TEST100 + TEST100 + TEST100 + TEST100 + TEST100 + TEST100 + TEST100 + TEST100;
    private static final String TEST4000 = TEST1000 + TEST1000 + TEST1000 + TEST1000;
    private static final String TEST8000 = TEST4000 + TEST4000;
    
    private static final String KOREAN50 = '고려사람고고려사람고고려사람고고려사람고고려사람고고려사람고고려사람고고려사람고고려사람고고려사람고';
    private static final String KOREAN100 = KOREAN50 + KOREAN50;
    private static final String KOREAN1000 = KOREAN100 + KOREAN100 + KOREAN100 + KOREAN100 + KOREAN100 + KOREAN100 + KOREAN100 + KOREAN100 + KOREAN100 + KOREAN100;
    private static final String KOREAN2000 = KOREAN1000 + KOREAN1000;
    private static final String KOREAN4000 = KOREAN1000 + KOREAN1000 + KOREAN1000 + KOREAN1000;
    private static final String KOREAN8000 = KOREAN4000 + KOREAN4000;
    
    private static final String K1000_T1000_K1000_T1000 = KOREAN1000 + TEST1000 + KOREAN1000 + TEST1000; 
    
    private static testmethod void testStrings() {
        system.assertEquals(40, TEST40.length());
        system.assertEquals(80, TEST80.length());
        system.assertEquals(50, TEST50.length());
        system.assertEquals(100, TEST100.length());
        system.assertEquals(1000, TEST1000.length());
        system.assertEquals(4000, TEST4000.length());
        system.assertEquals(8000, TEST8000.length());
        system.assertEquals(50, KOREAN50.length());
        system.assertEquals(100, KOREAN100.length());
        system.assertEquals(1000, KOREAN1000.length());
        system.assertEquals(2000, KOREAN2000.length());
        system.assertEquals(4000, KOREAN4000.length());
        system.assertEquals(8000, KOREAN8000.length());     
        system.assertEquals(4000, K1000_T1000_K1000_T1000.length());
    }
    
    private static testmethod void testEquals() {
        String s = TEST4000;
        
        system.assertEquals(TEST4000, CaseCommentTriggerHandler.byteAbbreviate(s, 4000));
    }
    
    private static testmethod void testGreaterThan() {
        String s = TEST4000 + 'ABCDEF';
        String expected = s.substring(0,3997) + '...';
        
        system.assertEquals(expected, CaseCommentTriggerHandler.byteAbbreviate(s, 4000));
    }
    
    private static testmethod void testGreaterThanKorean() {
        String s = KOREAN4000;
        
        String sNew = CaseCommentTriggerHandler.byteAbbreviate(s, 4000);
        system.assertEquals(true, sNew.contains(KOREAN1000));
    }
    
    private static testmethod void testGreaterThanEnglishKorean() {
        String s = K1000_T1000_K1000_T1000;
        
        system.assertEquals(true, CaseCommentTriggerHandler.byteAbbreviate(s, 4000).contains(KOREAN1000));
    }
    
    private static testmethod void testTriggeringMessagesToOM(){
 		
        UnitTestDataFactory.setRunValidationRules(false);
        
        Contact con = UnitTestDataFactory.createContact();
        con.Email = 'a@bb.com';
        con.Prefered_method_of_Contact__c = 'Facebook';
        con.Facebook_username__c = 'facebookuser@test.com';
        insert con;
        Config_Settings__c config =  new Config_Settings__c(Name = 'EMAIL_TO_CASE_USER_ID', Checkbox_Value__c = false);
		insert config;
        Case c = UnitTestDataFactory.createNormalCase(con);
        c.jl_OrderManagementNumber__c = '200000';
        c.jl_Prefers_Contact_By__c = null;        
        insert c;
        List<CaseComment> comments = [SELECT Id FROM CaseComment];
        System.assertEquals(1, comments.size());
        //Increate the code coverage - was 17% initially
        comments[0].commentBody = 'Testing';
        update comments;
	}
   
}