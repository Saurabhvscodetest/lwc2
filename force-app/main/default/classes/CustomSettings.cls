/**
 *	@author: Andrey Gavrikov (westbrook)
 *	@date: 2015-01-21
 *	@description:
 *	   proxy class for accessing Custom Settings 
 *
 *	   This class allows following:
 *
 *	   1. Retrieve custom setting values without having to check for null when getting List setting
 *		e.g. instead of 
 *			Callout_Setting__c setting = Callout_Setting__c.getInstance('some-name');
 *			if (null != setting) {
 *				String val = setting.Field_Name__c;
 *			...
 *			}
 *		you shall retrieve setting value as follows
 *			String val = CustomSettings.getCalloutSetting('some-name').getBoolean('Field_Name__c')
 *			//hint: CustomSettings.getCalloutSetting('some-name') will never return null
 *
 *		2. Initialise custom settings in Unit Tests without persisting values into the DB
 *		this allows to avoid UNABLE_TO_LOCK_ROW typ of errors when several tests run in parallel and initialise same vustom setting
 *
 *		3. De-couple access to custom settings from underlying object names.
 *		- this may be useful when some custom settings objects are merged/split later in the application life cycle
 *		- this also may be useful to hide the fact that custom setting value is taken e.g. from custom label, instead of actual Custom Setting record instance
 *
 *
 *
 *	@example
 *	getting value of a specific field of Hierarchy custom setting
 *		CustomSettings.getMyJLSetting().getBoolean('Activate_Kitchen_Drawer__c')
 *
 *	@example
 *	setting value of a specific field of Hierarchy custom setting in a Unit Test
 *		CustomSettings.TestRecord rec = new CustomSettings.TestRecord();
 *		rec.put('Activate_Kitchen_Drawer__c', true);
 *		CustomSettings.setTestMyJLSetting(UserInfo.getUserId(), rec);
 *
 *	@example
 *	getting value of a specific field of List custom setting
 *		CustomSettings.getCalloutSetting('serviceName').getString('Class_Name__c');
 *
 *	@example
 *	getting all values of a specific List custom setting
 *		final List<CustomSettings.Record> values = CustomSettings.getCalloutSettings('serviceName');
 *
 *	@example
 *	setting value of a specific field of List custom setting in a Unit Test
 *		CustomSettings.TestRecord rec = new CustomSettings.TestRecord();
 *		rec.put('Timeout__c', 10);
 *		CustomSettings.getTestCalloutSetting('myService', rec);
 *	
 *	Version History :   
 *	2015-01-23 - MJL-1466 - AG
 *  2020-01-06 -          - VC 
 *	  Add new values for test record creation in setTestLogHeaderConfig
 */
public with sharing class CustomSettings {
	
	public abstract class Record {

		public abstract Object getRaw(final String fName);
		
		public Boolean getBoolean(final String fName) {
			return null == getRaw(fName)? null: Boolean.valueOf(getRaw(fName));
		}
		public Double getNumber(final String fName) {
			return null == getRaw(fName)? null: Double.valueOf(getRaw(fName));
		}
		public String getString(final String fName) {
			return null == getRaw(fName)? null: String.valueOf(getRaw(fName));
		}
	}

	public class SObjectRecord extends Record {
		private final SObject record;
		
		private SObjectRecord(SObject record) {
			this.record = record;
		}

		public override Object getRaw(final String fName) {
			return null == record? null: record.get(fName);
		}
	}

	@TestVisible
	private class TestRecord extends Record {
		private final Map<String, Object> valueMap;
		
		@TestVisible
		private TestRecord() {
			this.valueMap = new Map<String, Object>();
		}
		public TestRecord(final Map<String, Object> valueMap) {
			this.valueMap = valueMap;
		}

		public override Object getRaw(final String fName) {
			return valueMap.get(fName);
		}
		public void put(final String fName, final Object val) {
			valueMap.put(fName, val);
		}
	}

	//MyJL_Settings__c
	public static Record getMyJLSetting() {
		return getMyJLSetting(UserInfo.getUserId());
	}
	public static Record getMyJLSetting(final Id userOrProfileId) {
		return getHierarchyRecord(MyJL_Settings__c.getInstance(userOrProfileId), 'MyJL_Settings__c', userOrProfileId);
	}

	@TestVisible
	public static void setTestMyJLSetting(final Id userOrProfileId, final Record rec) {
		CustomSettings.setTestHierarchyValue('MyJL_Settings__c', userOrProfileId, rec);
	}

	//Callout_Settings__c
	public static List<Record> getCalloutSettings() {
		return getListRecords(Callout_Settings__c.getAll().values(), 'Callout_Settings__c');
	}
	public static Record getCalloutSetting(final String serviceName) {
		return getListRecord(Callout_Settings__c.getInstance(serviceName), 'Callout_Settings__c', serviceName);
	}
	@TestVisible
	public static void setTestCalloutSetting(final String serviceName, final Record rec) {
		CustomSettings.setTestListValue('Callout_Settings__c', serviceName, rec);
	}

	//Log_Configuration__c
	private static String LOG_HEADER_SETTING_NAME = 'Header Logging'; 
	private static String LOG_DETAIL_SETTING_NAME = 'Detail Logging'; 
	public static List<Record> getLogConfiguration() {
		return getListRecords(Log_Configuration__c.getAll().values(), 'Log_Configuration__c');
	}
	public static Record getLogHeaderConfig() {
		return getListRecord(Log_Configuration__c.getInstance(LOG_HEADER_SETTING_NAME), 'Log_Configuration__c', LOG_HEADER_SETTING_NAME);
	}
	public static Record getLogDetailConfig() {
		return getListRecord(Log_Configuration__c.getInstance(LOG_DETAIL_SETTING_NAME), 'Log_Configuration__c', LOG_DETAIL_SETTING_NAME);
	}

	@TestVisible
	public static void setTestLogHeaderConfig(final Boolean isEnabled, final Integer deleteAfterNDays) {
		CustomSettings.setTestListValue('Log_Configuration__c', LOG_HEADER_SETTING_NAME, 
				new TestRecord(new Map<String, Object>{ 'Enabled__c'   => isEnabled,
													 	'Delete_after_N_Days__c' => deleteAfterNDays,
													 	'Housekeeping_Batch_Size__c' => 1,
													 	'Housekeeping_Records_Per_Run__c' => 3}));
	}
	@TestVisible
	public static void setTestLogDetailConfig(final Boolean isEnabled, final Integer deleteAfterNDays) {
		CustomSettings.setTestListValue('Log_Configuration__c', LOG_DETAIL_SETTING_NAME, 
				new TestRecord(new Map<String, Object>{'Enabled__c' => isEnabled, 'Delete_after_N_Days__c' => deleteAfterNDays}));
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	private static final Map<String, Record> HIERARCHICAL_VALUES = new Map<String, Record>();//Object-name + userOrProfileId -> Record
	private static final Map<String, Map<String, Record>> LIST_VALUES = new Map<String, Map<String, Record>>();//Object-name -> Name -> Record
	
	private static List<Record> getListRecords(final List<SObject> values, final String settingName) {
		if (Test.isRunningTest()) {
			final Map<String, Record> testValuesMap = LIST_VALUES.get(settingName);
			if (null != testValuesMap) {
				return testValuesMap.values();
			}
		}
		final List<Record> records = new List<Record>();
		for (SObject setting: values) {
			records.add(new SObjectRecord(setting));
		}
		return records;
	}
	
	private static Record getHierarchyRecord(final SObject value, final String settingName, final Id userOrProfileId) {
		if (Test.isRunningTest()) {
			Record rec = HIERARCHICAL_VALUES.get(settingName + userOrProfileId);
			if (null != rec) {
				return rec;
			}
		}

		return new SObjectRecord(value);
	}

	private static Record getListRecord(final SObject value, final String settingName, final String name) {
		if (Test.isRunningTest()) {
			Map<String, Record> valuesMap = LIST_VALUES.get(settingName);
			if (null != valuesMap && valuesMap.containsKey(name)) {
				return valuesMap.get(name);
			}
		}

		return new SObjectRecord(value);
	}

	private static void setTestListValue(final String settingName, final String name, final Record rec) {
		Map<String, Record> existingValueMap = LIST_VALUES.get(settingName);
		if (null == existingValueMap) {
			existingValueMap = new Map<String, Record>();
			LIST_VALUES.put(settingName, existingValueMap);
		}
		existingValueMap.put(name, rec);
	}

	private static void setTestHierarchyValue(final String settingName, final Id userOrProfileId, final Record rec) {
		final String key = settingName + userOrProfileId;
		HIERARCHICAL_VALUES.put(key, rec);
	}
}