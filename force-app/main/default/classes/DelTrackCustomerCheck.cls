Public class DelTrackCustomerCheck {

     Public class DelTrackCustomerRequest {
     @InvocableVariable(required= true)
     public String CustomerEnteredName;
     @InvocableVariable(required= true)
     public String EventHubCustomerName;
   }
    
    
    Public class DelTrackCustomerResponse {
     @InvocableVariable(required= true)
     public String CustomerExistResult;
   }
     @Invocablemethod(Label ='Chatbot Customer Name Check' description='Chatbot Customer Name Check')
    Public static List<DelTrackCustomerResponse> DelTrackChatBotProcess(List<DelTrackCustomerRequest > chatbotInputReqList){
        
        DelTrackCustomerResponse chatBotDelResponse = new DelTrackCustomerResponse();
        List<DelTrackCustomerResponse>   chatBotDelResponseList = new List<DelTrackCustomerResponse>();
        
           List<String> chatbotInputs = new List<String>();
        
            for( DelTrackCustomerRequest chatbotReq : chatbotInputReqList ){
           		chatbotInputs.add ( chatbotReq.CustomerEnteredName );
           		chatbotInputs.add ( chatbotReq.EventHubCustomerName );
         }
        
        String resultName = customerCheckMethod(chatbotInputs[0],chatbotInputs[1]);
        
        chatBotDelResponse.CustomerExistResult =resultName;
        
        chatBotDelResponseList.add(chatBotDelResponse);
        
        return chatBotDelResponseList;
    }
    
    Public Static String customerCheckMethod(String CustomerEnteredName,String EventHubCustomerName){
        
        String CustomerNotExists = 'Do Not Exist';
        String NameWithoutMr;
        String NameWithoutMrs;
        String NameWithoutMiss;
        String NameWithoutMs;
        String Mr = 'Mr';
        String Mrs = 'Mrs';
        String Miss = 'Miss';
        String Ms = 'Ms';
        
        if(CustomerEnteredName != NULL && EventHubCustomerName != NULL){
            
        List<String> EventHubCustomerNameList = EventHubCustomerName.split(' ');
        List<String> CustomerEnteredNameList = CustomerEnteredName.split(' ');
        
        if(EventHubCustomerName.containsIgnoreCase(CustomerEnteredName)){
            return 'Exist';
        }
        else if(CustomerEnteredName.containsIgnoreCase(EventHubCustomerName)){
            return 'Exist';
        }
        else if(CustomerEnteredName.startsWithIgnoreCase(Mr)|| CustomerEnteredName.startsWithIgnoreCase(Mrs) || CustomerEnteredName.startsWithIgnoreCase(Miss) || CustomerEnteredName.startsWithIgnoreCase(Ms)){
        if(CustomerEnteredName.containsIgnoreCase(Mr)|| CustomerEnteredName.containsIgnoreCase(Mrs) || CustomerEnteredName.containsIgnoreCase(Miss) || CustomerEnteredName.containsIgnoreCase(Ms)){
              
             if(CustomerEnteredName.startsWithIgnoreCase(Mr) && !CustomerEnteredName.startsWithIgnoreCase(Mrs)){
                 NameWithoutMr = CustomerEnteredName.remove(Mr);
                 if(NameWithoutMr != CustomerEnteredName){
              
                  if(EventHubCustomerName.containsIgnoreCase(NameWithoutMr.remove(' '))){
                    return 'Exist';
                }}
             }else if(CustomerEnteredName.startsWithIgnoreCase(Mrs)){
                 NameWithoutMrs = CustomerEnteredName.remove(Mrs);
                 
                  if(NameWithoutMrs != CustomerEnteredName){
            if(EventHubCustomerName.containsIgnoreCase(NameWithoutMrs.remove(' '))){
                    return 'Exist';
                }
            }
                 
             }
             else if (CustomerEnteredName.startsWithIgnoreCase(Miss)){
                  NameWithoutMiss = CustomerEnteredName.remove(Miss);
                 
                 if(NameWithoutMiss != CustomerEnteredName){
            if(EventHubCustomerName.containsIgnoreCase(NameWithoutMiss.remove(' '))){
                    return 'Exist';
                }
                 
             }
                 
                 else if (CustomerEnteredName.startsWithIgnoreCase(Ms)){
                      NameWithoutMs = CustomerEnteredName.remove(Ms);
					if(NameWithoutMs != CustomerEnteredName){
            if(EventHubCustomerName.containsIgnoreCase(NameWithoutMs.remove(' '))){
                    return 'Exist';
                }
            }                     
                 }
 
        }
           
        }
        }
        
        else if(CustomerEnteredNameList.size()>1){
            for(String str : CustomerEnteredNameList){
                for(String str1 : EventHubCustomerNameList){
                     if(str1 == str){
                    return 'Exist';
                }
                    
                }
            } 
        }
        
        else{
             for(String str : EventHubCustomerNameList){
                if(str == CustomerEnteredName){
                    return 'Exist';
                }
            }
        }
            
        }
       
        else{
            return CustomerNotExists; 
        }
        
    return CustomerNotExists;
        
    }
 
}