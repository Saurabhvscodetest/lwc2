@isTest
public with sharing class TaskTestDataFactory {
	
	public static Task createTask(Id whoId, Id whatId, String recordType){

		Id recordTypeId = CommonStaticUtils.getRecordTypeID('Task', recordType);

		Task task = new Task();
		task.whoId = whoId;
		task.whatId = whatId;
		task.RecordTypeId = recordTypeId;

		return task;
	}
}