/******************************************************************************
* @author       Matt Povey
* @date         23/07/2015
* @description  Handler class containing all logic relating to PCD updates into
*				Salesforce, i.e. updating superseded contacts.
*
* EDIT RECORD
*
* LOG     DATE        Author	JIRA		COMMENT      
* 001     23/07/2015  MP		CMP-507		Original code
* 002     25/08/2015  MP		CMP-542		Append .spd to email addresses
* 003     08/09/2015  MP		CMP-677		Improved error handling
* 004     08/09/2015  MP		CMP-678		Validations in before update
* 005     23/12/2015  RK		CMP-945		Track contact historty
*
*******************************************************************************
*/
public without sharing class PCDUpdateHandler {
	private static final Config_Settings__c PCD_USER_ID_CONFIG = Config_Settings__c.getInstance('PCD_USER_ID');
	private static final Id PCD_USER_ID = PCD_USER_ID_CONFIG != null ? PCD_USER_ID_CONFIG.Text_Value__c : null;
	public static Boolean IS_RUNNING_PCD_UPDATE = PCD_USER_ID != null && PCD_USER_ID == UserInfo.getUserId();
	public static Boolean BATCH_MODE = false;
	private static final Config_Settings__c PCD_UPDATE_BATCH_MODE_ONLY = Config_Settings__c.getInstance('PCD_UPDATE_BATCH_MODE_ONLY');
	private static final Boolean RUN_IN_BATCH_MODE_ONLY = PCD_UPDATE_BATCH_MODE_ONLY != null ? PCD_UPDATE_BATCH_MODE_ONLY.Checkbox_Value__c : false;
	private static final Id SUPERSEDED_RECORD_TYPE = [SELECT Id FROM RecordType WHERE SobjectType = 'Contact' AND DeveloperName = 'Superseded' LIMIT 1].Id;
	
    /**
     * Method to handle SCV id updates in before update trigger
	 * @params:	Map<Id, Contact> oldMap		Map of Contacts from Trigger.oldMap
	 *			Map<Id, Contact> newMap		Map of Contacts from Trigger.newMap
     */
	public static void handleContactBeforeUpdate(Map<Id, Contact> oldMap, Map<Id, Contact> newMap) {
		if(!(RUN_IN_BATCH_MODE_ONLY && !BATCH_MODE)) {
			system.debug('*** ENTERING PCDUpdateHandler.handleContactBeforeUpdate ***');
			Map<String, List<Contact>> supersededContactXrefMap = new Map<String, List<Contact>>();
		
			// Run through contacts and build set where the SCV ID has been updated from PCD
			for (Contact newContact : newMap.values()) {
				Contact oldContact = oldMap.get(newContact.Id);
				if (newContact.Superseded_By_SCV_Id__c != null && ((!BATCH_MODE && oldContact.Superseded_By_SCV_Id__c == null) || (BATCH_MODE && newContact.Superseded_By_Customer__c == null))) {
					List<Contact> conList;
					if (supersededContactXrefMap.containsKey(newContact.Superseded_By_SCV_Id__c)) {
						conList = supersededContactXrefMap.get(newContact.Superseded_By_SCV_Id__c);
						conList.add(newContact);
					} else {
						conList = new List<Contact> {newContact};
						supersededContactXrefMap.put(newContact.Superseded_By_SCV_Id__c, conList);
					}
				}
			}
		
			// If there are new superseding SCV Ids get the records they belong to and update the Salesforce lookup and record type Id
			if (!supersededContactXrefMap.isEmpty()) {
				List<Contact> masterContacts = [SELECT Id, PCD_Id__c FROM Contact WHERE PCD_Id__c IN :supersededContactXrefMap.keySet()];
				for (Contact master : masterContacts) {
					List<Contact> supersededContacts = supersededContactXrefMap.remove(master.PCD_Id__c); // changed from 'get' to 'remove' <<004>>
					for (Contact supersededContact : supersededContacts) {
						supersededContact.Superseded_By_Customer__c = master.Id;
						supersededContact.RecordTypeId = SUPERSEDED_RECORD_TYPE;
						if (supersededContact.Email != null) {
							supersededContact.Email = supersededContact.Email.left(76) + '.spd'; // <<002>>
						}
					}
				}
			}
			
			// <<004>> If any entries remain in the Xref Map they are invalid and need to be flagged as errors.
			if (!supersededContactXrefMap.isEmpty()) {
				for (String dodgyPCDID : supersededContactXrefMap.keySet()) {
					List<Contact> errorContacts = supersededContactXrefMap.get(dodgyPCDID);
					for (Contact errorContact : errorContacts) {
						errorContact.addError('Invalid Superseded By PCD Id specified: ' + dodgyPCDID);
					}
				}
			}
		}
	}
	
    /**
     * Method to handle SCV id updates in after update trigger
     * Reparents the following linked records to the superseding contact record:
     * 		- Case
     *		- Task
     *		- Contact Profile
	 * @params:	Map<Id, Contact> oldMap		Map of Contacts from Trigger.oldMap
	 *			Map<Id, Contact> newMap		Map of Contacts from Trigger.newMap
     */
	public static void handleContactAfterUpdate(Map<Id, Contact> oldMap, Map<Id, Contact> newMap) {
		if(!(RUN_IN_BATCH_MODE_ONLY && !BATCH_MODE)) {
			system.debug('*** ENTERING PCDUpdateHandler.handleContactAfterUpdate ***');
			Map<Id, Id> reparentingXrefMap = new Map<Id, Id>();
		
			// Build xref map of superseded contacts and the new master contact id
			for (Contact newContact : newMap.values()) {
				Contact oldContact = oldMap.get(newContact.Id);
				if (oldContact.Superseded_By_Customer__c == null && newContact.Superseded_By_Customer__c != null) {
					reparentingXrefMap.put(newContact.Id, newContact.Superseded_By_Customer__c);
				}
			}
		
			// If any records have been superseded, reparent linked records
			if (!reparentingXrefMap.isEmpty()) {
				List<Contact_Profile__c> profilesToUpdate = new List<Contact_Profile__c>();
				List<Case> casesToUpdate = new List<Case>();
				List<Task> tasksToUpdate = new List<Task>();
				Map<Id, Id> oldContactXrefMap = new Map<Id, Id>();

				List<Contact> supersededContactList = [SELECT Id, Name, (SELECT Id, Contact__c FROM ContactProfiles__r), (SELECT Id, ContactId FROM Cases), (SELECT Id, WhoId FROM Tasks) FROM Contact WHERE Id IN :reparentingXrefMap.keySet()];
				
				for (Contact con : supersededContactList) {
					for (Contact_Profile__c thisProfile : con.ContactProfiles__r) {
						oldContactXrefMap.put(thisProfile.Id, thisProfile.Contact__c);
						thisProfile.Previous_Contact__c = thisProfile.Contact__c; //<<005>> 
						thisProfile.Contact__c = reparentingXrefMap.get(thisProfile.Contact__c);
						profilesToUpdate.add(thisProfile);
					}
					for (Case thisCase : con.Cases) {
						oldContactXrefMap.put(thisCase.Id, thisCase.ContactId);
						thisCase.Previous_Contact__c = thisCase.ContactId;//<<005>>
						thisCase.ContactId = reparentingXrefMap.get(thisCase.ContactId);
						casesToUpdate.add(thisCase);
					}
					for (Task thisTask : con.Tasks) {
						oldContactXrefMap.put(thisTask.Id, thisTask.WhoId);
						thisTask.Previous_Contact__c = thisTask.WhoId;//<<005>>
						thisTask.WhoId = reparentingXrefMap.get(thisTask.WhoId);
						tasksToUpdate.add(thisTask);
					}
				}
			
				Savepoint sp = Database.setSavepoint();

				try {
					// <<003>> Start
					Set<Id> contactErrorRecords = new Set<Id>();
					if (!profilesToUpdate.isEmpty()) {
						List<Database.SaveResult> profileUpdateResults = Database.update(profilesToUpdate, false);
						processUpdateErrors(profileUpdateResults, profilesToUpdate, newMap, oldContactXrefMap, contactErrorRecords, 'Contact Profile');
					}
					if (contactErrorRecords.isEmpty() && !casesToUpdate.isEmpty()) {
						List<Database.SaveResult> caseUpdateResults = Database.update(casesToUpdate, false);
						processUpdateErrors(caseUpdateResults, casesToUpdate, newMap, oldContactXrefMap, contactErrorRecords, 'Cases');
					}
					if (contactErrorRecords.isEmpty() && !tasksToUpdate.isEmpty()) {
						List<Database.SaveResult> taskUpdateResults = Database.update(tasksToUpdate, false);
						processUpdateErrors(taskUpdateResults, tasksToUpdate, newMap, oldContactXrefMap, contactErrorRecords, 'Tasks');
					}
					if (!contactErrorRecords.isEmpty()) {
						Database.rollback(sp);
						system.debug('*** PCDUpdateHandler.handleContactAfterUpdate ROLLUP ERROR - ROLLBACK DONE ***');
						system.debug('PCDUpdateHandler ERROR SET: ' + contactErrorRecords);
						for (Id contactId : reparentingXrefMap.keySet()) {
							if (!contactErrorRecords.contains(contactId)) {
								Contact con = newMap.get(contactId);
								con.addError('Error encountered with another record in this batch - no updates committed');
							}
						}
					} else {
						system.debug('*** PCDUpdateHandler.handleContactAfterUpdate ALL ROLLUPS COMPLETED ***');
					}
					// <<003>> End
				} catch (Exception ex) {
					Database.rollback(sp);
	    			EmailNotifier.sendNotificationEmail('PCD Update Fatal Error', 'Error encountered when processing PCD Updates - please report to development team.\n\nError Details:\n' + ex.getMessage());
				}
			}
		}
        if(!Test.isRunningTest()) {
        Set<Id> conIds = NEW Set<Id>();
        for(Contact c : newMap.values()) {
            conIds.add(c.Id);
        }
    
        List<Contact> conList = [ SELECT Id, MobilePhone, HomePhone, Phone, OtherPhone, (SELECT Custom_Contact_Phone__c FROM Cases) FROM Contact WHERE Id IN: conIds];
        List<Case> updateCasList = NEW List<Case>();
        if(conIds != NULL) {
            for(Contact c : conList) {
                for(Case cas : c.Cases) {
                    updateCasList.add(cas);
                }
            }
            if(updateCasList != NULL) {
            	Database.update(updateCasList, false);
            }}
    	}
	}
	
    /**
     * <<003>> Common method to process update results.  Stamps any errors against the linked contacts
     * Reparents the following linked records to the superseding contact record:
     * 		- Case
     *		- Task
     *		- Contact Profile
	 * @params:	List<Database.SaveResult> resultList	List of results to process
	 *			List<SObject> updateList				The list of objects we tried to update
	 *			Map<Id, Contact> newMap					Map of Contacts from Trigger.newMap
	 *			Map<Id, Id> oldContactXrefMap			Map of Case/ContactProfile/Task Id to the old Contact Id
	 *			Set<Id> contactErrorRecords				Set storing the list of contact ids that errored (used later when generic errors added to other PCD updates in the batch)
	 *			String errorObject						The name of the object being updated that errored.  Used in error message output.
     */
	private static void processUpdateErrors(List<Database.SaveResult> resultList, List<SObject> updateList, Map<Id, Contact> newMap, Map<Id, Id> oldContactXrefMap, Set<Id> contactErrorRecords, String errorObject) {
		Integer listSize = resultList.size();
		for (Integer i = 0; i < listSize; i++) {
			Database.SaveResult sr = resultList.get(i);
			if (!sr.isSuccess()) {
				SObject updObj = updateList.get(i);
				Id linkedContactId = oldContactXrefMap.get(updObj.Id);
				contactErrorRecords.add(linkedContactId);
				Contact linkedContact = newMap.get(linkedContactId);
				linkedContact.addError('Error updating ' + errorObject + ': ' + sr.getErrors());
				system.debug('PCDUpdateHandler.processUpdateErrors: ' + errorObject + ': ' + sr.getErrors());
			}
		}
	}
}