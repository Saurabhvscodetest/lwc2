//	001		15/04/15	NTJ		MJL-1790 - Do not do a SELECT WHERE x IN set, if we have an empty set

public with sharing class ContactProfileUtils {

    public static void handleLoyaltyAccountAfterContactProfile(Map<id,Contact_Profile__c> newMaps, Boolean skipAllEventsForCurrentUser)
    {
        try
        {
             if(!skipAllEventsForCurrentUser){
              List<Loyalty_Account__c> loyaltyAccountsToUpdate = new List<Loyalty_Account__c>();
              // MJL-1790 
              List<Loyalty_Account__c>listofacc= new List<Loyalty_Account__c>();
			  if (!newMaps.keySet().isEmpty()) {
	              listofacc= new List<Loyalty_Account__c>([Select Id, Email_Address__c,Customer_Profile__c ,Customer_Profile__r.Email__c from Loyalty_Account__c where Customer_Profile__r.SourceSystem__c =:Label.MyJL_JL_comSourceSystem and Customer_Profile__c in :newMaps.keySet()]); 			  	
			  }
              Map<Id,Loyalty_Account__c> mapofaccounts = new Map<Id,Loyalty_Account__c>(listofacc);
              //System.debug('------------mafromlist ---------'+mapFromList );
              //system.debug('------------mapofaccounts ---------'+mapofaccounts );
              //system.debug('------------------listofacc------------'+listofacc);
              for(Loyalty_Account__c la: listofacc)
              {
                  If(mapofaccounts.get(la.id).Customer_Profile__r.Email__c !=la.Email_Address__c)
                  {
                      la.Email_Address__c =  mapofaccounts.get(la.id).Customer_Profile__r.Email__c;
                      loyaltyAccountsToUpdate.add(la );
                  }
              }
              //system.debug('-----------loyaltyAccountsToUpdate----------'+loyaltyAccountsToUpdate);
              if(!loyaltyAccountsToUpdate.isEmpty())
                 {
                    update loyaltyAccountsToUpdate;
                 }   
            }
        }
        catch(Exception e) {
            System.debug('An exception occurred: ' + e.getMessage());
           
        }
        
    }
    
}