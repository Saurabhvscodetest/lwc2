/******************************************************************************
* @author       Matt Povey
* @date         8 Jul 2016
* @description  This class is used to provide pagination to PageBlockTables in VF Pages / Components.
*
* EDIT RECORD
*
* LOG     DATE        Author	JIRA		COMMENT      
* 001     8 Jul 2016  MP		COPT-nnn	Original code
*
*******************************************************************************
*/
public virtual class DataTablePaginator {
	public List<SObject> fullDataList {get; set;}
	public Integer noOfRecords {get; set;}
	public Integer pageSize {get; set;}
    public String sortField {get; set;}
    public String previousSortField {get; set;}
    	
	public ApexPages.Standardsetcontroller setCon {
		get {
			if (setCon == null) {
				setCon = new ApexPages.Standardsetcontroller(fullDataList);
				setCon.setPageSize(pageSize);
				setCon.setPageNumber(1);
			}
			return setCon;
		}
		set;
	}

	public Boolean hasNext {
		get {
			return setCon.getHasNext();
		}
		set;
	}
	
	public Boolean hasPrevious {
		get {
			return setCon.getHasPrevious();
		}
		set;
	}
	
	public Integer pageNumber {
		get {
			return setCon.getPageNumber();
		}
		set;
	}
	
	public void first() {
		setCon.first();
	}
	
	public void last() {
		setCon.last();
	}
	
	public void previous() {
		setCon.previous();
	}
	
	public void next() {
		setCon.next();
	}

    public void doSort(){
        if (!fullDataList.isEmpty()) {
			String order = 'asc';
			
			/*This checks to see if the same header was click two times in a row, if so
			it switches the order.*/
			if (previousSortField == sortField) {
				order = 'desc';
				previousSortField = null;
			} else {
				previousSortField = sortField;
			}

			//To sort the table we simply need to use this one line, nice!
			SuperSort.sortList(fullDataList, sortField, order);
			setCon = null;
        } else {
        	sortField = null;
        	previousSortField = null;
        }
	}
}