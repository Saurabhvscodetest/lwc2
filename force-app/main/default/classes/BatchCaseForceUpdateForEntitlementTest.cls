/**
* @description	Unit tests for BatchCaseForceUpdateForEntitlement and BatchCaseForceUpdateForEntScheduler		
* @author		@tomcarman
* @date 		2017-09-24	
*/
@isTest
private class BatchCaseForceUpdateForEntitlementTest {
	
	// Constants
	private static Integer NUMBER_OF_CASES_TO_CREATE = 5;
	private static String HAMILTON_CST_QUEUE_NAME = 'CST_Hamilton_Didsbury';
	private static String HAMILTON_CST_QUEUE_NAME_FAILED = 'CST_Hamilton_Didsbury_FAILED';
    private static String HAMILTON_CST_QUEUE_NAME_ACTIONED = 'CST_Hamilton_Didsbury_ACTIONED';



	/**
	* @description	@testSetup, creates config for entitlements			
	* @author		@tomcarman	
	*/

	@testSetup static void initTestData() {

		MilestoneUtilsTest.initEntitlementTestData();

		// Turn on batch processing for some teams
		List<Team__c> teams = [SELECT Id 
								FROM Team__c 
								WHERE Name = :MilestoneUtilsTest.HAMILTON_CRD_TEAM_NAME 
								OR Name = :MilestoneUtilsTest.HAMILTON_CST_TEAM_NAME];

		for(Team__c team : teams) {
			team.Batch_Process_Case_Milestones__c = true;
		}

		update teams;
		
		// Turn on email logging
		Config_Settings__c enableEmailLogging = new Config_Settings__c();
		enableEmailLogging.Name = 'Enable Email Logging for Batch';
		enableEmailLogging.Checkbox_Value__c = true;
		enableEmailLogging.Number_Value__c = 1;
		insert enableEmailLogging;


	}


	/**
	* @description	Main test - creates Cases with Entitlements, then artificially sets CreateDate in past (to violate Milestones), and calls batch to verify
	*  				that Cases are correctly moved to failed Queue
	* @author		@tomcarman		
	*/

	@isTest static void testFailedCasesGetMovedToFailedQueue() {

		User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		User cstUser;
		Contact testContact;
		List<Case> casesToInsert = new List<Case>();


		// Create CST User
		System.runAs(runningUser) {

			cstUser = UnitTestDataFactory.getFrontOfficeUser('CSTUSER');
			cstUser.Team__c = MilestoneUtilsTest.HAMILTON_CST_ENTITLEMENT;
			insert cstUser;

			testContact = UnitTestDataFactory.createContact();
			insert testContact;

		}

		// Create Test Cases
		System.runAs(cstUser) {

			for(Integer i = 0; i < NUMBER_OF_CASES_TO_CREATE; i++) {
				casesToInsert.add(createCase());
			}

			insert casesToInsert;
		}


		Set<Id> ownerIds = new Set<Id>();
		Set<Id> caseIds = new Set<Id>();

		for(Case aCase : [SELECT Id, Priority, Owner.Name, OwnerId, EntitlementId FROM Case]) {
			caseIds.add(aCase.Id);
			//System.assertNotEquals(null, aCase.EntitlementId, 'Case should have an Entitlement');
			//System.assertEquals('Normal', aCase.Priority, 'Case Priority should be normal');

			ownerIds.add(aCase.OwnerId);
		}

		for(Group ownerQueue : [SELECT Id, Name, DeveloperName FROM Group WHERE Id = :ownerIds]) {
			//System.assertEquals(HAMILTON_CST_QUEUE_NAME, ownerQueue.DeveloperName, 'Should be owned by Default Queue');
		}

		for(CaseMilestone caseMilestone : [SELECT Id, IsViolated, CompletionDate, TimeRemainingInMins, TargetResponseInMins, MilestoneType.Name 
												FROM CaseMilestone 
												WHERE CaseId = :caseIds]) {

			//System.assertEquals(false, caseMilestone.IsViolated, 'The Milestones should not be violated');

		}


		// Set CreatedDate of test Cases to 1 day ago, so the Milestones fail
		for(Case aCase : casesToInsert) {
			Test.setCreatedDate(aCase.Id, Datetime.now().addDays(-2));
		}


		Test.startTest(); 
			Database.executeBatch(new BatchCaseForceUpdateForEntitlements());
		Test.stopTest();


		Set<Id> newOwnerIds = new Set<Id>();


		for(Case aCase : [SELECT Id, Priority, Owner.Name, OwnerId, EntitlementId FROM Case]) {
			caseIds.add(aCase.Id);
			//System.assertEquals('Actioned', aCase.Priority, 'Case Priority should be High');
			newOwnerIds.add(aCase.OwnerId);
		}

		for(Group ownerQueue : [SELECT Id, Name, DeveloperName FROM Group WHERE Id = :newOwnerIds]) {
			//System.assertEquals(HAMILTON_CST_QUEUE_NAME_ACTIONED, ownerQueue.DeveloperName, 'Should be owned by actioned Queue');
		}



        for(CaseMilestone caseMilestone : [SELECT Id, IsViolated, CompletionDate, TimeRemainingInMins, TargetResponseInMins, MilestoneType.Name 
                                           FROM CaseMilestone 
                                           WHERE CaseId = :caseIds]) {
                                               
                                               if(!caseMilestone.MilestoneType.Name.contains('Resolution')){
                                                   //System.assertEquals(false, caseMilestone.IsViolated, 'The Milestones should not be violated');
                                               }
                                               
                                           }

	}



	@isTest static void testErrorHandling() {

		Config_Settings__c loggingSettings = [SELECT Id FROM Config_Settings__c WHERE Name = 'Enable Email Logging for Batch'];
		loggingSettings.Number_Value__c = 2;
		update loggingSettings;

		User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		User cstUser;
		Contact testContact;
		List<Case> errorCases = new List<Case>();

		System.runAs(runningUser) {

			cstUser = UnitTestDataFactory.getFrontOfficeUser('CSTUSER');
			cstUser.Team__c = MilestoneUtilsTest.HAMILTON_CST_ENTITLEMENT;
			insert cstUser;

			testContact = UnitTestDataFactory.createContact();
			insert testContact;

		}

		System.runAs(cstUser) {

			for(Integer i = 0; i < NUMBER_OF_CASES_TO_CREATE; i++) {
				errorCases.add(createCase());
			}

			insert errorCases;
		}


		// Set CreatedDate of test Cases to 1 day ago, so the Milestones fail
		for(Case aCase : errorCases) {
			Test.setCreatedDate(aCase.Id, Datetime.now().addDays(-2));
		}


		Test.startTest(); 
			BatchCaseForceUpdateForEntitlements batchJob = new BatchCaseForceUpdateForEntitlements();
			batchJob.testForceError = true;
			Database.executeBatch(batchJob);
		Test.stopTest();

		Set<Id> newOwnerIds = new Set<Id>();

		for(Case aCase : [SELECT Id, Priority, Owner.Name, OwnerId, EntitlementId FROM Case]) {
			//System.assertEquals('Actioned', aCase.Priority, 'Case Priority should be still be Normal');
			newOwnerIds.add(aCase.OwnerId);
		}

		for(Group ownerQueue : [SELECT Id, Name, DeveloperName FROM Group WHERE Id = :newOwnerIds]) {
			//System.assertNotEquals(HAMILTON_CST_QUEUE_NAME_FAILED, ownerQueue.DeveloperName, 'Should not be owned by Failed Queue');
		}


	}



	/**
	* @description	Test scheduling funtionality			
	* @author		@tomcarman
	*/
	@isTest static void testScheduler() {

		Test.startTest();

			String CRON_EXP = '0 0 0 15 3 ? 2022';
			String jobId = System.schedule('Test', CRON_EXP, new BatchCaseForceUpdateForEntScheduler());

		Test.stopTest();

		//System.assertNotEquals(null, jobId);
	}


	/* Helpers */

	/**
	* @description	Helper method to create Case with Entitlement routed to a team setup in @testSetup		
	* @author		@tomcarman	
	*/
	private static Case createCase() {

		Case newCase = UnitTestDataFactory.createCase(UnitTestDataFactory.createContact().Id);
		newCase.Bypass_Standard_Assignment_Rules__c = true;
		newCase.JL_Branch_master__c = 'Oxford Street';
		newCase.Contact_Reason__c = 'Pre-Sales';
		newCase.Reason_Detail__c = 'Buying Office Enquiry';
		
		return newCase;

	}
    
    static testmethod void executeTest(){        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        System.runAs(runningUser){
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
        }        
        System.runAs(testUser){     
            contact testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'testing@gmail.com';
            insert testContact;            
            case testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            insert testCase;
            List<CaseMilestone> caseMilestones = [SELECT Id, MilestoneTypeId, IsCompleted,IsViolated, case.jl_Reopened_Count__c, case.Id ,  TargetDate, CompletionDate FROM CaseMilestone WHERE case.Id = :testCase.Id];
            Database.BatchableContext BC;
            Test.startTest();
            BatchCaseForceUpdateForEntitlements obj=new BatchCaseForceUpdateForEntitlements();
            obj.start(BC);
            obj.execute(BC,caseMilestones);
            obj.finish(BC);
            Test.stopTest();
        }
    }
	
    static testmethod void scheduleTest(){        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        System.runAs(runningUser){
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
        }        
        System.runAs(testUser){ 
            SchedulableContext sc;
            Test.startTest();
            BatchCaseForceUpdateForEntitlements obj=new BatchCaseForceUpdateForEntitlements();
            obj.execute(sc);
            Test.stopTest();
        }
    }
}