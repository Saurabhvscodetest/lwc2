//***************************************************************************
//Edit    Date        Author      Comment
//001     ??/??/??    ??          Original Class
//002     12/06/19    KM/DP       COPT-4681 Code Refactoring
//**************************************************************************
public without sharing class CaseActivityTriggerHandler {
public static void mainEntry(Boolean isBefore, 
                                 Boolean isDelete, Boolean isAfter,
                                 Boolean isInsert, Boolean isUpdate,
                                 Boolean isExecuting, List<Case_Activity__c> newList,
                                 Map<Id, SObject> newMap, List<Case_Activity__c> oldList,
                                 Map<Id, SObject> oldMap) {
                                     
                                     System.debug('@@entering CaseActivityTriggerHandler');
                                     
                                     CaseActivityTriggerHandler handler = new CaseActivityTriggerHandler();   
                                     
                                     if(isInsert && isBefore) {
                                         handleCaseActivityBeforeInsert(newList);
                                     }
                                     
                                     if(isInsert && isAfter) {
                                         handleCaseActivityAfterInsert(newList);                          
                                     }
                                     
                                     if(isUpdate && isBefore) {
                                         handleCaseActivityBeforeUpdate((Map<ID, Case_Activity__c>)newmap, (Map<ID, Case_Activity__c>)oldmap);                          
                                     }
                                     
                                     if(isUpdate && isAfter) {
                                        /* COPT-5415
                                        handleCaseActivityAfterUpdate(newList);
                                        COPT-5415 */
                                        
                                         /* COPT-5415 Start */
                                         Set<Id> relatedCaseIds = CaseActivityUtils.populateSetOfRelatedCaseIds(newList);
        
                                         Map<Id, Case> relatedCaseMap = CaseActivityUtils.fetchRelatedCasesMap ( relatedCaseIds );
                                         handleCaseActivityAfterUpdate(newList, relatedCaseMap);
                                         /* COPT-5415 End */                         
                                     }
                                     
                                 }
    
    public static void handleCaseActivityBeforeInsert(List<Case_Activity__c> caseActivityRecords) {
        
        System.debug('@@entering handleCaseActivityBeforeInsert');
        
        Set<Id> relatedCaseIds = CaseActivityUtils.populateSetOfRelatedCaseIds(caseActivityRecords);
        //<<002>>
        Map<Id, Case> relatedCaseMap = new Map<Id, Case>([SELECT Id, IsClosed, Number_of_Failed_Customer_Promises__c, Number_of_Failed_Internal_Actions__c,
                                                          Next_Customer_Promise_Due_Date_Time__c, Next_Customer_Promise_Start_Date_Time__c,
                                                          Next_Internal_Action_Due_Date_Time__c, Next_Internal_Action_Start_Date_Time__c,Origin,RecordTypeId,Reason_Detail__c,jl_Case_Type__c,
                                                          Reference_Type__c,jl_InitiatingSource__c,BusinessHoursId,jl_Last_Queue_Name__c,  
                                                          jl_Branch_master__c, Contact_Reason__c,CDH_Site__c, Branch_Department__c,Case_Owner__c,Case_Owner_Role__c,jl_First_Queue_Name__c,jl_ListView_QueueName__c,jl_Queue_Name__c,New_Last_Queue_Name_Group__c
                                                          FROM Case WHERE Id IN :relatedCaseIds]);
        //<<002>>
       
        CaseActivityUtils.setCaseActivityFieldsOnInsert(caseActivityRecords,relatedCaseMap);
        /* COPT-5414
        CaseActivityUtils.assignRecordType(caseActivityRecords);   
        CaseActivityUtils.insertCustomerPromiseType(caseActivityRecords);
        COPT-5414 */
    } 
    
    public static void handleCaseActivityAfterInsert(List<Case_Activity__c> caseActivityRecords) {
        
        System.debug('@@entering handleCaseActivityAfterInsert');
        Set<Id> relatedCaseIds = CaseActivityUtils.populateSetOfRelatedCaseIds(caseActivityRecords);
        
        Map<Id, Case> relatedCaseMap = new Map<Id, Case>([SELECT Id, IsClosed, Number_of_Failed_Customer_Promises__c, Number_of_Failed_Internal_Actions__c,
                                                          Next_Customer_Promise_Due_Date_Time__c, Next_Customer_Promise_Start_Date_Time__c, jl_Branch_master__c, Contact_Reason__c, Reason_Detail__c, CDH_Site__c, Branch_Department__c, 
                                                          Next_Internal_Action_Due_Date_Time__c, Next_Internal_Action_Start_Date_Time__c, 
                                                          Origin,RecordTypeId,jl_Case_Type__c,Reference_Type__c ,jl_InitiatingSource__c, BusinessHoursId, Assign_To_New_Branch__c  
                                                          FROM Case WHERE Id IN :relatedCaseIds]);
        Map<Id, Case> caseMap = new Map<Id, Case>();
        caseMap = CaseActivityUtils.updateRelatedCaseRecords(caseActivityRecords,relatedCaseMap);   
        caseMap = CaseActivityUtils.transferRelatedCase(caseActivityRecords,relatedCaseMap,caseMap);
        if(!caseMap.values().isEmpty()){
           try{
                //update caseMap.values();
                /* COPT-5415 */
                Database.update(caseMap.values(),false);
                /* COPT-5415 */
            }catch(Exception ex){
               System.debug('Exception from CaseActivityTriggerHandler'+ex.getMessage());
            }
        }
        CaseActivityUtils.createCaseComments(caseActivityRecords);
    }
    
    public static void handleCaseActivityBeforeUpdate(Map<ID, Case_Activity__c> newCaseActivityMap, Map<ID, Case_Activity__c> oldCaseActivityMap) {
        
        System.debug('@@entering handleCaseActivityBeforeUpdate');
        
        Set<Id> relatedCaseIds = CaseActivityUtils.populateSetOfRelatedCaseIds(newCaseActivityMap.values());
        
        Map<Id, Case> relatedCaseMap = new Map<Id, Case>([SELECT Id, owner.name, Origin, CreatedById, IsClosed, jl_First_Queue_Name__c, jl_Last_Queue__c,jl_Transfer_case__c,Contact_Reason__c, Reason_Detail__c, Branch_Department__c, jl_Last_Queue_Name__c,jl_InitiatingSource__c,jl_Case_Type__c,
                                                          Reference_Type__c, BusinessHoursId
                                                          FROM Case WHERE Id IN :relatedCaseIds]);
        if(userinfo.getName() == 'Marketing Cloud' || avoidTooManySOQL() == false){
            for (Case_Activity__c newCaseActivity : newCaseActivityMap.values()) {
            
            Case relatedCase = relatedCaseMap.get(newCaseActivity.Case__c);
            Case_Activity__c oldCaseActivity = oldCaseActivityMap.get(newCaseActivity.Id);
            
            CaseActivityUtils.setCaseActivityFieldsOnClosure(newCaseActivity, oldCaseActivity, relatedCase);
        } 
            
        }else{
            CaseActivityUtils.setCaseActivityFieldsOnClosureUpdate(newCaseActivityMap, oldCaseActivityMap, relatedCaseMap);
        }
     
    }
    
    public static void handleCaseActivityAfterUpdate(List<Case_Activity__c> caseActivityRecords, Map<Id, Case> relatedCaseMap) {
        
        System.debug('@@entering handleCaseActivityAfterUpdate');
        
        /* COPT-5415
        Set<Id> relatedCaseIds = CaseActivityUtils.populateSetOfRelatedCaseIds(caseActivityRecords);
        
        Map<Id, Case> relatedCaseMap = new Map<Id, Case>([SELECT EH_Exception_Name__c, CaseNumber, Id, IsClosed, Number_of_Failed_Customer_Promises__c, Number_of_Failed_Internal_Actions__c,
                                                          Next_Customer_Promise_Due_Date_Time__c, Next_Customer_Promise_Start_Date_Time__c,
                                                          Next_Internal_Action_Due_Date_Time__c, Next_Internal_Action_Start_Date_Time__c,Origin,RecordTypeId,Reason_Detail__c,
                                                          Reference_Type__c, jl_InitiatingSource__c, BusinessHoursId 
                                                          FROM Case WHERE Id IN :relatedCaseIds]);
        COPT-5415 */

        CaseActivityUtils.sendCaseStatusToMC(caseActivityRecords, relatedCaseMap);
        
        Map<Id, Case> caseMap = new Map<Id, Case>();
        caseMap = CaseActivityUtils.updateRelatedCaseRecords(caseActivityRecords,relatedCaseMap);
        caseMap = CaseActivityUtils.updateRelatedCaseFields(caseActivityRecords,relatedCaseMap,caseMap);
        
        try{
            if(!caseMap.values().isEmpty()){
            //update caseMap.values();
            /* COPT-5415 */
            Database.update(caseMap.values(),false);
            /* COPT-5415 */
        } 
        }catch(Exception e){
            system.debug('@@Error Message' + e.getMessage());
        }
    }
    
    
    Public static boolean avoidTooManySOQL(){
        
        
        List<PermissionSetAssignment> lstcurrentUserPerSet =    [   SELECT Id, PermissionSet.Name,AssigneeId
                                                                    FROM PermissionSetAssignment
                                                                    WHERE AssigneeId = :Userinfo.getUserId() AND PermissionSet.Name = 'Avoid_Too_Many_SOQL'];
        if(lstcurrentUserPerSet.size()>0){
            
            return true;
            
        }else{
            return false;
            
        }

    
    }

}