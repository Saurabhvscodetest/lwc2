/******************************************************************************
* @author       Shion Abdillah
* @date         28 Jun 2016
* @description  Controller class for BulkCaseTransfer page.
*
* EDIT RECORD
*
* LOG	DATE			AUTHOR	JIRA		COMMENT      
* 001	28/06/2016		SA		COPT-872	Original code
* 002	08/07/2016		MP		COPT-872	Further changes and additions
*
*******************************************************************************
*/
public class BulkCaseTransferController extends DataTablePaginator { 
	private static final String DEFAULT_SORT_FIELD = 'CaseNumber';
	@TestVisible private static final String BULK_CASE_TRANSFER_PAGE_SIZE_CONFIG_KEY = 'BULK_CASE_TRANSFER_PAGE_SIZE';
	@TestVisible private static final String BULK_CASE_TRANSFER_SELECT_RANGES_CONFIG_KEY = 'BULK_CASE_TRANSFER_SELECT_RANGES';
	private static final Integer DEFAULT_PAGE_SIZE = 50;
	private static final List<String> DEFAULT_SELECT_RANGES = new List<String> {'50', '100', '250', '500'};
	private static Set<string> qofConfig = QueueOwnerFilterHelper.getBulkTransferQueueFilter();
	private static final String DIRECTION_FROM = 'FROM';
	private static final String DIRECTION_TO = 'TO';
	private static final String ACTIONED = 'ACTIONED';
    //Commented as a part of COPT-3585
	//private static final String WARNING = 'WARNING'; 
	private static final String FAILED = 'FAILED';

	private static final Id PSE_RECORD_TYPE_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonStaticUtils.CASE_RT_NAME_PRODUCT_STOCK_ENQUIRY).getRecordTypeId();
	   
	@TestVisible private Set<Id> selectedCaseIds = new Set<Id>();
	public List<SelectOption> recordSelectOptions {get; private set;}
	public String recordSelectItem {get; set;}
    public string contextItem {get;set;}
	public List<RowItem> caseRows {get;set;}
	public List<RowItem> errorRows {get;set;}
	public Integer totalRecordCount {get; private set;}
	public String queueSourceItem {get; set;}
	public String queueDestinationItem {get; set;}
	public String transferComment {get; set;}
	public Boolean selectAllChecked {get; set;}
	private Map<Id, String> queueNameMap;

	/**
	 * No Args Constructor
	 * @params:	None
	 * @rtnval: N/A
	 */
	public BulkCaseTransferController() {
		Config_Settings__c pageSizeConfig = Config_Settings__c.getInstance(BULK_CASE_TRANSFER_PAGE_SIZE_CONFIG_KEY);
		pageSize = pageSizeConfig != null ? Integer.valueOf(pageSizeConfig.Number_Value__c) : DEFAULT_PAGE_SIZE;
		noOfRecords = 0;
		totalRecordCount = 0;
		transferComment = null;
		errorRows = new List<RowItem>();

		Config_Settings__c selectRangeConfig = Config_Settings__c.getInstance(BULK_CASE_TRANSFER_SELECT_RANGES_CONFIG_KEY);
		List<String> rangeConfigList;
		if (selectRangeConfig != null) {
			String tempRangeString = selectRangeConfig.Text_Value__c;
			rangeConfigList = tempRangeString.split(';');
		} else {
			rangeConfigList = DEFAULT_SELECT_RANGES;
		}
		
		recordSelectOptions = new List<SelectOption>();
		for (String s : rangeConfigList) {
			recordSelectOptions.add(new SelectOption(s, s));
		}

		List<QueueSObject> queueSOList = [SELECT Id, Queue.Name, Queue.Id FROM QueueSObject];
		queueNameMap = new Map<Id, String>();
		for (QueueSObject qso : queueSOList) {
			queueNameMap.put(qso.Queue.Id, qso.Queue.Name);
		}

		if(ApexPages.currentPage().getParameters().get('id') != null){
			if(ApexPages.currentPage().getParameters().get('id') != '') {
				queueSourceItem = ApexPages.currentPage().getParameters().get('id');
			}
		}
		searchCases();
	}

	/**
	 * Retrieve a page of records from the set controller.
	 * @params:	None
	 * @rtnval: List<RowItem>	List of RowItem objects for a single page
	 */
	public List<RowItem> getCases() {
		caseRows = new List<RowItem>();
		for(sObject r : setCon.getRecords()) {
			Case c = (Case)r;
			RowItem row = new RowItem(c, false);
			if (this.selectedCaseIds.contains(c.Id)) {
				row.IsSelected = true;
			} else {
				row.IsSelected = false;
			}
			caseRows.add(row);
		}
		noOfRecords = caseRows.size();
		return caseRows;
    }
                    
	/**
	 * Retrieve list of cases for selected source queue.  Blank out setCon and fullDataList so that data is reset.
	 * @params:	None
	 * @rtnval: void
	 */
	public void searchCases() {                   
		resetVariables();
		errorRows.clear();

		if (queueSourceItem != null) {
			fullDataList = [SELECT Description, OwnerId, Id, CaseNumber, Owner.Name, Contact.Name, 
								   Contact_Reason__c, Reason_Detail__c, jl_Customer_Response_By__c, ContactId,
								   LastModifiedDate, CreatedDate, Customer_Last_Name__c
							FROM Case WHERE IsClosed = false AND OwnerId = :queueSourceItem AND RecordTypeId != :PSE_RECORD_TYPE_ID ORDER BY CaseNumber DESC];
			totalRecordCount = fullDataList.size();
			if (!fullDataList.isEmpty()) {
				sortField = DEFAULT_SORT_FIELD;
			} else {
				ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.WARNING, 'No open cases found for this queue'));
			}
		}
    }
        
	/**
	 * Perform case transfer.  Return an error message if user selects same to and from queue.
	 * @params:	None
	 * @rtnval: PageReference		Page to redirect user to following update.
	 */
	public PageReference transferCases() {
		errorRows.clear();
        processSelectedCases();
		if (queueSourceItem == queueDestinationItem) {
			ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, 'Current and Destination Queues must be different'));
			return null;
		}
		
		if (selectedCaseIds.isEmpty()) {
			ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, 'No cases selected for transfer'));
			return null;
		}
		
		if (String.isBlank(transferComment)) {
			ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, 'You must enter a comment explaining why you are mass transferring these cases'));
			return null;
		}
       	
		if (queueDestinationItem != null) {
            
			PageReference pageRef = new PageReference('/apex/BulkCaseTransfer?id=' + queueDestinationItem);
			pageRef.setRedirect(true);
			Id transferUser = UserInfo.getUserId();
			DateTime transferDT = System.now();
            
			List<Case> casesToUpdate = [SELECT Id, CaseNumber, Contact.Name, OwnerId, Bulk_Transfer_Count__c FROM Case WHERE Id IN :selectedCaseIds];
			Map<Id, CaseComment> caseCommentsToAdd = new Map<Id, CaseComment>();

			// Add case comment text.
			String commentText = transferComment;
			Id sourceQueueId = queueSourceItem;
			Id destinationQueueId = queueDestinationItem;
			String sourceQueueName = queueNameMap.containsKey(sourceQueueId) ? queueNameMap.get(sourceQueueId) : queueSourceItem;
			String destinationQueueName = queueNameMap.containsKey(destinationQueueId) ? queueNameMap.get(destinationQueueId) : queueDestinationItem;
			commentText += '\n\nTransfer From: ' + sourceQueueName + '\n\nTransfer To: ' + destinationQueueName;

			System.debug('casesToUpdate: ' + casesToUpdate);
			for (Case caseRec : casesToUpdate) {
				caseRec.OwnerId = queueDestinationItem;
				caseRec.Bulk_Transfer_From__c = sourceQueueName;
				caseRec.Bulk_Transfer_To__c = destinationQueueName;
				caseRec.Bulk_Transfer_By__c = UserInfo.getUserId();
				caseRec.Bulk_Transfer_Date__c = transferDT;
				if (caseRec.Bulk_Transfer_Count__c == null) {
					caseRec.Bulk_Transfer_Count__c = 1;
				} else {
					caseRec.Bulk_Transfer_Count__c++;
				}
				CaseComment cc = new CaseComment(ParentId = caseRec.Id, CommentBody = commentText);
				caseCommentsToAdd.put(caseRec.Id, cc);
			}
            
			// Database.SaveResult[] updateResults = Database.update(casesToUpdate.values(), false);
			Database.SaveResult[] updateResults = Database.update(casesToUpdate, false);
			Integer updateCount = 0;
			Integer errorCount = 0;
			Integer pos = 0;

			for (Database.SaveResult srec : updateResults) {
				if (srec.isSuccess()) {
					updateCount++;					
				} else {
					errorCount++;
					Case errorCase = casesToUpdate.get(pos);
					for(Database.Error err : srec.getErrors()) {
						if (!err.getMessage().contains('INVALID_CROSS_REFERENCE_KEY')) {
							errorRows.add(new RowItem(errorCase, err.getMessage()));
						}					
					}
					// caseCommentsToAdd.remove(errorCase.Id);
				}
				pos++;
			}
			if (errorCount == 0 && !caseCommentsToAdd.isEmpty()) {
				insert caseCommentsToAdd.values();
			}
			if (!errorRows.isEmpty()) {
				ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, 'Unable perform case transfer due to errors - please correct and retry'));
				resetVariables();
				return null;
			} else {
			system.debug(updateCount + ' of ' + casesToUpdate.size() + ' updated');
			return pageRef;
			}
		} else {
			ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, 'Invalid destination specified - please check and retry'));
			return null;
		}       
	}

	/**
	 * Add the current item to the selectedCaseIds set.
	 * @params:	None
	 * @rtnval: void
	 */
	public void doSelectItem() {
		this.selectedCaseIds.add(this.contextItem);
		if (selectedCaseIds.size() == fullDataList.size()) {
			selectAllChecked = true;
		}
	}

	/**
	 * Remove the current item to the selectedCaseIds set.
	 * @params:	None
	 * @rtnval: void
	 */
	public void doDeselectItem() {
		selectAllChecked = false;
		this.selectedCaseIds.remove(this.contextItem);
	}

	/**
	 * Add all cases from fullDataList to the selectedCaseIds set.
	 * @params:	None
	 * @rtnval: void
	 */
	public void doSelectAllItems() {
		for (sObject so : fullDataList) {
			selectedCaseIds.add(so.Id);                    
		}
	}

	/**
	 * Add N cases from fullDataList to the selectedCaseIds set.
	 * @params:	None
	 * @rtnval: void
	 */
	public void doSelectNItems() {
        this.selectedCaseIds.clear();		        
		Integer noToSelect = Integer.valueOf(recordSelectItem);
		if (fullDataList.size() <= noToSelect) {
			noToSelect = fullDataList.size();
			selectAllChecked = true;
		} else {
			selectAllChecked = false;
		}
		for (Integer i = 0; i < noToSelect; i++) {
			selectedCaseIds.add(fullDataList.get(i).Id);
		}
	}

	/**
	 * Remove all cases from the selectedCaseIds set.
	 * @params:	None
	 * @rtnval: void
	 */
	public void doDeSelectAllItems() {
        this.selectedCaseIds.clear();		        
    }


    public void processSelectedCases() {
        //selectedCaseIds= new Set<Id>();
        for (RowItem eachItem: caseRows) {
            if (eachItem.isSelected) {
                selectedCaseIds.add(eachItem.caseRec.Id); 
            } else if(!eachItem.isSelected && selectedCaseIds.contains(eachItem.caseRec.Id)){
                selectedCaseIds.remove(eachItem.caseRec.Id);
            }
        }
    }
	/**
	 * Return an integer value of the number of pages needed to display all selected records
	 * based on page size.
	 * @params:	None
	 * @rtnval: Integer		Total number of pages
	 */
	public Integer getTotalPages(){
		Decimal totalSize = setCon.getResultSize();
		Decimal pageSize = setCon.getPageSize();
		Decimal pages = totalSize/pageSize;
		return (Integer)pages.round(System.RoundingMode.CEILING);
	}

	/**
	 * Inner class for displaying case rows
	 */
	public class RowItem {
		public Case caseRec {get; private set;}
		public Boolean isSelected {get;set;}
		public String errorMessage {get; private set;}
		public String shortDescription {get; private set;}
		
		public RowItem (Case c, Boolean s) {
			this.caseRec = c;
			this.isSelected = s;
			if (c.Description != null) {
				this.shortDescription = c.Description.left(100);
			}
		}
		
		public RowItem (Case c, String errorMsg) {
			this.caseRec = c;
			this.isSelected = false;
			this.errorMessage = errorMsg;
		}
	}


	public List<SelectOption> getfromQueueItems() {
		return getAllQueueItems(DIRECTION_FROM);
	}

	public List<SelectOption> getToQueueItems() {
		return getAllQueueItems(DIRECTION_TO);
	}

	/**
	 * Get list of queue names valid for transfers
	 * @params:	None
	 * @rtnval: List<SelectOption>		List of Queue Name select options.
	 */
	public List<SelectOption> getAllQueueItems(String direction) {


		Set<String> queuesToExclude = new Set<String>(qofConfig);

		if(direction == DIRECTION_FROM) {
			// strip FAILED, WARNING, ACTION from the exclusion list, as we always want these to show. This is a temporary fix - should be replaced with a more comprehensive review of the exclusion table
			for(String queueName : queuesToExclude) {
                //Commented and modified as a part of COPT-3585
				//if(queueName.containsIgnoreCase(ACTIONED) || queueName.containsIgnoreCase(WARNING) || queueName.containsIgnoreCase(FAILED)) {
                if(queueName.containsIgnoreCase(ACTIONED) || queueName.containsIgnoreCase(FAILED)) {
					queuesToExclude.remove(queueName);
				}
			}
		}


        List<SelectOption> options = new List<SelectOption>();
        system.debug('getQueueItems - queuesToExclude : ' + queuesToExclude );
        for (QueueSobject queueSO : [SELECT Id, Queue.Name, Queue.Id FROM QueueSobject ORDER BY Queue.Name]) { 

            if(queueSO.Queue.Name.length() > CommonStaticUtils.MAX_SEARCH_TEXT_LENGTH){
				boolean queueNameFound = false;
            	for(String qo : queuesToExclude) {
            		if(queueSO.Queue.Name.startswith(qo)) {
            			queueNameFound = true;
	                	break;
            		}
            	}
            	if(!queueNameFound){
            		system.debug('queueNameFound : ' + queueNameFound +  ' -Queue Name - ' + queueSO.Queue.Name);
        			options.add(new SelectOption(queueSO.Queue.Id, queueSO.Queue.Name));
        		}
        	} else if (!queuesToExclude.contains(queueSO.Queue.Name)) {
                options.add(new SelectOption(queueSO.Queue.Id, queueSO.Queue.Name));
            }
        }              
        return options;
    }

	/**
	 * Reset lists and variables
	 * @params:	None
	 * @rtnval: void
	 */
	private void resetVariables() {
		setCon = null;
		selectedCaseIds.clear();
		fullDataList = new List<Case>();
		totalRecordCount = 0;
		selectAllChecked = false;
		sortField = null;
		previousSortField = null;
	}
    
    
    public void first1() {
        processSelectedCases();
        setCon.first();
    }
    
    public void last1() {
        processSelectedCases();
        setCon.last();
    }
    
    public void previous1() {
        processSelectedCases();
        setCon.previous();
    }
    
    public void next1() {
        processSelectedCases();
        setCon.next();
    }
}