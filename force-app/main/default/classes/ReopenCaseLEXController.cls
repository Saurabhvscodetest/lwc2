public class ReopenCaseLEXController {

    /**
    * @description  Reopens the case depending on user profile      
    */
    @AuraEnabled
    public static void updateCase(String caseId, String comment, Boolean customActivityCheckbox){
        Case caseToReopen = CaseReopenUtilities.reopenCase(caseId);
        caseToReopen.jl_Case_RestrictedAddComment__c = comment;
        caseToReopen.Create_Case_Activity__c = customActivityCheckbox;

        try{
            update caseToReopen;
        }catch(Exception ex){
            System.debug('An error has occured when reopening this case ' +ex.getMessage() );
        }

    }
    
    /**Queue Capacity
     * @description To get the Queue name of the user who reopens the case based on Tier
     */
   @AuraEnabled
    public static String getCaseQueue(Id caseId){
        String lastQueName = '';
        String lastQueueName = 'Queue not found';
       	Case currentCase = CaseReopenUtilities.reopenCase(caseId);
       	lastQueueName = currentCase.jl_Last_Queue_Name__c;
        if(lastQueueName != NULL){
            
            if(lastQueueName.contains('ACTIONED')){
            lastQueName = lastQueueName.removeEnd('_ACTIONED');
        }
       else if(lastQueueName.contains('FAILED')){
            lastQueName = lastQueueName.removeEnd('_FAILED');
        }
        else{
            lastQueName = lastQueueName;
        }
        
        system.debug('lastQueueName'+lastQueName);
        return lastQueName;
        }else{
            
            return lastQueueName;
        }
        
        
    }
   

}