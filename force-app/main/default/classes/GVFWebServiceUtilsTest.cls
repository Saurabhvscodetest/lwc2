/*
* @File Name   : GVFWebServiceUtils
* @Description : Webservice Utility class created for invoking all the integration related services
* @Jira #      : #4277
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0       26-May-19         Vignesh kotha                Created
*/

@istest
public class GVFWebServiceUtilsTest {
    @isTest static void buildingxmlrequest() {
        String order_id = '12345678';
        string delivery_no = '12345678';
        String xmldata = GVFWebServiceUtils.buildXMLImageRequest(order_id);
        string delnumber = GVFWebServiceUtils.buildXMLImageRequestDelivery(delivery_no);
        //System.debug(xmldataval + ' test class');
        String xmldataval = '<?xml version="1.0"?><Command><Execute><Status>' +
            '<DocResponse>' +
            '<TotalDocumentsRead>1</TotalDocumentsRead>'+
            '<DocumentsAccepted>1</DocumentsAccepted>'+
            '<DocumentsRejected>0</DocumentsRejected>'+
            '<ListenerName>DocRequestTaskListener</ListenerName>'+
            '<ExtDocControlID>1</ExtDocControlID>'+
            '<ResultTime>19-06-18 07:06:50 GDT</ResultTime>'+
            '<CommandNo>CMD6352CF7EA12F47AEA8D71AB192D9540C</CommandNo>'+
            '<AuthServer>MCJS-CDSSQL2T\FWTest:LNOSFW_JL</AuthServer>'+
            '<DataServer>MCJS-CDSSQL2T\FWTest:DCF</DataServer>'+
            '<ElapsedTime>94</ElapsedTime>'+
            '<Message></Message>'+
            '<STADMessage>'+
            '<DocMasterBOL>'+
            '<DocBOL><DocStop><DocRouteHist>'+
            '<DocFieldDataHist></DocFieldDataHist>'+
            '</DocRouteHist><DocOrderLine></DocOrderLine></DocStop></DocBOL>'+
            '</DocMasterBOL>'+            
            '</STADMessage>'+
            '<Result>SUCCESS</Result>'+
            '<EchoData></EchoData>' +
            '</DocResponse>'+
            '</Status></Execute></Command>';
        List<string> response = GVFWebServiceUtils.parsePictureValuesFromResponse(xmldataval);
    }
    
}