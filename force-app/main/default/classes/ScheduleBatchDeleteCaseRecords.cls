/* Description  : Schedule class for Delete case Records As per the cirteria given in COPT-4293
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   20/02/2019        Vignesh Kotha                   Created               COPT-4293
*
*/
global class ScheduleBatchDeleteCaseRecords implements Schedulable{
   global void execute(SchedulableContext sc){
        BAT_DeleteCaseRecords obj = new BAT_DeleteCaseRecords();
        Database.executebatch(obj);
    }   
}