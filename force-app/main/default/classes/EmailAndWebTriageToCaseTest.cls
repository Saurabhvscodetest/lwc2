@isTest
private class EmailAndWebTriageToCaseTest {

   @testSetup static void initialiseData() {
		
		UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
		Config_Settings__c orederingCustomerRecordType = new Config_Settings__c(Name = 'ORDERING_CUSTOMER_RECORDTYPE', Text_Value__c = 'Ordering_Customer');
        insert orederingCustomerRecordType;
	}
	
   @isTest static void populateContactIdOnCaseWithOrderingCustomerWeb() {        
    
      Contact customer = MyJL_TestUtil.createCustomer('Ordering_Customer');  
      /* COPT-5272
      Case testCase = MyJL_TestUtil.createCase('web');
	  testCase = [SELECT id,contactid FROM case where id =:testCase.id];
      System.assertNotEquals(testCase.contactId,null);
	  COPT-5272 */
    }
    
    @isTest static void populateContactIdOnCaseWithOnlyOrderingCustomerWeb() {
      
      Contact orderingCustomer = MyJL_TestUtil.createCustomer('Ordering_Customer');  
      Contact nonOrderingCustomer = MyJL_TestUtil.createCustomer('Normal');  
      /* COPT-5272
	  Case testCase = MyJL_TestUtil.createCase('web');
      testCase = [SELECT id,contactid FROM case where id =:testCase.id];
      System.assertEquals(testCase.contactId,orderingCustomer.Id);
      COPT-5272 */
    }
    
    @isTest static void doNotPopulateContactIdwhenTwoOrderingCustomersWeb() {
    	   
    	Contact orderingCustomer1 = MyJL_TestUtil.createCustomer('Ordering_Customer');  
      	Contact orderingCustomer2 = MyJL_TestUtil.createCustomer('Ordering_Customer');  
      	/* COPT-5272
        Case testCase = MyJL_TestUtil.createCase('web');
     		testCase = [SELECT id,contactid FROM case where id =:testCase.id];
      		System.assertEquals(testCase.contactId,null);
		COPT-5272 */
    }
    
    @isTest static void doNotPopulateContactIdwhenOrderingAndNonCustomersWeb() {
    	   
    	Contact orderingCustomer1 = MyJL_TestUtil.createCustomer('Ordering_Customer');  
      	Contact orderingCustomer2 = MyJL_TestUtil.createCustomer('Ordering_Customer');  
      	Contact nonOrderingCustomer = MyJL_TestUtil.createCustomer('Normal');
      	/* COPT-5272
        Case testCase = MyJL_TestUtil.createCase('web');
     		testCase = [SELECT id,contactid FROM case where id =:testCase.id];
      		System.assertEquals(testCase.contactId,null);
		COPT-5272 */
    }	
    
   @isTest static void populateContactIdonCaseWithOrderingCustomerEmail() {        
    
      Contact customer = MyJL_TestUtil.createCustomer('Ordering_Customer');  
      /* COPT-5272
      Case testCase = MyJL_TestUtil.createCase('email');
	  testCase = [SELECT id,contactid FROM case where id =:testCase.id];
      System.assertNotEquals(testCase.contactId,null);
	  COPT-5272 */
    }
    
    @isTest static void populateContactIdOnCaseWithOnlyOrderingCustomerEmail() {
      
      Contact orderingCustomer = MyJL_TestUtil.createCustomer('Ordering_Customer');  
      Contact nonOrderingCustomer = MyJL_TestUtil.createCustomer('Normal');  
      /* COPT-5272
      Case testCase = MyJL_TestUtil.createCase('email');
      testCase = [SELECT id,contactid FROM case where id =:testCase.id];
      System.assertEquals(testCase.contactId,orderingCustomer.Id);
      COPT-5272 */
    }
    
    @isTest static void doNotPopulateContactIdwhenTwoOrderingCustomersEmail() {
    	   
    	Contact orderingCustomer1 = MyJL_TestUtil.createCustomer('Ordering_Customer');  
      	Contact orderingCustomer2 = MyJL_TestUtil.createCustomer('Ordering_Customer');  
      	/* COPT-5272
        Case testCase = MyJL_TestUtil.createCase('email');
     		testCase = [SELECT id,contactid FROM case where id =:testCase.id];
      		System.assertEquals(testCase.contactId,null);
		COPT-5272 */
    }
    
    @isTest static void doNotPopulateContactIdwhenOrderingAndNonCustomersEmail() {
    	   
    	Contact orderingCustomer1 = MyJL_TestUtil.createCustomer('Ordering_Customer');  
      	Contact orderingCustomer2 = MyJL_TestUtil.createCustomer('Ordering_Customer');  
      	Contact nonOrderingCustomer = MyJL_TestUtil.createCustomer('Normal');
      	/* COPT-5272
        Case testCase = MyJL_TestUtil.createCase('email');
     		testCase = [SELECT id,contactid FROM case where id =:testCase.id];
      		System.assertEquals(testCase.contactId,null);
		COPT-5272 */
    }   
}