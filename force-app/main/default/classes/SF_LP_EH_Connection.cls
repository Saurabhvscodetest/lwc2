/*
* @File Name   : SF_LP_EH_Connection 
* @Description : Rest Resource to connect LP to EH to fetch and display delivery information 
* @Copyright   : JohnLewis
* @Trello #    : #DC331
----------------------------------------------------------------------------------------------------------------
	Ver           Date               Author                     Modification
	---           ----               ------                     ------------
*   3.0         12-JAN-21            Vijay A                       Created
*/

@RestResource(urlMapping='/SF_ConnexToLPEH/*')
Global class SF_LP_EH_Connection {
   Public Static Boolean nameCheck = false;
   Public Static Boolean postCodeCheck = false;
   Public Static Boolean addressCheck = false;
   
    @HttpGet
    global static SF_ConnexToLivePersonWrapper fetchData() {
        
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        String orderNumber;
        String orderResponse;
            
        SF_ConnexToLivePersonWrapper livePersonResponse = NEW SF_ConnexToLivePersonWrapper();
        
  try {
        orderNumber = req.params.get('orderNumber');   
      
        if( String.isBlank(orderNumber) ){        
            res.statusCode = 500;
            livePersonResponse.statusCode = String.valueOf(res.statusCode);
 			livePersonResponse.ExceptionStatus = 'MISSING_PARAMS';
            return livePersonResponse;
            }
      
            else{    
            orderResponse = DelTrackChatBotUtils.chatBotFetchDeliveryDetails('',orderNumber);
            DelTrackChatBotUtils.WrapperResponse returnWR = new DelTrackChatBotUtils.WrapperResponse(); 
            returnWR = (DelTrackChatBotUtils.WrapperResponse)Json.deserialize(orderResponse, DelTrackChatBotUtils.WrapperResponse.class);
             
            system.debug('@@@ orderResponse ' + orderResponse);
            
            res.statusCode = 200;
            
              livePersonResponse.statusCode = String.valueOf(res.statusCode);
                   
                if(returnWR.errorDescription != NULL){
                livePersonResponse.ExceptionStatus = returnWR.errorDescription;
                } 
                else{    
                    livePersonResponse.ExceptionStatus = 'Success';
                }
                
        if(livePersonResponse.ExceptionStatus == 'Success'){
        
        if(returnWR.ccData == true && returnWR.gvfData == true){
            livePersonResponse.CombinedOrder = 'True';
        }
        else if(returnWR.ccData == true && returnWR.cdData == true){
            livePersonResponse.CombinedOrder = 'True';
        }
        else if(returnWR.ccData == true && returnWR.sdData == true){
            livePersonResponse.CombinedOrder = 'True';
        }
        else if(returnWR.sdData == true && returnWR.gvfData == true){
            livePersonResponse.CombinedOrder = 'True';
        }
        else if(returnWR.cdData == true && returnWR.gvfData == true){
            livePersonResponse.CombinedOrder = 'True';
        }
        else if(returnWR.cdData == true && returnWR.sdData == true){
            livePersonResponse.CombinedOrder = 'True';
        }
        else if(returnWR.cdData == true){                
             livePersonResponse.CombinedOrder = 'False';
             livePersonResponse.DeliveryType = 'CarrierDelivery';
                    }   
        else if(returnWR.sdData == true){
             livePersonResponse.CombinedOrder = 'False';
             livePersonResponse.DeliveryType = 'SupplierDirect';
                    }  
        else {
            livePersonResponse.CombinedOrder = 'False';
        }
            if(livePersonResponse.CombinedOrder == 'False'){  
                
            if(returnWR.ccData == true){
            livePersonResponse.DeliveryType = 'ClickAndCollect';
            livePersonResponse.DeliveryDate = returnWR.CCavailableForCollDateAlone;
            if ( returnWR.relatedCaseList != null && returnWR.relatedCaseList.size () > 0 ) {
                String caseList = String.valueOf(returnWR.relatedCaseList.size ());
                livePersonResponse.CaseNumber = caseList;
                livePersonResponse.ActualCaseNumber = returnWR.relatedCaseList[0].CaseNumber;
            }else{
                livePersonResponse.CaseNumber = 'No Data';
            }
            livePersonResponse.CurrentDateCheck = returnWR.currentDateCheck;
            livePersonResponse.CurrentDateTimeCheck = returnWR.currentDateTimeCheck;
            livePersonResponse.CustomerName = returnWR.CCcustomerName;
            livePersonResponse.CCAfterTimeCheck = returnWR.CCAfterTimeCheck;
            livePersonResponse.CCmodeTransport = returnWR.CCmodeTransport;
            livePersonResponse.ParcelListSize =     returnWR.CCdelTrack.size();
            livePersonResponse.CCExpectedLocation =     returnWR.CCexpectedLocation;
                
             //check the latest status of the order
			livePersonResponse.CCdelTrack =  returnWR.CCdelTrack;
                
                for(DeliveryTrackingVO.ParcelInformation cc : livePersonResponse.CCdelTrack){
                    
                  if(cc.productStatus == 'Ready For Collection' || cc.productStatus == 'Parcel In Store' ){
                      
                     livePersonResponse.CCAllParcelsReady = 'Ready';
                      
                } else if(cc.productStatus == 'In transit' || cc.productStatus == 'Dispatched' || cc.productStatus == 'Out for delivery to collection point' || cc.productStatus == 'Delivered to collection point' ){
                         
                     livePersonResponse.CCAllParcelsReady = 'Transit';
                      
                } else if(cc.productStatus == 'Scanned For Return' || cc.productStatus == 'Parcel scanned at Returns Centre' || cc.productStatus == 'Returned parcel scanned into store' || cc.productStatus == 'Scanned For Return' || cc.productStatus == 'Cage scanned at Returns Centre'){
                      
                     livePersonResponse.CCAllParcelsReady = 'Return';
                         
                  }
                    
                    else{
                      break;
                  }
                    
                }
                
                
                System.debug('@@@ returnWR.CCdelTrack ' +  returnWR.CCdelTrack);
                
                if( livePersonResponse.ParcelListSize  == 1){
                    livePersonResponse.ParcelLatestStatus = returnWR.CCdelTrack[0].productStatus;
                }
            }
             else if(returnWR.gvfData == true){
                 
            System.debug(' @@@ returnWR.gvfData ' + returnWR.gvfData);
            
            livePersonResponse.DeliveryType = 'TwoManGVF';
            livePersonResponse.GVFDeliveryStatus = returnWR.deliveryStatus;
            livePersonResponse.DeliveryDate = returnWR.deliverySlotToFormatted ;
             
            if ( returnWR.relatedCaseList != null && returnWR.relatedCaseList.size () > 0 ) {
                
                String caseList = String.valueOf(returnWR.relatedCaseList.size ());
                livePersonResponse.CaseNumber = caseList;
                livePersonResponse.ActualCaseNumber = returnWR.relatedCaseList[0].CaseNumber;
                 
                System.debug('@@@ livePersonResponse.CaseNumber' + livePersonResponse.CaseNumber);
                System.debug('@@@ livePersonResponse.ActualCaseNumber' + livePersonResponse.ActualCaseNumber);
            }
             else{
             
                 livePersonResponse.CaseNumber = 'No Data';
            }
                 
            livePersonResponse.DeliverySlotFrom = returnWR.deliverySlotFromFormattedTime;
            livePersonResponse.DeliverySlotTo = returnWR.deliverySlotToFormattedTime;
            livePersonResponse.PostCode = returnWR.postCode;       
            livePersonResponse.CustomerName = returnWR.recipientName;
            livePersonResponse.CurrentDateCheck = returnWR.currentDateCheck;
            livePersonResponse.CurrentDateTimeCheck = returnWR.currentDateTimeCheck;
            
            livePersonResponse.GVFtwoHourSlot = returnWR.twoHourSlot;
            livePersonResponse.GVFtwoHourDPDate = returnWR.twoHourDPDate;
            livePersonResponse.GVFtwoHourDPTime = returnWR.twoHourDPTime;   
            livePersonResponse.GVFtwoHourARTime = returnWR.twoHourARTime;  
            livePersonResponse.GVFDeliveryStatus = returnWR.deliveryStatus;
            livePersonResponse.GVFEightPMCheck = returnWR.twoHour8PMTimeCheck;
            livePersonResponse.GVFtwoHourARTimeCompare = returnWR.twoHourARTimeCompare;
  
                        }        
                    }
                }
            
       else if(livePersonResponse.ExceptionStatus == 'NO_DATA_FOUND'){
            
            res.statusCode = 400;
            livePersonResponse.statusCode = String.valueOf(res.statusCode);
            livePersonResponse.ExceptionStatus = 'NO_DATA_FOUND';
                }
                
            System.debug('@@@ livePersonResponse' + livePersonResponse);
            return livePersonResponse;
            }
        }
        
        catch(Exception e) {    
            
            System.debug('@@@ Error Message ' + e.getMessage());
            System.debug('@@@ Error Line Number' + e.getLineNumber());
                
            res.statusCode = 404;
            livePersonResponse.statusCode = String.valueOf(res.statusCode);
            livePersonResponse.ExceptionStatus = 'NO_DATA_FOUND';
            
            return livePersonResponse;
        }
    }    
    
    global class SF_ConnexToLivePersonWrapper { 
        
       
        global String statusCode{get;set;}
        global String ExceptionStatus{get;set;}
        global String CombinedOrder = 'No Data';
        global String DeliveryType = 'No Data';
        global String DeliveryDate = 'No Data';
        global String CaseNumber = 'No Data';
        global String ActualCaseNumber = 'No Data';
        // CC Parameters
        global String CurrentDateCheck = 'No Data';
        global String CurrentDateTimeCheck = 'No Data';
        global String CustomerName = 'No Data';
        global String CCAfterTimeCheck = 'No Data';
        global String CCmodeTransport = 'No Data';
		global Integer ParcelListSize = 0;
        global List<DeliveryTrackingVO.ParcelInformation> CCdelTrack {set;get;}
        global String ParcelLatestStatus = 'No Data';
        global String CCExpectedLocation = 'No Data';
         global String CCAllParcelsReady = 'No Data';
        // GVF Parameters
		global String DeliverySlotFrom = 'No Data';
        global String DeliverySlotTo = 'No Data';
        global String PostCode = 'No Data';
        global String GVFtwoHourSlot = 'No Data';
        global String GVFtwoHourDPDate = 'No Data';
        global String GVFtwoHourDPTime = 'No Data';
        global String GVFtwoHourARTime = 'No Data';
        global String GVFDeliveryStatus = 'No Data';
        global String GVFEightPMCheck = 'No Data';
        global String GVFtwoHourARTimeCompare = 'No Data';

    }
}