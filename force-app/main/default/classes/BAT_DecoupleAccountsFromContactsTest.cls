/* Description  : GDPR - Decouple Accounts from Contacts
*
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   28/03/2019      Vijay Ambati                        Created             COPT-4432
*
*/
@isTest
Public class BAT_DecoupleAccountsFromContactsTest {
    
    Public Static Id RecordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Superseded').getRecordTypeId();  
    
    @isTest
    Public static Void testInsertAccountContact(){
        
        List<Contact> ccInsert = new List<Contact>();
        
        for(integer i=0;i<300;i++){
            
            ccInsert.add(new Contact(FirstName = 'Test', LastName = 'Customer'+i,RecordTypeId = RecordTypeIdContact,Skip_Batch_Validation__c = True,Email = 'test@abc.com'));
        }
	   GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
       gDPR.GDPR_Class_Name__c = 'BAT_DecoupleAccountsFromContacts';
       gDPR.Name = 'BAT_DecoupleAccountsFromContacts';
       gDPR.Record_Limit__c = '5000';
        
       GDPR_Record_Limit__c gDPR1 = new GDPR_Record_Limit__c();
       gDPR1.GDPR_Class_Name__c = 'BAT_DecoupleAccountsFromCases';
       gDPR1.Name = 'BAT_DecoupleAccountsFromCases';
       gDPR1.Record_Limit__c = '5000';
        try{
            insert ccInsert;
            insert gDPR;
            insert gDPR1;
            system.assertEquals(300, ccInsert.size());
        }
        Catch(exception e){
            system.debug('Exception Caught:'+e.getmessage());
        }
		Database.BatchableContext BC;
		BAT_DecoupleAccountsFromContacts obj = new BAT_DecoupleAccountsFromContacts();
        Test.startTest();       
        obj.start(BC);
        obj.execute(BC,ccInsert); 
        Database.DeleteResult[] Delete_Result = Database.delete(ccInsert, false);
        obj.finish(BC);
        Test.stopTest();  
        
    }
@isTest
    Public static void testExceptionMethod(){
        
         List<Contact> ccInsert = new List<Contact>();
        
        for(integer i=0;i<300;i++){
            
            ccInsert.add(new Contact(FirstName = 'Test', LastName = 'Customer'+i,RecordTypeId = RecordTypeIdContact,Email = 'test@abc.com'));
        }
	   GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
       gDPR.GDPR_Class_Name__c = 'BAT_DecoupleAccountsFromContacts';
       gDPR.Name = 'BAT_DecoupleAccountsFromContacts';
       gDPR.Record_Limit__c = '';
	   GDPR_Record_Limit__c gDPR1 = new GDPR_Record_Limit__c();
       gDPR1.GDPR_Class_Name__c = 'BAT_DecoupleAccountsFromCases';
       gDPR1.Name = 'BAT_DecoupleAccountsFromCases';
       gDPR1.Record_Limit__c = '';

        try{
            insert ccInsert;
            insert gDPR;
            insert gDPR1;
            system.assertEquals(300, ccInsert.size());
        }
        Catch(exception e){
            system.debug('Exception Caught:'+e.getmessage());
        }

        Database.BatchableContext BC;
		BAT_DecoupleAccountsFromContacts obj = new BAT_DecoupleAccountsFromContacts();
        Test.startTest();       
        obj.start(BC);
        Database.DeleteResult[] Delete_Result = Database.delete(ccInsert, false);
        obj.execute(BC,ccInsert); 
        obj.finish(BC);
        Test.stopTest();  
        
    }

  @isTest
    Public static void testSchedule(){
       Test.startTest(); 
       GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
       gDPR.GDPR_Class_Name__c = 'BAT_DecoupleAccountsFromContacts';
       gDPR.Name = 'BAT_DecoupleAccountsFromContacts';
       gDPR.Record_Limit__c = '5000';
       insert gDPR;

	   GDPR_Record_Limit__c gDPR1 = new GDPR_Record_Limit__c();
       gDPR1.GDPR_Class_Name__c = 'BAT_DecoupleAccountsFromCases';
       gDPR1.Name = 'BAT_DecoupleAccountsFromCases';
       gDPR1.Record_Limit__c = '5000';
       insert gDPR1;
       ScheduleBatchDecoupleAccounts obj = NEW ScheduleBatchDecoupleAccounts();
       obj.execute(null);  
       Test.stopTest();
    }
    
}