/**
 *	@author: ???
 *	@date: ???
 *	@description:
 *	    looks like this class was meant as a collection of some utility methods
 *	
 *	Version History :   
 *	2014-12-10 - MJL-1383 - AG
 *	added new version of getCreateableFields(describeObj, convertTolowerCase)
 *	to avoid wasting cycles when result of the original
 *	getCreateableFields(describeObj) is then had to be converted to lower case
 *	for reliable matching
 *		
 */
public with sharing class JLHelperMethods 
{
	// TODO - do we want to keep these here - or reference them another way? e.g. Custom Settings
	// Or extent to hold more constants in Custom Settings
	
	public static final String PRODUCTION_ORG_ID = '00Db0000000d8Mo';
	
    // public static final String USER_PROFILE_ID_SYSTEM_ADMIN = '00eb0000000pGns';
    
    public static Boolean isProductionOrg()
    {
        return JLHelperMethods.idsAreEqual(Userinfo.getOrganizationId(),PRODUCTION_ORG_ID);
    }
    
    public static Boolean isSandboxOrg()
    {
        return !(JLHelperMethods.idsAreEqual(Userinfo.getOrganizationId(),PRODUCTION_ORG_ID));
    }
    
    public static Boolean idsAreEqual(String id1, String id2)
    {
    	Boolean isEqual = false;
    	
    	if (id1 != null) {
    		if (id2 != null) {
                if(id1.subString(0, 15).equals(id2.subString(0, 15)))
                {
                    return true;
                }    			
    		}
    	} else {
            if (id2 == null) {
                isEqual = true;
            } 		
    	}
        
        return isEqual;
    }
    
    public static Boolean valueIsNull(Object val)
    {
        if(val == null || String.valueOf(val).length() == 0)
        {
            return true;
        }
        
        return false;
    }
    
    public static Boolean isGroupId(String id)
    {
    	return id.startsWith('00G');
    }
    
    public static Boolean isCaseId(String id)
    {
    	return (id != null) ?  id.startsWith('500') : false;
    }
    
    // We may have multiple admi type profiles - so going to use profile 'Modify All Data' perm to check
    public static Boolean isSystemAdminUser()
    {
    	// UserInfo.getProfileId()
    	return [Select id, Name, PermissionsModifyAllData from Profile where Id = :UserInfo.getProfileId()].PermissionsModifyAllData; 
    }    
    
    public static List<SelectOption> getPickListValues(Sobject describeObj, String picklistName)
    {
    	Schema.sObjectType sobjectType = describeObj.getSObjectType(); //get the sobject Type
    	Schema.DescribeSObjectResult sobjectDescribe = sobjectType.getDescribe(); //describe the Object
      	Map<String, Schema.SObjectField> fieldMap = sobjectDescribe.fields.getMap(); //get a map of fields for the Object
      	List<Schema.PicklistEntry> picklistValues = fieldMap.get(picklistName).getDescribe().getPickListValues();
		
		List<SelectOption> selectOptions = new List<SelectOption>();
			
		for (Schema.PicklistEntry ple: picklistValues)
		{
			selectOptions.add(new SelectOption(ple.getValue(), ple.getLabel()));
		}
		return selectOptions;
    }
    
    public static Set<String> getAllFields(Sobject describeObj)
    {
        Set<String> cFields = new Set<String>();
        
        Schema.sObjectType sobjectType = describeObj.getSObjectType(); //get the sobject Type
        Schema.DescribeSObjectResult sobjectDescribe = sobjectType.getDescribe(); //describe the Object
        Map<String, Schema.SObjectField> fieldMap = sobjectDescribe.fields.getMap(); //get a map of fields for the Object
        
        for (String fName: fieldMap.keySet())
        {
            Schema.SObjectField sField = fieldMap.get(fName);
            cFields.add(fName);
        }
        
        return cFields; 
    }
    
    
	/**
	 * @param: convertTolowerCase - set to true if result set needs to be all-lower-case
	 */
    public static Set<String> getCreateableFields(final SObject describeObj, final Boolean convertTolowerCase) {
    	final Set<String> cFields = new Set<String>();
        
        final Schema.sObjectType sobjectType = describeObj.getSObjectType(); //get the sobject Type
        final Schema.DescribeSObjectResult sobjectDescribe = sobjectType.getDescribe(); //describe the Object
        final Map<String, Schema.SObjectField> fieldMap = sobjectDescribe.fields.getMap(); //get a map of fields for the Object
        
        for (String fName: fieldMap.keySet()) {
        	Schema.SObjectField sField = fieldMap.get(fName);
        	
        	if (sField.getDescribe().Createable) {
        		cFields.add(convertTolowerCase? fName.toLowerCase() : fName);
        	}
        }
        
        return cFields;	
    }

    public static Set<String> getCreateableFields(Sobject describeObj) {
        return getCreateableFields(describeObj, false);	
    }
    
}