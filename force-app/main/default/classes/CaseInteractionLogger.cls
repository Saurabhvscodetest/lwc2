public with sharing class CaseInteractionLogger {

	public static final String START_TIME = 'END';
	public static final String END_TIME = 'START';
	private static final String EVENT_TYPE = 'Handle';
	@TestVisible private static final String NON_OMNI_PERMISSION_SET = 'Non_Omni_User';


	public static void logInteraction(Datetime inputDatetime, String timeType, Id caseId, Id userId) {

		if(!isUserEligible(userId)) {
			return;
		}


		Id existingOpenInteractionRecord = checkForExistingOpenInteractionRecord(userId, caseId);

		Interaction__c interactionRecord;

		if(timeType == START_TIME && existingOpenInteractionRecord == null) {

			User theUser = getUserInformation(userId);
			Case theCase = getCaseInformation(caseId);

			// Standard details
			interactionRecord = new Interaction__c();
			interactionRecord.Name = theCase.CaseNumber +': '+EVENT_TYPE;
			interactionRecord.Event_Type__c = EVENT_TYPE;
			interactionRecord.Case__c = caseId;
			interactionRecord.Agent__c = userId;
			interactionRecord.Start_DT__c = inputDatetime;

			// User details
			interactionRecord.Team_Name__c = theUser.Team__c;
			interactionRecord.Group__c = theUser.Group__c;
			interactionRecord.Agent_Manager__c = theUser.My_Team_Manager__c;
			interactionRecord.JL_Department_Manager__c = theUser.JL_Department_Manager__c;

			// Case details
			interactionRecord.Contact_Reason__c = theCase.JL_Primary_Reason__c;
			interactionRecord.Reason_Detail__c = theCase.JL_Primary_Reason_Detail__c;

			if(theCase.OwnerId.getSObjectType() == Group.SObjectType) {
				interactionRecord.Queue__c = theCase.Owner.Name;
			} else {
				interactionRecord.Queue__c = theCase.JL_Last_Queue_Name__c;
			}
		
		} else if(timeType == END_TIME && existingOpenInteractionRecord != null) {

			interactionRecord = new Interaction__c();
			interactionRecord.Id = existingOpenInteractionRecord;
			interactionRecord.End_DT__c = inputDatetime;

		}

		if(interactionRecord != null) {
			upsert interactionRecord;
		}	

	}


	/* Helpers */
	private static Boolean isUserEligible(Id userId) {
		return PermissionSetLookup.CheckPermissionSetForUserByName(userId, NON_OMNI_PERMISSION_SET);
	}

	private static Id checkForExistingOpenInteractionRecord(Id userId, Id caseId) {

		List<Interaction__c> interactionRecord = [SELECT Id
													FROM Interaction__c
													WHERE
														Agent__c = :userId AND
														Case__c = :caseId AND
														Event_Type__c = 'Handle' AND
														Start_DT__c != null AND
														End_DT__c = null
													LIMIT 1
													];

		if(!interactionRecord.isEmpty()) {
			return interactionRecord[0].Id;
		} else {
			return null;
		}

	}



	private static User getUserInformation(Id userId) {
		
		return [SELECT Id, Team__c, Group__c, My_Team_Manager__c, JL_Department_Manager__c 
				FROM User 
				WHERE Id = :userId 
				LIMIT 1];
	
	}


	private static Case getCaseInformation(Id caseId) {

		return [SELECT OwnerId, Owner.Name, CaseNumber, JL_Primary_Reason__c, JL_Primary_Reason_Detail__c, JL_Last_Queue_Name__c
				FROM Case 
				WHERE Id = :caseId
				LIMIT 1];

	}
}