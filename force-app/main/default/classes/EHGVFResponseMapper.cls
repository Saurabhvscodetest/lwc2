/* Class that Maps the XML GVF Response to the VO object  */
// public class EHGVFResponseMapper extends EHServiceResponseMapper{
public class EHGVFResponseMapper {
    /*
    public static final String NODE_FULLFILLMENT_EVIDENCE = 'FulfilmentEventEvidence';
    public static final String NODE_FULLFILLMENT_EVIDENCE_TYPE = 'FulfilmentEvidenceType';
    public static final String NODE_FULLFILLMENT_EVIDENCE_URL = 'FulfilmentEvidenceImageURL';
    public static final String NODE_SHIPMENT_ROUE_PLAN = 'ShipmentRoutePlan';
    public static final String NODE_CUST_DEL_HUB = 'CustomerDeliveryHub';
    public static final String NODE_SHIP_TO_NAME = 'ShipToName';
    public static final String NODE_PLANNED_COLL_SERVICE = 'PlannedCustomerDeliveryCollectionService';
    public static final String NODE_SLOT_END_TIME = 'SlotEndTime';
    public static final String NODE_SLOT_START_TIME = 'SlotStartTime';
    public static final String NODE_RETAIL_SHIP_STATE_CODE = 'RetailTransactionShipmentStateCode';
    public static final String NODE_POSTAL_ADDRESS = 'PostalAddress';
    public static final String NODE_POSTAL_CODE_REF = 'PostalCodeReference';
    public static final String NODE_REATIL_SHIPMENT_ID = 'RetailTransactionShipmentID';
    public static final String NODE_POSTAL_CODE = 'PostalCode';
    
    
    Map<String, String> mappingLocations= null;
    
    public EHGVFResponseMapper(){
        Map<String, EHCDHLocation__c> locationsMap = EHCDHLocation__c.getAll();
            mappingLocations = new Map<String, String>();
            if(locationsMap != null && locationsMap.size() > 0){
                for(String key:  locationsMap.keySet()){
                    EHCDHLocation__c location = locationsMap.get(key);
                    mappingLocations.put(key.toUpperCase(), location.Value__c);
                }
        }
    }
    
    public override DeliveryTrackingVO mapResponse(Dom.XMLNode rootNode){
        
        DeliveryTrackingVO  delTrackingVO = new DeliveryTrackingVO();
        delTrackingVO.deliveryType = GVF;
        try{
            if(rootNode != null) {
                Dom.XMLNode bodyNode= rootNode.getChildElement(NODE_BODY, SOAP_ENV_NS);
                if(bodyNode != null){
                    Dom.XMLNode showRetailTransactionShipmentNode= bodyNode.getChildElement(NODE_SHOW_RETAIL_SHIPMENT, SERVICE_NS );
                    if(showRetailTransactionShipmentNode != null) {
                        Dom.XMLNode dataAreaNode = showRetailTransactionShipmentNode.getChildElement(NODE_DATA_AREA, null);
                        if(dataAreaNode != null) {
                            DeliveryTrackingVO.GVFOrderInformation orderInfo = new DeliveryTrackingVO.GVFOrderInformation(); 
                            orderInfo.deliveryInfoMap = new Map<String, DeliveryTrackingVO.GVFDeliveryInformation>(); 
                            delTrackingVO.gvfOrderInf= orderInfo;
                            List<Dom.XMLNode> dataAreaNodeChildList = dataAreaNode.getChildElements();
                            for(Dom.XMLNode dataAreaNodeChild: dataAreaNodeChildList){
                                //EACH DELIVERY 
                                if(dataAreaNodeChild.getName().equalsIgnoreCase(NODE_REATIL_SHIPMENT)) {
                                    DeliveryTrackingVO.GVFDeliveryInformation delInfo = new  DeliveryTrackingVO.GVFDeliveryInformation();
                                    List<Dom.XMLNode> retailShipChildList = dataAreaNodeChild.getChildElements();
                                    boolean isFullfillmentNodeFound = false;
                                    boolean isCdhNodeFound = false;
                                    Integer index = 0;
                                    Map<String, List<DeliveryTrackingVO.GVFParcelInformation>> parcelInfoMap = new Map<String, List<DeliveryTrackingVO.GVFParcelInformation>>();
                                    for(Dom.XMLNode retShipChild: retailShipChildList){
                                        //EACH PARCEL   
                                        if(retShipChild.getName().equalsIgnoreCase(NODE_REATIL_SHIPMENT_ITEM)) {
                                            String parcelId = PARCEL_KEY + (index++); 
                                            //Product Code & Product Description population
                                            String productCode = getProductCode(retShipChild);
                                            
                                            String productDescription = getProductDescription(retShipChild);
                                            
                                            List<Dom.XMLNode> retailTransactionShipmentItemChildList = retShipChild.getChildElements();
                                            for(Dom.XMLNode retailTransactionShipmentItemChildNode : retailTransactionShipmentItemChildList) {
                                                    //Based on the API assumptions - We must get only 1 ParcelForDelivery per  retailTransactionShipmentItemChildNode                            
                                                    if(retailTransactionShipmentItemChildNode.getName().equalsIgnoreCase(NODE_PARCEL_FOR_DELIVERY)) {
                                                        //Parcel Number
                                                        Dom.XMLNode parcelStatus = retailTransactionShipmentItemChildNode.getChildElement(NODE_PARCEL_STATUS, null) ;
                                                        Dom.XMLNode parcelBarCodeNode = retailTransactionShipmentItemChildNode.getChildElement(NODE_PARCEL_BAR_CODE, null) ;
                                                        String parcelNo = getValue(parcelBarCodeNode);
                                                        String productStatus = getValue(parcelStatus);
                                                        
                                                        //FulfilmentEvent
                                                        List<Dom.XMLNode> parcelForDeliveryChildNodes = retailTransactionShipmentItemChildNode.getChildElements();
                                                        List<DeliveryTrackingVO.GVFParcelInformation> parcelInfoList = new List<DeliveryTrackingVO.GVFParcelInformation>();
                                                        for(Dom.XMLNode parcelForDeliveryChildNode : parcelForDeliveryChildNodes) {
                                                             //EACH PARCEL EVENTS   
                                                             if(parcelForDeliveryChildNode.getName().equalsIgnoreCase(NODE_FULLFILLMENT_EVENT)) {
                                                                DeliveryTrackingVO.GVFParcelInformation parcelInfo = new DeliveryTrackingVO.GVFParcelInformation();
                                                                parcelInfo.parcelId = parcelId;
                                                                parcelInfo.productCode = productCode;
                                                                parcelInfo.productStatus = productStatus;
                                                                parcelInfo.productDescription = productDescription;
                        
                                                                Dom.XMLNode descriptionNode = parcelForDeliveryChildNode.getChildElement(NODE_DESCRIPTION, null) ;
                                                                parcelInfo.eventStatus = getValue(descriptionNode);
                                                                Dom.XMLNode actualEndDateTimestampNode = parcelForDeliveryChildNode.getChildElement(NODE_ACTUAL_DATE_TIME_STAMP, null) ;
                                                                parcelInfo.eventTime = getDateTime(getValue(actualEndDateTimestampNode));
                                                                
                                                                List<Dom.XMLNode> fulfilmentEventChildNodes = parcelForDeliveryChildNode.getChildElements();
                                                                List<String> photoUrlList = new List<String>();
                                                                for(Dom.XMLNode fulfilmentEventChildNode : fulfilmentEventChildNodes) {
                                                                    if(fulfilmentEventChildNode.getName().equalsIgnoreCase(NODE_FULLFILLMENT_EVIDENCE)) {
                                                                        Dom.XMLNode fulfilmentEvidenceTypeNode = fulfilmentEventChildNode.getChildElement(NODE_FULLFILLMENT_EVIDENCE_TYPE, null) ;
                                                                        String evidenceType = getValue(fulfilmentEvidenceTypeNode);
                                                                        Dom.XMLNode fulfilmentEvidenceImageURLNode = fulfilmentEventChildNode.getChildElement(NODE_FULLFILLMENT_EVIDENCE_URL, null) ;
                                                                        String evidenceUrl = getValue(fulfilmentEvidenceImageURLNode);
                                                                        if(evidenceType.equalsIgnoreCase(PHOTO)){
                                                                            photoUrlList.add(evidenceUrl);
                                                                        }
                                                                        if(evidenceType.equalsIgnoreCase(SIGNATURE)){
                                                                            parcelInfo.signatureUrl = evidenceUrl;
                                                                        }
                                                                    }
                                                                    
                                                                    if(fulfilmentEventChildNode.getName().equalsIgnoreCase(NODE_REASON)) {
                                                                        Dom.XMLNode nameNode = fulfilmentEventChildNode.getChildElement(NODE_NAME, null) ;
                                                                        parcelInfo.furtherInformation = getValue(nameNode);
                                                                    }
                                                                    if(fulfilmentEventChildNode.getName().equalsIgnoreCase(NODE_LOCATION)) {
                                                                        Dom.XMLNode parentLocationNode = fulfilmentEventChildNode.getChildElement(NODE_PARENT_LOCATION, null) ;
                                                                        if(parentLocationNode != null){
                                                                            Dom.XMLNode locationDescriptionNode = parentLocationNode.getChildElement(NODE_LOCATION_DESCRIPTION, null) ;
                                                                            parcelInfo.currentLocation = getMappedLocation(getValue(locationDescriptionNode));
                                                                        }
                                                                    }
                                                                }
                                                                parcelInfo.photoEvidenceUrls = photoUrlList;
                                                                parcelInfoList.add(parcelInfo);
                                                             }
                                                              if(!parcelInfoList.isEmpty()){
                                                                parcelInfoMap.put(parcelId, parcelInfoList);
                                                             } else {
                                                                //If the parcel has no events display just the parcel information
                                                                DeliveryTrackingVO.GVFParcelInformation parcelInfo = new DeliveryTrackingVO.GVFParcelInformation();
                                                                parcelInfo.parcelId = parcelId ;
                                                                parcelInfo.productCode = productCode ;
                                                                parcelInfo.productStatus = productStatus;
                                                                parcelInfo.productDescription = productDescription ;
                                                                parcelInfoMap.put(parcelId, new  List<DeliveryTrackingVO.GVFParcelInformation>{parcelInfo});
                                                             }
                                                        }
                                                        
                                                        // Populate the delivery CDH location.
                                                        if(!isCdhNodeFound){
                                                            //We need to get the first item information.
                                                            Dom.XMLNode routeDropCollectionNode = retailTransactionShipmentItemChildNode.getChildElement(NODE_ROUTE_DROP_COLL_NODES, null) ;
                                                            if(routeDropCollectionNode != null) {
                                                                Dom.XMLNode shipmentRoutePlanNode = routeDropCollectionNode.getChildElement(NODE_SHIPMENT_ROUE_PLAN, null) ;
                                                                if(shipmentRoutePlanNode != null) {
                                                                    Dom.XMLNode customerDeliveryHubNode = shipmentRoutePlanNode.getChildElement(NODE_CUST_DEL_HUB, null) ;
                                                                    if(customerDeliveryHubNode != null) {
                                                                        Dom.XMLNode nameNode = customerDeliveryHubNode.getChildElement(NODE_NAME, null) ;
                                                                        delInfo.deliveryCDHLocation = getMappedLocation(getValue(nameNode));                         
                                                                    }
                                                                }
                                                            }
                                                            isCdhNodeFound = true;
                                                            
                                                        }
                                                        
                                                        
                                                    }
                                                }
                                                
                                            }
                                            //Parcel information map for the delivery                                   
                                            delInfo.parcelInformationList = parcelInfoMap;
                                            
                                            if(retShipChild.getName().equalsIgnoreCase(NODE_FULLFILLMENT_EVENT) && (!isFullfillmentNodeFound) ) {
                                                isFullfillmentNodeFound = true;
                                                Dom.XMLNode actualEndDateTimestampNode = retShipChild.getChildElement(NODE_ACTUAL_DATE_TIME_STAMP, null) ;
                                                delInfo.deliveryTimeOnStatus = getDateTime(getValue(actualEndDateTimestampNode));
                                            }                    
                                        }
                                        
                                        delInfo.delieveryId = dataAreaNodeChild.getChildElement(NODE_REATIL_SHIPMENT_ID, null).getText();
                                        Dom.XMLNode shipToNode = dataAreaNodeChild.getChildElement(NODE_SHIP_TO_NAME, null) ;
                                        delInfo.receipentName = getValue(shipToNode) ;
                        
                                        Dom.XMLNode plannedCustDelCollNode = dataAreaNodeChild.getChildElement(NODE_PLANNED_COLL_SERVICE, null) ;
                                        Dom.XMLNode slotEndTimeNode = plannedCustDelCollNode.getChildElement(NODE_SLOT_END_TIME, null) ;
                                        Dom.XMLNode SlotStartTimeNode = plannedCustDelCollNode.getChildElement(NODE_SLOT_START_TIME, null);
                                        //String to Datetime
                                        delInfo.deliverySlotFrom = getDateTime(getValue(SlotStartTimeNode));
                                        delInfo.deliverySlotTo = getDateTime(getValue(slotEndTimeNode)); 
                                        
                                        Dom.XMLNode retailTransactionShipmentStateCodeNode = dataAreaNodeChild.getChildElement(NODE_RETAIL_SHIP_STATE_CODE, null) ;
                                        delInfo.delieveryStatus = getValue(retailTransactionShipmentStateCodeNode);
                                        
                                        
                                        Dom.XMLNode postalAddressNode = dataAreaNodeChild.getChildElement(NODE_POSTAL_ADDRESS, null) ;
                                        if(postalAddressNode != null) {
                                            Dom.XMLNode postalCodeReferenceNode = postalAddressNode.getChildElement(NODE_POSTAL_CODE_REF, null) ;
                                            if(postalCodeReferenceNode != null) {
                                                Dom.XMLNode postalCodeNode = postalCodeReferenceNode.getChildElement(NODE_POSTAL_CODE, null) ;
                                                if(postalCodeNode != null) {
                                                    delInfo.receipentPostcode = getValue(postalCodeNode);
                                                }
                                            }
                                        }
                                        orderInfo.deliveryInfoMap.put(delInfo.delieveryId, delInfo);
                                                
                                }
                              }
                        }
                    }
                }
            }
        } catch (Exception exp) {
            system.debug(Logginglevel.ERROR,'ERROR ==>' + exp.getStackTraceString());
            delTrackingVO = getErrorInfo(exp.getStackTraceString());
        }
        return delTrackingVO;
    
    }
    

     private String getMappedLocation(String ehValue){
        String location = null;
        if(String.isNotBlank(ehValue)){
            location = mappingLocations.get(ehValue.toUpperCase());
            if(String.isBlank(location)){
                    location = ehValue;
            } 
        }
        return location;
    }
    */
}