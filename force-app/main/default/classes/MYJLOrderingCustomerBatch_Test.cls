@isTest
private class MYJLOrderingCustomerBatch_Test {
    
     @testSetup static void initialiseData() {
        
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        Config_Settings__c batchSizeConfig = new Config_Settings__c(Name = 'DATAMIGRATION_BATCHSIZE', Text_Value__c = '100');
            insert batchSizeConfig;
        
        Config_Settings__c myJlRequestCaseRecTypeConfig = new Config_Settings__c(Name = 'ORDERING_CUSTOMER_RECORDTYPE', Text_Value__c = 'Ordering_Customer');
            insert myJlRequestCaseRecTypeConfig;
            
        Config_Settings__c notificationConfig = new Config_Settings__c(Name = 'DATAMIGRATION_NOTIFICATION_ADDRESS', Text_Value__c = 'test@test.com');
            insert notificationConfig;  
    }
    
    @isTest static void testSingleOrderingCustomer() {        
    
     Contact orderingCustomer = createCustomer('Ordering_Customer');  
     Contact nonOrderingCustomer = createCustomer('Normal');  
     
     orderingCustomer  = [SELECT ID,SourceSystem__c FROM Contact WHERE ID =:orderingCustomer.id];
     nonOrderingCustomer  = [SELECT ID,SourceSystem__c FROM Contact WHERE ID =:nonOrderingCustomer.id];
     
     System.assertEquals(null,orderingCustomer.SourceSystem__c);
     System.assertEquals(null,nonOrderingCustomer.SourceSystem__c);
     
     Test.startTest();
        MYJLOrderingCustomerBatch batch = new MYJLOrderingCustomerBatch();
        Database.executeBatch(batch); 
      Test.stopTest();
     
     orderingCustomer  = [SELECT ID,SourceSystem__c FROM Contact WHERE ID =:orderingCustomer.id];
     nonOrderingCustomer  = [SELECT ID,SourceSystem__c FROM Contact WHERE ID =:nonOrderingCustomer.id];
    
     System.assertNotEquals(null,orderingCustomer.SourceSystem__c);
     System.assertEquals(null,nonOrderingCustomer.SourceSystem__c);
     //System.assert(orderingCustomer.SourceSystem__c!= null,'Ordering Customer');
     //System.assert(nonOrderingCustomer.SourceSystem__c== null,'Non Ordering Customer');
    }
    
     private static contact createCustomer(String customerType){
        Contact customer = new Contact();
            customer.firstname= 'abc';
            customer.lastname='corp';
            customer.email='abccorp@test.com';

            if(customerType.equalsIgnoreCase('Ordering_Customer')){
                customer.Shopper_ID__c = 'ShopperId'+Math.Random();
                customer.RecordTypeId = CommonStaticUtils.getContactRecordType('Ordering_Customer').id;
            }else{
           		 customer.RecordTypeId =  customer.RecordTypeId = CommonStaticUtils.getContactRecordType('Normal').id;
            }
            customer.Mailing_House_No_Text__c = '15';
            customer.Mailing_House_Name__c = 'Wixford';
            customer.Mailing_Street__c = '15 Oakfield';
            customer.Mailing_Address_Line2__c = 'Block A';
            customer.Mailing_Address_Line3__c = 'Square C';
            customer.Mailing_Address_Line4__c = 'Area E';
            customer.mailingcity = 'Liverpool';
            customer.mailingstate = 'Merseyside';
            customer.mailingcountry = 'United Kingdom';
            customer.MailingCountryCode='GB';
            customer.mailingpostalcode = 'L4 2QH';
            insert(customer);
        return customer;
     }
    
    
   
}