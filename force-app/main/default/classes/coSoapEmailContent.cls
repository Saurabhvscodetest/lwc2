/*************************************************
coSoapEmailContent

class to encapsulate soap requests

Author: Steven Loftus (MakePositive)
Created Date: 13/11/2014
Modification Date: 
Modified By: 

**************************************************/
public class coSoapEmailContent {  

	// public parameters to use in the search
	public String EmailQueueId {get; set;}

	// public result parameters
	public String Status {get; set;}
	public ErrorResult Error {get; set;}
	public String EmailContent {get; set;}

	public String getSoapRequest() {

		// always need to have a first and last name
		String soapRequest = 	'<?xml version="1.0" encoding="UTF-8"?>' +
							    '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:com="http://www.johnlewis.co.uk/common" xmlns:v1="http://www.johnlewis.com/service/order/ccOrderManagement/v1">' +
							       '<soapenv:Header>' +
							          '<com:trackingHeader>' +
							             '<from>ord</from>' +
							             '<requestId>112</requestId>' +
							             '<requestTime>' + DateTime.now().format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'') + '</requestTime>' +
							          '</com:trackingHeader>' +
							       '</soapenv:Header>' +
							       '<soapenv:Body>' +
							          '<v1:getCCOrderEmailDetailsRequest>' +
							             '<messageQueueId>' + this.EmailQueueId + '</messageQueueId>' +
							          '</v1:getCCOrderEmailDetailsRequest>' +
							       '</soapenv:Body>' +
							    '</soapenv:Envelope>';

		// return the built soap request
		return soapRequest;
	}

	// called to parse the soap response from the service
	public void setSoapResponse(string soapResponseDocument) {

    	Dom.Document doc = new Dom.Document();
    	
    	doc.load(soapResponseDocument);

    	Dom.XMLNode rootNode = doc.getRootElement();

    	// get all the namespaces out of the response which we need
        String serNS = 'http://www.johnlewis.com/service/order/ccOrderManagement/v1';
    	String soapenvNS = 'http://schemas.xmlsoap.org/soap/envelope/';

    	this.Status = 'Success';

		try {
	
			if (rootNode.getChildElement('Body', soapenvNS).getChildElement('Fault', soapenvNS) != null) {

				dealWithFaultNode(rootNode.getChildElement('Body', soapenvNS).getChildElement('Fault', soapenvNS));
				return;
			}

			// get the dataarea and show nodes
			Dom.XMLNode bodyNode = rootNode.getChildElement('Body', soapenvNS);

		    this.EmailContent = bodyNode.getChildElement('getCCOrderEmailDetailsResponse', serNS).getChildElement('emailContent', null).getText();

			// in case that came back empty		            
		    if (String.isBlank(this.EmailContent)) {
		        // pull it directly out of the soap response
		        this.EmailContent = soapResponseDocument.substring(soapResponseDocument.indexOf('![CDATA[') + 8, soapResponseDocument.indexOf(']]'));
            }

        } catch (Exception e) {

        	this.Status = 'Error';
			// catch all in case we try to access a node we think should be there but it is not
			setErrorDetails('INTERNAL_ERROR', 'Technical', 'An exception was generated during the parsing of the soap response: ' + e.getMessage());
        }
	}

	private void dealWithFaultNode(Dom.XMLNode faultNode) {

       	this.Status = 'Error';
		setErrorDetails(faultNode.getChildElement('faultcode', null).getText(), null, faultNode.getChildElement('faultstring', null).getText());
	}

	// override of setErrorDetails
	private void setErrorDetails(Dom.XMLNode errorNode) {

		setErrorDetails(errorNode.getAttributeValue('errorCode', ''), errorNode.getAttributeValue('errorType', ''),errorNode.getAttributeValue('errorDescription', ''));
	}

	// set the error details passed in
	private void setErrorDetails(String code, String type, String description) {

		this.Error = new ErrorResult();
		this.Error.ErrorCode = code;
		this.Error.ErrorType = type;
		this.Error.ErrorDescription = description;			
	}

	// class to hold the error
    public class ErrorResult {

    	public String ErrorCode {get; set;}
    	public String ErrorType {get; set;}
    	public String ErrorDescription {get; set;}
    }
}