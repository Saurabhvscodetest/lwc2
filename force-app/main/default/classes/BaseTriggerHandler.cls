/**
 *  Copyright 2011-2014 Andrey Gavrikov.
 *  https://github.com/neowit
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *	limitations under the License.
 *
 *	@description:
 *		this is a base handler which can be used for all Trigger Handler classes
 *
 *	@example:
 *	if we want to handle BeforeInsert and AfterUpdate events on Contact then:
 *	the handler code may look like so:
 *	public without sharing class ContactHandler extends BaseTriggerHandler {
 *
 *		override public void beforeInsert () {
 *			//... your code here
 *		}
 *
 *		override public void afterUpdate () {
 *			//... your code here
 *		}
 *	}
 *
 *	Trigger code will look like so:
 *	trigger Contact on Contact (before insert, before update, before delete, 
 *									after insert, after update, after delete, after undelete) {
 *		BaseTriggerHandler.process('ContactHandler');
 *	}
 */
public abstract class BaseTriggerHandler {
	
	/**
	 * by default getName() returns Class Name, but you can override it if needed
	 */
	public virtual String getName() {
		if (null == className) {
			String strVal = String.valueOf(this);
			className = strVal.split(':')[0];
		}
		return className;
	}

	/**
	 * key may be any string, but there are some conventions which have additional meaning
	 * <Before|After>.<Insert|Update|Delete|Undelete>.<handlerName>
	 * e.g.
	 * to skip Before.Update event of ContactHandler add skip reason like so
	 * addSkipReason('Before.Update.ContactHandler', 'some reason here')
	 */
	public static void addSkipReason(final String key, final String reason) {
		skipReasonMap.put(key, reason);
	}
	public static Boolean hasSkipReason(final String key) {
		String reason = skipReasonMap.get(key);
		if (!String.isBlank(reason)) {
			System.debug('SKIP ' + key + ': ' + reason);
			return true;
		}
		return false;
	}
	public static void removeSkipReason(final String key) {
		skipReasonMap.remove(key);
	}

	public virtual Boolean skipAllEventsForCurrentUser() {
		JL_RunTriggers__c runTriggerConfig = JL_RunTriggers__c.getValues(UserInfo.getProfileId());
		return null != runTriggerConfig && false == runTriggerConfig.Run_Triggers__c;
	}


	public class BlankHandler extends BaseTriggerHandler {
		public override String getName() {
			return 'BlankHandler';
		}
	}


	public virtual void beforeInsert () {
		//override if necessary
	}

	public virtual void beforeUpdate () {
		//override if necessary
	}

	public virtual void beforeDelete () {
		//override if necessary
	}

	public virtual void afterInsert () {
		//override if necessary

	}

	public virtual void afterUpdate () {
		//override if necessary
	}

	public virtual void afterDelete () {
		//override if necessary
	}

	public virtual void afterUnDelete () {
		//override if necessary
	}

	public static BaseTriggerHandler process(final String handlerName) {
		final BaseTriggerHandler handler = getHandler(handlerName);
		if (handler.skipAllEventsForCurrentUser()) {
			return handler;
		}
		if (Trigger.isBefore) {
			if (Trigger.isInsert && !hasSkipReason('Before.Insert.' + handler.getName())) {
				handler.beforeInsert();
			} else if (Trigger.isUpdate && !hasSkipReason('Before.Update.' + handler.getName())) {
				handler.beforeUpdate();
			} else if (Trigger.isDelete && !hasSkipReason('Before.Delete.' + handler.getName())) {
				handler.beforeDelete();
			}
		} else {
			if (Trigger.isInsert && !hasSkipReason('After.Insert.' + handler.getName())) {
				handler.afterInsert();
			} else if (Trigger.isUpdate && !hasSkipReason('After.Update.' + handler.getName())) {
				handler.afterUpdate();
			} else if (Trigger.isUnDelete && !hasSkipReason('After.Undelete.' + handler.getName())) {
				handler.afterUnDelete();
			} else if (Trigger.isDelete && !hasSkipReason('After.Delete.' + handler.getName())) {
				handler.afterDelete();
			}
		}
		return handler;
	}

	//////////////// Utility Methods ///////////////////
	/**
	 * helper method, useful to get Ids of records involved in a list
	 */
	public static Set<Id> toIdSet(List<SObject> recs) {
		return toIdSet(recs, 'id');
	}

	/**
	 * same as toIds(recs) but we can also specify what field to use, as opposed to it just be 'id' 
	 */
	public static Set<Id> toIdSet(List<SObject> recs, final String fName) {
		final Set<Id> ids = new Set<Id>();
		for (SObject rec : recs) {
			Id idVal = (Id)rec.get(fName);
			if (null != idVal) {
				ids.add(idVal);
			}
		}
		return ids;
	}

	//////////////// Private Methods ///////////////////
	private static BaseTriggerHandler getHandler(final String handlerName) {
		//check if provided instance is assigned (useful for unit tests)
		BaseTriggerHandler h = instanceByName.get(handlerName);
		if (null == h) {
			Type t = Type.forName(handlerName);
			if (null != t) {
				h = (BaseTriggerHandler)t.newInstance();
			} else {
				System.assert(false, 'Handler class ' + handlerName + ' not found');
			}
		}
		return h;
	}

	private static Map<String, String> skipReasonMap = new Map<String, String>();

	//in unit tests it is sometimes useful to substitute handler implementation
	@TestVisible private static Map<String, BaseTriggerHandler> instanceByName = new Map<String, BaseTriggerHandler>();

	private String className = null;

	//////////////////////////// Unit Tests ////////////////////////////////////////////////
	//it is impossible to generate trigger code from Apex test, thus testing BaseTriggerHandler.process('name') is not done 

	public class UnitTestHandler extends BaseTriggerHandler {
	}

	static testMethod void testProvidedHandler () {
		BaseTriggerHandler.instanceByName.put('#TEST_HANDLER#', new BlankHandler());
		BaseTriggerHandler h = BaseTriggerHandler.getHandler('#TEST_HANDLER#');
		System.assert(h instanceof BlankHandler, 'Incorect handler returned');
		
	}

	static testMethod void testDynamicHandler () {
		BaseTriggerHandler h = BaseTriggerHandler.getHandler('BaseTriggerHandler.UnitTestHandler');
		System.assert(h instanceof BaseTriggerHandler.UnitTestHandler, 'Incorect handler returned');
		
	}

}