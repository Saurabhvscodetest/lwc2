@isTest
public class ActionCaseLExControllerTest {
    
    public static final String CONTACT_CENTRE_TEAM_NAME = '	Hamilton - CRD';
    public static final String CONTACT_CENTRE_QUEUE_NAME = 'CRD_Work_Allocation_Hamilton';
    
    /**
    * @description	Test the correct Case Details are getting returned		
* @author		@stuartbarber	
*/
    
    @isTest static void testCaseDetailsAreGettingReturned() {
        
        User runningUser = UserTestDataFactory.getRunningUser();
        
        User contactCentreUser;
        
        Contact testContact;
        Case testCase;
        
        System.runAs(runningUser) {
            
            CustomSettingTestDataFactory.initialiseTeamCustomSettings(CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_QUEUE_NAME);
            
            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
        }
        
        System.runAs(contactCentreUser) {
            
            testContact = CustomerTestDataFactory.createContact();
            insert testContact;
            
            testCase = CaseTestDataFactory.createComplaintCase(testContact.Id);
            insert testCase;
            
            Case savedCase = [SELECT RecordType.Name, RecordTypeId FROM Case WHERE Id = :testCase.Id];
            Case expectedResult = ActionCaseLExController.getCaseDetails(savedCase.Id);
            
            System.assertEquals(expectedResult.RecordTypeId, savedCase.RecordTypeId);
            System.assertEquals(expectedResult.RecordType.Name, savedCase.RecordType.Name);
        }
    }
    
  
    /**
* @description	Test Case Complaint Categorisation PermSet is Assigned To User			
* @author		@stuartbarber	
*/
    
    @isTest static void testCaseComplaintCatPermSetIsAssignedToUser() {
        
        User runningUser = UserTestDataFactory.getRunningUser();
        
        User contactCentreUser;
        
        Contact testContact;
        Case testCase;
        
        System.runAs(runningUser) {
            
            CustomSettingTestDataFactory.initialiseTeamCustomSettings(CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_QUEUE_NAME);
            
            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
            
            UserTestDataFactory.assignPermissionSetToUser(contactCentreUser.Id, 'Case_Complaint_Categorisation_Fields');
        }
        
        System.runAs(contactCentreUser) {
            
            Boolean actualResult = ActionCaseLExController.isPermSetAssignedToUser('Case_Complaint_Categorisation_Fields');
            
            System.assertEquals(true, actualResult);
        }
    }
    
    
    /**
* @description	Tests the outstanding tasks are closed when the case is closed - test with tasks		
*/
    
    @isTest static void testCloseOutstandingTasksWhenCaseClosedWithTasks() {
        
        User runningUser = UserTestDataFactory.getRunningUser();
        
        User contactCentreUser;
        
        Contact testContact;
        Case testCase;
        
        System.runAs(runningUser) {
            
            CustomSettingTestDataFactory.initialiseTeamCustomSettings(CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_QUEUE_NAME);
            
            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
        }
        
        System.runAs(contactCentreUser) {
            
            testContact = CustomerTestDataFactory.createContact();
            insert testContact;
            
            testCase = CaseTestDataFactory.createComplaintCase(testContact.Id);
            insert testCase;
            
            
            Task aTask = new Task();
            aTask.Description = 'A task for testing';
            aTask.WhatId = testCase.Id;
            aTask.Subject = 'New task';
            insert aTask;
            
            testCase.Status = 'Closed - Resolved';
            
            testCase.jl_Primary_Category__c = 'Branch';
            testCase.jl_Primary_Reason__c = 'Customer Catering';
            testCase.jl_AreaBusiness_pri__c = 'Aberdeen';
            testCase.jl_Primary_Reason_Detail__c = 'Cleanliness';
            testCase.jl_Primary_Detail_Explanation__c = 'Benugo';
            update testCase;
            
            Case savedCase = [SELECT jl_NumberOutstandingTasks__c, Status FROM Case WHERE Id = :testCase.Id];
            Task outstandingTasksBeforeClosure = [SELECT Id, Status FROM Task LIMIT 1 ];
            System.assertEquals(1, savedCase.jl_NumberOutstandingTasks__c);
            System.assertEquals('New', outstandingTasksBeforeClosure.Status);
            
            ActionCaseLExController.closeOutstandingTasksWhenCaseClosed(testCase.Id);
            System.assertEquals('Closed - Resolved', savedCase.Status);
            Task outstandingTasksAfterClosure = [SELECT Id, Description, Status FROM Task LIMIT 1 ];
            System.assertEquals('Closed - Resolved', outstandingTasksAfterClosure.Status);
            System.assertNOTEquals('A task for testing', outstandingTasksAfterClosure.Description);
            
        }
    }
    
    /**
* @description	Tests the outstanding tasks are closed when the case is closed - test without tasks		
*/
    
    @isTest static void testCloseOutstandingTasksWhenCaseClosedWithoutTasks() {
        
        User runningUser = UserTestDataFactory.getRunningUser();
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        User contactCentreUser;
        
        Contact testContact;
        Case testCase;
        
        System.runAs(runningUser) {            
            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
        }
        
        System.runAs(contactCentreUser) {
            Test.startTest();
            testContact = CustomerTestDataFactory.createContact();
            insert testContact;
            
            testCase = CaseTestDataFactory.createPSECase(testContact.Id);
            testCase.jl_NumberOutstandingTasks__c = 1;
            insert testCase;
            
            testCase.Status = 'Closed - Resolved';
            update testCase;
            
            ActionCaseLExController.closeOutstandingTasksWhenCaseClosed(testCase.Id);
            
            List<Task> outstandingTasksAfterClosure = [SELECT Id, Description, Status FROM Task LIMIT 1 ];
            System.assertEquals(0, outstandingTasksAfterClosure.size());
            Test.stopTest();
        }
    }
}