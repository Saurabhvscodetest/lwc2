/**
 * Unit tests for BulkCaseTransferController class methods.
 */
@isTest
public class BulkCaseTransferController_TEST { 

	/**
	 * Test the basic screen functions of the class for selection etc.
	 */
    static testMethod void testBasicFunctions() {
       	UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
       	QueueSobject queueSO = [SELECT Id, Queue.Name, Queue.Id FROM QueueSobject ORDER BY Queue.Name LIMIT 1];
       	Id ownerId = queueSO.Queue.Id;
       	Account testAccount = UnitTestDataFactory.createAccount(1, true);
       	Contact testCon = UnitTestDataFactory.createContact(1, testAccount.id, true);
       	List<Case> caseList = new List<Case>();
       	
       	for (Integer i = 1; i <= 10; i++) {
	       	Case testCase = UnitTestDataFactory.createQueryCase(testCon.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, false);
	       	testCase.OwnerId = ownerId;
	       	caseList.add(testCase);
       	}
       	insert caseList;
       	
       	List<Case> casesWithOwners = [SELECT Id, OwnerId FROM Case WHERE Id IN :caseList];
       	Id ownerIdTest = casesWithOwners.get(0).OwnerId;

		PageReference pageRef = new PageReference('/apex/BulkCaseTransfer');
		Test.setCurrentPageReference(pageRef);

		BulkCaseTransferController bctController = new BulkCaseTransferController();
		bctController.queueSourceItem = ownerIdTest;

       	test.startTest();
		bctController.searchCases();
		List<BulkCaseTransferController.RowItem> riList = bctController.getCases();
		test.stopTest();
		
		system.assertEquals(10, riList.size());
		
		Integer totalPages = bctController.getTotalPages();
		system.assertEquals(1, totalPages);

		bctController.doSelectAllItems();
		system.assertEquals(10, bctController.selectedCaseIds.size());

		bctController.recordSelectItem = '5';
		bctController.doSelectNItems();
		system.assertEquals(5, bctController.selectedCaseIds.size());
		
		bctController.recordSelectItem = '100';
		bctController.doSelectNItems();
		system.assertEquals(10, bctController.selectedCaseIds.size());
		
		bctController.doDeSelectAllItems();
		system.assertEquals(0, bctController.selectedCaseIds.size());
		
		Case case1 = caseList.get(0);
		Case case2 = caseList.get(1);
		bctController.contextItem = case1.Id;
		bctController.doSelectItem();
		bctController.contextItem = case2.Id;
		bctController.doSelectItem();
		system.assertEquals(2, bctController.selectedCaseIds.size());

		bctController.contextItem = case1.Id;
		bctController.doDeselectItem();
		system.assertEquals(1, bctController.selectedCaseIds.size());
		
		system.assertEquals(false, bctController.selectAllChecked);

		for (Case c : caseList) {
			bctController.contextItem = c.Id;
			bctController.doSelectItem();
		}
		
		system.assertEquals(true, bctController.selectAllChecked);
		List<BulkCaseTransferController.RowItem> riList2 = bctController.getCases();
		system.assertEquals(10, riList2.size());
		
		Case errorCase = caseList.get(0);
		BulkCaseTransferController.RowItem errorRowItem = new BulkCaseTransferController.RowItem(errorCase, 'TEST ERROR');
	}
    
	/**
	 * Test failing transfers.
	 */
    static testMethod void getQueueItems_ShouldFilterUnwantedQueues() {
    	UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
    	
    	/* Please ENSURE that this queue name does not exist in the org */
    	String QUEUE_NAME = 'ZZZZZZZZJL Branch supplier direct - exc';
        /* Since the custom settings Name Field can hold upto 38 characters only , So due to this we cannot add the full queue name. */
        String QUEUE_NAME_TRUNCATED = QUEUE_NAME.left(CommonStaticUtils.MAX_SEARCH_TEXT_LENGTH);
        String SEARCH_TEXT = QUEUE_NAME.substring(0, 10);

        User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		System.runAs (currentUser) {
	        QueueOwnerFilter__c qof = new QueueOwnerFilter__c(Name= QUEUE_NAME_TRUNCATED);
	        qof.Filter_from_Bulk_Transfer__c = true;
			insert qof;
	        
	        Group testGroup1 = new Group(Name = QUEUE_NAME, type='Queue');
	        insert testGroup1;
	        QueueSobject testQueue1 = new QueueSObject(QueueID = testGroup1.id, SobjectType = 'Case');
	        insert testQueue1;
	     } 

        BulkCaseTransferController bctController = new BulkCaseTransferController();
        List<SelectOption> queueItems = new List<SelectOption>();
        
        Test.startTest();
	        queueItems = bctController.getAllQueueItems('FROM');
        Test.stopTest();
        
    	Boolean queueNameFound = false;
    	for (SelectOption so : queueItems) {
			if (QUEUE_NAME.equals(so.getLabel())) {
				queueNameFound = true;
				break;
			}
        }     
        System.assert(!queueNameFound , 'Queue Name must not be found');
    }
	/**
	 * Test failing transfers.
	 */
    static testMethod void testTransferErrors() {
       	UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
       	
       	Config_Settings__c selectRangeConfig = new Config_Settings__c(Name = BulkCaseTransferController.BULK_CASE_TRANSFER_SELECT_RANGES_CONFIG_KEY, Text_Value__c = '50;250;1000');
       	insert selectRangeConfig;
       	
       	QueueSobject queueSO = [SELECT Id, Queue.Name, Queue.Id FROM QueueSobject ORDER BY Queue.Name LIMIT 1];
       	Id ownerId = queueSO.Queue.Id;
       	Account testAccount = UnitTestDataFactory.createAccount(1, true);
       	Contact testCon = UnitTestDataFactory.createContact(1, testAccount.id, true);
       	List<Case> caseList = new List<Case>();
       	
       	for (Integer i = 1; i <= 10; i++) {
	       	Case testCase = UnitTestDataFactory.createQueryCase(testCon.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, false);
	       	testCase.OwnerId = ownerId;
	       	caseList.add(testCase);
       	}
       	insert caseList;
       	
       	List<Case> casesWithOwners = [SELECT Id, OwnerId FROM Case WHERE Id IN :caseList];
       	Id ownerIdTest = casesWithOwners.get(0).OwnerId;

		PageReference pageRef = new PageReference('/apex/BulkCaseTransfer');
		Test.setCurrentPageReference(pageRef);

		BulkCaseTransferController bctController = new BulkCaseTransferController();
		bctController.queueSourceItem = ownerIdTest;
		bctController.queueDestinationItem = ownerIdTest;
		List<SelectOption> queueItems = bctController.getAllQueueItems('FROM');
		system.assertNotEquals(0, queueItems.size());

		bctController.searchCases();
		List<BulkCaseTransferController.RowItem> riList = bctController.getCases();
		
		system.assertEquals(10, riList.size());
		
		PageReference pr1 = bctController.transferCases();
		system.assertEquals(null, pr1);
		
		for (SelectOption so : queueItems) {
			Id tempId = so.getValue();
			if ((Id)so.getValue() != ownerIdTest) {
				bctController.queueDestinationItem = tempId;
				break;
			}
        }               		
		
		system.assertNotEquals(bctController.queueSourceItem, bctController.queueDestinationItem);
		
		PageReference pr2 = bctController.transferCases();
		system.assertEquals(null, pr2);
		
		bctController.doSelectAllItems();
		PageReference pr3 = bctController.transferCases();
		system.assertEquals(null, pr3);
    }

	/**
	 * Test cases transfer and comments are created as expected.
*/
    static testMethod void testTransferCases() {
       	UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
       	QueueSobject queueSO = [SELECT Id, Queue.Name, Queue.Id FROM QueueSobject ORDER BY Queue.Name LIMIT 1];
       	Id ownerId = queueSO.Queue.Id;
       	Account testAccount = UnitTestDataFactory.createAccount(1, true);
       	Contact testCon = UnitTestDataFactory.createContact(1, testAccount.id, true);
       	List<Case> caseList = new List<Case>();
       	
       	for (Integer i = 1; i <= 10; i++) {
	       	Case testCase = UnitTestDataFactory.createQueryCase(testCon.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, false);
	       	testCase.OwnerId = ownerId;
	       	caseList.add(testCase);
       	}
       	insert caseList;
       	
       	List<Case> casesWithOwners = [SELECT Id, OwnerId FROM Case WHERE Id IN :caseList];
       	Id ownerIdTest = casesWithOwners.get(0).OwnerId;

		PageReference pageRef = new PageReference('/apex/BulkCaseTransfer');
		Test.setCurrentPageReference(pageRef);

		BulkCaseTransferController bctController = new BulkCaseTransferController();
		bctController.queueSourceItem = ownerIdTest;
		bctController.queueDestinationItem = ownerIdTest;
		List<SelectOption> queueItems = bctController.getAllQueueItems('FROM');
		system.assertNotEquals(0, queueItems.size());

		bctController.searchCases();
		List<BulkCaseTransferController.RowItem> riList = bctController.getCases();
		
		system.assertEquals(10, riList.size());
		Id destinationQueueId;
		
		for (SelectOption so : queueItems) {
			Id tempId = so.getValue();
			if ((Id)so.getValue() != ownerIdTest) {
				destinationQueueId = tempId;
				bctController.queueDestinationItem = tempId;
				break;
			}
    }
		system.assertNotEquals(bctController.queueSourceItem, bctController.queueDestinationItem);

		bctController.transferComment = 'Test Comment';
        /* select all cases - which in turn expected to call the getCases to display all the selected RowItems */
		bctController.doSelectAllItems();
        bctController.getCases();
        system.debug('B4 - Sources ---  ' + bctController.queueSourceItem + '  --- DEST -- ' +  bctController.queueDestinationItem + ' -- destinationQueueId --' + destinationQueueId);
                
		PageReference pr1 = bctController.transferCases();
        system.debug('Transfer cases messages ' + ApexPages.getMessages());

		system.assertNotEquals(null, pr1);

       	List<Case> casesWithOwners2 = [SELECT Id, OwnerId FROM Case WHERE Id IN :caseList];
		system.assertEquals(10, casesWithOwners2.size());
        system.debug('AFTER - Sources ---  ' + bctController.queueSourceItem + '  --- DEST -- ' +  bctController.queueDestinationItem + ' -- destinationQueueId --' + destinationQueueId);       
       	for (Case c : casesWithOwners2) {
       		system.assertEquals(destinationQueueId, c.OwnerId);
       	}

       	List<CaseComment> ccList = [SELECT Id, ParentId FROM CaseComment WHERE ParentId IN :caseList];
		system.assertNotEquals(10, ccList.size());
    }

	/**
	 * Test DataTablePaginator functions.
	 */
    static testMethod void testDataTablePaginatorFunctions() {
       	UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        Integer PAGE_SIZE = 2;
        Integer NO_OF_RECORDS = 10;

        Config_Settings__c pageSizeConfig = new Config_Settings__c(Name = BulkCaseTransferController.BULK_CASE_TRANSFER_PAGE_SIZE_CONFIG_KEY, Number_Value__c = PAGE_SIZE );
       	insert pageSizeConfig;

       	QueueSobject queueSO = [SELECT Id, Queue.Name, Queue.Id FROM QueueSobject ORDER BY Queue.Name LIMIT 1];
       	Id ownerId = queueSO.Queue.Id;
       	Account testAccount = UnitTestDataFactory.createAccount(1, true);
       	Contact testCon = UnitTestDataFactory.createContact(1, testAccount.id, true);
       	List<Case> caseList = new List<Case>();
       	
        for (Integer i = 1; i <= NO_OF_RECORDS ; i++) {
	       	Case testCase = UnitTestDataFactory.createQueryCase(testCon.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, false);
	       	testCase.OwnerId = ownerId;
	       	caseList.add(testCase);
       	}
       	insert caseList;
       	
       	List<Case> casesWithOwners = [SELECT Id, OwnerId FROM Case WHERE Id IN :caseList];
       	Id ownerIdTest = casesWithOwners.get(0).OwnerId;

		PageReference pageRef = new PageReference('/apex/BulkCaseTransfer');
		Test.setCurrentPageReference(pageRef);

		BulkCaseTransferController bctController = new BulkCaseTransferController();
		bctController.queueSourceItem = ownerIdTest;
		bctController.searchCases();
		List<BulkCaseTransferController.RowItem> riList = bctController.getCases();
		
		Test.startTest();
        system.assertEquals(NO_OF_RECORDS / PAGE_SIZE , bctController.getTotalPages());
        
		system.assertEquals(true, bctController.hasNext);
		system.assertEquals(false, bctController.hasPrevious);
		system.assertEquals(1, bctController.pageNumber);
		bctController.next();
		system.assert(bctController.hasNext);
		system.assert(bctController.hasPrevious);
		system.assertEquals(2, bctController.pageNumber);
		bctController.last();
		system.assert(!bctController.hasNext);
		system.assert(bctController.hasPrevious);
		system.assertEquals(5, bctController.pageNumber);
		bctController.previous();
		system.assert(bctController.hasNext);
		system.assert(bctController.hasPrevious);
		system.assertEquals(4, bctController.pageNumber);
		bctController.first();
		system.assertEquals(true, bctController.hasNext);
		system.assertEquals(false, bctController.hasPrevious);
		system.assertEquals(1, bctController.pageNumber);
		bctController.sortField = 'CaseNumber';
		bctController.doSort();
		bctController.doSort();
		system.assertEquals(1, bctController.pageNumber);
		bctController.sortField = 'ContactId';
		bctController.doSort();
		Test.stopTest();
	}
}