@isTest
public class clsLoyaltyCardTest {

    
    //Create test data
    
    public static List<Loyalty_Account__c> createmyJLdata(){
        List<Loyalty_Account__c> myJLList=new List<Loyalty_Account__c>();
        Contact customer=new Contact(LastName='test1',Shopper_ID__c='iywi6876wqe',email='testmyJL@gmail.com');
        insert customer;
        Loyalty_Account__c myJL=new Loyalty_Account__c(IsActive__c=true,Name='296487218321',Send_Welcome_Email__c=true,ShopperId__c='iywi6876wqe');
        insert myJL;
        Loyalty_Card__c card=new Loyalty_Card__c(Loyalty_Account__c=myJL.Id,Name=myJL.Name);
        insert card;
        Transaction_Header__c th=new Transaction_Header__c(Card_Number__c=card.Id,IsValid__c=true,Transaction_Amount__c=10);
        insert th;
        myJLList.add(myJL);
        return myJLList;
    }
    
    @isTest
    public static void testdeletemyJL(){
        Test.startTest();
        List<Loyalty_Account__c> myJL=createmyJLdata();
        clsLoyaltyCard.mainEntry(true, true, myJL);
		test.stopTest();
        
    }
    
    
}