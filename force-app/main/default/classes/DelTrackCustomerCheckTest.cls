@IsTest
	Public class DelTrackCustomerCheckTest {
    
    Public static List< DelTrackCustomerCheck.DelTrackCustomerRequest > chatbotInputs = new List< DelTrackCustomerCheck.DelTrackCustomerRequest > ();
    Public static List< DelTrackCustomerCheck.DelTrackCustomerRequest > chatbotInputs1 = new List< DelTrackCustomerCheck.DelTrackCustomerRequest > ();
    Public static List< DelTrackCustomerCheck.DelTrackCustomerRequest > chatbotInputs2 = new List< DelTrackCustomerCheck.DelTrackCustomerRequest > ();
    Public static List< DelTrackCustomerCheck.DelTrackCustomerRequest > chatbotInputs3 = new List< DelTrackCustomerCheck.DelTrackCustomerRequest > ();
    Public static List< DelTrackCustomerCheck.DelTrackCustomerRequest > chatbotInputs4 = new List< DelTrackCustomerCheck.DelTrackCustomerRequest > ();
    Public static List< DelTrackCustomerCheck.DelTrackCustomerRequest > chatbotInputs5 = new List< DelTrackCustomerCheck.DelTrackCustomerRequest > ();
    Public static List< DelTrackCustomerCheck.DelTrackCustomerRequest > chatbotInputs6 = new List< DelTrackCustomerCheck.DelTrackCustomerRequest > ();
    Public static List< DelTrackCustomerCheck.DelTrackCustomerRequest > chatbotInputs7 = new List< DelTrackCustomerCheck.DelTrackCustomerRequest > ();
        
        
@IsTest
    Public Static void testCustomerCheckMethod(){
        
        DelTrackCustomerCheck.DelTrackCustomerRequest delCustCodeCheck = new DelTrackCustomerCheck.DelTrackCustomerRequest();
       
        delCustCodeCheck.CustomerEnteredName = 'Vijay';
        delCustCodeCheck.EventHubCustomerName = 'Mr Vijay Ambati';
        chatbotInputs.add(delCustCodeCheck);
        
        DelTrackCustomerCheck.DelTrackCustomerRequest delCustCodeCheck1 = new DelTrackCustomerCheck.DelTrackCustomerRequest();
       
        delCustCodeCheck1.CustomerEnteredName = 'Mr Vijay Ambati';
        delCustCodeCheck1.EventHubCustomerName = 'Vijay';
        chatbotInputs1.add(delCustCodeCheck1);
        
        DelTrackCustomerCheck.DelTrackCustomerRequest delCustCodeCheck2 = new DelTrackCustomerCheck.DelTrackCustomerRequest();
       
        delCustCodeCheck2.CustomerEnteredName = 'MrsVijay';
        delCustCodeCheck2.EventHubCustomerName = 'Mrs Vijay Ambati';
        chatbotInputs2.add(delCustCodeCheck2);
        
        DelTrackCustomerCheck.DelTrackCustomerRequest delCustCodeCheck3 = new DelTrackCustomerCheck.DelTrackCustomerRequest();
       
        delCustCodeCheck3.CustomerEnteredName = 'MsVijay';
        delCustCodeCheck3.EventHubCustomerName = 'Ms Vijay Ambati';
        chatbotInputs3.add(delCustCodeCheck3);
        
        
        DelTrackCustomerCheck.DelTrackCustomerRequest delCustCodeCheck4 = new DelTrackCustomerCheck.DelTrackCustomerRequest();
       
        delCustCodeCheck4.CustomerEnteredName = 'MissVijay';
        delCustCodeCheck4.EventHubCustomerName = 'Miss Vijay Ambati';
        chatbotInputs4.add(delCustCodeCheck4);
        
        
        DelTrackCustomerCheck.DelTrackCustomerRequest delCustCodeCheck5 = new DelTrackCustomerCheck.DelTrackCustomerRequest();
       
        delCustCodeCheck5.CustomerEnteredName = 'MrVijay';
        delCustCodeCheck5.EventHubCustomerName = 'Mr Vijay Ambati';
        chatbotInputs5.add(delCustCodeCheck5);
        
        DelTrackCustomerCheck.DelTrackCustomerRequest delCustCodeCheck6 = new DelTrackCustomerCheck.DelTrackCustomerRequest();
       
        delCustCodeCheck6.CustomerEnteredName = 'Sekhar';
        delCustCodeCheck6.EventHubCustomerName = 'Mr Vijay Ambati';
        chatbotInputs6.add(delCustCodeCheck6);
        
        
        DelTrackCustomerCheck.DelTrackCustomerRequest delCustCodeCheck7 = new DelTrackCustomerCheck.DelTrackCustomerRequest();
       
        delCustCodeCheck7.CustomerEnteredName = null;
        delCustCodeCheck7.EventHubCustomerName = null;
        chatbotInputs7.add(delCustCodeCheck7);
        
        Test.startTest();
		DelTrackCustomerCheck.DelTrackChatBotProcess(chatbotInputs);
        DelTrackCustomerCheck.DelTrackChatBotProcess(chatbotInputs1);
        DelTrackCustomerCheck.DelTrackChatBotProcess(chatbotInputs2);
        DelTrackCustomerCheck.DelTrackChatBotProcess(chatbotInputs3);
        DelTrackCustomerCheck.DelTrackChatBotProcess(chatbotInputs4);
        DelTrackCustomerCheck.DelTrackChatBotProcess(chatbotInputs5);
        DelTrackCustomerCheck.DelTrackChatBotProcess(chatbotInputs6);
        DelTrackCustomerCheck.DelTrackChatBotProcess(chatbotInputs7);
		Test.stopTest();
        
    }
    

}