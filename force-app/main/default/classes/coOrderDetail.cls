/*************************************************
coOrderDetail

class to hold the details of an order

Author: Steven Loftus (MakePositive)
Created Date: 12/11/2014
Modification Date: 
Modified By: 

// Edit     Date        Who         Comment
// 001   11/06/2015    KG         Declaring New Lists and variables as part of OM related Tickets - CMP-66,67,68,84,85,86,87
// 002   16/06/2015    MP         Inner class used for displaying nested table - CMP-87
// 003   16/06/2015    MP         Get the next Order History Display Record -CMP-87
// 004   19/06/2015    MP         Sorting the Tansaction History table list - CMP-370
**************************************************/
public class coOrderDetail {
    
    public String OrderNumber {get; set;}
    public Date OrderDate {get; set;}
    public String OrderStatusCode {get; set;}
    public String OrderStatusDescription {get;set;}
    public Decimal TotalOrderValue {get; set;}
    public AddressType BillingAddress {get; set;}
    public AddressType ShippingAddress {get; set;}
    public List<TenderType> Tenders {get; set;}
    public List<OrderHistoryDisplayRecord> displayList {get; private set;}
   
    public TenderType PrimaryTender {
        get {
            if (this.privatePrimaryTender == null) {

                this.privatePrimaryTender = new TenderType();

                Integer giftVoucherIndex;
                Integer giftCardIndex;
                Integer listIndex = 0;

                // loop the held tenders looking for specific card types
                for (TenderType tender : this.Tenders) {

                    // if we find a gift voucher then that's all we need
                    if (tender.CardType == 'John Lewis Gift Voucher') {
                        giftVoucherIndex = listIndex;
                        break;
                    }

                    // if we find a gift card then we may use it if no gift voucher is found
                    if (tender.CardType == 'John Lewis Gift Card' && giftCardIndex == null) giftCardIndex = listIndex;

                    listIndex++;
                }

                // if we have a gift voucher then use it
                if (giftVoucherIndex != null) this.privatePrimaryTender = this.Tenders.get(giftVoucherIndex);

                // if we have a gift card then use it
                if (giftCardIndex != null) this.privatePrimaryTender = this.Tenders.get(giftCardIndex);

                // if we get to here then we had neither a gift voucher nor a gift card so use the first payment method
                this.privatePrimaryTender = this.Tenders.get(0);
            }

            return this.privatePrimaryTender;
        }
    }
    public List<BatchType> Batches {get; set;}
    public List<EmailType> Emails {get; set;}
    public List<CommentType> Comments {get; set;}
    //  001   11/06/2015    KG         Declaring New Lists and variables as part of OM related Tickets - CMP-66,67,68,84,85,86,87
    public List<PaymentType> Payments {get; set;}
    public List<HistoryType> HistoryDetails {get;set;}
    public List<TransactionType> TransactionDetails {get;set;}
    public List<GiftCardType> GiftCards {get; set;}
    //  001   11/06/2015    KG         Declaring New Lists and variables as part of OM related Tickets - CMP-66,67,68,84,85,86,87 --End

    private TenderType privatePrimaryTender;

    public void addComment(CommentType comment){
        if (this.Comments == null) this.Comments = new List<CommentType>();

        this.Comments.add(comment);
    }

    public void addEmail(EmailType email) {
        if (this.Emails == null) this.Emails = new List<EmailType>();

        this.Emails.add(email);
    }

    public void addBatch(BatchType batch) {
        if (this.Batches == null) this.Batches = new List<BatchType>();

        this.Batches.add(batch);
    }
    
    //  001   11/06/2015    KG         Declaring New Lists and variables as part of OM related Tickets - CMP-66,67,68,84,85,86,87
    public void addPayment(PaymentType payment) {
        if (this.Payments == null) this.Payments = new List<PaymentType>();

        this.Payments.add(payment);
    }
    
    public void addHistory(HistoryType history) {
        if(this.HistoryDetails == null) this.HistoryDetails = new List<HistoryType>();
        
        this.HistoryDetails.add(history);
    }
    
    public void addTransaction(TransactionType transactions) {
        if(this.TransactionDetails == null) this.TransactionDetails = new List<TransactionType>();
        
        this.TransactionDetails.add(transactions);
    } 
    
    public void addGiftCard(GiftCardType giftcards) {
        if(this.GiftCards == null) this.GiftCards = new List<GiftCardType>();
        
        this.GiftCards.add(giftcards);
    } 
    //  001   11/06/2015    KG         Declaring New Lists and variables as part of OM related Tickets - CMP-66,67,68,84,85,86,87-- End   
    public void addTender(TenderType tender) {
        // make sure we have a list to add to
        if (this.Tenders == null) this.Tenders = new List<TenderType>();

        // add to the list
        this.Tenders.add(tender);
    }


    public class EmailType {

        public String MessageQueueId {get; set;}
        public String RecipientEmail {get; set;}
        public DateTime SentDate {get; set;}
        public String Subject {get; set;}
        public String Content {get; set;}
    }

    public class CommentType {
        
        public String OrderComments {get; set;}
        public DateTime CommentTime {get; set;}
        public String EnteredByName {get; set;}
        public String EnteredByPosition {get; set;}
    }   

    public class BatchType {
        
        public String InternalDeliveryMethodDescription {get; set;}
        public Date DeliveryDate {get; set;}
        public Integer LeadTime {get;set;}
        
        
        public List<ItemType> Items {get; set;}

        public void addItem(ItemType item) {

            if (this.Items == null) Items = new List<ItemType>();

            this.Items.add(item);
        }
    }

    public class ItemType {

        public String ProductCode {get; set;}
        public String Item {get; set;}
        //  001   11/06/2015    KG         Declaring New Lists and variables as part of OM related Tickets - CMP-66,67,68,84,85,86,87
        public String Distributor {get; set;}
        public Decimal UnitPrice {get; set;}
        public Decimal UnitPartnerDiscount {get; set; }
        public Integer QuantityOrdered {get; set;}
        public Decimal TotalPrice {get; set;}
        public String Status {get; set;}
        //  001   11/06/2015    KG         Declaring New Lists and variables as part of OM related Tickets - CMP-66,67,68,84,85,86,87
        public String DistributorReference {get; set;}
        public Integer PurchaseOrderId {get; set;}
        //  001   11/06/2015    KG         Declaring New Lists and variables as part of OM related Tickets - CMP-66,67,68,84,85,86,87--End
        public Integer QuantityShipped {get; set;}
        public Date DateShipped {get; set;}
        //  001   11/06/2015    KG         Declaring New Lists and variables as part of OM related Tickets - CMP-66,67,68,84,85,86,87
        public Integer QuantityCancelled {get; set;}
        //  001   11/06/2015    KG         Declaring New Lists and variables as part of OM related Tickets - CMP-66,67,68,84,85,86,87
        public Integer QuantityReturned {get; set;}
    }
    
    //  001   11/06/2015    KG         Declaring New Lists and variables as part of OM related Tickets - CMP-66,67,68,84,85,86,87
    public class PaymentType {
    
        public String BillingEmail {get; set;}
        public String Title {get; set;}
        public String FirstName {get; set;}
        public String LastName {get; set;}
        public String CompanyName {get; set;}
        public String HouseNumberOrName {get; set;}
        public String StreetName {get; set;}
        public String AddressLine2 {get; set;}
        public String AddressLine3 {get; set;}
        public String AddressLine4 {get; set;}
        public String Town {get; set;}
        public String County {get; set;}
        public String PostCode {get; set;}
        public String Country {get; set;}
        public String DayTimeTelephoneNumber {get; set;}
        public String EveningTelephoneNumber {get; set;}
        public Date DateOfBirth {get; set;}
        public String PaymentMethod {get; set;}
        public String NameonCreditCard {get; set;}
        public String MaskedCardNumber {get; set;}
        public String ExpiryMonth {get; set;}
        public String ExpiryYear {get; set;}
        public String StartMonth {get; set;}
        public String Year {get; set;}
        public String StartYear {get; set;}
        public String IssueNumber {get; set;}
        public String SmartPayID {get; set;}
        public String Curency {get; set;}
        public String PayPalPayerId {get;set;}
        public String PayPalStatus {get;set;}
    }
    
    public class HistoryType {
    
        public dateTime TransactionDate {get; set;}
        public integer OrderNumber {get; set;}
        public String TransactionMethod {get; set;}
        public String AuthCode {get; set;}
        public String AVSCode {get; set;}
        public String TransactionType {get; set;}
        public Decimal TotalAmount {get; set;}
        public String TransactionResult {get; set;}
        public String Reason {get; set;}
        public String Authentication {get; set;}
        public integer TransactionID {get; set;} 
           
    }
    
    public class TransactionType {
    
        public integer OrderNumbers {get; set;}
        public String Description {get; set;}
        public String Type {get; set;}
        public integer Quantity {get; set;}
        public Decimal TotalAmount {get; set;}
        public Decimal TotalVoucher {get; set;}
        public Decimal TotalGiftCard {get; set;}
        public Decimal TotalDiscount {get; set;}
        public integer ReceiptItemID {get;set;}
    }
    
    public class GiftCardType {
    	
    	public String GiftCardNumber {get; set;}
    	public String TransactionType {get; set;}
    	public Decimal Amount {get; set;}
    	public String Result {get; set;}
    }
    
    //  001   11/06/2015    KG         Declaring New Lists and variables as part of OM related Tickets - CMP-66,67,68,84,85,86,87--End
       
    /**
     * // 002   16/06/2015    MP         Inner class used for displaying nested table - CMP-87
     */
     // 004   19/06/2015    MP         Sorting the Tansaction History table list - CMP-370
    public class OrderHistoryDisplayRecord implements Comparable {
        public coOrderDetail.HistoryType topLevel {get; private set;}
        public List<coOrderDetail.TransactionType> detailLevel {get; private set;}
        
        public void addTopLevel(coOrderDetail.HistoryType inHistoryType) {
            topLevel = inHistoryType;
        }

        public void addDetailLevel(coOrderDetail.TransactionType inTransactionType) {
            if (detailLevel == null) {
                detailLevel = new List<coOrderDetail.TransactionType>();
            }
            this.detailLevel.add(inTransactionType);
        }
             
        // Implement the compareTo() method to sort descending
        public Integer compareTo(Object compareTo) {
            OrderHistoryDisplayRecord compareToDispRec = (OrderHistoryDisplayRecord) compareTo;
            DateTime thisTransactionDate = this.topLevel.TransactionDate;
            DateTime compareTransactionDate = compareToDispRec.topLevel.TransactionDate;
            if (thisTransactionDate == compareTransactionDate) return 0;
            if (thisTransactionDate < compareTransactionDate) return 1;
            return -1;        
        }
    }
    // 004   19/06/2015    MP         Sorting the Tansaction History table list - CMP-370 -- End
    /**
     * // 003   16/06/2015    MP         Get the next Order History Display Record -CMP-87
     */
    public OrderHistoryDisplayRecord getOrderHistoryDisplayRecord() {
        if (displayList == null) {
            displayList = new List<OrderHistoryDisplayRecord>();
        }
        OrderHistoryDisplayRecord displayRec = new OrderHistoryDisplayRecord();
        displayList.add(displayRec) ;
        return displayRec;
    } 
    // 003   16/06/2015    MP         Get the next Order History Display Record -CMP-87--End
    public class TenderType {

        public String CardHolderName {get; set;}
        public String CardNumber {get; set;}
        public String StartDate {get; set;}
        public String ExpiryDate {get; set;}
        public Decimal TenderAmount {get; set;}
        public String CardType {get; set;}
    }

    public class AddressType {

        public String LastName {get; set;}
        public String FirstName {get; set;}
        public String Title {get; set;}
        public String Company {get; set;}
        public String HouseNumberOrName {get; set;}
        public String Street1 {get; set;}
        public String Street2 {get; set;}
        public String Street3 {get; set;}
        public String Street4 {get; set;}
        public String City {get; set;}
        public String County {get; set;}
        public String Country {get; set;}
        public String Phone {
            get {
                if (this.privatePhone == '0') return 'Unknown';
                return this.privatePhone;
            } 
            set {
                this.privatePhone = value;
            }
        }
        public String PhoneEvening {get; set;}
        public String Email {get; set;}
        public String PostCode {get; set;}

        private String privatePhone;

        public String FullName {
            get {
                String title = String.isBlank(this.Title) ? '' : this.Title + ' ';
                String firstName = String.isBlank(this.FirstName) ? '' : this.FirstName + ' ';

                return (title + firstName + this.LastName).trim();
            }
        }

        public String HtmlAddress {
            get {
                return (withBR(this.FullName) + withBR(this.Company) + withBR(this.HouseNumberOrName) + withBR(this.Street1) + withBR(this.Street2) + withBR(this.Street3) + 
                    withBR(this.Street4) + withBR(this.City) + withBR(this.County) + withBR(this.PostCode) + withBR(this.Country)).removeEnd('<BR/>');
            }
        }

        private String withBR(String value) {
            return withDelimeter(value, '<BR/>');
        }

        private String withDelimeter(String value, String delimeter) {

            return String.isBlank(value) ? '' : value + delimeter;
        }
    }
}