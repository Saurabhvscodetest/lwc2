/**
 *	@author: Andrey Gavrikov (westbrook)
 *	@date: 2014-10-08 11:35:38 
 *	@description:
 *	    Test methods for MyJL_SystemDateHandler
 *	
 *	Version History :   
 *	2014-10-08 - MJL-1158 - AG
 *	initial version copying on *create*
 *	- Source_LastModifiedDate__c -> LastModifiedDate
 *	- Source_CreatedDate__c -> CreatedDate
 *		
 */
@isTest
public class MyJL_SystemDateHandlerTest  {
	/**
	 * 
	 *	MJL-1158
	 *	check that following data is copied on Insert and not on Update
	 *	- Source_LastModifiedDate__c -> LastModifiedDate
	 *	- Source_CreatedDate__c -> CreatedDate
	 */
	static testMethod void testSystemDatesInsert () {
		//check if we can actually run this test, i.e. if CreatedDate is still writable on Insert
		try {
			Database.insert(new Loyalty_Account__c( Name = 'Test', CreatedDate = System.now() -10, LastModifiedDate = System.now() - 9));
		} catch (System.DmlException e) {
			if (e.getDmlType(0) == System.StatusCode.CANNOT_INSERT_UPDATE_ACTIVATE_ENTITY) {
				return;// can not run unit test, STOP
			}
		}

		System.runAs(MyJL_TestUtil.getTestUser('testSystemDatesInsert')) {
			//tell MyJL_SystemDateHandler that it needs to run the logic
			CustomSettings.setTestMyJLSetting(UserInfo.getUserId(), new CustomSettings.TestRecord(new Map<String, Object>{'Populate_Audit_fields__c' => true}));

			final Datetime todayMinus10 = Datetime.now() - 10;
			final Datetime todayMinus5 = Datetime.now() - 5;
			List<Loyalty_Account__c> accs = new List<Loyalty_Account__c>{
				new Loyalty_Account__c (Name = 'CreatedDate only', Source_CreatedDate__c = todayMinus10),
					new Loyalty_Account__c (Name = 'BothDates', 
							Source_CreatedDate__c = todayMinus10,
							Source_LastModifiedDate__c = todayMinus5
							),
					new Loyalty_Account__c (Name = 'NoDates')
			};

			Database.insert(accs);
			//load results
			final Map<String, Loyalty_Account__c> accByName = new Map<String, Loyalty_Account__c>();
			for(Loyalty_Account__c acc : [select Name, Source_LastModifiedDate__c, Source_CreatedDate__c, LastModifiedDate, CreatedDate 
					from Loyalty_Account__c]) {
				accByName.put(acc.Name, acc);
					}
			//System.assertEquals(todayMinus10, accByName.get('CreatedDate only').CreatedDate, 'Account "CreatedDate only" must have received custom value for CreatedDate');
			//System.assertEquals(todayMinus10, accByName.get('BothDates').CreatedDate, 'Account "BothDates" must have received custom value for CreatedDate');
			//System.assertEquals(todayMinus5, accByName.get('BothDates').LastModifiedDate, 'Account "BothDates" must have received custom value for CreatedDate');
			//System.assertEquals(System.today(), accByName.get('NoDates').CreatedDate.date(), 'Account "NoDates" must not have received custom value for CreatedDate');

			//make sure that no values are set on Update
			accs[0].Source_LastModifiedDate__c = todayMinus5;
			try {
				Database.update(accs[0]);
			} catch (System.DmlException e) {
				if (e.getDmlType(0) == System.StatusCode.CANNOT_INSERT_UPDATE_ACTIVATE_ENTITY && e.getDmlMessage(0).indexOf('Field CreatedDate is not editable') > 0) {
				//System.assert(false, 'Did not expect exception here, trigger must not have attempted to set LastModifiedDate on Update');
				}
			}
		}
	}
}