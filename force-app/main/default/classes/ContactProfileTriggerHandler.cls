//	Edit	Date		By		Why
//	001		18/02/15	NTJ		MJL-1570 - If the SourceLastModifiedDate is supplied check to see whether it is LATER than the current value. If it is then the edit can happen else
//								we need to use the previous values
//	002		26/02/15	NTJ		MJL-1581 - Make House No a Text field so that it does not display as 999,999 (should be 999999)
//	003		10/03/15	NTJ		MJL-1615 - Membership number needed in Outbound Messages so link Contact_Profile__c and Loyalty_Account__c.
//	004		24/03/15	NTJ		Fix date of birth
//	005		25/03/15	NTJ		Fix Salutation of 0
//	006		08/04/15	NTJ		MJL-1763 - Fix telephone number lengths, if (length < 8) then OMIT if (length > 19) then TRUNCATE
//	007		13/04/15	NTJ		MJL-1763 - Further telephone fixes
//	008		15/04/15	NTJ		MJL-1790 - Do not do a SELECT WHERE x IN set, if we have an empty set
//	009		29/06/15	NTJ		MJL-2100 - Use Contact_Profile__c.AnonymousFlag2__c
//	010		08/11/16	LLJ		Renamed file

public with sharing class ContactProfileTriggerHandler {
    
    public static boolean FIRST_BEFORE_TRIGGER_RUN = true;
    public static boolean FIRST_AFTER_TRIGGER_RUN = true;
    
    public static void mainEntry(Boolean isBefore, 
                               Boolean isDelete, Boolean isAfter, 
                               Boolean isInsert, Boolean isUpdate, 
                               Boolean isExecuting, List<Contact_Profile__c> newlist, 
                               Map<ID,Sobject> newmap, List<Contact_Profile__c> oldlist, 
                               Map<Id,Sobject> oldmap)
    {
        
		BaseTriggerHandler.process('MyJL_SystemDateHandler'); 
        //system.debug('@@entering ContactProfileTriggerHandler');
        
        ContactProfileTriggerHandler handler = new ContactProfileTriggerHandler();
        
        if (isDelete)
        {
            return;
        }       
    
        if (isInsert && isBefore)
        {
            //system.debug('@@calling handleContactProfileBeforeInserts');
            handler.handleContactProfileBeforeInserts(newlist);
			BaseTriggerHandler.process('MyJL_ContactProfileHandler');
            FIRST_BEFORE_TRIGGER_RUN = false;
        }
        
        if (isInsert && isAfter)
        {
            //system.debug('@@calling handleContactProfileAfterInserts');
            handler.handleContactProfileAfterInserts(newlist);
            FIRST_AFTER_TRIGGER_RUN = false;
        }
                        
        if (isUpdate && isBefore)
        {
            //system.debug('@@calling handleContactProfileBeforeUpdates');
            handler.handleContactProfileBeforeUpdates((Map<ID,Contact_Profile__c>)newmap, (Map<ID,Contact_Profile__c>)oldmap);
            FIRST_BEFORE_TRIGGER_RUN = false;
        }
        
        if (isUpdate && isAfter)
        {
            //system.debug('@@calling handleContactProfileAfterUpdates');
            handler.handleContactProfileAfterUpdates((Map<ID,Contact_Profile__c>)newmap, (Map<ID,Contact_Profile__c>)oldmap);
			BaseTriggerHandler.process('MyJL_ContactProfileHandler');
            FIRST_AFTER_TRIGGER_RUN = false;
        }
    }
        
    public void handleContactProfileBeforeInserts(List<Contact_Profile__c> newValues) {
        
        // create contact matching keyes
        for(Contact_Profile__c newCP : newValues){
            ContactUtils.updateContactProfileCaseMatchKeys(newCP);
            fixDateOfBirth(newCp);
            //fixSalutation(newCp);
            fixPhoneNumbers(newCp);
        }
        
    }
    
    public void handleContactProfileAfterInserts(List<Contact_Profile__c> newValues) {
        // MJL-1615 Look for Loyalty Accounts with same email
        Set<String> cpSet = new Set<String>();
		Map<String,Id> cpMap = new Map<String,Id>();
        for (Contact_Profile__c cp:newValues) {
        	if(cp.email__c !=null){
        		cpSet.add(cp.email__c);
        		cpMap.put(cp.email__c, cp.Id);
        	}	
        }
        
        List<Loyalty_Account__c> laUpdateList = new List<Loyalty_Account__c>();
		// MJL-1790
        List<Loyalty_Account__c> laList = new List<Loyalty_Account__c>();
        if (!cpSet.isEmpty()) {
	        laList = [SELECT Id, Customer_Profile__c, Email_Address__c FROM Loyalty_Account__c WHERE Email_Address__c IN :cpSet limit 50000];        	
        }
        for (Loyalty_Account__c la:laList) {
        	//system.debug('la: '+la);
        	// if the Loyalty Account is not already linked, we need to link it
        	if (la.Customer_Profile__c == null) {
				// find the email in the Contact Profile map
	        	Id cpId = (Id) cpMap.get(la.Email_Address__c);
	        	//system.debug('cpId: '+cpId);
				if (cpId != null) {
					// Link the Loyalty Account to the Contact profile
					la.Customer_Profile__c = cpId;
					la.adopted__c = true;
					laUpdateList.add(la);
				}
        	}
        }
        if (laUpdateList.size() > 0) {
        	update laUpdateList;
        }        
    }
        
    public void handleContactProfileBeforeUpdates(Map<ID,Contact_Profile__c> newMap, Map<Id,Contact_Profile__c> oldMap) {
        
        // in case any of the fields used to create the matching keys change, update the matching keys
        for(Contact_Profile__c newCP : newmap.values()){
            Contact_Profile__c oldCP = oldMap.get(newCP.id);
            // Start 001
            if (!MyJL_Util.lastModifiedIsLater(oldCP.Source_LastModifiedDate__c, newCP.Source_LastModifiedDate__c)) {
            	copyCP(oldCP, newCP);
            }
            // End 001
            if(newCP.FirstName__c != oldCP.FirstName__c
                    || newCP.LastName__c != oldCP.LastName__c
                    || newCP.Email__c != oldCP.Email__c
                    || newCP.Mailing_Street__c != oldCP.Mailing_Street__c
                    || newCP.MailingPostCode__c != oldCP.MailingPostCode__c){
                    
                ContactUtils.updateContactProfileCaseMatchKeys(newCP);
                    
            }
        }
        
    }
    
    public void handleContactProfileAfterUpdates(Map<ID,Contact_Profile__c> newValues, Map<Id,Contact_Profile__c> oldValues) {
        
        ContactProfileUtils.handleLoyaltyAccountAfterContactProfile((Map<Id,Contact_Profile__c>)newValues, skipAllEventsForCurrentUser());
    }
    
    public Boolean skipAllEventsForCurrentUser() {
        JL_RunTriggers__c runTriggerConfig = JL_RunTriggers__c.getValues(userinfo.getProfileId());
        return null != runTriggerConfig && false == runTriggerConfig.Run_Triggers__c;
    }
    
	/*
    private Boolean lastModifiedIsLater(Contact_Profile__c oldCP, Contact_Profile__c newCP) {
		// Assume we will update
		// Only do this check if we are recording last modified
		// true means that we will write the change to Salesforce
		Boolean isLater = MyJL_Util.isMigrationMode();

		if (isLater) {
			if ( oldCP.Source_LastModifiedDate__c != null) {
				if (newCP.Source_LastModifiedDate__c != null) {
					//system.debug('new: '+newCP.Source_LastModifiedDate__c+' old: '+oldCP.Source_LastModifiedDate__c);
					isLater = (newCP.Source_LastModifiedDate__c > oldCP.Source_LastModifiedDate__c);
				}
			}
			//system.debug('lastModifiedIsLater: '+isLater);			
		}
		
		return isLater;
    }
    */
    private void copyCP(Contact_Profile__c oldCP, Contact_Profile__c newCP) { 
		newCP.Source_LastModifiedDate__c = oldCP.Source_LastModifiedDate__c;
		newCP.FirstName__c = oldCP.FirstName__c;
        newCP.Mailing_House_Name__c = oldCP.Mailing_House_Name__c;
		newCP.Mailing_Street__c = oldCP.Mailing_Street__c;
		newCP.FirstName__c = oldCP.FirstName__c;
		newCP.Mailing_House_No_Text__c = oldCP.Mailing_House_No_Text__c;
		newCP.Telephone_Number__c = oldCP.Telephone_Number__c;
		newCP.Salutation__c = oldCP.Salutation__c;
		newCP.Initials__c = oldCP.Initials__c;
		newCP.Mailing_Address_Line2__c = oldCP.Mailing_Address_Line2__c;
		newCP.MailingCountry__c = oldCP.MailingCountry__c;
		newCP.Date_of_Birth__c = oldCP.Date_of_Birth__c;
		newCP.Mailing_County_Name__c = oldCP.Mailing_County_Name__c;
		newCP.Extract_ConcatReference__c = oldCP.Extract_ConcatReference__c;
		newCP.Mailing_Address_Line3__c = oldCP.Mailing_Address_Line3__c;
		newCP.LastName__c = oldCP.LastName__c;
		newCP.Shopper_ID__c = oldCP.Shopper_ID__c;
		newCP.Mailing_Address_Line4__c = oldCP.Mailing_Address_Line4__c;
		newCP.MailingCity__c = oldCP.MailingCity__c;
		newCP.Email__c = oldCP.Email__c;
		newCP.MailingPostCode__c = oldCP.MailingPostCode__c;
		newCP.SourceSystem__c  = oldCP.SourceSystem__c;
		newCP.AnonymousFlag2__c = oldCP.AnonymousFlag2__c;  	
	}
	
	private void fixDateOfBirth(Contact_Profile__c cp) {
		//system.debug('dob: '+cp.Date_of_Birth_Text__c);
		if (!String.isBlank(cp.Date_of_Birth_Text__c)) {
			if (cp.Date_of_Birth_Text__c.length() == 10) {
				String dt = cp.Date_of_Birth_Text__c;
				String yyyy = dt.subString(0,4);
				String mm = dt.subString(5,7);
				String dd = dt.subString(8,10);
				//system.debug(yyyy);
				//system.debug(mm);
				//system.debug(dd);
				Integer year = Integer.valueOf(yyyy);
				Integer month = Integer.valueOf(mm);
				Integer day = Integer.valueOf(dd);
				cp.Date_of_Birth__c = Date.newInstance(year, month, day);
			}
		}
		//system.debug(cp.Date_of_Birth__c);		
	}
	/*
	private void fixSalutation(Contact_Profile__c cp) {
		if (!String.IsBlank(cp.Salutation__c)) {
			if (cp.Salutation__c == '0') {
				if (!String.IsBlank(cp.Title__c)) {
					if (cp.Title__c != '0') {
						cp.Salutation__c = cp.Title__c;
					} else {
						cp.Salutation__c = '';
					}
				} else {
					cp.Salutation__c = '';
				}
			}
		}
	}
	*/
	
	private static final Integer MIN_PHONE_LENGTH = 8;
	private static final Integer MAX_PHONE_LENGTH = 19;
	
	private String fixPhoneNumber(String phone) {
		String normalised = phone;
		if (!String.isBlank(phone)) {
			// replace apostrophe, period and comma, semi colon and colon
			normalised = phone.replace('\'', '');
			normalised = normalised.replace('.', '');
			normalised = normalised.replace(',', '');
			normalised = normalised.replace(':', '');
			normalised = normalised.replace(';', '');
			
			
			// MJL-1763 - try to stop bad phone numbers from stopping Contact Profile insert
			Integer length = normalised.length();
			if (length < MIN_PHONE_LENGTH) {
				// just omit it
				normalised = null;
			} else if (length > MAX_PHONE_LENGTH){
				// truncate it
				normalised = normalised.substring(0, MAX_PHONE_LENGTH);
			}
			// finally check for alphabetic characters
			if (String.isNotBlank(normalised)) {
				Boolean bAlpha = normalised.toLowerCase().containsAny('abcdefghijklmnopqrstuvwxyz');
				if (bAlpha) {
					normalised = null;
				}							
			}
		}
		return normalised;
	}
	
	private void fixPhoneNumbers(Contact_Profile__c cp) {
		cp.HomePhone__c = fixPhoneNumber(cp.HomePhone__c);
		cp.MobilePhone__c = fixPhoneNumber(cp.MobilePhone__c);
		cp.OtherPhone__c = fixPhoneNumber(cp.OtherPhone__c);
		cp.Phone__c = fixPhoneNumber(cp.Phone__c);
		cp.Telephone_Number__c = fixPhoneNumber(cp.Telephone_Number__c);
	}
}