/******************************************************************************
* @author       Matt Povey
* @date         22/09/2015
* @description  Handler class for Interaction trigger events
*
* EDIT RECORD
*
* LOG     DATE        Author	JIRA		COMMENT      
* 001     22/09/2015  MP		CMP-684		Original code
*
*******************************************************************************
*/
public with sharing class InteractionTriggerHandler {

    /**
     * Main handler method called from clsTriggerHandler class
	 * @params:	All standard Trigger context variables and data lists/maps
     */

	public static final String dateFormat = 'yyyy-MM-dd HH:mm:ss';
	public static void mainEntry(Boolean isBefore, 
                               Boolean isDelete, Boolean isAfter, 
                               Boolean isInsert, Boolean isUpdate, 
                               Boolean isExecuting, List<Interaction__c> newlist, 
                               Map<ID,Sobject> newmap, List<Interaction__c> oldlist, 
                               Map<Id,Sobject> oldmap) {
                               	
		if (isBefore && (isInsert || isUpdate)) {
			for (Interaction__c interaction : newList) {
				updateDateTextValues(interaction);
			}
		}
	}
	
    /**
     * Set Start/End Date Text field values on Interaction record
	 * @params:	Interaction__c interaction		Incoming interaction records
     */
	private static void updateDateTextValues(Interaction__c interaction) {
		interaction.Start_DT_Text__c = setDateTextValue(interaction.Start_DT__c);
		interaction.End_DT_Text__c = setDateTextValue(interaction.End_DT__c);
	}
	
    /**
     * Convert date passed in to String representation
	 * @params:	DateTime inDateTime		DateTime value to convert
	 * @rtnval:	String					String representation of DateTime value
     */
	@TestVisible
	private static String setDateTextValue(DateTime inDateTime) {
		if (inDateTime == null) {
			return null;
		} else {
			return inDateTime.format(dateFormat);
		}
	}
}