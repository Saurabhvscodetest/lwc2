public class coSoapOrderSearch {

	// public parameters to use in the search
	public String CustomerId {get; set;}

	// public result parameters
	public String Status {get; set;}
	public ErrorResult Error {get; set;}
	public List<OrderSearchResult> Orders {get; set;}

	// default constructor
	public coSoapOrderSearch() {

		// create an empty list if null
		if (Orders == null) {
			Orders = new List<OrderSearchResult>();
		}
	}

	public String getSoapRequest() {

		// always need to have a first and last name
		String soapRequest = 	'<?xml version="1.0" encoding="UTF-8"?>' +
						        '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">' +
						            '<soapenv:Header/>' +
						            '<soapenv:Body>' +
						                '<viewcord:SearchCustomerOrder xmlns:viewcord="http://soa.johnlewispartnership.com/service/es/Transaction/CustomerOrder/CustomerOrder/ViewCustomerOrder/v2" xmlns:cmnEBM="http://soa.johnlewispartnership.com/schema/ebm/CommonEBM/v1" xmlns:txn="http://soa.johnlewispartnership.com/schema/ebo/Transaction/v1" xmlns:cmn="http://soa.johnlewispartnership.com/schema/ebo/Common/v1" xmlns:viewcordEBM="http://soa.johnlewispartnership.com/schema/ebm/Transaction/CustomerOrder/CustomerOrder/v1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:wsa="http://www.w3.org/2005/08/addressing">' +
						                    '<cmnEBM:ApplicationArea sentDateTime="' + DateTime.now().format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'') + '">'+
						                        '<cmnEBM:Channel>' +
						                            '<cmnEBM:ChannelID schemeID="OCIP" schemeAgencyID="JL"/>' +
						                            '<cmnEBM:ChannelName>OCIP</cmnEBM:ChannelName>' +
						                            '<cmnEBM:ChannelType>Sales</cmnEBM:ChannelType>' +
						                        '</cmnEBM:Channel>' +
						                        '<cmnEBM:Sender>' +
						                            '<cmnEBM:LogicalID schemeID="CaseManagement" schemeAgencyID="JL"/>' +
						                        '</cmnEBM:Sender>' +
						                        '<wsa:MessageID>' + coSoapHelper.generateGuid() + '</wsa:MessageID>' +
						                        '<cmnEBM:TrackingID>' + coSoapHelper.generateGuid() + '</cmnEBM:TrackingID>' +
						                    '</cmnEBM:ApplicationArea>' +
						                    '<viewcordEBM:DataArea>' +
						                        '<viewcordEBM:Search/>' +
						                        '<viewcordEBM:CustomerOrder>' +
						                            '<txn:CustomerID>' +
						                            	'<cmn:xrefIdentity ID="' + this.CustomerId + '" applicationID="JLDOrderManager"/>' +
						                            '</txn:CustomerID>' +
						                        '</viewcordEBM:CustomerOrder>' +
						                        '<viewcordEBM:CalendarPeriod>' +
						                            '<cmn:CalendarID code="OrderPlacedDate"/>' +
						                            // spec says that if no end date is provided then the current date time is used by the service
						                            //'<cmn:EndDateTime>2014-07-27T00:12:10.321+01:00</cmn:EndDateTime>' +
						                            '<cmn:StartDateTime>2010-01-01T00:00:00.000Z</cmn:StartDateTime>' +
						                        '</viewcordEBM:CalendarPeriod>' +
						                    '</viewcordEBM:DataArea>' +
						                '</viewcord:SearchCustomerOrder>' +
						            '</soapenv:Body>' +
						        '</soapenv:Envelope>';

		// return the built soap request
		return soapRequest;
	}

	// called to parse the soap response from the service
	public void setSoapResponse(string soapResponseDocument) {

    	Dom.Document doc = new Dom.Document();
    	
    	doc.load(soapResponseDocument);

    	Dom.XMLNode rootNode = doc.getRootElement();

    	// get all the namespaces out of the response
    	String serverEndPoint = 'http://soa.johnlewispartnership.com/';

		String viewcordNS = serverEndPoint + 'service/es/Transaction/CustomerOrder/CustomerOrder/ViewCustomerOrder/v2';
		String txnNS = serverEndPoint + 'schema/ebo/Transaction/v1';
		String cmnNS = serverEndPoint + 'schema/ebo/Common/v1';
		String cmnEBMNS = serverEndPoint + 'schema/ebm/CommonEBM/v1';
		String viewcordEBMNS = serverEndPoint + 'schema/ebm/Transaction/CustomerOrder/CustomerOrder/v1';
    	String soapenvNS = 'http://schemas.xmlsoap.org/soap/envelope/';
		String xsiNS = 'http://www.w3.org/2001/XMLSchema-instance';
		String wsaNS = 'http://www.w3.org/2005/08/addressing';

		try {
	
			
			if (rootNode.getChildElement('Body', soapenvNS).getChildElement('Fault', soapenvNS) != null) {
		
				dealWithFaultNode(rootNode.getChildElement('Body', soapenvNS).getChildElement('Fault', soapenvNS));
				return;
			}

			// get the dataarea and show nodes
			Dom.XMLNode dataAreaNode = rootNode.getChildElement('Body', soapenvNS).getChildElement('ShowCustomerOrder', viewcordNS).getChildElement('DataArea', viewcordEBMNS);
			Dom.XMLNode showNode = dataAreaNode.getChildElement('Show', viewcordEBMNS);

			// if there is a problem then get the error details and return
			this.Status = showNode.getAttributeValue('responseCode', '');

			if (this.Status != 'Success') {
				if (showNode.getAttributeValue('hasActionStatusRecords', '') == 'true') {

					setErrorDetails(showNode.getChildElement('ActionStatusRecord', cmnEBMNS).getChildElement('ServiceError', cmnEBMNS));
					return;

				// see if there is an internal error then get the details and return
				} else {

					setErrorDetails(showNode.getChildElement('ServiceError', cmnEBMNS));
					return;
				}
			}

			// if we get here then there is no error and we may have some orders
			for (Dom.XMLNode childNode : dataAreaNode.getChildElements()) {

				// there will be 0 to many customer orders
				if (childNode.getName() == 'CustomerOrder') {

					// get the oder id
                    String orderId = childNode.getChildElement('CustomerOrderID', txnNS).getChildElement('xrefIdentity', cmnNS).getAttributeValue('ID','');

                    // get the amount - this may not be the total for the order but assuming it is until test
                    String amount = childNode.getChildElement('CustomerOrderLineItem', txnNS).getChildElement('CustomerOrderChargePriceModifier', txnNS).getChildElement('Amount', txnNS).getAttributeValue('amount','');

                    String status;
                    String datePlaced;

                    // loop the possible statuses - assumption here is that they are in chronological order so the last status found will be stored
                    for (Dom.XMLNode subNode : childNode.getChildElements()) {

                        if (subNode.getName() == 'CustomerOrderStateAssignment') { 
                        
                        	// if there is a code then store it as the status
                        	if (subNode.getChildElement('CustomerOrderStateTypeCode', txnNS).getChildElement('xrefCode', cmnNS).getAttributeValue('code', '') != null) {

                            	status = subNode.getChildElement('CustomerOrderStateTypeCode', txnNS).getChildElement('xrefCode', cmnNS).getAttributeValue('code', '');
                        	}
   
   							// get the valid from date if it exists
   							if (subNode.getChildElement('ValidFrom', txnNS) != null) {

                            	datePlaced = subNode.getChildElement('ValidFrom', txnNS).getText();
                        	}
                        }
                    }

                    // if we get here then we have all the data we can find and should create a new order result record and add to the list

                    OrderSearchResult order = new OrderSearchResult();
                    order.Id = orderId;
                    order.TotalValue = coSoapHelper.getDecimal(amount);

                    order.DateTimePlaced = coSoapHelper.getDateTime(datePlaced);

                    order.Status = status;

                    this.Orders.add(order);
				}
			}
        } catch (Exception e) {

        	this.Status = 'Error';
			// catch all in case we try to access a node we think should be there but it is not
			setErrorDetails('INTERNAL_ERROR', 'Technical', 'An exception was generated during the parsing of the soap response: ' + e.getMessage());
        }
	}

	private void dealWithFaultNode(Dom.XMLNode faultNode) {

       	this.Status = 'Error';
		setErrorDetails(faultNode.getChildElement('faultcode', null).getText(), null, faultNode.getChildElement('faultstring', null).getText());
	}

	// override eof setErrorDetails
	private void setErrorDetails(Dom.XMLNode errorNode) {

		setErrorDetails(errorNode.getAttributeValue('errorCode', ''), errorNode.getAttributeValue('errorType', ''),errorNode.getAttributeValue('errorDescription', ''));
	}

	// set the error details passed in
	private void setErrorDetails(String code, String type, String description) {

		this.Error = new ErrorResult();
		this.Error.ErrorCode = code;
		this.Error.ErrorType = type;
		this.Error.ErrorDescription = description;			
	}

	// class to hold the error
    public class ErrorResult {

    	public String ErrorCode {get; set;}
    	public String ErrorType {get; set;}
    	public String ErrorDescription {get; set;}
    }

    // class to hold the customers found in the search
    public class OrderSearchResult {

    	public String Id {get; set;}
    	public Decimal TotalValue {get; set;}
    	public DateTime DateTimePlaced {get; set;}
    	public String Status {get; set;}
    }
}