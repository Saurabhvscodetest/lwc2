/**
 *	@author: Andrey Gavrikov (westbrook)
 *	@date: 2014-08-26
 *	@description:
 *	    main handler for all Loyalty_Card__c trigger events
 *	
 *	Version History :   
 *	2014-08-26 - MJL-768 - AG
 *	initial version
 *		
 * 	001 	19/02/15	NTJ			MJL-1570 - If in migration mode then check source last modified date
 *  002		15/04/15	NTJ			MJL-1790 - Do not do a SELECT WHERE x IN set, if we have an empty set
  		
 */
public without sharing class LoyaltyCardHandler extends MyJL_TriggerHandler {

	static public Boolean OPTIMISED_JOIN_CODE = true;

	override public void afterInsert () {
		if (OPTIMISED_JOIN_CODE == false) {
			linkOrphanedTransactions((List<Loyalty_Card__c>)Trigger.new, null);			
		}

		UpdateLoyaltyAccount((List<Loyalty_Card__c>)Trigger.new);
	}

	override public void beforeUpdate () {
        for(Loyalty_Card__c newLC : (List<Loyalty_Card__c>) trigger.new){
            Loyalty_Card__c oldLC = (Loyalty_Card__c) trigger.oldMap.get(newLC.id);
    		// Edit 001
    		// MJL-1570 - Check whether migration in-progress and check source last modification
            if (!myJL_Util.lastModifiedIsLater(oldLC.Source_LastModifiedDate__c, newLC.Source_LastModifiedDate__c)) {
            	copyLC(oldLC, newLC);
            }
            // End 001
        }		

		linkOrphanedTransactions((List<Loyalty_Card__c>)Trigger.new, (Map<Id, Loyalty_Card__c>)Trigger.oldMap);
	}
    
    
     override public void beforeDelete () {
		DeleteLoyaltyCards((List<Loyalty_Card__c>)Trigger.old);
	}
    
  
	public static void linkOrphanedTransactions(final List<Loyalty_Card__c> newCards, final Map<Id, Loyalty_Card__c> oldCardsMap) {
		final Boolean isInsert = null == oldCardsMap;
		final Set<String> cardNumbers = new Set<String>();
		final Map<String, Id> cardIdByCardNumber = new Map<String, Id>();
		
		for(Loyalty_Card__c card : newCards) {
			if (isInsert || card.Force_Orphan_Transaction_Search__c) {
				if (!MyJL_Util.isBlank(card.Name)) {
					cardIdByCardNumber.put(card.Name.toLowerCase(), card.Id);
				}
			}
			if (card.Force_Orphan_Transaction_Search__c) {
				card.Force_Orphan_Transaction_Search__c = false; //reset checkbox to disable further transaction searches on Card update
			}
		}

		final List<Transaction_Header__c> transactionsToLink = new List<Transaction_Header__c>();
		// MJL-1790
		if (!cardIdByCardNumber.keySet().isEmpty()) {
			for(Transaction_Header__c trans : [select Id, Card_Number_Text_Version__c from Transaction_Header__c 
												where Card_Number_Text_Version__c in: cardIdByCardNumber.keySet()
													and Orphaned_Record__c = true ]) {
				if (String.isNotBlank(trans.Card_Number_Text_Version__c)) {
					String cardNumber = trans.Card_Number_Text_Version__c.toLowerCase();
					Id cardId = cardIdByCardNumber.get(cardNumber);
					if (null != cardId) {
						trans.Card_Number__c = cardId;
						transactionsToLink.add(trans);
					}					
				}										
			}			
		}
		Database.update(transactionsToLink);
	}


	private static void UpdateLoyaltyAccount(final List<Loyalty_Card__c> newCards){
		
		final Map<Id, Loyalty_Card__c> accountIdsWithCards = new Map<Id, Loyalty_Card__c>();
		final List<Loyalty_Account__c> accountsToUpdate = new List<Loyalty_Account__c>();

		for(Loyalty_Card__c card : newCards) {
			if( card.Card_Type__c == MyJL_Const.MY_PARTNERSHIP_CARD_TYPE){
				accountIdsWithCards.put(card.Loyalty_Account__c, card);
			}
		}
        

		for (Loyalty_Account__c a : [Select Id, Membership_Number__c, Customer_Loyalty_Account_Card_ID__c From Loyalty_Account__c Where id IN :accountIdsWithCards.keySet()]) {
            
            Loyalty_Card__c card = accountIdsWithCards.get(a.id);
            if(card !=null){
                a.MyJL_Old_Membership_Number__c = a.Membership_Number__c;
                a.MyJL_Old_Barcode__c = a.Customer_Loyalty_Account_Card_ID__c;
                a.Membership_Number__c = card.Card_Number__c;
                a.Customer_Loyalty_Account_Card_ID__c = card.Card_Number__c;
                a.Name = card.Card_Number__c;
                a.My_Partnership_Activation_Date_Time__c = System.now();
                a.Scheme_Joined_Date_Time__c = System.now();
                accountsToUpdate.add(a);
            }
        }

		if(!accountsToUpdate.isEmpty()){
			try{
				Database.update(accountsToUpdate);
			}
			catch(Exception e){ //something else happened
				System.debug(e.getMessage());
			}
		}
	}
    
    
    
      public static void UpdateLoyaltyAccountDetails(final List<Loyalty_Card__c> newCards, final Map<Id, Loyalty_Card__c> oldCardsMap) {
	    final Map<Id, Loyalty_Card__c> accountIdsWithCards = new Map<Id, Loyalty_Card__c>();
		final List<Loyalty_Account__c> accountsToUpdate = new List<Loyalty_Account__c>();

        for(Loyalty_Card__c card : newCards) {
            if(card.Card_Type__c == MyJL_Const.MY_PARTNERSHIP_CARD_TYPE && card.Disabled__c == true && !oldCardsMap.get(card.id).Disabled__c == true){
				accountIdsWithCards.put(card.Loyalty_Account__c, card);
			}
		}

        for (Loyalty_Account__c a : [Select Id,Name, Membership_Number__c, Customer_Loyalty_Account_Card_ID__c,MyJL_Old_Membership_Number__c,MyJL_Old_Barcode__c From Loyalty_Account__c Where id IN :accountIdsWithCards.keySet()]) {
            Loyalty_Card__c card = accountIdsWithCards.get(a.id);
	        if(card != null){
				a.Membership_Number__c = a.MyJL_Old_Membership_Number__c;
				a.Customer_Loyalty_Account_Card_ID__c =a.MyJL_Old_Barcode__c;
		        a.Name = a.MyJL_Old_Membership_Number__c;
				accountsToUpdate.add(a);
			}
		}        
        
		if(!accountsToUpdate.isEmpty()){
			try{
				Database.update(accountsToUpdate);
			}
			catch(Exception e){ //something else happened
				System.debug(e.getMessage());
			}
		}   
        
    }
    
    
       public static void DeleteLoyaltyCards(List<Loyalty_Card__c> oldCards) {
	    final Map<Id, Loyalty_Card__c> accountIdsWithCards = new Map<Id, Loyalty_Card__c>();
		final List<Loyalty_Account__c> accountsToUpdate = new List<Loyalty_Account__c>();
        final List<Loyalty_Card__c> cardsToDelete = new List<Loyalty_Card__c>();

        for(Loyalty_Card__c card : oldCards) {
            if(card.Card_Type__c == MyJL_Const.MY_PARTNERSHIP_CARD_TYPE){
				accountIdsWithCards.put(card.Loyalty_Account__c, card);
                cardsToDelete.add(card);
			}
		}

        for (Loyalty_Account__c a : [Select Id,Name, Membership_Number__c, Customer_Loyalty_Account_Card_ID__c,MyJL_Old_Membership_Number__c,MyJL_Old_Barcode__c From Loyalty_Account__c Where id IN :accountIdsWithCards.keySet()]) {
            Loyalty_Card__c card = accountIdsWithCards.get(a.id);
	        if(card != null){
				a.Membership_Number__c = a.MyJL_Old_Membership_Number__c;
				a.Customer_Loyalty_Account_Card_ID__c =a.MyJL_Old_Barcode__c;
		        a.Name = a.MyJL_Old_Membership_Number__c;
				accountsToUpdate.add(a);
			}
		}        
        
		if(!accountsToUpdate.isEmpty()){
			try{
				Database.update(accountsToUpdate);
			}
			catch(Exception e){ //something else happened
				System.debug(e.getMessage());
			}
		}   
        
    }
    
    
    

    // MJL-1570 - Check whether migration in-progress and check source last modification    
    private static void copyLC(Loyalty_Card__c oldLA, Loyalty_Card__c newLA) {
	   	newLA.Card_Number__c = oldLA.Card_Number__c;
		newLA.Extract_ConcatReference__c = oldLA.Extract_ConcatReference__c;
		newLA.Source_LastModifiedDate__c = oldLA.Source_LastModifiedDate__c;
    }	
}