/* Description  : Delete Customer records as per given criteria in COPT-4283
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   29/03/2019        Vignesh Kotha                   Created                COPT-4283
*
*/
@isTest
public class BAT_DeletePrimaryCustomerTest {
    static testmethod void testDeleteCustomerData(){        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        // Running user Context
        System.runAs(runningUser){
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;       
            insert CustomSettingTestDataFactory.createConfigSettings();
        }        
        contact testContact = CustomerTestDataFactory.createContact();
        testContact.Email = 'test@gmail.com';
        insert testContact;     
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.GDPR_Class_Name__c = 'BAT_DeletePrimaryCustomer';
        gDPR.Name = 'BAT_DeletePrimaryCustomer';
        gDPR.Record_Limit__c = '5000';
        GDPR_Record_Limit__c gDPR1 = new GDPR_Record_Limit__c();
        gDPR1.GDPR_Class_Name__c = 'BAT_DeleteTransactionHeaders';
        gDPR1.Name = 'BAT_DeleteTransactionHeaders';
        gDPR1.Record_Limit__c = '5000'; 
        GDPR_Record_Limit__c gDPR2 = new GDPR_Record_Limit__c();
        gDPR2.GDPR_Class_Name__c = 'BAT_DeleteBulkLoyaltyAccount';
        gDPR2.Name = 'BAT_DeleteBulkLoyaltyAccount';
        gDPR2.Record_Limit__c = '5000';
        Test.startTest();
        Insert gDPR;
        Insert gDPR1;
        Insert gDPR2;
        List<contact> ConList=new  List<contact>();
        for(integer i=0; i<99; i++){
            ConList.add(new contact(FirstName='test',LastName='test1',Email = 'Test@gmail.com'));
        }       
        insert ConList;
        task tas = new task();
        tas.subject= 'Email:hii';
        insert tas;
        delete tas;
        Database.BatchableContext BC;
        BAT_DeletePrimaryCustomer obj=new BAT_DeletePrimaryCustomer();
        obj.start(BC);
        obj.execute(BC,ConList);
        obj.finish(BC);
        DmlException expectedException;
        Test.stopTest();
    }
       static testmethod void testDeletePrimaryCustomerData(){        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        // Running user Context
        System.runAs(runningUser){
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;       
            insert CustomSettingTestDataFactory.createConfigSettings();
        }        
        contact testContact = CustomerTestDataFactory.createContact();
        testContact.Email = 'Tobepurged@gmail.com';
        insert testContact; 
		GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.GDPR_Class_Name__c = 'BAT_DeletePrimaryCustomer';
        gDPR.Name = 'BAT_DeletePrimaryCustomer';
        gDPR.Record_Limit__c = '5000'; 
        GDPR_Record_Limit__c gDPR1 = new GDPR_Record_Limit__c();
        gDPR1.GDPR_Class_Name__c = 'BAT_DeleteTransactionHeaders';
        gDPR1.Name = 'BAT_DeleteTransactionHeaders';
        gDPR1.Record_Limit__c = '5000'; 
        GDPR_Record_Limit__c gDPR2 = new GDPR_Record_Limit__c();
        gDPR2.GDPR_Class_Name__c = 'BAT_DeleteBulkLoyaltyAccount';
        gDPR2.Name = 'BAT_DeleteBulkLoyaltyAccount';
        gDPR2.Record_Limit__c = '5000';
        Test.startTest();
        Insert gDPR;
        Insert gDPR1;
        Insert gDPR2;
        List<contact> ConList=new  List<contact>();
        for(integer i=0; i<99; i++){
            ConList.add(new contact(FirstName='test',LastName='test1',Email = 'Tobepurged@gmail.com'));
        }       
        insert ConList;
        task tas = new task();
        tas.subject= 'Email:hii';
        insert tas;
        delete tas;
        Database.BatchableContext BC;
        BAT_DeletePrimaryCustomer obj=new BAT_DeletePrimaryCustomer();
        obj.start(BC);
        obj.execute(BC,ConList);
        obj.finish(BC);
        DmlException expectedException;
        Test.stopTest();
    }
}