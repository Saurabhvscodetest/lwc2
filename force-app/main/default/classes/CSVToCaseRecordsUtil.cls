public class CSVToCaseRecordsUtil {    
    
    //CSV File To Cases
    public static void createCSVToCaseRecords(Blob csvFileBody, Map<String,sObject> mapOfCustomSetting, Id RecordTypeId, String csvFileName, String emailSubject, String Origin) {
        
        try{
            //Parsing CSV Columns name
            String csvAsString = csvFileBody.toString();
            String [] csvFileLines = csvAsString.split('\n'); 
            String[] csvRecordColumn = csvFileLines[0].split(',');
            System.debug('*** Total Column CSV Found *** '+csvRecordColumn.size());
            System.debug('*** CSV Column Name *** '+csvRecordColumn);
            
            //Checking Columns name present in custom setting
            List<String> lstOfFieldName = new List<String>();
            Set<Integer> setOfFieldIndexNA = new Set<Integer>();
            Google_Sheet_Duplicate_Field__mdt[] googleSheetSettings = [SELECT  Email_Subject__c FROM Google_Sheet_Duplicate_Field__mdt Where Email_Subject__c = :emailSubject Limit 1];
            //System.debug('csvRecordColumn.size()'+csvRecordColumn.size());
            if(!csvRecordColumn.isEmpty()){
                for(Integer i=0; i < csvRecordColumn.size(); i++){
                    if(mapOfCustomSetting.containsKey(csvRecordColumn[i].trim())){
                        lstOfFieldName.add(csvRecordColumn[i].trim());
                    }
                    //Added for BackOrder Exception as Custom Setting Name value exceeding the limit
                    else if(csvRecordColumn[i].trim() == 'Expected Delivery Date to CDH/Customer'){
                        lstOfFieldName.add('Expected Delivery Date to CDH Customer');
                    }
                    else{
                        lstOfFieldName.add(null);
                        setOfFieldIndexNA.add(i);
                    }        
                }
                System.debug('*** Column Mapped with Custom Setting Fields *** '+lstOfFieldName);
                System.debug('*** CSV Column Index Not Mapped with Custom Setting Fields *** '+setOfFieldIndexNA);
                
                
                //Iterate CSV file lines
                List<SObject> sObjectList = new List<SObject>();
                Set<String> setOfCRNumber = new Set<String>(); // Added for COPT - 5940
                Set<String> setOfOrderNumber = new Set<String>(); // Added for COPT - 6004
                Set<String> setOfOtherRefNumber = new Set<String>(); // Added for COPT - 6004
                Set<String> setOfDeliveryNumber = new Set<String>(); // Added for COPT - 6004
                boolean isCommaFlag = false;
                if(csvRecordColumn.size() != setOfFieldIndexNA.size()){
                    for(Integer i=1; i < csvFileLines.size(); i++){
                        SObject sObj = new Case() ;
                        String[] csvRecordData = csvFileLines[i].split(',');
                        if(csvRecordData.size() == csvRecordColumn.size()){
                            for(Integer j=0; j < csvRecordData.size(); j++){
                                if(!setOfFieldIndexNA.contains(j)){
                                    if(csvRecordData[j].trim() != null){
                                        sObject obj = mapOfCustomSetting.get(lstOfFieldName[j]);
                                        
                                        if(obj!=null && obj.get('Feild_Name__c') != null){
                                            if(obj.get('Date__c') == true)
                                                sObj.put(string.valueOf(obj.get('Feild_Name__c')), date.parse(csvRecordData[j].trim()));
                                            else if(obj.get('Decimal__c') == true)
                                                sObj.put(string.valueOf(obj.get('Feild_Name__c')), decimal.valueOf(csvRecordData[j].trim()));                                            
                                            else if(String.isNotEmpty((String)sObj.get((string)obj.get('Feild_Name__c')))) // Added for COPT - 5940
                                                sObj.put(string.valueOf(obj.get('Feild_Name__c')), (String)sObj.get((string)obj.get('Feild_Name__c')) + ' - ' + csvRecordData[j].trim());
                                            else
                                                sObj.put(string.valueOf(obj.get('Feild_Name__c')), csvRecordData[j].trim());
                                        }
                                    }
                                }
                            }
                            sObj.put('RecordTypeId',RecordTypeId) ;
                            sObj.put('Origin', Origin) ;
                            sObj.put('Subject', emailSubject);
                            //COPT-6004,COPT-6005
                           
                            // Added for COPT - 5940
                            if(String.isNotEmpty((String)sObj.get('jl_CRNumber__c'))) setOfCRNumber.add((String)sObj.get('jl_CRNumber__c'));
                            //Added for COPT -6004-Start
                            if(String.isNotEmpty((String)sObj.get('jl_OrderManagementNumber__c'))) setOfOrderNumber.add((String)sObj.get('jl_OrderManagementNumber__c'));
                            if(String.isNotEmpty((String)sObj.get('jl_DeliveryNumber__c'))) setOfDeliveryNumber.add((String)sObj.get('jl_DeliveryNumber__c'));
                            if(String.isNotEmpty((String)sObj.get('jl_OtherSystemRef__c'))) setOfOtherRefNumber.add((String)sObj.get('jl_OtherSystemRef__c'));
                            //Added for COPT -6004-End
                            sObjectList.add(sObj);  
                        }else{
                            isCommaFlag = true;
                            system.debug('*******Comma Delimeter present in column values, Please check Line number :'+i);
                        }
                    } 
                    
                    
                    //Check if google sheet is eligible for duplicate check on CR Number,Order Number,Delivery Number and Other reference Number
                    if (googleSheetSettings.size()>0){
                        //COPT-5975- End
                        List<Case> listOfCases = [Select Id, jl_CRNumber__c,jl_OrderManagementNumber__c,jl_DeliveryNumber__c,jl_OtherSystemRef__c from Case where Subject = :emailSubject AND IsClosed  = false AND (jl_CRNumber__c IN: setOfCRNumber OR jl_OrderManagementNumber__c IN :setOfOrderNumber OR jl_DeliveryNumber__c IN :setOfDeliveryNumber OR jl_OtherSystemRef__c IN :setOfOtherRefNumber)];
                        Set<String> setOfDuplicateCRNumbers = new Set<String>();
                        //COPT-6004
                        Set<String> setOfDuplicateOrderNumbers = new Set<String>();
                        Set<String> setOfDuplicateOtherRefNumbers = new Set<String>();
                        Set<String> setOfDuplicateDeliveryNumbers = new Set<String>();
                        
                        for(Case c: listOfCases){
                            if(String.isNotEmpty(c.jl_CRNumber__c))setOfDuplicateCRNumbers.add(c.jl_CRNumber__c);
                            if(String.isNotEmpty(c.jl_OrderManagementNumber__c))setOfDuplicateOrderNumbers.add(c.jl_OrderManagementNumber__c);
                            if(String.isNotEmpty(c.jl_OtherSystemRef__c))setOfDuplicateOtherRefNumbers.add(c.jl_OtherSystemRef__c);
                            if(String.isNotEmpty(c.jl_DeliveryNumber__c))setOfDuplicateDeliveryNumbers.add(c.jl_DeliveryNumber__c);
                        }
                        integer i = 0;
                        while (i < sObjectList.size()){                           
                            if(setOfDuplicateCRNumbers.contains((String)sObjectList[i].get('jl_CRNumber__c'))
                               //COPT-6004
                              || setOfDuplicateOrderNumbers.contains((String)sObjectList[i].get('jl_OrderManagementNumber__c'))
                              || setOfDuplicateOtherRefNumbers.contains((String)sObjectList[i].get('jl_OtherSystemRef__c'))
                               || setOfDuplicateDeliveryNumbers.contains((String)sObjectList[i].get('jl_DeliveryNumber__c'))){
                                sObjectList.remove(i); 
                               }
                            else{
                                i++; 
                            }
                        }
                         integer j = 0;
                        Set<String> crNumberSet = new Set<String>(); // Added for COPT - 5940
                Set<String> orderNumberSet = new Set<String>(); // Added for COPT - 6990
                Set<String> otherRefNumberSet = new Set<String>(); // Added for COPT - 6990
                Set<String> deliveryNumberSet = new Set<String>(); // Added for COPT - 6990
                        
                         while (j < sObjectList.size()){ 

                             if(j==0){
                                  if(String.isNotEmpty((String)sObjectList[j].get('jl_CRNumber__c'))) crNumberSet.add((String)sObjectList[j].get('jl_CRNumber__c'));
                                if(String.isNotEmpty((String)sObjectList[j].get('jl_OrderManagementNumber__c'))) orderNumberSet.add((String)sObjectList[j].get('jl_OrderManagementNumber__c'));
                                if(String.isNotEmpty((String)sObjectList[j].get('jl_DeliveryNumber__c'))) deliveryNumberSet.add((String)sObjectList[j].get('jl_DeliveryNumber__c'));
                                if(String.isNotEmpty((String)sObjectList[j].get('jl_OtherSystemRef__c'))) otherRefNumberSet.add((String)sObjectList[j].get('jl_OtherSystemRef__c'));
                                 j++;
                             }
                             
                            else if(crNumberSet.contains((String)sObjectList[j].get('jl_CRNumber__c'))
                               || orderNumberSet.contains((String)sObjectList[j].get('jl_OrderManagementNumber__c'))
                               || otherRefNumberSet.contains((String)sObjectList[j].get('jl_OtherSystemRef__c'))
                               || deliveryNumberSet.contains((String)sObjectList[j].get('jl_DeliveryNumber__c'))){
                                   sObjectList.remove(j); 
                                  // j++;
                               }
                            else{
                                 if(String.isNotEmpty((String)sObjectList[j].get('jl_CRNumber__c'))) crNumberSet.add((String)sObjectList[j].get('jl_CRNumber__c'));
                                //Added for COPT -6990-Start
                                if(String.isNotEmpty((String)sObjectList[j].get('jl_OrderManagementNumber__c'))) orderNumberSet.add((String)sObjectList[j].get('jl_OrderManagementNumber__c'));
                                if(String.isNotEmpty((String)sObjectList[j].get('jl_DeliveryNumber__c'))) deliveryNumberSet.add((String)sObjectList[j].get('jl_DeliveryNumber__c'));
                                if(String.isNotEmpty((String)sObjectList[j].get('jl_OtherSystemRef__c'))) otherRefNumberSet.add((String)sObjectList[j].get('jl_OtherSystemRef__c'));
                                j++;
                            }
                             
                        }
                         System.debug('After Google sheet duplicates removal');
                        System.debug(sObjectList);
                        System.debug('setOfCRNumber'+crNumberSet);
                        System.debug('setOfOrderNumber'+orderNumberSet);
                        System.debug('setOfDeliveryNumber'+otherRefNumberSet);
                        System.debug('setOfOtherRefNumber'+deliveryNumberSet);
                        
                        
                       /* while (i < sObjectList.size()){
                                if((!(setOfCRNumber.size()>0 && setOfCRNumber.contains((String)sObj.get('jl_CRNumber__c'))))
                                   ||(!(setOfOrderNumber.size()>0 && setOfOrderNumber.contains((String)sObj.get('jl_OrderManagementNumber__c'))))
                                   ||(!(setOfDeliveryNumber.size()>0 && setOfDeliveryNumber.contains((String)sObj.get('jl_DeliveryNumber__c'))))
                                   ||(!(setOfOtherRefNumber.size()>0 && setOfOtherRefNumber.contains((String)sObj.get('jl_OtherSystemRef__c')))))
                                    sObjectList.add(sObj);  
                               // if(setOfCRNumber.isEmpty() && setOfOrderNumber.isEmpty() && setOfDeliveryNumber.isEmpty() && setOfOtherRefNumber.isEmpty())sObjectList.add(sObj);
                                i++
                            }
                            else{
                                sObjectList.add(sObj);
                            }
                    }*/
                    /* END - Added for COPT - 5940 */
                    }
                    system.debug('***sObject List*** '+sObjectList.size());
                    
                    //Checking sObject List before Processing
                    if(sObjectList.size()>0 && isCommaFlag == false){
                        //BackOrder conditions check to process only the qualify records
                        if(csvFileName.containsIgnoreCase('BackOrders')){                                   
                            Integer j = 0;
                            Set<String> setOfDistId = new Set<String>();
                            List<String> lstSuppliersName = new List<String>();
                            //Custom Setting for Supplier Name
                            BackOrders_SuppliersName__c customSettingObj = BackOrders_SuppliersName__c.getValues('Supplier Name');
                            if(customSettingObj != null && customSettingObj.value__c != null)
                                lstSuppliersName = customSettingObj.value__c.split(',');
                            
                            while (j < sObjectList.size()){
                                sObject sObj = sObjectList[j];
                                if(lstSuppliersName.contains(string.valueOf(sObj.get('Supplier_Name__c')))){
                                    if(setOfDistId.contains(string.valueOf(sObj.get('Disti_Ref__c'))))
                                        sObjectList.remove(j); 
                                    else{
                                        setOfDistId.add(string.valueOf(sObj.get('Disti_Ref__c')));
                                        j++;
                                    }
                                }
                                else
                                    sObjectList.remove(j);
                            }
                            system.debug('***sObject List Back Orders*** '+sObjectList.size());         
                        }
                        
                        //Inserting Records in Case Objects 
                        Database.SaveResult[] saveResultList = Database.insert(sObjectList, false);
                        String messageBody = '';
                        Integer successCount = 0;
                        Integer failCount = 0;
                        for (Database.SaveResult sr : saveResultList) {
                            if (sr.isSuccess()) {                              
                                System.debug('Successfully inserted Record. Record ID: ' + sr.getId()); 
                                successCount++;
                            } else {
                                for(Database.Error objErr : sr.getErrors()) {
                                    messageBody += 'Status Code : '+ objErr.getStatusCode();
                                    messageBody += '<br/>Message : ' + objErr.getMessage();
                                    messageBody += '<br/>Object field which are affected by the error: ' + objErr.getFields()+'<br/><br/>';
                                    failCount++;
                                }
                            }
                        }
                        system.debug('***Error Message*** '+messageBody);
                        system.debug('***SuccessCount: '+successCount+ '***FailCount: '+failCount);
                        if(failCount>0)
                            sendEmail(csvFileBody, csvFileName, messageBody, successCount, failCount);
                    }
                    else{
                        sendEmail(csvFileBody, csvFileName,'An error has occured while processing the CSV file. Comma Delimeter present in row values, Please remove and re-processes the file.<br/>', 0, 0);
                    } 
                }
            }
        }
        catch (Exception e)
        {
            sendEmail(csvFileBody, csvFileName,'An Exception has occured while processing the CSV file. Please refer the error message. <br/> Error Message: '+e.getMessage()+'<br/><br/>', 0, 0);
            system.debug('Exception Message **** : '+e);
        }
    }
    
    //Email Body To Case 
    public static void createEmailBodyToCaseRecord(Messaging.InboundEmail email, Map<String,sObject> mapOfCustomSetting, Id QueryRecordTypeId, Id ComplaintRecordTypeId, String emailSubject) {
        
        String EmailTextBody = email.plainTextBody;
        try{
            If(String.isNotEmpty(EmailTextBody)){ 
                SObject sObjCase = new Case();
                for(sObject sObj : mapOfCustomSetting.values()){
                    system.debug('*** Custom Setting Column Name ***'+string.valueOf(sObj.get('Name')));
                    Integer columnNameIndex = EmailTextBody.indexOfIgnoreCase(string.valueOf(sObj.get('Name')));
                    Integer columnNameLength = string.valueOf(sObj.get('Name')).length();
                    System.debug('columnNameIndex***'+columnNameIndex);
                    System.debug('columnNameLength***'+columnNameLength);
                    
                    if(columnNameIndex >= 0){
                        String columnDataValue = EmailTextBody.substring(columnNameIndex + columnNameLength, EmailTextBody.indexOf('\n', columnNameIndex + columnNameLength));
                        system.debug('******ColumnDataValue*******'+columnDataValue.trim());
                        if(string.valueOf(sObj.get('Feild_Name__c')) != null && String.isNotBlank(columnDataValue)){
                            if(sObj.get('Date__c') == true)
                                sObjCase.put(string.valueOf(sObj.get('Feild_Name__c')), date.parse(columnDataValue.trim()));
                            else if(sObj.get('Decimal__c') == true)
                                sObj.put(string.valueOf(sObj.get('Feild_Name__c')), decimal.valueOf(columnDataValue.trim()));
                            else{
                                //Case Type Based on Value
                                if(columnDataValue.trim().equalsIgnoreCase('Query')){
                                    sObjCase.put('RecordTypeId',QueryRecordTypeId);
                                    sObjCase.put('Origin','MP3 Queries');
                                }
                                else if(columnDataValue.trim().equalsIgnoreCase('Exception')){
                                    sObjCase.put('RecordTypeId',ComplaintRecordTypeId);
                                    sObjCase.put('Origin','MP3 Exceptions');
                                }
                                else
                                    sObjCase.put(string.valueOf(sObj.get('Feild_Name__c')),columnDataValue.trim());
                            }
                        }
                    }
                }
                sObjCase.put('Subject', emailSubject);
                System.debug('***Case Object Before Inserting***'+sObjCase);
                insert sObjCase;
                System.debug('Case Created Id****'+((Case)sObjCase).Id);
                //Attaching Email to created Case
                EmailMessage emailMsgObj = new EmailMessage();
                emailMsgObj.ParentId = ((Case)sObjCase).Id;
                emailMsgObj.Subject = email.subject;
                emailMsgObj.FromAddress = email.fromAddress;
                emailMsgObj.HtmlBody = email.htmlBody;
                emailMsgObj.Incoming = True;
                emailMsgObj.FromAddress = email.fromAddress;
                emailMsgObj.FromName = email.fromName;
                emailMsgObj.ToAddress = email.toAddresses[0];
                insert emailMsgObj;
                System.debug('Email Message Id****'+emailMsgObj.Id);
            }
        }
        catch (Exception e){
            sendEmail(null, '','An Exception has occured while processing the email. Please refer the error message. <br/> Error Message: '+e.getMessage()+'<br/><br/>', 0, 0);
            system.debug('Exception Message **** : '+e);
        }
    }
    
    //Method to send an email on failure/Exceptions
    public static void sendEmail(blob csvFileBody, string csvFileName, string msgbody, Integer successCount, Integer failCount){
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        IFC_ErrorEmail__c obj = IFC_ErrorEmail__c.getvalues('Error Email Id');
        if(obj != null && obj.value__c != null){
            String[] strAddresses = obj.value__c.split(',');
            mail.setToAddresses(strAddresses);
            mail.setReplyTo('noreplay@Connex.com');
            mail.setSenderDisplayName('Connex Application');  
            mail.setSubject('Google Sheet - Exception Messages');
            String emailbody = 'Dear Team,<br/><br/>';
            emailbody += 'Following errors or exceptions has been occured while processing the email.<br/><br/>';
            emailbody += msgbody;
            emailbody += 'Number of record successfully inserted :'+successCount+'<br/>';
            emailbody += 'Number of record failed due to error :'+failCount+'<br/>';
            emailbody += '<br/><br/> Regards, <br/> Connex Team';
            mail.setHtmlBody(emailbody);
            //Attaching CSV File
            if(csvFileBody !=null){
                Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
                attach.Body = csvFileBody;
                attach.setFileName(csvFileName);
                attach.setContentType('text/csv');
                mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach });
            }
            //Send email
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
}