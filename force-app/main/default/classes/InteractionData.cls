/******************************************************************************
* @author       Krish Gopal/Shion Abdillah/Matt Povey
* @date			05/05/2015
* @description	Class that handles Interaction__c record updates from Case or Email
*				trigger handler classes.
*
* EDIT RECORD
*
* LOG	DATE		Author	JIRA		COMMENT      
* 001	03/06/2015  MP		CMP-121		Refactored for additional scenarios
* 002	24/06/2015	MP		CMP-121		Add debug method and change 'processedCases' so only closed/reopened/transferred cases are added
*										Also amend interaction creation methods to only create them for open cases
* 003	06/11/2015	MP		COPT-438	Refactor to reduce number of SOQLs.
* 004	21/01/2016	MP		COPT-528	Stamp Case Status on the Interaction record for Emails.
* 005	02/03/2016	SA		COPT-568	Adding Contact Customer interaction type.
* 006									Reduced SOQL statements in private functions by using a master collection for Interactions.
* 007									Added closeContactInteractionRecord for closing off contact records for emails.
* 008	17/03/2016	MP		COPT-568	Refactoring to remove repeated 'for' loops.
* 009	01/04/2016	MP		COPT-683	Changes to allow external access to closeCaseInteractions and buildUserInfoMap.
* 010	15/06/2016	MP		COPT-840	Bug fix where closing ownership interactions was updating ALL ownership interactions linked to case.
* 011	20/07/2016	MP		COPT-930	Bug fix for closing Email Interactions not overwriting agent details.
* 012	25/07/2016	MP		COPT-949	Bug fix for status updates on close interactions.
*
*******************************************************************************
*/
public without sharing class InteractionData {
	@TestVisible private static final String CASE_OVERALL_PERFORMANCE = 'Case';
	@TestVisible private static final String CASE_QUEUE_TIME = 'Queue';
	@TestVisible private static final String AGENT_CASE_HANDLING_TIME = 'Agent';
	@TestVisible private static final String OVERALL_EMAIL_HANDLING_PERFORMANCE = 'Email';
	@TestVisible private static final String CONTACT_CUSTOMER = 'Contact';
	@TestVisible private static Set<Id> processedCases = new Set<Id>();
	@TestVisible private static Map<Id, Interaction__c> caseInteractionXref = new Map<Id, Interaction__c>();
	private static final Id EMAIL_TO_CASE_USER_ID = Config_Settings__c.getInstance('EMAIL_TO_CASE_USER_ID').Text_Value__c;
	private static final set<String> CASE_OWNERSHIP_INTERACTION_TYPES = new set<String> {AGENT_CASE_HANDLING_TIME, CASE_QUEUE_TIME};
	public static final Set<String> EMAIL_CONTACT_INTERACTION_TYPES = new set<String> {OVERALL_EMAIL_HANDLING_PERFORMANCE, CONTACT_CUSTOMER};
	private static Boolean interactionXrefValuesUpdated = false;
	private static Integer executionCount = 0; // <<02>> Execution counter for debug purposes
	private static Map<Id, User> mapUserInfo = new Map<Id, User>(); // <<003>>
	// private static Map<Id, QueueSObject> mapQueueInfo = new Map<Id, QueueSObject>(); // <<003>>
	//<005>
    private static final Map<String, String> caseActionTakenForContactInteraction = new Map<String, String> {'CASE RESOLVED' => 'CASE RESOLVED',
    																										 'CUSTOMER CONTACTED' => 'CUSTOMER CONTACTED',
    																										 'INVALID CONTACT NUMBER'=> 'INVALID CONTACT NUMBER',
    																										 'MESSAGE LEFT FOR CUSTOMER'=> 'MESSAGE LEFT FOR CUSTOMER',
    																										 'NO ANSWER ON CONTACT NUMBER'=> 'NO ANSWER ON CONTACT NUMBER',
    																										 'NO CUSTOMER CONTACT REQUIRED' => 'NO CUSTOMER CONTACT REQUIRED'}; //use this collection by .toUpperCase() string method when finding values
    //</005>	    	

    /**
	 * Single method to handle Case Inserts called from clsCaseTriggerHandler
	 * @params:	List<Case> newCases			List of Cases from Trigger.new
 	 * @rtnval:	void
     */
    public static void handleCaseInserts(List<Case> newCases, Map<Id, QueueSObject> allQueuesMap) {
		executionCount++;
		buildUserInfoMap(newCases); // <<003>>
		Map<Id, QueueSObject> mapQueueInfo = getSpecificQueuesMap(newCases, allQueuesMap);
    	List<Interaction__c> interactionInsertList = new List<Interaction__c>();
    	
    	for (Case caseRec : newCases) {
    		addCreateCaseInteractions(caseRec, interactionInsertList, true);
    		addNewOwnerInteractions(caseRec, mapQueueInfo, interactionInsertList, true);
    		processedCases.add(caseRec.Id);
    	}

		outputDebugInfo('interactionInsertList:' + interactionInsertList); 

    	if (!interactionInsertList.isEmpty()) {
    		Database.insert(interactionInsertList,false);
    	}
    	outputDebugInfo('handleCaseInserts'); // <<02>>
    }
    
    /**
     * Single method to handle Case Updates
	 * @params:	Map<Id, Case> oldMap		Map of Cases from Trigger.oldMap
	 *			Map<Id, Case> newMap		Map of Cases from Trigger.newMap
 	 * @rtnval:	void
     */
    public static void handleCaseUpdates(Map<Id, Case> oldMap, Map<Id, Case> newMap, Map<Id, QueueSObject> allQueuesMap) {
		executionCount++;
		buildUserInfoMap(newMap.values()); // <<003>>
		Map<Id, QueueSObject> mapQueueInfo = getSpecificQueuesMap(newMap.values(), allQueuesMap);

		Set<Id> masterCaseIdSet = new Set<Id>();	
    	Set<Id> casesToClose = new Set<Id>();
    	Set<Id> casesToReopen = new Set<Id>();
    	Set<Id> casesToTransfer = new Set<Id>();
    	Set<Id> casesWithContactComplete = new Set<Id>();
    	Map<Id, Interaction__c> interactionUpdateMap = new Map<Id, Interaction__c>();		
    	List<Interaction__c> interactionUpsertList = new List<Interaction__c>();
		
		for (Case newCase : newMap.values()) {
			Case oldCase = oldMap.get(newCase.Id);

			// Closed case
			if (!oldCase.isClosed && newCase.isClosed && !processedCases.contains(newCase.Id)) {
				casesToClose.add(newCase.Id);								
				system.debug('--Added to casesToClose collection.');
				masterCaseIdSet.add(newCase.Id);
			// Reopened case
			} else if (oldCase.isClosed && !newCase.isClosed && !processedCases.contains(newCase.Id)) {
				casesToReopen.add(newCase.Id);
				system.debug('--Added to casesToReopen collection.');
				masterCaseIdSet.add(newCase.Id);
			} else {
				// Case transferred
				if (!newCase.isClosed && oldCase.OwnerId != newCase.OwnerId) {
					casesToTransfer.add(newCase.Id);
					system.debug('--Added to casesToTransfer collection.');
					masterCaseIdSet.add(newCase.Id);
				}
				// Customer contact completed
				if (!casesToClose.contains(newCase.Id) && oldCase.jl_Action_Taken__c  != newCase.jl_Action_Taken__c && caseActionTakenForContactInteraction.containsKey(newCase.jl_Action_Taken__c.toUpperCase())  && !processedCases.contains(newCase.Id)) {								
        	        casesWithContactComplete.add(newCase.Id);
					system.debug('--Added to casesWithContactComplete collection.');
					masterCaseIdSet.add(newCase.Id);
				}			
			}
		}
		
		system.debug('masterCaseIdSet: ' + masterCaseIdSet);
		
		if (!masterCaseIdSet.isEmpty()) {
			Map<Id, Case> casesWithInteractionsMap = new Map<Id, Case>([SELECT Id, (SELECT Id, End_DT__c, Event_Type__c, Start_Dt__c, Team_Name__c, Agent__c, Agent_Manager__c, Case__c, Case_Status__c, Case__r.Status
    	    																		FROM Interaction__r)
        																FROM Case WHERE Id IN :masterCaseIdSet]); 																																		

			for (Case caseWithInteractions : casesWithInteractionsMap.values()) {
				Case caseRec = newMap.get(caseWithInteractions.Id);
				if (casesToClose.contains(caseRec.Id)) {
					// Closed case - close all open interactions
					closeCaseInteractions(caseRec, interactionUpdateMap, caseWithInteractions.Interaction__r);
				} else if (casesToReopen.contains(caseRec.Id)) {
					// Reopened case - reopen case interaction, create new contact and owner interactions
					reopenCaseInteractions(caseRec, mapQueueInfo, interactionUpdateMap, interactionUpsertList, caseWithInteractions.Interaction__r);
				} else {
					// Transfer or closed contact - update appropriate linked interactions
					Boolean isTransfer = casesToTransfer.contains(caseRec.Id);
					Boolean isContactComplete = casesWithContactComplete.contains(caseRec.Id);
					if (isTransfer || isContactComplete) {
						transferAndCloseContactInteractions(caseRec, mapQueueInfo, interactionUpdateMap, interactionUpsertList, caseWithInteractions.Interaction__r, isTransfer, isContactComplete);
					}
				}
			}
		}

		if (!interactionUpdateMap.isEmpty()) {
			interactionUpsertList.addAll(interactionUpdateMap.values());
		}
		
    	if (!interactionUpsertList.isEmpty()) {
    		Database.upsert(interactionUpsertList,false);
    	}
    	
    	if (interactionXrefValuesUpdated) {
    		Database.update(caseInteractionXref.values(),false);
    		interactionXrefValuesUpdated = false;
    	}
    	
    	// <<02>> changed from 'for' loop
    	outputDebugInfo('handleCaseUpdates'); // <<02>>
    	processedCases.addAll(casesToClose);
    	processedCases.addAll(casesToReopen);
    	processedCases.addAll(casesToTransfer);		
    	// REMOVED AS THIS PREVENTS UPDATES ON CASE CLOSURE processedCases.addAll(casesWithContactComplete);		
    }
    
    /**
	 * Create Case Overall Performance and Contact interactions for a newly created / reopened Case
	 * @params:	Case caseRec						The current case being created / reopened
	 *			List<Interaction__c> intList	The list of Interactions to be upserted at the end of the insert/update method
	 *			Boolean useCreatedDateForContact	If true user CreatedDate from Case for Contact interaction, else use system.now()
 	 * @rtnval:	void
     */
	private static void addCreateCaseInteractions(Case caseRec, List<Interaction__c> intList, Boolean useCreatedDateForContact) {
		User objUser = mapUserInfo.get(caseRec.CreatedById) != null ? mapUserInfo.get(caseRec.CreatedById) : mapUserInfo.get(UserInfo.getUserId());
		if (!caseRec.isClosed) {
			Interaction__c caseOverallPerformanceObj = openInteractionRecord(CASE_OVERALL_PERFORMANCE, caseRec, objUser, null, caseRec.CreatedDate);				
			DateTime dateToUse;
			if (useCreatedDateForContact) {
				dateToUse = caseRec.CreatedDate;
			} else {
				dateToUse = system.now();
			}
			Interaction__c contactObj = openInteractionRecord(CONTACT_CUSTOMER, caseRec, objUser, null, dateToUse);							
			intList.add(caseOverallPerformanceObj);
			intList.add(contactObj);
		}
    }

	/**
	 * Create new interaction records where the case owner has changed or case is new
	 * @params:	Case caseRec						The current case for which the owner has changed
	 *			Map<Id, QueueSObject> mapQueueInfo	Map of Queue ids and the corresponding QueueSObject records
	 *			List<Interaction__c> intList	The list of Interactions to be upserted at the end of the insert/update method
	 *			Boolean isInsert					Denotes whether this method is being called from an insert or update operation
	 */
	private static void addNewOwnerInteractions(Case caseRec, Map<Id, QueueSObject> mapQueueInfo, List<Interaction__c> intList, Boolean isInsert) {
		// Must exclude email to case user for inserts as the trigger fires multiple times
		if (!caseRec.isClosed && !(isInsert && caseRec.OwnerId == EMAIL_TO_CASE_USER_ID)) {
			User objUser;
			QueueSObject objQueue = mapQueueInfo.get(caseRec.OwnerId) != null ? mapQueueInfo.get(caseRec.OwnerId) : null;
			String interactionType;
			
			if (CommonStaticUtils.isGroupId(caseRec.OwnerId)) {
				interactionType = CASE_QUEUE_TIME;
				objUser = mapUserInfo.get(UserInfo.getUserId());
			} else {
				interactionType = AGENT_CASE_HANDLING_TIME;
				objUser = mapUserInfo.get(caseRec.OwnerId) != null ? mapUserInfo.get(caseRec.OwnerId) : mapUserInfo.get(UserInfo.getUserId());
			}
        											
			// Need to reuse existing xref record if it's there - if it hits this code it means multiple updates
			Interaction__c objInt;
			if (caseInteractionXref.containsKey(caseRec.Id)) {
				objInt = caseInteractionXref.get(caseRec.Id);
				objInt.Event_Type__c = interactionType;
				objInt.Name = caseRec.CaseNumber + ': ' + interactionType;
				if (interactionType == CASE_QUEUE_TIME) {
					if (objQueue != null) {
						objInt.Queue__c = objQueue.Queue.Name;
					} else {
						objInt.Queue__c = caseRec.OwnerId;
					}
				}
				addAgentInfoToInteraction(objUser, objInt);
				interactionXrefValuesUpdated = true;
			} else {
				objInt = openInteractionRecord(interactionType, caseRec, objUser, objQueue, caseRec.LastModifiedDate);
			intList.add(objInt);
				caseInteractionXref.put(caseRec.Id, objInt);       
			}                          
        }
	}
	
	/**
	 * Close all open interactions linked to a closed case.
	 * @params:	Case updatedCase								The case being closed
	 *			Map<Id, Interaction__c>	interactionUpdateMap	The map of Interactions to be upserted at the end of the insert/update method
	 *			List<Interaction__c> interactionsToClose		The collection of interactions linked to the current Case record.
	 */
	public static void closeCaseInteractions(Case updatedCase, Map<Id, Interaction__c> interactionUpdateMap, List<Interaction__c> interactionsToClose) {
		closeCaseInteractions(updatedCase.LastModifiedDate, interactionUpdateMap, interactionsToClose);
	}
        											
	/**
	 * Close all open interactions linked to a closed case.
	 * @params:	DateTime closeDateTime							The close Date Time stamp to use
	 *			Map<Id, Interaction__c>	interactionUpdateMap	The map of Interactions to be upserted at the end of the insert/update method
	 *			List<Interaction__c> interactionsToClose		The collection of interactions linked to the current Case record.
	 */
	public static void closeCaseInteractions(DateTime closeDateTime, Map<Id, Interaction__c> interactionUpdateMap, List<Interaction__c> interactionsToClose) {
		User objUser = mapUserInfo.get(UserInfo.getUserId());
		// Run though all linked interactions and close any open ones
		if (!interactionsToClose.isEmpty()) {
			for (Interaction__c objInt : interactionsToClose) {
				if (objInt.End_DT__c == null) {
					// Boolean addAgentInfo = objInt.Event_Type__c == CONTACT_CUSTOMER;
					Boolean addAgentInfo = EMAIL_CONTACT_INTERACTION_TYPES.contains(objInt.Event_Type__c); // <<011>>
					closeInteractionRecord(objInt, objUser, closeDateTime, addAgentInfo);
					interactionUpdateMap.put(objInt.Id, objInt);
				}
			}
        }
	}
	
	/**
	 * Create / update interactions linked to a reopened case.
	 * @params:	Case updatedCase								The case being reopened
	 *			Map<Id, QueueSObject> mapQueueInfo	Map of Queue ids and the corresponding QueueSObject records
	 *			Map<Id, Interaction__c>	interactionUpdateMap	The map of Interactions to be updated at the end of the update method (for updates only - add these to the upsert list later)
	 *			List<Interaction__c> interactionUpsertList			The list of Interactions to be upserted at the end of the update method (will initially contain only new records)
	 *			List<Interaction__c> interactionsToReopen		The collection of interactions linked to the current Case record.
	 */
	private static void reopenCaseInteractions(Case updatedCase, Map<Id, QueueSObject> mapQueueInfo, Map<Id, Interaction__c> interactionUpdateMap, List<Interaction__c> interactionUpsertList, List<Interaction__c> interactionsToReopen) {
		User objUser = mapUserInfo.get(UserInfo.getUserId());
		Boolean interactionReopened = false;
		
		// Find existing 'Case' interaction - should always only be one.  If we find it, reopen it.
		if (!interactionsToReopen.isEmpty()) {
			for (Interaction__c objInt : interactionsToReopen) {
				if (objInt.Event_Type__c == CASE_OVERALL_PERFORMANCE) {
					objInt.End_DT__c = null;
					interactionUpdateMap.put(objInt.Id, objInt);
					interactionReopened = true;
					break;
				}
			}
		}
		
		if (!interactionReopened) {
    		// If no existing 'Case' interaction reopened, treat as new case
    		addCreateCaseInteractions(updatedCase, interactionUpsertList, false);
		} else {
			// Need to open a new Customer Contact interaction
			Interaction__c contactObj = openInteractionRecord(CONTACT_CUSTOMER, updatedCase, objUser, null, system.now());
			interactionUpsertList.add(contactObj);							
		}
    	
    	// Create an owner interaction for the current Case owner
    	addNewOwnerInteractions(updatedCase, mapQueueInfo, interactionUpsertList, false);
	}
	
	/**
	 * Create / update interactions linked to a change in case owner and/or close contact status.
	 * @params:	Case updatedCase								The case being transferred
	 *			Map<Id, QueueSObject> mapQueueInfo				Map of Queue ids and the corresponding QueueSObject records
	 *			Map<Id, Interaction__c>	interactionUpdateMap	The map of Interactions to be updated at the end of the update method (for updates only - add these to the upsert list later)
	 *			List<Interaction__c> interactionUpsertList			The list of Interactions to be upserted at the end of the update method (will initially contain only new records)
	 *			List<Interaction__c> interactionsToClose		The collection of interactions linked to the current Case record.
	 *			Boolean isTransfer								This case is being transferred to a new owner.
	 *			Boolean isContactComplete						This case has been moved to contact complete status.
	 */
	private static void transferAndCloseContactInteractions(Case updatedCase, Map<Id, QueueSObject> mapQueueInfo, Map<Id, Interaction__c> interactionUpdateMap, List<Interaction__c> interactionUpsertList, List<Interaction__c> interactionsToClose, Boolean isTransfer, Boolean isContactComplete) {
		User objUser = mapUserInfo.get(UserInfo.getUserId());
		
		for(Interaction__c objInt : interactionsToClose){
			// For transfers - close old owner interaction, for contact complete close any open contact interaction
			// <<010>> Fix IF statement where End_DT__c check wasn't being done for Case Ownership following 16.5 release.
			if (objInt.End_DT__c == null && 
					((isTransfer && CASE_OWNERSHIP_INTERACTION_TYPES.contains(objInt.Event_Type__c) && !caseInteractionXref.keySet().contains(updatedCase.Id))
					|| isContactComplete && objInt.Event_Type__c == CONTACT_CUSTOMER)) {
				Boolean addAgentInfo = objInt.Event_Type__c == CONTACT_CUSTOMER;
				closeInteractionRecord(objInt, objUser, updatedCase.LastModifiedDate, addAgentInfo);
				interactionUpdateMap.put(objInt.Id, objInt);
			}
		}
		// For transfers, need to open a new owner interaction
		if (isTransfer) {
			addNewOwnerInteractions(updatedCase, mapQueueInfo, interactionUpsertList, false);
		}
	}
		
	/**
	 * Close all open contact interactions for Cases live agent conversations.
	 * @params:	List<LiveChatTranscript> transcriptList			The list of live chat transcripts for which contact interactions should close.
 	 * @rtnval:	void
	 */
	public static void closeContactInteractionsForLiveChat(List<LiveChatTranscript> transcriptList) {
		Set<Id> caseIdSet = new Set<Id>();
		for (LiveChatTranscript lct : transcriptList) {
			if (lct.CaseId != null) {
				caseIdSet.add(lct.CaseId);
			}
		}
		
		if (!caseIdSet.isEmpty()) {
			List<Case> linkedCaseList = [SELECT Id, Status, CaseNumber, OwnerId, jl_HasEmailOrigin__c, IsClosed, Origin, jl_Queue_Name__c, jl_Last_Queue_Name__c, LastModifiedDate, CreatedById, (SELECT End_DT__c, Event_Type__c, Case_Status__c, Case__r.Status FROM Interaction__r WHERE Event_Type__c = 'Contact' AND End_DT__c = null) FROM Case WHERE Id IN :caseIdSet];
			buildUserInfoMap(linkedCaseList);
			User objUser = mapUserInfo.get(UserInfo.getUserId());
			Map<Id, Interaction__c> interactionUpdateMap = new Map<Id, Interaction__c>();
			DateTime closeInteractionStamp = system.now();
			
			for (Case thisCase : linkedCaseList) {
				// Close contact interactions linked to this Case
				InteractionData.closeCaseInteractions(closeInteractionStamp, interactionUpdateMap, thisCase.Interaction__r);
			}
			if (!interactionUpdateMap.isEmpty()) {
				update interactionUpdateMap.values();
			}
		}
	}

    /**
     * Create email handling performance records from tasks
	 * @params:	List<Task> taskCollection		The list of Tasks for which new Email Interactions should be created.
     */
    public static void OpenOverallEmailsHandlingPerformanceRecords(List<Task> taskCollection, Map<Id, QueueSObject> allQueuesMap) {
		executionCount++;
        list<Interaction__c> lstInteractions = new List<Interaction__c>();
    	Set<Id> caseIdSet = new Set<Id>();
    	for(Task task : taskCollection) {
        	caseIdSet.add(task.WhatID);
        }
        Map<Id, Case> caseMap = new Map<Id, Case>([SELECT Id, CaseNumber, CreatedById, OwnerId, isClosed FROM Case WHERE Id IN :caseIdSet]);
        Map<Id, QueueSObject> mapQueueInfo = getSpecificQueuesMap(caseMap.values(), allQueuesMap);

    	buildUserInfoMap(taskCollection); // <<003>>
    	buildUserInfoMap(caseMap.values()); // <<003>>

    	for(Task task : taskCollection) {
          	Case caseRec = caseMap.get(task.WhatID);
          	if (!caseRec.isClosed) {
	          	Id userIdToUse = task.OwnerId != null ? task.OwnerId : UserInfo.getUserId();
    	      	QueueSObject objQueue = mapQueueInfo.get(caseRec.OwnerId) != null ? mapQueueInfo.get(caseRec.OwnerId) : null;
        	  	User objUser = mapUserInfo.get(userIdToUse);
          		Interaction__c objInt = openInteractionRecord(OVERALL_EMAIL_HANDLING_PERFORMANCE, caseRec, objUser, objQueue, DateTime.now());
	          	objInt.RelatedActivity__c = task.Id;
    	        lstInteractions.add(objInt);
          	} 
        }
        
    	outputDebugInfo('OpenOverallEmailsHandlingPerformanceRecords'); // <<02>>
        try {
          if (!lstInteractions.isEmpty()) {
	          Database.upsert(lstInteractions,false);
    	      System.debug('Overall Email Handling Performance: ' + lstInteractions);
          }
        } catch(DMLException e) {
          System.debug(e.getMessage());
        }
    }
    
	/**
	 * Close email performance interactions.  This will stamp the interaction with the closing user details
	 * <<012>> UPDATED: updated for COPT-949 bug fix.  Since COPT-690 this will only be used for Close No Response Required updates
	 * @params:	Set<Id> activityIds			The list of Task Ids for which any linked Interactions should be closed.
	 */
	public static void closeOverallEmailHandlingPerformanceRecord(Set<Id> activityIds) {      
		System.debug('Running closeOverallEmailHandlingPerformanceRecord');
		executionCount++;
		buildUserInfoMap(new Set<Id>()); // <<003>>
		User objUser = mapUserInfo.get(UserInfo.getUserId());
		DateTime closeDateToUse = system.now();

		List<Interaction__c> lstInteractions = new List<Interaction__c>();
		// <<004>> Add Case Status to SOQL
		for (Interaction__c objInt : [SELECT Id, RelatedActivity__c, End_dt__c, Start_Dt__c, Team_Name__c, Agent__c, Agent_Manager__c, Case__c, Case__r.Status FROM Interaction__c WHERE RelatedActivity__c IN :activityIds]) {
			// <<004>> Set Interaction to Closed - No Response Required if called specifically from that button on Case page
			Id relatedTaskId = objInt.RelatedActivity__c;
			objInt.Case_Status__c = 'Closed - No Response Required';
			closeInteractionRecord(objInt, objUser, closeDateToUse, true);
			lstInteractions.add(objint);
		}		
    	
    	if (!lstInteractions.isEmpty()) {
			try {
	        	Database.upsert(lstInteractions,false);
			} catch (DMLException e) {
        		System.debug(e.getMessage());
			}
    	}
    }
   
	/**
	 * Adds users to mapUserInfo for Owners and Creators of Cases <<003>>
	 * @params:	List<Case> caseList				The List of Cases to use as the base for the UserMap
	 */
	public static void buildUserInfoMap(List<Case> caseList) {
		Set<Id> setUserIds = new Set<Id>();
		
		for(Case caseRec : caseList) {
			setUserIds.add(caseRec.OwnerId);
			setUserIds.add(caseRec.CreatedById);
		}
		
		buildUserInfoMap(setUserIds);
	}
	
	/**
	 * Adds users to mapUserInfo for Task Owners <<003>>
	 * @params:	List<Task> taskList				The List of Tasks to use as the base for the UserMap
	 */
	public static void buildUserInfoMap(List<Task> taskList) {
		Set<Id> setUserIds = new Set<Id>();
		
		for(Task taskRec : taskList) {
			setUserIds.add(taskRec.OwnerId);
		}
		
		buildUserInfoMap(setUserIds);
	}
	
	/**
	 * Adds users to mapUserInfo as needed - reduces SOQLs <<003>>
	 * @params:	Set<Id> setUserIds				The Set of User Ids for which to retrive data and populate the UserMap
	 */
	private static void buildUserInfoMap(Set<Id> setUserIds) {
		setUserIds.add(UserInfo.getUserId());
		List<Id> processList = new List<Id>(setUserIds);
		Set<Id> usersToAdd = new Set<Id>();
		for (Id uid : processList) {
			if (uid != null && CommonStaticUtils.isUserId(uid) && !mapUserInfo.containsKey(uid)) {
				usersToAdd.add(uid);
			}
		}
		if (!usersToAdd.isEmpty()) {
			Map<Id,User> mapUserInfoTemp = new Map<Id, User>([SELECT Id, Team__c, My_Team_Manager__c, Custom_Team__c, Group__c FROM User WHERE Id IN :usersToAdd]);
			mapUserInfo.putAll(mapUserInfoTemp);
		}
	}

	/**
	 * Retrieve a map of specific Queues based on a list of cases' ownerId
	 * @params:	List<Case> caseList				The List of Cases to use as the values for the maps
	 * @params: Map<Id, QueueSObject> allQueues The map of all Queues that will be  filtered down using list above
	 */

	private static Map<Id, QueueSObject> getSpecificQueuesMap(List<Case> caseList, Map<Id, QueueSObject> allQueues) {

		Map<Id, QueueSObject> specificQueueMap = new Map<Id, QueueSObject>();
		
		for(Case caseRec : caseList) {
			for(Id queueId : allQueues.keySet()){
				if(caseRec.OwnerId == queueId){
					specificQueueMap.put(queueId, allQueues.get(queueId));
				} 
			}
		}
		return specificQueueMap;
	}

	/**
	 * Retrieve  Map of all Queues
	 */

	public static Map<Id, QueueSObject> getAllQueuesMap() {

		Map<Id, QueueSObject> mapQueueInfo = new Map<Id, QueueSObject>();
		List<QueueSObject> qList = [SELECT Id, QueueId, Queue.Name FROM QueueSObject LIMIT 500];
		
		if (!qList.isEmpty()) {
			for (QueueSObject q : qList) {
				mapQueueInfo.put(q.QueueId, q);
			}			
		}
		return mapQueueInfo;
	}
	
	/**
	 * Close interaction record with user details
	 * @params:	Interaction__c pInteraction		The Interaction record to be updated
	 *			User pUserRec					The User record to use for the closing agent details
	 *			DateTime closedDateTime			The Date/Time to stamp in the End_DT__c field
	 *			Boolean overwriteUserInfo		If true, overwrite any existing Agent fields on this Interaction with those from pUserRec,
	 *											this is used for Email Interactions only (as these will have the automated system user when opened)
	 */
	private static void closeInteractionRecord(Interaction__c pInteraction, User pUserRec, DateTime closedDateTime, Boolean overwriteUserInfo) {
		pInteraction.End_dt__c = closedDateTime;
		pInteraction.Closing_Agent__c = pUserRec.Id;
		pInteraction.Closing_Agent_Manager__c = pUserRec.My_Team_Manager__c;
		pInteraction.Closing_Agent_Team__c = pUserRec.Custom_Team__c;
		pInteraction.Closing_Agent_Group__c = pUserRec.Group__c;

		if (overwriteUserInfo) {
			// <<004>> Status stamp from Case
			if (String.isBlank(pInteraction.Case_Status__c)) {
				pInteraction.Case_Status__c = pinteraction.Case__r.Status;
			}
			addAgentInfoToInteraction(pUserRec, pInteraction);
		}
	}
	
	/**
	 * Open interaction record - any logic to create an interaction can go here
	 * @params:	String pEventType				The Interaction record to be updated
	 *			Case pCaseRec					The Case this Interaction relates to
	 *			User pUserRec					The User record to use for the agent details
	 *			QueueSObject pQueueRec			The Queue the Case has been assigned to (for 'Queue' Interactions)
	 *			DateTime pStartDateTime			The Date/Time to stamp in the Start_DT__c field
	 */
	private static Interaction__c openInteractionRecord(String pEventType, Case pCaseRec, User pUserRec, QueueSObject pQueueRec, DateTime pStartDateTime) {
		Interaction__c pInteraction = new Interaction__c(Event_Type__c = pEventType, Case__c = pCaseRec.Id, Name = pCaseRec.CaseNumber + ': ' + pEventType);
		if (pEventType == CASE_QUEUE_TIME) {
			if (pQueueRec != null) {
				pInteraction.Queue__c = pQueueRec.Queue.Name;
			} else {
				pInteraction.Queue__c = pCaseRec.OwnerId;
			}
		}
		if (pEventType == OVERALL_EMAIL_HANDLING_PERFORMANCE) {
			if (CommonStaticUtils.isGroupId(pCaseRec.OwnerId)) {
				pInteraction.Email_Team__c = pQueueRec.Queue.Name;
			} else {
				pInteraction.Email_Team__c = pUserRec.Group__c;
			}
		}
			
		addAgentInfoToInteraction(pUserRec, pInteraction);
		pInteraction.Start_Dt__c = pStartDateTime != null ? pStartDateTime : DateTime.now();
		
		return pInteraction;
	}
	
	/**
	 * Update interaction record with agent details
	 * @params:	User pUserRec					The User record to use for the agent details
	 *			Interaction__c pInteraction		The Interaction record to be updated
	 */
	private static void addAgentInfoToInteraction(User pUserRec, Interaction__c pInteraction) {
		pInteraction.Agent__c = pUserRec.Id;
		pInteraction.Agent_Manager__c = pUserRec.My_Team_Manager__c;
		pInteraction.Team_Name__c = pUserRec.Custom_Team__c;
		pInteraction.Group__c = pUserRec.Group__c;
	}
	
	/**
	 * Output information for key variables for debugging purposes 
	 * @params:	String fromMethod				The name of the calling method (plus any other relevant info)
	 */
	private static void outputDebugInfo(String fromMethod) {
		system.debug('MPDEBUG INTERACTION DATA FROM: ' + fromMethod);
		system.debug('EXECUTION COUNT: ' + executionCount);
		system.debug('INTERACTION XREF UPDATED: ' + interactionXrefValuesUpdated);
		for (Id i : caseInteractionXref.keySet()) {
			system.debug('INTERACTION XREF: KEY=' + i + ', INTERACTION=' + caseInteractionXref.get(i));
		}
		for (Id p : processedCases) {
			system.debug('PROCESSED CASE: ' + p);
		}
	}
}