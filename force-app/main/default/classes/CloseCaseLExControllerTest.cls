@isTest
public class CloseCaseLExControllerTest {
	
	public static final String CONTACT_CENTRE_TEAM_NAME = '	Hamilton - CRD';
    public static final String CONTACT_CENTRE_QUEUE_NAME = 'CRD_Work_Allocation_Hamilton';
    
    @isTest static void testCaseDetailsAreGettingReturned() {
        
        User runningUser = UserTestDataFactory.getRunningUser();
        
        User contactCentreUser;
        
        Contact testContact;
        Case testCase;
        
        System.runAs(runningUser) {
            
            CustomSettingTestDataFactory.initialiseTeamCustomSettings(CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_QUEUE_NAME);
            
            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
        }
        
        System.runAs(contactCentreUser) {
            Test.startTest();
            testContact = CustomerTestDataFactory.createContact();
            insert testContact;
            
            testCase = CaseTestDataFactory.createComplaintCase(testContact.Id);
            insert testCase;
            
            Case savedCase = [SELECT RecordType.Name, RecordTypeId FROM Case WHERE Id = :testCase.Id];
            Case expectedResult = CloseCaseLExController.getCaseDetails(savedCase.Id);
            
            System.assertEquals(expectedResult.RecordTypeId, savedCase.RecordTypeId);
            System.assertEquals(expectedResult.RecordType.Name, savedCase.RecordType.Name);
            Test.stopTest();
        }
    }
    
    @isTest static void testCaseComplaintCatPermSetIsAssignedToUser() {
        
        User runningUser = UserTestDataFactory.getRunningUser();
        
        User contactCentreUser;
        
        Contact testContact;
        Case testCase;
        
        System.runAs(runningUser) {
            
            CustomSettingTestDataFactory.initialiseTeamCustomSettings(CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_QUEUE_NAME);
            
            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
            
            UserTestDataFactory.assignPermissionSetToUser(contactCentreUser.Id, 'Case_Complaint_Categorisation_Fields');
        }
        
        System.runAs(contactCentreUser) {
            Test.startTest();
            Boolean actualResult = CloseCaseLExController.isPermSetAssignedToUser('Case_Complaint_Categorisation_Fields');
            
            System.assertEquals(true, actualResult);
            Test.stopTest();
        }
    }
    
    @isTest static void testCloseOutstandingTasksWhenCaseClosedWithTasks() {
        
        User runningUser = UserTestDataFactory.getRunningUser();
        
        User contactCentreUser;
        
        Contact testContact;
        Case testCase;
        
        System.runAs(runningUser) {
            
            CustomSettingTestDataFactory.initialiseTeamCustomSettings(CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_QUEUE_NAME);
            
            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
        }
        
        System.runAs(contactCentreUser) {
            Test.startTest();
            testContact = CustomerTestDataFactory.createContact();
            insert testContact;
            
            testCase = CaseTestDataFactory.createComplaintCase(testContact.Id);
            insert testCase;
            
            
            Task aTask = new Task();
            aTask.Description = 'A task for testing';
            aTask.WhatId = testCase.Id;
            aTask.Subject = 'New task';
            insert aTask;
            
            testCase.Status = 'Closed - Resolved';
            
            testCase.jl_Primary_Category__c = 'Branch';
            testCase.jl_Primary_Reason__c = 'Customer Catering';
            testCase.jl_AreaBusiness_pri__c = 'Aberdeen';
            testCase.jl_Primary_Reason_Detail__c = 'Cleanliness';
            testCase.jl_Primary_Detail_Explanation__c = 'Benugo';
            update testCase;
            
            Case savedCase = [SELECT jl_NumberOutstandingTasks__c, Status FROM Case WHERE Id = :testCase.Id];
            Task outstandingTasksBeforeClosure = [SELECT Id, Status FROM Task LIMIT 1 ];
            System.assertEquals(1, savedCase.jl_NumberOutstandingTasks__c);
            System.assertEquals('New', outstandingTasksBeforeClosure.Status);
            
            CloseCaseLExController.closeOutstandingTasksWhenCaseClosed(testCase.Id);
            System.assertEquals('Closed - Resolved', savedCase.Status);
            Task outstandingTasksAfterClosure = [SELECT Id, Description, Status FROM Task LIMIT 1 ];
            System.assertEquals('Closed - Resolved', outstandingTasksAfterClosure.Status);
            System.assertNOTEquals('A task for testing', outstandingTasksAfterClosure.Description);
            Test.stopTest();
        }
    }
    
    @isTest static void testCloseOutstandingTasksWhenCaseClosedWithoutTasks() {
        
        User runningUser = UserTestDataFactory.getRunningUser();
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        User contactCentreUser;
        
        Contact testContact;
        Case testCase;
        
        System.runAs(runningUser) {            
            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
        }
        
        System.runAs(contactCentreUser) {
            Test.startTest();
            testContact = CustomerTestDataFactory.createContact();
            insert testContact;
            
            testCase = CaseTestDataFactory.createPSECase(testContact.Id);
            testCase.jl_NumberOutstandingTasks__c = 1;
            insert testCase;
            
            testCase.Status = 'Closed - Resolved';
            update testCase;
            
            CloseCaseLExController.closeOutstandingTasksWhenCaseClosed(testCase.Id);
            
            List<Task> outstandingTasksAfterClosure = [SELECT Id, Description, Status FROM Task LIMIT 1 ];
            System.assertEquals(0, outstandingTasksAfterClosure.size());
            Test.stopTest();
        }
    }

    @isTest static void testRelatedCaseActivityIsGettingReturned() {
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User testUser;
        Case testCase;
        Case_Activity__c testCaseActivity;
        Contact testContact;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;

            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        
        System.runAs(testUser) {

            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;

            testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            insert testCase;

            Test.startTest();
            
            List<Case_Activity__c> createdCaseActivity = CloseCaseLExController.getRelatedCaseActivityDetails(testCase.Id);
			System.assertEquals(1, createdCaseActivity.size(), 'Number of open Case Activities should equal 1');

            Test.stopTest();

        }
    }

    @isTest static void testCaseActivityResolutionMethodPicklistIsPopulated() {
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User testUser;
        Case testCase;
        Case_Activity__c testCaseActivity;
        Contact testContact;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;

            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        
        System.runAs(testUser) {

            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;

            testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            insert testCase;

            Test.startTest();
            
            testCaseActivity = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            insert testCaseActivity;

            Case_Activity__c createdCaseActivity = [SELECT Id, RecordTypeId FROM Case_Activity__c WHERE Id = :testCaseActivity.Id][0];
			List<CloseCaseLExController.WrapperCaseActivity> createdCaseActivity1 = CloseCaseLExController.getRelatedWrapperCaseActivityDetails(testCase.Id);
			List<Id> IdList = new List<Id>();
            IdList.add(testCaseActivity.Id);
                  
            List<Customer_Promise_Resolution_Dependency__mdt> caseActivityResolutionMethodPicklist = CloseCaseLExController.initiateCaseActivityResolutionMethodPicklist(createdCaseActivity.RecordTypeId);
			
            System.assertNotEquals(0, caseActivityResolutionMethodPicklist.size(), 'List should not be empty');
            
            system.assert(CloseCaseLExController.initiateLevelOnePicklist().size()>0);
            system.assert(CloseCaseLExController.generateCaseCategorizationDependentFieldMap().keySet().size()>0);
			CloseCaseLExController.closeselectedCaseActivities(JSON.serialize(IdList), 'No action required');
            Test.stopTest();

        }
    }
}