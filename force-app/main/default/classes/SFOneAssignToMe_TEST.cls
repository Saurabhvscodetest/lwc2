@istest
public with sharing class SFOneAssignToMe_TEST {

	@testSetup static void setup() {
         UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
    }

    @IsTest
    public static void testExtController()
    {
        UnitTestDataFactory.setRunValidationRules(false);        
        Contact con = UnitTestDataFactory.createContact(new Account());
        con.Email = 'a@bb.com';        
        insert con;        
        Case c = UnitTestDataFactory.createNormalCase(con);                
        insert c;        
        Case testCase = [Select Id, jl_Prefers_Contact_By__c from Case where Id = :c.Id];
    
        ApexPages.StandardController sc = new ApexPages.standardController(testCase);
        SFOneAssignToMeController sfOneAssignToMeController = new SFOneAssignToMeController(sc);
    
        PageReference result = sfOneAssignToMeController.assignOwnerToCurrentUser();
        
    }
    
    @IsTest
    public static void testExtController2()
    {
        UnitTestDataFactory.setRunValidationRules(false);        
        Contact con = UnitTestDataFactory.createContact(new Account());
        con.Email = 'a@bb.com';        
        insert con;        
        Case c = UnitTestDataFactory.createNormalCase(con);                
        insert c;        
        Case testCase = [Select Id, jl_Prefers_Contact_By__c from Case where Id = :c.Id];
        testCase.OwnerId = UserInfo.getUserId();
        update testCase;
        testCase = [Select Id, OwnerId, jl_Prefers_Contact_By__c from Case where Id = :c.Id];
        
        ApexPages.StandardController sc = new ApexPages.standardController(testCase);
        SFOneAssignToMeController sfOneAssignToMeController = new SFOneAssignToMeController(sc);
        
        PageReference result = sfOneAssignToMeController.assignOwnerToCurrentUser();
        
    }
}