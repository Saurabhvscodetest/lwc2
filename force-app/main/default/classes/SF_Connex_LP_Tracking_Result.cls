/*
* @File Name   : SF_LP_EH_Connection 
* @Description : IDV Result Insert
* @Copyright   : JohnLewis
* @Trello #    : #DC331
----------------------------------------------------------------------------------------------------------------
    Ver           Date               Author                     Modification
    ---           ----               ------                     ------------
*   1.0         14-FEB-21            Vijay A                       Created
*/

Public class SF_Connex_LP_Tracking_Result {
    
    Public static void trackingResults(List<String> customerInput){
        
        Live_Person_Tracking__c lpTrack = new Live_Person_Tracking__c();
        List<Live_Person_Tracking__c> lpTrackList = new List<Live_Person_Tracking__c>();
        
        System.debug('@@@ customerInput.size()' + customerInput.size());
        
        try{
           
            if(customerInput != null && customerInput.size() > 0){
                
                lpTrack.LP_Conversation_ID__c = customerInput[0]; //conversationId
                lpTrack.Customer_PhoneNumber__c = customerInput[1]; //customerPhoneNumber
                lpTrack.Customer_Email__c = customerInput[2]; //customerEmail
                lpTrack.Customer_Postcode__c = customerInput[3]; //customerPostcode
                lpTrack.Customer_Name__c = customerInput[4]; //customerName
                lpTrack.Customer_Address__c = customerInput[5]; //customerAddress
                
                if(customerInput.size() == 9){
                   
                lpTrack.Connex_Customer__c = customerInput[6]; //customerId
                lpTrack.LP_Search_Customer_Result__c = customerInput[7]; //customerSearchResult
                lpTrack.IDV_Status__c = customerInput[8]; //IDVStatus
                lpTrack.IDV_Type__c = 'Bot'; //IDVType
                    
                }
                
                else if( customerInput.size() == 8){
                    
              //lpTrack.Connex_Customer__c = customerInput[6]; //customerId
                lpTrack.LP_Search_Customer_Result__c = customerInput[6]; //customerSearchResult
                lpTrack.IDV_Status__c = customerInput[7]; //IDVStatus
                lpTrack.IDV_Type__c = 'Bot'; //IDVType
                    
                }
                
            }
            
            else{
                
                lpTrack.LP_Search_Customer_Result__c = 'NO_DATA_FOUND';
                lpTrack.IDV_Status__c = 'Bot_Failed';
                lpTrack.IDV_Type__c = 'Bot';
                
            }
            
            lpTrackList.add(lpTrack);  
        
        Insert lpTrackList;
            
        }
        Catch(Exception e){
            
            System.debug('@@@ Line 66 Exception Message' + e.getMessage());
            System.debug('@@@ Line 67 Exception Line Number' + e.getLineNumber());
        }
        
    }
    
    Public static void trackingResultsMissingParams(){
        
        Live_Person_Tracking__c lpTrack = new Live_Person_Tracking__c();
        List<Live_Person_Tracking__c> lpTrackList = new List<Live_Person_Tracking__c>();
        
        try{
            
            lpTrack.LP_Search_Customer_Result__c = 'MISSING_PARAMS';            
            lpTrackList.add(lpTrack);  
            Insert lpTrackList;
        }
        
        catch(Exception e){
            
            System.debug('@@@ Line 86 Exception Message' + e.getMessage());
            System.debug('@@@ Line 87 Exception Line Number' + e.getLineNumber());
            
        }
        
    }
    
     Public static void trackingResultsException(){
        
        Live_Person_Tracking__c lpTrack = new Live_Person_Tracking__c();
        List<Live_Person_Tracking__c> lpTrackList = new List<Live_Person_Tracking__c>();
        
        try{
            
            lpTrack.LP_Search_Customer_Result__c = 'NO_DATA_FOUND_EXCEPTION';            
            lpTrackList.add(lpTrack);  
            Insert lpTrackList;
        }
        
        catch(Exception e){
            
            System.debug('@@@ Line 108 Exception Message' + e.getMessage());
            System.debug('@@@ Line 109 Exception Line Number' + e.getLineNumber());
            
        }
        
    }
    
     Public static void saveCustomerChoosenOptions(List<String> customerOptions){
         
         try{
             
         Live_Person_Tracking__c lpTrack = new Live_Person_Tracking__c();
         List<Live_Person_Tracking__c> lpTrackList = new List<Live_Person_Tracking__c>();
        
         if(customerOptions.size() == 1){
             
            lpTrack.LP_Conversation_ID__c = customerOptions[0]; 
         } 
         
        else if(customerOptions.size() == 2){
            lpTrack.LP_Conversation_ID__c = customerOptions[0];
            lpTrack.Customer_Option_1__c = customerOptions[1];
        }
        else if(customerOptions.size() == 3){
            lpTrack.LP_Conversation_ID__c = customerOptions[0];
            lpTrack.Customer_Option_1__c = customerOptions[1];
            lpTrack.Customer_Option_2__c = customerOptions[2];
            
        }
         else if(customerOptions.size() == 4){
            lpTrack.LP_Conversation_ID__c = customerOptions[0];
            lpTrack.Customer_Option_1__c = customerOptions[1];
            lpTrack.Customer_Option_2__c = customerOptions[2];
            lpTrack.Customer_Option_3__c = customerOptions[3]; 
        }
         else if(customerOptions.size() == 5){
            lpTrack.LP_Conversation_ID__c = customerOptions[0];
            lpTrack.Customer_Option_1__c = customerOptions[0];
            lpTrack.Customer_Option_2__c = customerOptions[1];
            lpTrack.Customer_Option_3__c = customerOptions[2]; 
            lpTrack.Customer_Option_4__c = customerOptions[3]; 
        }
         else if(customerOptions.size() == 6){
            lpTrack.LP_Conversation_ID__c = customerOptions[0];
            lpTrack.Customer_Option_1__c = customerOptions[1];
            lpTrack.Customer_Option_2__c = customerOptions[2];
            lpTrack.Customer_Option_3__c = customerOptions[3]; 
            lpTrack.Customer_Option_4__c = customerOptions[4]; 
            lpTrack.Customer_Option_5__c = customerOptions[5];  
            
        }
         else if(customerOptions.size() == 7){
            lpTrack.LP_Conversation_ID__c = customerOptions[0];
            lpTrack.Customer_Option_1__c = customerOptions[1];
            lpTrack.Customer_Option_2__c = customerOptions[2];
            lpTrack.Customer_Option_3__c = customerOptions[3]; 
            lpTrack.Customer_Option_4__c = customerOptions[4]; 
            lpTrack.Customer_Option_5__c = customerOptions[5];  
            lpTrack.Customer_Option_6__c = customerOptions[6];  
        }
         else if(customerOptions.size() >  7){
            lpTrack.LP_Conversation_ID__c = customerOptions[0];
            lpTrack.Customer_Option_1__c = customerOptions[1];
            lpTrack.Customer_Option_2__c = customerOptions[2];
            lpTrack.Customer_Option_3__c = customerOptions[3]; 
            lpTrack.Customer_Option_4__c = customerOptions[4]; 
            lpTrack.Customer_Option_5__c = customerOptions[5];  
            lpTrack.Customer_Option_6__c = customerOptions[6];  
         }else{
             
             system.debug('@@@ customerOptions.size' + customerOptions.size());
         }
        
        lpTrackList.add(lpTrack);
        Insert lpTrackList;
             
         }
         
         catch(Exception e){
             
			System.debug('@@@ Line 108 Exception Message' + e.getMessage());
            System.debug('@@@ Line 109 Exception Line Number' + e.getLineNumber());             
         }
        
        
    }
   
    
}