@isTest
public  class DeliveryTrackingLEXControllerTest {
    public static String ORDER_ID = '333';
    public static String orderNumber = null ;
    
    @isTest static void testGVFrequestWithDeliveryID() {
        setUp();
        string DELIVERY_ID_INPUT = ORDER_ID + DeliveryTrackingVOMock.INPUT_MOCK;
        System.debug('DELID'+DELIVERY_ID_INPUT);
        Test.setMock(HttpCalloutMock.class, new DeliveryTrackingVOMock());
        
        Test.startTest();
        DeliveryTrackingLEXController.WrapperResponse wr = DeliveryTrackingLEXController.invokeDeliveryTracking(orderNumber,DELIVERY_ID_INPUT);
        Test.stopTest();
        
        System.debug(':::'+wr);
        System.assert(wr != null,  'Must populate the JSON response');
    }
    
    @isTest static void testfetchRelatedCase()
    {
        String delivernumber='12345678';
        String ordernumber = '87654321';
        List<Case> CaseList = [Select id,jl_Case_Type__c,CaseNumber, RecordType.Name,status,createddate,OwnerId ,jl_DeliveryNumber__c, Show_Case__c from case where jl_DeliveryNumber__c =: delivernumber];
        
        List<Case> CaseList1=DeliveryTrackingLEXController.fetchRelatedCase(delivernumber,ordernumber);
        
        System.assertEquals(CaseList,CaseList1);
    }
    
    @isTest static void testfetchCaseInfo()
    {
        Contact testContact;
        Case testCase;
       testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;
        testCase = CaseTestDataFactory.createCase(testContact.Id);
       
        List<Case> CaseList = [Select id,jl_OrderManagementNumber__c,jl_DeliveryNumber__c from Case where id =: testCase.id];
        List<Case> CaseList1=DeliveryTrackingLEXController.fetchCaseInfo(testCase.id);
        
        System.assertEquals(CaseList,CaseList1);
    } 
    
   /* @isTest static void testfetchCurrentSignatureList() {
        setUp();
        //string DELIVERY_ID_INPUT = ORDER_ID + DeliveryTrackingVOMock.INPUT_MOCK;
        //System.debug('DELID'+DELIVERY_ID_INPUT);
        Test.setMock(HttpCalloutMock.class, new DeliveryTrackingVOMock());
        
        Test.startTest();
        	List<String> imageStringList = DeliveryTrackingLEXController.fetchCurrentSignatureList('50027838');
        Test.stopTest();
        
        //System.debug(':::'+wr);
        System.assert(imageStringList != null,  'Images');
    } */
    
    static void setUp() {
        //Insert Callout Setting
        Callout_Settings__c calloutSettings = new Callout_Settings__c();
        calloutSettings.name = EHConstants.EVENT_HUB_GET_EVENTS;
        calloutSettings.Class_Name__c = EHGetEventsService.class.getName();
        calloutSettings.Timeout_Milliseconds__c = 3000;
        calloutSettings.Endpoint__c = 'http://localhost'; 
        insert calloutSettings;
        
        Config_Settings__c defaultNotificationEmail = 
            new Config_Settings__c(Name = 'EMAIL_NOTIFICATION_DEFAULT_ADDRESS', Text_Value__c = 'test@test.co.uk');
        insert defaultNotificationEmail;
    }
}