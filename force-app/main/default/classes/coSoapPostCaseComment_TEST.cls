/*************************************************
coSoapPostCaseComment_TEST

Mock class to be used with coSoapPostCaseComments
Author: Steven Loftus (MakePositive)
Created Date: 19/11/2014
Modification Date: 
Modified By: 

**************************************************/
@isTest
private class coSoapPostCaseComment_TEST {
	
    public static testmethod void postCaseComment() {

        Config_Settings__c configSearch = new Config_Settings__c();
        configSearch.Name = CustomSettingsManager.WS_Endpoint_SearchCustomer;
        configSearch.Text_Value__c = 'SearchCustomer';
        insert configSearch;

        // set up the config settings
        Config_Settings__c configCase = new Config_Settings__c();
        configCase.Name = CustomSettingsManager.WS_Endpoint_ManageCustomerComments;
        configCase.Text_Value__c = 'ManageCustomerComments';
        insert configCase;

        // create a case for the comment
		List<CaseComment> caseCommentList = new List<CaseComment>();
		CaseComment caseComment = new CaseComment();
		caseComment.ParentId = '500D000000IEEmT';
		caseComment.CommentBody = 'Test';
		caseCommentList.add(caseComment);

        Map<Id, String> orderIdByCaseIdMap = new Map<Id, String>();
        orderIdByCaseIdMap.put('500D000000IEEmT', '123456');

        Test.setMock(HttpCalloutMock.class, new coSoapSetCaseComment_MOCK());

        Test.startTest();
        coSoapManager.postCaseComments(caseCommentList, orderIdByCaseIdMap, 'Steven', 'Administrator');
        coSoapPostCaseComment coSoap = new coSoapPostCaseComment();
        try {
        	coSoap.getSoapRequest();
        }
        catch(Exception e) {}
        Test.stopTest();

        // no asserts yet
    }
    
    public static testmethod void postCaseCommentTest() {

        Config_Settings__c configSearch = new Config_Settings__c();
        configSearch.Name = CustomSettingsManager.WS_Endpoint_SearchCustomer;
        configSearch.Text_Value__c = 'SearchCustomer';
        insert configSearch;

        // set up the config settings
        Config_Settings__c configCase = new Config_Settings__c();
        configCase.Name = CustomSettingsManager.WS_Endpoint_ManageCustomerComments;
        configCase.Text_Value__c = 'ManageCustomerComments';
        insert configCase;

        // create a case for the comment
		List<CaseComment> caseCommentList = new List<CaseComment>();
		CaseComment caseComment = new CaseComment();
		caseComment.ParentId = '500D000000IEEmT';
		caseComment.CommentBody = 'Test';
		caseCommentList.add(caseComment);

        Map<Id, String> orderIdByCaseIdMap = new Map<Id, String>();
        orderIdByCaseIdMap.put('500D000000IEEmT', '123456');

        Test.setMock(HttpCalloutMock.class, new coSoapSetCaseComment_MOCK());

        Test.startTest();
        coSoapManager.postCaseComments(caseCommentList, orderIdByCaseIdMap, 'Steven', 'Administrator');
        coSoapPostCaseComment coSoap = new coSoapPostCaseComment();
        try {
        	coSoap.setSoapResponse('test');
        }
        catch(Exception e) {}
        Test.stopTest();

        // no asserts yet
    }

    public static testmethod void postCaseCommentError() {

        Config_Settings__c configSearch = new Config_Settings__c();
        configSearch.Name = CustomSettingsManager.WS_Endpoint_SearchCustomer;
        configSearch.Text_Value__c = 'SearchCustomer';
        insert configSearch;

        // set up the config settings
        Config_Settings__c configCase = new Config_Settings__c();
        configCase.Name = CustomSettingsManager.WS_Endpoint_ManageCustomerComments;
        configCase.Text_Value__c = 'ManageCustomerCommentsError';
        insert configCase;

        // create a case for the comment
		List<CaseComment> caseCommentList = new List<CaseComment>();
		CaseComment caseComment = new CaseComment();
		caseComment.ParentId = '500D000000IEEmT';
		caseComment.CommentBody = 'Test';
		caseCommentList.add(caseComment);

        Map<Id, String> orderIdByCaseIdMap = new Map<Id, String>();
        orderIdByCaseIdMap.put('500D000000IEEmT', '123456');

        Test.setMock(HttpCalloutMock.class, new coSoapSetCaseComment_MOCK());

        Test.startTest();
        coSoapManager.postCaseComments(caseCommentList, orderIdByCaseIdMap, 'Steven', 'Administrator');
        Test.stopTest();

        // no asserts yet
    }


}