/* Description  : GDPR - Decouple Accounts from Contacts
*
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   28/03/2019      Vijay Ambati                        Created             COPT-4432
*
*/
global class ScheduleBatchDecoupleAccounts implements Schedulable{
   global void execute(SchedulableContext sc){
        BAT_DecoupleAccountsFromContacts obj = new BAT_DecoupleAccountsFromContacts();
        Database.executebatch(obj);
    }
   
}