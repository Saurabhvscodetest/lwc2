public with sharing class CustomSettingsManager 
{

    public static String NEW_CASE_FROM_EMAIL_CLOSED_DAYS = 'NEW_CASE_FROM_EMAIL_CLOSED_DAYS';
    public static String ActSite_Maximum_Security_Trials='ActSite_Maximum_Security_Trials';
    public static String ActSite_Maximum_Warning_Trails='ActSite_Maximum_Warning_Trails';
    public static String WS_Endpoint_SearchCustomer = 'WS_Endpoint_SearchCustomer';
    public static String WS_Endpoint_ViewCustomerOrdersV2 = 'WS_Endpoint_ViewCustomerOrdersV2';
    public static String WS_Endpoint_ViewCustomerOrdersV1 = 'WS_Endpoint_ViewCustomerOrdersV1';
    public static String WS_Endpoint_ManageCustomerComments = 'WS_Endpoint_ManageCustomerComments';
    public static String WS_Endpoint_EmailContent = 'WS_Endpoint_EmailContent';
    public static String EmailDeleteProfiles = 'EmailDeleteProfiles';
    public static final String OPENSPAN_DEFAULT_PSE_OWNER = 'OpenSpan_Default_PSE_Owner';
    
    public static String FORWARD_EMAIL_ADDRESS='FORWARD_EMAIL_ADDRESS';

    public static Map<String,Config_Settings__c> getAllConfigSettings() 
    {
        return Config_Settings__c.getAll();
    }
    
    public static Boolean getConfigSettingBooleanVal(String settingName)
    {
        return CustomSettingsManager.getConfigSetting(settingName).Checkbox_Value__c;
    }
    
    public static String getConfigSettingStringVal(String settingName)
    {
        return CustomSettingsManager.getConfigSetting(settingName).Text_Value__c;
    }
    
    public static Decimal getConfigSettingNumberVal(String settingName)
    {
        return CustomSettingsManager.getConfigSetting(settingName).Number_Value__c;
    }
    
    public static Integer getConfigSettingIntegerVal(String settingName)
    {
        return Integer.valueOf(CustomSettingsManager.getConfigSetting(settingName).Number_Value__c);
    }
    
    public static Config_Settings__c getConfigSetting(String settingName)
    {
        return Config_Settings__c.getAll().get(settingName);
    }
    
}