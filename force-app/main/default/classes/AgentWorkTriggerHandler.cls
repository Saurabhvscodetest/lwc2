/******************************************************************************
 * @author       Stuart Barber
 * @date         05.04.2017
 * @description  Class that calls trigger actions for the Agent Work object
 
******************************************************************************/
public without sharing class AgentWorkTriggerHandler {

    /**
     * mainEntry method called from the Trigger.
	 * @params:	Standard Trigger context variables. 
	 * @rtnval:	void
     */

	public static void mainEntry(Boolean isBefore, 
								 Boolean isDelete, Boolean isAfter,
								 Boolean isInsert, Boolean isUpdate,
								 Boolean isExecuting, List<AgentWork> newList,
								 Map<Id, SObject> newMap, List<AgentWork> oldList,
								 Map<Id, SObject> oldMap) {
        
        System.debug('@@entering AgentWorkTriggerHandler');
        
        AgentWorkTriggerHandler handler = new AgentWorkTriggerHandler();                      

    	if(isBefore) {
            	handler.handleAgentWorkBeforeInsert(newlist);                          
        }                                 
    }

    /**
	* @description	Maps case fields to corresponding fields on agent work records
	* @fields		Case_Number__c, Contact_Reason__c, Reason_Details__c  			
	* @author		@stuartbarber	
	*/

    public void handleAgentWorkBeforeInsert(List<AgentWork> newAgentWorkRecords) {
	
		System.debug('@@entering handleAgentWorkBeforeInserts');

		Set<Id> relatedCaseIds = new Set<Id>();

		for(AgentWork newAgentWork : newAgentWorkRecords) {
			relatedCaseIds.add(newAgentWork.WorkItemId);	
		}

		Map<Id, Case> relatedCaseMap = new Map<Id, Case>([SELECT Id, CaseNumber, Contact_Reason__c, Reason_Detail__c FROM Case WHERE Id IN :relatedCaseIds]);

		for(AgentWork newAgentWork : newAgentWorkRecords) {
			if(relatedCaseMap.keySet().contains(newAgentWork.WorkItemId)){
				if(newAgentWork.Case_Number__c == null){
					newAgentWork.Case_Number__c = relatedCaseMap.get(newAgentWork.WorkItemId).CaseNumber;
				}
				if(newAgentWork.Contact_Reason__c == null){
					newAgentWork.Contact_Reason__c = relatedCaseMap.get(newAgentWork.WorkItemId).Contact_Reason__c;
				}
				if(newAgentWork.Reason_Detail__c == null){
					newAgentWork.Reason_Detail__c = relatedCaseMap.get(newAgentWork.WorkItemId).Reason_Detail__c;
				}
			}
		}		
	}
}