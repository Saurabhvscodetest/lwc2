@isTest
private class AutoAssignPermissionSetHelperTest {
	
	@isTest
    static void upsertAutoAssignPermSetsForQueueMembers_ShouldCreateNewAutoAssignPermSets() {
    	Integer randomNo = UnitTestDataFactory.getRandomNumber();
    	String QUEUE_NAME = 'TESTQUEUE' + randomNo;
    	User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    	
        UserRole r = new UserRole(Name = QUEUE_NAME);
    	System.runAs(runningUser) { //
			insert r;
    	}
		
		String autoAssignPersmissionSetName =  AutoAssignPermissionSetHelper.getAutoAssignPermissionSetName(QUEUE_NAME);
		List<Auto_Assign_Permission_Set__c> autoAssignPermissionSetList = [Select Id , Name, Permission_Sets_To_Assign__c  from Auto_Assign_Permission_Set__c where Name =:autoAssignPersmissionSetName LIMIT 1];
		system.assert(autoAssignPermissionSetList.isEmpty() , 'The autoassign permission set be empty');
		
		 Test.startTest();
    		AutoAssignPermissionSetHelper.upsertAutoAssignPermSetsForQueueMembers(new Set<ID>{r.Id});
        Test.stopTest();
        
		//Requery the permission set
		autoAssignPermissionSetList = [Select Id , Name, Permission_Sets_To_Assign__c  from Auto_Assign_Permission_Set__c where Name =:autoAssignPersmissionSetName LIMIT 1];
		system.assert(!autoAssignPermissionSetList.isEmpty() , 'The autoassign permission set NOT be empty');
		system.assertEquals(AutoAssignPermissionSetHelper.CASE_SERVICE_PRESENCE_STATUS_PERMSET, autoAssignPermissionSetList[0].Permission_Sets_To_Assign__c , 'The permission set name should match');
		
    }

	@isTest
    static void upsertAutoAssignPermSetsForQueueMembers_AddTheNewPermissionSetIfThereArentAny() {
    	Integer randomNo = UnitTestDataFactory.getRandomNumber();
    	String QUEUE_NAME = 'TESTQUEUE' + randomNo;
    	User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    	
        UserRole r = new UserRole(Name = QUEUE_NAME);
    	System.runAs(runningUser) { //
			insert r;
    	}
		
		//User Role that was created as part of the setup using the same team Name
		String autoAssignPersmissionSetName =  AutoAssignPermissionSetHelper.getAutoAssignPermissionSetName(QUEUE_NAME);
		Auto_Assign_Permission_Set__c aap  = new Auto_Assign_Permission_Set__c(Name = autoAssignPersmissionSetName);
		insert aap;
		
		Test.startTest();
    		AutoAssignPermissionSetHelper.upsertAutoAssignPermSetsForQueueMembers(new Set<ID>{r.Id});
        Test.stopTest();
        
		//Requery the permission set
		List<Auto_Assign_Permission_Set__c> autoAssignPermissionSetList = [Select Id , Name, Permission_Sets_To_Assign__c  from Auto_Assign_Permission_Set__c where Name =:autoAssignPersmissionSetName LIMIT 1];
		system.assert(!autoAssignPermissionSetList.isEmpty() , 'The autoassign permission set NOT be empty');
		system.assertEquals(AutoAssignPermissionSetHelper.CASE_SERVICE_PRESENCE_STATUS_PERMSET, autoAssignPermissionSetList[0].Permission_Sets_To_Assign__c , 'The permission set name should match');
		
    }
    
	@isTest
    static void upsertAutoAssignPermSetsForQueueMembers_AmendTheNewPermissionSetToTheExistingSets() {
    	Integer randomNo = UnitTestDataFactory.getRandomNumber();
    	String QUEUE_NAME = 'TESTQUEUE' + randomNo;
    	User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
    	String EXISTING_PERMISSION_SET_NAME = 'ZZZZZZZ';
        UserRole r = new UserRole(Name = QUEUE_NAME);
    	System.runAs(runningUser) { //
			insert r;
    	}
		
		//User Role that was created as part of the setup using the same team Name
		String autoAssignPersmissionSetName =  AutoAssignPermissionSetHelper.getAutoAssignPermissionSetName(QUEUE_NAME);
		Auto_Assign_Permission_Set__c aap  = new Auto_Assign_Permission_Set__c(Name = autoAssignPersmissionSetName, Permission_Sets_To_Assign__c = EXISTING_PERMISSION_SET_NAME);
		insert aap;
		
		Test.startTest();
    		AutoAssignPermissionSetHelper.upsertAutoAssignPermSetsForQueueMembers(new Set<ID>{r.Id});
        Test.stopTest();
        
		//Requery the permission set
		List<Auto_Assign_Permission_Set__c> autoAssignPermissionSetList = [Select Id , Name, Permission_Sets_To_Assign__c  from Auto_Assign_Permission_Set__c where Name =:autoAssignPersmissionSetName LIMIT 1];
		system.assert(!autoAssignPermissionSetList.isEmpty() , 'The autoassign permission set NOT be empty');
		system.assert(autoAssignPermissionSetList[0].Permission_Sets_To_Assign__c.contains(AutoAssignPermissionSetHelper.CASE_SERVICE_PRESENCE_STATUS_PERMSET) , 'The new permission should be added');
		
    }
    
}