/**
 * Unit tests for PCDUpdateHandler class methods.
 */
@isTest
private class PCDUpdateHandler_TEST {
	private static final Id SUPERSEDED_RECORD_TYPE = [SELECT Id FROM RecordType WHERE SobjectType = 'Contact' AND DeveloperName = 'Superseded' LIMIT 1].Id;
	
	/**
	 * Test single contact reparenting logic
	 */
	static testMethod void testTransferSingleContact() {
    	system.debug('TEST START: testTransferSingleContact');
		initialiseSettings(false);
       	Contact masterCon = UnitTestDataFactory.createContact(1, false);
       	masterCon.PCD_Id__c = 'PCDMASTER123';
       	Contact supersededCon = UnitTestDataFactory.createContact(2, false);
       	supersededCon.PCD_Id__c = 'PCDTEST123';
       	List<Contact> conList = new List<Contact> {masterCon, supersededCon};
       	insert conList;
       	       	
       	Case testCase1 = UnitTestDataFactory.createQueryCase(supersededCon.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, true);
		Contact_Profile__c testContactProfile1 = new Contact_Profile__c(Contact__c = supersededCon.Id);
		insert testContactProfile1;
		Task testTask1 = new Task(WhatId = testCase1.Id, WhoId = supersededCon.Id, ActivityDate = Date.today(), Type = 'Case Assist', Subject = 'Assist',
								  Status = 'Completed', jl_created_from_case__c = true);
		Task testTask2 = new Task(WhoId = supersededCon.Id, ActivityDate = Date.today(), Type = 'Case Assist', Subject = 'Test Contact Task',
								  Status = 'Completed', jl_created_from_case__c = true);
		List<Task> taskList = new List<Task> {testTask1, testTask2};
		insert taskList;
		
		clsCaseTriggerHandler.resetCaseTriggerStaticVariables();
		supersededCon.Superseded_By_SCV_Id__c = masterCon.PCD_Id__c;
		//test.startTest();
		update supersededCon;
		//test.stopTest();
		
		system.assertEquals(false, PCDUpdateHandler.BATCH_MODE);
		system.assertEquals(true, PCDUpdateHandler.IS_RUNNING_PCD_UPDATE);

		List<Contact> testConList1 = [SELECT Id, Name, Superseded_By_Customer__c, RecordTypeId, Email, 
											(SELECT Id, Contact__c FROM ContactProfiles__r), 
											(SELECT Id, ContactId FROM Cases), 
											(SELECT Id, WhoId FROM Tasks) 
									  FROM Contact WHERE Id IN :conList];
		system.assertEquals(2, testConList1.size());
		
		for (Contact testCon : testConList1) {
			if (testCon.Id == masterCon.Id) {
				system.assertNotEquals(SUPERSEDED_RECORD_TYPE, testCon.RecordTypeId);
				system.assertEquals(null, testCon.Superseded_By_Customer__c);
				system.assertEquals(masterCon.Email, testCon.Email);
				system.assertEquals(1, testCon.ContactProfiles__r.size());
				system.assertEquals(1, testCon.Cases.size());
				system.assertEquals(2, testCon.Tasks.size());
			} else if (testCon.Id == supersededCon.Id) {
				system.assertEquals(SUPERSEDED_RECORD_TYPE, testCon.RecordTypeId);
				system.assertEquals(masterCon.Id, testCon.Superseded_By_Customer__c);
				system.assertEquals(supersededCon.Email + '.spd', testCon.Email);
				system.assertEquals(0, testCon.ContactProfiles__r.size());
				system.assertEquals(0, testCon.Cases.size());
				system.assertEquals(0, testCon.Tasks.size());
			} else {
				system.assert(false);
			}
		}
    	system.debug('TEST END: testTransferSingleContact');
	}
	
	/**
	 * Test multiple contact reparenting logic
	 */
	static testMethod void testTransferMultipleContacts() {
    	system.debug('TEST START: testTransferMultipleContacts');
		initialiseSettings(false);
       	Contact masterCon = UnitTestDataFactory.createContact(1, false);
       	masterCon.PCD_Id__c = 'PCDMASTER123';
       	
       	Contact supersededCon1 = UnitTestDataFactory.createContact(2, false);
       	Contact supersededCon2 = UnitTestDataFactory.createContact(3, false);
       	Contact supersededCon3 = UnitTestDataFactory.createContact(4, false);
       	supersededCon1.PCD_Id__c = 'PCDTEST121';
       	supersededCon2.PCD_Id__c = 'PCDTEST122';
       	supersededCon3.PCD_Id__c = 'PCDTEST123';
       	List<Contact> conList = new List<Contact> {masterCon, supersededCon1, supersededCon2, supersededCon3};
       	insert conList;
       	       	
       	Case testCase1 = UnitTestDataFactory.createQueryCase(supersededCon1.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, false);
       	Case testCase2 = UnitTestDataFactory.createQueryCase(supersededCon2.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, false);
       	Case testCase3 = UnitTestDataFactory.createQueryCase(supersededCon3.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, false);
       	List<Case> caseList = new List<Case> {testCase1, testCase2, testCase3};
       	insert caseList;
       	
		Contact_Profile__c testContactProfile1 = new Contact_Profile__c(Contact__c = supersededCon1.Id);
		Contact_Profile__c testContactProfile2 = new Contact_Profile__c(Contact__c = supersededCon2.Id);
		Contact_Profile__c testContactProfile3 = new Contact_Profile__c(Contact__c = supersededCon3.Id);
       	List<Contact_Profile__c> cpList = new List<Contact_Profile__c> {testContactProfile1, testContactProfile2, testContactProfile3};
		insert cpList;

		Task testTask1 = new Task(WhatId = testCase1.Id, WhoId = supersededCon1.Id, ActivityDate = Date.today(), Type = 'Case Assist', Subject = 'Assist',
								  Status = 'Completed', jl_created_from_case__c = true);
		Task testTask2 = new Task(WhoId = supersededCon1.Id, ActivityDate = Date.today(), Type = 'Case Assist', Subject = 'Test Contact Task',
								  Status = 'Completed', jl_created_from_case__c = true);
		Task testTask3 = new Task(WhatId = testCase2.Id, WhoId = supersededCon2.Id, ActivityDate = Date.today(), Type = 'Case Assist', Subject = 'Assist',
								  Status = 'Completed', jl_created_from_case__c = true);
		Task testTask4 = new Task(WhoId = supersededCon2.Id, ActivityDate = Date.today(), Type = 'Case Assist', Subject = 'Test Contact Task',
								  Status = 'Completed', jl_created_from_case__c = true);
		Task testTask5 = new Task(WhatId = testCase3.Id, WhoId = supersededCon3.Id, ActivityDate = Date.today(), Type = 'Case Assist', Subject = 'Assist',
								  Status = 'Completed', jl_created_from_case__c = true);
		Task testTask6 = new Task(WhoId = supersededCon3.Id, ActivityDate = Date.today(), Type = 'Case Assist', Subject = 'Test Contact Task',
								  Status = 'Completed', jl_created_from_case__c = true);
		List<Task> taskList = new List<Task> {testTask1, testTask2, testTask3, testTask4, testTask5, testTask6};
		insert taskList;
		
		clsCaseTriggerHandler.resetCaseTriggerStaticVariables();
		
		supersededCon1.Superseded_By_SCV_Id__c = masterCon.PCD_Id__c;
		supersededCon2.Superseded_By_SCV_Id__c = masterCon.PCD_Id__c;
		supersededCon3.Superseded_By_SCV_Id__c = masterCon.PCD_Id__c;
		List<Contact> updConList = new List<Contact> {supersededCon1, supersededCon2, supersededCon3};
		
		test.startTest();
		update updConList;
		test.stopTest();
		
		system.assertEquals(false, PCDUpdateHandler.BATCH_MODE);
		system.assertEquals(true, PCDUpdateHandler.IS_RUNNING_PCD_UPDATE);

		List<Contact> testConList1 = [SELECT Id, Name, Superseded_By_Customer__c, RecordTypeId, Email, 
											(SELECT Id, Contact__c FROM ContactProfiles__r), 
											(SELECT Id, ContactId FROM Cases), 
											(SELECT Id, WhoId FROM Tasks) 
									  FROM Contact WHERE Id IN :conList];
		system.assertEquals(4, testConList1.size());
		
		for (Contact testCon : testConList1) {
			if (testCon.Id == masterCon.Id) {
				system.assertNotEquals(SUPERSEDED_RECORD_TYPE, testCon.RecordTypeId);
				system.assertEquals(null, testCon.Superseded_By_Customer__c);
				system.assertEquals(masterCon.Email, testCon.Email);
				system.assertEquals(3, testCon.ContactProfiles__r.size());
				system.assertEquals(3, testCon.Cases.size());
				system.assertEquals(6, testCon.Tasks.size());
			} else if (testCon.Id == supersededCon1.Id || testCon.Id == supersededCon2.Id || testCon.Id == supersededCon3.Id) {
				system.assertEquals(SUPERSEDED_RECORD_TYPE, testCon.RecordTypeId);
				system.assertEquals(masterCon.Id, testCon.Superseded_By_Customer__c);
				system.assertEquals('.spd', testCon.Email.right(4));
				system.assertEquals(0, testCon.ContactProfiles__r.size());
				system.assertEquals(0, testCon.Cases.size());
				system.assertEquals(0, testCon.Tasks.size());
			} else {
				system.assert(false);
			}
		}
    	system.debug('TEST END: testTransferMultipleContacts');
	}

	/**
	 * Test reparenting blocked for standard updates when Batch Only is set in Custom Settings
	 */
	static testMethod void testBatchOnlyBlocksStandardTransfer() {
    	system.debug('TEST START: testBatchOnlyBlocksStandardTransfer');
		initialiseSettings(true);
       	Contact masterCon = UnitTestDataFactory.createContact(1, false);
       	masterCon.PCD_Id__c = 'PCDMASTER123';
       	Contact supersededCon = UnitTestDataFactory.createContact(2, false);
       	supersededCon.PCD_Id__c = 'PCDTEST123';
       	List<Contact> conList = new List<Contact> {masterCon, supersededCon};
       	insert conList;
       	       	
       	Case testCase1 = UnitTestDataFactory.createQueryCase(supersededCon.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, true);
		Contact_Profile__c testContactProfile1 = new Contact_Profile__c(Contact__c = supersededCon.Id);
		insert testContactProfile1;
		Task testTask1 = new Task(WhatId = testCase1.Id, WhoId = supersededCon.Id, ActivityDate = Date.today(), Type = 'Case Assist', Subject = 'Assist',
								  Status = 'Completed', jl_created_from_case__c = true);
		Task testTask2 = new Task(WhoId = supersededCon.Id, ActivityDate = Date.today(), Type = 'Case Assist', Subject = 'Test Contact Task',
								  Status = 'Completed', jl_created_from_case__c = true);
		List<Task> taskList = new List<Task> {testTask1, testTask2};
		insert taskList;
		
		clsCaseTriggerHandler.resetCaseTriggerStaticVariables();
		
		supersededCon.Superseded_By_SCV_Id__c = masterCon.PCD_Id__c;
		test.startTest();
		update supersededCon;
		test.stopTest();
		
		system.assertEquals(false, PCDUpdateHandler.BATCH_MODE);
		system.assertEquals(true, PCDUpdateHandler.IS_RUNNING_PCD_UPDATE);

		List<Contact> testConList1 = [SELECT Id, Name, Superseded_By_Customer__c, RecordTypeId, Email, 
											(SELECT Id, Contact__c FROM ContactProfiles__r), 
											(SELECT Id, ContactId FROM Cases), 
											(SELECT Id, WhoId FROM Tasks) 
									  FROM Contact WHERE Id IN :conList];
		system.assertEquals(2, testConList1.size());
		
		for (Contact testCon : testConList1) {
			if (testCon.Id == masterCon.Id) {
				system.assertNotEquals(SUPERSEDED_RECORD_TYPE, testCon.RecordTypeId);
				system.assertEquals(null, testCon.Superseded_By_Customer__c);
				system.assertEquals(masterCon.Email, testCon.Email);
				system.assertEquals(0, testCon.ContactProfiles__r.size());
				system.assertEquals(0, testCon.Cases.size());
				system.assertEquals(0, testCon.Tasks.size());
			} else if (testCon.Id == supersededCon.Id) {
				system.assertNotEquals(SUPERSEDED_RECORD_TYPE, testCon.RecordTypeId);
				system.assertEquals(null, testCon.Superseded_By_Customer__c);
				system.assertEquals(supersededCon.Email, testCon.Email);
				system.assertEquals(1, testCon.ContactProfiles__r.size());
				system.assertEquals(1, testCon.Cases.size());
				system.assertEquals(2, testCon.Tasks.size());
			} else {
				system.assert(false);
			}
		}
    	system.debug('TEST END: testBatchOnlyBlocksStandardTransfer');
	}

	/**
	 * Test Invalid PCD Reference validation
	 */
	static testMethod void testDodgyPCDReference() {
    	system.debug('TEST START: testDodgyPCDReference');
		initialiseSettings(false);
       	Contact masterCon = UnitTestDataFactory.createContact(1, false);
       	masterCon.PCD_Id__c = 'PCDMASTER123';
       	Contact supersededCon = UnitTestDataFactory.createContact(2, false);
       	supersededCon.PCD_Id__c = 'PCDTEST123';
       	List<Contact> conList = new List<Contact> {masterCon, supersededCon};
       	insert conList;
       	       	
       	Case testCase1 = UnitTestDataFactory.createQueryCase(supersededCon.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, true);
		Contact_Profile__c testContactProfile1 = new Contact_Profile__c(Contact__c = supersededCon.Id);
		insert testContactProfile1;
		Task testTask1 = new Task(WhatId = testCase1.Id, WhoId = supersededCon.Id, ActivityDate = Date.today(), Type = 'Case Assist', Subject = 'Assist',
								  Status = 'Completed', jl_created_from_case__c = true);
		Task testTask2 = new Task(WhoId = supersededCon.Id, ActivityDate = Date.today(), Type = 'Case Assist', Subject = 'Test Contact Task',
								  Status = 'Completed', jl_created_from_case__c = true);
		List<Task> taskList = new List<Task> {testTask1, testTask2};
		insert taskList;
		
		clsCaseTriggerHandler.resetCaseTriggerStaticVariables();
		
		supersededCon.Superseded_By_SCV_Id__c = 'BOGUSPCDID123';
		test.startTest();
		try {
			update supersededCon;
			system.assert(false, 'Invalid PCD Id validation not executed');
		} catch (Exception ex) {
			system.assert(ex.getMessage().contains('Invalid Superseded By PCD Id specified: BOGUSPCDID123'), 'Unexpected Exception occurred - should be invalid PCD Id');
		}
		test.stopTest();
		
		system.assertEquals(false, PCDUpdateHandler.BATCH_MODE);
		system.assertEquals(true, PCDUpdateHandler.IS_RUNNING_PCD_UPDATE);

		List<Contact> testConList1 = [SELECT Id, Name, Superseded_By_Customer__c, RecordTypeId, Email, 
											(SELECT Id, Contact__c FROM ContactProfiles__r), 
											(SELECT Id, ContactId FROM Cases), 
											(SELECT Id, WhoId FROM Tasks) 
									  FROM Contact WHERE Id IN :conList];
		system.assertEquals(2, testConList1.size());
		
		for (Contact testCon : testConList1) {
			system.assertNotEquals(SUPERSEDED_RECORD_TYPE, testCon.RecordTypeId);
			system.assertEquals(null, testCon.Superseded_By_Customer__c);
			if (testCon.Id == masterCon.Id) {
				system.assertEquals(masterCon.Email, testCon.Email);
				system.assertEquals(0, testCon.ContactProfiles__r.size());
				system.assertEquals(0, testCon.Cases.size());
				system.assertEquals(0, testCon.Tasks.size());
			} else if (testCon.Id == supersededCon.Id) {
				system.assertEquals(supersededCon.Email, testCon.Email);
				system.assertEquals(1, testCon.ContactProfiles__r.size());
				system.assertEquals(1, testCon.Cases.size());
				system.assertEquals(2, testCon.Tasks.size());
			} else {
				system.assert(false);
			}
		}
    	system.debug('TEST END: testDodgyPCDReference');
	}
	
	/**
	 * Test large batch of contacts reparenting logic
	 */
	static testMethod void testTransferBatchOfContacts() {
    	system.debug('TEST START: testTransferBatchOfContacts');
        Integer SANDBOX_BATCH_SIZE = 1000;
        Integer PROD_BATCH_SIZE = 5;
        Integer batchSize = PROD_BATCH_SIZE;
		
		initialiseSettings(false);
		
       	List<Contact> masterConList = new List<Contact>();
       	List<Contact> supersededConList = new List<Contact>();
       	for (Integer i = 1; i <= batchSize; i++) {
	       	Contact masterCon = UnitTestDataFactory.createContact(i, false);
    	   	masterCon.PCD_Id__c = 'PCDMASTER' + i;
    	   	masterConList.add(masterCon);
	       	Contact supersededCon = UnitTestDataFactory.createContact(i + batchSize, false);
	       	supersededCon.PCD_Id__c = 'PCDTEST' + i;
    	   	supersededConList.add(supersededCon);
       	}
       	List<Contact> megaConList = new List<Contact>();
       	megaConList.addAll(masterConList);
       	megaConList.addAll(supersededConList);
       	insert megaConList;
       	
       	List<Case> caseList = new List<Case>();
       	List<Contact_Profile__c> cpList = new List<Contact_Profile__c>();
       	List<Task> taskList = new List<Task>();
       	for (Contact con : supersededConList) {
	       	Case testCase = UnitTestDataFactory.createQueryCase(con.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, false);
	       	caseList.add(testCase);
			Contact_Profile__c testContactProfile = new Contact_Profile__c(Contact__c = con.Id);
			cpList.add(testContactProfile);
			Task testTask1 = new Task(WhoId = con.Id, ActivityDate = Date.today(), Type = 'Case Assist', Subject = 'Assist',
									  Status = 'Completed', jl_created_from_case__c = false);
			Task testTask2 = new Task(WhoId = con.Id, ActivityDate = Date.today(), Type = 'Case Assist', Subject = 'Test Contact Task',
									  Status = 'Completed', jl_created_from_case__c = false);
       		taskList.add(testTask1);
       		taskList.add(testTask2);
       	}
       	insert caseList;
       	insert cpList;
       	insert taskList;

		clsCaseTriggerHandler.resetCaseTriggerStaticVariables();
		
       	for (Contact con : supersededConList) {
       		String newPCDId = 'PCDMASTER' + con.PCD_Id__c.substring(7);
       		con.Superseded_By_SCV_Id__c = newPCDId;
       	}

		test.startTest();
		update supersededConList;
		test.stopTest();
		
		system.assertEquals(false, PCDUpdateHandler.BATCH_MODE);
		system.assertEquals(true, PCDUpdateHandler.IS_RUNNING_PCD_UPDATE);

		List<Contact> testMasterList1 = [SELECT Id, Name, Superseded_By_Customer__c, RecordTypeId, 
												(SELECT Id, Contact__c FROM ContactProfiles__r), 
												(SELECT Id, ContactId FROM Cases), 
												(SELECT Id, WhoId FROM Tasks) 
										 FROM Contact WHERE Id IN :masterConList];
		system.assertEquals(batchSize, testMasterList1.size());
		
		for (Contact testCon : testMasterList1) {
			system.assertNotEquals(SUPERSEDED_RECORD_TYPE, testCon.RecordTypeId);
			system.assertEquals(null, testCon.Superseded_By_Customer__c);
			system.assertEquals(1, testCon.ContactProfiles__r.size());
			system.assertEquals(1, testCon.Cases.size());
			system.assertEquals(2, testCon.Tasks.size());
		}

		List<Contact> testSupersededList1 = [SELECT Id, Name, Superseded_By_Customer__c, RecordTypeId, 
													(SELECT Id, Contact__c FROM ContactProfiles__r), 
													(SELECT Id, ContactId FROM Cases), 
													(SELECT Id, WhoId FROM Tasks) 
											 FROM Contact WHERE Id IN :supersededConList];
		system.assertEquals(batchSize, testSupersededList1.size());
		
		for (Contact testCon : testSupersededList1) {
			system.assertEquals(SUPERSEDED_RECORD_TYPE, testCon.RecordTypeId);
			system.assertNotEquals(null, testCon.Superseded_By_Customer__c);
			system.assertEquals(0, testCon.ContactProfiles__r.size());
			system.assertEquals(0, testCon.Cases.size());
			system.assertEquals(0, testCon.Tasks.size());
		}
    	system.debug('TEST END: testTransferBatchOfContacts');
	}

	/**
	 * Custom setting initialisation
	 */
	private static void initialiseSettings(Boolean batchMode) {
       	UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
		Config_Settings__c c1 = new Config_Settings__c(name='PCD_UPDATE_BATCH_MODE_ONLY', Checkbox_Value__c = batchMode);
		Config_Settings__c c2 = new Config_Settings__c(name='PCD_USER_ID', Text_Value__c = UserInfo.getUserId());
		List<Config_Settings__c> configList = new List<Config_Settings__c> {c1, c2};
		insert configList;
	}	
}