global class ScheduleBatchDeleteLoyaltyAccount implements Schedulable{
   global void execute(SchedulableContext sc){
        GDPR_BatchDeleteLoyaltyAccount obj = new GDPR_BatchDeleteLoyaltyAccount();
        Database.executebatch(obj);
    }
   
}