/* Description  : Delete Live chat Transcripts when criteria satisfy in  COPT-4293
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   20/02/2019        Vignesh Kotha                   Created               COPT-4293
*
*/
global class BAT_DeleteLiveChatTranscript implements Database.Batchable<sObject>,Database.Stateful {
    global Map<Id,String> ErrorMap = new Map<Id,String>();
    global integer result=0;
    global String query;
    private static final DateTime Prior_Date_Months = System.now().addMonths(-12);
    private static final DateTime Prior_Date_Years = System.now().addyears(-6);
    private static final DateTime Prior_Date_Years_10 = System.now().addyears(-10);
    global Database.QueryLocator start(Database.BatchableContext BC){
         GDPR_Record_Limit__c gDPRLimit = GDPR_Record_Limit__c.getValues('BAT_DeleteLiveChatTranscript');
        String recordLimit = gDPRLimit.Record_Limit__c;
        Integer qLimit = Integer.valueOf(recordLimit);
       return Database.getQueryLocator([select id,recordtypeid,Number_of_Email_Contacts__c,ClosedDate,CreatedDate from case where
                                         (recordtype.name ='myJL Request' and ClosedDate < : Prior_Date_Months) OR 
                                         (recordtype.name ='Product Stock Enquiry' AND ClosedDate < : Prior_Date_Months AND Number_of_Email_Contacts__c = 0) OR
                                         (recordtype.name ='Product Stock Enquiry' AND ClosedDate < : Prior_Date_Years AND Number_of_Email_Contacts__c >= 1) OR
                                         (recordtype.name ='Order Exception' AND ClosedDate < : Prior_Date_Months AND Number_of_Email_Contacts__c = 0) OR
                                         (recordtype.name ='Order Exception' AND ClosedDate < : Prior_Date_Years AND Number_of_Email_Contacts__c >= 1) OR
                                         /* COPT-5272
                                         (recordtype.name ='PSE Sub-case' AND ClosedDate < : Prior_Date_Months AND Number_of_Email_Contacts__c = 0) OR
                                         (recordtype.name ='PSE Sub-case' AND ClosedDate < : Prior_Date_Years AND Number_of_Email_Contacts__c >= 1) OR
                                         (recordtype.name ='Email Triage' and ClosedDate < : Prior_Date_Years) OR
                                         COPT-5272 */
										 (recordtype.name ='New Case' and ClosedDate < : Prior_Date_Years) OR
                                         (recordtype.name ='NKU' and ClosedDate < : Prior_Date_Years) OR 
                                         (recordtype.name ='Query' and ClosedDate < : Prior_Date_Years) OR
                                         /* COPT-5272 
                                         (recordtype.name ='Web Triage' and ClosedDate < : Prior_Date_Years) OR
                                         COPT-5272 */
										 (recordtype.name ='Complaint' and ClosedDate < : Prior_Date_Years_10) LIMIT : qLimit
                                        ]);
    }
    global void execute(Database.BatchableContext BC, list<Case> scope){
        List<id> caselist = new List<id>();
        List<id> recordstoDelete = new List<id>();
        if(scope.size()>0){
            try{
                for(Case recordScope : scope){
                    caselist.add(recordScope.id);
                }
                for(LiveChatTranscript tansset : [select id,CaseId  from LiveChatTranscript where CaseId In :caselist]){
                    recordstoDelete.add(tansset.id);
                }
                if(recordstoDelete.size()>0){
                    list<Database.deleteResult> srList = Database.delete(recordstoDelete, false);
                    for(Integer counter = 0; counter < srList.size(); counter++) {
                        if (srList[counter].isSuccess()) {
                            result++;
                        }else {
                            for(Database.Error err : srList[counter].getErrors()) {
                                ErrorMap.put(srList.get(counter).id,err.getMessage());
                            }
                        }
                    }
                }
                // Database.emptyRecycleBin(recordstoDelete);
            }Catch(exception e){
                EmailNotifier.sendNotificationEmail('Exception from BAT_DeleteLiveChatTranscript', e.getMessage());
            }
        }
    }
    global void finish(Database.BatchableContext BC) {
        String textBody ='';
        set<Id> allIds = new set<Id>();
        textBody+= result +' records deleted in object LiveChatTranscript'+'\n';
        if (!ErrorMap.isEmpty()){
            for (Id recordids : ErrorMap.KeySet())
            {
                allIds.add(recordids);
            } 
            textBody+= 'Error log: '+allIds+'\n';
        }
        EmailNotifier.sendNotificationEmail('GDPR Delete LiveChatTranscript Records', textBody);
        BAT_DeleteCaseRecords deeltecases = new BAT_DeleteCaseRecords();
        Database.executeBatch(deeltecases);
    }
}