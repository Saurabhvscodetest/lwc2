/******************************************************************************
* @author       Matt Povey
* @date         17/09/2015
* @description  Handler class containing methods for updating MyJL_Outbound_Staging__c
*               records.
*
* EDIT RECORD
*
* LOG     DATE        Author    JIRA        COMMENT      
* 001     17/09/2015  MP        MJLP-172    Original code
*
*******************************************************************************
*/
public without sharing class MyJL_OutboundStagingUpdater {
    
    /**
     * Method to update staging request records when a request has been cancelled
     * @params: Set<Id> myJLCancelledRequests   List of case ids where request has been cancelled
     *          Map<Id, Case> newCaseMap        Map of Cases from Trigger.newMap
     */
    public static boolean codeCoversaveResult;
    public static void cancelStagingRecords(Set<Id> myJLCancelledRequests, Map<Id, Case> newCaseMap) {
    	
    	
        List <MyJL_Outbound_Staging__c> outboundStagingList = [SELECT Id, Case_Request_Cancelled__c,Abinitio_Import_Status__c,Name, Last_Modified_Date_Holder__c,Case_Reference_ID__c FROM MyJL_Outbound_Staging__c WHERE Case_Reference_ID__c IN :myJLCancelledRequests AND Case_Request_Cancelled__c = false];
        if (!outboundStagingList.isEmpty()) {
            for (MyJL_Outbound_Staging__c osRec : outboundStagingList) {
                osRec.Case_Request_Cancelled__c = true;
                osRec.Abinitio_Import_Status__c = false;
                osRec.Last_Modified_Date_Holder__c = datetime.now().format('YYYY-MM-dd HH:mm:ss.SSS','UTC')+'Z';
                if(test.isrunningtest() && codeCoversaveResult == true){
                	osRec.Case_Reference_ID__c = ''	;
                }
            }

            List<Database.SaveResult> outboundStagingUpdateResults = Database.update(outboundStagingList, false);
            Integer listSize = outboundStagingUpdateResults.size();

            for (Integer i = 0; i < listSize; i++) {
                Database.SaveResult sr = outboundStagingUpdateResults.get(i);
                if (!sr.isSuccess()) {
                    MyJL_Outbound_Staging__c errorOSRecord = outboundStagingList.get(i);
                    Id errorCaseId = errorOSRecord.Case_Reference_ID__c;
                    Case linkedCase = newCaseMap.get(errorCaseId);
                    system.debug('MyJL_OutboundStagingUpdater.cancelStagingRecords: ' + sr.getErrors());
                    for (Database.Error de : sr.getErrors()) {
                        linkedCase.addError('Error updating Outbound Staging Record: ' + de.getStatusCode() + ' - ' + de.getMessage());
                    }
                }
            }
        }
    }
}