/*
** LiveChatTranscriptTriggerHelper
**
** This class provides the trigger for linking LiveChatTranscript to Survey__c
**
**  Edit    Date        Author          Comment
**  001     10/09/14    NTJ             Original
**  002     11/09/14    NTJ             Reinstate use of LiveTranscript chatKey
**  003     28/11/14    NTJ             Remove surplus chat_guid__c field
**	004		28/11/14	NTJ				If associated with a case:
**										- JCTC-79 -> Add transcript to case comments
**										- JCTC-80 -> Increment Contact Count and Last Customer Contact Date Time 
**	005		18/12/14	NTJ				JCTC-80 -> Set the Last Chat Date
**	006		23/12/14	NTJ				JCTC-141 -> If we point the Transcript at a different case then we have to restore the 
**										last chat contact datetime and also decrement the chat count
*/
public with sharing class LiveChatTranscriptTriggerHelper {
    private final String SURVEY_REVISION = '1';

    public LiveChatTranscriptTriggerHelper() {
        
    }
    
    // NOTE: NOT ALL TRANSCRIPTS HAVE A SURVEY!!!
    // Only about 33% to 40% of transcripts will have been offered a survey and only a percentage of customers will have responded
    public void addSurveyToTranscript(List<LiveChatTranscript> newList) {
        Set<String> chatKeySet = new Set<String>();
        for (LiveChatTranscript lct:newList) {
            if (!String.isEmpty(lct.chatKey)) {
                chatKeySet.add(lct.chatKey);
            }
        }
        system.debug('chatKeySet: '+chatKeySet);

        // find the surveys
        List<Survey__c> surveyList = [SELECT Id, chat_Key__c, chat_rating_1__c, chat_question_1__c, chat_answer_1__c, revision__c, status__c FROM Survey__c WHERE chat_Key__c IN :chatKeySet];

        // make a map
        Map<String, SObject> surveyMap = makeStringMap(surveyList, 'chat_Key__c');
        system.debug('surveyMap: '+surveyMap);
             
         // loop through the transcripts and add the survey results
        for (LiveChatTranscript lct:newList) {
            system.debug('lct: '+lct);
            if (!String.isEmpty(lct.chatKey)) {
                // get the object from the map
                Survey__c survey = (Survey__c) surveyMap.get(lct.chatKey);
                system.debug('survey: '+survey);
                if (survey != null) {
                    // add the survey to the transcript
                    lct.Question_1__c = survey.Chat_Question_1__c;
                    lct.answer_1__c = survey.chat_Answer_1__c;
                    lct.rating_1__c = survey.chat_rating_1__c;
                    lct.survey_revision__c = survey.revision__c;
                    lct.Survey__c = survey.Id;
                    lct.Survey_Status__c = survey.status__c;                    
                }
            }
        }
         
    }
    
    private Map<String, SObject> makeStringMap(List<SObject> soList, String idName) {
        Map<String, SObject> soMap = new Map<String, SObject>();
        for (SObject so:soList) {
            String s = (String)so.get(idName);
            if (!String.isEmpty(s)) {
                soMap.put(s, so);               
            }
        }
        return soMap;       
    }
    
    public void redact(List<LiveChatTranscript> newList) {
        for (LiveChatTranscript lct:newList) {
            try {
                lct.Body = CreditCardMask.maskCC(lct.body);
            } catch (Exception e) {
                system.debug('could not mask cc. '+e);
            }
        }
    }
    
    // A PSE Sub Case cannot be linked to a Transcript, use the parent instead
	// This can be use for both Insert and Update
	/* COPT-5272
    public void handlePSESubCase(List<LiveChatTranscript> newList) {
		Set<Id> associatedCaseIdSet = new Set<Id>();

    	// see what cases are mentioned
    	for (LiveChatTranscript lct:newList) {
    		if (lct.CaseId != null) {
    			system.debug('Associated case: '+lct.CaseId);
    			associatedCaseIdSet.add(lct.CaseId);
    		}
    	}

		// get the cases 
		Map<Id, Case> associatedCaseMap = new Map<Id, Case>();
		if (associatedCaseIdSet.size() > 0) {     	
	    	associatedCaseMap = new Map<Id, Case>([SELECT Id, ParentId, jl_Case_Type__c FROM Case WHERE Id IN :associatedCaseIdSet]);
    	}
    	
		// update the insert list    	
    	for (LiveChatTranscript lct:newList) {
 			Case associatedCase = (lct.CaseId != null) ? associatedCaseMap.get(lct.CaseId) : null;
 			if (associatedCase != null) {
 				system.debug('Case Type: '+associatedCase.jl_Case_Type__c);
	    		if (associatedCase.jl_Case_Type__c == 'PSE Sub-case') {
					system.debug('PSE sub case: '+associatedCase.Id);
					system.debug('PSE parentId: '+associatedCase.ParentId);
					if (associatedCase.ParentId != null) {
						lct.CaseId = associatedCase.ParentId;
					}
	    		}			
 			}
     	}
    } COPT-5272 */
    
	private static final Integer MAX_COMMENT_SIZE = 4000;

    //
    // Call this in the after Insert trigger or after update trigger. Note: for update we will not delete case comments for the old case.
    // Use a utility method to add the comments if possible
    //
    public void handleCaseComments(List<LiveChatTranscript> newList, Set<Id> oldCaseIdSet) {
    	List<CaseComment> caseCommentList = new List<CaseComment>();
    	Set<Id> caseIdSet = new Set<Id>();
		
    	// loop through transcripts and create the case comment
    	for (LiveChatTranscript lct:newList) {
    		if (lct.CaseId != null) {
				// store the caseId
				caseIdSet.add(lct.CaseId);

				system.debug('Make casecomment for: '+lct.CaseId);
    			CaseComment cc = new CaseComment();
				cc.CommentBody = convertToPlainText(lct.Body);
				cc.ParentId = lct.CaseId;		
		
    			caseCommentList.add(cc);
    		}
    	}
    	
    	for (Id oldCaseId:oldCaseIdSet) {
   			CaseComment cc = new CaseComment();
			cc.CommentBody = 'Transcript moved to another case';
			cc.ParentId = oldCaseId;		
		
    		caseCommentList.add(cc);    		
    	}
    	
    	if (caseCommentList.size() > 0) {
			// Catch the error but do not stop the transcript from being added just because the case comments failed
			try {
				Database.DMLOptions dmlOpts = new Database.DMLOptions();
				dmlOpts.allowFieldTruncation = true;
				
	    		Database.insert(caseCommentList, dmlOpts);				

				// insert worked ok so calculate the counts
				calculateCounts(caseIdSet);
				calculateCounts(oldCaseIdSet);

			} catch (Exception ex) {
				system.debug('Could not add case comments for Case');
			}
    	}
    }
    
    private void calculateCounts(Set<Id> caseIdSet) {
    	// did we get any cases?
    	if (caseIdSet.size() > 0) {
			DateTime lastUpdateDtm = null;

    		List<Case> caseList = [SELECT Id,Number_of_Chat_Contacts__c, (Select CaseId, CreatedDate From LiveChatTranscripts) FROM Case WHERE Id IN :caseIdSet];

			// loop through the cases and calculate the counts
			for (Case c:caseList) {
				// Initialise the case fields
				c.Number_of_Chat_Contacts__c = 0;
				c.Last_Chat_Contact__c = null;
				
				// loop through the transcripts to set the values
				for (LiveChatTranscript lct:c.LiveChatTranscripts) {
					c.Number_of_Chat_Contacts__c++;
					c.Last_Chat_Contact__c = lastDtm(c.Last_Chat_Contact__c, lct.CreatedDate);
				}
				// 005 - Set the Last Chat Date
				//c.Last_Chat_Contact__c = lastUpdateDtm;
			}
			
			try {
				update caseList;
			} catch (Exception ex) {
				system.debug('warning: could not update cases: '+ex);
			}
    	}
    }
    
    // Because we are comparing CreatedDate we know it is never null.
    // This method returns the most recent date time
	private DateTime lastDtm(DateTime previousDtm, DateTime newDtm) {
		DateTime latestDtm = newDtm;
		
		if (previousDtm != null) {
			if (previousDtm > newDtm) {
				latestDtm = previousDtm;
			}
		} 
		return latestDtm;
	}

    private String convertToPlainText(String html) {
		String outString = '';
		if (!String.isBlank(html)) {
	    	outString = html.replace('<p>','\r\n');
			system.debug('outString1: '+outString);
	    	outString = outString.replace('</p>','');
			system.debug('outString2: '+outString);
	    	outString = outString.replace('<p align="center">','\r\n');
			system.debug('outString3: '+outString);
	    	outString = outString.replace('<br>','\r\n');
			system.debug('outString4: '+outString);			
		}
    	return outString;
    }
    
	private List<LiveChatTranscript> getChangedList(List<LiveChatTranscript> oldList, Map<Id, LiveChatTranscript> newMap) {
    	List<LiveChatTranscript> changedList = new List<LiveChatTranscript>();
    	
    	for(LiveChatTranscript oldLct:oldList) {
    		LiveChatTranscript newLct = (LiveChatTranscript)newMap.get(oldLct.Id);
    		if (newLct != null) {
    			// only process if caseId changed
    			if (newLct.CaseId != oldLct.CaseId) {
    				changedList.add(newLct);
    			}
    		}
    	}
    	system.debug('getChangedList: size: '+changedList.size());
		return changedList;		
	}

    //
    // Having decided that a Transcript needs to be handled (because its case has changed) then get a set of the CaseIds in the OldMap
    //
    private Set<Id> getOldCases(List<LiveChatTranscript> changedList, Map<Id, LiveChatTranscript> oldMap) {
		Set<Id> oldCaseIdSet = new Set<Id>();

		for (LiveChatTranscript newLct:changedList) {
			LiveChatTranscript oldLct = (LiveChatTranscript) oldMap.get(newLct.Id);
			if (oldLct != null) {
				if (oldLct.CaseId != null) {
					oldCaseIdSet.add(oldLct.CaseId);
				}
			}
		}

    	return oldCaseIdSet;	
    }
    
    // methods that first check update changes
    /* COPT-5272
    public void updatePSESubCase(List<LiveChatTranscript> oldList, Map<Id, LiveChatTranscript> newMap) {
    	List<LiveChatTranscript> changedList = getChangedList(oldList, newMap);
    	
    	handlePSESubCase(changedList);
    } COPT-5272 */

    // methods that first check update changes
    public void updateCaseComments(List<LiveChatTranscript> oldList, Map<Id, LiveChatTranscript> oldMap, Map<Id, LiveChatTranscript> newMap) {
    	List<LiveChatTranscript> changedList = getChangedList(oldList, newMap);
    	
    	Set<Id> oldCaseIdSet = getOldCases(changedList, oldMap);
    	
    	handleCaseComments(changedList, oldCaseIdSet);
    }
    
    public void linkRelatedCustomerRecord ( Map < Id, LiveChatTranscript > chatTranscriptMap ) {
        CRSearchCustomerUtils.searchCustomerRecord ( chatTranscriptMap );
    }
}