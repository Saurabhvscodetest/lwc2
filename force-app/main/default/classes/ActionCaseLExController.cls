public class ActionCaseLExController {

	@AuraEnabled
	public static Case getCaseDetails(String caseId){
		return [SELECT RecordType.Name, RecordTypeId, Status FROM Case WHERE Id = :caseId LIMIT 1][0];

	}

	@AuraEnabled
	public static Boolean isPermSetAssignedToUser(String permSetName){
		User currentUser = CommonStaticUtils.getCurrentUser();
		return CommonStaticUtils.isPermissionSetAssignedToSingleUserByName(currentUser.Id, permSetName);
	}

	@AuraEnabled
	public static void closeOutstandingTasksWhenCaseClosed(String caseId){
		List<Case> currentCase = [SELECT Id, jl_NumberOutstandingTasks__c, RecordType.Name
							      FROM Case 
							      WHERE Id =: caseId AND jl_NumberOutstandingTasks__c > 0 LIMIT 1];

		if(currentCase.size()>0 && (currentCase[0].RecordType.Name == 'Complaint' || currentCase[0].RecordType.Name == 'Query'
			 || currentCase[0].RecordType.Name == 'Product Stock Enquiry' ) ){
			List<Task> outstandingTasks = [SELECT Id, Description
									       FROM Task 
									   	   WHERE IsClosed = FALSE AND WhatId = :caseId LIMIT 100];

			if(outstandingTasks.size() > 0){
				for(Task aTask: outstandingTasks){
					aTask.Status = 'Closed - Resolved';
					aTask.Description = 'Task closed as part of bulk closure';
				}
				System.debug('inside task closure');
				try{
					currentCase[0].jl_NumberOutstandingTasks__c = 0;
					update currentCase;

					update outstandingTasks;

				}catch(Exception ex){
						System.debug('An error has occured when closing outstandingTasks from action case component '+ex.getMessage());
				}
			}else{
				currentCase[0].jl_NumberOutstandingTasks__c = 0;
				try{
					update currentCase;
				}catch(Exception ex){
						System.debug('An error has occured when updating case from action case component '+ex.getMessage());
			}
		}
		}					      
		
		
	}
}