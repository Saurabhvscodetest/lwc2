/**
* @description	Class encapsulating the logic for reopening Cases. Based on the JS Re-Open button.
*				Not bulkified.			
* @author		@tomcarman
*/

public class CaseReopenUtilities {    
    
    public static Set<String> errorMessages;    
    private static String userTeam;
    private static User currentUser;
    private static Case caseRecord;
    private static Set<String> CASE_RECORDTYPE_TO_BE_OWNED_BY_QUEUE = new Set<String>{'Product Stock Enquiry', 'Email Triage', 'Web Triage', 'NKU'};
    Public Static String BRANCH = 'Branch';
    Public Static String ACTIONED = '_ACTIONED';
    Public Static String FAILED = '_FAILED';
    Public Static String ROBOT = 'Robot';

/**
* @description	Main reopen entry point			
* @author		@tomcarman
* @param		caseId    Id of Case to reopen
* @return		Case 	  Case with correct reopen values, ready to be updated
*/
        
        public static Case reopenCase(Id caseId) {
            
            userTeam = null;
            errorMessages = new Set<String>();
            
            caseRecord = [SELECT Id, RecordType.Name, JL_Last_Queue_Name__c, Close_Status__c, Final_Case_Activity_Resolution_Method__c FROM Case WHERE Id = :caseId];
            currentUser = getCurrentUser();
            caseRecord.Status = 'Reopened';
            caseRecord.JL_Action_Taken__c = 'New case created';
            caseRecord.Close_Status__c = '';
            caseRecord.Final_Case_Activity_Resolution_Method__c = '';
            caseRecord.JL_ReOpened__c = true;
            
            if(CASE_RECORDTYPE_TO_BE_OWNED_BY_QUEUE.contains(caseRecord.RecordType.Name)) {
                reopenCaseAsQueue();
            } else if (currentUser.Tier__c == 1) {
                reopenCaseAsTier1User();
            } else {
                reopenCaseAsUser();
            }
            
            if(errorMessages.isEmpty()) {
                return caseRecord;
            } else {
                return null;
            }
        }
    
    /**
* @description	Logic specific to reopening a Case as a Tier 1 User			
* @author		@tomcarman
*/
    private static void reopenCaseAsTier1User() {
        
        if(caseRecord.JL_Last_Queue_Name__c != null) {
            
            caseRecord.OwnerId = getQueueId(caseRecord.JL_Last_Queue_Name__c);
            
        } else if (getUserTeamQueue(currentUser) != null) {
            
            caseRecord.JL_Last_Queue_Name__c = getUserTeamQueue(currentUser);
            caseRecord.OwnerId = getQueueId(getUserTeamQueue(currentUser));
            
        } else {
            errorMessages.add('You cannot re-open a case as a Tier 1 user if you are not assigned to a Primary Team.');
            
        }
        
    }
    
/**
* @description	Logic for opening Case as a non-Tier 1 user			
* @author		@tomcarman
*/
    private static void reopenCaseAsUser() {
        caseRecord.OwnerId = UserInfo.getUserId();
        caseRecord.JL_Last_Queue_Name__c = getUserTeamQueue(currentUser);
        caseRecord.JL_Reopen_As_User__c = true;
    }
    
    /**
* @description	Logic for Cases which should never be owned by a User -> eg they get assigned back to a Queue			
* @author		@tomcarman	
*/
    private static void reopenCaseAsQueue() {
        caseRecord.OwnerId = getQueueId(caseRecord.JL_Last_Queue_Name__c);  
    }
    /* Helpers */
    public static User getCurrentUser() {
        
        if(currentUser == null) {
            currentUser = [SELECT Id, Tier__c, Team__c FROM User WHERE Id = :UserInfo.getUserId()]; 
        }
        return currentUser;
    }
    
    public static String getUserTeamQueue(User currentUser) {
        if(userTeam == null) {
            List<Team__c> userTeams = [SELECT Queue_Name__c FROM Team__c WHERE Name = :currentUser.Team__c];
            
            if(!userTeams.isEmpty() && userTeams[0].Queue_Name__c != null) {
                userTeam = userTeams[0].Queue_Name__c;
            }
        }
        return userTeam;
    }
    
    public static Id getQueueId(String queueName) {
        
        try{
      ReOpenUtilityQueue__mdt[] reopenMTdate =  [SELECT DeveloperName,QueueName__c FROM ReOpenUtilityQueue__mdt order by DeveloperName];
            if(queueName != NULL){
                if(queueName.containsIgnoreCase(ROBOT)){
             //String RobotQueue = JLCom_Robot_Held_Orders_Exceptions;
             String RobotQueue = reopenMTdate[4].QueueName__c;
             if(queueName.equals(RobotQueue)){
                 String RobotQueueReplace = reopenMTdate[1].QueueName__c;
                 return [SELECT Queue.Id FROM QueueSObject WHERE Queue.DeveloperName = :RobotQueueReplace].QueueId;
             }
				else {
                 String BackOfficeNKUQueue = reopenMTdate[0].QueueName__c;
                 return [SELECT Queue.Id FROM QueueSObject WHERE Queue.DeveloperName = :BackOfficeNKUQueue].QueueId;
             }
        }
        
        else if(queueName.contains(FAILED)){
            String ActQueue = queueName.removeEnd(FAILED) + ACTIONED;
            return [SELECT Queue.Id FROM QueueSObject WHERE Queue.DeveloperName = :ActQueue].QueueId;
        }        
        else if(!(queueName.containsIgnoreCase(FAILED)) && !(queueName.containsIgnoreCase(ACTIONED)) && !(queueName.containsIgnoreCase(BRANCH)) && !(queueName.containsIgnoreCase(ROBOT)) ){
            String ActQueue1 = queueName + ACTIONED;
            return [SELECT Queue.Id FROM QueueSObject WHERE Queue.DeveloperName = :ActQueue1].QueueId;
        }
        else{
             return [SELECT Queue.Id FROM QueueSObject WHERE Queue.DeveloperName = :queueName].QueueId;
        }
            }
            else{
                return Userinfo.getUserId();
            }
         
        } 
    Catch(Exception e){
        
        return [SELECT Queue.Id FROM QueueSObject WHERE Queue.DeveloperName = :queueName].QueueId;
    }
    }
}