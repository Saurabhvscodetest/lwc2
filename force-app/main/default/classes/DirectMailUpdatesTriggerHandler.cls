/**
* Author:       Nawaz Ahmed
* Date:         15/09/2015
* Description:  Trigger handler class for  Direct Mail Update
*
* ******************* Change Log *******************
* Modified by       Change Date     Change
* Nawaz Ahmed	     13/08/2015     Initial Version (MJLP-331)
* Llyr Jones 		 11/11/2015 	Renamed Class
*
*/

public with sharing class DirectMailUpdatesTriggerHandler {
	
	//Variable to hold the caseids
	public static set<id> caseids = new set<id>();
	public static Boolean is_jl_request_update =false;
	
	public static set<id> duplicateRec = new set<id>();
	public static set<string> uniqids = new set<string>();
	public static set<string> getdupunqids = new set<string>();
	public static map<string,Direct_Mail_Update__c> originalRec = new map<string,Direct_Mail_Update__c>();
	public static list<Direct_Mail_Update__c> updatelist = new list<Direct_Mail_Update__c>();
	public static map<id,Direct_Mail_Update__c> casetodst = new map<id,Direct_Mail_Update__c>();
	public static map<id,Direct_Mail_Update__c> casetodsttracking = new map<id,Direct_Mail_Update__c>();
	public static list<Case> updateCasesonInsert = new list<Case>();
	public static list<Case> updateCasesonUpdate = new list<Case>();
	
		
	public static void mainEntry(Boolean isBefore, 
                               Boolean isDelete, Boolean isAfter, 
                               Boolean isInsert, Boolean isUpdate, 
                               Boolean isExecuting, List<Direct_Mail_Update__c> newlist, 
                               Map<ID,sobject> newmap, List<Direct_Mail_Update__c> oldlist, 
                               Map<Id,sobject> oldmap){
                               	
     	if(isBefore && isInsert){
     		
     		//if case reference is null set te Mailing type to New welcome pack
     		for(Direct_Mail_Update__c getdms:newlist){
     			if(getdms.Case_Reference_ID__c == null){
     				getdms.Mailing_Type__c = 'New Welcome Pack';
     				if(getdms.MYJL_Membership_Number__c!=null){
     					getdms.Unique_Identifier__c = getdms.MYJL_Membership_Number__c+'|'+'New Welcome Pack';
     				}
     			}
     			else{
     				caseids.add(getdms.Case_Reference_ID__c);	
     				
     			}
     		}
     		
     		//Get the case details with request type
     		Map<id,case> casemap = new map<id,case>( [select id,request_type__c from case where id in:caseids]);
     		
     		//If Case reference is not null get the mailing type from the case
     		for(Direct_Mail_Update__c getdms:newlist){
     			if(getdms.Case_Reference_ID__c != null){
     				getdms.Mailing_Type__c = casemap.get(getdms.Case_Reference_ID__c).request_type__c;	
     				getdms.Unique_Identifier__c = getdms.Case_Reference_ID__c+'|'+getdms.MYJL_Membership_Number__c+'|'+casemap.get(getdms.Case_Reference_ID__c).request_type__c;
     			}	
     		}
     	}
     	
     	else if(isafter && isinsert){
     		
     		//retrieve the list of unique identifiers
     		for(Direct_Mail_Update__c getdms:newlist){
     			if(getdms.Unique_Identifier__c!=null){
     				uniqids.add(getdms.Unique_Identifier__c);		
     			}	
     			
     			//Construct a map with case reference to direct mail updates to update the case with despatch details later
     			if(getdms.Case_Reference_ID__c !=null){
     				casetodst.put(getdms.Case_Reference_ID__c,getdms);
     			}		
     		}	
     		
     		//query for inbounds staging with unique identifier
     		List<Direct_Mail_Update__c> getInstdetails = new list<Direct_Mail_Update__c>([select id,Address_LIne_1__c,Address_LIne_2__c,Address_LIne_3__c,Address_LIne_4__c,Address_LIne_5__c,
     		                                                                              Address_LIne_6__c,Case_Reference_ID__c,Despatch_Status_c__c,Despatched__c,DST_File_Name__c,Duplicate__c,
     		                                                                              Email_Address__c,Fast_track__c,First_Name__c,Initial__c,International__c,Issue_Date__c,Issue_Status__c,
     		                                                                              Last_Name__c,Long_Name__c,Mailing_Type__c,MYJL_Barcode_NUmber__c,MYJL_Membership_Number__c,Post_Code__c,
     		                                                                              Record_Removed__c,Shopper_ID__c,Title__c,Town__c,Tracking_Number__c,Unique_Identifier__c,Valid_From__c,
     		                                                                              Valid_To__c,Voucher_Type__c from Direct_Mail_Update__c where Unique_Identifier__c in:uniqids
     		                                                                              order by createddate,Unique_Identifier__c asc]);
     		
     		//Browse the list to get duplicates
     		for(Direct_Mail_Update__c getdms1:getInstdetails){
     			system.debug('getdms1.Unique_Identifier__c'+getdms1.Unique_Identifier__c);
     			system.debug('set'+getdupunqids);
     			
     			
     			if(!getdupunqids.contains(getdms1.Unique_Identifier__c)){
     				getdupunqids.add(getdms1.Unique_Identifier__c);	
     				originalRec.put(getdms1.Unique_Identifier__c,getdms1);
     			}
     			else{
     				duplicateRec.add(getdms1.id);	
     			    
     				//Update the master record
     				system.debug('enterhere');
     				
     				if(originalRec.containskey(getdms1.Unique_Identifier__c)){
	     				Direct_Mail_Update__c tempdms = originalRec.get(getdms1.Unique_Identifier__c) ;
						tempdms.Address_LIne_1__c  =    getdms1.Address_LIne_1__c!=null ? getdms1.Address_LIne_1__c: tempdms.Address_LIne_1__c;
						tempdms.Address_LIne_2__c  =    getdms1.Address_LIne_2__c!=null ? getdms1.Address_LIne_2__c: tempdms.Address_LIne_2__c;
						tempdms.Address_LIne_3__c  =    getdms1.Address_LIne_3__c!=null ? getdms1.Address_LIne_3__c: tempdms.Address_LIne_3__c;
						tempdms.Address_LIne_4__c  =    getdms1.Address_LIne_4__c!=null ? getdms1.Address_LIne_4__c: tempdms.Address_LIne_4__c;
						tempdms.Address_LIne_5__c  =    getdms1.Address_LIne_5__c!=null ? getdms1.Address_LIne_5__c: tempdms.Address_LIne_5__c;
						tempdms.Address_LIne_6__c  =    getdms1.Address_LIne_6__c!=null ? getdms1.Address_LIne_6__c: tempdms.Address_LIne_6__c;
						tempdms.Case_Reference_ID__c  =    getdms1.Case_Reference_ID__c!=null ? getdms1.Case_Reference_ID__c: tempdms.Case_Reference_ID__c;
						tempdms.Despatch_Status_c__c  =    getdms1.Despatch_Status_c__c!=null ? getdms1.Despatch_Status_c__c: tempdms.Despatch_Status_c__c;
						tempdms.Despatched__c  =    getdms1.Despatched__c!=null ? getdms1.Despatched__c: tempdms.Despatched__c;
						tempdms.DST_File_Name__c  =    getdms1.DST_File_Name__c!=null ? getdms1.DST_File_Name__c: tempdms.DST_File_Name__c;
						tempdms.Duplicate__c  =    getdms1.Duplicate__c;
						tempdms.Email_Address__c  =    getdms1.Email_Address__c!=null ? getdms1.Email_Address__c: tempdms.Email_Address__c;
						tempdms.Fast_track__c  =    getdms1.Fast_track__c;
						tempdms.First_Name__c  =    getdms1.First_Name__c!=null ? getdms1.First_Name__c: tempdms.First_Name__c;
						tempdms.Initial__c  =    getdms1.Initial__c!=null ? getdms1.Initial__c: tempdms.Initial__c;
						tempdms.International__c  =    getdms1.International__c;
						tempdms.Issue_Date__c  =    getdms1.Issue_Date__c!=null ? getdms1.Issue_Date__c: tempdms.Issue_Date__c;
						tempdms.Issue_Status__c  =    getdms1.Issue_Status__c!=null ? getdms1.Issue_Status__c: tempdms.Issue_Status__c;
						tempdms.Last_Name__c  =    getdms1.Last_Name__c!=null ? getdms1.Last_Name__c: tempdms.Last_Name__c;
						tempdms.Long_Name__c  =    getdms1.Long_Name__c!=null ? getdms1.Long_Name__c: tempdms.Long_Name__c;
						tempdms.Mailing_Type__c  =    getdms1.Mailing_Type__c!=null ? getdms1.Mailing_Type__c: tempdms.Mailing_Type__c;
						tempdms.MYJL_Barcode_NUmber__c  =    getdms1.MYJL_Barcode_NUmber__c!=null ? getdms1.MYJL_Barcode_NUmber__c: tempdms.MYJL_Barcode_NUmber__c;
						tempdms.MYJL_Membership_Number__c  =    getdms1.MYJL_Membership_Number__c!=null ? getdms1.MYJL_Membership_Number__c: tempdms.MYJL_Membership_Number__c;
						tempdms.Post_Code__c  =    getdms1.Post_Code__c!=null ? getdms1.Post_Code__c: tempdms.Post_Code__c;
						tempdms.Record_Removed__c  =    getdms1.Record_Removed__c;
						tempdms.Shopper_ID__c  =    getdms1.Shopper_ID__c!=null ? getdms1.Shopper_ID__c: tempdms.Shopper_ID__c;
						tempdms.Title__c  =    getdms1.Title__c!=null ? getdms1.Title__c: tempdms.Title__c;
						tempdms.Town__c  =    getdms1.Town__c!=null ? getdms1.Town__c: tempdms.Town__c;
						tempdms.Tracking_Number__c  =    getdms1.Tracking_Number__c!=null ? getdms1.Tracking_Number__c: tempdms.Tracking_Number__c;
						tempdms.Unique_Identifier__c  =    getdms1.Unique_Identifier__c!=null ? getdms1.Unique_Identifier__c: tempdms.Unique_Identifier__c;
						tempdms.Valid_From__c  =    getdms1.Valid_From__c!=null ? getdms1.Valid_From__c: tempdms.Valid_From__c;
						tempdms.Valid_To__c  =    getdms1.Valid_To__c!=null ? getdms1.Valid_To__c: tempdms.Valid_To__c;
						tempdms.Voucher_Type__c =    getdms1.Voucher_Type__c!=null ? getdms1.Voucher_Type__c: tempdms.Voucher_Type__c;
						updatelist.add(tempdms);
     				}
     			}
     			
     		}
     		
     		if(updatelist.size() > 0){
     				update updatelist;
     		}
     	    
     	    if(duplicateRec.size() > 0){
     			deleteSObjects(duplicateRec,'Direct_Mail_Update__c');
     	    }
     	    
     	    if(casetodst.keyset().size() >0){
     	    	updateCaseDespatchDateandStatus(casetodst);
     	    }
     		
     		getdupunqids.clear();
     		
     	} else if(isafter && isupdate){
     		for(Direct_Mail_Update__c getdms:newlist){
     			String oldTrackingNumber = ((Direct_Mail_Update__c) oldMap.get(getdms.id)).Tracking_Number__c;
     			
     			if(getdms.Tracking_Number__c !=null && oldTrackingNumber==null){
     				
     				casetodsttracking.put(getdms.Case_Reference_ID__c,getdms);		
     			}	
     		}
     		
     		if(casetodsttracking.keyset().size() > 0){
     			updateCaseTrackingNumber(casetodsttracking);	
     		}
     	}                          	
                               	
     }
     
     
    //method to update the case with despatch details
    public static void updateCaseDespatchDateandStatus (map<id,Direct_Mail_Update__c> dstdetailsforcase){ 
    	
    	
    	for(Case retrieveCases : [select id,Despatch_Date__c,Despatch_Status__c,Tracking_Number__c,DST_MAILED_TITLE__c,DST_MAILED_FORENAME__c,
    	                                 DST_MAILED_INITIALS__c,DST_MAILED_SURNAM__c,DST_MAILED_ADDRESS1__c,DST_MAILED_ADDRESS2__c,DST_MAILED_ADDRESS3__c,
    	                                 DST_MAILED_ADDRESS4__c,DST_MAILED_TOWN__c,DST_MAILED_COUNTY__c,DST_MAILED_POSTCODE__c,MyJL_Request_Record_Locked__c from case where id in:dstdetailsforcase.keyset()]){
    	                                 	
			retrieveCases.Despatch_Date__c = retrieveCases.Despatch_Date__c == null ? dstdetailsforcase.get(retrieveCases.id).Despatched__c:retrieveCases.Despatch_Date__c;
			retrieveCases.Despatch_Status__c = 'Out for Delivery';
			retrieveCases.DST_MAILED_TITLE__c =  retrieveCases.DST_MAILED_TITLE__c == null ? dstdetailsforcase.get(retrieveCases.id).Title__c:retrieveCases.DST_MAILED_TITLE__c;
			retrieveCases.DST_MAILED_FORENAME__c =  retrieveCases.DST_MAILED_FORENAME__c == null ? dstdetailsforcase.get(retrieveCases.id).First_Name__c:retrieveCases.DST_MAILED_FORENAME__c;
			retrieveCases.DST_MAILED_INITIALS__c = retrieveCases.DST_MAILED_INITIALS__c  == null ? dstdetailsforcase.get(retrieveCases.id).Initial__c:retrieveCases.DST_MAILED_INITIALS__c;
			retrieveCases.DST_MAILED_SURNAM__c = retrieveCases.DST_MAILED_SURNAM__c  == null ? dstdetailsforcase.get(retrieveCases.id).Last_Name__c:retrieveCases.DST_MAILED_SURNAM__c;
			retrieveCases.DST_MAILED_ADDRESS1__c =  retrieveCases.DST_MAILED_ADDRESS1__c == null ? dstdetailsforcase.get(retrieveCases.id).Address_LIne_1__c:retrieveCases.DST_MAILED_ADDRESS1__c;
			retrieveCases.DST_MAILED_ADDRESS2__c = retrieveCases.DST_MAILED_ADDRESS2__c  == null ? dstdetailsforcase.get(retrieveCases.id).Address_LIne_2__c:retrieveCases.DST_MAILED_ADDRESS2__c;
			retrieveCases.DST_MAILED_ADDRESS3__c = retrieveCases.DST_MAILED_ADDRESS3__c  == null ? dstdetailsforcase.get(retrieveCases.id).Address_LIne_3__c:retrieveCases.DST_MAILED_ADDRESS3__c;
			retrieveCases.DST_MAILED_ADDRESS4__c = retrieveCases.DST_MAILED_ADDRESS4__c  == null ? dstdetailsforcase.get(retrieveCases.id).Address_LIne_4__c:retrieveCases.DST_MAILED_ADDRESS4__c;
			retrieveCases.DST_MAILED_TOWN__c = retrieveCases.DST_MAILED_TOWN__c  == null ? dstdetailsforcase.get(retrieveCases.id).Address_LIne_5__c:retrieveCases.DST_MAILED_TOWN__c;
			retrieveCases.DST_MAILED_COUNTY__c = retrieveCases.DST_MAILED_COUNTY__c  == null ? dstdetailsforcase.get(retrieveCases.id).Town__c:retrieveCases.DST_MAILED_COUNTY__c;
			retrieveCases.DST_MAILED_POSTCODE__c  = retrieveCases.DST_MAILED_POSTCODE__c  == null ? dstdetailsforcase.get(retrieveCases.id).Post_Code__c:retrieveCases.DST_MAILED_POSTCODE__c;
			retrieveCases.MyJL_Request_Record_Locked__c   = true; 
			retrieveCases.status = 'Closed - Resolved';    		                                 	
    		updateCasesonInsert.add(retrieveCases);  
    		
    		if(dstdetailsforcase.get(retrieveCases.id).Record_Removed__c == true && dstdetailsforcase.get(retrieveCases.id).Duplicate__c==false && 
    		   dstdetailsforcase.get(retrieveCases.id).Long_Name__c == false && dstdetailsforcase.get(retrieveCases.id).International__c==false){
    			retrieveCases.Despatch_Status__c = 'Not Sent';	
    		}
    		
    		if(dstdetailsforcase.get(retrieveCases.id).Record_Removed__c == true &&  dstdetailsforcase.get(retrieveCases.id).Long_Name__c == true ){
    			retrieveCases.Despatch_Status__c = 'Processing';	
    		}    		
    	}
    	
    	if(updateCasesonInsert.size() > 0){
    		is_jl_request_update = true;
    		update updateCasesonInsert;
    		updateCasesonInsert.clear();
    		is_jl_request_update = false;
    	}
    	
    }
    
    //method to update the case with Tracking Number
    public static void updateCaseTrackingNumber (map<id,Direct_Mail_Update__c> dstdetailsforcaseTracking){
    	system.debug('dstdetailsforcaseTracking'+dstdetailsforcaseTracking);
    	for(Case retrieveCases : [select id,Despatch_Date__c,Despatch_Status__c,Tracking_Number__c,DST_MAILED_TITLE__c,DST_MAILED_FORENAME__c,
    	                                 DST_MAILED_INITIALS__c,DST_MAILED_SURNAM__c,DST_MAILED_ADDRESS1__c,DST_MAILED_ADDRESS2__c,DST_MAILED_ADDRESS3__c,
    	                                 DST_MAILED_ADDRESS4__c,DST_MAILED_TOWN__c,DST_MAILED_COUNTY__c,DST_MAILED_POSTCODE__c,MyJL_Request_Record_Locked__c  
    	                                 from case where id in:dstdetailsforcaseTracking.keyset()]){
    	    system.debug('retrieveCases.id'+retrieveCases.id);                             	
			retrieveCases.Tracking_Number__c = 	 dstdetailsforcaseTracking.get(retrieveCases.id).Tracking_Number__c;  
		    retrieveCases.Despatch_Date__c = dstdetailsforcaseTracking.get(retrieveCases.id).Despatched__c;
			retrieveCases.Despatch_Status__c = 'Out for Delivery';
			retrieveCases.DST_MAILED_TITLE__c = dstdetailsforcaseTracking.get(retrieveCases.id).Title__c;
			retrieveCases.DST_MAILED_FORENAME__c = dstdetailsforcaseTracking.get(retrieveCases.id).First_Name__c;
			retrieveCases.DST_MAILED_INITIALS__c = dstdetailsforcaseTracking.get(retrieveCases.id).Initial__c;
			retrieveCases.DST_MAILED_SURNAM__c = dstdetailsforcaseTracking.get(retrieveCases.id).Last_Name__c;
			retrieveCases.DST_MAILED_ADDRESS1__c = dstdetailsforcaseTracking.get(retrieveCases.id).Address_LIne_1__c;
			retrieveCases.DST_MAILED_ADDRESS2__c = dstdetailsforcaseTracking.get(retrieveCases.id).Address_LIne_2__c;
			retrieveCases.DST_MAILED_ADDRESS3__c = dstdetailsforcaseTracking.get(retrieveCases.id).Address_LIne_3__c;
			retrieveCases.DST_MAILED_ADDRESS4__c = dstdetailsforcaseTracking.get(retrieveCases.id).Address_LIne_4__c;
			retrieveCases.DST_MAILED_TOWN__c = dstdetailsforcaseTracking.get(retrieveCases.id).Address_LIne_5__c;
			retrieveCases.DST_MAILED_COUNTY__c = dstdetailsforcaseTracking.get(retrieveCases.id).Town__c;
			retrieveCases.DST_MAILED_POSTCODE__c  = dstdetailsforcaseTracking.get(retrieveCases.id).Post_Code__c; 	
			retrieveCases.MyJL_Request_Record_Locked__c   = true;                               	
    	    updateCasesOnUpdate.add(retrieveCases);         
    	    system.debug('updateCasesOnUpdate'+updateCasesOnUpdate)  ;                   	
    	 }
    	
    	if(updateCasesOnUpdate.size() > 0){
    		system.debug('updateCasesOnUpdate'+updateCasesOnUpdate)  ;
    		is_jl_request_update = true;   
    		update updateCasesOnUpdate;
    		is_jl_request_update = false;
    		updateCasesOnUpdate.clear();
    	}    	 
    	
    } 
    
    //Method to delete the duplicate records as future
     @future 
	public static void deleteSObjects(Set<Id> idSet, String objType) {
	List<SObject> objectList = new List<SObject>();
	for (Id sid : idSet) {
		sObject obj = Schema.getGlobalDescribe().get(objType).newSObject(sid);
		objectList.add(obj);
	}
	delete objectList;
}

}