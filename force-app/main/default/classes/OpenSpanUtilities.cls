public with sharing class OpenSpanUtilities {
    
    public static Boolean validateCaseMatchingKeys(Data_Hub__c dataHubRecord){
        
        //MJS: 03Oct2014 - modified to use the central ContactUtils key validation function
        //note: not sure if this is used now as I have implemented function to validate each key individually
        //but updated this one in case it is used somewhere I am not aware of
        
        return ContactUtils.validateCaseMatchingKeys(dataHubRecord.First_Name__c, dataHubRecord.Surname__c, dataHubRecord.Email__c, dataHubRecord.Address1__c, dataHubRecord.Post_Code__c);
        
    }
    
    
    public static Data_Hub__c validateCase(Data_Hub__c dataHubRecord){
        // TODO: clean null value
        Boolean rejected = false;
        String validationResults = '';
        
        /* accept the data hub record (opposite from openspan transformer), in order to be able to create an orphan case
        if(validateCaseMatchingKeys(dataHubRecord)){
            rejected = true;
            validationResults += 'Matching Key incomplete for a closed Case\n';
        }
        */
        
        String valResult = validateOpenSpanCQM2SFDCCMMatch(dataHubRecord);
        if(rejected == false &&  valResult != null){
            rejected = true;
            validationResults = valResult + '\n';
            validationResults += 'No mapping found for the three input values (Query Reason and Action By)\n';
            //validationResults += validationResults + '\n';
        }
        
        if(rejected == false && validateEmail(dataHubRecord.Email__c) == false){
            rejected = true;
            validationResults += 'Email address is not in the correct format. : ' + dataHubRecord.Email__c;
        }
        
        if(rejected){
            dataHubRecord = new Data_Hub__c(Id = dataHubRecord.Id);
            dataHubRecord.Status__c = OpenSpanDataHubTransformer.OPENSPAN_REJECTED_CASE;
            dataHubRecord.Error_Messages__c = validationResults;
        }
        
        
        return dataHubRecord;
            
     }
    
    public static Case matchCaseByKey1(Data_Hub__c dataHubRecord){

        //MJS: 03Oct2014 - updated to use key composition function from Central ContactUtils
        //note: when called from OpenSpanDataHubTransformer then we know the key is valid before we get here
         
        return convertToSFDCCase(dataHubRecord, new Contact(Case_Matching_Key_1__c = ContactUtils.createMatchKey1(dataHubRecord.First_Name__c, dataHubRecord.Surname__c, dataHubRecord.Email__c)));
    }
     
    public static Case matchCaseByKey2(Data_Hub__c dataHubRecord){
               
        //MJS: 03Oct2014 - updated to use key composition function from Central ContactUtils
        //note: when called from OpenSpanDataHubTransformer then we know the key is valid before we get here
        
        return convertToSFDCCase(dataHubRecord, new Contact(Case_Matching_Key_2__c = ContactUtils.createMatchKey2(dataHubRecord.First_Name__c, dataHubRecord.Surname__c, dataHubRecord.Address1__c, dataHubRecord.Post_Code__c)));
    }
    
    public static Case matchCaseByKey3(Data_Hub__c dataHubRecord){
        
        //MJS: 03Oct2014 - updated to use key composition function from Central ContactUtils
        //note: when called from OpenSpanDataHubTransformer then we know the key is valid before we get here
        
        return convertToSFDCCase(dataHubRecord, new Contact_Profile__c(Case_Matching_Key_1__c = ContactUtils.createMatchKey1(dataHubRecord.First_Name__c, dataHubRecord.Surname__c, dataHubRecord.Email__c)));
    }
    
    public static Case matchCaseByKey4(Data_Hub__c dataHubRecord){
        
        //MJS: 03Oct2014 - updated to use key composition function from Central ContactUtils
        //note: when called from OpenSpanDataHubTransformer then we know the key is valid before we get here
        
        return convertToSFDCCase(dataHubRecord, new Contact_Profile__c(Case_Matching_Key_2__c = ContactUtils.createMatchKey2(dataHubRecord.First_Name__c, dataHubRecord.Surname__c, dataHubRecord.Address1__c, dataHubRecord.Post_Code__c)));
    }
    
    public static Case createOrphanCase(Data_Hub__c dataHubRecord){
        
        return convertToSFDCCase(dataHubRecord);
    }
    
    public static Case convertToSFDCCase(Data_Hub__c dataHubRecord, Contact pContact){
        
        Case returnCase = convertToSFDCCase(dataHubRecord);
        returnCase.contact = pContact;
        return returnCase;
                
     }
     
    public static Case convertToSFDCCase(Data_Hub__c dataHubRecord, Contact_Profile__c pContactProfile){
        
        Case returnCase = convertToSFDCCase(dataHubRecord);
        returnCase.Contact_Profile__r = pContactProfile;
        return returnCase;
                
     }
     
     /*public static Case linkCaseWithCustomer(Case pCase, Contact_Profile__c pContactProfile){
        List<Contact_Profile__c> contProfList = [SELECT Id, Contact__c 
                                        FROM Contact_Profile__c 
                                        WHERE Id =: pContactProfile.Id];
        if(!contProfList.isEmpty()){
            pCase.ContactId = contProfList.iterator().next().Contact__c;
        }
        
        return pCase;
     }*/
     
     /*public static Case linkCaseWithCustomer(Case pCase){
        List<Contact_Profile__c> contProfList = [SELECT Id, Contact__c 
                                        FROM Contact_Profile__c 
                                        WHERE Id =: pCase.Contact_Profile__r.Id];
        if(!contProfList.isEmpty()){
            pCase.ContactId = contProfList.iterator().next().Contact__c;
        }
        
        return pCase;
     }*/
     
     public static Case convertToSFDCCase(Data_Hub__c dataHubRecord){
        
        List<OS_CaseRoutingMapping__c> os2cmMappingList = filterOS2CMMapping(dataHubRecord);
        
        // if mapping is not 1-1, don't create a case, but mark the data hub record as processed
        // (covered by validation of the data hub record)
        if(os2cmMappingList == null || os2cmMappingList.size() != 1){
            return null;
        } else {
            OS_CaseRoutingMapping__c os2cmMapping = os2cmMappingList.iterator().next();
            // (covered by validation of the data hub record)
            if(!(isComplaint(os2cmMapping) || isQuery(os2cmMapping) || isPSE(os2cmMapping))){
                return null;
            } else {
                Database.DMLOptions opts = new Database.DMLOptions();
                opts.allowFieldTruncation = true;
                // instantiate the object and set the mutual fields
                Case newCase = new Case(
                    Description = dataHubRecord.QueryDetails__c,
                    jl_Case_Inbound_Data_Id__c = dataHubRecord.Id,
                    jl_CRNumber__c = dataHubRecord.CR_Ref__c,
                    jl_DeliveryNumber__c = dataHubRecord.Del_Ref__c,
                    jl_Branch_master__c =  getBranchMapping(dataHubRecord.Query_For_Branch__c),    //os2cmMapping.CM_Branch__c,
                    RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(os2cmMapping.CM_Record_Type_Name__c).getRecordTypeId()
                     /*Subject = dataHubRecord.jl_ComplaintType__c,
                     Description = dataHubRecord.jl_BriefDescriptionOf__c,
                     jl_Data_Hub__c = dataHubRecord*/
                );
                
                if(isComplaint(os2cmMapping)){
                    newCase.jl_Assign_complaint_to_queue__c = os2cmMapping.CM_Assign_Case_To_Queue__c;
                    newCase.jl_Complaint_type__c =  os2cmMapping.CM_Case_Type__c;
                    newCase.jl_Complaint_type_detail__c = os2cmMapping.CM_Case_Type_Detail__c;
                } else if(isQuery(os2cmMapping)){
                    newCase.jl_Assign_query_to_queue__c = os2cmMapping.CM_Assign_Case_To_Queue__c;
                    newCase.jl_Query_type__c = os2cmMapping.CM_Case_Type__c;
                    newCase.jl_Query_type_detail__c = os2cmMapping.CM_Case_Type_Detail__c;
                } else if(isPSE(os2cmMapping)){
                    // set the fields for all three products
                    newCase.jl_Store_To_Query__c = getBranchMapping(dataHubRecord.Query_For_Branch__c);//os2cmMapping.CM_Assign_Case_To_Queue__c;
                    newCase.jl_Issue_By__c = dataHubRecord.Item1_Issue_By__c;
                    
                    // item1
                    if(!String.isBlank(dataHubRecord.Item1_Disection__c) || !String.isBlank(dataHubRecord.Item1_Stock_Code__c)){
                        newCase.jl_pse1_Stock_Number__c = dataHubRecord.Item1_Disection__c + dataHubRecord.Item1_Stock_Code__c;
                    }
                    newCase.jl_pse1_Quantity__c = dataHubRecord.Item1_Quantity__c;
                    if(!String.isBlank(dataHubRecord.Item1_Description__c) || !String.isBlank(dataHubRecord.Item1_Selling_Department__c)){
                        newCase.jl_pse1_Item_Description__c = 'Department: ' + (!String.isBlank(dataHubRecord.Item1_Selling_Department__c) ? dataHubRecord.Item1_Selling_Department__c : '') + '\n'
                                                            + 'Item Description: ' + (!String.isBlank(dataHubRecord.Item1_Description__c) ? dataHubRecord.Item1_Description__c : '');
                        
                    }
                    newCase.jl_pse1_Item_Price__c = dataHubRecord.Item1_Price__c != null ? toDecimal(dataHubRecord.Item1_Price__c) : null;
                    newCase.jl_pse1_Item_in_Stock__c = dataHubRecord.Item1_in_Stock__c;
                    // TODO: department - OCEP 1161
                    
                    // item2
                    if(!String.isBlank(dataHubRecord.Item2_Disection__c) || !String.isBlank(dataHubRecord.Item2_Stock_Code__c)){
                        newCase.jl_pse2_Stock_Number__c = dataHubRecord.Item2_Disection__c + dataHubRecord.Item2_Stock_Code__c;
                    }
                    newCase.jl_pse2_Quantity__c = dataHubRecord.Item2_Quantity__c;
                    if(!String.isBlank(dataHubRecord.Item2_Description__c) || !String.isBlank(dataHubRecord.Item2_Selling_Department__c)){
                        newCase.jl_pse2_Item_Description__c = 'Department: ' + (!String.isBlank(dataHubRecord.Item2_Selling_Department__c) ? dataHubRecord.Item2_Selling_Department__c : '') + '\n'
                                                            + 'Item Description: ' + (!String.isBlank(dataHubRecord.Item2_Description__c) ? dataHubRecord.Item2_Description__c : '');
                        
                    }
                    newCase.jl_pse2_Item_Price__c = dataHubRecord.Item2_Price__c != null ? toDecimal(dataHubRecord.Item2_Price__c) : null;
                    newCase.jl_pse2_Item_in_Stock__c = dataHubRecord.Item2_in_Stock__c;
                    
                    // item3
                    if(!String.isBlank(dataHubRecord.Item3_Disection__c) || !String.isBlank(dataHubRecord.Item3_Stock_Code__c)){
                        newCase.jl_pse3_Stock_Number__c = dataHubRecord.Item3_Disection__c + dataHubRecord.Item3_Stock_Code__c;
                    }
                    newCase.jl_pse3_Quantity__c = dataHubRecord.Item3_Quantity__c;
                    if(!String.isBlank(dataHubRecord.Item3_Description__c) || !String.isBlank(dataHubRecord.Item3_Selling_Department__c)){
                        newCase.jl_pse3_Item_Description__c = 'Department: ' + (!String.isBlank(dataHubRecord.Item3_Selling_Department__c) ? dataHubRecord.Item3_Selling_Department__c : '') + '\n'
                                                            + 'Item Description: ' + (!String.isBlank(dataHubRecord.Item3_Description__c) ? dataHubRecord.Item3_Description__c : '');
                        
                    }
                    newCase.jl_pse3_Item_Price__c = dataHubRecord.Item3_Price__c != null ? toDecimal(dataHubRecord.Item3_Price__c) : null;
                    newCase.jl_pse3_Item_in_Stock__c = dataHubRecord.Item3_in_Stock__c;
                    
                    // assign this PSE case to default owner set up in Config_Settings__c Custom Settings
                    String userName = CustomSettingsManager.getConfigSettingStringVal(CustomSettingsManager.OPENSPAN_DEFAULT_PSE_OWNER);
                    if(!String.isBlank(userName)){
                        List<User> userList = [SELECT Id FROM User WHERE Username =:userName];
                        if(userList != null && !userList.isEmpty()){
                            User defaultOwner = userList.iterator().next();
                            newCase.ownerId = defaultOwner.Id; 
                        }
                    }
                }
                newCase.setOptions(opts); // To allow truncations while mapping data from Data Hub
                system.debug('===newCase===' + newCase);
                return newCase;
                
            }                    
        }
     }
     
     private static Boolean isComplaint(OS_CaseRoutingMapping__c mapping){
        if(mapping != null && mapping.CM_Record_Type_Name__c.equalsIgnoreCase(CommonStaticUtils.CASE_RT_NAME_COMPLAINT)){
            return true;
        } else {
            return false;
        }
     }
     
     private static Boolean isPSE(OS_CaseRoutingMapping__c mapping){
        if(mapping != null && mapping.CM_Record_Type_Name__c.equalsIgnoreCase(CommonStaticUtils.CASE_RT_NAME_PRODUCT_STOCK_ENQUIRY)){
            return true;
        } else {
            return false;
        }
     }
     
     private static Boolean isQuery(OS_CaseRoutingMapping__c mapping){
        if(mapping != null && mapping.CM_Record_Type_Name__c.equalsIgnoreCase(CommonStaticUtils.CASE_RT_NAME_QUERY)){
            return true;
        } else {
            return false;
        }
     }
     
     public static List<OS_CaseRoutingMapping__c> filterOS2CMMapping(Data_Hub__c dataRecord){
        List<OS_CaseRoutingMapping__c> caseRoutingList = OS_CaseRoutingMapping__c.getAll().values();
        List<OS_CaseRoutingMapping__c> retList = new List<OS_CaseRoutingMapping__c>();
        for(OS_CaseRoutingMapping__c cr : caseRoutingList){
            if(cr.OS_Action_By__c.EqualsIgnoreCase(dataRecord.action_by__c)
                //&& cr.OS_Case_For_Branch__c.EqualsIgnoreCase(dataRecord.Query_For_Branch__c) // No need to match with the branch .
                && cr.OS_Case_Reason__c.EqualsIgnoreCase(dataRecord.Query_Reason__c) ){
                    retList.add(cr);
                }
        }
        return retList;
     }
     
     public static String getBranchMapping(String val){
        
        List<OpenSpan_Branch_Mapping__c> branchList = OpenSpan_Branch_Mapping__c.getAll().values();
        
        for(OpenSpan_Branch_Mapping__c bm : branchList){
            system.debug('========val0=====' + bm.CQM_Branch__c.trim());
            system.debug('========val1=====' + val.trim());
            if(bm.CQM_Branch__c.replaceAll('\n','').replaceAll('\t','').trim() == val.trim()){
                system.debug('========val2=====' + bm.CQM_Branch__c.trim());
                return bm.Connect_Branch__c ; 
            }
        }
        
        return '';
        
     }
     
     public static String validateOpenSpanCQM2SFDCCMMatch(Data_Hub__c dataRecord){
        
        // try to retrieve a single custom setting record
        
        String retErrorMessage = null;
        
        List<OS_CaseRoutingMapping__c> matchedMappings = filterOS2CMMapping(dataRecord);
        if(matchedMappings == null || matchedMappings.size() != 1){
            return 'No mapping or more then 1 mappings found.';
        } else {
            OS_CaseRoutingMapping__c os2crMapping = matchedMappings.iterator().next();
            
            // check whether the matched queue name exists in sfdc 
            
            if(isComplaint(os2crMapping) && os2crMapping.CM_Assign_Case_To_Queue__c != null){
                // if complaint, check in jl_Assign_complaint_to_queue__c
                Schema.Picklistentry ple = getPicklistEntry(Case.jl_Assign_complaint_to_queue__c, os2crMapping.CM_Assign_Case_To_Queue__c);
                return ple == null ? 'Assigned To Queue value from mappings does not match with values in jl_Assign_complaint_to_queue__c value in Case' : null;
            } else if(isQuery(os2crMapping) && os2crMapping.CM_Assign_Case_To_Queue__c!=null){
                // if query, check in jl_Assign_query_to_queue__c
                Schema.Picklistentry ple = getPicklistEntry(Case.jl_Assign_query_to_queue__c, os2crMapping.CM_Assign_Case_To_Queue__c);
                return ple == null ? 'Assigned To Queue value from mappings does not match with values in jl_Assign_query_to_queue__c value in Case' : null;
            } else if(isPSE(os2crMapping) && dataRecord.Query_For_Branch__c!=null){
                //return null;
                // this data is not provided by custom settings so not comparing it with this. 
                Schema.Picklistentry ple = getPicklistEntry(Case.jl_Store_To_Query__c  , getBranchMapping(dataRecord.Query_For_Branch__c).replaceAll('\t','').replaceAll('\n','').trim());
                return ple == null ? 'Assigned To Queue value from mappings does not match with values in Query For Branch' : null;
            } else {
                return 'Assigned To Data Not Found in mapping';
            }
            
            /* instead of manually checking the record type, leave the system to check it and return an exception in case of invalida data
            // check if record type is valid (exists in this org)
            RecordType rc = CommonStaticUtils.getRecordType(os2crMapping.CM_Record_Type_Name__c);
            return rc == null ? false : true;
            */ 
            
        }
     }
     
     public static Schema.PicklistEntry getPicklistEntry(Schema.sObjectField pField, String pValue){
        Schema.DescribeFieldResult fieldResult = pField.getDescribe();
        List<Schema.PicklistEntry> pleList = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry ple : pleList){
            if(pValue != null && pValue.equals(ple.getValue())){
                return ple;
            }
        }
        return null;
    }
     
     public static String cleanMatchingValue(String theValue){
        
          //MJS: 03Oct2014 - replaced with central ContactUtils function to reduce duplication in code base
          
          return ContactUtils.cleanMatchingValue(theValue);
      
     }
     
     public static String cleanNullValue(String theValue){
        
         //MJS: 03Oct2014 - replaced with central ContactUtils function to reduce duplication in code base
         
         return ContactUtils.cleanNullValue(theValue);
         
     }
     
     public static Decimal toDecimal(String decimalString){
        Decimal retVal;
        try{
            retVal = Decimal.valueOf(decimalString);
        } catch (System.TypeException e){
            //retVal = 0;
        }
        return retVal;
     }
     
    public static Boolean validateEmail(String email) {
        if(email == null)
            return true;
        Boolean res = true;
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
        Pattern MyPattern = Pattern.compile(emailRegex);
        Matcher MyMatcher = MyPattern.matcher(email);
    
        if (!MyMatcher.matches()) 
            res = false;
        return res; 
        
    }
    
    public static CaseComment createCaseComment(Id caseId, Data_Hub__c dataHubRecord){
        CaseComment cc = new CaseComment(ParentId=caseId, commentBody='This Case was created by CSS User ' + formatValue(dataHubRecord.Initiating_User__c) + ' via OpenSpan integration.\n\n');
        cc.commentBody += 'The Customer details supplied by OpenSpan were as follows:\nSalutation: ' + formatValue(datahubRecord.Salutation__c)
                            + '\nFirst Name: ' + formatValue(dataHubRecord.First_Name__c) + '\nLast Name: ' + formatValue(dataHubRecord.Surname__c) 
                            + '\nContact Number: '+ datahubRecord.Contact_Telephone__c+'\nAlternative Number: '+ formatValue(datahubRecord.Alternate_Phone1__c)
                            + '\nAlternativeNumber: '+formatValue(datahubRecord.Alternate_Phone2__c)+'\nEmail: ' + formatValue(dataHubRecord.Email__c) 
                            + '\nFirst Line of Address: ' + formatValue(dataHubRecord.Address1__c) + '\nPost Code: ' + formatValue(dataHubRecord.Post_Code__c);
        return cc;                      
    }
    
    public static string formatValue(String val){
         
         //MJS: 03Oct2014 - replaced with central ContactUtils function to reduce duplication in code base
         //note although the function name here is different the code here was doing the same thing.
         
         return ContactUtils.cleanNullValue(val);
    }
    
    
}