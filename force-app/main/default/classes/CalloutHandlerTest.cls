/**
 *	@author: Andrey Gavrikov (westbrook)
 *	@date: 2014-09-01 16:22:22 
 *	@description:
 *	    Test methods for CalloutHandler
 *	
 *	Version History :   
 *		
 */
@isTest
public class CalloutHandlerTest  {
	
	
	public static CustomSettings.Record initCalloutSettings(final String servicename, final String className, final Integer numOfRetries) {
		CustomSettings.TestRecord testRec = new CustomSettings.TestRecord();
		testRec.put('name', serviceName);
		testRec.put('Class_Name__c', className);
		testRec.put('Timeout_Milliseconds__c', 10000);
		testRec.put('Number_of_Retries__c', numOfRetries);
		testRec.put('Endpoint__c', 'http://localhost');
		testRec.put('Topic__c', 'test_topic');
		testRec.put('Certificate_Name__c', 'test_cert');

		CustomSettings.setTestCalloutSetting(servicename, testRec);
		return testRec;
	}
	

	private static Boolean serviceCalled = false;
	private static Boolean serialiseCalled = false;
	private static Boolean deserialiseCalled = false;
	private static Boolean setEndpointCalled = false;
	private static Boolean setTimeoutCalled = false;
	private static Boolean setClientCertificateNameCalled = false;
	
	public class TestImpl implements RetriableCallout {
		private MyJL_LoyaltyAccountHandler.NotifyCustomerEventPort service = new MyJL_LoyaltyAccountHandler.NotifyCustomerEventPort();

		public String serialise (final Object obj) {
			serialiseCalled = true;
			return JSON.serialize(obj);
		}
		
		public Object deserialise (final String val) {
			deserialiseCalled = true;
			return JSON.deserialize(val, SObject.class);
		}

		public void setEndpoint(final String url) {
			setEndpointCalled = true;
		}

		public void setTimeout(final Integer mills) {
			setTimeoutCalled = true;
		}

		public void callService(final List<Object> sobjects) {
			serviceCalled = true;
		}
		public void setTopic(final String topic) {
			//ignore
		}
		public void setClientCertificateName(final String uniqueName) {
			setClientCertificateNameCalled = String.isNotBlank(uniqueName);
		}

	}
	/**
	 * check if all steps of process are being triggered
	 */
	static testMethod void testInitialProcessFlow () {
		//prepare header and details
		initCalloutSettings('TEST_SERVICE', 'CalloutHandlerTest.TestImpl', 0);
		final List<Account> accounts = new List<Account> {new Account(Name = 'Account 1'), new Account(Name = 'Account 2')};
		Database.insert(accounts);
		Test.startTest();
		CalloutHandler.callout('TEST_SERVICE', accounts);
		Test.stopTest();

		//check that all relevant methods have been called
		System.assert(serialiseCalled, 'serialiseCalled has not been invoked');
		System.assert(setEndpointCalled, 'setEndpointCalled has not been invoked');
		System.assert(setTimeoutCalled, 'setTimeoutCalled has not been invoked');
		System.assert(deserialiseCalled, 'deserialiseCalled has not been invoked');
		System.assert(serviceCalled, 'callService has not been invoked');
		System.assert(setClientCertificateNameCalled, 'setClientCertificateName has not been invoked or called with blank value');
	}

	static testMethod void testRetryProcessFlow () {
		//prepare header and details
		initCalloutSettings('TEST_SERVICE', 'CalloutHandlerTest.TestImpl', 0);
		final List<Account> accounts = new List<Account> {new Account(Name = 'Account 1'), new Account(Name = 'Account 2')};
		Database.insert(accounts);

		final List<Log_Header__c> headers = new List<Log_Header__c> {
			new Log_Header__c(
					Callout_Status__c = 'Retry',
					Service__c = 'TEST_SERVICE',
					Activate_Callout_Retry_Workflow__c = true,
					Serialised_Data__c = JSON.serialize(accounts[0])
					),
			new Log_Header__c(
					Callout_Status__c = 'Retry',
					Service__c = 'TEST_SERVICE',
					Activate_Callout_Retry_Workflow__c = true,
					Serialised_Data__c = JSON.serialize(accounts[1])
					)
		};
		Database.insert(headers);
		Test.startTest();
		headers[0].Callout_Status__c = 'Retry';
		headers[0].Do_Callout__c = true;
		headers[1].Callout_Status__c = 'Retry';
		headers[1].Do_Callout__c = true;
		Database.update(headers);
		
		Database.executeBatch(new CalloutRetryBatch());
		//schedule two in a row to check if this will cause any problems
		Database.executeBatch(new CalloutRetryBatch());
		Test.stopTest();


		//check that all relevant methods have been called
		//System.assert(setTimeoutCalled, 'setTimeoutCalled has not been invoked');
		System.assert(deserialiseCalled, 'deserialiseCalled has not been invoked');
		System.assert(serviceCalled, 'callService has not been invoked');
	}
	
	static testMethod void testNextCalloutTime () {
		CustomSettings.Record config = initCalloutSettings('TEST_SERVICE', 'CalloutHandlerTest.TestImpl', 10);
		//config.Number_of_Retries__c = 10;
		final Log_Header__c header = new Log_Header__c();
		header.Callout_Status__c = 'Retry';//Soft Fail
		header.Callout_Attempts__c = 1;

		CalloutHandler.setNextCalloutTime(config, header);
		System.assert(header.Next_Callout_Date__c > System.now(), 'Expected Next_Callout_Date__c to be in the future');
		System.assertEquals(true, header.Activate_Callout_Retry_Workflow__c, 'Expected Activate_Callout_Retry_Workflow__c flag to be set ON');

		//check persistent problem
		header.Callout_Status__c = 'Test Hard Fail';
		CalloutHandler.setNextCalloutTime(config, header);
		System.assertEquals(false, header.Activate_Callout_Retry_Workflow__c, 'Expected Activate_Callout_Retry_Workflow__c flag to be set OFF');
	}

	private class NotIOException extends Exception {}
	
	/**
	 * check how it reacts to non IO exception, i.e. hard Fail
	 */
	static testMethod void testProcessFailedCallout () {
		initCalloutSettings('TEST_SERVICE', 'CalloutHandlerTest.TestImpl', 0);
		final Log_Header__c header = new Log_Header__c();
		header.Callout_Status__c = 'Retry';
		header.Service__c = 'TEST_SERVICE';
		header.Activate_Callout_Retry_Workflow__c = true;
		Database.insert(header);
		//Hard Failure
		CalloutHandler.processFailedCallout('TEST_SERVICE', new List<Log_Header__c> {header}, new NotIOException());
		System.assertEquals(false, header.Activate_Callout_Retry_Workflow__c, 'Expected Activate_Callout_Retry_Workflow__c flag to be set OFF');
		System.assertNotEquals('Retry', header.Callout_Status__c, 'Hard failure must not cause a Retry');
	}


	/**
	 * MJL-1306 - check how CalloutRetryScheduler page behaves when wrong frequency is entered
	 */
	static testMethod void checkSchedulingPageWithError () {
		CalloutRetryBatch controller = new CalloutRetryBatch();
		//check validation error
		controller.frequency = -10;
		controller.setupNewSchedule();
		System.assertEquals(true, ApexPages.hasMessages(ApexPages.Severity.Error), 'Expected to get a validation error');
		
	}

	/**
	 * MJL-1306 - check that CalloutRetryScheduler page actually does schedule batches
	 */
	static testMethod void checkSchedulingPageOK () {
		initCalloutSettings('TEST_SERVICE', 'CalloutHandlerTest.TestImpl', 10);
		final Log_Header__c header = new Log_Header__c();
		header.Callout_Status__c = 'Retry';
		header.Service__c = 'TEST_SERVICE';
		header.Activate_Callout_Retry_Workflow__c = true;
		header.Next_Callout_Date__c = System.now() - 1;//yesterday
		Database.insert(header);
		
		CalloutRetryBatch controller = new CalloutRetryBatch();
		//check scheduler
		controller.frequency = 1;
		Test.startTest();
		controller.setupNewSchedule();
		System.assertEquals(false, ApexPages.hasMessages(ApexPages.Severity.Error), 'Did not expect to get a validation error');
		System.assertEquals(1, [select count() from CronTrigger where CronJobDetail.JobType = '7' and CronJobDetail.Name like 'Callout retry%'], 'Expected to see exactly 1 cron entry');
		//twice an hour
		controller.frequency = 2;
		controller.setupNewSchedule();
		Test.stopTest();

		System.assertEquals(2, [select count() from CronTrigger where CronJobDetail.JobType = '7' and CronJobDetail.Name like 'Callout retry%'], 'Expected to see exactly 2 cron entries');
		
		//just code coverage
		controller.runNow();
	}

}