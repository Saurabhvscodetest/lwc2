/**
 * Unit tests for clsUserTriggerHandler class methods.
 */
@Istest
public class clsUserTriggerHandler_TEST {
	
	/**
	 * Original test class method 1
	 */
    static testMethod void test_getTierFromRole() {
        Test.startTest();
        String tier = clsUserTriggerHandler.getTierFromRole ('MJ:OROR:T3');
        Test.stopTest();
        System.assertEquals('3',tier);
    }

	/**
	 * Original test class method 2
	 */
    static testMethod void test_getBranchFromRole() {
        Test.startTest();
        String branch = clsUserTriggerHandler.getBranchFromRole ('MJ:Branch:Cardiff');
        Test.stopTest();
        System.assertEquals('Cardiff',branch);
    }

	/**
	 * Test auto assign permission sets for a Role and Profile for a new user
	 */
	static testMethod void testAssignPermissionsToNewUser() {
		Profile prof = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		Map<String, PermissionSet> permissionSetNameXrefMap = new Map<String, PermissionSet>();
		List<PermissionSet> permissionSetList = [SELECT Id, Label FROM PermissionSet WHERE IsOwnedByProfile = false];
		if (!permissionSetList.isEmpty()) {
			for (PermissionSet ps : permissionSetList) {
				permissionSetNameXrefMap.put(ps.Label, ps);
			}
		}
		Id apiEnabledPSId = permissionSetNameXrefMap.get('API Enabled').Id;
		Id apiOnlyUserPSId = permissionSetNameXrefMap.get('Api Only User').Id;
		Id dataloaderPSId = permissionSetNameXrefMap.get('Dataloader').Id;

		UserRole ur1 = new UserRole(RollupDescription = 'UT User Role 1', Name = 'UT User Role 1', DeveloperName = 'UT_User_Role_1');
		insert ur1;
	    
		Auto_Assign_Permission_Set__c aaps1 = new Auto_Assign_Permission_Set__c(Name = 'ROLE: UT User Role 1', Permission_Sets_To_Assign__c = 'API Enabled;Api Only User');
		Auto_Assign_Permission_Set__c aaps2 = new Auto_Assign_Permission_Set__c(Name = 'PROFILE: System Administrator', Permission_Sets_To_Assign__c = 'Dataloader');
		List<Auto_Assign_Permission_Set__c> aapsList = new List<Auto_Assign_Permission_Set__c> {aaps1, aaps2};
		User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
		System.runAs(runningUser) { // Avoid MIX_DML
		insert aapsList;
		}
		
		User usr = new User(FirstName = 'Freddie', LastName = 'Admin', Username = 'freddietest@test.com.test', 
							Email = 'freddietest.test@test.com.test', CommunityNickname = 'freddiet', 
							Alias = 'freddie', TimeZoneSidKey = 'Europe/London', 
							LocaleSidKey = 'en_GB', EmailEncodingKey = 'ISO-8859-1', 
							ProfileId = prof.Id, LanguageLocaleKey = 'en_US', isActive = true,
							Team__c = 'JL.com - Bookings', Tier__c = 2, UserRoleId = ur1.Id);		
    	
    	test.startTest();
    	insert usr;
    	test.stopTest();
    	
		List<PermissionSetAssignment> psaList = [SELECT Id, AssigneeId, PermissionSetId, PermissionSet.Name
												 FROM PermissionSetAssignment
												 WHERE AssigneeId = :usr.Id AND PermissionSet.ProfileId = null];    	
    	
    	system.assertEquals(3, psaList.size());
    	for (PermissionSetAssignment psa : psaList) {
    		system.assert(psa.PermissionSetId == apiEnabledPSId || psa.PermissionSetId == apiOnlyUserPSId || psa.PermissionSetId == dataloaderPSId);
    	}
    }
    
	/**
	 * Test auto assign permission sets update for an existing user where Role changes.
	 */
	static testMethod void testUpdatePermissionsOnExistingUser() {
		Profile prof = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		Map<String, PermissionSet> permissionSetNameXrefMap = new Map<String, PermissionSet>();
		List<PermissionSet> permissionSetList = [SELECT Id, Label FROM PermissionSet WHERE IsOwnedByProfile = false];
		if (!permissionSetList.isEmpty()) {
			for (PermissionSet ps : permissionSetList) {
				permissionSetNameXrefMap.put(ps.Label, ps);
			}
		}
		Id apiEnabledPSId = permissionSetNameXrefMap.get('API Enabled').Id;
		Id apiOnlyUserPSId = permissionSetNameXrefMap.get('Api Only User').Id;
		Id dataloaderPSId = permissionSetNameXrefMap.get('Dataloader').Id;
		Id interactionReportingPSId = permissionSetNameXrefMap.get('Interaction Reporting Permission').Id;

		UserRole ur1 = new UserRole(RollupDescription = 'UT User Role 1', Name = 'UT User Role 1', DeveloperName = 'UT_User_Role_1');
		UserRole ur2 = new UserRole(RollupDescription = 'UT User Role 2', Name = 'UT User Role 2', DeveloperName = 'UT_User_Role_2');
		List<UserRole> urList = new List<UserRole> {ur1, ur2};
		insert urList;
	    
		Auto_Assign_Permission_Set__c aaps1 = new Auto_Assign_Permission_Set__c(Name = 'ROLE: UT User Role 1', Permission_Sets_To_Assign__c = 'API Enabled;Api Only User');
		Auto_Assign_Permission_Set__c aaps2 = new Auto_Assign_Permission_Set__c(Name = 'ROLE: UT User Role 2', Permission_Sets_To_Assign__c = 'Interaction Reporting Permission');
		Auto_Assign_Permission_Set__c aaps3 = new Auto_Assign_Permission_Set__c(Name = 'PROFILE: System Administrator', Permission_Sets_To_Assign__c = 'Dataloader');
		List<Auto_Assign_Permission_Set__c> aapsList = new List<Auto_Assign_Permission_Set__c> {aaps1, aaps2, aaps3};
		User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
		System.runAs(runningUser) { // Avoid MIX_DML
		insert aapsList;
		}
		
		User usr = new User(FirstName = 'Freddie', LastName = 'Admin', Username = 'freddietest@test.com.test', 
							Email = 'freddietest.test@test.com.test', CommunityNickname = 'freddiet', 
							Alias = 'freddie', TimeZoneSidKey = 'Europe/London', 
							LocaleSidKey = 'en_GB', EmailEncodingKey = 'ISO-8859-1', 
							ProfileId = prof.Id, LanguageLocaleKey = 'en_US', isActive = true,
							Team__c = 'JL.com - Bookings', Tier__c = 2, UserRoleId = ur1.Id);		
    	insert usr;
    	
		List<PermissionSetAssignment> psaList = [SELECT Id, AssigneeId, PermissionSetId, PermissionSet.Name
												 FROM PermissionSetAssignment
												 WHERE AssigneeId = :usr.Id AND PermissionSet.ProfileId = null];    	
    	
    	system.assertEquals(3, psaList.size());
    	for (PermissionSetAssignment psa : psaList) {
    		system.assert(psa.PermissionSetId == apiEnabledPSId || psa.PermissionSetId == apiOnlyUserPSId || psa.PermissionSetId == dataloaderPSId);
    		system.assertNotEquals(interactionReportingPSId, psa.PermissionSetId);
    	}
    	
    	usr.UserRoleId = ur2.Id;
    	test.startTest();
    	update usr;
    	test.stopTest();

		List<PermissionSetAssignment> psaList2 = [SELECT Id, AssigneeId, PermissionSetId, PermissionSet.Name
												  FROM PermissionSetAssignment
												  WHERE AssigneeId = :usr.Id AND PermissionSet.ProfileId = null];    	
    	
    	system.assertEquals(2, psaList2.size());
    	for (PermissionSetAssignment psa : psaList2) {
    		system.assert(psa.PermissionSetId == interactionReportingPSId || psa.PermissionSetId == dataloaderPSId);
    		system.assertNotEquals(apiEnabledPSId, psa.PermissionSetId);
    		system.assertNotEquals(apiOnlyUserPSId, psa.PermissionSetId);
    	}
    }

	/**
	 * Test add/remove static permission set assignments.
	 */
	static testMethod void testAddRemoveStaticPermissionSets() {
		final String ACCESS_BUTTON_PERMISSION_NAME = 'Access Button Permission';
		final String JL_TIER_3_PERMISSION_NAME = 'JL Tier 3 Permissions';
		final String SST_TAB_PERMISSION_NAME = 'SST Tab Permission';
		final String TRANSFER_CASES_PERMISSION_NAME = 'Transfer Cases';
		final String DUTY_MANAGER_PERMISSION_NAME = 'Duty Manager';
		final String IN_HOUSE_CC_TEAM_MANAGER_PERMISSION_NAME = 'In House CC Team Manager';
		final String SPECIAL_CIRCUMSTANCES_PERMISSION_NAME = 'Special Circumstances';
		
		Profile prof = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		Map<String, PermissionSet> permissionSetNameXrefMap = new Map<String, PermissionSet>();
		List<PermissionSet> permissionSetList = [SELECT Id, Label FROM PermissionSet WHERE IsOwnedByProfile = false];
		if (!permissionSetList.isEmpty()) {
			for (PermissionSet ps : permissionSetList) {
				permissionSetNameXrefMap.put(ps.Label, ps);
			}
		}
		Id accessButtonPSId = permissionSetNameXrefMap.get(ACCESS_BUTTON_PERMISSION_NAME).Id;
		Id tier3PSId = permissionSetNameXrefMap.get(JL_TIER_3_PERMISSION_NAME).Id;
		Id sstTabPSId = permissionSetNameXrefMap.get(SST_TAB_PERMISSION_NAME).Id;
		Id transferCasesPSId = permissionSetNameXrefMap.get(TRANSFER_CASES_PERMISSION_NAME).Id;
		Id dutyManagerPSId = permissionSetNameXrefMap.get(DUTY_MANAGER_PERMISSION_NAME).Id;
		Id ccTeamManagerPSId = permissionSetNameXrefMap.get(IN_HOUSE_CC_TEAM_MANAGER_PERMISSION_NAME).Id;

		UserRole ur1 = new UserRole(RollupDescription = 'UT User Role 1', Name = 'UT User Role 1', DeveloperName = 'UT_User_Role_1');
		insert ur1;
		
		User usr = new User(FirstName = 'Freddie', LastName = 'Admin', Username = 'freddietest@test.com.test', 
							Email = 'freddietest.test@test.com.test', CommunityNickname = 'freddiet', 
							Alias = 'freddie', TimeZoneSidKey = 'Europe/London', 
							LocaleSidKey = 'en_GB', EmailEncodingKey = 'ISO-8859-1', 
							ProfileId = prof.Id, LanguageLocaleKey = 'en_US', isActive = true,
							Team__c = 'JL.com - Bookings', Tier__c = 3, UserRoleId = ur1.Id,
							Trusted_User__c = true, JL_com_Duty_Manager__c = true, Is_In_House_CC_Team_Manager__c = true);		
    	insert usr;
    	
		List<PermissionSetAssignment> psaList = [SELECT Id, AssigneeId, PermissionSetId, PermissionSet.Name
												 FROM PermissionSetAssignment
												 WHERE AssigneeId = :usr.Id AND PermissionSet.ProfileId = null];    	
    	
    	system.assertEquals(5, psaList.size());
    	for (PermissionSetAssignment psa : psaList) {
    		system.assert(psa.PermissionSetId == accessButtonPSId || psa.PermissionSetId == tier3PSId || psa.PermissionSetId == sstTabPSId || psa.PermissionSetId == dutyManagerPSId || psa.PermissionSetId == ccTeamManagerPSId);
    	}
    	
    	usr.Tier__c = 2;
    	usr.Trusted_User__c = false;
    	usr.JL_com_Duty_Manager__c = false;
    	usr.Is_In_House_CC_Team_Manager__c = false;
    	test.startTest();
    	update usr;
    	test.stopTest();

		List<PermissionSetAssignment> psaList2 = [SELECT Id, AssigneeId, PermissionSetId, PermissionSet.Name
												  FROM PermissionSetAssignment
												  WHERE AssigneeId = :usr.Id AND PermissionSet.ProfileId = null];    	
    	
    	system.assertEquals(0, psaList2.size());
    }
    
    static testMethod void userShouldContianRelatedQueues_IfPrimaryTeamQueueAssociatedRelatedQueuesArePresent() {
   		Integer randomNo = UnitTestDataFactory.getRandomNumber();
    	String TEAM_NAME = 'TESTTEAM' + randomNo;
   		UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.CREATE_RELATED_QUEUES);
   		
   		Team__c teamObj = Team__c.getInstance(TEAM_NAME);
   	    system.assert(teamObj != null , 'Team should exists with the name' + TEAM_NAME);
   		
		User backOfficeUser 	= UnitTestDataFactory.getBackOfficeUser('backOfficeUser1', TEAM_NAME);
   		
		Set<String> queueNameSet = new Set<String>(); 
		Test.startTest();
			User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
			System.runAs(runningUser) { // Avoid MIX_DML
			insert backOfficeUser;
			}
   		Test.stopTest();
   		
   		//These queues should be present for the team
   	    User actualUserObj = [select ID, Queues__c from User where Id =: backOfficeUser.ID LIMIT 1];
   		system.assert(actualUserObj != null, ' should find the user'); 
   		//system.assert(actualUserObj.Queues__c.contains( teamObj.Queue_Name__c + '_' + TeamUtils.QUEUE_TYPES.WARNING), 'WARNING queue should be included in the USER QUEUE list ' + actualUserObj.Queues__c);
   		system.assert(actualUserObj.Queues__c.contains( teamObj.Queue_Name__c + '_' + TeamUtils.QUEUE_TYPES.ACTIONED), 'ACTIONED queue should be included in the USER QUEUE list ' + actualUserObj.Queues__c);
   		system.assert(actualUserObj.Queues__c.contains( teamObj.Queue_Name__c + '_' + TeamUtils.QUEUE_TYPES.FAILED), 'FAILED queue should be included in the USER QUEUE list ' + actualUserObj.Queues__c);
   			
    }
    
    static testMethod void userShouldNOTContianRelatedQueues_IfPrimaryTeamQueueAssociatedRelatedQueuesAreNOTPresent() {
 		Integer randomNo = UnitTestDataFactory.getRandomNumber();
    	String TEAM_NAME = 'TESTTEAM' + randomNo;
   		UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
   		
		User backOfficeUser 	= UnitTestDataFactory.getBackOfficeUser('backOfficeUser1', TEAM_NAME);
   		
		Set<String> queueNameSet = new Set<String>(); 
		Test.startTest();
			User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
			System.runAs(runningUser) { // Avoid MIX_DML
			insert backOfficeUser;
			}
   		Test.stopTest();
   		
   		Team__c teamObj = Team__c.getInstance(TEAM_NAME);
   			
   	    User actualUserObj = [select ID, Queues__c from User where Id =: backOfficeUser.ID LIMIT 1];
   		system.assert(actualUserObj != null, ' should find the user'); 
   		//system.assert(!actualUserObj.Queues__c.contains(teamObj.Queue_Name__c  + TeamUtils.SEPARATOR + TeamUtils.QUEUE_TYPES.WARNING.name()), 'WARNING queue should NOT be included in the list for ' + teamObj.Queue_Name__c);
    }
    
    static testMethod void userShouldContianRelatedQueues_IfSecondaryTeamQueueAssociatedRelatedQueuesArePresent() {
		Integer randomNo = UnitTestDataFactory.getRandomNumber();
    	String TEAM_NAME = 'TESTTEAM' + randomNo;
    	String ADDTIONAL_TEAM_NAME = TEAM_NAME+ 'A';
   		UnitTestDataFactory.setUpTeams(new List<String>{TEAM_NAME, ADDTIONAL_TEAM_NAME}, UnitTestDataFactory.CREATE_RELATED_QUEUES);
   		
   		Team__c teamObj = Team__c.getInstance(TEAM_NAME);
   	    system.assert(teamObj != null , 'Team should exists with the name' + TEAM_NAME);
   		
   		
   		//Set Up the user
		User backOfficeUser 	= UnitTestDataFactory.getBackOfficeUser('backOfficeUser1', TEAM_NAME);
   		backOfficeUser.My_Teams__c = ADDTIONAL_TEAM_NAME;
		Set<String> queueNameSet = new Set<String>(); 
		Test.startTest();
			User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
			System.runAs(runningUser) { // Avoid MIX_DML
			insert backOfficeUser;
			}
   		Test.stopTest();
   		
   	    User actualUserObj = [select ID, Queues__c from User where Id =: backOfficeUser.ID LIMIT 1];
   		system.assert(actualUserObj != null, ' should find the user'); 
   		//system.assert(actualUserObj.Queues__c.contains( teamObj.Queue_Name__c + '_' + TeamUtils.QUEUE_TYPES.WARNING), 'WARNING queue should be included in the USER QUEUE list ' + actualUserObj.Queues__c);
   		system.assert(actualUserObj.Queues__c.contains( teamObj.Queue_Name__c + '_' + TeamUtils.QUEUE_TYPES.ACTIONED), 'ACTIONED queue should be included in the USER QUEUE list ' + actualUserObj.Queues__c);
   		system.assert(actualUserObj.Queues__c.contains( teamObj.Queue_Name__c + '_' + TeamUtils.QUEUE_TYPES.FAILED), 'FAILED queue should be included in the USER QUEUE list ' + actualUserObj.Queues__c);
   			
    }
    
    
    
}