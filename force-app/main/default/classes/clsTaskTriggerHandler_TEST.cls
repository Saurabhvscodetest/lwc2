//************************************************
// @Original Class
//<<002>> @Author KM/DP  Code Refactoring   25/06/2019
//************************************************
@Istest
public class clsTaskTriggerHandler_TEST 
{   
    //<<002>>
    private static final Id queryCaseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonStaticUtils.CASE_RT_NAME_QUERY).getRecordTypeId();
    private static final Id complaintCaseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonStaticUtils.CASE_RT_NAME_COMPLAINT).getRecordTypeId();
    private static final Id pseCaseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonStaticUtils.CASE_RT_NAME_PRODUCT_STOCK_ENQUIRY).getRecordTypeId();
    private static final Id nKUCaseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonStaticUtils.CASE_RT_NAME_NKU).getRecordTypeId(); 
    private static final Id newCaseRTId= Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonStaticUtils.CASE_RT_New_Case).getRecordTypeId();
    //<<002>>
    private static User getBackOfficeUser(){
        User sysAdmin = UnitTestDataFactory.getSystemAdministrator('admin1');
        User backOfficeUser = UnitTestDataFactory.getBackOfficeUser('bo', 'JL.com - Bookings');
        List<User> uList = new List<User> {sysAdmin, backOfficeUser};
        insert uList;
    
        System.runAs(sysAdmin) {
            jl_runvalidations__c jrv = new jl_runvalidations__c(Name = backOfficeUser.Id, Run_Validations__c = true);
            insert jrv;     
        }
        return backOfficeUser;
    }
    
    @testSetup static void setup() {
         UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
    }

    @IsTest
    public static void testTaskCreationFailClosedCase()
    {
        UnitTestDataFactory.setRunValidationRules(false);
        
        UnitTestDataFactory.setRunValidationRules(false);
        Contact con = UnitTestDataFactory.createContact();
        insert con;
        
        Case c = UnitTestDataFactory.createNormalCase(con);
        c.Status = 'Closed - resolved';
        
        insert c;

        Id cid = c.id;

        Task t = new Task();
        t.WhatId = c.id;
        t.WhoId = c.contactid;
        t.ActivityDate = Date.today();
        t.type = 'Case Assist';
        t.subject = 'Assist';
        t.status = 'Completed';
        t.jl_created_from_case__c = true;

        Test.startTest();
        
        try {
            insert t;
            System.Assert(false);
        }
        catch (Exception ex)
        {
            System.Assert(true);
        }
        Test.stopTest();
        
    }

    @IsTest
    public static void testTaskDeleteFail(){
        
    User backOfficeUser = getBackOfficeUser();
        
    System.runAs(backOfficeUser) {
        
        UnitTestDataFactory.setRunValidationRules(false);
        
        UnitTestDataFactory.setRunValidationRules(false);
        Contact con = UnitTestDataFactory.createContact();
        insert con;
        
        Case c = UnitTestDataFactory.createNormalCase(con);
        c.Status = 'New';
        
        insert c;

        Id cid = c.id;

        Task t = new Task();
        t.WhatId = c.id;
        t.WhoId = c.contactid;
        t.ActivityDate = Date.today();
        t.type = 'Case Assist';
        t.subject = 'Assist';
        t.status = 'New';
        t.jl_created_from_case__c = true;

        insert t;

        Test.startTest();
        
        try {
            delete t;
            System.Assert(false);
        }
        catch (Exception ex)
        {
            System.Assert(true);
        }
        Test.stopTest();
        }
    }

    @isTest
    public static void testCaseIsAssociatedToTask(){

        Contact con = CustomerTestDataFactory.createContact();
        insert con;
        
        Case c = CaseTestDataFactory.createCase(con.Id);        
        insert c;

        Task t = TaskTestDataFactory.createTask(null, null, CommonStaticUtils.TASK_RT_NAME_CALL_LOG);
        
        t.Subject = CommonStaticUtils.TASK_SUBJECT_CALL_LOG;
        t.Status = CommonStaticUtils.TASK_STATUS_CLOSED_RESOLVED;
        t.Case__c = c.Id;

        test.startTest();
        insert t;
        test.stopTest();

        t = [select id,whatid from Task where id = :t.id limit  1];

        System.assertEquals(c.Id, t.WhatId, 'Case should be associated to the task due to trigger code.');
    }

    @IsTest
    public static void testTaskCreationSystem()
    {
        UnitTestDataFactory.setRunValidationRules(false);
        
        UnitTestDataFactory.setRunValidationRules(false);
        Contact con = UnitTestDataFactory.createContact();
        insert con;
        
        Case c = UnitTestDataFactory.createNormalCase(con);     
        insert c;

        Id cid = c.id;

        Task t = new Task();
        t.WhatId = c.id;
        t.WhoId = c.contactid;
        t.ActivityDate = Date.today();
        t.type = 'Case Assist';
        t.subject = 'Assist';
        t.status = 'New';
        t.jl_created_from_case__c = true;
        insert t;

        Test.startTest(); 

        t.status = 'Closed - Resolved';
        update t;
        
        Test.stopTest();

        List<CaseComment> listcc = [select id from CaseComment where parentid = :cid];
        System.assertEquals(1,listcc.size());
        
    }

    @IsTest
    public static void testTaskCreationManual()
    {
        UnitTestDataFactory.setRunValidationRules(false);
        
        UnitTestDataFactory.setRunValidationRules(false);
        Contact con = UnitTestDataFactory.createContact();
        insert con;
        
        Case c = UnitTestDataFactory.createNormalCase(con);     
        c.jl_NumberOutstandingTasks__c = 0;
        insert c;

        Id cid = c.id;

        Task t = new Task();
        t.WhatId = c.id;
        t.WhoId = c.contactid;
        t.ActivityDate = Date.today();
        t.type = 'Case Assist';
        t.subject = 'Assist';
        t.status = 'New';
        t.jl_created_from_case__c = true;
        t.jl_team__c = 'COT';
        insert t;

        Test.startTest(); 

        t.jl_created_from_case__c = false;
        update t;
        
        Test.stopTest();

        List<Case> listcafter = [select id, jl_NumberOutstandingTasks__c from Case where id = :cid];
        
        System.assertEquals(1,listcafter[0].jl_NumberOutstandingTasks__c);
        
    }
    
    @IsTest
    public static void testInboundEmailTasks() {
        UnitTestDataFactory.setRunValidationRules(false);
        UnitTestDataFactory.setRunValidationRules(false);
        Contact con = UnitTestDataFactory.createContact();
        insert con;
        
        Case c = UnitTestDataFactory.createNormalCase(con);     
        c.jl_NumberOutstandingTasks__c = 0;
        c.origin = 'Email';
        insert c;
        
        string cid = c.id;
        
        Test.startTest();
        Task t = new Task();
        t.WhatId = cid;
        t.WhoId = c.contactid;
        t.ActivityDate = Date.today();
        t.subject = 'Assist';
        t.status = 'Closed';
        t.jl_team__c = 'COT';   
        t.jl_created_from_case__c = false;
        
        insert t;
        
        string tid = t.id;
        
       
        EmailMessage em = new EmailMessage();
        em.ActivityId = tid;
        em.parentid = cid;
        em.Subject = 'Email Subject';
        em.FromAddress = 'aCustomer@gmail.com';
        em.ToAddress = 'jlServiceAddress@j.com';
        em.Incoming = true;
        insert em;
        string emid = em.id;
        Test.stopTest();
        
        c = [select id, origin from Case where id = :c.id limit 1];
        t = [select id,subject,whatid,JL_Related_to_Incoming_Email__c,jl_Related_Case_Last_Queue_Name__c,JL_Closed_Date_Time__c,jl_Agent_Name__c from Task where id = :tid limit  1];
        system.debug('CHECKING t ' + t);
        system.assert(c!=null);
        //The task should have been "related to incoming email" after inserting the emailmessage object
        system.assert(t.jl_related_to_incoming_Email__c==true);
        
    }
    
    @isTest static void CompletingAMilestoneShouldCreateReportingTask() {
        MilestoneUtilsTest.createEntitlementAccounts();
        MilestoneUtilsTest.createEntitlements();
        MilestoneUtilsTest.createCaseRoutingCategorisation();
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        Id caseComplaintRecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Case' AND DeveloperName = 'Complaint' LIMIT 1].Id;
        User newCRDUser;
        Case testCase;
        Contact testContact;
    
        System.runAs(runningUser) {
            newCRDUser = UnitTestDataFactory.getFrontOfficeUser('TEST_CRD_USER');
            newCRDUser.Team__c = MilestoneUtilsTest.HAMILTON_CRD_TEAM_NAME;
            insert newCRDUser;

            testContact = UnitTestDataFactory.createContact();
            insert testContact;
        }

        Date tommorow = Date.today().addDays(1);
        System.runAs(newCRDUser) {
            testCase = UnitTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            insert testCase;
        }

        //we cannot put this code between Test.starttest() and Test.stoptest() since we get the exception. System.NoAccessException: Use Test.setCreatedDate() only before Test.startTest().
        //Modifying the created date of the case to violate the first contact milestone
        Test.setCreatedDate(testCase.Id, Datetime.now().addDays(-2));

        List<Task> reportingTaskList = [select id from Task where RecordTypeId =:clsTaskTriggerHandler.MS_REPORTING_RECORD_TYPE_ID ];
        system.assert (reportingTaskList.isempty(), 'No reporting task should be present');
        Test.startTest();  
        //triggering a dummy update so that the case calculates its priority
        testCase.Description = 'Dummy update';
        update testCase; 

        // Requery case
        testCase = [SELECT Id,EntitlementId,Status, jl_Action_Taken__c, Entitlement.Name, ownerId,jl_Last_Queue__c,jl_Reopened_Count__c, BusinessHoursId , priority FROM Case WHERE Id = :testCase.Id];
        Map<String, List<CaseMilestone>> milestoneNameToCaseMilestoneMap = MilestoneUtilsTest.getCaseMilestonesAsMap(testCase.Id);
        system.debug('milestoneNameToCaseMilestoneMap***'+milestoneNameToCaseMilestoneMap);
        CaseMilestone firstContactAckMilestone = milestoneNameToCaseMilestoneMap.get(MilestoneHandlerSupport.MILESTONE_WARNING)[0];
        //System.assert(!firstContactAckMilestone.IsViolated, 'Next Case Activity milestone should not be violated for the case' );
        //System.assert(firstContactAckMilestone.CompletionDate == null, 'Milestone is not completed' );


        //Closing the first Contact Milestone
        testCase.Status = 'Active - Awaiting on customer update'; 
        testCase.jl_Action_Taken__c = EntitlementConstants.CUSTOMER_CONTACTED; 
        update testCase;
        Test.stopTest();

        reportingTaskList = [select id,RecordTypeId, Milestone_Id__c, whatId, Subject,Case_Owner_Id__c,Case_Last_Queue_Id__c, Milestone_Violated__c,
                             Milestone_Completed_Date__c, Status, Case_Reopened_Count__c , Milestone_Reporting_Key__c from Task
                             where RecordTypeId =: clsTaskTriggerHandler.MS_REPORTING_RECORD_TYPE_ID ];
        //Requery the milestones 
        milestoneNameToCaseMilestoneMap =MilestoneUtilsTest.getCaseMilestonesAsMap(testCase.Id);
        firstContactAckMilestone = milestoneNameToCaseMilestoneMap.get(MilestoneHandlerSupport.MILESTONE_WARNING)[0];

        System.assert(firstContactAckMilestone.CompletionDate == null, 'Milestone should be not completed' );

    }

    @isTest static void FailedNotificationTaskShouldCreateReportingTask() {
        MilestoneUtilsTest.createEntitlementAccounts();
        MilestoneUtilsTest.createEntitlements();
        MilestoneUtilsTest.createCaseRoutingCategorisation();
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        Id caseComplaintRecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Case' AND DeveloperName = 'Complaint' LIMIT 1].Id;
        User newCRDUser;
        Case testCase;
        Contact testContact;

        System.runAs(runningUser) {
            newCRDUser = UnitTestDataFactory.getFrontOfficeUser('TEST_CRD_USER');
            newCRDUser.Team__c = MilestoneUtilsTest.HAMILTON_CRD_TEAM_NAME;
            insert newCRDUser;

            testContact = UnitTestDataFactory.createContact();
            insert testContact;
        }
        
        Date tommorow = Date.today().addDays(1);
        System.runAs(newCRDUser) {
            testCase = UnitTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            insert testCase;
        }

        //we cannot put this code between Test.starttest() and Test.stoptest() since we get the exception. System.NoAccessException: Use Test.setCreatedDate() only before Test.startTest().
        //Modifying the created date of the case to violate the first contact milestone
        Test.setCreatedDate(testCase.Id, Datetime.now().addDays(-2));
        Test.startTest();
        List<Task> reportingTaskList = [select id from Task where RecordTypeId =:clsTaskTriggerHandler.MS_REPORTING_RECORD_TYPE_ID ];
        system.assert (reportingTaskList.isempty(), 'No reporting task should be present');

        //Mocking the task for the dummy update triggered by an entitlement process 
        Task autoUpdatetask  = new Task();
        autoUpdatetask.subject = clsTaskTriggerHandler.MILESTONE_FAILED_TASK_SUBJECT_PREFIX +'- FIRST CONTACT'; 
        autoUpdatetask.Status = clsTaskTriggerHandler.TASK_CLOSED_RESOLVED_STATUS;
        autoUpdatetask.whatId = testCase.Id;
        //triggering a dummy update so that the case calculates its priority
        testCase.Description = 'Dummy update';
        update testCase; 
        
        insert autoUpdatetask;
        Test.stopTest();

        // Requery case
        testCase = [SELECT Id,EntitlementId,Entitlement.Name, ownerId,jl_Last_Queue__c,jl_Reopened_Count__c, BusinessHoursId , priority FROM Case WHERE Id = :testCase.Id];
        Map<String, List<CaseMilestone>> milestoneNameToCaseMilestoneMap =MilestoneUtilsTest.getCaseMilestonesAsMap(testCase.Id);
        CaseMilestone firstContactAckMilestone = milestoneNameToCaseMilestoneMap.get(MilestoneHandlerSupport.MILESTONE_WARNING)[0];
       // System.assert(!firstContactAckMilestone.IsViolated, 'Next Case Activity milestone should not be violated for the case' );

    }

    @isTest static void WariningNotificationTaskShouldNOTCreateReportingTask() {
        MilestoneUtilsTest.createEntitlementAccounts();
        MilestoneUtilsTest.createEntitlements();
        MilestoneUtilsTest.createCaseRoutingCategorisation();
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        Id caseComplaintRecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Case' AND DeveloperName = 'Complaint' LIMIT 1].Id;
        User newCRDUser;
        Case testCase;
        Contact testContact;
    
        System.runAs(runningUser) {
            newCRDUser = UnitTestDataFactory.getFrontOfficeUser('TEST_CRD_USER');
            newCRDUser.Team__c = MilestoneUtilsTest.HAMILTON_CRD_TEAM_NAME;
            insert newCRDUser;

            testContact = UnitTestDataFactory.createContact();
            insert testContact;
        }

        Date tommorow = Date.today().addDays(1);
        System.runAs(newCRDUser) {
            testCase = UnitTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            insert testCase;
        }

        //we cannot put this code between Test.starttest() and Test.stoptest() since we get the exception. System.NoAccessException: Use Test.setCreatedDate() only before Test.startTest().
        //Modifying the created date of the case to violate the first contact milestone
        Test.setCreatedDate(testCase.Id, Datetime.now().addDays(-2));

        List<Task> reportingTaskList = [select id from Task where RecordTypeId =:clsTaskTriggerHandler.MS_REPORTING_RECORD_TYPE_ID ];
        system.assert (reportingTaskList.isempty(), 'No reporting task should be present');

        //Mocking the task for the dummy update triggered by an entitlement process 
        Task autoUpdatetask  = new Task();
        autoUpdatetask.subject = clsTaskTriggerHandler.MILESTONE_WARNING_TASK_SUBJECT_PREFIX +'- FIRST CONTACT'; 
        autoUpdatetask.Status = clsTaskTriggerHandler.TASK_CLOSED_RESOLVED_STATUS;
        autoUpdatetask.whatId = testCase.Id;
        //triggering a dummy update so that the case calculates its priority
        testCase.Description = 'Dummy update';
        Test.startTest();
        update testCase;
        insert autoUpdatetask;
        

        // Requery case
        testCase = [SELECT Id,EntitlementId,Entitlement.Name, ownerId,jl_Last_Queue__c,jl_Reopened_Count__c, BusinessHoursId , priority FROM Case WHERE Id = :testCase.Id];
        Map<String, List<CaseMilestone>> milestoneNameToCaseMilestoneMap =MilestoneUtilsTest.getCaseMilestonesAsMap(testCase.Id);
        CaseMilestone firstContactAckMilestone = milestoneNameToCaseMilestoneMap.get(MilestoneHandlerSupport.MILESTONE_WARNING)[0];
        //System.assert(!firstContactAckMilestone.IsViolated, 'Next Case Activity milestone should not be violated for the case' );

        reportingTaskList = [select id,RecordTypeId, Milestone_Id__c, whatId, Subject,Case_Owner_Id__c,Case_Last_Queue_Id__c, Milestone_Violated__c,
                             Milestone_Completed_Date__c, Status, Case_Reopened_Count__c , Milestone_Reporting_Key__c from Task
                             where RecordTypeId =: clsTaskTriggerHandler.MS_REPORTING_RECORD_TYPE_ID ];
        System.debug('reportingTaskList' +  reportingTaskList);
        system.assert (reportingTaskList.isempty() ,  ' reporting task should NOT be created ' );
        Test.stopTest();
    }

    /**
* @descriptionTest the number of repeat contact is set to 1 when an initial call log is created
* @author@stuartbarber
*/

    @isTest static void testInitialSettingOfNumberOfRepeatContacts() {

        User runningUser = UserTestDataFactory.getRunningUser();

        User contactCentreUser;

        Contact testContact;
        Case testCase;
        Task testTask;

        System.runAs(runningUser) {

            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;

            UserTestDataFactory.assignPermissionSetToUser(contactCentreUser.Id, 'Call_Logging_Access');
        
        }

        System.runAs(contactCentreUser) {

            testContact = CustomerTestDataFactory.createContact();
            insert testContact;

            testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            testCase.Number_of_Repeat_Contacts__c = null;
            testCase.Repeat_Contact_from_Customer__c = false;
            insert testCase;

            testTask = TaskTestDataFactory.createTask(testContact.Id, testCase.Id, CommonStaticUtils.TASK_RT_NAME_CALL_LOG);
            testTask.Subject = CommonStaticUtils.TASK_SUBJECT_CALL_LOG;
            testTask.Repeat_Contact_from_Customer__c = true;
            insert testTask;

            Case savedCase = [SELECT Number_of_Repeat_Contacts__c, Repeat_Contact_from_Customer__c FROM Case WHERE Id = :testCase.Id];

            System.assertEquals(1, savedCase.Number_of_Repeat_Contacts__c);
            System.assertEquals(true, savedCase.Repeat_Contact_from_Customer__c);

        }
    }

    /**
* @descriptionTest the number of repeat contact increments by 1 each time a call logging task is created
* @author@stuartbarber
*/

    @isTest static void testNumberOfRepeatContactsIncrements() {

        User runningUser = UserTestDataFactory.getRunningUser();

        User contactCentreUser;

        Contact testContact;
        Case testCase;
        Task testTask;

        System.runAs(runningUser) {

            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;

            UserTestDataFactory.assignPermissionSetToUser(contactCentreUser.Id, 'Call_Logging_Access');
        
        }

        System.runAs(contactCentreUser) {

            testContact = CustomerTestDataFactory.createContact();
            insert testContact;

            testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            testCase.Number_of_Repeat_Contacts__c = 4;
            testCase.Repeat_Contact_from_Customer__c = true;
            insert testCase;

            testTask = TaskTestDataFactory.createTask(testContact.Id, testCase.Id, CommonStaticUtils.TASK_RT_NAME_CALL_LOG);
            testTask.Subject = CommonStaticUtils.TASK_SUBJECT_CALL_LOG;
            testTask.Repeat_Contact_from_Customer__c = true;
            insert testTask;

            Case savedCase = [SELECT Number_of_Repeat_Contacts__c, Repeat_Contact_from_Customer__c FROM Case WHERE Id = :testCase.Id];

            System.assertEquals(5, savedCase.Number_of_Repeat_Contacts__c);
            System.assertEquals(true, savedCase.Repeat_Contact_from_Customer__c);

        }
    }

    /**
* @descriptionTest the number of repeat contact does not exceed the maximum amount - capped at 99.
* @author@stuartbarber
*/

    @isTest static void testMaxNumberOfRepeatContacts() {

        User runningUser = UserTestDataFactory.getRunningUser();

        User contactCentreUser;

        Contact testContact;
        Case testCase;
        Task testTask;

        System.runAs(runningUser) {

            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;

            UserTestDataFactory.assignPermissionSetToUser(contactCentreUser.Id, 'Call_Logging_Access');
        
        }

        System.runAs(contactCentreUser) {

            testContact = CustomerTestDataFactory.createContact();
            insert testContact;

            testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            testCase.Number_of_Repeat_Contacts__c = 99;
            testCase.Repeat_Contact_from_Customer__c = false;
            insert testCase;

            testTask = TaskTestDataFactory.createTask(testContact.Id, testCase.Id, CommonStaticUtils.TASK_RT_NAME_CALL_LOG);
            testTask.Subject = CommonStaticUtils.TASK_SUBJECT_CALL_LOG;
            testTask.Repeat_Contact_from_Customer__c = true;
            insert testTask;

            Case savedCase = [SELECT Number_of_Repeat_Contacts__c, Repeat_Contact_from_Customer__c FROM Case WHERE Id = :testCase.Id];

            System.assertEquals(99, savedCase.Number_of_Repeat_Contacts__c);

        }
    }
     //<<002>>
    @isTest static void testCasePromiseConfig() {
        //Insert test contact
        List<case> testCaseList = new List<case>(); 
        Contact con = UnitTestDataFactory.createContact();
        con.Email = 'ab@bb.com';
        insert con;
       
        Case query_case=new Case();
        query_case.ContactId=con.Id;
        query_case.RecordTypeId=queryCaseRTId;
        query_case.Origin='Phone';
        testCaseList.add(query_case);
        
        
        Case complaint_case=new Case();
        complaint_case.ContactId=con.Id;
        complaint_case.RecordTypeId=complaintCaseRTId;
        complaint_case.Origin='Phone';
        complaint_case.Reference_Type__c=null;
        testCaseList.add(complaint_case);
        
        Case pse_case=new Case();
        pse_case.ContactId=con.Id;
        pse_case.RecordTypeId=pseCaseRTId;
        pse_case.Origin='Phone';
        pse_case.Reference_Type__c=null;
        
        testCaseList.add(pse_case);
        
        
        Case nku_case=new Case();
        nku_case.ContactId=con.Id;
        nku_case.RecordTypeId=nKUCaseRTId;
        nku_case.Origin='Phone';
        nku_case.Reference_Type__c=null;
        testCaseList.add(nku_case);
        
        Case newCase=new Case();
        newCase.RecordTypeId=newCaseRTId;
        testCaseList.add(newCase);
        
        insert testCaseList;
        
        Case_Promise_Configuration__c cp= new Case_Promise_Configuration__c();
        cp=CaseActivityUtils.getCasePromiseConfiguration(testCaseList[0], 'Initial Customer Contact Promise');
        
        Case_Promise_Configuration__c compl_cp= new Case_Promise_Configuration__c();
        compl_cp=CaseActivityUtils.getCasePromiseConfiguration(testCaseList[1], 'Initial Customer Contact Promise');
        
        Case_Promise_Configuration__c pse_cp= new Case_Promise_Configuration__c();
        pse_cp=CaseActivityUtils.getCasePromiseConfiguration(testCaseList[2], 'Initial Customer Contact Promise');
        
        Case_Promise_Configuration__c nku_cp= new Case_Promise_Configuration__c();
        nku_cp=CaseActivityUtils.getCasePromiseConfiguration(testCaseList[3], 'Initial Customer Contact Promise');
      
        Case_Promise_Configuration__c new_cp= new Case_Promise_Configuration__c();
        new_cp=CaseActivityUtils.getCasePromiseConfiguration(testCaseList[4], 'Initial Customer Contact Promise');
     
      }
    //<<002>>
    
}