/*************************************************
coCustomerOrdersController

controller extension for the coCustomerOrders page

Author: Steven Loftus (MakePositive)
Created Date: 13/11/2014
Modification Date: 
Modified By: 

**************************************************/
public with sharing class coCustomerOrdersController {

	// holds the order headers returned from the ws calls in the sudo related list on the page
    public List<coOrderHeader> OrderHeaders {get;set;}  

    // search parameters
    public String SearchFirstName {get; set;}
    public String SearchLastName {get; set;}
    public String SearchEmail {get; set;}
    public String SearchPostcode {get; set;} 
    public String SearchOrderNumber {get; set;}

    public Contact Con {get; set;}

    // controll parameters
    private Integer maximumCallouts = 9;

    private coSoapManager privateSoapManager;
    // soap manager
    public coSoapManager SoapManager {
    	get {
    		if (this.privateSoapManager == null) this.privateSoapManager = new coSoapManager();

    		return this.privateSoapManager;
    	}
    }

    // properties to deal with showing more order headers
    public Boolean DisplayShowMore {get; set;}
    public String ShowMoreText {get; set;}
    
    
    public coCustomerOrdersController(ApexPages.StandardController std) {

        // Pre populate the search fields from Customer record.
        Id contactId = std.getId();

        this.Con = [select Id, Salutation, Firstname, Lastname, Name, Email, MailingPostalCode from Contact where Id = : contactId];
        this.SearchFirstName = con.Firstname;
        this.SearchLastName = con.Lastname;
        this.SearchEmail = con.Email;
        this.SearchPostcode = con.MailingPostalCode;

        // initialise the order headers list
        this.OrderHeaders = new List<coOrderHeader>();
    }

    // This methods is called when user clicks on Search button.
    public void getOrders() {

        ApexPages.getMessages().clear();

        this.OrderHeaders.clear();
        this.SoapManager.ClearCustomers();

        try {

            if(String.isBlank(this.SearchFirstName) && String.isBlank(this.SearchLastName) && String.isBlank(this.SearchPostcode) && String.isBlank(this.SearchOrderNumber) && String.isBlank(this.SearchEmail)){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please enter at least one search criteria field before clicking on the Search button'));
                return ;
            }
            
            if((String.isBlank(this.SearchFirstName) || String.isBlank(this.SearchLastName)) && String.isBlank(this.SearchOrderNumber)){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Firstname and Lastname are required fields'));
                return ;
            }
            
            if((String.isBlank(this.SearchEmail) && String.isBlank(this.SearchPostcode) && ( String.isNotBlank(this.SearchFirstname) && String.isNotBlank(searchLastName))) && String.isBlank(searchOrderNumber)){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Email and Postcode cannot be blank at the same time.'));
                return ;
            }
            
            coSoapManager.Customer customerSearchCriteria = new coSoapManager.Customer();
            customerSearchCriteria.FirstName = this.SearchFirstName;
            customerSearchCriteria.LastName = this.SearchLastName;
            customerSearchCriteria.Postcode = this.SearchPostcode;
            customerSearchCriteria.Email = this.SearchEmail;

            if (String.isBlank(this.SearchOrderNumber)) {
                // If OrderNumber is blank, call ViewCustomer and ViewCustomerOrders_V2 web services to get the resutls.
                // minValue and Maxvalue are there to tell the web service that which shopper Ids to process because of the 
                // salesforce governer limit of 10 callouts in a single call.
                this.OrderHeaders = this.SoapManager.getOrderHeaders(this.OrderHeaders, this.maximumCallouts, customerSearchCriteria);

                // Sort the results in ascending order of Email, Postcode and Order Number
                if (this.OrderHeaders.size() > 0) this.OrderHeaders.sort();
            
                // Display the text on search results. If search results are less then 9 records, then dont show "Show more link"
                if (this.SoapManager.MoreCustomers) {
                    this.DisplayShowMore = true;
                    this.ShowMoreText = 'Displaying Orders for the first ' + this.SoapManager.CustomersSearched + ' Customers searched out of the '+ this.SoapManager.CustomersFound +' Customers returned by the search';

                } else {

                    this.DisplayShowMore = false;
                }
            
            } else {
                
                // If Ordernumber is not blank, ignore other seach fields and call ViewCustomerOrders_V1 to get the Order details.
                this.OrderHeaders = this.SoapManager.getOrderHeaders(this.SearchOrderNumber);
                displayShowMore = false;

            }
                        
            system.debug('this.SoapManager [' + this.SoapManager + ']');
            
            if (this.SoapManager.Status != 'Success') {

                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, this.SoapManager.ErrorDescription));
                return;
            }
            
        } catch(Exception e) {

            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage() + '-' + e.getStackTraceString()));
            return ;
        }
       
    }
    
    // This method is called when user clicks on Show more link in the results.
    public void showMore() {
        
        // Web service call out to get data for next 10 shopper Ids.
        this.OrderHeaders = this.SoapManager.getOrderHeaders(this.OrderHeaders, this.maximumCallouts, null);

        // Sort the results in ascending order of Email, Postcode and Order Number
        this.OrderHeaders.sort();
        
        // Hide show more link if this is the last set of records
        displayShowMore = this.SoapManager.MoreCustomers;

        showMoreText = 'Displaying Orders for the first ' + this.SoapManager.CustomersSearched + ' Customers searched out of the '+ this.SoapManager.CustomersFound +' Customers returned by the search';
    }
}