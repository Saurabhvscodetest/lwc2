/**
 *  @author: Andrey Gavrikov (westbrook)
 *  @date: 2014-08-28 12:01:13 
 *  @description:
 *      Test methods for MyJL_ContactProfileHandler
 *  
 *  Version History :   
 *  2014-08-28 - MJL-746 - AG
 *  contact profile update notification
 *      
 * 001  11/05/15    NTJ     We changed the code so that we explicitly do the send notification in the code. Do the same here
 * 002  27/06/16    NA                  Decommissioning contact profile  
 */
@isTest
global class MyJL_ContactProfileHandlerTest  {
    static {
        BaseTriggerHandler.addSkipReason(MyJL_ContactProfileHandler.UPDATE_NOTIFICATION_SKIP_CODE, 'disabled during test data preparation');
    }
    private static String SOURCE_SYSTEM = MyJL_ContactProfileHandler.SOURCE_SYSTEM;
    
    global class UpdateNotificationMock implements WebServiceMock { 
        global void doInvoke( Object stub, Object request, Map<String, Object> response, String endpoint,
                            String soapAction, String requestName, String responseNS, String responseName, String responseType) {

            SFDCMyJLCustomerTypes.PublishCustomerEventResponse responseElement = new SFDCMyJLCustomerTypes.PublishCustomerEventResponse();
            response.put('response_x', responseElement);
        } 
    }

    /**
     * check if callout mechanism is initiated on Profile Update
     */
    static testMethod void testContactProfileUpdateWithEmail() {
        testContactProfileUpdate('example.user@test.xyz', 1);
    } 
     
    static testMethod void testContactProfileUpdateWithNoEmail() {
        testContactProfileUpdate(null, 0);      
    } 

    static void testContactProfileUpdate (String email, Integer expectedCount) {

        BaseTriggerHandler.addSkipReason(MyJL_ContactProfileHandler.UPDATE_NOTIFICATION_SKIP_CODE, 'disabled during test data preparation');
        Contact contact = new Contact(LastName = 'Test');
        Database.insert(contact);

        contact profile = new contact(SourceSystem__c = SOURCE_SYSTEM, email = email,lastname='lastname');
        Database.insert(profile);
        List<contact> profileList = new List<contact>();
        profileList.add(profile);

        Loyalty_Account__c account = new Loyalty_Account__c(IsActive__c = false, activation_date_time__c=system.now(),contact__c = profile.Id, email_Address__c = email);
        Database.insert(account);
        
        Loyalty_Card__c card = new Loyalty_Card__c(Loyalty_Account__c = account.Id, Name = 'test-card-id');
        Database.insert(card);

        //setup callout config
        CalloutHandlerTest.initCalloutSettings(LogUtil.SERVICE.OUTBOUND_CONTACT_PROFILE_UPDATE_NOTIFY.name(), 'MyJL_ContactProfileHandler', 10);
        Test.setMock(WebServiceMock.class, new UpdateNotificationMock());
        //enable logging
        CustomSettings.setTestLogHeaderConfig(true, -1);
        CustomSettings.setTestLogDetailConfig(true, -1);
        
        Test.startTest();
        BaseTriggerHandler.removeSkipReason(MyJL_ContactProfileHandler.UPDATE_NOTIFICATION_SKIP_CODE);
        //fire @future method
        Database.update(profile);
        // 001 - we now do this after the update - so we have to do it here
        myJL_ContactProfileHandler.sendUpdateNotification(profileList);                         
        Test.stopTest();

        //check that outbound notification log record has been inserted
        System.assertEquals(expectedCount, [select count() from Log_Header__c where Service__c = :LogUtil.SERVICE.OUTBOUND_CONTACT_PROFILE_UPDATE_NOTIFY.name() ], 
                                'Expected log record for outbound notification we have just done');
    }
   
}