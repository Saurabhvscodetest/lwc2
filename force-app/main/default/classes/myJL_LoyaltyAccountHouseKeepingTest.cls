/** Test the Loyalty Account tie up batch process
 */
@isTest
private class myJL_LoyaltyAccountHouseKeepingTest {
	static {
		//prepare configuration
		Database.insert(new House_Keeping_Config__c(Name = 'LoyaltyAccountBatch', Class_Name__c = 'myJL_LoyaltyAccountHouseKeeping', Order__c = 0, Run_Frequency_Days__c = 1));
	}

    static testMethod void test() {
		CustomSettings.setTestLogHeaderConfig(true, -1);
		Database.insert(new Log_Header__c(Name = 'LoyaltyAccountBatch', Service__c = 'LoyaltyAccountBatch'));

        // create a contact profile with shopperId and email
		contact cp = new contact(lastname='tester', Shopper_ID__c='sh1', email='email1@test.com', SourceSystem__c=MyJL_UpdateCustomerHandler.SOURCE_SYSTEM);
		insert cp;
		contact cpDistractor = new contact(lastname='tester', Shopper_ID__c='sh2', email='email2@test.com', SourceSystem__c=MyJL_UpdateCustomerHandler.SOURCE_SYSTEM);
		insert cpDistractor;

        // create an account with same but don't link
		Loyalty_Account__c la = new Loyalty_Account__c(ShopperID__c='sh1', email_Address__c='email1@test.com', Membership_Number__c='mn1');
		insert la;

        // create a card and link to account
        Loyalty_Card__c lc = new Loyalty_Card__c(Loyalty_Account__c=la.Id, Card_Number__c='c1');
        insert lc;
        
        // run the job
        Test.StartTest();
			Database.executeBatch(new HouseKeeperBatch(new myJL_LoyaltyAccountHouseKeeping(), true));
        Test.StopTest();
        
        // check that account got linked to card
        List<Loyalty_Account__c> las = [SELECT contact__c, adopted__c FROM Loyalty_Account__c];
        system.assertEquals(1, las.size());
        system.assertEquals(true, las[0].adopted__c);
        system.assertEquals(cp.Id, las[0].contact__c);        
    }
}