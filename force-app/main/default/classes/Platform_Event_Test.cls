@isTest
public class Platform_Event_Test {
     private static final String PARTNERSHIP_CARD_NUMBER = '3456789456123';
     private static final String ORIGIN = 'Unit Test';
     private static final DateTime DATE_CREATED = DateTime.NOW(); 
     static testMethod void subscribePlatformEvent () {
        Contact customer = new Contact(LastName = 'Test', SourceSystem__c='johnlewis.com', Shopper_ID__c='123456789123');
        Database.insert(customer);
        
        Loyalty_Account__c account = new Loyalty_Account__c( Email_Address__c = 'test666@test.com', Contact__c = customer.id,
                                                            ShopperID__c = customer.Shopper_ID__c,
                                                            Voucher_Preference__c = 'Digital',
                                                            IsActive__c = true,
                                                           Membership_Number__c = '1234567890');
        Database.insert(account);
        Loyalty_Card__c myJLCard = new Loyalty_Card__c(Loyalty_Account__c = account.Id, Name = 'myJL-card-id', Card_Type__c = MyJL_Const.MY_JL_CARD_TYPE);
        Database.insert(myJLCard);
        List<Customer_Status_Platform_Event__e> PlatformEventsub = new List<Customer_Status_Platform_Event__e>();
        Test.startTest();
        PlatformEventsub.add(New Customer_Status_Platform_Event__e(
                                                           Is_Active__c=account.IsActive__c,
                                                           Shopper_ID__c=account.ShopperId__c,
                                                           Loyalty_Number__c=account.Membership_Number__c,
                                                           Barcode__c = myJLCard.name,
                                                           Flag_Type__c= myJLCard.Card_Type__c                        
                                                       ));
        EventBus.publish(PlatformEventsub);
        Test.stopTest();
    }
   static testmethod void LoyaltyAccountUpdate(){
       Contact customer = new Contact(LastName = 'Test', SourceSystem__c='johnlewis.com', Shopper_ID__c='123456789123');
        Database.insert(customer);
        
        Loyalty_Account__c account = new Loyalty_Account__c( Email_Address__c = 'test666@test.com', Contact__c = customer.id,
                                                            ShopperID__c = customer.Shopper_ID__c,
                                                            Voucher_Preference__c = 'Digital',
                                                            IsActive__c = true);
        Database.insert(account);
        Loyalty_Card__c myJLCard = new Loyalty_Card__c(Loyalty_Account__c = account.Id, Name = 'myJL-card-id', Card_Type__c = MyJL_Const.MY_JL_CARD_TYPE);
        Database.insert(myJLCard);
        CalloutHandlerTest.initCalloutSettings(MyJL_LoyaltyAccountHandler.FULL_JOIN_NOTIFY, 'MyJL_LoyaltyAccountHandler.JoinLeaveCallout', 10);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        RestContext.request = req;
        RestContext.response = res;
        
        req.requestURI = '/MyLoyaltyService/Subscribe/' + customer.Shopper_ID__c;
        req.httpMethod = 'PUT';
        
        Test.startTest();
        BaseTriggerHandler.removeSkipReason('JoinNotification');
        
        MyLoyaltyService.joinShopper( PARTNERSHIP_CARD_NUMBER, ORIGIN, DATE_CREATED);
        
        Contact contactProfile = [SELECT Id, Name, (SELECT Id, Name, IsActive__c, Join_Date__c, ShopperId__c,
                                                    Membership_Number__c, Scheme_Joined_Date_Time__c, 
                                                    Activation_Date_Time__c, Deactivation_Date_Time__c, 
                                                    contact__c, contact__r.id, 
                                                    contact__r.Name, Channel_ID__c 
                                                    FROM MyJL_Accounts__r WHERE IsActive__c = true ORDER BY Join_Date__c),
                                  Shopper_Id__c, AnonymousFlag2__c, Birthdate, Salutation,
                                  firstname, lastname, email, MobilePhone, Mailing_House_No_Text__c,
                                  Mailing_House_Name__c, mailingstreet, mailingcity, mailingstate,
                                  mailingcountry, mailingpostalcode, mailingcountrycode, lastmodifieddate
                                  FROM contact
                                  WHERE Id =: customer.Id];    
        SFDCMyJLCustomerTypes.Customer customerToBeSent = MyJL_MappingUtil.populateCustomer2(contactProfile, null); 
        
        SFDCMyJLCustomerTypes.CustomerAccount customerAccount = customerToBeSent.CustomerAccount[0];
       
        Integer UpdatedAccountSize = [select count() from Loyalty_Card__c where Card_Type__c='My Partnership' and Disabled__c= true];
        Loyalty_Account__c LoyaltyAccountList = [select IsActive__c,Membership_Number__c,MyJL_Old_Membership_Number__c,ShopperId__c,Voucher_Preference__c from Loyalty_Account__c];
        List<Customer_Status_Platform_Event__e> PlatformEventsub = new List<Customer_Status_Platform_Event__e>();
        PlatformEventsub.add(New Customer_Status_Platform_Event__e(
                                                           Is_Active__c=LoyaltyAccountList.IsActive__c,
                                                           Shopper_ID__c=LoyaltyAccountList.ShopperId__c,
                                                           Loyalty_Number__c=LoyaltyAccountList.Membership_Number__c,
                                                           Barcode__c = '123456789',
            											   Voucher_Type__c = LoyaltyAccountList.Voucher_Preference__c,
                                                           Flag_Type__c= 'My Partnership'                        
                                                       ));
        EventBus.publish(PlatformEventsub);
    }
    
}