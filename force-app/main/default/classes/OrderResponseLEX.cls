public with sharing class OrderResponseLEX {
	public OrderResponseLEX() {	}

	@AuraEnabled public String Status {get; set;}
    @AuraEnabled public String ErrorDescription {get; set;}
	@AuraEnabled public List<coOrderHeader> Orders {get; set;}
}