/*************************************************
coOrderDetailController_TEST

test class for the coOrderDetailController controller

Author: Steven Loftus (MakePositive)
Created Date: 18/11/2014
Modification Date: 
Modified By: 

**************************************************/
@isTest
private class coOrderDetailController_TEST {

    public static testmethod void getOrderDetails() {

        Config_Settings__c configSearch = new Config_Settings__c();
        configSearch.Name = CustomSettingsManager.WS_Endpoint_SearchCustomer;
        configSearch.Text_Value__c = 'SearchCustomer';
        insert configSearch;

        // set up the config settings
        Config_Settings__c configOrders = new Config_Settings__c();
        configOrders.Name = CustomSettingsManager.WS_Endpoint_ViewCustomerOrdersV1;
        configOrders.Text_Value__c = 'ViewCustomerOrdersV1';
        insert configOrders;

        // insert a contact to hang the page off
        Contact searchContact = new Contact();
        searchContact.FirstName = 'Steve';
        searchContact.LastName = 'Loftus';
        searchContact.Email = 'test@nowhere.com';
        searchContact.MailingPostalCode = 'HX7 6DJ';
        insert searchContact;

        // get the page
        PageReference orderDetailsPage = Page.coOrderDetail;
        Test.setCurrentPage(orderDetailsPage);

        // push the url params
        ApexPages.currentPage().getParameters().put('orderId', '12345678');
        ApexPages.currentPage().getParameters().put('customerId', searchContact.Id);


        Test.setMock(HttpCalloutMock.class, new coSoapGetOrderDetail_MOCK());

        Test.startTest();
        // create a controller
        coOrderDetailController orderDetailController = new coOrderDetailController();
        Test.stopTest();

        //system.assertEquals('100127241', orderDetailController.Order.OrderNumber, 'Order not found');
    }
    
    public static testmethod void getOrderDetailsError() {

        Config_Settings__c configSearch = new Config_Settings__c();
        configSearch.Name = CustomSettingsManager.WS_Endpoint_SearchCustomer;
        configSearch.Text_Value__c = 'SearchCustomer';
        insert configSearch;

        // set up the config settings
        Config_Settings__c configOrders = new Config_Settings__c();
        configOrders.Name = CustomSettingsManager.WS_Endpoint_ViewCustomerOrdersV1;
        configOrders.Text_Value__c = 'ViewCustomerOrdersV1Error';
        insert configOrders;

        // insert a contact to hang the page off
        Contact searchContact = new Contact();
        searchContact.FirstName = 'Steve';
        searchContact.LastName = 'Loftus';
        searchContact.Email = 'test@nowhere.com';
        searchContact.MailingPostalCode = 'HX7 6DJ';
        insert searchContact;

        // get the page
        PageReference orderDetailsPage = Page.coOrderDetail;
        Test.setCurrentPage(orderDetailsPage);

        // push the url params
        ApexPages.currentPage().getParameters().put('orderId', '12345678');
        ApexPages.currentPage().getParameters().put('customerId', searchContact.Id);


        Test.setMock(HttpCalloutMock.class, new coSoapGetOrderDetail_MOCK());

        Test.startTest();
        // create a controller
        coOrderDetailController orderDetailController = new coOrderDetailController();
        Test.stopTest();

        //system.assertEquals(null, orderDetailController.Order.OrderNumber, 'Order was not found');
    }
    
    public static testmethod void getOrderDetailsNonPartialReadWarning() {

        Config_Settings__c configSearch = new Config_Settings__c();
        configSearch.Name = CustomSettingsManager.WS_Endpoint_SearchCustomer;
        configSearch.Text_Value__c = 'SearchCustomer';
        insert configSearch;

        // set up the config settings
        Config_Settings__c configOrders = new Config_Settings__c();
        configOrders.Name = CustomSettingsManager.WS_Endpoint_ViewCustomerOrdersV1;
        configOrders.Text_Value__c = 'ViewCustomerOrdersV1NonPartialReadWarning';
        insert configOrders;

        // insert a contact to hang the page off
        Contact searchContact = new Contact();
        searchContact.FirstName = 'Steve';
        searchContact.LastName = 'Loftus';
        searchContact.Email = 'test@nowhere.com';
        searchContact.MailingPostalCode = 'HX7 6DJ';
        insert searchContact;

        // get the page
        PageReference orderDetailsPage = Page.coOrderDetail;
        Test.setCurrentPage(orderDetailsPage);

        // push the url params
        ApexPages.currentPage().getParameters().put('orderId', '12345678');
        ApexPages.currentPage().getParameters().put('customerId', searchContact.Id);


        Test.setMock(HttpCalloutMock.class, new coSoapGetOrderDetail_MOCK());

        Test.startTest();
        // create a controller
        coOrderDetailController orderDetailController = new coOrderDetailController();
        Test.stopTest();

        //system.assertEquals('Customer Order ID not found on the database', ApexPages.getMessages()[0].getSummary(), 'Warning was not ignored');
    }
    
}