//
// Test class for the HouseKeeperBatchController
//
@isTest	
public with sharing class HouseKeeperBatchControllerTest {

	private static testmethod void test_Basic() {
		// instantiate
		HouseKeeperBatchController hkbc = new HouseKeeperBatchController();
		system.assertNotEquals(null, hkbc);
		// test initial values
		system.assertEquals(HouseKeeperBatchController.DEFAULT_BATCH_NAME, hkbc.batchName);
		// use a test name to avoid any running service on the Salesforce instance
		hkbc.batchName = 'BatchUnderTest';
		system.assertEquals(HouseKeeperBatchController.STATUS_OFF, hkbc.status); 
		system.assertEquals(false, hkbc.running); 
		system.assertEquals(0,hkbc.jobs.size());
		system.assertEquals(0,hkbc.jobTriggers.size());		
	}
	
	private static testmethod void test_Start() {
		// instantiate
		HouseKeeperBatchController hkbc = new HouseKeeperBatchController();
		system.assertNotEquals(null, hkbc);
		// use a test name to avoid any running service on the Salesforce instance
		hkbc.batchName = 'BatchUnderTest';

		Test.StartTest();
			hkbc.start();
		Test.StopTest();
		
		// test values after start
		system.assertEquals(HouseKeeperBatchController.STATUS_ON, hkbc.status);
		system.assertEquals(true, hkbc.running);
		system.assertEquals(1,hkbc.jobTriggers.size());		
	}
	
	private static testmethod void test_StartStop() {
		// instantiate
		HouseKeeperBatchController hkbc = new HouseKeeperBatchController();
		system.assertNotEquals(null, hkbc);
		// use a test name to avoid any running service on the Salesforce instance
		hkbc.batchName = 'BatchUnderTest';

		Test.StartTest();
			hkbc.start();
			hkbc.stop();
		Test.StopTest();
		
		// test values after start
		//system.assertEquals(HouseKeeperBatchController.STATUS_OFF, hkbc.status); 
		system.assertEquals(false, hkbc.running);
		system.assertEquals(0,hkbc.jobTriggers.size());		
	}		
}