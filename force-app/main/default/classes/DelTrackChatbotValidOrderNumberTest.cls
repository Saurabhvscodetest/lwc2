/*
* @File Name   : DelTrackChatbotValidOrderNumber 
* @Description : Test Class to related to MyJLChatBot Delivery Tracking functionalities  
* @Copyright   : Zensar
* @Jira #      : #5174
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0      18-MAR-20          Vijay A                       Created
*/
@IsTest
Public class DelTrackChatbotValidOrderNumberTest {

@IsTest
    Public Static void testMethodForDelTrackChatBotProcess(){
        
        DelTrackChatbotValidOrderNumber.DelTrackChatBotRequest delRequest = new DelTrackChatbotValidOrderNumber.DelTrackChatBotRequest();
        delRequest.orderNumberInput = '213456789';
        List<DelTrackChatbotValidOrderNumber.DelTrackChatBotRequest> listDelRequest = new List<DelTrackChatbotValidOrderNumber.DelTrackChatBotRequest>();
        listDelRequest.add(delRequest);
               
        DelTrackChatbotValidOrderNumber.DelTrackChatBotProcess(listDelRequest);

    }

@IsTest
    Public Static void testMethodForDelTrackChatBotProcessNew(){

        DelTrackChatbotValidOrderNumber.DelTrackChatBotRequest delRequest1 = new DelTrackChatbotValidOrderNumber.DelTrackChatBotRequest();
        delRequest1.orderNumberInput = 'I dont have a valid number';
        List<DelTrackChatbotValidOrderNumber.DelTrackChatBotRequest> listDelRequest1 = new List<DelTrackChatbotValidOrderNumber.DelTrackChatBotRequest>();
        listDelRequest1.add(delRequest1);

        DelTrackChatbotValidOrderNumber.DelTrackChatBotProcess(listDelRequest1);

    }
    
    
@IsTest
    Public Static void testMethodForDelTrackChatBotProcessException(){

        DelTrackChatbotValidOrderNumber.DelTrackChatBotRequest delRequest1 = new DelTrackChatbotValidOrderNumber.DelTrackChatBotRequest();
        
        List<DelTrackChatbotValidOrderNumber.DelTrackChatBotRequest> listDelRequest1 = new List<DelTrackChatbotValidOrderNumber.DelTrackChatBotRequest>();
        listDelRequest1.add(delRequest1);

        DelTrackChatbotValidOrderNumber.DelTrackChatBotProcess(listDelRequest1);

    }

}