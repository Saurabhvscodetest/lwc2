/* Class that Maps the XML CC Response to the VO object  */
// public class EHClickAndCollectResponseMapper extends EHServiceResponseMapper{
public class EHClickAndCollectResponseMapper {
    /*
    public static final String NODE_ROUTE_DROP_COLL_NODES = 'RouteDropCollectionNodes';
    public static final String NODE_SHIPMENT_ROUE_PLAN = 'ShipmentRoutePlan';
    public static final String NODE_CUST_DEL_HUB = 'CustomerDeliveryHub';
    public static final String NODE_SHIP_TO_NAME = 'ShipToName';
    public static final String NODE_PLANNED_COLL_SERVICE = 'PlannedCustomerDeliveryCollectionService';
    public static final String NODE_SLOT_END_TIME = 'SlotEndTime';
    public static final String NODE_SLOT_START_TIME = 'SlotStartTime';
    public static final String NODE_RETAIL_SHIP_STATE_CODE = 'RetailTransactionShipmentStateCode';
    public static final String NODE_POSTAL_ADDRESS = 'PostalAddress';
    public static final String NODE_POSTAL_CODE_REF = 'PostalCodeReference';
    public static final String NODE_POSTAL_CODE = 'PostalCode';
    public static final String NODE_SALE_RETURN_ITEM = 'SaleReturnLineItem';
    public static final String NODE_CUST_ORDER_LINE_ITEM = 'CustomerOrderLineItem';
    public static final String NODE_CUST_ORDER = 'CustomerOrder';
    public static final String NODE_CUST_ORDER_ID = 'CustomerOrderID';
    public static final String NODE_CUST_ORDER_ASSIGNMENT = 'CustomerOrderStateAssignment';
    public static final String NODE_CUST_ORDER_STATE = 'CustomerOrderState';
    public static final String NODE_CUST_ORDER_NAME = 'CustomerOrderStateName';
    public static final String NODE_CUST = 'Customer';
    public static final String NODE_PERSON = 'Person';
    public static final String NODE_FIRST_NAME = 'FirstName';
    public static final String NODE_LAST_NAME = 'LastName';
    public static final String NODE_COLLECTION_LOCATION = 'CollectionLocation';
    public static final String NODE_WAITROSE_COLLECTION_LOCATION = 'WaitroseClickAndCollectLocation';
    public static final String NODE_WAITROSE_LONG_NAME = 'WaitroseLongName';
    public static final String NODE_AVAILABLE_FOR_COLLECTION_DATE = 'AvailableForCollectionDate';
    
    
    public override DeliveryTrackingVO mapResponse(Dom.XMLNode rootNode){
        DeliveryTrackingVO  delTrackingVO = new DeliveryTrackingVO();
        delTrackingVO.deliveryType = CLICK_AND_COLLECT;
        try{
            
            if(rootNode != null) {
                Dom.XMLNode bodyNode= rootNode.getChildElement(NODE_BODY, SOAP_ENV_NS);
                if(bodyNode != null){
                    Dom.XMLNode showRetailTransactionShipmentNode= bodyNode.getChildElement(NODE_SHOW_RETAIL_SHIPMENT, SERVICE_NS );
                    if(showRetailTransactionShipmentNode != null) {
                        Dom.XMLNode dataAreaNode = showRetailTransactionShipmentNode.getChildElement(NODE_DATA_AREA, null);
                        if(dataAreaNode != null) {
                           DeliveryTrackingVO.CCOrderInformation orderInfo = new DeliveryTrackingVO.CCOrderInformation();
                           delTrackingVO.ccOrderInf =  orderInfo;
                           Dom.XMLNode retailTransactionShipmentNode = dataAreaNode.getChildElement(NODE_REATIL_SHIPMENT, null);
                          
                           if(retailTransactionShipmentNode != null) {
                                Dom.XMLNode retailTransactionShipmentItemNode = retailTransactionShipmentNode.getChildElement(NODE_REATIL_SHIPMENT_ITEM, null);
                                if(retailTransactionShipmentItemNode != null) {
                                    Dom.XMLNode saleReturnLineItemNode = retailTransactionShipmentItemNode.getChildElement(NODE_SALE_RETURN_ITEM, null);
                                    if(saleReturnLineItemNode != null) {
                                        Dom.XMLNode customerOrderLineItemNode = saleReturnLineItemNode.getChildElement(NODE_CUST_ORDER_LINE_ITEM, null);
                                        if(customerOrderLineItemNode != null) {
                                            Dom.XMLNode customerOrderNode = customerOrderLineItemNode.getChildElement(NODE_CUST_ORDER, null);
                                            if( customerOrderNode != null) {
                                                Dom.XMLNode customerOrderIDNode = customerOrderNode.getChildElement(NODE_CUST_ORDER_ID, null);
                                                //Delivery ID
                                                orderInfo.deliveryId = getValue(customerOrderIDNode);
                                                
                                                Dom.XMLNode customerOrderStateAssignmentNode = customerOrderNode.getChildElement(NODE_CUST_ORDER_ASSIGNMENT, null);
                                                if(customerOrderStateAssignmentNode != null) {
                                                    Dom.XMLNode customerOrderStateNode = customerOrderStateAssignmentNode.getChildElement(NODE_CUST_ORDER_STATE, null);
                                                    if(customerOrderStateNode!= null){
                                                        Dom.XMLNode customerOrderStateNameNode = customerOrderStateNode.getChildElement(NODE_CUST_ORDER_NAME, null);
                                                        //Order Status
                                                        orderInfo.orderStatus = getValue(customerOrderStateNameNode);
                                                    }
                                                }
                                                Dom.XMLNode customerNode = customerOrderNode.getChildElement(NODE_CUST, null);
                                                if(customerNode != null) {
                                                    Dom.XMLNode personNode = customerNode.getChildElement(NODE_PERSON, null);
                                                    if(personNode != null) {
                                                        Dom.XMLNode firstNameNode = personNode.getChildElement(NODE_FIRST_NAME, null);
                                                        Dom.XMLNode lastNameNode = personNode.getChildElement(NODE_LAST_NAME, null);
                                                        //Customer Name
                                                        orderInfo.customerName = getValue(firstNameNode) + ' ' + getValue(lastNameNode);
                                                    }
                                                    
                                                }
                                                Dom.XMLNode fulfilmentEventNode = customerOrderNode.getChildElement(NODE_FULLFILLMENT_EVENT, null);
                                                if(fulfilmentEventNode != null) {
                                                    Dom.XMLNode actualEndDateTimestampNode = fulfilmentEventNode.getChildElement(NODE_ACTUAL_DATE_TIME_STAMP, null);
                                                    orderInfo.timeOfTheSatus = getDateTime(getValue(actualEndDateTimestampNode));
                                                }
                                                
                                                
                                            }
                                        }
                                    }
                                    
                                    //Expected Location
                                    
                                    Dom.XMLNode parcelForDeliveryNode = retailTransactionShipmentItemNode.getChildElement(NODE_PARCEL_FOR_DELIVERY, null);
                                    if(parcelForDeliveryNode != null) {
                                        Dom.XMLNode routeDropCollectionNodesNode = parcelForDeliveryNode.getChildElement(NODE_ROUTE_DROP_COLL_NODES, null);
                                        if(routeDropCollectionNodesNode != null) {
                                            Dom.XMLNode collectionLocationNode = routeDropCollectionNodesNode.getChildElement(NODE_COLLECTION_LOCATION, null);
                                            if(collectionLocationNode != null){
                                                Dom.XMLNode waitroseClickAndCollectLocationNode = collectionLocationNode.getChildElement(NODE_WAITROSE_COLLECTION_LOCATION, null);
                                                if(waitroseClickAndCollectLocationNode != null){
                                                    Dom.XMLNode waitroseLongNameNode = waitroseClickAndCollectLocationNode.getChildElement(NODE_WAITROSE_LONG_NAME, null);
                                                    orderInfo.expectedLocation = getValue(waitroseLongNameNode);
                                                }
                                            }
                                        }
                                    }
                                    
                                    
                                }
                                Dom.XMLNode plannedCustomerDeliveryCollectionServiceNode = retailTransactionShipmentNode.getChildElement(NODE_PLANNED_COLL_SERVICE, null);
                                if(plannedCustomerDeliveryCollectionServiceNode != null) {
                                    Dom.XMLNode availableForCollectionDateNode = plannedCustomerDeliveryCollectionServiceNode.getChildElement(NODE_AVAILABLE_FOR_COLLECTION_DATE, null);
                                    orderInfo.availableForCollDate = getDateTime(getValue(availableForCollectionDateNode));
                                }
                                
                                
                           }
                           
                         
                            Integer index = 0;
                            Map<String, List<DeliveryTrackingVO.CCParcelInformation>> parcelsMap = new Map<String, List<DeliveryTrackingVO.CCParcelInformation>>();
                            List<Dom.XMLNode> retailShipChildList = retailTransactionShipmentNode.getChildElements();
                            for(Dom.XMLNode retShipChild: retailShipChildList){
                                //EACH PARCEL   
                                if(retShipChild.getName().equalsIgnoreCase(NODE_REATIL_SHIPMENT_ITEM)) {
                                    
                                    List<Dom.XMLNode> retailTransactionShipmentItemChildList = retShipChild.getChildElements();
                                    for(Dom.XMLNode retailTransactionShipmentItemChildNode : retailTransactionShipmentItemChildList) {
                                        //Based on the API assumptions - We must get only 1 ParcelForDelivery per  retailTransactionShipmentItemChildNode                            
                                        if(retailTransactionShipmentItemChildNode.getName().equalsIgnoreCase(NODE_PARCEL_FOR_DELIVERY)) {
                                            List<DeliveryTrackingVO.CCParcelInformation> parcelInfoList = new List<DeliveryTrackingVO.CCParcelInformation>();
                                            String parcelId = PARCEL_KEY + (index++);
                                            //Parcel Number
                                            Dom.XMLNode parcelBarCodeNode = retailTransactionShipmentItemChildNode.getChildElement(NODE_PARCEL_BAR_CODE, null) ;
                                            String parcelNo = getValue(parcelBarCodeNode);
                                            //Parcel Status
                                            Dom.XMLNode parcelStatusNode = retailTransactionShipmentItemChildNode.getChildElement(NODE_PARCEL_STATUS, null) ;
                                            String parcelStatus = getValue(parcelStatusNode);
                                            //FulfilmentEvent
                                            List<Dom.XMLNode> parcelForDeliveryChildNodes = retailTransactionShipmentItemChildNode.getChildElements();
                                            for(Dom.XMLNode parcelForDeliveryChildNode : parcelForDeliveryChildNodes) {
                                                 //EACH PARCEL EVENTS   
                                                 if(parcelForDeliveryChildNode.getName().equalsIgnoreCase(NODE_FULLFILLMENT_EVENT)) {
                                                    DeliveryTrackingVO.CCParcelInformation parcelInfo = new DeliveryTrackingVO.CCParcelInformation();
                                                    parcelInfo.parcelId = parcelId;
                                                    parcelInfo.parcelNo = parcelNo;
                                                    parcelInfo.parcelStatus = parcelStatus;
                                                    
                                                    Dom.XMLNode descriptionNode = parcelForDeliveryChildNode.getChildElement(NODE_DESCRIPTION, null) ;
                                                    parcelInfo.eventStatus = getValue(descriptionNode);
                                                    
                                                    Dom.XMLNode actualEndDateTimestampNode = parcelForDeliveryChildNode.getChildElement(NODE_ACTUAL_DATE_TIME_STAMP, null) ;
                                                    parcelInfo.eventTime = getDateTime(getValue(actualEndDateTimestampNode));
                                                    
                                                    List<Dom.XMLNode> fulfilmentEventChildNodes = parcelForDeliveryChildNode.getChildElements();
                                                    for(Dom.XMLNode fulfilmentEventChildNode : fulfilmentEventChildNodes) {
                                                        if(fulfilmentEventChildNode.getName().equalsIgnoreCase(NODE_LOCATION)) {
                                                            Dom.XMLNode parentLocationNode = fulfilmentEventChildNode.getChildElement(NODE_PARENT_LOCATION, null) ;
                                                            if(parentLocationNode != null){
                                                                Dom.XMLNode locationDescriptionNode = parentLocationNode.getChildElement(NODE_LOCATION_DESCRIPTION, null) ;
                                                                parcelInfo.currentLocation = getValue(locationDescriptionNode);
                                                            }
                                                        }
                                                        if(fulfilmentEventChildNode.getName().equalsIgnoreCase(NODE_REASON)) {
                                                            Dom.XMLNode nameNode = fulfilmentEventChildNode.getChildElement(NODE_NAME, null) ;
                                                            parcelInfo.furtherInformation = getValue(nameNode);
                                                        }
                                                    }
                                                    
                                                    parcelInfoList.add(parcelInfo);
                                                 }  
                                                 
                                            }
                                            if(!parcelInfoList.isEmpty()){
                                                parcelsMap.put(parcelId, parcelInfoList);
                                             } else {
                                                //If the parcel has no events display just the parcel information
                                                DeliveryTrackingVO.CCParcelInformation parcelInfo = new DeliveryTrackingVO.CCParcelInformation();
                                                parcelInfo.parcelId = parcelId;
                                                parcelInfo.parcelNo = parcelNo;
                                                parcelInfo.parcelStatus = parcelStatus;
                                                parcelsMap .put(parcelId, new  List<DeliveryTrackingVO.CCParcelInformation>{parcelInfo});
                                            }
                                        }
                                    }
                                        
                                }
                            }
                            orderInfo.parcelInformationList = parcelsMap;
                        }
                    }
                }
            }
        } catch (Exception exp) {
            system.debug(Logginglevel.ERROR,'ERROR ==>' + exp.getStackTraceString());
            delTrackingVO = getErrorInfo(exp.getStackTraceString());
        }
        return delTrackingVO;
    }
    */
}