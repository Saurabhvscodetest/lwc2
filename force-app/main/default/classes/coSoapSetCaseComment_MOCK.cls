/*************************************************
coSoapSetCaseComment_MOCK

Mock class to be used with coSoapPostCaseComments
Author: Steven Loftus (MakePositive)
Created Date: 19/11/2014
Modification Date: 
Modified By: 

**************************************************/
@isTest
global class coSoapSetCaseComment_MOCK implements HttpCalloutMock {
	
	global HttpResponse respond(HttpRequest req) {

		HttpResponse res = new HttpResponse();

		if (req.getEndpoint() == 'ManageCustomerComments') {

			res.setBody('<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body><managecusoc:AcknowledgeCustomerOrderComment systemEnvironmentCode="Production" languageCode="en-GB" xmlns:managecusoc="http://soa.johnlewispartnership.com/service/es/Transaction/CustomerOrder/CustomerOrderComment/ManageCustomerOrderComment/v1"><cmnEBM:ApplicationArea sentDateTime="2014-11-19T16:45:38.656Z" xmlns:cmnEBM="http://soa.johnlewispartnership.com/schema/ebm/CommonEBM/v1"><cmnEBM:Sender><cmnEBM:LogicalID schemeID="ManageCustomerOrderComment-CreateCustomerOrderComment" schemeAgencyID="JL"/></cmnEBM:Sender><wsa:MessageID xmlns:wsa="http://www.w3.org/2005/08/addressing">b5fed499-e382-4ad3-937c-fa3507ff19bc</wsa:MessageID><cmnEBM:TrackingID>b56431dd-5ebd-27cd-525d-b7e0c291930</cmnEBM:TrackingID></cmnEBM:ApplicationArea><managecusocEBM:DataArea xmlns:managecusocEBM="http://soa.johnlewispartnership.com/schema/ebm/Transaction/CustomerOrder/ManageCustomerOrderComment/v1"><managecusocEBM:Acknowledge responseCode="Success" hasActionStatusRecords="true"><cmnEBM:OriginalApplicationArea sentDateTime="2014-07-04T09:30:47.321+01:00" xmlns:cmnEBM="http://soa.johnlewispartnership.com/schema/ebm/CommonEBM/v1"><cmnEBM:Channel><cmnEBM:ChannelID schemeID="OCIP" schemeAgencyID="JL"/><cmnEBM:ChannelName/><cmnEBM:ChannelType>filler</cmnEBM:ChannelType></cmnEBM:Channel><cmnEBM:Sender><cmnEBM:LogicalID schemeID="CaseManagement" schemeAgencyID="JL"/></cmnEBM:Sender><wsa:MessageID xmlns:wsa="http://www.w3.org/2005/08/addressing">265aac97-a47a-cf9d-4f28-b23806618d6</wsa:MessageID><cmnEBM:TrackingID>b56431dd-5ebd-27cd-525d-b7e0c291930</cmnEBM:TrackingID></cmnEBM:OriginalApplicationArea><cmnEBM:ActionStatusRecord xmlns:cmnEBM="http://soa.johnlewispartnership.com/schema/ebm/CommonEBM/v1"><cmnEBM:NounID schemeID="26522740"/><cmnEBM:ActionStatus>Success</cmnEBM:ActionStatus><cmnEBM:ActionDateTime>2014-11-19T16:45:38.547Z</cmnEBM:ActionDateTime><cmnEBM:Message>Success</cmnEBM:Message></cmnEBM:ActionStatusRecord></managecusocEBM:Acknowledge></managecusocEBM:DataArea></managecusoc:AcknowledgeCustomerOrderComment></soapenv:Body></soapenv:Envelope>');

		} else if (req.getEndpoint() == 'ManageCustomerCommentsError') {

			res.setBody('<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Header/><soapenv:Body><managecusoc:AcknowledgeCustomerOrderComment xmlns:managecusoc="http://soa.johnlewispartnership.com/service/es/Transaction/CustomerOrder/CustomerOrderComment/ManageCustomerOrderComment/v1" xmlns:cmnEBM="http://soa.johnlewispartnership.com/schema/ebm/CommonEBM/v1" xmlns:managecusocEBM="http://soa.johnlewispartnership.com/schema/ebm/Transaction/CustomerOrder/ManageCustomerOrderComment/v1" xmlns:cmn="http://soa.johnlewispartnership.com/schema/ebo/Common/v1" xmlns:txn="http://soa.johnlewispartnership.com/schema/ebo/Transaction/v1" xmlns:wsa="http://www.w3.org/2005/08/addressing"><cmnEBM:ApplicationArea sentDateTime="2014-07-04T09:30:48.657+01:00"><cmnEBM:Sender><cmnEBM:LogicalID schemeID="ManageCustomerOrderComment-Create" schemeAgencyID="JL"/></cmnEBM:Sender><wsa:MessageID>7dc08a34-9b67-4836-8774-d44d8660fe91</wsa:MessageID><cmnEBM:TrackingID>5c0b9aef-6083-4c5a-bfea-044ff24293fc</cmnEBM:TrackingID></cmnEBM:ApplicationArea><managecusocEBM:DataArea><managecusocEBM:Acknowledge responseCode="Error" hasActionStatusRecords="false"><cmnEBM:OriginalApplicationArea sentDateTime="2014-07-04T09:30:47.321+01:00"><cmnEBM:Channel><cmnEBM:ChannelID schemeID="OCIP" schemeAgencyID="JL"/><cmnEBM:ChannelName>OCIP</cmnEBM:ChannelName><cmnEBM:ChannelType>filler</cmnEBM:ChannelType></cmnEBM:Channel><cmnEBM:Sender><cmnEBM:LogicalID schemeID="CaseManagement" schemeAgencyID="JL"/></cmnEBM:Sender><wsa:MessageID>27b59bb0-f721-43f4-8c91-782cbfa10710</wsa:MessageID><cmnEBM:TrackingID>5c0b9aef-6083-4c5a-bfea-044ff24293fc</cmnEBM:TrackingID></cmnEBM:OriginalApplicationArea><cmnEBM:ServiceError errorCode="INTERNAL_ERROR" errorType="Technical" errorDescription="Internal error. Please contact JL Integration Operations team and provide the trackingID: 5c0b9aef-6083-4c5a-bfea-044ff24293fc"/></managecusocEBM:Acknowledge></managecusocEBM:DataArea></managecusoc:AcknowledgeCustomerOrderComment></soapenv:Body></soapenv:Envelope>');

		}

		return res;
	}

}