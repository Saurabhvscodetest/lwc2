/* Description  : GDPR Test class for Delete Loyalty Account records
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   25/1/2020        Ramesh Vasudevan                   Created                 COPT-5232
*
*/
@isTest
public class BAT_DelNeverActivatedLoyaltyAccountTest {
 @isTest
 public static void testLoyaltyAccount() {
        contact c = new contact();
        c.FirstName = 'Ramesh';
        c.LastName='Vasudevan';
        insert c;
        list<Loyalty_Account__c> loyaltylist = new list<Loyalty_Account__c>();
        for(integer counter =0; counter<300 ; counter++){
            Loyalty_Account__c la = new Loyalty_Account__c();
            la.Name = 'test'+counter;
            la.Contact__c = c.id;
            la.IsActive__c = false;
            loyaltylist.add(la);
        }
        try{
            insert loyaltylist;
            system.assertEquals(300, loyaltylist.size());
        }
        Catch(exception e){
            system.debug('Exception Caught:'+e.getmessage());
        }
        Database.BatchableContext BC;
        BAT_DelNeverActivatedLoyaltyAccount obj = NEW BAT_DelNeverActivatedLoyaltyAccount();
        Test.startTest();
        Database.DeleteResult[] Delete_Result = Database.delete(loyaltylist, false);
        obj.start(BC);
        obj.execute(BC,loyaltylist); 
        obj.finish(BC);
     	
        Test.stopTest();
    }
    
     @isTest
    Public static void testDmlException_Delete()
    {
        contact c = new contact();
        c.FirstName = 'Ramesh';
        c.LastName='Vasudevan';
        insert c;
        list<Loyalty_Account__c> conprofdata = new list<Loyalty_Account__c>();
        for(integer counter =0; counter<300 ; counter++){
            Loyalty_Account__c cp = new Loyalty_Account__c();
            cp.Name = 'test'+counter;
            cp.Contact__c = c.id;
            conprofdata.add(cp);
        }
        try{
            insert conprofdata;
            system.assertEquals(300, conprofdata.size());
        }
        Catch(exception e){
            system.debug('Exception Caught:'+e.getmessage());
        }
        Database.DeleteResult[] Delete_Result = Database.delete(conprofdata, false);
        DmlException expectedException;
        Test.startTest();
        try { 
            delete conprofdata;
        }
        catch (DmlException dException) {
            expectedException = dException; 
            system.assertNotEquals(NULL, expectedException);
        }
        Test.stopTest();
    }
    
    @isTest
    Public static void testSchedule(){
        Test.startTest();
        ScheduleBatchDelNALoyaltyAccount  obj = NEW ScheduleBatchDelNALoyaltyAccount();
        obj.execute(null);  
        Test.stopTest();
    }
}