/* 
Holds Information on case creation from EventHub into Connex.       
*/
global with sharing class EventHubVO {
    /* Request Object for Raise Exception */
    global class EHExceptionRequest{
        /* Id of the Exception.*/
        webservice String ehExceptionId;
        /* Exception Name of the Exception. */
        webservice String ehExceptionName;
        /* Exception Code. */
        webservice String ehExceptionCode;
        /* Delivery Type against which this exception was raised */
        webservice String deliveryType;
        /* customerName Name*/
        webservice String customerName;
        /* Branch Name*/
        webservice String enrichmentBranchName;
        /* Branch Number*/
        webservice String enrichmentBranchNumber;
        /* GVF Delivery Information */
        webservice EHGVfInfo gvfInfo;
        /* ClickAndCollect Order Information */
        webservice EHClickAndCollectInfo clickAndCollectInfo; 
        /* Supplier Direct Order Information */
        webservice EHSupplierDirectInfo supplierDirectInfo; 
        /*Supplied Contact Information*/
        webservice EHContactInfo contactInfo;
    }
    
    /* GVF information */
    global class EHGvfInfo{
        /* Order Id information */
        webservice String orderId;    
        /* Delivery Id of the delivery */           
        webservice String deliveryId;
        /* Source information */
        webservice boolean isOnline;
        /* CDH information of the delivery */           
        webservice String deliveryCdh; 
        /* Status of the delivery */            
        webservice String deliveryStatus;
        /*Delivery Slot from date of the delivery */
        webservice dateTime deliverySlotFrom;
        /*Delivery Slot end date of the delivery */
        webservice dateTime deliverySlotTo;
        /* Package Number on the package within the delivery against which the exception is being raised*/
        webservice dateTime deliveryPlannedDepartureTime;
        /*Planned Departure time */
        webservice dateTime deliveryPlannedArrivalTime;
        /*Planned Arrival time */
        webservice String packageNo;
        /* Package Status on the package within the delivery where the exception is being raised*/
        webservice String packageStatus;
        /* Place holder field to pass any extra information to Connex*/
        webservice String extraInformation;
        /* Further Information on package*/
        webservice String furtherInformation;
        /* Current location of the delivery where the exception is being raised*/
        webservice String currentLocation;
        /* Photo evidences of the delivery*/
        webservice List<EHPhotoEvidences> photoEvidences;       
    }
    
    /* Click and Collect Information */
    global class EHClickAndCollectInfo{
        /* Order Id information */
        webservice String orderId;
        /* Status of the order */
        webservice String orderStatus;
        /* modeOfTransport information either JL or Waitrose */
        webservice String modeOfTransport;
        /* Expected Location on the order */            
        webservice String expectedLocation;
        /* Package Number on the package within the delivery against which the exception is being raised*/
        webservice String packageNo;
        /* Package Status on the package within the delivery where the exception is being raised */
        webservice String packageStatus;
        /* Current location of the delivery where the exception is being raised*/
        webservice String currentLocation;
        /* Further Information on package*/
        webservice String furtherInformation;
        /* Place holder field to pass any extra information to Connex*/
        webservice String extraInformation;
    }
    
    /* Supplier Direct Order Information */
    global class EHSupplierDirectInfo{
        /* Order Id information */
        webservice String orderId;
        /* Source information */
        webservice boolean isOnline;
        /* Branch Name*/
        webservice String branchName;
        /* Supplier Name which the exception is being raised*/
        webservice String supplierName;
        /*Promised Delivery Datetime of the delivery */
        webservice Datetime promisedDeliveryDate;
        /* Status of the order Line Item */
        webservice String orderLineStatus;
        /* Product Code*/
        webservice String productCode;
        /* Further Information on package*/
        webservice String furtherInformation;
        /* Place holder field to pass any extra information to Connex*/
        webservice String extraInformation;
        
        webservice String purchaseOrderNo;
        webservice Datetime customerOrderedDate;
        webservice String narrative;
        webservice String productDescription;
        webservice Datetime expectedDeliveryDate;
        webservice String fulfilmentType;
        webservice String orderLineStatusReason;
        webservice String otherRemarks;
        webservice String supplierRemark1;
        webservice String supplierRemark2;
        webservice String supplierRemark3;
        webservice String supplierExpectation;
        webservice DateTime statusAsOfDate;
        webservice DateTime stockDueInDate;
        webservice String timesExpDelChg;
    }
    
    /* Photo evidence Information */
    global class EHPhotoEvidences{
        /* Evidence type may be Photo or signature */
        webservice String evidenceType;
        /* URL of the  Photo or signature evidence*/
        webservice String evidenceUrl; 
    }
    
    /*Supplied Contact Information*/
    global class EHContactInfo{
        /* Customer shopperId */
        webservice String shopperId;
        /* Customer Salutation */
        webservice String salutation;
        /* Customer Firstname */
        webservice String firstName;
        /* Customer Lastname */
        webservice String lastName;
        /* Customer Email */
        webservice String customerEmail;
        /* Customer Postcode */
        webservice String customerPostCode;
        /* Customer Home Phone */
        webservice String customerPhoneHome;
        /* Customer Other Phone */
        webservice String customerPhoneOther;
        /* Customer Mobile Phone */
        webservice String customerPhoneMobile;
        /* Customer Street */
        webservice String customerMailingStreet;
        /* Customer City */
        webservice String customerMailingCity;
        /* Customer State */
        webservice String customerMailingState;
        /* Customer Country */
        webservice String customerMailingCountry;
    }
    
    /* Response Information on raise Exception*/
    global class EHExceptionResponse{
        /* Exception ID */
        webservice String exceptionId;
        /* Status contains either "Success" or "Error" */
        webservice String status;
        /* Id of the created task for the Exception */
        webservice String taskId;
        /* Error Code populated on error Condition */
        webservice String errorCode;
        /* Error Description populated on error Condition */
        webservice String errorDescription;   
        /* Id of the created case for the Exception */
        webservice String caseId;
    }
    
}