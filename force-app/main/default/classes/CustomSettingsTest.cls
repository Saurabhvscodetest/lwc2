/**
 *	@author: Andrey Gavrikov (westbrook)
 *	@date: 2015-01-22
 *	@description:
 *	    Unit tests for CustomSettings class logic
 *	
 *	Version History :   
 *		
 */
@isTest
public with sharing class CustomSettingsTest {
	
	/**
	 * check that under Test context the Hierarchy custom setting value is not taken from the actual custom
	 * settings when Test value is explicitly provided in Test.isRunningTest context
	 * However, if no test value is provided the result must be taken from the actual custom setting
	 */
	static testMethod void testHierarchySettings () {

		Test.startTest();
		System.runAs(MyJL_TestUtil.getTestUser('testHierarchySettings')) {
			MyJL_Settings__c setting = new MyJL_Settings__c();
			setting.SetupOwnerId = UserInfo.getUserId();
			setting.Run_MyJL_Triggers__c = false;
			Database.insert(setting);

			System.assertEquals(false, CustomSettings.getMyJLSetting().getBoolean('Run_MyJL_Triggers__c'), 
					'Expected to get value we have just set directly to Custom Setting');

			// now check that when running unit test and Test custom setting value is not
			// provided then custom setting value is taken from the actual custom settings  
			CustomSettings.setTestMyJLSetting(UserInfo.getUserId(), new CustomSettings.TestRecord(new Map<String, Object>{'Run_MyJL_Triggers__c' => true}));
			System.assertEquals(true, CustomSettings.getMyJLSetting().getBoolean('Run_MyJL_Triggers__c'), 
					'Expected to get "test override" value we have just set');
		}
		Test.stopTest();
	}
	
	/**
	 * check that under Test context the List custom setting value is not taken from the actual custom
	 * settings when Test value is explicitly provided in Test.isRunningTest context
	 * However, if no test value is provided the result must be taken from the actual custom setting
	 */
	static testMethod void testListSettings () {

		Test.startTest();
		System.runAs(MyJL_TestUtil.getTestUser('testListSettings')) {
			System.assert(CustomSettings.getCalloutSettings().isEmpty(), 'Did not expect to get anything because settings have not been initialised');
			
			Callout_Settings__c setting1 = new Callout_Settings__c();
			setting1.name = 'testListSettings1';
			setting1.Class_Name__c = 'testListSettings1Class';
			setting1.Timeout_Milliseconds__c = 10000;
			setting1.Number_of_Retries__c = 10;
			setting1.Endpoint__c = 'http://localhost';
			Callout_Settings__c setting2 = setting1.clone(false, true);
			setting2.name = 'testListSettings2';
			setting2.Class_Name__c = 'testListSettings2Class';

			Database.insert(new List<Callout_Settings__c>{setting1, setting2});

			System.assertEquals(2, CustomSettings.getCalloutSettings().size(), 'When real setting record exists expected to get non empty list');

			System.assertEquals('testListSettings2Class', CustomSettings.getCalloutSetting('testListSettings2').getString('Class_Name__c')
					, 'Expected to get value set in the real custom setting record');


			// now check that when running unit test and Test custom setting value is not
			// provided then custom setting value is taken from the actual custom settings  
			CustomSettings.TestRecord testRec = new CustomSettings.TestRecord();
			testRec.put('Class_Name__c', 'changedTestClassName');
			CustomSettings.setTestCalloutSetting('testListSettings2', testRec);
			System.assertEquals('changedTestClassName', CustomSettings.getCalloutSetting('testListSettings2').getString('Class_Name__c'), 
					'Expected to get "test override" value we have just set');
		}
		Test.stopTest();
	}
}