@IsTest
Public class DelTrackPostCodeCheckTest {
    
Public static List< DelTrackPostCodeCheck.DelTrackPostCodeRequest > chatbotInputs = new List< DelTrackPostCodeCheck.DelTrackPostCodeRequest > ();
Public static List< DelTrackPostCodeCheck.DelTrackPostCodeRequest > chatbotInputs1 = new List< DelTrackPostCodeCheck.DelTrackPostCodeRequest > ();
Public static List< DelTrackPostCodeCheck.DelTrackPostCodeRequest > chatbotInputs2 = new List< DelTrackPostCodeCheck.DelTrackPostCodeRequest > ();
Public static List< DelTrackPostCodeCheck.DelTrackPostCodeRequest > chatbotInputs3 = new List< DelTrackPostCodeCheck.DelTrackPostCodeRequest > ();
Public static List< DelTrackPostCodeCheck.DelTrackPostCodeRequest > chatbotInputs4 = new List< DelTrackPostCodeCheck.DelTrackPostCodeRequest > ();
Public static List< DelTrackPostCodeCheck.DelTrackPostCodeRequest > chatbotInputs5 = new List< DelTrackPostCodeCheck.DelTrackPostCodeRequest > ();
Public static List< DelTrackPostCodeCheck.DelTrackPostCodeRequest > chatbotInputs6 = new List< DelTrackPostCodeCheck.DelTrackPostCodeRequest > ();
   
@IsTest
Public static void testPostCodeCheckMethod(){
        
     DelTrackPostCodeCheck.DelTrackPostCodeRequest delPostCodeCheck = new DelTrackPostCodeCheck.DelTrackPostCodeRequest();
        
        delPostCodeCheck.CustomerEnteredPostCode = 'EF 9PY';
        delPostCodeCheck.EventHubPostCode = 'EF 9PY';
        chatbotInputs.add(delPostCodeCheck);
    
    
     DelTrackPostCodeCheck.DelTrackPostCodeRequest delPostCodeCheck1 = new DelTrackPostCodeCheck.DelTrackPostCodeRequest();
    
        delPostCodeCheck1.CustomerEnteredPostCode = 'EF9PY';
        delPostCodeCheck1.EventHubPostCode = 'EF 9PY';
        chatbotInputs1.add(delPostCodeCheck1);
    
    
    DelTrackPostCodeCheck.DelTrackPostCodeRequest delPostCodeCheck2 = new DelTrackPostCodeCheck.DelTrackPostCodeRequest();
    
        delPostCodeCheck2.CustomerEnteredPostCode = 'EF 9PY';
        delPostCodeCheck2.EventHubPostCode = 'EF9PY';
        chatbotInputs2.add(delPostCodeCheck2);
    
      DelTrackPostCodeCheck.DelTrackPostCodeRequest delPostCodeCheck3 = new DelTrackPostCodeCheck.DelTrackPostCodeRequest();
    
        delPostCodeCheck3.CustomerEnteredPostCode = 'EF9PY';
        delPostCodeCheck3.EventHubPostCode = 'EF9PY';
        chatbotInputs3.add(delPostCodeCheck3);
    
    
    DelTrackPostCodeCheck.DelTrackPostCodeRequest delPostCodeCheck4 = new DelTrackPostCodeCheck.DelTrackPostCodeRequest();
    
        delPostCodeCheck4.CustomerEnteredPostCode = 'EF9PY';
        delPostCodeCheck4.EventHubPostCode = 'EF9PY';
        chatbotInputs4.add(delPostCodeCheck4);
    
    
    DelTrackPostCodeCheck.DelTrackPostCodeRequest delPostCodeCheck5 = new DelTrackPostCodeCheck.DelTrackPostCodeRequest();
    
        delPostCodeCheck5.CustomerEnteredPostCode = 'E99PY';
        delPostCodeCheck5.EventHubPostCode = 'EF9PY';
        chatbotInputs5.add(delPostCodeCheck5);
    
    DelTrackPostCodeCheck.DelTrackPostCodeRequest delPostCodeCheck6 = new DelTrackPostCodeCheck.DelTrackPostCodeRequest();
    
        delPostCodeCheck6.CustomerEnteredPostCode = '';
        delPostCodeCheck6.EventHubPostCode = '';
        chatbotInputs6.add(delPostCodeCheck6);
    
		Test.startTest();
		DelTrackPostCodeCheck.DelTrackChatBotProcess(chatbotInputs);     
		DelTrackPostCodeCheck.DelTrackChatBotProcess(chatbotInputs1);    
    	DelTrackPostCodeCheck.DelTrackChatBotProcess(chatbotInputs2); 
    	DelTrackPostCodeCheck.DelTrackChatBotProcess(chatbotInputs3); 
    	DelTrackPostCodeCheck.DelTrackChatBotProcess(chatbotInputs4); 
        DelTrackPostCodeCheck.DelTrackChatBotProcess(chatbotInputs5); 
    	DelTrackPostCodeCheck.DelTrackChatBotProcess(chatbotInputs6); 
		Test.stopTest();      
        
    }
    
    
}