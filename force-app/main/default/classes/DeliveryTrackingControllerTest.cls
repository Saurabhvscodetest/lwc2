@isTest
global class DeliveryTrackingControllerTest{
    public static String ORDER_ID = '333';
    public static List<Constants__c> constCustSettingsVal;
    
     /* load the custom settings */  
     static {
        try{
            constCustSettingsVal = Test.loadData(Constants__c.sObjectType, 'Constants');
            system.debug(constCustSettingsVal.size());
        }catch(Exception e){
          System.debug('Exception'+e.getMessage());
        }
    }
    
     @isTest static void testGVFrequestWithDeliveryID() {
        setUp();
        string DELIVERY_ID_INPUT = ORDER_ID + DeliveryTrackingVOMock.INPUT_GVF;
        Test.setMock(HttpCalloutMock.class, new DeliveryTrackingVOMock());
        DeliveryTrackingController  controller = new DeliveryTrackingController();
        controller.deliveryId = DELIVERY_ID_INPUT ;
        Test.startTest();
        controller.searchDelivery();
        DelTrackChatBotUtils.chatBotFetchDeliveryDetails(DELIVERY_ID_INPUT,DELIVERY_ID_INPUT);
        Test.stopTest();
       
    }
     
    @isTest static void testCCrequestWithOrderID() {
        setUp();
        string ORDER_ID_INPUT = ORDER_ID + DeliveryTrackingVOMock.INPUT_WR_CC;
        Test.setMock(HttpCalloutMock.class, new DeliveryTrackingVOMock());
        DeliveryTrackingController  controller = new DeliveryTrackingController();
        controller.orderId = ORDER_ID_INPUT ;
        
        Test.startTest();
        DelTrackChatBotUtils.chatBotFetchDeliveryDetails(ORDER_ID_INPUT,ORDER_ID_INPUT);
        controller.searchDelivery();
        Test.stopTest();
     
    }
    
    @isTest static void testCarrierRequestWithOrderID() {
        setUp();
        string ORDER_ID_INPUT = ORDER_ID + DeliveryTrackingVOMock.INPUT_CARRIER;
        Test.setMock(HttpCalloutMock.class, new DeliveryTrackingVOMock());
        DeliveryTrackingController  controller = new DeliveryTrackingController();
        controller.orderId = ORDER_ID_INPUT ;
        Test.startTest();
        DelTrackChatBotUtils.chatBotFetchDeliveryDetails(ORDER_ID_INPUT,ORDER_ID_INPUT);
        controller.searchDelivery();
        Test.stopTest();
        
    }
    
     
    @isTest static void testRequestWithOrderID() {
        setUp();
        string ORDER_ID_INPUT = ORDER_ID + DeliveryTrackingVOMock.INPUT_SD_BRANCH;
        Test.setMock(HttpCalloutMock.class, new DeliveryTrackingVOMock());
        DeliveryTrackingController  controller = new DeliveryTrackingController();
        controller.orderId = ORDER_ID_INPUT ;
        Test.startTest();
        DelTrackChatBotUtils.chatBotFetchDeliveryDetails(ORDER_ID_INPUT,ORDER_ID_INPUT);
        controller.searchDelivery();
        Test.stopTest();
           }
    
     @isTest static void testReturnsTrackingWithOrderID() {
        setUp();
        string ORDER_ID_INPUT = ORDER_ID + DeliveryTrackingVOMock.INPUT_WR_CC;
        Test.setMock(HttpCalloutMock.class, new DeliveryTrackingVOMock());
        DeliveryTrackingController  controller = new DeliveryTrackingController();
        controller.orderId = ORDER_ID_INPUT ;
        Test.startTest();
         DelTrackChatBotUtils.chatBotFetchDeliveryDetails(ORDER_ID_INPUT,ORDER_ID_INPUT);
        controller.searchReturns();
        Test.stopTest();
        
    }
    
     @isTest static void testRequestWithTechnicalError() {
        setUp();
        string ORDER_ID_INPUT = ORDER_ID + DeliveryTrackingVOMock.INPUT_CONNECTION_ERROR;
        Test.setMock(HttpCalloutMock.class, new DeliveryTrackingVOMock());
        
        DeliveryTrackingController  controller = new DeliveryTrackingController();
        controller.orderId = ORDER_ID_INPUT;
        
        Test.startTest();
         DelTrackChatBotUtils.chatBotFetchDeliveryDetails(ORDER_ID_INPUT,ORDER_ID_INPUT);
        controller.searchDelivery();
        Test.stopTest();
        
        validateResetDetails(controller);
    }
    
    @isTest static void testRequestWithBusinessError() {
        setUp();
        string ORDER_ID_INPUT = ORDER_ID + DeliveryTrackingVOMock.INPUT_INVALID_DEATILS;
        Test.setMock(HttpCalloutMock.class, new DeliveryTrackingVOMock());
        
        DeliveryTrackingController  controller = new DeliveryTrackingController();
        controller.orderId = ORDER_ID_INPUT;
        
        Test.startTest();
        DelTrackChatBotUtils.chatBotFetchDeliveryDetails(ORDER_ID_INPUT,ORDER_ID_INPUT);
        controller.searchDelivery();
        Test.stopTest();
        
       
        validateResetDetails(controller);
    }

    @isTest static void testRequestWithBusinessErrorForReturns() {
        setUp();
        string ORDER_ID_INPUT = ORDER_ID + DeliveryTrackingVOMock.INPUT_INVALID_DEATILS;
        Test.setMock(HttpCalloutMock.class, new DeliveryTrackingVOMock());
        
        DeliveryTrackingController  controller = new DeliveryTrackingController();
        controller.orderId = ORDER_ID_INPUT;
        DelTrackChatBotUtils.chatBotFetchDeliveryDetails(ORDER_ID_INPUT,ORDER_ID_INPUT);
        Test.startTest();
        controller.searchReturns();
        Test.stopTest();
        
        validateResetDetails(controller);
    }

    @isTest static void testShouldPopulateValidationErrors_WhenBothParamsAreNull() {
        DeliveryTrackingController  controller = new DeliveryTrackingController();
        controller.orderId = null ;
        controller.deliveryId = null;
        Test.startTest();
        controller.searchDelivery();
        DelTrackChatBotUtils.chatBotFetchDeliveryDetails('','');
        Test.stopTest();
      
        validateResetDetails(controller);
    }
    
    @isTest static void testShouldPopulateValidationErrors_WhenBothParamsAreProvided() {
        DeliveryTrackingController  controller = new DeliveryTrackingController();
        controller.orderId = '123';
        controller.deliveryId = '123';
        Test.startTest();
        controller.searchDelivery();
        Test.stopTest();
       
        validateResetDetails(controller);
    }
    
    @isTest static void testReturnsTracking_WhenBothParamsAreNull() {
        DeliveryTrackingController  controller = new DeliveryTrackingController();
        controller.orderId = null ;
        controller.trackingId = null;
        Test.startTest();
        controller.searchReturns();
        Test.stopTest();
       
        validateResetDetails(controller);
    }
    
    @isTest static void testReturnsTracking_WhenBothParamsAreProvided() {
        DeliveryTrackingController  controller = new DeliveryTrackingController();
        controller.orderId = '123';
        controller.trackingId = '123';
        Test.startTest();
        controller.searchReturns();
        Test.stopTest();
        
        validateResetDetails(controller);
    }
    
    @isTest static void testClearAllData() {
        DeliveryTrackingController  controller = new DeliveryTrackingController();
        Test.startTest();
        controller.clearAllData();
        Test.stopTest();

        validateResetDetails(controller);
    }
    
    @isTest static void testMustCheckTheFullfillmentTypeValues() {
        DeliveryTrackingController controller = new DeliveryTrackingController();
        
    }
    
    @isTest static void testCheckDateTimeFormat() {
        Datetime dt = DateTime.now();
        FormatDateTime formatTimeInstance = new FormatDateTime();
        formatTimeInstance.dateTimeValue = dt;
        String dateTimeString = dt.format(EHConstants.DATE_TIME_FORMAT);
        String dateString = dt.format(EHConstants.DATE_FORMAT);
       
        //If the datetime is null
        formatTimeInstance.dateTimeValue = null;
        
    }
    
    private static void validateResetDetails(DeliveryTrackingController controller){
        
        Integer DEFAULT_INDEX = 0; 
        
        
    }
    static void setUp() {
        //Insert Callout Setting
        Callout_Settings__c calloutSettings = new Callout_Settings__c();
        calloutSettings.name = EHConstants.EVENT_HUB_GET_EVENTS;
        calloutSettings.Class_Name__c = EHGetEventsService.class.getName();
        calloutSettings.Timeout_Milliseconds__c = 3000;
        calloutSettings.Endpoint__c = 'http://localhost'; 
        insert calloutSettings;
        
        Config_Settings__c defaultNotificationEmail = 
        new Config_Settings__c(Name = 'EMAIL_NOTIFICATION_DEFAULT_ADDRESS', Text_Value__c = 'test@test.co.uk');
        insert defaultNotificationEmail;
    }
    
    
}