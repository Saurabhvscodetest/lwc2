public class CaseCloseButtonController {

	private ApexPages.StandardController standardController;
	private Id recordId;
	private Case caseRecord;

	public CaseCloseButtonController(ApexPages.StandardController standardController) {
		this.standardController = standardController;
	}

	public PageReference validate() {

		recordId = standardController.getId();

		caseRecord = [SELECT Id, JL_NumberOutstandingTasks__c, Origin FROM Case WHERE Id = :recordId];

		PageReference caseClosePage = new PageReference('/'+recordId+'/s'+'?retURL=%2F' + recordId + '&cas7=Closed%20-%20Resolved');

		if(caseRecord.JL_NumberOutstandingTasks__c == 0 || (caseRecord.JL_NumberOutstandingTasks__c <= 1 && caseRecord.Origin == 'Email')) {
			return caseClosePage;
		} else {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'This case has 1 or more open activities. Please review and close all open activities prior to closing this case.'));
			return null;
		}

	}

}