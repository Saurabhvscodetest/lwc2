/*------------------------------------------------------------
Author:         Jan-Willem Brokamp
Company:        Salesforce.com
Description:    Helper methods to store data in the Log_Header and Log_Detail objects 
    
History
<Date>      <Authors Name>     <Brief Description of Change>
2014-06-25  JWB                Initial Release

Edit	Date		Who			Comment
001		21/05/15	NTJ			Make logging @future
002		04/06/15	NTJ			Truncate the KD Get Log as the response data (and its XML) could
								be too big for the record
003		22/06/15	NTJ			If Log Header Update fails then probably because of disk space, so improve error message
004		24/06/15	NTJ			Add a Log Header type for Leave Deactivation type
------------------------------------------------------------*/
public with sharing class LogUtil {
    public class LogException extends Exception {}
    
	// 002 the size of the Response_Data__c LONG TEXT AREA
	private static final Integer MAX_LOGGED_RESPONSE_SIZE = 131072;

    public enum SERVICE {
        WS_GET_CUSTOMER,
        WS_CREATE_CUSTOMER,
        WS_UPDATE_CUSTOMER,
        WS_JOIN_CUSTOMER,
        WS_LEAVE_CUSTOMER,
        OUTBOUND_JOIN_LEAVE_NOTIFY,
        WS_KD_GET_CUSTOMER,
        WS_KD_SEARCH_TRANSACTIONS,
        WS_KD_CREATE_TRANSACTIONS,
		OUTBOUND_CONTACT_PROFILE_UPDATE_NOTIFY,
		LEAVE_DEACTIVATE_MYJL
    }
    
    /* Return Salesforce header ID which is to be used when logging the details */
    public static ID startLogHeader(final SERVICE service, final SFDCMyJLCustomerTypes.RequestHeaderType requestHeader, final SFDCMyJLCustomerTypes.Customer[] customers) {
        Id logHeaderId = null;
        
        //Is header logging enabled?
        if(true == CustomSettings.getLogHeaderConfig().getBoolean('Enabled__c')) {
        	// do it now
        	logHeaderId = startLogHeaderNow(service, requestHeader, customers);
        } else {
        	// do in future        	
        	String shopperId = getFirstShopperId(customers);
        	Decimal customerCount = getCustomerCount(customers);
        	String messageId;
        	String header;
        	
        	if (requestHeader != null) {
        		messageId = requestHeader.MessageId;
        		header = requestHeader.toString();
        	}
        	//system.debug('messageId: '+messageId);
        	//system.debug('header: '+header);
        	//system.debug('service.name(): '+service.name());
        	//system.debug('shopperId: '+shopperId);
        	//system.debug('customerCount: '+customerCount);
        	startLogHeaderFuture(messageId, header, service.name(), shopperId, customerCount);
        } 
        return  logHeaderId; 	
    }

    @future
    public static void startLogHeaderREST(final String messageId, final String requestData, final String service, final String shopperId, final Decimal customerCount) {
        Log_Header__c logHeader = new Log_Header__c();
        try {
            
            
            logHeader.Service__c = service;
            logHeader.Message_ID__c = messageId;
            logHeader.Shopper_ID__c = shopperId;
            logHeader.Customer_Count__c = customerCount;
            logHeader.Request_Data__c = requestData;
            insert logHeader;
            
        } catch (Exception ex) {
            system.debug('problem writing log header');     
        }
        system.debug('inside logheader');
        //return logHeader.ID;
    }
    
    @future
    public static void startLogHeaderFuture(final String messageId, final String requestData, final String service, final String shopperId, final Decimal customerCount) {
    	try {
            Log_Header__c logHeader = new Log_Header__c();
            
            logHeader.Service__c = service;
            logHeader.Message_ID__c = messageId;
            logHeader.Shopper_ID__c = shopperId;
            logHeader.Customer_Count__c = customerCount;
            logHeader.Request_Data__c = requestData;
            insert logHeader;
       	} catch (Exception ex) {
        	system.debug('problem writing log header');   	
       	}
    }
    
    private static ID startLogHeaderNow(final SERVICE service, final SFDCMyJLCustomerTypes.RequestHeaderType requestHeader, final SFDCMyJLCustomerTypes.Customer[] customers) {
        //system.debug('startLogHeader start [service: ' + service + ', requestHeader: ' + requestHeader + ']');
        
        //Is header logging enabled?
        if(true == CustomSettings.getLogHeaderConfig().getBoolean('Enabled__c')) {
            //system.debug('startLogHeader enabled');
            
            try {
                Log_Header__c logHeader = new Log_Header__c();
                
                logHeader.Service__c = service.name();
                if(requestHeader != null) {
                    logHeader.Message_ID__c = requestHeader.MessageId;
                    logHeader.Request_Data__c = requestHeader.toString();
                    logHeader.Shopper_Id__c = getFirstShopperId(customers);
                    logHeader.Customer_Count__c = getCustomerCount(customers);
                }
                //Insert                
                insert logHeader;
                
                //system.debug('startLogHeader return [ID: ' + logHeader.ID + ']');
                
                return logHeader.ID;
            } catch(Exception e) {
                System.debug('startLogHeader ERROR: ' + e);
                throw new LogException(e);
            }
        } 
        
        return null;
    }
    
    /* Update header log record, indicated by logHeaderID, with the response and optional errorStackTrace. This method will also 
    calculate the processingtime (difference between this call and the related startLogHeader) and store this */
	public static void updateLogHeader(final ID logHeaderID, final SFDCMyJLCustomerTypes.ResponseHeaderType responseHeader, final String errorStackTrace) {
        //system.debug('updateLogHeader start [logHeaderID: ' + logHeaderID + ', responseHeader: ' + responseHeader + ', errorStackTrace: ' + errorStackTrace + ']');
        
        //Is header logging enabled?
        if(true == CustomSettings.getLogHeaderConfig().getBoolean('Enabled__c')) {
            //system.debug('updateLogHeader enabled');
            
            if(logHeaderId == null || String.isBlank(logHeaderId)) {
                throw new LogException('Log Header ID cannot be empty. If this is a sandbox, could be caused by lack of disk space');
            } 
            
            //Retrieve existing record
            Log_Header__c logHeader = [SELECT Log_Header__c.ID, Log_Header__c.LastModifiedDate FROM Log_Header__c WHERE ID = :logHeaderId];
            
            //system.debug('updateLogHeader existing logHeader: [' + logHeader + ']');
            
            if(logHeader == null) {
                system.debug('no existing Log Header entry found for ID [' + logHeaderId + ']');
                throw new LogException('no existing Log Header entry found for ID [' + logHeaderId + ']');
            }
            
            //Update succes, errorCode & full response payload
            if(responseHeader != null && responseHeader.ResultStatus != null) {
                logHeader.Success__c = responseHeader.ResultStatus.Success;
                logHeader.Error_Code__c = responseHeader.ResultStatus.Code;
                logHeader.Response_Data__c = responseHeader.toString();
            }
            
            //Update detailed error message if available
            logHeader.Error_Detail__c = errorStackTrace;
            
            //Calculate processing time and store
            Long processingStopInMillis = System.currentTimeMillis();
            Long processingStartInMillis = logHeader.LastModifiedDate.getTime();
            
            Long processingTime = processingStopInMillis - processingStartInMillis;
            logHeader.Processing_Time__c = processingTime;
            
            //system.debug('updateLogHeader Success__c: ' + logHeader.Success__c);
            //system.debug('updateLogHeader Error_Code__c: ' + logHeader.Error_Code__c);
            //system.debug('updateLogHeader Error_Detail__c: ' + logHeader.Error_Detail__c);
            //system.debug('updateLogHeader Response_Data__c: ' + logHeader.Response_Data__c);
            //system.debug('updateLogHeader Processing_Time__c: ' + logHeader.Processing_Time__c);
            
            //Update record
            try {
                update logHeader;
            } catch(Exception e) {
                System.debug('updateLogHeader ERROR: ' + e);
                throw new LogException(e);
            }
        } 
    }
    
    /* Create detail log records */    
    public static void logDetails(final ID logHeaderID, final List<MyJL_Util.CustomerWrapper> customerWrappers) {
        //system.debug('logDetails start [logHeaderID: ' + logHeaderID + ', customerWrappers: ' + customerWrappers + ']');
        
        //Is detail logging enabled?
        if(true == CustomSettings.getLogDetailConfig().getBoolean('Enabled__c')) {
            //system.debug('logDetails enabled');
            
            if(logHeaderId == null || String.isBlank(logHeaderId)) {
                throw new LogException('Log Header ID cannot be empty');
            } 
            
            List<Log_Detail__c> logDetails = new List<Log_Detail__c>();
            for(MyJL_Util.CustomerWrapper customerWrapper : customerWrappers) {
                //system.debug('logDetails customerWrapper: [' + customerWrapper + ']');
                
                Log_Detail__c logDetail = new Log_Detail__c();
                logDetail.Log_Header__c = logHeaderID;
                
                //Update succes & errorCode
                logDetail.Success__c = customerWrapper.getCustomerResult().Success;
                logDetail.Error_Code__c = customerWrapper.getCustomerResult().Code;
            
                //Update detailed error message if available
                logDetail.Error_Detail__c = customerWrapper.getStackTrace();
            
                //Update full request & response
                logDetail.Request_Data__c = customerWrapper.getCustomerInput().toString();
                logDetail.Response_Data__c = customerWrapper.getCustomerResult().toString();
                
                //system.debug('logDetails Success__c: ' + logDetail.Success__c);
                //system.debug('logDetails Error_Code__c: ' + logDetail.Error_Code__c);
                //system.debug('logDetails Error_Detail__c: ' + logDetail.Error_Detail__c);
                //system.debug('logDetails Request_Data__c: ' + logDetail.Request_Data__c);
                //system.debug('logDetails Response_Data__c: ' + logDetail.Response_Data__c);
                
                logDetails.add(logDetail);
            }
            
            try {
                Database.Saveresult[] saveResults = Database.Insert(logDetails, false);
                
                for(Database.Saveresult saveResult : saveResults) {
                    if(saveResult.success) {
                        //TODO - checks/logs?
                    }
                }
                
            } catch(Exception e) {
                System.debug('logDetails ERROR: ' + e);
                throw new LogException(e);
            }
        } 
    }
    
    public static ID startKDLogHeader(final SERVICE service, final SFDCKitchenDrawerTypes.RequestHeader requestHeader, 
    								  final SFDCKitchenDrawerTypes.TransactionSearchCriteria searchCriteria, String[] transIds) {
        Id logHeaderId = null;
        
        //Is header logging enabled?
        if(true == CustomSettings.getLogHeaderConfig().getBoolean('Enabled__c')) {
        	// do it now
        	logHeaderId = startKDLogHeaderNow(service, requestHeader, searchCriteria, transIds);
        } else {
        	// do in future
        	String shopperId;
        	Decimal customerCount = 1;
        	String transactionId;
        	String cardNumber;
        	
        	if (searchCriteria != null) {
        		if (String.isNotBlank(searchCriteria.customerId)) {
	        		shopperId = searchCriteria.customerId;        			
        		} else {
	        		cardNumber = searchCriteria.cardNumber;        			        			
        		}
        	} else if (transIds != null) {
        		if (transIds.size() > 0) {
        			transactionId = transIds[0];
        		}
        	}	       	
        	startKDLogHeaderFuture(requestHeader.MessageId, requestHeader.toString(), service.name(), shopperId, customerCount, transactionId, cardNumber);
        } 
        return  logHeaderId; 	
    }
    
    @future
    public static void startKDLogHeaderFuture(final String messageId, final String requestData, final String service, final String shopperId, final Decimal customerCount,
    									    final String transactionId, final String cardNumber) {
    	try {
            Log_Header__c logHeader = new Log_Header__c();
            
            logHeader.Service__c = service;
            logHeader.Message_ID__c = messageId;
            logHeader.Shopper_ID__c = shopperId;
            logHeader.Customer_Count__c = customerCount;
            logHeader.Request_Data__c = requestData;
            logHeader.Card_Number__c = cardNumber;
            logHeader.Transaction_Id__c = transactionId;
	        
            insert logHeader;
       	} catch (Exception ex) {
        	system.debug('problem writing log header');   	
       	}
    }
    
    //Log method for KD project
    public static ID startKDLogHeaderNow(final SERVICE service, final SFDCKitchenDrawerTypes.RequestHeader requestHeader, 
    									final SFDCKitchenDrawerTypes.TransactionSearchCriteria searchCriteria, String[] transIds) {
    
    //system.debug('startLogHeader start [service: ' + service + ', requestHeader: ' + requestHeader + ']');
        
        //Is header logging enabled?
		if(true == CustomSettings.getLogHeaderConfig().getBoolean('Enabled__c')) {
            //system.debug('startLogHeader enabled');
            
            try {
                Log_Header__c logHeader = new Log_Header__c();
                
                logHeader.Service__c = service.name();
                if(requestHeader != null) {
                    logHeader.Message_ID__c = requestHeader.MessageId;
                    logHeader.Request_Data__c = requestHeader.toString();
		        	String shopperId;
		        	Decimal customerCount = 1;
		        	String transactionId;
		        	String cardNumber;
		        	
		        	if (searchCriteria != null) {
		        		if (String.isNotBlank(searchCriteria.customerId)) {
			        		shopperId = searchCriteria.customerId;        			
		        		} else {
			        		cardNumber = searchCriteria.cardNumber;        			        			
		        		}
		        	} else if (transIds != null) {
		        		if (transIds.size() > 0) {
		        			transactionId = transIds[0];
		        		}
		        	}	       	
                 }
                //Insert                
                insert logHeader;
                
                //system.debug('startKDLogHeader return [ID: ' + logHeader.ID + ']');
                
                return logHeader.ID;
            } catch(Exception e) {
                System.debug('startKDLogHeader ERROR: ' + e);
                throw new LogException(e);
            }
        } 
        
        return null;
    }
    
    public static void updateKDLogHeader(final ID logHeaderID, final SFDCKitchenDrawerTypes.ActionResponse responseHeader, final String errorStackTrace) {
        //system.debug('updateLogHeader start [logHeaderID: ' + logHeaderID + ', responseHeader: ' + responseHeader + ', errorStackTrace: ' + errorStackTrace + ']');
        
        //Is header logging enabled?
		if(true == CustomSettings.getLogHeaderConfig().getBoolean('Enabled__c')) {
            //system.debug('updateLogHeader enabled');
            
            if(logHeaderId == null || String.isBlank(logHeaderId)) {
                throw new LogException('Log Header ID cannot be empty');
            } 
            
            //Retrieve existing record
            Log_Header__c logHeader = [SELECT Log_Header__c.ID, Log_Header__c.LastModifiedDate FROM Log_Header__c WHERE ID = :logHeaderId];
            
            //system.debug('updateLogHeader existing logHeader: [' + logHeader + ']');
            
            if(logHeader == null) {
                throw new LogException('no existing Log Header entry found for ID [' + logHeaderId + ']');
            }
            
            //Update succes, errorCode & full response payload
            if(responseHeader != null ) {
                logHeader.Success__c = responseHeader.success;
                //logHeader.Success__c = true;
                
                ////system.debug('AMIT success value : '+logHeader.Success__c);
                logHeader.Error_Code__c = responseHeader.message;
                // 002 - abbreviate to size of LONG TEXT AREA in case the response is really large (which it could be if it is the full XML)
                logHeader.Response_Data__c = responseHeader.toString().abbreviate(MAX_LOGGED_RESPONSE_SIZE-1);
            }
            
            //Update detailed error message if available
            logHeader.Error_Detail__c = errorStackTrace;
            
            //Calculate processing time and store
            Long processingStopInMillis = System.currentTimeMillis();
            Long processingStartInMillis = logHeader.LastModifiedDate.getTime();
            
            Long processingTime = processingStopInMillis - processingStartInMillis;
            logHeader.Processing_Time__c = processingTime;
            
            //system.debug('updateKDLogHeader Success__c: ' + logHeader.Success__c);
            //system.debug('updateKDLogHeader Error_Code__c: ' + logHeader.Error_Code__c);
            //system.debug('updateKDLogHeader Error_Detail__c: ' + logHeader.Error_Detail__c);
            //system.debug('updateKDLogHeader Response_Data__c: ' + logHeader.Response_Data__c);
            //system.debug('updateKDLogHeader Processing_Time__c: ' + logHeader.Processing_Time__c);
            
            //Update record
            try {
                update logHeader;
            } catch(Exception e) {
                System.debug('updateLogHeader ERROR: ' + e);
                throw new LogException(e);
            }
        } 
    }
    
     public static void logKDDetails(final ID logHeaderID, final List<MyJL_Util.TransactionWrapper> transactionWrappers) {
        //system.debug('logDetails start [logHeaderID: ' + logHeaderID + ', customerWrappers: ' + transactionWrappers + ']');
        
        //Is detail logging enabled?
		if(true == CustomSettings.getLogDetailConfig().getBoolean('Enabled__c')) {
            //system.debug('logDetails enabled');
            
            if(logHeaderId == null || String.isBlank(logHeaderId)) {
                throw new LogException('Log Header ID cannot be empty');
            } 
            
            List<Log_Detail__c> logDetails = new List<Log_Detail__c>();
            for(MyJL_Util.TransactionWrapper transactionWrapper : transactionWrappers) {
                //system.debug('logDetails transactionWrapper: [' + transactionWrapper + ']');
                
                Log_Detail__c logDetail = new Log_Detail__c();
                logDetail.Log_Header__c = logHeaderID;
                
                //Update succes & errorCode
                logDetail.Success__c = transactionWrapper.getActionResult().success;
                logDetail.Error_Code__c = transactionWrapper.getActionResult().code;
            
                //Update detailed error message if available
                logDetail.Error_Detail__c = transactionWrapper.getStackTrace();
            
                //Update full request & response
                logDetail.Request_Data__c = transactionWrapper.getKDTransactionInput().toString();
                logDetail.Response_Data__c = transactionWrapper.getActionResult().toString();
                
                //system.debug('logDetails Success__c: ' + logDetail.Success__c);
                //system.debug('logDetails Error_Code__c: ' + logDetail.Error_Code__c);
                //system.debug('logDetails Error_Detail__c: ' + logDetail.Error_Detail__c);
                //system.debug('logDetails Request_Data__c: ' + logDetail.Request_Data__c);
                //system.debug('logDetails Response_Data__c: ' + logDetail.Response_Data__c);
                
                logDetails.add(logDetail);
            }
            
            try {
                Database.insert(logDetails, false);
            } catch(Exception e) {
                System.debug('logDetails ERROR: ' + e);
            }
        } 
    }
    
    // Most calls include just one customer, it is useful to log
    private static String getFirstShopperId(final SFDCMyJLCustomerTypes.Customer[] customers) {
    	String shopperId = null;
    	
    	if (customers != null) {
			for (SFDCMyJLCustomerTypes.Customer customer:customers) {
	    		shopperId = myJL_Util.getCustomerId(customer);
	    		break;
	    	}    		
	    }
    	    	
    	return shopperId;
    }
    
    // How many customers are in the call?
    private static Decimal getCustomerCount(final SFDCMyJLCustomerTypes.Customer[] customers) {    	
    	return (customers != null) ? (Decimal) customers.size() : 0;
    }
    
    public static Integer headerLimit() {
    	Integer i = (true == CustomSettings.getLogHeaderConfig().getBoolean('Enabled__c')) ? 1 : 0;  
    	system.debug('limit: '+i);
    	return i;  	
    }
}