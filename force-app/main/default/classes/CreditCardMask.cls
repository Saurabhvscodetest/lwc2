/*
** Code inspired by http://developer.force.com/cookbook/recipe/text-masking-using-regex
**
**
		American Express: 3711-078176-03214  |  371107817603214  |  3711 078176 03214
		Visa: 4321-5321-6321-7321  |  4321532163217321  |  4321 5321 6321 7321
		Master Card: 5321-4321-6321-7321  |  5321432163217321  |  5321 4321 6321 7321
		Discover: 6011-0009-9013-9424  |  6500000000000002  |  6011 0009 9013 9424	

	Are there other cards?

*/

public with sharing class CreditCardMask {
	
	static public String maskCC(String inString) {
		List<String> listPatterns = new List<String>{'((?:(?:4\\d{3})|(?:5[1-5]\\d{2})|6(?:011|5[0-9]{2}))(?:-?|\\040?)(?:\\d{4}(?:-?|\\040?)){3}|(?:3[4,7]\\d{2})(?:-?|\\040?)\\d{6}(?:-?|\\040?)\\d{5})'};		
		return maskString(inString, listPatterns, '*', 4);
	}

	static private String maskString(String inString, List<string> inPatterns, String inMask, Integer inVisibleCharacters) {
		system.debug('maskString: in: '+inString);
    	// validate the passed in variables
      	if (inString == null || instring.length() < 1 ||
        	inPatterns == null || inPatterns.size() < 1 ||
          	inMask == null) return inString;
       
       // now do the work
       if (inVisibleCharacters < 0) {
       		inVisibleCharacters = 0;
       }

       // prime the internal variables to be used during processing
       String stringToMask = inString;
       String maskedString = inString;
        
       // iterate through each pattern and mask any matches leaving the last visible characters
       for(string regEx : inPatterns) {
           Pattern p = Pattern.compile(regEx);
           Matcher m = p.matcher(stringToMask);
           while(m.find()) {
               // find the start and end indexes of the match
               Integer startIdx = m.start();
               Integer endIdx = m.end();

               // extract the matched string
               String patternMatch = stringToMask.substring(startIdx, endIdx);                   

               // mask the string leaving any visible characters
               String partToBeMasked = patternMatch.substring(0, patternMatch.length() - inVisibleCharacters);                                                            

               String mask = '';                                         

               for(integer i = 0; i < partToBeMasked.length(); i++) {
                   mask += inMask;
               }   
               // concatenate mask string with the last visible characters              
               String maskedNumber = mask + patternMatch.substring(patternMatch.length() - inVisibleCharacters);                  
                
               // replace the the card number with masked number
               maskedString = maskedString.replace(patternMatch, maskedNumber);
           }              
       }
       system.debug('maskString: out: '+maskedString);
             
       return maskedString;    
	}
}