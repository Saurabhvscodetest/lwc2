public class DatetimeUtilities {

	public static Integer getMinutesBetween (Datetime earlierDatetime, Datetime laterDatetime) {

		//convert to unix time
		Long earlierTimeUnix 				= earlierDatetime.getTime();
		Long laterTimeUnix   				= laterDatetime.getTime();
		Long amountOfMillisecondsBetween	= laterTimeUnix - earlierTimeUnix;
		Long amountOfSecondsBetween 		= amountOfMillisecondsBetween / 1000;
		Long amountOfMinutesBetween 		= amountOfSecondsBetween / 60;

		return Integer.valueOf(amountOfMinutesBetween);

	}

}