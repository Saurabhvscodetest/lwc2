Public class MyJLChatBotInvocableClass{
	
    public class MyJLChatBotRequest {
	   @InvocableVariable(required= true)
	   public String loyaltyMembershipNumber;
	   @InvocableVariable(required= true)
       public String shopperID;
   }
    
    @Invocablemethod(Label ='Invoke Process Case Method' description='Invoke Process Case Method')
    Public static List<String> myJLChatBotInvokeProcess(List<MyJLChatBotRequest> chatbotInputReqList){
        List<String> Output = new List<String>();
        List<String> chatbotInputs = new List<String>();
         for( MyJLChatBotRequest chatbotReq : chatbotInputReqList ){
         	chatbotInputs.add ( chatbotReq.loyaltyMembershipNumber );
         	chatbotInputs.add ( chatbotReq.shopperID );
         }
        
        String jsonResponse =  MyJLChatBotUtils.myJLChatBotProcessInput(chatbotInputs[0],chatbotInputs[1]);
        
        Output.add(jsonResponse);
        
        return Output;
    }
    
}