/* Description  : GDPR - Decouple Accounts from Contacts
*
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   28/03/2019      Vijay Ambati                        Created             COPT-4432
*
*/

global class BAT_DecoupleAccountsFromContacts implements Database.Batchable<sObject>,Database.Stateful{
    
	global Map<Id,String> ErrorMap = new Map<Id,String>();
    global integer result=0;
    global String query;
    
    global Database.QueryLocator start(Database.BatchableContext BC){
    GDPR_Record_Limit__c gDPRLimit = GDPR_Record_Limit__c.getValues('BAT_DecoupleAccountsFromContacts');
    if(gDPRLimit.Record_Limit__c != NULL){
	String recordLimit = gDPRLimit.Record_Limit__c;
    String limitSpace = ' ' + 'Limit' + ' '; 
	query = 'SELECT Id from Contact where AccountId != NULL order by createddate asc' + limitSpace + recordLimit;
        }
        else{
            query = 'SELECT Id from Contact where AccountId != NULL order by createddate asc';
        }
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, list<Contact> scope){
			
         try{ 
             for(Contact con:scope){
                 con.AccountId = NULL;
                 con.Skip_Batch_Validation__c = True;
             }
             list<Database.SaveResult> srList = Database.Update(scope);
             if(srList.size()>0){
        	 for(Integer counter = 0; counter < srList.size(); counter++) {
                    if (srList[counter].isSuccess()) {
                        result++;
                    }else {
                        for(Database.Error err : srList[counter].getErrors()) {
                            ErrorMap.put(srList.get(counter).id,err.getMessage());
                        }
                    }   
                }
            }
         }
        Catch(exception e){
                EmailNotifier.sendNotificationEmail('Exception from BAT_DecoupleAccountsFromContacts', e.getMessage());
            }
    }
    global void finish(Database.BatchableContext BC) {
        String textBody ='';
        set<Id> allIds = new set<Id>();
        textBody+= result +' records Decoupled from Accounts to Contacts'+'\n';
        if (!ErrorMap.isEmpty()){
            for (Id recordids : ErrorMap.KeySet())
            {
             allIds.add(recordids);
            } 
             textBody+= 'Error log: '+allIds+'\n';
        }
   EmailNotifier.sendNotificationEmail('GDPR Decouple Accounts From Contacts', textBody);
   BAT_DecoupleAccountsFromCases objNextBatch = new BAT_DecoupleAccountsFromCases();
   Database.executeBatch(objNextBatch);
}
}