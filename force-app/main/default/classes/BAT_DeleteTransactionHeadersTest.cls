/* Description : Delete Trasaction Header Records
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0  29/03/2019        Vijay Ambati                      Created              COPT-4296
* 1.1  22/04/2019        Vignesh Kotha                     Modified             COPT-4578
*/

@IsTest
Public class BAT_DeleteTransactionHeadersTest { 
    static testmethod void testTransactionDelete(){         
        List<Loyalty_Account__c> Loyaltydata = new List<Loyalty_Account__c>();
        for(Integer counter =0; counter<10 ; counter++){
            Loyalty_Account__c Lobject = new Loyalty_Account__c();
            Lobject.Name = 'test'+counter;
            Lobject.IsActive__c= false;
            Lobject.Activation_Date_Time__c = NULL;
            Lobject.createddate = System.now().addYears(-6);
            Lobject.Deactivation_Date_Time__c = System.now().addYears(-3);
            Lobject.Source_CreatedDate__c=System.now().addYears(-3); 
            Lobject.Contact__c = NULL;
            Loyaltydata.add(Lobject);    
        }
        insert Loyaltydata;
        list<Loyalty_Card__c> card = new list<Loyalty_Card__c>();
        for(Loyalty_Account__c Loy: Loyaltydata){
            Loyalty_Card__c lCard = new Loyalty_Card__c();
            lCard.Card_Number__c = '123' + Loy.Id;
            lCard.Loyalty_Account__c = Loy.Id;
            card.add(lCard);
        }
          insert card;       
       
        
        List<Transaction_Header__c> tHead = new List<Transaction_Header__c>();
        for(Loyalty_Card__c LCA : card){
            Transaction_Header__c tH = new Transaction_Header__c();
            tH.Receipt_Barcode_Number__c = LCA.id ;
            tH.Card_Number__c = LCA.id;
            tH.Transaction_Type__c = 'Sale';
            tH.IsValid__c = true;
            tHead.add(tH);    
        }
        insert tHead;
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.GDPR_Class_Name__c = 'BAT_DeleteTransactionHeaders';
        gDPR.Name = 'BAT_DeleteTransactionHeaders';
        gDPR.Record_Limit__c = '5000'; 
        
        GDPR_Record_Limit__c gDPR1 = new GDPR_Record_Limit__c();
        gDPR1.GDPR_Class_Name__c = 'BAT_DeleteBulkLoyaltyAccount';
        gDPR1.Name = 'BAT_DeleteBulkLoyaltyAccount';
        gDPR1.Record_Limit__c = '5000'; 
        try{
           Insert gDPR;
            Insert gDPR1;
        }
        Catch(Exception e)
        {
            System.debug('Exception Caught:'+e.getmessage());
        }
        Database.BatchableContext BC;
        BAT_DeleteTransactionHeaders obj = new BAT_DeleteTransactionHeaders();
        obj.start(BC);
        obj.execute(BC,Loyaltydata); 
        obj.finish(BC);
        /* insert MYJL records */
         List<Loyalty_Account__c> Loyaltydata1 = new List<Loyalty_Account__c>();
        for(Integer counter =0; counter<20 ; counter++){
            Loyalty_Account__c Lobject = new Loyalty_Account__c();
            Lobject.Name = 'test'+counter;
            Lobject.IsActive__c= false;
            Lobject.Activation_Date_Time__c = NULL;
            Lobject.createddate = System.now().addYears(-6);
            Lobject.Deactivation_Date_Time__c = System.now().addYears(-3);
            Lobject.Source_CreatedDate__c=System.now().addYears(-3); 
            Lobject.Contact__c = NULL;
            Loyaltydata1.add(Lobject);    
        }
        insert Loyaltydata1;
        GDPR_Record_Limit__c gDPR3 = new GDPR_Record_Limit__c();
        gDPR3.GDPR_Class_Name__c = 'BAT_DeleteBulkLoyaltyAccount';
        gDPR3.Name = 'BAT_DeleteBulkLoyaltyAccount';
        gDPR3.Record_Limit__c = '5000'; 
        try{
          Insert gDPR3;
        }
        Catch(Exception e)
        {
            System.debug('Exception Caught:'+e.getmessage());
        }
    } 
    
    @isTest
    Public static void testSchedule(){
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.GDPR_Class_Name__c = 'BAT_DeleteTransactionHeaders';
        gDPR.Name = 'BAT_DeleteTransactionHeaders';
        gDPR.Record_Limit__c = '5000'; 
        GDPR_Record_Limit__c gDPR1 = new GDPR_Record_Limit__c();
        gDPR1.GDPR_Class_Name__c = 'BAT_DeleteBulkLoyaltyAccount';
        gDPR1.Name = 'BAT_DeleteBulkLoyaltyAccount';
        gDPR1.Record_Limit__c = '5000'; 
        Test.startTest();
        Insert gDPR1;
        Insert gDPR;
        ScheduleBatchdeleteTransactionHeader obj = NEW ScheduleBatchdeleteTransactionHeader();
        obj.execute(null);  
        Test.stopTest();
    }  
     static testmethod void testTransactionDelete1(){
        List<Loyalty_Account__c> Loyaltydata = new List<Loyalty_Account__c>();
        for(Integer counter =0; counter<300 ; counter++){
             Loyalty_Account__c Lobject = new Loyalty_Account__c();
            Lobject.Name = 'test'+counter;
            Lobject.IsActive__c= true;
            Lobject.Activation_Date_Time__c = NULL;
            Lobject.createddate = System.now().addYears(-1);
            Lobject.Deactivation_Date_Time__c = System.now().addYears(-1);
            Lobject.Source_CreatedDate__c=System.now().addYears(-1); 
            Lobject.Contact__c = NULL;   
            Loyaltydata.add(Lobject);
        }
        insert Loyaltydata;        
        Loyalty_Card__c lCard = new Loyalty_Card__c();
        lCard.Card_Number__c = '9623683951828338';
        lCard.Loyalty_Account__c = Loyaltydata[0].Id;
        Insert lCard;              
        List<Transaction_Header__c> tHead = new List<Transaction_Header__c>();
        for(Integer counter =0; counter<300 ; counter++){
            Transaction_Header__c tH = new Transaction_Header__c();
            tH.Receipt_Barcode_Number__c = '1234' + counter;
            tH.Card_Number__c = lCard.id;
            tH.Transaction_Type__c = 'Sale';
            tHead.add(tH);    
        }
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.GDPR_Class_Name__c = 'BAT_DeleteTransactionHeaders';
        gDPR.Name = 'BAT_DeleteTransactionHeaders';
        gDPR.Record_Limit__c = ''; 
        GDPR_Record_Limit__c gDPR1 = new GDPR_Record_Limit__c();
        gDPR1.GDPR_Class_Name__c = 'BAT_DeleteBulkLoyaltyAccount';
        gDPR1.Name = 'BAT_DeleteBulkLoyaltyAccount';
        gDPR1.Record_Limit__c = '5000'; 
        try{
            Insert tHead;
            Insert gDPR;
			Insert gDPR1;
        }
        Catch(Exception e)
        {
            System.debug('Exception Caught:'+e.getmessage());
        }
        Test.startTest();
        Database.BatchableContext BC;
        BAT_DeleteTransactionHeaders obj=new BAT_DeleteTransactionHeaders();
        Database.DeleteResult[] Delete_Result = Database.delete(tHead, false);
        obj.start(BC);
        obj.execute(BC,Loyaltydata);
        obj.finish(BC);
        DmlException expectedException;
        Test.stopTest();
        GDPR_Record_Limit__c gDPR2 = new GDPR_Record_Limit__c();
        gDPR2.GDPR_Class_Name__c = 'BAT_DeleteBulkLoyaltyAccount';
        gDPR2.Name = 'BAT_DeleteBulkLoyaltyAccount';
        gDPR2.Record_Limit__c = ''; 
        try{
            Insert gDPR2;
        }
        Catch(Exception e)
        {
            System.debug('Exception Caught:'+e.getmessage());
        }
    } 
    @IsTest
Public static void testDmlException_Delete()
    {
        contact c = new contact();
        c.FirstName = 'Vignesh';
        c.LastName='kotha';
        insert c;
        list<Loyalty_Account__c> conprofdata = new list<Loyalty_Account__c>();
        for(integer counter =0; counter<200 ; counter++){
            Loyalty_Account__c cp = new Loyalty_Account__c();
            cp.Name = 'test'+counter;
            cp.Contact__c = c.id;
            conprofdata.add(cp);
        }
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
 		gDPR.Name = 'BAT_DeleteTransactionHeaders';   
        gDPR.GDPR_Class_Name__c = 'BAT_DeleteTransactionHeaders';
        gDPR.Record_Limit__c = '100';
        try{
            insert conprofdata;
            insert gDPR;
            system.assertEquals(200, conprofdata.size());
        }
        Catch(exception e){
            system.debug('Exception Caught:'+e.getmessage());
        }
        Loyalty_Card__c lCard = new Loyalty_Card__c();
        lCard.Card_Number__c = '9623683951828338';
        lCard.Loyalty_Account__c = conprofdata[0].Id;
        Insert lCard;              
        List<Transaction_Header__c> tHead = new List<Transaction_Header__c>();
        for(Integer counter =0; counter<300 ; counter++){
            Transaction_Header__c tH = new Transaction_Header__c();
            tH.Receipt_Barcode_Number__c = '1234' + counter;
            tH.Card_Number__c = lCard.id;
            tH.Transaction_Type__c = 'Sale';
            tHead.add(tH);    
        }
        insert tHead;
        Database.DeleteResult[] Delete_Result = Database.delete(tHead, false);
        DmlException expectedException;
        Test.startTest();
        try { 
            delete conprofdata;
        }
        catch (DmlException dException) {
            expectedException = dException; 
            system.assertNotEquals(NULL, expectedException);
        }
        Test.stopTest();
    }    
}