/*
    @Author  : Yousuf Mohammad
    @Date    : 10-06-2016
    @Purpose : This Helper class provides methods to validate the Service Requests and create the case with details sent from the external system.
    It also has methods to fetch the data from the Custom meta data for the relevant Exception Type.
*/
public class ExceptionServiceHelper { 
    @TestVisible private static final Map<String,List<QueryExceptionType__mdt>> EXCEPTION_CONFIG = buildExceptionConfig();
    @TestVisible private static final Map<String,ServiceConstants__mdt> ORDER_EXCEPTIONS_MESSAGE = getOrderExceptionConstantsMap();
    @TestVisible private static final String STERLING_ORDER_START_NO = System.Label.STERLING_ORDER_START_NO;
    @TestVisible private static final String EXCEPTION_ID_NULL_ERROR_MESSAGE_KEY = 'EXCEPTION_ID_NULL_ERROR_MESSAGE';
    @TestVisible private static final String EXCEPTION_ID_INVALID_ERROR_MESSAGE_KEY = 'EXCEPTION_ID_INVALID_ERROR_MESSAGE';
    @TestVisible private static final String ORDER_ID_NULL_ERROR_MESSAGE_KEY = 'ORDER_ID_NULL_ERROR_MESSAGE';
    @TestVisible private static final String ORDER_ID_INVALID_ERROR_MESSAGE_KEY = 'ORDER_ID_INVALID_ERROR_MESSAGE';
    @TestVisible private static final String ORDER_EXCEPTION_SERVICE = 'ORDER EXCEPTION SERVICE';
    @TestVisible private static final String ERROR_PREFIX = 'Error : ';
    @TestVisible private static final String ERROR_SUBJECT_DURING_SAVING_CASES = 'PROBLEM WHILE SAVING CASES';
    
  public static Map<String,List<QueryExceptionType__mdt>> buildExceptionConfig(){
        Map<String,List<QueryExceptionType__mdt>> mapQueryExceptionConfig = new Map<String,List<QueryExceptionType__mdt>>();          
        List<QueryExceptionType__mdt> queryExceptionTypes = null;
        for(QueryExceptionType__mdt config :[SELECT Query_Type__c,Initiating_Source__c, Assign_Query_to_Queue__c,Require_Product_Information_Display__c, EXCEPTION_TYPE__C, Query_Description__c,  Fulfillment_Option_Description__c, QueryTypeDetail__c,  Label,  Id, ExceptionID__c,FulfillmentOptionId__c, 
                                                                 Further_Information__c,Contact_Reason__c, Reason_Detail__c,Exception_Description_New__c,  DeveloperName,  Case_Orgin__c,  Branch__c ,Populate_Sterling_Description__c
                                                                 From QueryExceptionType__mdt ]){
             if(mapQueryExceptionConfig.containsKey(config.ExceptionID__c)){
                 mapQueryExceptionConfig.get(config.ExceptionID__c).add ( config );
             } else {
                 mapQueryExceptionConfig.put(getUpperCase(config.ExceptionID__c), new List < QueryExceptionType__mdt > { config });
             }
        }
        return mapQueryExceptionConfig;
    }
    
    /*
    * This method is to validate the Case Service Class request to check if all mandatory values are sent by the external System.
    * 
    * @param  orderExceptionList       : list of case exception requests sent by external system
    * @return List<ServiceResponse>    : List of Error Responses. If there are no validation errors then the list will be empty with size 0.
    */
    public static void validateExceptions(List<ExceptionServiceVo> orderExceptionList )   {
        
        for(ExceptionServiceVo orderException : orderExceptionList ) {
            if(orderException.exceptionId ==null || orderException.exceptionId.length()==0) {
                addError(EXCEPTION_ID_NULL_ERROR_MESSAGE_KEY,orderException); 
                continue;
            }
            //check if the exception is supposed to be handled , we only contain the config info of all valid exceptions
            if(!EXCEPTION_CONFIG.containsKey(getUpperCase(orderException.exceptionId))) {
                addError(EXCEPTION_ID_INVALID_ERROR_MESSAGE_KEY,orderException); 
                continue;
            }
            if(orderException.orderNumber ==null || orderException.orderNumber.length()==0) {
                addError(ORDER_ID_NULL_ERROR_MESSAGE_KEY,orderException); 
                continue;
            }
            try{
                Long orderNo = Long.valueOf(orderException.orderNumber);
                Long sterlingOrderStartNo = Long.valueOf(STERLING_ORDER_START_NO);
                //If its not a valid STERLING ORDER 
                 if(!(orderNo > sterlingOrderStartNo)){
                    addError(ORDER_ID_INVALID_ERROR_MESSAGE_KEY,orderException); 
                continue;
            }
            } catch(Exception exp){
                system.debug('Exception ::: ' + exp);
                addError(ORDER_ID_INVALID_ERROR_MESSAGE_KEY,orderException); 
                continue;
            }
        }
    }
    
    
    
    /*
    * This method is to get all the valid requests sent by the external System.
    * @param  orderExceptionList  : list of case exception requests sent by external system 
    * @return List<ExceptionServiceVo> :  list of valid exception case requests.
    */
    public static List<ExceptionServiceVo> getValidServiceRequests(List<ExceptionServiceVo> orderExceptionList) {
        List<ExceptionServiceVo> validList = new List<ExceptionServiceVo>();
        for(ExceptionServiceVo orderException : orderExceptionList ) { 
            if(!((orderException.serviceResult != null) && (orderException.serviceResult.hasError))) {//has error
                validList.add(orderException);
            }
        }
        return validList;
    }
    
    
    /*
    * This method is to get all Shopper ids.
    * @param  orderExceptionList  : list of case exception requests sent by external system 
    * @return List<String> : List of Shopper ids.
    */
    public static  List<String> getShopperIdsFromRequest(List<ExceptionServiceVo> orderExceptionList)   {
        List<String> shopperIdList = new List<String>();
        for(ExceptionServiceVo orderException : orderExceptionList ) {
            if(orderException.customerId!=null) {
                shopperIdList.add(orderException.customerId);
            }
        }
        return shopperIdList;
    }
    
    /*
    * This method is to get the map where shopper id is key and respective contact as value.
    * @param  shopperIdList  : List of Shopper ids.
    * @return Map<String, ID> : map containing shopper id as key and contact as value.
    */
    public static Map<String, ID> getContactToShopperIdMap(List<ExceptionServiceVo> orderExceptionList) {
        List<String>          shopperIdList = getShopperIdsFromRequest(orderExceptionList); 
        Map<String, ID> contactToShopperMap = new Map<String, ID>();
        List<Contact>   contactList   = [SELECT id ,Shopper_Id__c FROM Contact WHERE Shopper_Id__c IN :shopperIdList];
        for(Contact cont : contactList )    
        {
            contactToShopperMap.put(cont.Shopper_Id__c,cont.id);
        }
        return contactToShopperMap;
    }
    
    /*
    * This method is to get Query Exception Config list, where exception id is key and related metadatatype as value
    * @param  exceptionIds  : list of case exception requests sent by external system 
    * @return List<QueryExceptionType__mdt> : the list of QueryException Metadata related to the input exception ID's
    */
    public static List<QueryExceptionType__mdt> getQueryExceptionConfigList(List<ExceptionServiceVo> orderExceptionList)  {
        Set<String> exceptionIds = new Set<String>();
        List<QueryExceptionType__mdt> queryExceptionTypes = new List<QueryExceptionType__mdt>();
        for(ExceptionServiceVo orderException : orderExceptionList ) {
            if(orderException != null && orderException.exceptionId!=null)  {
                String exceptionId = getUpperCase(orderException.exceptionId);
                if(String.isNotBlank(exceptionId) && !exceptionIds.contains(exceptionId)){
                    exceptionIds.add(exceptionId);
                    List<QueryExceptionType__mdt> qMdtList = EXCEPTION_CONFIG.get(getUpperCase(exceptionId));
                    if(qMdtList != null && (!qMdtList.isEmpty())){
                        queryExceptionTypes.addAll(qMdtList);
                    }
                }
            }
        }
        system.debug('exceptionIds' + exceptionIds);
        system.debug('exceptionConficMetadata' + queryExceptionTypes);
        return queryExceptionTypes;
    }
    
        
    private static Map<String, QueryExceptionType__mdt> getQueryExceptionMetaDataWithExceptionAndFullfilmentKey(List<QueryExceptionType__mdt> queryExceptionConfigList) {
        Map<String, QueryExceptionType__mdt> queryExceptionMetadaMap = new Map<String, QueryExceptionType__mdt>();
        for(QueryExceptionType__mdt mdt : queryExceptionConfigList) {
            String exceptionId    = getUpperCase(mdt.ExceptionID__c);
            String fullfillmentId = getUpperCase(mdt.FulfillmentOptionId__c);
            String key = exceptionId+fullfillmentId;
            queryExceptionMetadaMap.put(key, mdt);
        }
        return queryExceptionMetadaMap;
    }
    
    /*
    * This method is for fetching the details from CustomMetaData with reference to the Exception Type recieved.
    *
    * @param  ExceptionServiceVo         : value object holding case creation information
    * @param  queryExceptionConfigList   : the list of QueryException Metadata related to the input exception ID's
    * 
    */
    public Static void loadExceptionServiceVoFromCustomMetaData(ExceptionServiceVo orderException, List<QueryExceptionType__mdt> queryExceptionConfigList)                  {
        Map<String, QueryExceptionType__mdt> queryExceptionMap = getQueryExceptionMetaDataWithExceptionAndFullfilmentKey(queryExceptionConfigList);
        system.debug('queryExceptionMap :: ' + queryExceptionMap);
        system.debug('orderException :: ' + orderException);
        String  exceptionId       = getUpperCase(orderException.exceptionId);
        String  fullfillmentId    = getUpperCase(orderException.fulfillmentOptionId);
        String                               keyToSearch       = exceptionId+fullfillmentId;
        system.debug('keyToSearch :: ' + keyToSearch);
        system.debug('queryExceptionMap.get(keyToSearch) :: ' + queryExceptionMap.get(keyToSearch));
        
        QueryExceptionType__mdt queryExceptionMDT = queryExceptionMap.get(keyToSearch);
        if(queryExceptionMDT==null && String.isNotBlank(fullfillmentId)) {
            //if for some reason we get an invalid fulfillment option Id and unable to locate the custom metadata, we would default it to the metadata without fulfillment option id
            queryExceptionMDT = queryExceptionMap.get(exceptionId);
        if(queryExceptionMDT==null) {
            return;
            }
        }
        
        if(!queryExceptionMDT.Populate_Sterling_Description__c){            
            orderException.exceptionDescription= queryExceptionMDT.Exception_Description_New__c;
        }
        orderException.queryType = queryExceptionMDT.Query_Type__c;
        orderException.queryDescription= queryExceptionMDT.Exception_Description_New__c;
        orderException.queryTypeDetail= queryExceptionMDT.QueryTypeDetail__c;        
        orderException.caseOrigin=queryExceptionMDT.Case_Orgin__c;
        orderException.initiatingSource = queryExceptionMDT.Initiating_Source__c;//<YM002>
        orderException.branch=queryExceptionMDT.Branch__c; 
        orderException.exceptionType=queryExceptionMDT.EXCEPTION_TYPE__C; 
        orderException.assignQueryToQue=queryExceptionMDT.Assign_Query_to_Queue__c;
        orderException.contactReason =queryExceptionMDT.Contact_Reason__c;
        orderException.reasonDetail =queryExceptionMDT.Reason_Detail__c; 
        orderException.furtherInformation =queryExceptionMDT.Further_Information__C;
        orderException.fulfillmentOptionId  = queryExceptionMDT.Fulfillment_Option_Description__c;
        system.debug('queryExceptionMDT.Exception_Description_New__c' + queryExceptionMDT.Exception_Description_New__c); 
        
    }

    public static List<ServiceResponse> getServiceResponses(List<ExceptionServiceVo> orderExceptionList)  {
        List<ServiceResponse> responseList = new  List<ServiceResponse>();
        for(ExceptionServiceVo orderException : orderExceptionList ) { 
            ExceptionServiceVo.ServiceResult res= orderException.serviceResult;
            if(((res != null) && (res.hasError))) {//has error
                ExceptionServiceFailResponse failRes = new ExceptionServiceFailResponse(res.errorLabel, res.errorMessage);
                responseList.add(failRes);
            } else {
                ExceptionServiceSuccessResponse successRes = new ExceptionServiceSuccessResponse(res.caseID, res.caseNumber);
                responseList.add(successRes);
            }
        }
        return responseList;
    }
    
    public static void createCasesForValidExceptions(List<ExceptionServiceVo> orderExceptionList)  {
        Map<String, ID> shopperToContactMap = getContactToShopperIdMap(orderExceptionList);
        List<Case> caseList = new List<Case>();
        List<ExceptionServiceVo> validExceptionList = getValidServiceRequests(orderExceptionList);
        
        OrderDetailsWrapper orderObj = fetchOrderObjectDetails ( validExceptionList );
        
        for(ExceptionServiceVo requestVO : validExceptionList)   {
            caseList.add(getCaseWithContact(requestVO, requestVO.customerId==null ? null : shopperToContactMap.get(requestVO.customerId)));
        }
        Database.SaveResult[] saveResults;
        try {
             saveResults = Database.insert(caseList, false);
        } catch ( Exception ex ) {
            System.debug ( ' An exception has occured while inserting the case : ' + ex.getMessage ()) ;
        }
        Set<ID> successfulCases = new Set<ID>();
        for(Database.SaveResult result : saveResults )   {
            if(result.isSuccess())  {
                successfulCases.add(result.getId());
            }
        }
        Map<ID, Case> insertedCasesMap = new Map<ID, Case>([Select id,CaseNumber,createddate from Case where id in :successfulCases]);
        
        Integer index = 0;
        for(Database.SaveResult result : saveResults )   {
            ExceptionServiceVo requestVO = validExceptionList[index];
            if(result.isSuccess())  {
                ExceptionServiceVo.ServiceResult res = new  ExceptionServiceVo.Serviceresult();
                ID successId = result.getId();
                case latestCase = insertedCasesMap.get(successId);
                res.caseNumber = latestCase.CaseNumber; 
                res.caseID = successId;
                requestVO.serviceResult = res;
            } else {
                String errorDesc = ERROR_PREFIX;
                for(Database.Error err : result.getErrors()) {
                    errorDesc = errorDesc+'>>>>'+err.getStatusCode() + ': ' + err.getMessage();
                }
                addError(ERROR_SUBJECT_DURING_SAVING_CASES, errorDesc , requestVO);
        }
            index++;
            }
            
        List < Case > singleSupplierCaseList = new List < Case > ();
        singleSupplierCaseList = [ SELECT Id,jl_Branch_master__c, Supplier_Order_Number__c ,jl_Last_Queue_Name__c,jl_Queue_Name__c,singleSupplier__c,OwnerId, jl_OrderManagementNumber__c 
                                   FROM Case 
                                   WHERE singleSupplier__c = true
                                   AND ID IN : caseList];
        if ( singleSupplierCaseList.size () > 0 && orderObj.emailAddressFullList.size () > 0 ) {
            processSingleSupplierCases ( singleSupplierCaseList, orderObj );
        }    
        
        
    } 
    
    
    
    /*
    * This method is to implement the requirement COPT-5843
    * @param  singleSupplierCaseList  :     case record list where  singleSupplier__c = true
    * @param  
    * @return : void
    */
    
    
    public static OrderDetailsWrapper fetchOrderObjectDetails ( List<ExceptionServiceVo> exceptionList ) {
        List < String > orderNumberList = new List < String > ();
        List < String > validEmailAddressList = new List < String > ();
        
        OrderDetailsWrapper orderWrapper = new OrderDetailsWrapper ();
        
        for ( ExceptionServiceVo orderException : exceptionList) {
            try{
               if(!orderException.orderLineItems.isEmpty()){
                   if(orderException.orderLineItems[0].supplierOrders != NULL ){
                    if ( orderException.orderLineItems[0].supplierOrders[0].supplierNo != null ) {
                        orderNumberList.add ( orderException.orderLineItems[0].supplierOrders[0].supplierNo );
                    }
                }  
            } 
            }Catch(Exception e){
                System.debug('Exception with supplierOrders');
            }
        }
        
        List < String > orderContentList = new List < String > ();
        for ( ExceptionServiceVo orderException : exceptionList) {
            if ( orderException.orderLineItems != null ) {
                for ( ExceptionServiceVo.OrderLineItem orderLineItem : orderException.orderLineItems ) {
            
                    if(orderLineItem.supplierOrders != NULL ){
                        String emailContent = '<br>'+'OrderLineItem:'+ '\"'+ orderLineItem.orderLineItemId + '\"'+ '<br>' 
                                        +'ProductDescription:'+ '\"'+ orderLineItem.productDescription + '\"' +'<br>' 
                                        +'ProductCode:'+ '\"'+ orderLineItem.productCode + '\"'+ '<br>' 
                                        +'Quantity:'+ '\"'+  orderLineItem.quantity + '\"' +'<br>' 
                                        +'orderNo:'+ '\"'+ orderLineItem.supplierOrders[0].orderNo + '\"'+ '<br>';
                        orderContentList.add ( emailContent );
                    } 
                }
            }
            
        }
         
 

        
        
        
        Map < String, Supplier__c > supplierNumberToSupplierMap = new Map < String, Supplier__c > (); 

        for ( Supplier__c supplierRecord: [SELECT Company__c,Id,Name,Supplier_Email__c,Supplier_Name__c, Supplier_Number__c FROM Supplier__c]) {
            supplierNumberToSupplierMap.put ( supplierRecord.Supplier_Number__c, supplierRecord);
        }
        
        System.debug (' Supplier Number To Supplier Map ' + supplierNumberToSupplierMap);
        
        for ( String orderNumber : orderNumberList ) {
            if ( supplierNumberToSupplierMap.containsKey ( orderNumber) ) {
                if ( supplierNumberToSupplierMap.get( orderNumber ).Supplier_Email__c != null ) {
                    String emailAddress = supplierNumberToSupplierMap.get( orderNumber ).Supplier_Email__c;
                    validEmailAddressList.add ( emailAddress );
                }               
            }               
        }
        
        orderWrapper.emailAddressFullList = validEmailAddressList;
        orderWrapper.orderDetailsList = orderContentList;
        
        return orderWrapper;
        
    }
        
        
    
    
    
    /*
    * This method is to implement the requirement COPT-5843
    * @param  singleSupplierCaseList  :     case record list where  singleSupplier__c = true
    * @param  
    * @return : void
    */
    
    
    public static void processSingleSupplierCases ( List < Case > singleSupplierCaseList , OrderDetailsWrapper orderObject ) {
        EmailServiceUtils.sendEmailMethod ( singleSupplierCaseList, orderObject );
        closeCaseProcess ( singleSupplierCaseList );        
        
    }
    
    
    /*
    * This method is to implement the requirement COPT-5843, to close the cases with Single Supplier is true and
      have the email mentioned in the supplier table
    * @param  singleSupplierCaseList  :     case record list where  singleSupplier__c = true
    * @param  
    * @return : void
    */
    
    
    public static void closeCaseProcess ( List < Case > singleSupplierCaseList ) {
        
        List < Case > updateCaseList = new List < Case > ();
        for ( Case caseRecord : singleSupplierCaseList ) {
            caseRecord.status = 'Closed - Resolved';
            updateCaseList.add ( caseRecord );
        }
        
        try {
            
            Database.update( updateCaseList, true );
        } catch ( Exception Ex ) {
            System.debug ( ' Exception Occurred while closing the case ' + Ex.getMessage () );
        }
                
        List < Case_Activity__c > caseActivityUpdateList = new List < Case_Activity__c > (); 
                
        for(Case_Activity__c activityRecord : [select id, Customer_Promise_Type__c, Case__c from  Case_Activity__c where Case__c in :singleSupplierCaseList]){           
            activityRecord.Activity_Completed_Date_Time__c = system.now();            
            activityRecord.Resolution_Method__c = System.Label.OE_No_Action_Required;  
            activityRecord.Add_Comment__c       = System.Label.OE_Close_Activity_Comment;
            caseActivityUpdateList.add ( activityRecord );
            
        }   
        system.debug('closeselectedCaseActivities >>>>>>'+ caseActivityUpdateList.size());
        
        //update case activity to close in try catch block       
        
        if(caseActivityUpdateList.size() > 0) {
            try {
                CommonStaticUtils.runTrigger = false;
                 if(!Test.isRunningTest()) {
                     Database.update ( caseActivityUpdateList , true );
                 }
                
            } catch ( Exception Ex ) {
                System.debug (' Exception occurred during update activity ' + Ex.getMessage () );
              }
        }  
        
    }
    
    
    
    /*
    * This method is get the case with inserting given contact..
    * @param  orderException  :     case exception request sent by external system 
    * @param  contactId       : Contact record
    * @return Case : Case record attached to the given contact record.
    */
    private static Case getCaseWithContact(ExceptionServiceVo orderException, ID contactId) {
        Case newCase                    =   new Case();
        newCase.ContactId               =  orderException.IsGuestOrder != null && orderException.IsGuestOrder.equalsIgnoreCase('Y') ?null:contactId;
        newCase.Status                  =   'New';
        newCase.jl_OrderManagementNumber__c   =   orderException.orderNumber;
        newCase.FulfillmentOptionId__c  =   orderException.fulfillmentOptionId;
        
        if(!String.isBlank(orderException.furtherInformation)){
            newCase.Further_Information__c  =   orderException.furtherInformation;
        }
        
        //CASE0006
        if(orderException.exceptionId.containsIgnoreCase(System.Label.Case_Exception_FurthurInfo_From_Error_Description)){
            if(orderException.orderLineItems != null && orderException.orderLineItems.size() > 0){
                String furthurInfo = '';
                for(ExceptionServiceVo.OrderLineItem olItem: orderException.orderLineItems){
                    if(!String.isBlank(olItem.PORejectedErrorDescription) && !String.isBlank(olItem.productDescription)){
                        if(!String.isBlank(furthurInfo))
                            furthurInfo = furthurInfo +  '\n' +olItem.PORejectedErrorDescription + ' - '+ olItem.productDescription;
                        else
                         furthurInfo = olItem.PORejectedErrorDescription + ' - '+ olItem.productDescription;
                    }
                }
                
                if(!String.isBlank(furthurInfo)){
                      newCase.Further_Information__c = furthurInfo;             
                }
            }
        }
        //CASE0031
        if(orderException.exceptionId.containsIgnoreCase(System.Label.Case_Exception_FurthurInfo_From_Error_Code)){
                if(orderException.orderLineItems != null && orderException.orderLineItems.size() > 0){
                String furthurInfo = '';
                for(ExceptionServiceVo.OrderLineItem olItem: orderException.orderLineItems){
                    if(!String.isBlank(olItem.PORejectedErrorCode)){
                        if(!String.isBlank(furthurInfo))
                            furthurInfo = furthurInfo +  '\n' +olItem.PORejectedErrorCode.replace('_',' ');
                        else
                         furthurInfo = olItem.PORejectedErrorCode.replace('_',' ');
                    }
                }
                
                if(!String.isBlank(furthurInfo)){
                      newCase.Further_Information__c = furthurInfo;             
                }
            }
        }
        
        
        if(!String.isBlank(orderException.giftCardNumber)){         
            newCase.Further_Information__c = newCase.Further_Information__c + orderException.giftCardNumber;
        }
        
        //CASE0029
        if(orderException.exceptionId.containsIgnoreCase(System.Label.Case_Exception_Paypal_Payment_Failure_Id)){
            
                newCase.Further_Information__c = System.Label.Case_Exception_Payment_Reference +orderException.paymentReference+ '\n' + 
                            System.Label.Case_Exception_Transaction_Id +orderException.transactionId;       
        }  
        
      //CASE0009
        if(orderException.exceptionId.containsIgnoreCase(System.Label.Case_Exception_Id_Paypal_Failure)){
            
            if(!String.isBlank(orderException.paymentType)  && orderException.paymentType.containsIgnoreCase(System.Label.Case_Exception_Payment_Type)){
                newCase.Further_Information__c = newCase.Further_Information__c + orderException.paymentType ; 
            }else{
                newCase.Further_Information__c = newCase.Further_Information__c + orderException.paymentType + '\n' + 
                            System.Label.Case_Exception_Payment_Reference +orderException.paymentReference+ '\n' + 
                            System.Label.Case_Exception_Transaction_Id +orderException.transactionId;
            }               
        }  
        //CASE0070
        if(!String.isBlank(orderException.paymentType)  && !String.isBlank(orderException.smartPayRefundAmount)){
            newCase.Further_Information__c = newCase.Further_Information__c + orderException.paymentType + '\n' + System.Label.Case_Exception_Payment_Info +orderException.smartPayRefundAmount;
        }
       
        ExceptionServiceVo.OrderLineItem latestOrderLineItem = getLatestOrderLineItems(orderException);
        newCase.GiftCardNumber__c = orderException.giftCardNumber;        
        newCase.PaymentType__c          =   orderException.paymentType;        
        if(!String.isBlank(orderException.paymentReference)){
            newCase.PaymentReference__c     =   orderException.paymentReference;
        }    
        if(!String.isBlank(orderException.transactionId)){
             newCase.TransactionId__c  =  orderException.transactionId;
        }    
        if(latestOrderLineItem != null){
            System.debug('@@@@@@@@@@@@@@ latestOrderLineItem ' + latestOrderLineItem);     
            newCase.jl_DeliveryNumber__c    =   latestOrderLineItem.deliveryNumber;
            newCase.jl_Delivery_Date__c     =   latestOrderLineItem.deliveryDate;
            newCase.jl_Delivery_Method__c     =     latestOrderLineItem.fulfilmentOptionName;
            // Changes for COPT-5834
            if ( String.isNotBlank(latestOrderLineItem.orderReleaseLeadTime) ) {
                newCase.Order_Release_Lead_Time__c   =     decimal.valueOf(latestOrderLineItem.orderReleaseLeadTime);
            }
            
            // End Changes for COPT-5834
        }
        if(orderException.orderLineItems != null && !orderException.orderLineItems.isEmpty()){
            newCase.Collection_Date_Returns__c    = orderException.orderLineItems[0].collectionDate;
        }
        newCase.exceptionDescription__c =   orderException.exceptionDescription;
        newCase.ExceptionId__c          =   orderException.exceptionId; 
        newCase.ExceptionType__c        =   orderException.exceptionType;
        newCase.Description             =   orderException.exceptionDescription;
        newCase.Origin                  =   orderException.caseOrigin; 
        newCase.jl_Branch_master__c     =   orderException.branch;
        newCase.jl_InitiatingSource__c  =   orderException.initiatingSource;
        newCase.RecordTypeId            =   Schema.SObjectType.Case.getRecordTypeInfosByName().get('Order Exception').getRecordTypeId();
        newCase.Contact_Reason__c       =   orderException.contactReason;
        newCase.Reason_Detail__c        =   orderException.reasonDetail;       
        newCase.SmartPay_Refund_Amount__c = orderException.smartPayRefundAmount != null ? Decimal.valueOf(orderException.smartPayRefundAmount) : null;   
        newCase.Bypass_Standard_Assignment_Rules__c  = true;
        newCase.CDH_Site__c  = getCDHLocation(orderException);
        
         try{
           if(!orderException.orderLineItems.isEmpty()){
           if(orderException.orderLineItems[0].supplierOrders != NULL ){
            newCase.Supplier_Name__c =   orderException.orderLineItems[0].supplierOrders[0].supplierName;
            newCase.Supplier_Order_Number__c =   orderException.orderLineItems[0].supplierOrders[0].orderNo;
            newCase.jl_OtherSystemRef__c =   orderException.orderLineItems[0].supplierOrders[0].orderNo;
            newCase.Supplier_Order_Line_Number__c = orderException.orderLineItems[0].supplierOrders[0].orderLineNo;
        }  
        } 
        }Catch(Exception e){
            System.debug('Exception with supplierOrders');
        }
        
        
        // Changes for COPT-5843 
        if ( String.isNotBlank(orderException.singleSupplier) && orderException.singleSupplier.equalsIgnoreCase('true') ) {
            newCase.singleSupplier__c  =   true;
        } 
        
        
        //End Changes for COPT-5843 
        return newCase;
    }
    
    private static String getCDHLocation(ExceptionServiceVo orderException){
        String cdhLocation = null;
        if(orderException.orderLineItems != null && !orderException.orderLineItems.isEmpty()){
            //always take the first occurance of the location for CDH location ID
            String locationId = orderException.orderLineItems[0].fullLocationID;
            system.debug('locationId ' + locationId);
            if(String.isNotBlank(locationId)) {
                cdhLocation = EHLocationMapper.getLocation(locationId);
            }
            system.debug('cdhLocation ' + cdhLocation);
        }
        return cdhLocation;
    }
    
    private static ExceptionServiceVo.OrderLineItem getLatestOrderLineItems(ExceptionServiceVo caseDetails){
        if(caseDetails.orderLineItems == null){
            return null;
        }
        System.debug('@@@@@@@@@@@@@@@@@@@@@caseDetails - '+caseDetails.exceptionId);
        List<ExceptionServiceVo.OrderLineItem> odlItems = caseDetails.orderLineItems.clone();
        odlItems.sort();
        //return odlItems[0];
        
        //New Code Starts Here
        String occoExceptionLabelValue = System.Label.OccoExceptionIdSet;
        String[] exceptioIdvalues = occoExceptionLabelValue.split(',');
        System.debug('exceptioIdvalues - '+exceptioIdvalues);
        
        ExceptionServiceVo.OrderLineItem returnitem = new ExceptionServiceVo.OrderLineItem();
        
        System.debug('@@@@@@@@@@@@@@@@@@firstFulfilmentOptionName -> '+odlItems[0].fulfilmentOptionName);
        //if(odlItems[0].fulfilmentOptionName != null && (caseDetails.exceptionId.equals('CASE0011') || (caseDetails.exceptionId.equals('CASE0037')))){
        if(odlItems[0].fulfilmentOptionName != null && (exceptioIdvalues.contains(caseDetails.exceptionId))){
            System.debug('@@@@@@Exception Id will be one among - CASE0011,CASE0037,CASE0029');
            for(ExceptionServiceVo.OrderLineItem olitems: odlItems){
                if(olitems.fulfilmentOptionName.equals(odlItems[0].fulfilmentOptionName)){
                    System.debug('@@@@@@@@@@@@@@@@@@fulfilmentOptionName matches....first condition is true');
                    if(olitems.deliveryDate != null){
                        System.debug('@@@@@@@@@@@@@@@@@@Second Check PAss - deliveryDate is not null '+olitems.deliveryDate);
                        returnitem = olitems;
                        System.debug('Item to returned - '+returnitem);
                    }
                }
            }
        }else{
            return odlItems[0];
        }            
        //New Code Ends Here
                
        return returnitem;
    }
    
  private static Map<String , ServiceConstants__mdt> getOrderExceptionConstantsMap()  {
        //SalesForce internally caches the custom metadata records. Querying custom metadata also doesn’t count towards SOQL limits
        Map<String, ServiceConstants__mdt> orderExceptionConstantsMap = new Map<String, ServiceConstants__mdt> ();
        for(ServiceConstants__mdt sConstanceMdt : [SELECT label,MasterLabel, developerName,Value__c From ServiceConstants__mdt WHERE  Service_Name__c=:ORDER_EXCEPTION_SERVICE  ]) {
            orderExceptionConstantsMap.put(sConstanceMdt.developerName,  sConstanceMdt);
        }
        return orderExceptionConstantsMap;
    }

    
    public static void addError(String key,ExceptionServiceVo orderException){
        ServiceConstants__mdt serviceError =  ORDER_EXCEPTIONS_MESSAGE.get(key);
        addError(serviceError.MasterLabel,serviceError.Value__c,orderException );
    }
    
    public static void addError(String label,String value, ExceptionServiceVo orderException){
        ExceptionServiceVo.ServiceResult res = new ExceptionServiceVo.ServiceResult();
        res.hasError = true;
        res.errorMessage=value;
        res.errorLabel=label;
        orderException.serviceResult = res;
    }
    
    
    
     private static String getUpperCase(String value){
        String returnVal = '';
        if(String.isNotBlank(value)){
            returnVal = value.toUpperCase();
        }
        return returnVal;
     }
     
     
     public class OrderDetailsWrapper {
         public List< String > emailAddressFullList;
         public List< String > orderDetailsList;
         
     }
}