//  Edit    Date        Author      Comment
//  001     12/10/14    JA          Updated the "Please refresh your Case list" error message
//  002     31/10/16    MP          Allow Tier 3 & 4 users to skip the assigned to another user validation
//  003     07/11/2016  LLJ         Rename Class

global class AssignToMeController {
    
    @RemoteAction
    global static String assign(ID myId, string sobjecttype) {
        System.debug('@@entering AssignToMeController.assign');

        try {
            if ('Case'.equals(sobjecttype)) {
                // <<002>> Add Tier check
                User thisUser = [SELECT Id, Tier__c FROM User WHERE Id = :UserInfo.getUserId()];
                Case c = [SELECT id, OwnerId FROM Case WHERE id = :myId FOR UPDATE];
                if (String.valueOf(c.OwnerId).startsWith('005') && (thisUser.Tier__c == null || thisUser.Tier__c < 3)) {
                    return 'Error : This Case has already been assigned to another user. Please refresh your Case list and choose a different Case.';
                }
                c.OwnerId = UserInfo.getUserId();
                update c;
                System.debug('@@in kbassign: record updated. Case id = ' + myId);
            }
            if ('Task'.equals(sobjecttype)) {
                Task t = [SELECT id, OwnerId FROM Task WHERE id = :myId];
                t.OwnerId = UserInfo.getUserId();
                update t;
                System.debug('@@in kbassign: record updated. Task id = ' + myId);
            }
        }catch(Exception e){
            return 'Error : ' + e.getMessage();
        }
        System.debug('@@exiting AssignToMeController.assign');
        
        return '';
    }

    public AssignToMeController () {
    }

    public AssignToMeController (ApexPages.StandardController controller) {
        this();
        System.debug('in the controller constructor');
    }
}