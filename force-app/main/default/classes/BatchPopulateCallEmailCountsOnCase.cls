/*
	This batch apex is used to populate the Total Call Logs and Email counts on the Cases
*/

global class BatchPopulateCallEmailCountsOnCase implements Database.Batchable<sObject> {
	
	private String query;
	
	global  BatchPopulateCallEmailCountsOnCase(String query){
		this.query = query;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC){
		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, List<Case> scope){
	
		List<Case> newCaseList = [select Id,Number_of_Call_Contacts__c, Last_Call_Contact__c, Number_of_Email_Contacts__c, Last_Email_Contact__c,
									(select Id, createdDate from Tasks where Subject = 'Call log'), (select Id, createdDate from EmailMessages where status!= '5')
									from Case where Id IN :scope];
		
        						
		for(Case c : newCaseList){
			Integer totalCallLogCount = 0;
			DateTime latestTaskDate;
			Integer totalEmailMessagesCount = 0;
			DateTime latestEmailMessageDate;
			
			
			totalCallLogCount = c.Tasks.size();
			for(Task t : c.Tasks){
				if(latestTaskDate == null || latestTaskDate < t.createdDate)
					latestTaskDate = t.createdDate;
					
			}
			
			totalEmailMessagesCount = c.EmailMessages.size();
			for(EmailMessage em : c.EmailMessages){
				if(latestEmailMessageDate == null || latestEmailMessageDate < em.createdDate)
					latestEmailMessageDate = em.createdDate;
			}
			
			c.Number_of_Call_Contacts__c = totalCallLogCount;
			c.Last_Call_Contact__c = latestTaskDate;
			c.Number_of_Email_Contacts__c = totalEmailMessagesCount;
			c.Last_Email_Contact__c = latestEmailMessageDate;
			system.debug('====totalCallLogCount===' + totalCallLogCount);
			system.debug('====latestTaskDate===' + latestTaskDate);
			system.debug('====totalEmailMessagesCount===' + totalEmailMessagesCount);
			system.debug('====latestEmailMessageDate===' + latestEmailMessageDate);

		}
		Database.update(newCaseList, false);
	}

	global void finish(Database.BatchableContext BC){
	}

}