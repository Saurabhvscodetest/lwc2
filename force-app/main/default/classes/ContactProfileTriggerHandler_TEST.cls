@IsTest
public with sharing class ContactProfileTriggerHandler_TEST { 
	
	public static testMethod void testContactProfileDMLs(){
		
		UnitTestDataFactory.setRunValidationRules(false);
		
		Contact con = UnitTestDataFactory.createContact(new Account());
		con.Email = 'a@bb.com';
		insert con;
		
		Contact_Profile__c cp = UnitTestDataFactory.createContactProfile(con);
		cp.Email__c = 'a@bb.co.uk';
		insert cp;
		
		cp.FirstName__c = 'FirstName';
		update cp;
		
		delete cp;
		
	}

}