/**
 * Unit tests for CSVReportEmailer class.
 */
@isTest (seeAllData=false)
private class CSVReportEmailer_TEST {

    /**
     * Code coverage method for CSVReportEmailer.emailCSVReport()
     */
	static testMethod void testEmailCSVReport() {
    	system.debug('TEST START: testEmailCSVReport');
		Test.startTest();
    	CSVReportEmailer.emailCSVReport('00Ob0000003pS3c', 'TEST REPORT', 'matthew.povey@johnlewis.co.uk');
		Test.stopTest();
    	system.debug('TEST END: testEmailCSVReport');
	}

    /**
     * Code coverage method for CSVReportEmailer.invokeEmailCSVReportService()
     */
	static testMethod void testInvokeEmailCSVReportServiceNoSession() {
		Id anyUserID = [select id from user where isActive=true limit 1].id;
		Config_Settings__c c1 = new Config_Settings__c(name='WORK_ALLOCATION_REPORT_USER',text_value__c = anyUserID);
		Config_Settings__c c2 = new Config_Settings__c(name='EMAIL_NOTIFICATION_DEFAULT_ADDRESS',text_value__c = anyUserID);
		List<Config_Settings__c> configList = new List<Config_Settings__c> {c1, c2};
		insert configList;

    	system.debug('TEST START: testInvokeEmailCSVReportServiceNoSession');
		Test.startTest();
    	CSVReportEmailer.invokeEmailCSVReportService('00Ob0000003pS3c', 'TEST REPORT', 'matthew.povey@johnlewis.co.uk', null);
		Test.stopTest();
    	system.debug('TEST END: testInvokeEmailCSVReportServiceNoSession');
	}
	
    /**
     * Code coverage method for CSVReportEmailer.getSessionIdFromResponse()
     */
	static testMethod void testGetSessionIdFromResponse() {
    	system.debug('TEST START: testGetSessionIdFromResponse');
		String xml1 = '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns="urn:partner.soap.sforce.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><soapenv:Body><loginResponse><result><metadataServerUrl>https://johnlewis--case1dev.cs20.my.salesforce.com/services/Soap/m/22.0/00Dm00000001jKU</metadataServerUrl><passwordExpired>false</passwordExpired><sandbox>true</sandbox><serverUrl>https://johnlewis--case1dev.cs20.my.salesforce.com/services/Soap/u/22.0/00Dm00000001jKU</serverUrl><sessionId>00Dm00000001jKU!AR0AQIM4njWa98QJJgwH_CG1CddCk8SqLAqADXmn99nuCyMmUTFfyvcqAXHc61SzT4p8xuZmwlvvMsg.FZXqAKxVh6clehx4</sessionId><userId>005m0000001Br5VAAS</userId><userInfo><accessibilityMode>false</accessibilityMode><currencySymbol>£</currencySymbol><orgAttachmentFileSizeLimit>26214400</orgAttachmentFileSizeLimit><orgDefaultCurrencyIsoCode>GBP</orgDefaultCurrencyIsoCode><orgDisallowHtmlAttachments>false</orgDisallowHtmlAttachments><orgHasPersonAccounts>false</orgHasPersonAccounts><organizationId>00Dm00000001jKUEAY</organizationId><organizationMultiCurrency>false</organizationMultiCurrency><organizationName>John Lewis</organizationName><profileId>00eb0000000ZJYaAAO</profileId><roleId>00Eb0000000Yhx5EAC</roleId><sessionSecondsValid>7200</sessionSecondsValid><userDefaultCurrencyIsoCode xsi:nil="true"/><userEmail>matthew.povey@johnlewis.co.uk</userEmail><userFullName>Matt Povey</userFullName><userId>005m0000001Br5VAAS</userId><userLanguage>en_US</userLanguage><userLocale>en_GB</userLocale><userName>matthew.povey@johnlewis.co.uk.case1dev</userName><userTimeZone>Europe/London</userTimeZone><userType>Standard</userType><userUiSkin>Theme3</userUiSkin></userInfo></result></loginResponse></soapenv:Body></soapenv:Envelope>';
		Dom.document doc = new Dom.document();
      	doc.load(xml1);
      	Dom.XMLNode root = doc.getRootElement();

		Test.startTest();
		String sessionId = CSVReportEmailer.getSessionIdFromResponse(root);
		Test.stopTest();
		
		system.assertEquals('00Dm00000001jKU!AR0AQIM4njWa98QJJgwH_CG1CddCk8SqLAqADXmn99nuCyMmUTFfyvcqAXHc61SzT4p8xuZmwlvvMsg.FZXqAKxVh6clehx4', sessionId);
    	system.debug('TEST END: testGetSessionIdFromResponse');
	}
}