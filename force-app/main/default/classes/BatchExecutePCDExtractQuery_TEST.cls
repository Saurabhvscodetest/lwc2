/**
 * Unit tests for BatchExecutePCDExtractQuery class methods.
 */
@isTest
private class BatchExecutePCDExtractQuery_TEST {

	/**
	 * Code coverage for BatchExecutePCDExtractQuery.execute(SchedulableContext sc)
	 */
	static testMethod void testSchedulerExecuteMethod() {
    	system.debug('TEST START: testSchedulerExecuteMethod');
		initialiseSettings();

		SchedulableContext sc = null;
		BatchExecutePCDExtractQuery pcdBatch = new BatchExecutePCDExtractQuery();

		Test.startTest();
		pcdBatch.execute(sc);
		Test.stopTest();

    	system.debug('TEST END: testSchedulerExecuteMethod');
	}

    /**
     * Code coverage for BatchExecutePCDExtractQuery.start
     */
    static testMethod void testStartMethod() {
        system.debug('TEST START: testStartMethod');
        initialiseSettings();

        Database.BatchableContext bc = null;
		BatchExecutePCDExtractQuery pcdBatch = new BatchExecutePCDExtractQuery();

        Test.startTest();
        Database.QueryLocator dql = pcdBatch.start(bc);  
        Test.stopTest();

        system.debug('TEST END: testStartMethod');
    }

    /**
     * Test BatchPCDSupersededContactUpdate.execute
     */
    static testMethod void testExecuteMethod() {
        system.debug('TEST START: testExecuteMethod');
        initialiseSettings();

        Contact testCon1 = UnitTestDataFactory.createContact(1, false);
        Contact testCon2 = UnitTestDataFactory.createContact(2, false);
        List<Contact> conList = new List<Contact> {testCon1, testCon2};
        insert conList;
                
        Database.BatchableContext bc = null;
		BatchExecutePCDExtractQuery pcdBatch = new BatchExecutePCDExtractQuery();

        test.startTest();
        pcdBatch.execute(bc, conList);
        test.stopTest();

        system.debug('TEST END: testExecuteMethod');
    }

    /**
     * Code coverage for BatchPCDSupersededContactUpdate.finish()
     */
    static testMethod void testFinishMethod() {
        system.debug('TEST START: testFinishMethod');
        initialiseSettings();

        Database.BatchableContext bc = null;
		BatchExecutePCDExtractQuery pcdBatch = new BatchExecutePCDExtractQuery();

        Test.startTest();
        pcdBatch.finish(bc);
        Test.stopTest();

        system.debug('TEST END: testFinishMethod');
    }

    /**
     * Custom setting initialisation
     */
    private static void initialiseSettings() {
    	Config_Settings__c defaultNotificationEmail = new Config_Settings__c(Name = 'EMAIL_NOTIFICATION_DEFAULT_ADDRESS', Text_Value__c = 'Matthew.Povey@johnlewis.co.uk');
    	Config_Settings__c pcdQueryEmailSetting = new Config_Settings__c(Name = 'PCD_QUERY_RUNNER_EMAIL_LIST', Text_Value__c = 'Matthew.Povey@johnlewis.co.uk');
		Config_Settings__c pcdNoDaysSetting  = new Config_Settings__c(Name = 'PCD_QUERY_RUNNER_NUMBER_OF_DAYS', Number_Value__c = 1);
		Config_Settings__c pcdExcludeUserSetting  = new Config_Settings__c(Name = 'PCD_QUERY_RUNNER_EXCLUDE_USERS', Text_Value__c = '005b0000001DohCAAS;005b0000002TtUSAA0');
    	List<Config_Settings__c> csList = new List<Config_Settings__c> {defaultNotificationEmail, pcdQueryEmailSetting, pcdNoDaysSetting, pcdExcludeUserSetting};
    	insert csList;
    }
}