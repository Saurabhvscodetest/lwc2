public class clsLoyaltyCard {
    
    //Variables to get records
	private static List<id> AccountCard=new List<id>();
    private static List<Loyalty_Card__c> loyaltyCard=new List<Loyalty_Card__c>();
    private static List<id> loyaltyCardId=new List<id>();
    private static List<Transaction_Header__c> transactionHeaders=new List<Transaction_Header__c>();
    
    
	//main method called by trigger    
    public static void mainEntry(Boolean isbefore,Boolean isdelete,List<Loyalty_Account__c> oldvalue){
    system.debug('List of value'+oldvalue);
    //Getting Loyalty account Id
    for(Loyalty_Account__c a:oldvalue){        
        AccountCard.add(a.Id);
    }
 
    loyaltyCardId=getLoyaltyCardDetails(AccountCard);
    transactionHeaders=getTransactionHeaders(loyaltyCardId);
    delete transactionHeaders;  
    }
    
    //Method to get Loyalty Card Ids
    public static List<id> getLoyaltyCardDetails(List<id> AccountCard){
        loyaltyCard=[select id from Loyalty_Card__c where Loyalty_Account__c IN : AccountCard];
        system.debug('loyaltyCard'+loyaltyCard);
        
        for (Loyalty_Card__c b: loyaltyCard){
            loyaltyCardId.add(b.id);
          }
        return loyaltyCardId;
    }
    
    // Method to get transaction header releated to loyalty card
    public static List<Transaction_Header__c> getTransactionHeaders(List<id> loyaltyCardId){
        transactionHeaders=[select id from Transaction_Header__c where Card_Number__c IN : loyaltyCardId ];
        system.debug('transactionHeader'+transactionHeaders);
  			return transactionHeaders;
    }

    
}