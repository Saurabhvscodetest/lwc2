/*************************************************
coSoapPostCaseComment

class to encapsulate soap requests

Author: Steven Loftus (MakePositive)
Created Date: 14/11/2014
Modification Date: 
Modified By: 

**************************************************/
public class coSoapPostCaseComment {

    // public parameters to use in the search
    public List<CaseComment> CaseCommentList {get; set;}
    public Map<Id, String> OrderIdByCaseIdMap {get; set;}
    public String UserName {get; set;}
    public String UserRole {get; set;}

    // public result parameters
    public String Status {get; set;}
    public ErrorResult Error {get; set;}

    public String getSoapRequest() {

        // always need to have a first and last name
        String soapRequest =    '<?xml version="1.0" encoding="UTF-8"?>' +
                                '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">' +
                                    '<soapenv:Header/>' +
                                    '<soapenv:Body>' +
                                        '<managecusoc:CreateCustomerOrderComment xmlns:managecusoc="http://soa.johnlewispartnership.com/service/es/Transaction/CustomerOrder/CustomerOrderComment/ManageCustomerOrderComment/v1" xmlns:cmnEBM="http://soa.johnlewispartnership.com/schema/ebm/CommonEBM/v1" xmlns:managecusocEBM="http://soa.johnlewispartnership.com/schema/ebm/Transaction/CustomerOrder/ManageCustomerOrderComment/v1" xmlns:cmn="http://soa.johnlewispartnership.com/schema/ebo/Common/v1" xmlns:txn="http://soa.johnlewispartnership.com/schema/ebo/Transaction/v1" xmlns:wsa="http://www.w3.org/2005/08/addressing">' +
                                            '<cmnEBM:ApplicationArea sentDateTime="' + DateTime.now().format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'') + '">' +
                                                '<cmnEBM:Channel>' +
                                                    '<cmnEBM:ChannelID schemeID="OCIP" schemeAgencyID="JL"/>' +
                                                    '<cmnEBM:ChannelName />' +
                                                    '<cmnEBM:ChannelType>filler</cmnEBM:ChannelType>' +
                                                '</cmnEBM:Channel>' +
                                                '<cmnEBM:Sender>' +
                                                    '<cmnEBM:LogicalID schemeID="CaseManagement" schemeAgencyID="JL"/>' +
                                                '</cmnEBM:Sender>' +
                                                '<wsa:MessageID>' + coSoapHelper.generateGuid() + '</wsa:MessageID>' +
                                                '<cmnEBM:TrackingID>' + coSoapHelper.generateGuid() + '</cmnEBM:TrackingID>' +
                                            '</cmnEBM:ApplicationArea>' +
                                            '<managecusocEBM:DataArea>' +
                                                '<managecusocEBM:Create/>';

        for (CaseComment cc : this.CaseCommentList) {
            
            soapRequest +=                      '<managecusocEBM:CustomerOrderComment>' +
                                                    '<txn:CommentBody><![CDATA[' + cc.CommentBody + '  ----  ' + cc.Parent.CaseNumber 
                           + ']]></txn:CommentBody>' +
                                                    '<txn:CustomerOrderCommentTypeID>' +
                                                        '<cmn:xrefIdentity ID="3" applicationID="JLDOrderManager"/>' +
                                                    '</txn:CustomerOrderCommentTypeID>' +
                                                    '<txn:CustomerOrderID>' +
                                                        '<cmn:xrefIdentity ID="' + this.OrderIdByCaseIdMap.get(cc.ParentId) + '" applicationID="JLDOrderManager"/>' +
                                                    '</txn:CustomerOrderID>' +
                                                    '<txn:WorkerID ID="FirstName LastName">' +
                                                        '<cmn:xrefIdentity ID="' + this.UserName + '" applicationID="OCIP"/>' +
                                                    '</txn:WorkerID>' +
                                                    '<txn:WorkerPositionAtCommentCreation>' + this.UserRole + '</txn:WorkerPositionAtCommentCreation>' +
                                                '</managecusocEBM:CustomerOrderComment>';
        }

        soapRequest +=                      '</managecusocEBM:DataArea>'+
                                        '</managecusoc:CreateCustomerOrderComment>'+
                                    '</soapenv:Body>'+
                                '</soapenv:Envelope>';

        // return the built soap request
        return soapRequest;
    }

    // called to parse the soap response from the service
    public void setSoapResponse(string soapResponseDocument) {

        Dom.Document doc = new Dom.Document();
        
        doc.load(soapResponseDocument);

        Dom.XMLNode rootNode = doc.getRootElement();

        // get all the namespaces out of the response
        String serverEndPoint = 'http://soa.johnlewispartnership.com/';

        String cmnNs = serverEndPoint + 'schema/ebm/CommonEBM/v1';
        String managecusocNS = serverEndPoint + 'service/es/Transaction/CustomerOrder/CustomerOrderComment/ManageCustomerOrderComment/v1'; 
        String managecusocEBMNS = serverEndPoint + 'schema/ebm/Transaction/CustomerOrder/ManageCustomerOrderComment/v1';   
        String soapenvNS = 'http://schemas.xmlsoap.org/soap/envelope/';  

        try {
    
            if (rootNode.getChildElement('Body', soapenvNS).getChildElement('Fault', soapenvNS) != null) {

                dealWithFaultNode(rootNode.getChildElement('Body', soapenvNS).getChildElement('Fault', soapenvNS));
                return;
            }

            // get the dataarea and show nodes
            Dom.XMLNode dataAreaNode = rootNode.getChildElement('Body', soapenvNS).getChildElement('AcknowledgeCustomerOrderComment', managecusocNS).getChildElement('DataArea', managecusocEBMNS);
            Dom.XMLNode acknowledgeNode = dataAreaNode.getChildElement('Acknowledge', managecusocEBMNS);

            // if there is a problem then get the error details and return
            this.Status = acknowledgeNode.getAttributeValue('responseCode', '');

            if (this.Status != 'Success') {
                if (acknowledgeNode.getAttributeValue('hasActionStatusRecords', '') == 'true') {

                    setErrorDetails(acknowledgeNode.getChildElement('ActionStatusRecord', cmnNS).getChildElement('ServiceError', cmnNS));
                    return;

                // see if there is an internal error then get the details and return
                } else {

                    setErrorDetails(acknowledgeNode.getChildElement('ServiceError', cmnNS));
                    return;
                }
            }

        } catch (Exception e) {

            this.Status = 'Error';
            // catch all in case we try to access a node we think should be there but it is not
            setErrorDetails('INTERNAL_ERROR', 'Technical', 'An exception was generated during the parsing of the soap response: ' + e.getMessage());
        }
    }

    private void dealWithFaultNode(Dom.XMLNode faultNode) {

        this.Status = 'Error';
        setErrorDetails(faultNode.getChildElement('faultcode', null).getText(), null, faultNode.getChildElement('faultstring', null).getText());
    }

    // override eof setErrorDetails
    private void setErrorDetails(Dom.XMLNode errorNode) {

        setErrorDetails(errorNode.getAttributeValue('errorCode', ''), errorNode.getAttributeValue('errorType', ''),errorNode.getAttributeValue('errorDescription', ''));
    }

    // set the error details passed in
    private void setErrorDetails(String code, String type, String description) {

        this.Error = new ErrorResult();
        this.Error.ErrorCode = code;
        this.Error.ErrorType = type;
        this.Error.ErrorDescription = description;          
    }

    // class to hold the error
    public class ErrorResult {

        public String ErrorCode {get; set;}
        public String ErrorType {get; set;}
        public String ErrorDescription {get; set;}
    }
}