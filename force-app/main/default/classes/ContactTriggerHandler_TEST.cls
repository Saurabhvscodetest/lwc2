@IsTest
public with sharing class ContactTriggerHandler_TEST 
{
  
  
  public static TestMethod void testInsertNewContactNoAccount()
  {
    // Want to skip validation rules
    UnitTestDataFactory.setRunValidationRules(false);

    // Insert a contact that does not have a parent account record
    Contact con = UnitTestDataFactory.createContact();
    
    insert con;
  }
  
  public static TestMethod void testUpdateMatchingKeys(){
    
    // Want to skip validation rules
    UnitTestDataFactory.setRunValidationRules(false);
    
    // Insert a contact that does not have a parent account record
    Contact con = new Contact();
    con.FirstName = 'fName';
    con.LastName = 'lName';
    con.Email = 'email@address.com';
    con.MailingStreet = 'first line \nsecond line';
    con.MailingPostalCode = 'qwe rty';
    
    insert con;
    
    Contact result = [SELECT Id, Case_Matching_Key_1__c, Case_Matching_Key_2__c FROM Contact WHERE Id=:con.Id];
    
    System.assertEquals('fnamelnameemail@address.com', result.Case_Matching_Key_1__c);
    System.assertEquals('fnamelnamefirstlinesecondlineqwerty', result.Case_Matching_Key_2__c);
    
    con.FirstName = 'fName2';
    update con;
    
    result = [SELECT Id, Case_Matching_Key_1__c, Case_Matching_Key_2__c FROM Contact WHERE Id=:con.Id];
    
    System.assertEquals('fname2lnameemail@address.com', result.Case_Matching_Key_1__c);
    System.assertEquals('fname2lnamefirstlinesecondlineqwerty', result.Case_Matching_Key_2__c);
    
  }
  
  public static testMethod void canEditOrderingCustomerWithProperPermissionSet(){
    User frontOfficeUser = UnitTestDataFactory.getFrontOfficeUser('TEST.JOHNLEWIS');
    insert frontOfficeUser;
    
    System.runAs(frontOfficeUser){  
      //assign the permission set to edit the ordering customer
      UnitTestDataFactory.assignPermissionSet(frontOfficeUser.Id, clsContactTriggerHandler.ORDERING_CUSTOMERS_EDIT_PERMISSION_SET_NAME);
    }   
    Account acc= UnitTestDataFactory.createAccount('TESTACCOUNT');
    Id orderingCustRecordTypeId = CommonStaticUtils.getRecordType('Contact', CommonStaticUtils.CONTACT_RT_DEVELEPER_NAME_OC).Id ;
    Contact con= UnitTestDataFactory.createContact(1, acc.Id, false);
    con.RecordTypeId = orderingCustRecordTypeId;
    System.runAs(frontOfficeUser){  
      insert con;
    }
    
    //Get a front office user , since they cannot edit the ordering customer by default
    String firstNameAfterEdit = con.FirstName +'ZZZ';
    Test.startTest();
      System.runAs(frontOfficeUser){  
        //edit ordering customer as the front office user
        con.FirstName = firstNameAfterEdit ;
        update con;
      }
    Test.stopTest();
    
    Contact actualContact = [Select Id, FirstName from Contact where Id=: con.Id LIMIT 1];
    System.assertEquals(firstNameAfterEdit, actualContact.FirstName, 'Ordering customer can be edited by user with correct permission set');
    
  }

  public static testMethod void canEditOrderingCustomer_WithUnrestrictedProfile(){
    Profile prof = [Select Id, Name From Profile Where Name = 'JL: Unrestricted Profile (5 Users Max)'];
    User unrestrcitedUser =  UnitTestDataFactory.getUnrestrcitedUser('TEST123');
    insert unrestrcitedUser;
    
    Account acc= UnitTestDataFactory.createAccount('TESTACCOUNT');
    Id orderingCustRecordTypeId = CommonStaticUtils.getRecordType('Contact', CommonStaticUtils.CONTACT_RT_DEVELEPER_NAME_OC).Id ;
    Contact con= UnitTestDataFactory.createContact(1, acc.Id, false);
    con.RecordTypeId = orderingCustRecordTypeId;
    System.runAs(unrestrcitedUser){ 
      insert con;
    }
    
    //Get a front office user , since they cannot edit the ordering customer by default
    String firstNameAfterEdit = con.FirstName +'ZZZ';
    Test.startTest();
      System.runAs(unrestrcitedUser){ 
        //edit ordering customer as the front office user
        con.FirstName = firstNameAfterEdit ;
        update con;
      }
    Test.stopTest();
    
    Contact actualContact = [Select Id, FirstName from Contact where Id=: con.Id LIMIT 1];
    System.assertEquals(firstNameAfterEdit, actualContact.FirstName, 'Ordering customer can be edited by user with Unrestricted Profile');
  }
  
  public static testMethod void cannotEditOrderingCustomer_WithoutProperPermissionSet(){
    
    User frontOfficeUser = UnitTestDataFactory.getFrontOfficeUser('TEST.JOHNLEWIS');
    insert frontOfficeUser;
    User unrestrcitedUser =  UnitTestDataFactory.getUnrestrcitedUser('TEST123');
    insert unrestrcitedUser;
    
    Account acc= UnitTestDataFactory.createAccount('TESTACCOUNT');
    Id orderingCustRecordTypeId = CommonStaticUtils.getRecordType('Contact', CommonStaticUtils.CONTACT_RT_DEVELEPER_NAME_OC).Id ;
    Contact con= UnitTestDataFactory.createContact(1, acc.Id, false);
    con.RecordTypeId = orderingCustRecordTypeId;
    
    System.runAs(unrestrcitedUser){ 
      insert con;
    }
    
    //Get a front office user , since they cannot edit the ordering customer by default
    String firstNameAfterEdit = con.FirstName +'ZZZ';
    Test.startTest();
      System.runAs(frontOfficeUser){  
        //edit ordering customer as the front office user
        con.FirstName = firstNameAfterEdit ;
        try{
          update con;
        } catch( Exception exp){
          System.assert(exp.getMessage().contains(clsContactTriggerHandler.ORDERING_CUSTOMERS_EDIT_ERROR), 'Ordering customer cannot be edited by user without correct permission set');
        }
      }
    Test.stopTest();
  }
  
  
}