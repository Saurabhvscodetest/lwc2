/**
* Author:       Nawaz Ahmed
* Date:         09/03/2016
* Description:  Controller for CommsGateway page, for the tab Customer Communciations
*
* ******************* Change Log *******************
* Modified by       Change Date     Change
* Nawaz Ahmed        09/03/2016     Initial Version
*/
public with sharing class CommsGatewayController {
    public static final boolean SEARCH_ARCHIVE = true; 
    public static final boolean DO_NOT_SEARCH_ARCHIVE = false; 
    public transient List<DataExtensionObject_DO> dataList {get;set;}
    public static final string MESSAGE_PROVIDE_ATLEAST_ONE_INPUT = Comms_Gateway_Constants__c.getinstance('MESSAGE_PROVIDE_ATLEAST_ONE_INPUT').Value__c;
    public static final string MESSAGE_PROVIDE_ONLY_ONE_INPUT = Comms_Gateway_Constants__c.getinstance('MESSAGE_PROVIDE_ONLY_ONE_INPUT').Value__c;
    public static final string MESSAGE_ORDER_NUMBER_INVALID = Comms_Gateway_Constants__c.getinstance('MESSAGE_ORDER_NUMBER_INVALID').Value__c;
    public static final string MESSAGE_EMAIL_ADDRESS_INVALID = Comms_Gateway_Constants__c.getinstance('MESSAGE_EMAIL_ADDRESS_INVALID').Value__c;
    public static final string EMAIL_ADDRESS_CHARACTER = Comms_Gateway_Constants__c.getinstance('EMAIL_CHARCTER').Value__c;
    public static final string MESSAGE_DELIVERY_NUMBER_INVALID = Comms_Gateway_Constants__c.getinstance('MESSAGE_DELIVERY_NUMBER_INVALID').Value__c;
    public static final string MESSAGE_PHONE_NUMBER_INVALID = Comms_Gateway_Constants__c.getinstance('MESSAGE_PHONE_NUMBER_INVALID').Value__c;
    public static final string MESSAGE_DELIVERY_NUMBER_LENGTH_INVALID = Comms_Gateway_Constants__c.getinstance('MESSAGE_DELIVERY_NUMBER_LENGTH_INVALID').Value__c;
    public static final string MESSAGE_ORDER_NUMBER_LENGTH_INVALID = Comms_Gateway_Constants__c.getinstance('MESSAGE_ORDER_NUMBER_LENGTH_INVALID').Value__c;
    public static final string MESSAGE_PHONE_NO_LENGTH_INVALID_PREFIX = Comms_Gateway_Constants__c.getinstance('MESSAGE_PHONE_NO_LENGTH_INVALID_PREFIX').Value__c;
    public static final string MESSAGE_PHONE_NO_LENGTH_INVALID_SUFFIX = Comms_Gateway_Constants__c.getinstance('MESSAGE_PHONE_NO_LENGTH_INVALID_SUFFIX').Value__c;
    public static final string MESSAGE_EMAIL_ADDRESS_LENGTH_INVALID = Comms_Gateway_Constants__c.getinstance('MESSAGE_EMAIL_ADDRESS_LENGTH_INVALID').Value__c;
    public static final string NO_COMMUNICATION_FOUND = Comms_Gateway_Constants__c.getinstance('NO_COMMUNICATION_FOUND').Value__c;
    public static final string NO_COMMUNICATION_FOUND_ARCHIVE = Comms_Gateway_Constants__c.getinstance('NO_COMMUNICATION_FOUND_ARCHIVE').Value__c;
    public static final string COMMUNICATION_ERROR = Comms_Gateway_Constants__c.getinstance('COMMUNICATION_ERROR').Value__c;
    public static final INTEGER PHONE_NUMBER_MIN_LENGTH = INTEGER.VALUEOF(Comms_Gateway_Constants__c.getinstance('PHONE_NUMBER_MIN_LENGTH').Value__c);
    public static final INTEGER PHONE_NUMBER_MAX_LENGTH = INTEGER.VALUEOF(Comms_Gateway_Constants__c.getinstance('PHONE_NUMBER_MAX_LENGTH').Value__c);
    public static final INTEGER EMAIL_ADDRESS_LENGTH = INTEGER.VALUEOF(Comms_Gateway_Constants__c.getinstance('EMAIL_ADDRESS_LENGTH').Value__c);
    public static final String DISPLAY_DATE_TIME_FORMAT = Comms_Gateway_Constants__c.getinstance('DISPLAY DATE FORMAT').Value__c;
    public static final String STATUS_DATE_TIME_HOVER_TEXT = Comms_Gateway_Constants__c.getinstance('DATE_TIME_STATUS_HOVER_TEXT').Value__c;
    public static final String SENT_DATE_TIME_HOVER_TEXT = Comms_Gateway_Constants__c.getinstance('DATE_TIME_SENT_HOVER_TEXT').Value__c;
    public static final String MSG_MORE_RECORDS_ARE_PRESENT = Comms_Gateway_Constants__c.getinstance('MSG_MORE_RECORDS_ARE_PRESENT').Value__c;
    
    
    public string ORDER_NUMBER_DESCRIPTION  {get;set;}
    public string DELIVERY_NUMBER_DESCRIPTION {get;set;}
    public string PHONE_NUMBER_DESCRIPTION  {get;set;}
    public string EMAIL_ADDRESS_DESCRIPTION  {get;set;}
    public String orderId {get;set;}
    public String deliveryId {get;set;}
    public String phoneNumber {get;set;}
    public string emailAddress {get;set;}
    public boolean orderIdError {get;set;}
    public boolean deliveryIdError {get;set;}
    public boolean phoneNumberError {get;set;}
    public boolean emailAddressError {get;set;}
    
    
    public string statusDateTimeHoverText {
        get{
            return STATUS_DATE_TIME_HOVER_TEXT;
        }
        set;
    }
    
    public boolean isArchiveSearchEnabled {
        get{
            Profile_Configurations__c profileConfig = Profile_Configurations__c.getValues(UserInfo.getUserId());
            return (profileConfig != null && profileConfig.Enable_Comms_Gateway_Archive_Search__c);
        }
        set;
    }
    
    public string sentDateTimeHoverText {
        get{
            return SENT_DATE_TIME_HOVER_TEXT;
        }
        set;
    }
    
    /* Indication of any kind of service errors in connectivitye */
    public Boolean isBussinessError { get; set; }
    
    /* Error message populated in case of error. */
    public String errorMessage { get; set; }
    
    public  CommsGatewayController(){
        ORDER_NUMBER_DESCRIPTION = Comms_Gateway_Constants__c.getinstance('ORDER_NUMBER_DESCRIPTION').Value__c;
        DELIVERY_NUMBER_DESCRIPTION = Comms_Gateway_Constants__c.getinstance('DELIVERY_NUMBER_DESCRIPTION').Value__c;
        PHONE_NUMBER_DESCRIPTION = Comms_Gateway_Constants__c.getinstance('PHONE_NUMBER_DESCRIPTION').Value__c;
        EMAIL_ADDRESS_DESCRIPTION = Comms_Gateway_Constants__c.getinstance('EMAIL_ADDRESS_DESCRIPTION').Value__c;
    }
    
    /**
    * method called when search is initiated from Customer Communications page
    * @params:  
    * @rtnval:  void
    */
    
    public void searchDelivery() {
        performSearch(DO_NOT_SEARCH_ARCHIVE, NO_COMMUNICATION_FOUND);
    }
    
    public void performSearch(boolean searchArchive, String noRecordsMessage) {
        resetFields();
        if(!areInputsValid()){
            dataList = null;
            return;
        }
        ExactTargetServiceHelper.ResponseInfo respInfo = null;
        try{
            respInfo =  ExactTargetServiceHelper.getResponseList(orderId, deliveryId , phoneNumber, emailAddress, searchArchive);
            dataList = respInfo.responseDataList;
            system.debug('dataList SIZE ==>' + ((dataList!= null) ? '' + dataList.size() : 'EMPTY' ) );
        }
        catch (System.CalloutException exp) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,COMMUNICATION_ERROR));
            return ;                
        }
        catch (Exception exp) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,COMMUNICATION_ERROR));
            return ;   
        }   
        if(dataList.size() ==0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING ,noRecordsMessage));
            return ;
        }
        if(respInfo.areThereSomeMoreCommunications){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING ,MSG_MORE_RECORDS_ARE_PRESENT));
            return ;
        }
    }
    
    public void searchArchiveDelivery() {
       performSearch(SEARCH_ARCHIVE, NO_COMMUNICATION_FOUND_ARCHIVE);
    }
    
    /**
    * method to check wehter the iputs are valid
    * @params:  
    * @rtnval:  boolean
    */
    private boolean areInputsValid(){
        boolean isInputsValid = true;
        populateErrorMessage();
        if(String.isNotBlank(errorMessage)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage));
            isInputsValid = false;           
        }     
        return isInputsValid;
    }
    
    /**
    * method called when clear is initiated from Customer Communications page
    * @params:  
    * @rtnval:  void
    */
    public void clearData(){
        orderId = null;
        deliveryId = null;
        phoneNumber = null;
        emailAddress = null;
        dataList = null;
        resetFields();
    }
    
    public void resetFields(){
        ApexPages.getMessages().clear();
        orderIdError = false;
        deliveryIdError = false;
        phoneNumberError = false;
        emailAddressError = false;
    }
    /**
    * Handle the validations on the Inputs and populate the error message if required
    * @params:  
    * @rtnval:  void
    */    
    public void populateErrorMessage() {
        
        errorMessage  = null;
        checkifNoInputs();
        //Check if the error message is populated if its populated return the call
        if(String.isNotBlank(errorMessage)){
            return ;
        }
        checkifMultipleInputs();
        //Check if the error message is populated if its populated return the call
        if(String.isNotBlank(errorMessage)){
            return ;
        }
        checkAnyAlphabets();
        //Check if the error message is populated if its populated return the call
        if(String.isNotBlank(errorMessage)){
            return ;
        }
        checkInvalidLength();
    }
    
    public string getDisplayDateFormat(){ return DISPLAY_DATE_TIME_FORMAT; }

    /**
    * method called to check if all input search fields are blank
    * @params:  
    * @rtnval:  void
    */    
    public void checkifNoInputs(){
        if(String.isBlank(orderId) && (String.isBlank(deliveryId) ) && (String.isBlank(phoneNumber) ) && (String.isBlank(emailAddress) )){
            errorMessage =  MESSAGE_PROVIDE_ATLEAST_ONE_INPUT;          
        } 
    }   

    /**
    * method called to check if more than one input search field is entered
    * @params:  
    * @rtnval:  void
    */       
    public void checkifMultipleInputs(){
        integer numberofInputs = 0;
        if (String.isNotBlank(orderId))
        {
            numberofInputs =   numberofInputs+1; 
        }
        
        if (String.isNotBlank(deliveryId))
        {
            numberofInputs =   numberofInputs+1; 
        }
        
        if (String.isNotBlank(phoneNumber))
        {
            numberofInputs =   numberofInputs+1; 
        }
        
        if (String.isNotBlank(emailAddress))
        {
            numberofInputs =   numberofInputs+1; 
        }
        
        if(numberofInputs >1){
            errorMessage =  MESSAGE_PROVIDE_ONLY_ONE_INPUT;
            if (String.isNotBlank(orderId)) {
                 orderIdError = true;
            }
            if (String.isNotBlank(deliveryId)) {
                deliveryIdError = true;
            }
            
            if (String.isNotBlank(phoneNumber)) {
                phoneNumberError = true;
            }
            
            if (String.isNotBlank(emailAddress)) {
                 emailAddressError = true;
            }
        }
    }
    
    /**
    * method called to check if any alphabets entered in Order Number, Delivery Number and Phone Number
    * @params:  
    * @rtnval:  void
    */     
    public boolean checkAnyAlphabets() {
        if (String.isNotBlank(orderId)){
            orderId = orderId.replaceall(' ','');
            if(!orderId.isnumeric()){
                errorMessage =   MESSAGE_ORDER_NUMBER_INVALID ;  
                orderIdError = true;
                return false;
            }
        } else  if (String.isNotBlank(deliveryId)){
            deliveryId = deliveryId.replaceall(' ','');
             if(!deliveryId.isnumeric()){
                errorMessage =  MESSAGE_DELIVERY_NUMBER_INVALID;  
                deliveryIdError = true;
                return false;
            }
        } else  if (String.isNotBlank(phoneNumber)){
            phoneNumber = phoneNumber.replaceall(' ','');
            if((!phoneNumber.startsWith('0')) || (!phoneNumber.isnumeric())){
                errorMessage =  MESSAGE_PHONE_NO_LENGTH_INVALID_PREFIX  + ' ' + PHONE_NUMBER_MIN_LENGTH + ' or ' + PHONE_NUMBER_MAX_LENGTH  + ' digits' + MESSAGE_PHONE_NO_LENGTH_INVALID_SUFFIX;  
                phoneNumberError = true;
                return false;
            }
        } else if(String.isNotBlank(emailAddress)){
            emailAddress = emailAddress.replaceall(' ','');
            if(!emailAddress.containsIgnoreCase(EMAIL_ADDRESS_CHARACTER)){
                errorMessage =  MESSAGE_EMAIL_ADDRESS_INVALID;  
                emailAddressError = true;
                return false;
            }
        }
        return true;
    }

    /**
    * method called to check input serach fields are exceeded in length
    * @params:  
    * @rtnval:  void
    */       
    public boolean checkInvalidLength() {
        if (String.isNotBlank(phoneNumber)){
            if((phoneNumber.length() > PHONE_NUMBER_MAX_LENGTH)  || (phoneNumber.length() < PHONE_NUMBER_MIN_LENGTH)){
                errorMessage =  MESSAGE_PHONE_NO_LENGTH_INVALID_PREFIX  + ' ' + PHONE_NUMBER_MIN_LENGTH + ' or ' + PHONE_NUMBER_MAX_LENGTH  + ' digits' + MESSAGE_PHONE_NO_LENGTH_INVALID_SUFFIX; 
                phoneNumberError = true;
                return false;
            }
        }
        
        else  if (String.isNotBlank(emailAddress)){
            if(emailAddress.length() > EMAIL_ADDRESS_LENGTH){
                errorMessage =  MESSAGE_EMAIL_ADDRESS_LENGTH_INVALID +  ' ' + EMAIL_ADDRESS_LENGTH + ' digits'; 
                emailAddressError = true;
                return false;
            }
        }
        return true;
    } 
    
    
}