@Istest
public class CaseSearchResultsController_TEST {
	
	  @isTest static void test_CaseSearchResults(){
        Case c;
		User sysAdmin 			= UnitTestDataFactory.getSystemAdministrator('admin1');
		User backOfficeUser 	= UnitTestDataFactory.getBackOfficeUser('bo', 'JL.com - Bookings');
		
		insert sysAdmin;	
		insert backOfficeUser;

        System.runAs(sysAdmin) {
            UnitTestDataFactory.initialiseContactCentreAndTeamSettings();   
            jl_runvalidations__c jrv = new jl_runvalidations__c(Name = backOfficeUser.Id, Run_Validations__c = true);
            insert jrv;             
        }

        System.runAs(backOfficeUser) {
        	Account acc = UnitTestDataFactory.createAccount(1, true);
        	Contact ctc = UnitTestDataFactory.createContact(1, acc.id, true);
			List<Case> caseList = new List<Case>();
	       	for (Integer i = 1; i <= 10; i++) {
		       	Case testCase = UnitTestDataFactory.createQueryCase(ctc.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, false);		       	
		       	testCase.Create_Case_Activity__c = true;
                caseList.add(testCase);
	       	}
	       	insert caseList;
	       	
		}

		List<Case> caseListTest = [select Id, Description,Type_Detail__c,jl_Customer_Response_By__c,Type__c,Contact.Name,CaseNumber FROM Case LIMIT 10]; 
       	Test.startTest();
		
			CaseSearchResultsController caseSearchResultsController = new CaseSearchResultsController();
			System.assertEquals(10, caseListTest.size());
          	
          	CaseSearchResultsController.CaseCollection = null;
          	CaseSearchResultsController.CaseCollection = caseListTest;
			
          	List<CaseSearchResultsController.RowItem> riList = caseSearchResultsController.getCases();			
			System.assertEquals(10,riList.Size());
          	
          	caseSearchResultsController.CaseId = caseListTest[0].Id; 			
          	PageReference pageRef;
			pageRef = caseSearchResultsController.GetCaseID();          
			System.assertEquals(TRUE, pageRef.getUrl().contains(caseSearchResultsController.CaseId));
			
          	caseSearchResultsController.CaseId = caseListTest[1].Id; 
			pageRef = caseSearchResultsController.LoadCase();          	
			System.assertEquals(TRUE,pageRef.getUrl().contains(caseSearchResultsController.CaseId));

		Test.stopTest();
          
    }


	/**
	 * Test DataTablePaginator functions.
	 */
	 
    static void testDataTablePaginatorFunctions() {
       	
		Case c;
		User sysAdmin 			= UnitTestDataFactory.getSystemAdministrator('admin1');
		User backOfficeUser 	= UnitTestDataFactory.getBackOfficeUser('bo', 'JL.com - Bookings');
		
		insert sysAdmin;	
		insert backOfficeUser;

        System.runAs(sysAdmin) {
            UnitTestDataFactory.initialiseContactCentreAndTeamSettings();   
            jl_runvalidations__c jrv = new jl_runvalidations__c(Name = backOfficeUser.Id, Run_Validations__c = true);
            insert jrv;             
        }


        System.runAs(backOfficeUser) {
        
	       	UnitTestDataFactory.initialiseContactCentreAndTeamSettings();             	
	       	Account testAccount = UnitTestDataFactory.createAccount(1, true);
	       	Contact testCon = UnitTestDataFactory.createContact(1, testAccount.id, true);
	       	List<Case> caseList = new List<Case>();
	       	for (Integer i = 1; i <= 10; i++) {
		       	Case testCase = UnitTestDataFactory.createQueryCase(testCon.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, false);		       	
		       	testCase.Create_Case_Activity__c = true;
                caseList.add(testCase);
	       	}
	       	insert caseList;
       	} 


       	List<Case> caseListTest = [select Id, CaseNumber FROM Case LIMIT 10]; 
       	PageReference pageRef = new PageReference('/apex/CustomerContact');
		Test.setCurrentPageReference(pageRef);
		CaseSearchResultsController csrController = new CaseSearchResultsController();				
		
		Test.startTest();
			csrController.CaseCollection = caseListTest;
			List<CaseSearchResultsController.RowItem> riList = csrController.getCases();			
		Test.stopTest();
	}
}