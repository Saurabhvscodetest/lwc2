public class createCase {
	@AuraEnabled
    public static Map<string,string> createCasewithLWC(string description,string customerid, string branch, string contactreason, string reasondetail){
       long startTime = system.currentTimeMillis();
        Map<String,String> returnMap = new Map<String,String>();

        Case caseObject = new case(Description=description,ContactId=customerid,jl_Branch_master__c=branch,Contact_Reason__c=contactreason,Reason_Detail__c=reasondetail);
        insert caseObject;
         returnMap.put('CaseId',String.valueOf(caseObject.Id));
         returnMap.put('redirectURL',String.format(Label.CaseRedirectionLEXURL,new List<String>{String.valueOf(caseObject.Id)}));
			long forEndTime = system.currentTimeMillis();
        	System.debug('Test.For.availabilityList.endTime:'+forEndTime); 
            System.debug('Test.For.availabilityList.duration:'+(forEndTime-StartTime));
    
    return returnMap;
    }
}