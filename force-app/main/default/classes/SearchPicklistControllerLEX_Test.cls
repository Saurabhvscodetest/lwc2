@istest
public class SearchPicklistControllerLEX_Test {
    
    @isTest static void testCallLogSearch() {
        Test.startTest();
        String searchResultJSON = SearchPicklistControllerLEX.getSearchResults('Refunds','Call Log');
        searchResultJSON = SearchPicklistControllerLEX.getSearchResults('JL.Com After Sales JL.Com Remove Services','Call Log');
        Test.stopTest();
    }

     @isTest static void testNoCallCaseCreation() {
        
        Test.startTest();
        String searchResultJSON = SearchPicklistControllerLEX.getSearchResults('No Call Case Creation','Call Log');
        Test.stopTest();
    }

     @isTest static void testOxfordRefund() {

        Test.startTest();
        String searchResultJSON = SearchPicklistControllerLEX.getSearchResults('Oxford Street Refund Catering','Case Assignment');
        Test.stopTest();
    }

     @isTest static void testCaseCategorization() {

        Test.startTest();        
        String searchResultJSON = SearchPicklistControllerLEX.getSearchResults('John Lewis Finance','Case Categorization');
        Test.stopTest();
    }
}