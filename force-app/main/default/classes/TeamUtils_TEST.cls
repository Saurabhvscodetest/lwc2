/******************************************************************************
* @author       CarMatPovey
* @date         9 Nov 2016
* @description  Tests TeamUtils methods no covered in Case tests.
*				Renamed from original clsTeam_TEST class.
*
* EDIT RECORD
*
* LOG     DATE        Author	JIRA		COMMENT      
* 001     9 Nov 2016  MP		COPT-656	Copied from clsTeam_TEST
*
*******************************************************************************
*/
@isTest
private class TeamUtils_TEST {
    
    /*@testSetup static void setup() {
UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
}*/
    
    /* 
This test uses the data that is created by the setup Method
It relies on the queue and Team created 
*/
    static testMethod void getQueueId_getNormalQueueIdFromWarningQueueId() {
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.CREATE_RELATED_QUEUES);
        
        Team__c teamObj = Team__c.getInstance(TEAM_NAME);  
        
        QueueSobject expectedQueueSobject = [SELECT Queue.Id, Queue.DeveloperName FROM QueueSobject Where Queue.DeveloperName =:teamObj.Queue_Name__c LIMIT 1];
        system.assert(expectedQueueSobject != null, 'This Queue should exist in the org please check :- ' + teamObj.Queue_Name__c );    	 
       	//Commented and modified as a part of COPT-3585
        //QueueSobject warningQueueSobject = [SELECT Queue.Id, Queue.DeveloperName FROM QueueSobject Where Queue.DeveloperName =: (teamObj.Queue_Name__c + TeamUtils.SEPARATOR +TeamUtils.QUEUE_TYPES.WARNING) LIMIT 1];
        //system.assert(warningQueueSobject == null, 'This Queue should exist in the org please check :- ' + (teamObj.Queue_Name__c + TeamUtils.SEPARATOR +TeamUtils.QUEUE_TYPES.WARNING )); 
        
        Id actualQueueId = null;        
        Test.startTest();
        actualQueueId = TeamUtils.getQueueId(expectedQueueSobject.Queue.Id, TeamUtils.QUEUE_TYPES.DEFAULT_QUEUE);
        Test.stopTest();
        
        system.assert(actualQueueId != null, TeamUtils.QUEUE_TYPES.DEFAULT_QUEUE.name() + 'queueId should not be null for' +expectedQueueSobject.Queue.Id );
        system.assertEquals(expectedQueueSobject.Queue.Id,  actualQueueId,  'Both the queuid should match ' );    	 
    }
    
    
    /* 
This test uses the data that is created by the setup Method
It relies on the queue and Team created 
*/
    static testMethod void getQueueId_getWarningQueueIdFromNormalQueueId() {      
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.CREATE_RELATED_QUEUES);
        
        Team__c teamObj = Team__c.getInstance(TEAM_NAME);    
        QueueSobject expectedQueueSobject = [SELECT Queue.Id, Queue.DeveloperName FROM QueueSobject Where Queue.DeveloperName =:teamObj.Queue_Name__c LIMIT 1];
        system.assert(expectedQueueSobject != null, 'This Queue should exist in the org please check :- ' + teamObj.Queue_Name__c );    	 
        //Commented and modified as a part of COPT-3585
        //QueueSobject warningQueueSobject = [SELECT Queue.Id, Queue.DeveloperName FROM QueueSobject Where Queue.DeveloperName =: (teamObj.Queue_Name__c + TeamUtils.SEPARATOR +TeamUtils.QUEUE_TYPES.WARNING) LIMIT 1];
        //system.assert(warningQueueSobject != null, 'This Queue should exist in the org please check :- ' + (teamObj.Queue_Name__c + TeamUtils.SEPARATOR +TeamUtils.QUEUE_TYPES.WARNING )); 
        
        Id actualQueueId = null;        
        Test.startTest();        
        actualQueueId = TeamUtils.getQueueId(expectedQueueSobject.Queue.Id, TeamUtils.QUEUE_TYPES.DEFAULT_QUEUE);
        Test.stopTest();        
        
        system.assert(actualQueueId != null, TeamUtils.QUEUE_TYPES.DEFAULT_QUEUE.name() +'queueId should not be null for' +expectedQueueSobject.Queue.Id );
        system.assertEquals(expectedQueueSobject.Queue.Id,  actualQueueId,  'Both the queuid should match ' );    	 
    }
    
    
    /* 
This test uses the data that is created by the setup Method
It relies on the queue and Team created 
*/
    static testMethod void getTeamForQueueId_shouldGettheTeamForTheInitailQueue() {      
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
        Team__c teamObj = Team__c.getInstance(TEAM_NAME); 
        
        QueueSobject expectedQueueSobject = [SELECT Queue.Id, Queue.DeveloperName FROM QueueSobject Where Queue.DeveloperName =:teamObj.Queue_Name__c LIMIT 1];
        system.assert(expectedQueueSobject != null, 'This Queue should exist in the org please check :- ' + teamObj.Queue_Name__c );  
        Team__c actualTeam = null;
        Test.startTest();        
        actualTeam = TeamUtils.getTeamForQueueId(expectedQueueSobject.Queue.Id);
        Test.stopTest();        
        system.assert(actualTeam != null, 'Team must not be null for QID ' +expectedQueueSobject.Queue.Id );
        system.assertEquals(teamObj.name , actualTeam.name  ,'Expected Team should be retrieved for ' + TEAM_NAME); 
    }
    
    
    /* 
This test uses the data that is created by the setup Method
It relies on the queue and Team created 
*/
    static testMethod void getTeamForQueueId_shouldGettheTeamForTheRealtedQueue() {      
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        UnitTestDataFactory.setUpTeam(TEAM_NAME, UnitTestDataFactory.CREATE_RELATED_QUEUES);
        
        Team__c teamObj = Team__c.getInstance(TEAM_NAME);    
        QueueSobject expectedQueueSobject = [SELECT Queue.Id, Queue.DeveloperName FROM QueueSobject Where Queue.DeveloperName =:teamObj.Queue_Name__c LIMIT 1];
        system.assert(expectedQueueSobject != null, 'This Queue should exist in the org please check :- ' + teamObj.Queue_Name__c );    	 
        //Commented and modified as a part of COPT-3585
        //QueueSobject warningQueueSobject = [SELECT Queue.Id, Queue.DeveloperName FROM QueueSobject Where Queue.DeveloperName =: (teamObj.Queue_Name__c + TeamUtils.SEPARATOR +TeamUtils.QUEUE_TYPES.WARNING) LIMIT 1];
        //system.assert(warningQueueSobject != null, 'This Queue should exist in the org please check :- ' + (teamObj.Queue_Name__c + TeamUtils.SEPARATOR +TeamUtils.QUEUE_TYPES.WARNING )); 
        
        Team__c actualTeam = null;
        Test.startTest();        
        actualTeam = TeamUtils.getTeamForQueueId(expectedQueueSobject.Queue.Id);
        Test.stopTest();        
        system.assert(actualTeam != null, 'Team must not be null for QID ' + expectedQueueSobject.Queue.Id );
        system.assertEquals(TEAM_NAME , actualTeam.name  ,'Expected Team should be retrieved for ' + (expectedQueueSobject.queue.DeveloperName)); 
    }
    
    
    static testMethod void test_concatenateStrings() {
        List<String> lists = new List<String>{'a','b','c'};
            
            Test.startTest();        
        String s = TeamUtils.concatenateStrings(lists, ',');
        Test.stopTest();        
        
        String sExpected = 'a,b,c';
        
        System.assertEquals(sExpected, s);
    }
    
    static testMethod void test_getAllPrimaryTeamNames() {
        Test.startTest();
        Set <String> allPTNames = TeamUtils.getAllPrimaryTeamNames();
        Map <String, Id> ptGroup2IDMap = TeamUtils.getPrimaryTeamNamesToGroupMap();
        Test.stopTest();
        system.assert (ptGroup2IDMap.size() > 0 && ptGroup2IDMap.size() <= allPTNames.size(), 'Check number of Primary Team Public Groups is <= number of entries in User.PrimaryTeam picklist ' );
    }
    
    
    
    /* 
This test uses the data that is created by the setup Method
It relies on the queue and Team created 
*/
    static testMethod void getTeamForQueueIds() {      
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        String ANOTHER_TEAM_NAME = TEAM_NAME+ 'A';
        UnitTestDataFactory.setUpTeams(new List<String>{TEAM_NAME, ANOTHER_TEAM_NAME}, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
        
        Map<Id, String> queueIdToNameMap = new Map<Id, String>();
        List<String> queueNameList = new List<String>{Team__c.getInstance(TEAM_NAME).Queue_Name__c, Team__c.getInstance(ANOTHER_TEAM_NAME).Queue_Name__c};
            Map<Id, QueueSobject> mapQueueSobject = new Map<Id, QueueSobject>([SELECT Queue.Id, Queue.DeveloperName FROM QueueSobject Where Queue.DeveloperName IN :queueNameList]);
        
        for(QueueSobject qObj: mapQueueSobject.values()){
            queueIdToNameMap.put(qObj.Queue.Id, qObj.Queue.DeveloperName);             
        }
        
        Map<Id, Team__c> queueIDToTeamMap = null;
        Test.startTest(); 
        queueIDToTeamMap = TeamUtils.getTeamForQueueIds(queueIdToNameMap.keySet());
        Test.stopTest();        
        system.assertEquals( queueIdToNameMap.size(), queueIDToTeamMap.size()  ,'Team Map must match'); 
        for(Id qId:queueIdToNameMap.keySet()){
            Team__c teamObj =   queueIDToTeamMap.get(qId);
            system.assert(teamObj != null, 'Team must not be null for QID ' +qId );
            system.assertEquals( queueIdToNameMap.get(qId),teamObj.Queue_Name__c  ,'QueueName must match'); 
        }
    }

    /* 
This test uses the data that is created by the setup Method
It relies on the queue and Team created 
*/
    static testMethod void getTeamForUserIds() {      
        Integer randomNo = UnitTestDataFactory.getRandomNumber();
        String TEAM_NAME = 'TESTTEAM' + randomNo;
        String ANOTHER_TEAM_NAME = TEAM_NAME+ 'A';
        UnitTestDataFactory.setUpTeams(new List<String>{TEAM_NAME, ANOTHER_TEAM_NAME}, UnitTestDataFactory.DO_NOT_CREATE_RELATED_QUEUES);
        
        User backOfficeUser1 	= UnitTestDataFactory.getBackOfficeUser('backOfficeUser1',TEAM_NAME);
        User backOfficeUser2 	= UnitTestDataFactory.getBackOfficeUser('backOfficeUser2',ANOTHER_TEAM_NAME);
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        System.runAs(runningUser) { // Avoid MIX_DML
            insert (new List<User>{backOfficeUser1, backOfficeUser2});
        }
        
        Map<Id, String> expectedUserIdToTeamNameMap = new Map<Id, String>();
        expectedUserIdToTeamNameMap.put(backOfficeUser1.Id, TEAM_NAME);             
        expectedUserIdToTeamNameMap.put(backOfficeUser2.Id, ANOTHER_TEAM_NAME);             
        
        Map<Id, Team__c> actualUserIdToTeamMap = new Map<Id, Team__c>();
        Test.startTest();        
        actualUserIdToTeamMap = TeamUtils.getTeamForUserIds(expectedUserIdToTeamNameMap.keySet());
        Test.stopTest();        
        system.assertEquals( expectedUserIdToTeamNameMap.size(), actualUserIdToTeamMap.size()  ,'Team Map must match'); 
        for(Id userId:actualUserIdToTeamMap.keySet()){
            Team__c teamObj =   actualUserIdToTeamMap.get(userId);
            system.assert(teamObj != null, 'Team must not be null for User ID ' + userId );
            system.assertEquals( expectedUserIdToTeamNameMap.get(userId),teamObj.Name  ,'TeamName must match'); 
        }
    }
    
    /**
* @description  Test a case is correctly assign to Customer Delivery Resolution team when transferring a case to CDH            
* @author       @stuartbarber   
*/
    
    @isTest static void caseCorrectlyAssignedToCustomerDeliveryResolutionTeam() {
        
        String CDR_TEAM_NAME = 'Customer Delivery Resolution';
        
        UnitTestDataFactory.setUpTeam(CDR_TEAM_NAME, false);
        UnitTestDataFactory.createConfigSettings();
        UnitTestDataFactory.createCaseRoutingCategorisation('Delivery/Collection_GVF - Delivery/Collection Failure', 'Delivery/Collection', 'GVF - Delivery/Collection Failure', CDR_TEAM_NAME, 'Complaint');
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        Contact testContact;
        Case testCase;
        
        System.runAs(runningUser) {
            Test.startTest();
            testContact = UnitTestDataFactory.createContact();
            testContact.Email = 'stuart.little@test.com';
            insert testContact;
            
            testCase = UnitTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            testCase.jl_Branch_master__c = 'Aberdeen';
            testCase.CDH_Site__c = 'Altens';
            testCase.Contact_Reason__c = 'Delivery/Collection';
            testCase.Reason_Detail__c = 'GVF - Delivery/Collection Failure';
            insert testCase;
            
            Group grp = new Group();
            grp.Name = 'Customer Delivery Resolution';
            //grp.OwnerId = runningUser.Id;
            insert grp;
            
            Case savedCase = [SELECT Id, OwnerId, Assign_to__c FROM Case WHERE Id = :testCase.Id];
            testCase.CDH_Site__c = 'Ashdown Park';
            testCase.jl_Transfer_case__c = TRUE;
            testCase.jl_Case_RestrictedAddComment__c = 'Test Comment';
            update testCase;
            
            Case updatedCase = [SELECT Id, OwnerId, Assign_to__c FROM Case WHERE Id = :testCase.Id];
            
            Test.stopTest();
            
        }
    }
}