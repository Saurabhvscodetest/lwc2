/******************************************************************************
* @author       CarMatPovey
* @date         10 Nov 2016
* @description  Tests SLAUtils methods no covered in Case tests.
*				Renamed from original clsSLA_TEST class.
*
* EDIT RECORD
*
* LOG     DATE        Author	JIRA		COMMENT      
* 001     10 Nov 2016  MP		COPT-655	Copied from clsSLA_TEST
*
*******************************************************************************
*/
@isTest
private class SLAUtils_TEST {
	
	@testSetup static void setup() {
         UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
    }

	static testMethod void test_createDateTime() {
		Date d = Date.newInstance(2014, 1, 1);
		String t = '09-30';
        
		Test.startTest();
		DateTime dt = SLAUtils.createDateTime(d, t);
		Test.stopTest();
        
		DateTime dtExpected = DateTime.newInstance(2014, 1, 1, 9, 30, 59);

		System.assertEquals(dtExpected, dt);
	}

    @IsTest
    public static void setFirstResponseAt_Valid() {
    	UnitTestDataFactory.setRunValidationRules(false);
    	UnitTestDataFactory.createCaseActionsConfigSettings();

		date myDate = date.newInstance(2014, 2, 17);
		datetime myDateTime = SLAUtils.CURRENT_DATE_TIME;

		Case newCase = UnitTestDataFactory.createNormalCase(new Contact());
		newCase.SuppliedEmail = 'a@bb.co.uk';
		newCase.Origin = 'Email2Case';
		newCase.jl_Action_Taken__c = 'Customer contacted';
		newCase.jl_Next_Response_Date__c = myDate;
		newCase.jl_Next_Response_Time__c = '20:30';
		
		insert newCase;

		Test.startTest();
		SLAUtils.setFirstResponseAt(newCase); 
		Test.stopTest();

		Case testCase = [SELECT Id, jl_First_Response_At__c FROM Case WHERE Id = :newCase.Id];

		System.assertEquals(myDateTime, testCase.jl_First_Response_At__c);
    }

    @IsTest
    public static void setFirstResponseAt_Invalid() {
    	UnitTestDataFactory.setRunValidationRules(false);
    	UnitTestDataFactory.createCaseActionsConfigSettings();

		date myDate = date.newInstance(2014, 2, 17);

		Case newCase = UnitTestDataFactory.createNormalCase(new Contact());
		newCase.SuppliedEmail = 'a@bb.co.uk';
		newCase.Origin = 'Email2Case';
		newCase.jl_Action_Taken__c = 'New case created';
		newCase.jl_Next_Response_Date__c = myDate;
		newCase.jl_Next_Response_Time__c = '20:30';
		
		insert newCase;

		Test.startTest();
		SLAUtils.setFirstResponseAt(newCase); 
		Test.stopTest();

		Case testCase = [SELECT Id, jl_First_Response_At__c FROM Case WHERE Id = :newCase.Id];

		System.assertEquals(null, testCase.jl_First_Response_At__c);
    }

    @IsTest
	public static void testNextResponse1() {
		UnitTestDataFactory.setRunValidationRules(false);

		date myDate = date.newInstance(2014, 2, 17);
		datetime myDateTime = datetime.newInstance(2014, 2, 17,15,30, 59);
        
		Case c = UnitTestDataFactory.createNormalCase(new Contact());
		c.SuppliedEmail = 'a@bb.co.uk';
		c.Origin = 'Email2Case';
		c.jl_Next_Response_Date__c = myDate;
		c.jl_Next_Response_Time__c = '15:30';
		insert c;

		Test.startTest();
		SLAUtils.setNextResponse(c); 
		Test.stopTest();

		Case testCase = [SELECT Id, jl_Next_Response_Date__c, jl_First_Response_By__c, jl_Next_Response_By__c FROM Case WHERE Id = :c.Id];        

//      System.assertEquals(testCase.jl_Next_Response_Date__c, testCase.jl_First_Response_By__c);
		System.assertEquals(myDateTime, c.jl_Next_Response_By__c);
		System.assertEquals(myDateTime, c.jl_First_Response_By__c);
	}

	@IsTest
	public static void testNextResponse2() {
		UnitTestDataFactory.setRunValidationRules(false);

		date myDate = date.newInstance(2014, 2, 17);
		datetime myDateTime = datetime.newInstance(2014, 2, 17, 15, 30, 59);
		datetime myFirstDateTime = datetime.newInstance(2014, 1, 17, 15, 30, 0);

		Case c = UnitTestDataFactory.createNormalCase(new Contact());
		c.SuppliedEmail = 'a@bb.co.uk';
		c.Origin = 'Email2Case';
		c.jl_First_Response_At__c = myFirstDateTime;
		c.jl_Next_Response_Date__c = myDate;
		c.jl_Next_Response_Time__c = '15:30';
		c.OwnerId = UnitTestDataFactory.findACaseQueue().QueueId;
		insert c;

		Test.startTest();
		SLAUtils.setNextResponse(c); 
		Test.stopTest();

		Case testCase = [SELECT Id, jl_Next_Response_Date__c, jl_First_Response_By__c, jl_Next_Response_By__c FROM Case WHERE Id = :c.Id];

//      System.assertEquals(testCase.jl_Next_Response_Date__c,testCase.jl_First_Response_By__c);
		System.assertEquals(myDateTime,c.jl_Next_Response_By__c);
	}

    static testMethod void testAddBusinessMinutes() {

		List<BusinessHours> listbh = [SELECT Id, Name, TimeZoneSidKey, 
											 MondayStartTime, MondayEndTime,
											 TuesdayStartTime, TuesdayEndTime,
											 WednesdayStartTime, WednesdayEndTime,
											 ThursdayStartTime, ThursdayEndTime,
											 FridayStartTime, FridayEndTime,
											 SaturdayStartTime, SaturdayEndTime,
											 SundayStartTime, SundayEndTime
									  FROM BusinessHours
									  WHERE IsActive = true AND Name = 'Branch NEWBURY'];
		
		if (listbh.isEmpty()) {
			listbh = [SELECT Id, Name, TimeZoneSidKey,
							 MondayStartTime, MondayEndTime,
							 TuesdayStartTime, TuesdayEndTime, 
							 WednesdayStartTime, WednesdayEndTime,
							 ThursdayStartTime, ThursdayEndTime, 
							 FridayStartTime, FridayEndTime,
							 SaturdayStartTime, SaturdayEndTime,
							 SundayStartTime, SundayEndTime 
					  FROM BusinessHours
					  WHERE IsActive = true];			
		}

		BusinessHours bh = listbh[0];

		DateTime dt = DateTime.newInstance(2014, 7, 21, 3, 49, 0);
		Date d = Date.newInstance(2014, 7, 21);
		
		DateTime opening = DateTime.newInstance(d, bh.MondayStartTime);
		// manually add 59s padding
		Datetime paddedOpeningHours = Datetime.newInstance(opening.year(), opening.month(), opening.day(),
												opening.hour(), opening.minute(), 59);

		Datetime sla = SLAUtils.addBusinessMinutes (dt, 60, 'Branch NEWBURY');

	//	System.assertEquals(paddedOpeningHours.addMinutes(60),sla);
	}
}