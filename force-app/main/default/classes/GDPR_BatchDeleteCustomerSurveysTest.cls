/* Description  : Test Class for GDPR - Customer Survey(VOC) Data Deletion
*
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   26/12/2018        Chanchal Ghodki                  Created                COPT-4278
*
*/
@isTest
Public class GDPR_BatchDeleteCustomerSurveysTest {
    
    static testmethod void CustomerSurveyInsert(){
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        Case testCase;
        Contact testContact;
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;       
            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        System.runAs(testUser) {
            
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;
            
            testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            insert testCase;
            Test.startTest();
            List<Customer_Survey__c>  csInsert = New List<Customer_Survey__c>();
            for(integer i=0; i<300; i++){
                csInsert.add(new Customer_Survey__c(Case__c = testCase.id));
            }
            insert csInsert;  
            system.assertEquals(300, csInsert.size());
            Database.BatchableContext BC;
            GDPR_BatchDeleteCustomerSurveys obj=new GDPR_BatchDeleteCustomerSurveys();
            Database.DeleteResult[] Delete_Result = Database.delete(csInsert, false);
            obj.start(BC);
            obj.execute(BC,csInsert);
            obj.finish(BC);
            DmlException expectedException;
            try{
                delete csInsert;
            }
            catch (DmlException dException) {
            expectedException = dException; 
            system.assertNotEquals(NULL, expectedException);
        } 
            Test.stopTest();
            
        }  
    }
    
     @isTest
    Public static void testSchedule(){
        Test.startTest();
        ScheduleBatchDeleteCustomerSurveys obj = NEW ScheduleBatchDeleteCustomerSurveys();
        obj.execute(null);  
        Test.stopTest();
    }
 
}