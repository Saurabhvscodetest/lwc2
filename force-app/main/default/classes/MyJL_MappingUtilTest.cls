/**
* See the Apex Language Reference for more information about Testing and Code Coverage. 
*/
@isTest
private class MyJL_MappingUtilTest {
    
    static testMethod void testgetCacheLoyaltyAccounts() {
        MyJL_MappingUtil.resetCacheLoyaltyAccountMap();
        
        List<contact> cpList = new List<contact>();
        cpList.add(new contact(email='e0@email.com'));
        
        Set<String> emset1 = MyJL_MappingUtil.makeEmailSet(cpList);
        system.assertEquals(1, emSet1.size());
        system.assertEquals(true, emSet1.contains('e0@email.com'));
        
        // what happens if we query and we don't have any accounts
        List<Loyalty_Account__c> la1 = MyJL_MappingUtil.getCacheLoyaltyAccounts(emset1);
        system.assertEquals(0, la1.size(),'Expecting an empty list, when have zero LA in db');
        
        // Create some LoyaltyAccounts
        List<Loyalty_Account__c> las = new List<Loyalty_Account__c>();
        for (Integer i=0; i < 20; i++) {
            las.add(new Loyalty_Account__c(email_address__c='e'+i+'@email.com'));
        }
        insert las;
        
        // What happens if we query with an empty set?
        Set<String> emset2 = new Set<String>();
        List<Loyalty_Account__c> la2 = MyJL_MappingUtil.getCacheLoyaltyAccounts(emset2);
        system.assertEquals(0, la2.size(),'Expecting an empty list, when we have no such LA in DB');
        
        // now lets try one row that we now exists
        List<Loyalty_Account__c> la3 = MyJL_MappingUtil.getCacheLoyaltyAccounts(emset1);
        system.assertEquals(1, la3.size(),'Expecting a list of 1');
        system.assertEquals(las[0].Id, la3[0].Id);
        
        // do it again
        List<Loyalty_Account__c> la4 = MyJL_MappingUtil.getCacheLoyaltyAccounts(emset1);
        system.assertEquals(1, la4.size(),'Expecting a list of 1');
        system.assertEquals(las[0].Id, la4[0].Id);
        
        // now get some more
        emset1.add('e1@email.com');
        emset1.add('e5@email.com');
        emset1.add('e11@email.com');
        
        List<Loyalty_Account__c> la5 = MyJL_MappingUtil.getCacheLoyaltyAccounts(emset1);
        system.assertEquals(4, la5.size(),'Expecting a list of 4');
        Map<String, Loyalty_Account__c> laMap5 = MyJL_MappingUtil.getCacheLoyaltyAccountMap(la5);
        system.assertEquals(4, laMap5.size(),'Expecting a map of 4');
        system.assertEquals(las[0].Id, ((Loyalty_Account__c)laMap5.get('e0@email.com')).Id);
        system.assertEquals(las[1].Id, ((Loyalty_Account__c)laMap5.get('e1@email.com')).Id);
        system.assertEquals(las[5].Id, ((Loyalty_Account__c)laMap5.get('e5@email.com')).Id);
        system.assertEquals(las[11].Id, ((Loyalty_Account__c)laMap5.get('e11@email.com')).Id);
        
        // now get some new ones and some that we just did (so we now have e0, e5, e6, e7,e8)
        emset1.remove('e1@email.com');
        emset1.remove('e11@email.com');
        emset1.add('e6@email.com');
        emset1.add('e7@email.com');
        emset1.add('e8@email.com');
        
        List<Loyalty_Account__c> la6 = MyJL_MappingUtil.getCacheLoyaltyAccounts(emset1);
        system.assertEquals(5, la6.size(),'Expecting a list of 5');
        
        //system.assertEquals(x,y,'Expected only X SOQL calls');
    }
    
    private static testMethod void testPopulateCustomerPreferenceForVoucherPreference(){
        Loyalty_Account__c account = new Loyalty_Account__c(Voucher_Preference__c = 'Paper');
        List<SFDCMyJLCustomerTypes.CustomerPreference> result = MyJL_MappingUtil.populateCustomerPreferenceForVoucherPreference(account);
        System.assertEquals(1, result.size(),'Expecting a list of 1');
        
    }
    
    private static testMethod void testPopulateCustomerPreferenceForVoucherPreferenceExpectNullValue(){
        Loyalty_Account__c account = new Loyalty_Account__c();
        List<SFDCMyJLCustomerTypes.CustomerPreference> result = MyJL_MappingUtil.populateCustomerPreferenceForVoucherPreference(account);
        System.assertEquals(null, result, 'Should have returned null');
    }
    
    private static testMethod void testGetLoyaltyAccountCards(){
        BaseTriggerHandler.addSkipReason('JoinNotification', 'disabled during test data preparation');
        Contact contact = new Contact(LastName = 'Test', SourceSystem__c='johnlewis.com');
        Database.insert(contact);
        
        Loyalty_Account__c account = new Loyalty_Account__c(IsActive__c = true, ShopperId__c='shopper1',Voucher_Preference__c = 'Digital', activation_date_time__c= system.now(), Contact__c = contact.Id);
        Database.insert(account);
        Set<Id> accounts = new Set<Id>();
        accounts.add(account.Id);
        
        Loyalty_Card__c myJLCard = new Loyalty_Card__c(Loyalty_Account__c = account.Id, Name = 'myJL-card-id', Card_Type__c = MyJL_Const.MY_JL_CARD_TYPE, Pack_Type__c = MyJL_Const.PACK_TYPE_PHYSICAL);
        Database.insert(myJLCard);
        
        Map<String, Loyalty_Card__c> returnedMap = MyJL_MappingUtil.getLoyaltyAccountCards(accounts);
        String key = new List<String> (returnedMap.keySet()).get(0); 
        
        System.assertEquals(MyJL_Const.MY_JL_CARD_TYPE, key);
        System.assertEquals(MyJL_Const.PACK_TYPE_PHYSICAL, returnedMap.get(MyJL_Const.MY_JL_CARD_TYPE).Pack_Type__c );
    }
    
}