@IsTest
public class DeliveryTrackingVOUserInputMockTest {
	public static String ORDER_ID = '333';
	public static String DELIVERY_ID = '333';
	public static String TRACKING_ID = '333';
	
	@isTest static void getReturnsTrackingInfo() {
		DeliveryTrackingVOUserInputMock mock = new DeliveryTrackingVOUserInputMock();
		DeliveryTrackingVO result = mock.getReturnsTrackingInfo(ORDER_ID, null);
		system.assert(result != null, 'Should return a result from a static resource');
		system.assert(result.orderId != null, 'Order Id must be populated');
	}

	@isTest static void getDeliveryTrackingInfo_UsingOrderID() {
		DeliveryTrackingVOUserInputMock mock = new DeliveryTrackingVOUserInputMock();
		DeliveryTrackingVO result = mock.getDeliveryTrackingInfo(ORDER_ID, null);
		system.assert(result != null, 'Should return a result from a static resource');
		system.assert(result.orderId != null, 'Order Id must be populated');
	}
	
	@isTest static void getDeliveryTrackingInfo_UsingDeliveryID() {
		DeliveryTrackingVOUserInputMock mock = new DeliveryTrackingVOUserInputMock();
		DeliveryTrackingVO result = mock.getDeliveryTrackingInfo(null, DELIVERY_ID);
		system.assert(result != null, 'Should return a result from a static resource');
	}
    
}