/// Bean class for Customer Order to store Order
//  and Order line item values
//
// Edit    Date        By            Comments
//  001    28/05/14    Vikram Middha    Initial Version
public with sharing class CustomerOrder {
	
	public String orderNumber {get;set;}
	public String orderStatus {get;set;}
	public String datePlaced {get;set;}
	public String totalOrderValue {get;set;}
	public String shopperId {get;set;}
	public String paymentMethod {get;set;}
	public String cardLast4Digits {get;set;}
	public String cardExpiryDate {get;set;}
	public String billingName {get;set;}
	public String shippingMethod {get;set;}
	public String expectedDeliveryDate {get;set;}
	public String mobilePhone {get;set;}
	public String errorCode {get;set;}
	public String errorMessage {get;set;}
	public List<OrderLineItem> oliList {get;set;}
	public List<OrderEmails> emailList {get;set;}
	public List<OrderComments> commentList {get;set;}

	public void addLineItem(OrderLineItem oli){
		if(oliList == null)
			oliList = new List<OrderLineItem>();
		oliList.add(oli);
	}

	public void addOrderComment(OrderComments oc){
		if(commentList == null){
			commentList = new List<OrderComments>();
		}
		commentList.add(oc);
	}

	public void addOrderEmail(OrderEmails oe){
		if(emailList == null){
			emailList = new List<OrderEmails>();
		}
		emailList.add(oe);
	}

	public class OrderLineItem {

		public String oItemId {get;set;}
		public String fullfillmentSku {get;set;}
		public String sku {get;set;}
		public String isReturnable {get;set;}
		public String quantityOrdered {get;set;}
		public String quantityReturned {get;set;}
		public String quantityShipped {get;set;}
		public String status {get;set;}
		public String title {get;set;}
		public String totalPriceIncVat {get;set;}
		public String unitPartnerDiscount {get;set;}
		public String unitPriceIncVat {get;set;}
		public String carrier {get;set;}
		public String dateShipped {get;set;}
		public String trackingNumber {get;set;}
		public String trackingUrl {get;set;}
		public String deliveryCity {get;set;}
		public String deliveryCounty {get;set;}
		public String deliveryForename {get;set;}
		public String deliveryPostcode {get;set;}
		public String deliveryStreet1 {get;set;}
		public String deliveryStreet2 {get;set;}
		public String deliveryStreet3 {get;set;}
		public String deliveryStreet4 {get;set;}
		public String delivertSurname {get;set;}
		public String deliveryTitle {get;set;}
		public String bundleItems {get;set;}
		

	}

	public Class OrderEmails {
		public String emailDate {get;set;}
		public String subject {get;set;}
		public String content {get;set;}
	}

	public Class OrderComments {
		public String orderComments {get;set;}
		public String commentTime {get;set;}
		public String enteredByName {get;set;}
		public String enteredByPosition {get;set;}
	}

	public CustomerOrder() {
		
	}
}