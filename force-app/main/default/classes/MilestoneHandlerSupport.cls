/**
* @description  Supporting methods for MilestoneHandler     
* @author       @tomcarman  
//001     ??/??/??    ??          ?????????????????????????
//002     12/06/19    RV          COPT-4681 Code Refactoring
*/

public class MilestoneHandlerSupport {

    public static Map<String, Milestone_Configuration__c> milestoneConfigurationMap = buildMilestoneConfig();

    public static Boolean PRIORITY_ASSIGNMENT_DISABLED;
    public static final String SEPARATOR = ':';
    public static final String FIRST_CONTACT_MILESTONE = System.Label.First_Contact;
    //Old name of the First conact milestone , this needs to be removed , its a temp fix for 17.7 release 
    public static final String FIRST_CONTACT_MILESTONE_OLD_ONE =    System.Label.First_Contact_Acknowledgment;
    public static final String QUERY_RESOLUTION_MILESTONE = System.Label.Query_Resolution;
    public static final String NEXT_ACTION_DATE = System.Label.Next_Action_Date;
    public static final String REOPEN_MILESTONE = System.Label.Case_Reopen;
    public static final String EMAIL_CONTACT = System.Label.Email_Contact;
    public static final String COMPLAINT_RESOLUTION_MILESTONE = System.Label.Complaint_Resolution;
    public static final String PSE_RESOLUTION_MILESTONE = System.Label.PSE_Resolution;
    public static final String NKU_RESOLUTION_MILESTONE = System.Label.NKU_Resolution;
    public static final String TRIAGE_RESOLUTION_MILESTONE = System.Label.Triage_Resolution;
    public static final String EXCEPTION_RESOLUTION_MILESTONE = System.Label.Exception_Resolution;
    public static final String NEXT_CASE_ACTIVITY = System.Label.Next_Case_Activity;
    // Added new code //<<002>>
    public static final String MILESTONE_WARNING = system.label.NextCaseActivityWarning; //<<002>> added 
    public static final String MILESTONE_VIOLATION = system.label.NextCaseActivityViolation; //<<002>> added

    /* FILTER HELPERS */


    /**
    * @description  Business logic to filter Cases owned by a Queue (as opposed to User)            
    * @author       @tomcarman
    * @param        newCases    A list of Cases 
    * @return       A list of filtered Cases
    */

    public static List<Case> filterCasesOwnedByQueue(List<Case> newCases) {

        List<Case> casesOwnedByQueue = new List<Case>();

        for(Case newCase : newCases) { // Create a list of Cases owned by a Queue
            if(CommonStaticUtils.isGroupId(newCase.OwnerId)) {
                casesOwnedByQueue.add(newCase);
            }
        }

        return casesOwnedByQueue;
    }



    /*** LOGIC HELPERS ***/


    public static String calculatePriorityForCaseWithNCA(String entitlementName, CaseMilestone nadMilestone) {

        if(nadMilestone.IsViolated) {
            return EntitlementConstants.HIGH_PRIORITY;      
          } else if(isMilestoneAtRisk(entitlementName, nadMilestone)) {
               return EntitlementConstants.MEDIUM_PRIORITY;
                // No break, incase the is a subsequent violated MS in the loop
          } else {
                return EntitlementConstants.NORMAL_PRIORITY;
          }
    }
    
    public static String calculatePriorityForCaseWithNCAV(String entitlementName, CaseMilestone nadMilestone) {

        if(nadMilestone.IsViolated) {
            return EntitlementConstants.HIGH_PRIORITY;     
        } else {
            return EntitlementConstants.ACTIONED_PRIORITY;
        }
    }
  
    //<<002>> added 
    // This Method is used to set the priority of the case 
    // For default queue it would be Medium
    // For Failed queue it would be High
    public static String calculatePriorityForCaseWithNCAWV(String entitlementName, CaseMilestone warningMilestone,CaseMilestone violationMilestone) {

        if(violationMilestone.IsViolated) {
            return EntitlementConstants.HIGH_PRIORITY;     
        }
        else if(warningMilestone.IsViolated)
        {
            return EntitlementConstants.MEDIUM_PRIORITY;     
        }
        else
        {
            return EntitlementConstants.ACTIONED_PRIORITY;
        }
    }
    public static String calculatePriorityForCaseWithoutNCA(String entitlementProcessName, Map<String, CaseMilestone> caseMilestoneMap) {

        String priorityToReturn;

        for(CaseMilestone cm : caseMilestoneMap.values()) {

            if(cm.IsViolated) {
                priorityToReturn = EntitlementConstants.HIGH_PRIORITY;
                break; // Break out of the loop - any violated MS means the Case.Priority should be high
            } else if(isMilestoneAtRisk(entitlementProcessName, cm)) {
                priorityToReturn = EntitlementConstants.MEDIUM_PRIORITY;
                // No break, incase the is a subsequent violated MS in the loop
            } else {
                priorityToReturn = EntitlementConstants.NORMAL_PRIORITY;
            }
        }

        return priorityToReturn;

    }


    /**
    * @description  Business logic to determine if a Milestone is in amber/warning status, based on thresholds set in Milestone Configuration custom setting            
    * @author       @tomcarman
    * @param        entitlementName     The name of the Entitlement against the Case, as a String
    * @param.       cm                  The CaseMilestone to be evaluated
    * @return       Boolean of if the CaseMilestone is in amber/warning status
    */

    public static Boolean isMilestoneAtRisk(String entitlementProcessName, CaseMilestone cm) {

        Milestone_Configuration__c configurationForThisMilestone = milestoneConfigurationMap.get(entitlementProcessName.toUpperCase() + SEPARATOR + cm.MilestoneType.Name.toUpperCase());

        Boolean returnVal = false;
        
        if(configurationForThisMilestone != null) {

            Integer warningThreshold = (Integer)configurationForThisMilestone.Amber_Status_Threshold__c;

            returnVal = DatetimeUtilities.getMinutesBetween(DateTime.now(), cm.TargetDate) < warningThreshold;

        }
        
        return returnVal;

    }



    /*** DATA RETRIEVE HELPERS ***/

    /**
    * @description  Retrieves Milestone Amber thresholds from a Custom Settings, and caches in a map            
    * @author       @tomcarman
    * @return       Map of Entitlement.Name + MilestonName | Config Record
    */

    public static Map<String, Milestone_Configuration__c> buildMilestoneConfig() {

        Map<String, Milestone_Configuration__c> milestoneConfigMap = new Map<String, Milestone_Configuration__c>();

        for(Milestone_Configuration__c config : Milestone_Configuration__c.getAll().values()) {
            if(config.Entitlement_Name__c != null && config.Milestone_Name__c != null) {
                milestoneConfigMap.put(config.Entitlement_Name__c.toUpperCase() + SEPARATOR + config.Milestone_Name__c.toUpperCase(), config);
            }
        }

        return milestoneConfigMap;

    }


    /**
    * @description  Utility to convert a List of CaseMilestones into a Map of Case to inner Map of MilestoneType.Name to CaseMilestone          
    * @author       @tomcarman
    * @param        casesWithMilestones     A list of Cases with related Case.CaseMilestones
    * @return       Map of Case to inner Map of MilestoneType.Name to CaseMilestone         
    */

    public static Map<Case, Map<String, CaseMilestone>> getCaseToMilestoneTypeMap(List<Case> casesWithMilestones) {

        Map<Case, Map<String, CaseMilestone>> returnMap = new Map<Case, Map<String, CaseMilestone>>();

        for(Case aCase : casesWithMilestones) {

            Map<String, CaseMilestone> milestoneTypeMap = new Map<String, CaseMilestone>();

            for(CaseMilestone cm : aCase.CaseMilestones) {
                milestoneTypeMap.put(cm.MilestoneType.Name, cm);
            }

            returnMap.put(aCase, milestoneTypeMap);

        }

        return returnMap;

    }


    /**
    * @description  A method to retrieve Cases and their related Open CaseMilestones            
    * @author       @tomcarman
    * @param        caseIdSet   A set of Case Ids to retrieve
    * @return       List of Cases with related CaseMilestones
    */

    public static List<Case> getCasesWithMilestones(Set<Id> caseIdSet){
        
        List<String> milestoneNames = new List<String>{FIRST_CONTACT_MILESTONE,FIRST_CONTACT_MILESTONE_OLD_ONE, NEXT_ACTION_DATE, REOPEN_MILESTONE, EMAIL_CONTACT, NEXT_CASE_ACTIVITY,MILESTONE_WARNING,MILESTONE_violation}; 
        
        return new List<Case>([SELECT Id, Is_Owner_A_Queue__c, Entitlement.Name, Entitlement.SlaProcess.Name, Priority, OwnerId,
                                        (SELECT Id, IsViolated, CompletionDate, TimeRemainingInMins, TargetResponseInMins, MilestoneType.Name, TargetDate 
                                         FROM CaseMilestones 
                                         WHERE CompletionDate = null 
                                         AND MilestoneType.Name IN :milestoneNames)
                                 FROM Case 
                                 WHERE Id IN :caseIdSet 
                                 AND EntitlementId != null]);
    }



    /**
    * @description  Business logic to check if Priority Case Assignement is globally enabled            
    * @author       @tomcarman
    * @return       Boolean
    */

    public static Boolean priorityAssignmentDisabled() {

        if(PRIORITY_ASSIGNMENT_DISABLED == null) {

            Config_Settings__c priorityAssignmentCustomSetting = Config_Settings__c.getInstance('PRIORITY_ASSIGNMENT_DISABLED');

            if(priorityAssignmentCustomSetting != null) {
                PRIORITY_ASSIGNMENT_DISABLED = priorityAssignmentCustomSetting.Checkbox_Value__c;
            } else {
                PRIORITY_ASSIGNMENT_DISABLED = false;
            }
        }

        return PRIORITY_ASSIGNMENT_DISABLED;

    }



    // TODO - merge back into Team Utils (changed from using ENUM to string) 
    public static Id getQueueId(Id queueId, String expectedQueueType){
        if(queueId != null || expectedQueueType != null ){
            QueueSobject queueSObj = TeamUtils.mapIdQueueSobject.get(queueId);
            if(queueSObj != null) {
                String queueName = queueSObj.Queue.DeveloperName;
                //Replace the suffix on the queue Name to get the initail queuename
                for(String qType : EntitlementConstants.QUEUE_TYPES) {
                    if(queueName.endsWithIgnoreCase(EntitlementConstants.SEPARATOR + qType)){
                        queueName = queueName.substringBeforeLast(EntitlementConstants.SEPARATOR + qType);
                        break;
                    }
                }
                queueName = (expectedQueueType == EntitlementConstants.DEFAULT_QUEUE) ? queueName : queueName + (EntitlementConstants.SEPARATOR + expectedQueueType);            
                return TeamUtils.mapQueueNameId.get(queueName);
            }
        }
        return null;    
    }
}