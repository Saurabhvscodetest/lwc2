public class MilestoneReportingTaskHelper{
	public static final String MILESTONE_REPORTING_TASK_SUBJECT = 'Milestone Reporting';
	public static final String FIRST_CONTACT_TASK_SUBJECT = 'First Contact';
	public static final String REOPEN_TASK_SUBJECT = 'REOPEN';
	public static final String NAD_TASK_SUBJECT = 'NAD';
	public static final String EMAIL_CONTACT_TASK_SUBJECT = 'Email Contact';
	public static final String QUERY_RESOLUTION_TASK_SUBJECT = 'Query Resolution';
	public static final String COMPLAINT_RESOLUTION_TASK_SUBJECT = 'Complaint Resolution';
	public static final String NKU_RESOLUTION_TASK_SUBJECT = 'NKU Resolution';
	public static final String PSE_RESOLUTION_TASK_SUBJECT = 'PSE Resolution';
	public static final String TRIAGE_RESOLUTION_TASK_SUBJECT = 'Triage Resolution';
	public static final String EXCEPTION_RESOLUTION_TASK_SUBJECT = 'Exception Resolution';
	
	@Future
	public static void createMilestoneReportingTask(Set<Id> failedMilestoneIds){
		List<CaseMilestone> violatedCaseMilestone = [SELECT Id,isViolated, CompletionDate,  case.Id, case.jl_Reopened_Count__c,  case.OwnerId, case.jl_Last_Queue__c FROM CaseMilestone WHERE Id IN :failedMilestoneIds];
		List<Task> tasksToBeInserted = new List<Task>();
		
		system.debug('+++ violatedCaseMilestone : ' + violatedCaseMilestone);

		Map<String, Task> uniqueMRTaskIdenitfierToTaskMap = new Map<String, Task>();

		for(CaseMilestone cm: violatedCaseMilestone){
			Task t = createReportingTask(cm);
			// this is replicating the same logic (formula) as the Milestone_Reporting_Key__c
			String uniqueIdentifier = getMilestoneReportingKey(cm);
			// create a map of unique identifiers and it's associated task
			uniqueMRTaskIdenitfierToTaskMap.put(uniqueIdentifier, t);
		}
		
		List<Task> tasks = [SELECT Milestone_Reporting_Key__c FROM Task WHERE Milestone_Reporting_Key__c IN :uniqueMRTaskIdenitfierToTaskMap.keySet()];
		Set<String> existingMilestoneReportingKeys = new Set<String>();
		
		for(Task t : tasks){
			existingMilestoneReportingKeys.add(t.Milestone_Reporting_Key__c);
		}

		for (String uniqueKey : uniqueMRTaskIdenitfierToTaskMap.keySet()){
			if(!existingMilestoneReportingKeys.contains(uniqueKey)){
				tasksToBeInserted.add(uniqueMRTaskIdenitfierToTaskMap.get(uniqueKey));
			}
		}
				
	}
	
	public static String convertMilestoneReportingTaskSubjectToCorrespondingMilestoneName(Task newTask){
		String result = null;
		if(newTask != null & String.isNotEmpty(newTask.Subject) ){
			if(newTask.Subject.containsIgnoreCase(REOPEN_TASK_SUBJECT)){
				result = System.Label.Case_Reopen;
			} else if (newTask.Subject.containsIgnoreCase(NAD_TASK_SUBJECT)){
				result =  System.Label.Next_Action_Date;
			} else if(newTask.Subject.containsIgnoreCase(FIRST_CONTACT_TASK_SUBJECT)){
				result = System.Label.First_Contact;
			} else if(newTask.Subject.containsIgnoreCase(EMAIL_CONTACT_TASK_SUBJECT)){
				result = System.Label.Email_Contact;
			} else if(newTask.Subject.containsIgnoreCase(QUERY_RESOLUTION_TASK_SUBJECT)){
				result = System.Label.Query_Resolution;
			} else if(newTask.Subject.containsIgnoreCase(COMPLAINT_RESOLUTION_TASK_SUBJECT)){
				result = System.Label.Complaint_Resolution;
			} else if(newTask.Subject.containsIgnoreCase(NKU_RESOLUTION_TASK_SUBJECT)){
				result =  System.Label.NKU_Resolution;
			} else if(newTask.Subject.containsIgnoreCase(PSE_RESOLUTION_TASK_SUBJECT)){
				result = System.Label.PSE_Resolution;
			} else if(newTask.Subject.containsIgnoreCase(TRIAGE_RESOLUTION_TASK_SUBJECT)){
				result = System.Label.Triage_Resolution; 
			} else if(newTask.Subject.containsIgnoreCase(EXCEPTION_RESOLUTION_TASK_SUBJECT)){
				result = System.Label.Exception_Resolution;
			}
		}				
		return result;
	}

	public static Id isCaseMilestoneViolated(String milestoneName, Case c){
		for(CaseMilestone cm : c.CaseMilestones){ 
			system.debug('milestoneName   ' + milestoneName);
			system.debug('cm.MilestoneType.Name   ' + cm.MilestoneType.Name);
			if(cm.MilestoneType.Name.equalsIgnoreCase(milestoneName)){
				return cm.Id;
				break;
			}
		}
		return null;
	} 
	
	public static Task createReportingTask(caseMilestone cm){
		system.debug('+++ cm : ' + cm);
		Task t = new Task(
			RecordTypeId = clsTaskTriggerHandler.MS_REPORTING_RECORD_TYPE_ID,
            Milestone_Id__c = cm.Id,
            whatId = cm.case.Id,
            Subject = MILESTONE_REPORTING_TASK_SUBJECT,
            Case_Owner_Id__c = cm.Case.OwnerId,
            Case_Last_Queue_Id__c = cm.Case.jl_Last_Queue__c,
            Milestone_Violated__c = cm.isViolated,
            Milestone_Completed_Date__c = cm.CompletionDate,
            Status = clsTaskTriggerHandler.TASK_CLOSED_RESOLVED_STATUS,
            Case_Reopened_Count__c = cm.case.jl_Reopened_Count__c,
            Milestone_Reporting_Key__c = getMilestoneReportingKey(cm)
    		);
		return t;
	}
	
	public static String getMilestoneReportingKey(caseMilestone cm){
		String completedDateTime = (cm.CompletionDate == null) ? '' : ('-'+ cm.CompletionDate);
		String uniqueIdentifier =  cm.Id +  '-' + cm.case.jl_Reopened_Count__c + completedDateTime;
		return uniqueIdentifier;
	}

}