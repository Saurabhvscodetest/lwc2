/*************************************************
coOrderDetailController

controller extension for the coOrderDetail page

Author: Steven Loftus (MakePositive)
Created Date: 13/11/2014
Modification Date: 
Modified By: 

**************************************************/
public with sharing class coOrderDetailController {

    


    // these link properties hold the url manipulation strings to create new cases of certain record types based on buttons on the order detail page
    public String QueryNewCaseLink {get;set;}
    public String ComplaintNewCaseLink {get;set;}

    // main property holding the order details
    public coOrderDetail Order {get; set;}
    public Contact Con {get; set;}

    // private properties
    private String orderId {get; set;}
    
    Private String var='Payment Details' ;
        public void setVar(String n) {
    var = n;
    }

    public String getVar() {
    return var;
    }

    private coSoapManager privateSoapManager;
    // soap manager
    public coSoapManager SoapManager {
        get {
            if (this.privateSoapManager == null) this.privateSoapManager = new coSoapManager();

            return this.privateSoapManager;
        }
    }
    
    public coOrderDetailController() {

        this.orderId = ApexPages.currentPage().getParameters().get('orderId');
        String customerId = ApexPages.currentPage().getParameters().get('customerId');

        system.debug('orderId [' + orderId + ']');
        system.debug('customerId [' + customerId + ']');
        this.Con = [select Id from Contact where Id = : customerId];

        // Prepare Onclick links for New Query and New Complaint buttons on OrderLineItem VF pages.
        // These links will open a new sub tab under the customer primary tab.
        Id rtQueryId = Schema.SObjectType.Case.RecordTypeInfosByName.get('Query').RecordTypeId;
        system.debug(rtQueryId);
        this.QueryNewCaseLink = '/500/e?retURL=%2F500%2Fo&ent=Case&cas3_lkid='  + this.Con.Id +  '&RecordType=' + rtQueryId + '&00Nb0000009JjpC=' + this.orderId;
        
        Id rtComplaintId = Schema.SObjectType.Case.RecordTypeInfosByName.get('Complaint').RecordTypeId;
        system.debug(rtComplaintId);
        this.ComplaintNewCaseLink = '/500/e?retURL=%2F500%2Fo&ent=Case&cas3_lkid='  + this.Con.Id +  '&RecordType=' + rtComplaintId + '&00Nb0000009JjpC=' + this.orderId;

        getTheOrder();      

        system.debug('this.Order [' + this.Order + ']');
    }

    private void getTheOrder() {

        ApexPages.getMessages().clear();

        this.Order = this.SoapManager.getOrder(this.orderId);

        // if we got an error back then tell the user
        if (soapManager.Status != 'Success') {

            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, SoapManager.ErrorDescription));
        }
    } 
    
    
    public String currentTabName;
        public String selectedTab {

            get {
                return currentTabName;                     
            }
        }
        public void setActiveTab(){
            String para = ApexPages.CurrentPage().getParameters().get('tabname');        
            
        }  
}