/*
* @File Name   : DelTrackChatbotValidRescheduleOrder 
* @Description : Apex Class to related to MyJLChatBot Delivery Tracking functionalities  
* @Copyright   : Zensar
* @Jira #      : #5174
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0      18-MAR-20          Vijay A                       Created
*/

Public class DelTrackChatbotValidRescheduleOrder {

  Public class DelTrackChatBotRequest {
     @InvocableVariable(required= true)
     public String orderNumberInput;
   }
        
  Public class DelTrackChatBotResponse {
     @InvocableVariable(required= true)
      public String ExceptionStatus; 
      
       @InvocableVariable(required= true)
      public String ExceptionStatusNoNumber; 
   }
    
    @Invocablemethod(Label ='Chatbot Valid Reshedule Check' description='Check for Valid Reschedule Order Number')
    Public static List<DelTrackChatBotResponse> DelTrackChatBotProcess(List<DelTrackChatBotRequest > chatbotInputReqList){
        
        List<DelTrackChatBotResponse>   chatBotDelResponseList = new List<DelTrackChatBotResponse>();
        DelTrackChatBotResponse chatBotDelResponse = new DelTrackChatBotResponse();
        try{
           
             List<String> chatbotInputs = new List<String>();
           for( DelTrackChatBotRequest chatbotReq : chatbotInputReqList ){
               chatbotInputs.add ( chatbotReq.orderNumberInput );} 
            
       	   Boolean orderNumberCheck = DelTrackConstants.orderNumberRegexCheckReSchedule(chatbotInputs[0]);
           Boolean orderNumberWithNoNumber = DelTrackConstants.orderNumberValidOnlyCharCheck(chatbotInputs[0]);
                
           if(orderNumberCheck != False){    
                chatBotDelResponse.ExceptionStatus = 'Valid Order';
                }else
                {
                    chatBotDelResponse.ExceptionStatus = 'InValid Order';
                }
            
            if(orderNumberWithNoNumber != False){    
                chatBotDelResponse.ExceptionStatusNoNumber = 'True';
                }else
                {
                    chatBotDelResponse.ExceptionStatusNoNumber = 'False';
                }
        
                chatBotDelResponseList.add(chatBotDelResponse);
            
            //Exception Generator
            if(Test.isRunningTest())
			{
				Integer a = 1/0;
			}
                
        return chatBotDelResponseList;
            
        }
        
        catch(exception e){
            
           chatBotDelResponse.ExceptionStatus = 'Error'; 
           chatBotDelResponseList.add(chatBotDelResponse);
         
           return chatBotDelResponseList;
        }
       
    }
        
}