/*
* @File Name   : EmailServiceUtils
* @Description : Utility class to send an email based on the template Supplier_Cancellation_Email
* @Copyright   : Zensar
* @Jira #      : #5843
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0       17-OCT-2020         Ragesh G                     Created
*   1.1       18-NOV-2020         Victor C                     Updated to remove CDATA statement from email
*/


public with sharing class EmailServiceUtils {
    
    
    public static void sendEmailMethod ( List < Case> singleSupplierCaseList, ExceptionServiceHelper.OrderDetailsWrapper orderObj ) {
        
        Messaging.SingleEmailMessage[] messageList = new List<Messaging.SingleEmailMessage> ();
        
        OrgWideEmailAddress orgWideAddressRecord = [ SELECT id, Address, DisplayName from OrgWideEmailAddress 
                                                     WHERE DisplayName='Customer Services'];
        
        
        for ( Case caseRecord : singleSupplierCaseList ) {
                        
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
            // Set from Email Address
            
            mail.setOrgWideEmailAddressId( orgWideAddressRecord.Id );
            mail.setSaveAsActivity(true);
            mail.setWhatId(caseRecord.Id);

            //set to email ID
            mail.setToAddresses( orderObj.emailAddressFullList );               
            //retrieve template
            EmailTemplate templateRecord =[SELECT Id,HTMLValue,Body,Name, Subject FROM EmailTemplate WHERE DeveloperName= 'Supplier_Cancellation_Email' LIMIT 1];
            mail.setSubject( templateRecord.Subject );
            String emailBody = templateRecord.HTMLValue;
                
            emailbody=emailBody.replace( System.Label.OE_BRANCH_NAME, caseRecord.jl_Branch_master__c );
            String itemsContent = '';
            for ( String orderDetail : orderObj.orderDetailsList ) {
                itemsContent = itemsContent + orderDetail+'\n';
            }
            emailBody =emailBody.replace( System.Label.OE_ITEM_NUMBER, itemsContent ); 
            emailBody = emailBody.replaceAll(']]>', '');
            emailBody = emailBody.replaceAll('<!\\[CDATA\\[', '');
            mail.setHtmlBody(emailBody);
                
            messageList.add ( mail );
        }
        
        Messaging.SendEmailResult[] results = Messaging.sendEmail( messageList );
    }
    
}