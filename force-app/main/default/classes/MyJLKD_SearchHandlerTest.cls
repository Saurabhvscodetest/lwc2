/**
 *  @author: Andrey Gavrikov (westbrook)
 *  @date: 2014-08-11 15:14:54 
 *  @description:
 *      Test methods for MyJLKD_SearchHandler
 *  
 *  Version History :   
 *  2014-11-10 - MJL-1276 - AG
 *  if Attachment relates to Transaction_Header__c and
 *  Transaction_Header__c now has both Full & Short XMLs then update
 *  Transaction_Header__c and set IsValid__c = true
 
  * 001     23/06/15    NA      Fixing the Test class failures
 *      
 */
@isTest
public class MyJLKD_SearchHandlerTest  {
	private static final String SHORT_XML = 'short xml';
	private static final String FULL_XML = 'full xml';
    
    private static Boolean IS_SANDBOX = [select count() from Organization where IsSandbox = true] > 0;
    
    static {
        BaseTriggerHandler.addSkipReason(CalloutHandler.SKIP_ALL_NOTIFICATIONS_CODE, 'disable outbound events in this test class');

    }
    

    /************************ DATA PREPARATION *******************************************/
    /**
     * this method will create numberOfCards Loyalty_Account__c and Loyalty_Card__c records,
     * i.e. one Loyalty_Card__c per Loyalty_Account__c
     */
    public static List<Loyalty_Card__c> generateMYJL_LoyaltyCards(final Integer numberOfCards) {
        
        final List<Loyalty_Account__c> loyaltyAccounts = new List<Loyalty_Account__c>();
        
        for(Integer i = 0; i < numberOfCards; i++) {
            loyaltyAccounts.add(new Loyalty_Account__c(Name = 'LA_' + i, Email_Address__c = 'la_' + i + '@test.com'));
        }
        Database.insert(loyaltyAccounts);
        
        final List<Loyalty_Card__c> cards = new List<Loyalty_Card__c>();
        
        for(Integer i = 0; i < numberOfCards; i++) {
            cards.add(new Loyalty_Card__c(Name = 'Card_' + i, Loyalty_Account__c = loyaltyAccounts[i].Id));
        }
        Database.insert(cards);
        
        
        ///NA -(start)(23-06-2015) fix for testCardIds () test method failure as it is expeciting Customer_Loyalty_Account_Card_ID__c(loyalty account) filled with card number
        list<Loyalty_Account__c> updatLAs = new list<Loyalty_Account__c>();
        for(Integer i = 0; i < numberOfCards; i++) {
            
            loyaltyAccounts[i].Customer_Loyalty_Account_Card_ID__c = cards[i].name; 
            updatLAs.add(loyaltyAccounts[i]);   
        }
        
        update updatLAs;
        //(End Fix)
        
        List<Loyalty_Card__c> requeryCards = [SELECT name, Id FROM Loyalty_Card__c];
        system.debug('requeryCards: '+requeryCards);
        return cards;
    }

    /**
     * create Attachment documents for given Transaction_Header__c
     * shortXmlText - if not empty then Short XML Attachment will be generated
     * fullXmlText - if not empty then Full XML Attachment will be generated
     */
    private static List<Attachment> initAttachments(final Transaction_Header__c tran, final String shortXmlText, final String fullXmlText) {
        final List<Attachment> atts = new List<Attachment>();
        
        if (!String.IsBlank(shortXmlText)) {
            Attachment att = new Attachment(ParentId = tran.Id, Name = MyJLKD_SearchHandler.getShortXmlName());
            att.Body = Blob.valueOf(shortXmlText);
            att.ContentType = 'text/xml';
            atts.add(att);
        }

        
        if (!String.IsBlank(fullXmlText)) {
            Attachment att = new Attachment(ParentId = tran.Id, Name = MyJLKD_SearchHandler.getFullXmlName());
            att.Body = Blob.valueOf(fullXmlText);
            att.ContentType = 'text/xml';
            atts.add(att);
        }
        return atts;
    }
    /************************ END DATA PREPARATION *******************************************/
    
    /**
     * MJL-729
     * check getCardIds() method
     */
    static testMethod void testCardIds () {
        final SFDCKitchenDrawerTypes.TransactionSearchCriteria searchCriteria = new SFDCKitchenDrawerTypes.TransactionSearchCriteria();
        searchCriteria.maxItems = 100;
        
        Test.starttest();
        List<Loyalty_Card__c> cards = generateMYJL_LoyaltyCards(3);
        final List<Transaction_Header__c> trans = new List<Transaction_Header__c>{
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR0_0', Card_Number__c = cards[0].Id),
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR1_0', Card_Number__c = cards[1].Id),
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR1_1', Card_Number__c = cards[1].Id),
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR2_0', Card_Number__c = cards[2].Id),
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR2_1', Card_Number__c = cards[2].Id),
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR2_2', Card_Number__c = cards[2].Id)
        };
        Database.insert(trans);
        
        
        Set<Id> cardIds = MyJLKD_SearchHandler.getCardIds(searchCriteria);
        System.assertEquals(cards.size(), cardIds.size(), 'Invalid number of Card Ids returned, expected all cards in the DB');


        //check if we can specify Membership Number (aka Loyalty_Account__c.Name)
        searchCriteria.membershipNumber = 'LA_1';
        cardIds = MyJLKD_SearchHandler.getCardIds(searchCriteria);
        
        Test.stoptest();
        System.assertEquals(1, cardIds.size(), 'Expected exactly 1 Card record attached to Loyalty_Account__c = ' + searchCriteria.membershipNumber);
        System.assert(cardIds.contains(cards[1].Id), 'Incorrect Card returned, expected one associated with Loyalty_Account__c Name=' + searchCriteria.membershipNumber);

        //check if empty is returned for non existing transactionId
        searchCriteria.membershipNumber = 'does-not-exist';
        System.assertEquals(0, MyJLKD_SearchHandler.getCardIds(searchCriteria).size(), 'Expected 0 cards because Loyalty_Account__c with given Number does not exist');

        //search by provided card Id
        searchCriteria.membershipNumber = null;
        searchCriteria.cardNumber = 'Card_1';
        cardIds = MyJLKD_SearchHandler.getCardIds(searchCriteria);
        System.assertEquals(1, cardIds.size(), 'Expected exactly 1 Card record with Name = ' + searchCriteria.cardNumber);
        System.assert(cardIds.contains(cards[1].Id), 'Incorrect Card returned, expected Card_1');
        


    }
    /**
     * MJL-729
     * check various basic search scenarios
     * 1. simpple return of all existing transactions (using date range which includes all test records)
     * 2. specify max number of records to return
     * 3. check that nothing is returned when search criteria does not match any record(s)
     */
    static testMethod void testBasicSearch_withJustMYJLCards () {

        List<Loyalty_Card__c> cards = generateMYJL_LoyaltyCards(3);
        final List<Transaction_Header__c> trans = new List<Transaction_Header__c>{
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR0_0', Card_Number__c = cards[0].Id, Transaction_Date_Time__c = System.now(), IsValid__c = true),
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR1_0', Card_Number__c = cards[1].Id, Transaction_Date_Time__c = System.now(), IsValid__c = true),
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR1_1', Card_Number__c = cards[1].Id, Transaction_Date_Time__c = System.now(), IsValid__c = true),
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR2_0', Card_Number__c = cards[2].Id, Transaction_Date_Time__c = System.now(), IsValid__c = true),
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR2_1', Card_Number__c = cards[2].Id, Transaction_Date_Time__c = System.now(), IsValid__c = true),
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR2_2', Card_Number__c = cards[2].Id, Transaction_Date_Time__c = System.now(), IsValid__c = true)
        };
        Database.insert(trans);
        
        //generate XML attachments
        final List<Attachment> atts = new List<Attachment>();
        atts.addAll(initAttachments(trans[0], SHORT_XML, FULL_XML));

        Database.insert(atts);
        
        MyJLKD_SearchHandler.TEST_DISABLE_VALIDATION = true;//disable required field validation to enable any search criteria

        SFDCKitchenDrawerTypes.RequestHeader requestHeader = new SFDCKitchenDrawerTypes.RequestHeader();
        requestHeader.messageId = 'message-1';

        //1.
        SFDCKitchenDrawerTypes.TransactionSearchCriteria searchCriteria = new SFDCKitchenDrawerTypes.TransactionSearchCriteria();
        searchCriteria.isFullXml = false;
        searchCriteria.maxItems = 100;
        searchCriteria.transactionsStartDate = System.now() - 10;
        searchCriteria.transactionsEndDate = System.now() + 10;

        //check that all records returned
        SFDCKitchenDrawerTypes.ActionResponse response = MyJLKD_SearchHandler.process(requestHeader, searchCriteria);
        System.assert(response.success, 'Call failed. ' + response.message);
        System.assertEquals(trans.size(), response.results.size(), 'Expected all transaction records in the specified date range');
        System.assertEquals(SHORT_XML, response.results[0].kdTransaction.shortXml, 'Expected short XML to be returned for transaction 0. ' + response.results[0].kdTransaction);

        //2. check if we can specify number of results to return
        searchCriteria.isFullXml = true;
        searchCriteria.maxItems = 3;
        response = MyJLKD_SearchHandler.process(requestHeader, searchCriteria);
    
        System.assertEquals(3, response.results.size(), 'Expected exactly 3 records in the specified date range');
        //check that full XML was loaded for trans[0]
        System.assertEquals(FULL_XML, response.results[0].kdTransaction.fullXml, 'Expected full XML to be returned for transaction 0');
        System.assert(String.isBlank(response.results[0].kdTransaction.shortXml), 'Expected NO Short XML to be returned for transaction 0');

        
        //3. check if empty is returned for non existing transactionId
        searchCriteria.membershipNumber = 'does-not-exist';
        response = MyJLKD_SearchHandler.process(requestHeader, searchCriteria);
        System.assertEquals(0, response.results.size(), 'Expected 0 cards because Loyalty_Account__c with given Number does not exist');

        //search with all attributes, only doing this to check that SOQL query is valid
        MyJLKD_SearchHandler.TEST_DISABLE_VALIDATION = false;//enable required field validation
        searchCriteria.cardNumber = 'Card_1';
        searchCriteria.branchNumber = 'xxx';
        searchCriteria.channel = 'xxx';
        searchCriteria.customerId = 'xxx';
        searchCriteria.email = 'xxx';
        searchCriteria.membershipNumber = 'xxx';
        searchCriteria.transactionId = 'xxx';
        searchCriteria.sortDirection = SFDCKitchenDrawerTypes.SortDirections.ASCENDING;

        requestHeader = new SFDCKitchenDrawerTypes.RequestHeader();
        requestHeader.messageId = 'message-2';
        response = MyJLKD_SearchHandler.process(requestHeader, searchCriteria);
        System.assert(response.success, 'Call failed. ' + response.message);
        System.assertEquals(0, response.results.size(), 'Expected 0 cards because criteria is too strict to find anything');
    }

    
    static testMethod void searchAllTransactions_ForBoth_MYJL_AND_PArtnership_Cards() {
		final String SHOPPER_ID = '212112121212121212121';
		final String PARTNERSHIP_CARD_NO = '1234567890123456';
		Contact con = new Contact(Shopper_ID__c = SHOPPER_ID, LastName = 'SSSS');
		Database.insert(con);
        
        Loyalty_Account__c loyaltyAcc = new Loyalty_Account__c(Name = 'LA_', Email_Address__c = 'la_' + '@test.com', Contact__c = con.Id, Customer_Loyalty_Account_Card_ID__c = null);
        Database.insert(loyaltyAcc);
        
        
        Loyalty_Card__c myJLCard = new Loyalty_Card__c(Name = 'Card_' , Loyalty_Account__c = loyaltyAcc.Id);
        Loyalty_Card__c partnershipCard = new Loyalty_Card__c(Name = PARTNERSHIP_CARD_NO, Card_Number__c = '1234567890123456', Loyalty_Account__c = loyaltyAcc.Id
        									, Card_Type__c = MyJL_Const.MY_PARTNERSHIP_CARD_TYPE, Date_Created__c = DateTime.Now(),Origin__c = 'TEST' );
        									
        Database.insert(new List<Loyalty_Card__c>{myJLCard, partnershipCard});
        
        
        loyaltyAcc.Customer_Loyalty_Account_Card_ID__c = PARTNERSHIP_CARD_NO; 
        update loyaltyAcc;
                
        
        final List<Transaction_Header__c> trans = new List<Transaction_Header__c>{
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR0_0', Card_Number__c = myJLCard.Id, Transaction_Date_Time__c = System.now(), IsValid__c = true),
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR1_0', Card_Number__c = partnershipCard.Id, Transaction_Date_Time__c = System.now(), IsValid__c = true)
        };
        Database.insert(trans);
        
        
        SFDCKitchenDrawerTypes.RequestHeader requestHeader = new SFDCKitchenDrawerTypes.RequestHeader();
        requestHeader.messageId = 'message-1';

        //1.
        SFDCKitchenDrawerTypes.TransactionSearchCriteria searchCriteria = new SFDCKitchenDrawerTypes.TransactionSearchCriteria();
        searchCriteria.customerId = SHOPPER_ID;

        //check that all records returned
        SFDCKitchenDrawerTypes.ActionResponse response = MyJLKD_SearchHandler.process(requestHeader, searchCriteria);
        System.assert(response.success, 'Call failed. ' + response.message);
        System.assertEquals(trans.size(), response.results.size(), 'Expected all transaction records in the specified date range');

    }

    /**
     * MJL-729
     * check search scenarios with offset
     * 1. simpple return of all existing transactions (using date range which includes all test records)
     * 2. specify max number of records to return
     * 3. check that nothing is returned when search criteria does not match any record(s)
     */
    static testMethod void testSearchWithOffset () {

        List<Loyalty_Card__c> cards = generateMYJL_LoyaltyCards(3);
        final List<Transaction_Header__c> trans = new List<Transaction_Header__c>{
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR0_0', Card_Number__c = cards[0].Id, Transaction_Date_Time__c = System.now(), IsValid__c = true),
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR1_0', Card_Number__c = cards[1].Id, Transaction_Date_Time__c = System.now(), IsValid__c = true),
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR1_1', Card_Number__c = cards[1].Id, Transaction_Date_Time__c = System.now(), IsValid__c = true),
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR2_0', Card_Number__c = cards[2].Id, Transaction_Date_Time__c = System.now(), IsValid__c = true),
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR2_1', Card_Number__c = cards[2].Id, Transaction_Date_Time__c = System.now(), IsValid__c = true),
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR2_2', Card_Number__c = cards[2].Id, Transaction_Date_Time__c = System.now(), IsValid__c = true)
        };
        Database.insert(trans);
        
        //generate XML attachments
        final List<Attachment> atts = new List<Attachment>();
        atts.addAll(initAttachments(trans[0], 'short xml 0_0', 'full xml 0_0'));
        atts.addAll(initAttachments(trans[1], 'short xml 1_0', 'full xml 1_0'));

        Database.insert(atts);
        
        MyJLKD_SearchHandler.TEST_DISABLE_VALIDATION = true;//disable required field validation to enable any search criteria

        final SFDCKitchenDrawerTypes.RequestHeader requestHeader = new SFDCKitchenDrawerTypes.RequestHeader();
        requestHeader.messageId = 'message-1';

        //1.
        final SFDCKitchenDrawerTypes.TransactionSearchCriteria searchCriteria = new SFDCKitchenDrawerTypes.TransactionSearchCriteria();
        searchCriteria.isFullXml = false;
        searchCriteria.maxItems = 1000;
        searchCriteria.transactionsStartDate = System.now() - 10;
        searchCriteria.transactionsEndDate = System.now() + 10;

        searchCriteria.sortByField = SFDCKitchenDrawerTypes.SortByFields.TRANSACTION_NUMBER;
        searchCriteria.recordSetStartNumber = 0;
        
        //check that all records returned
        SFDCKitchenDrawerTypes.ActionResponse response = MyJLKD_SearchHandler.process(requestHeader, searchCriteria);
        System.assertEquals(trans.size(), response.results.size(), 'Expected all transaction records in the specified date range');
        //check that returned records start with transaction 0
        System.assertEquals('short xml 0_0', response.results[0].kdTransaction.shortXml, 'Expected short XML to be returned for transaction 0. ' + response.results[0].kdTransaction);

        //2. check if we can specify number of results to return
        searchCriteria.recordSetStartNumber = 2;
        response = MyJLKD_SearchHandler.process(requestHeader, searchCriteria);

        System.assertEquals(trans.size() - 2 , response.results.size(), 'Expected all (but two) transaction records in the specified date range because offset is 2');
        System.assertEquals('TR1_1', response.results[0].kdTransaction.transactionId, 'Expected first transaction to be the one which goes third in the ordered record set. ' + response.results[0].kdTransaction);
    }


    /**
     * MJL-729
     * check search scenarios with associated transactions
     *       Associated transactions
     * T1  | 
     * T2  | T1
     * T3  | T1, T2
     */
    static testMethod void testSearchWithAssociatedTransactions () {

        List<Loyalty_Card__c> cards = generateMYJL_LoyaltyCards(3);

        Transaction_Header__c t1 = new Transaction_Header__c(Receipt_Barcode_Number__c = 'T1', Card_Number__c = cards[0].Id, Transaction_Date_Time__c = System.now(), IsValid__c = true);
        Transaction_Header__c t2 = new Transaction_Header__c(Receipt_Barcode_Number__c = 'T2', Card_Number__c = cards[1].Id, Transaction_Date_Time__c = System.now(), IsValid__c = true);
        Transaction_Header__c t3 = new Transaction_Header__c(Receipt_Barcode_Number__c = 'T3', Card_Number__c = cards[2].Id, Transaction_Date_Time__c = System.now(), IsValid__c = true);
        Database.insert(new Transaction_Header__c[] {t1, t2, t3});

        Associated_Transaction__c at2 = new Associated_Transaction__c(Transaction__c = t2.Id, Associated_Transaction__c = t1.Id);
        Associated_Transaction__c at3_1 = new Associated_Transaction__c(Transaction__c = t3.Id, Associated_Transaction__c = t1.Id);
        Associated_Transaction__c at3_2 = new Associated_Transaction__c(Transaction__c = t3.Id, Associated_Transaction__c = t2.Id);
        Database.insert(new Associated_Transaction__c[] {at2, at3_1, at3_2});

        final SFDCKitchenDrawerTypes.TransactionSearchCriteria searchCriteria = new SFDCKitchenDrawerTypes.TransactionSearchCriteria();
        searchCriteria.isFullXml = false;
        searchCriteria.transactionsStartDate = System.now() - 10;
        searchCriteria.transactionsEndDate = System.now() + 10;

        searchCriteria.sortByField = SFDCKitchenDrawerTypes.SortByFields.TRANSACTION_NUMBER;

        MyJLKD_SearchHandler.TEST_DISABLE_VALIDATION = true;//disable required field validation to enable any search criteria

        final SFDCKitchenDrawerTypes.RequestHeader requestHeader = new SFDCKitchenDrawerTypes.RequestHeader();
        requestHeader.messageId = 'message-1';
        
        SFDCKitchenDrawerTypes.ActionResponse response = MyJLKD_SearchHandler.process(requestHeader, searchCriteria);
        System.assertEquals(3, response.results.size(), 'Expected all transaction records in the specified date range');

        //check that T1 contains associated Transaction T2 & T3
        SFDCKitchenDrawerTypes.KDTransaction kdTran1 = response.results[0].kdTransaction;
        System.assertEquals('T1', kdTran1.transactionId, 'Expected T1 to be first in the list of results, because we ordered by TRANSACTION_NUMBER');
        
        Set<String> associatedTransactionIds = new Set<String>(kdTran1.associatedTransactionIds);

        System.assertEquals(2, associatedTransactionIds.size(), 'Expected T1 to contain exactly 2 associated transactions, associatedTransactionIds=' + associatedTransactionIds);
        System.assert(associatedTransactionIds.contains('T2'), 'Expected T2 to be in the list of associated transactions for T1. associatedTransactionIds=' + associatedTransactionIds);
        System.assert(associatedTransactionIds.contains('T3'), 'Expected T3 to be in the list of associated transactions for T1. associatedTransactionIds=' + associatedTransactionIds);

        //check that T2 also contains associated Transaction T3
        SFDCKitchenDrawerTypes.KDTransaction kdTran2 = response.results[1].kdTransaction;
        System.assertEquals('T2', kdTran2.transactionId, 'Expected T2 to be second in the list of results, because we ordered by TRANSACTION_NUMBER');

        associatedTransactionIds = new Set<String>(kdTran2.associatedTransactionIds);

        System.assertEquals(1, associatedTransactionIds.size(), 'Expected T2 to contain exactly 1 associated transaction');
        System.assert(associatedTransactionIds.contains('T3'), 'Expected T3 to be in the list of associated transactions for T2. associatedTransactionIds=' + associatedTransactionIds);

    }

    /**
     * MJL-729
     * check various error scenarios
     */
    static testMethod void testVariousErrors () {
        //missing message Id
        SFDCKitchenDrawerTypes.RequestHeader requestHeader = new SFDCKitchenDrawerTypes.RequestHeader();
        SFDCKitchenDrawerTypes.TransactionSearchCriteria searchCriteria = new SFDCKitchenDrawerTypes.TransactionSearchCriteria();
        
        SFDCKitchenDrawerTypes.ActionResponse response = MyJLKD_SearchHandler.process(requestHeader, searchCriteria);
        System.assertEquals(false, response.success, 'Must have failed due to missing message Id');
        System.assertEquals(MyJL_Const.ERRORCODE.MESSAGEID_NOT_PROVIDED.name(), response.code, 'Must have failed due to missing message Id');

        //missing required search criteria
        requestHeader = new SFDCKitchenDrawerTypes.RequestHeader();
        requestHeader.messageId = 'message-1';
        
        searchCriteria = new SFDCKitchenDrawerTypes.TransactionSearchCriteria();
        response = SFDCKitchenDrawerGet.searchTransactions(requestHeader, searchCriteria);
        System.assertEquals(false, response.success, 'Must have failed due to missing search fields');
        System.assertEquals(MyJL_Const.ERRORCODE.INCORRECT_SEARCH_REQUEST.name(), response.code, 'Must have failed due to missing search fields');

        //non unique message Id
        CustomSettings.setTestLogHeaderConfig(true, 1);
        CustomSettings.setTestLogDetailConfig(true, 1);

        Database.insert(new Log_Header__c(Message_ID__c = 'TEST-Duplicate_1', Service__c = MyJL_Util.SOURCE_SYSTEM));

        requestHeader = new SFDCKitchenDrawerTypes.RequestHeader();
        requestHeader.messageId = 'TEST-Duplicate_1';
        
        searchCriteria = new SFDCKitchenDrawerTypes.TransactionSearchCriteria();
        response = MyJLKD_SearchHandler.process(requestHeader, searchCriteria);
        System.assertEquals(false, response.success, 'Must have failed due to duplicate MessageId');
        System.assertEquals(MyJL_Const.ERRORCODE.MESSAGEID_DUPLICATE.name(), response.code, 'Must have failed due to duplicate MessageId');
    }
    
    /**
     * MJL-1183 - simulate work of KitchenDrawer services when they are disabled
     */
    static testMethod void testDisabledServices () {
        SFDCKitchenDrawerGet.TEST_ENABLE_SERVICES = false;

        //search
        SFDCKitchenDrawerTypes.RequestHeader requestHeader = new SFDCKitchenDrawerTypes.RequestHeader();
        requestHeader.messageId = 'message-1';
        
        final SFDCKitchenDrawerTypes.TransactionSearchCriteria searchCriteria = new SFDCKitchenDrawerTypes.TransactionSearchCriteria();
        try {
            SFDCKitchenDrawerTypes.ActionResponse response = SFDCKitchenDrawerGet.searchTransactions(requestHeader, searchCriteria);
            System.assertEquals(false, response.success, 'Expected main response to report failure with INACTIVE_SERVICE');
            System.assertEquals(MyJL_Const.ERRORCODE.INACTIVE_SERVICE.name(), response.code, 
                    'Expected main response to report failure with INACTIVE_SERVICE');
            
        } catch (Exception e) {
            System.assert(false, 'Exception must not be thrown. Result: ' + e);
        }

        //get
        requestHeader = new SFDCKitchenDrawerTypes.RequestHeader();
        requestHeader.messageId = 'message-1';
        
        try {
            SFDCKitchenDrawerTypes.ActionResponse response = SFDCKitchenDrawerGet.getTransaction(requestHeader, new String[]{});
            System.assertEquals(false, response.success, 'Expected main response to report failure with INACTIVE_SERVICE');
            System.assertEquals(MyJL_Const.ERRORCODE.INACTIVE_SERVICE.name(), response.code, 
                    'Expected main response to report failure with INACTIVE_SERVICE');
            
        } catch (Exception e) {
            System.assert(false, 'Exception must not be thrown. Result: ' + e);
        }
    }
    
    /**
     *  2014-11-10 - MJL-1276 - AG
     *  if Attachment relates to Transaction_Header__c and
     *  Transaction_Header__c now has both Full & Short XMLs then update
     *  Transaction_Header__c and set IsValid__c = true
     */
    static testMethod void testAttachmentValidation () {
        List<Loyalty_Card__c> cards = generateMYJL_LoyaltyCards(3);
        final List<Transaction_Header__c> trans = new List<Transaction_Header__c>{
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR0_0', Card_Number__c = cards[0].Id, Transaction_Date_Time__c = System.now()),
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR1_0', Card_Number__c = cards[1].Id, Transaction_Date_Time__c = System.now()),
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR1_1', Card_Number__c = cards[1].Id, Transaction_Date_Time__c = System.now()),
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR2_0', Card_Number__c = cards[2].Id, Transaction_Date_Time__c = System.now()),
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR2_1', Card_Number__c = cards[2].Id, Transaction_Date_Time__c = System.now()),
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR2_2', Card_Number__c = cards[2].Id, Transaction_Date_Time__c = System.now())
        };
        Database.insert(trans);
        
        //generate XML attachments
        final List<Attachment> atts = new List<Attachment>();
        atts.addAll(initAttachments(trans[0], SHORT_XML, FULL_XML)); //IsValid__c = true
        atts.addAll(initAttachments(trans[1], SHORT_XML, ''));//IsValid__c = false, missing full XML
        atts.addAll(initAttachments(trans[2], '', FULL_XML)); //IsValid__c = false, missing short XML

        //also add attachment to Card to make sure that objects other than Transaction_Header__c are ignored
        List<Attachment> nonTransactionAttachments = new List<Attachment>();
        Attachment cardAtt1 = initAttachments(trans[0], SHORT_XML, '')[0];
        cardAtt1.ParentId = cards[0].Id;

        Attachment cardAtt2 = initAttachments(trans[0], '', FULL_XML)[0];
        cardAtt2.ParentId = cards[0].Id;
        
        Database.insert(atts);

        final Map<Id, Transaction_Header__c> headerMap = new Map<Id, Transaction_Header__c>([select Id, IsValid__c from Transaction_Header__c where Id in :new Set<Id>{trans[0].Id, trans[1].Id, trans[2].Id, trans[3].Id}]);
        
        System.assert(headerMap.get(trans[0].Id).IsValid__c, 'trans[0] has both XML attachments and must be marked as valid');
        System.assert(!headerMap.get(trans[1].Id).IsValid__c, 'trans[1] has missing full XML attachments and must be marked as Invalid');
        System.assert(!headerMap.get(trans[2].Id).IsValid__c, 'trans[2] missing short XML attachments and must be marked as Invalid');
        System.assert(!headerMap.get(trans[3].Id).IsValid__c, 'trans[3] missing both XML attachments and must be marked as Invalid');

        System.debug('agX start here');
        //create full attachment for trans[1] to make it valid
        Database.insert(initAttachments(trans[1], '', FULL_XML));
        System.debug('agX stop here');

        System.assert([select Id, IsValid__c from Transaction_Header__c where Id = :trans[1].Id].IsValid__c, 
                'Expected trans[1] to be now valid, because it has both XML attachments');

        //invalidate trans[0] & trans[1] by deleting short XML attachments
        final List<Attachment> atts0_and_1 = [select Id from Attachment 
                                                where (ParentId = :trans[0].Id or ParentId = :trans[1].Id) 
                                                    and 
                                                    Name = :MyJLKD_SearchHandler.getShortXmlName()];
        Database.delete(atts0_and_1);

        System.assertEquals(2, [select count() from Transaction_Header__c where (Id = :trans[0].Id or Id = :trans[1].Id) and IsValid__c = false], 
                'Expected trans 0 and 1 to be now Invalid, because they are missing Short XML attachments');


        //restore short XML attachments of trans 0 and 1 to make them valid again
        Database.undelete(atts0_and_1);
        System.assertEquals(2, [select count() from Transaction_Header__c where (Id = :trans[0].Id or Id = :trans[1].Id) and IsValid__c = true], 
                'Expected trans 0 and 1 to be now Invalid, because they are missing  has both XML attachments');

        //invalidate trans[0 & 1] by changing name of short XML to something else
        atts0_and_1[0].Name = 'Not XML';
        atts0_and_1[1].Name = 'Not XML';
        Database.update(atts0_and_1);
        System.assertEquals(2, [select count() from Transaction_Header__c where (Id = :trans[0].Id or Id = :trans[1].Id) and IsValid__c = false], 
                'Expected trans 0 and 1 to be now Invalid, because they are missing Short XML attachments');
    }

    /**
     *  NOTE:
     *  THIS TEST IS NOT RECOMMENDED FOR DEPLOYMENT IN PROD because it is very
     *  slow, especially if TRANSACTION_COUNT is high
     *  
     *  2014-11-10 - MJL-1421 - AG
     *  check what happens when lots of attachments are inserted like so
     *  a) Create 3000 transaction headers
     *  b) Insert 3000 short xml attachments
     *  c) Insert 3000 full xml attachments
     *  d) check that the isvalid flag is set correctly
     *  
     */
    static testMethod void testAttachmentValidationInBulk () {
        final Integer TRANSACTION_COUNT = IS_SANDBOX ? 300 : 20;//reduce the time it test takes to run in Prod
        final Integer TRANSACTIONS_PER_CARD = IS_SANDBOX ? 10 : 2;
        
        List<Loyalty_Card__c> cards = generateMYJL_LoyaltyCards(TRANSACTION_COUNT / TRANSACTIONS_PER_CARD);
        final List<Transaction_Header__c> trans = new List<Transaction_Header__c>();
        for(Integer i=0; i < TRANSACTION_COUNT; i++) {
            Integer card_i = Math.mod(i, TRANSACTIONS_PER_CARD);
            Transaction_Header__c header = new Transaction_Header__c(
                    Receipt_Barcode_Number__c = 'TR0_' + i, Card_Number__c = cards[card_i].Id, Transaction_Date_Time__c = System.now());
            
            trans.add(header);
            
        }
        Database.insert(trans);

        final Set<Id> headerIds = new Set<Id>();
        
        //generate XML attachments
        final List<Attachment> atts = new List<Attachment>();
        for(Transaction_Header__c header : trans) {
            atts.addAll(initAttachments(header, SHORT_XML, ''));//IsValid__c = false, missing full XML
            headerIds.add(header.Id);
        }
        
        Test.startTest();//this is important, otherwise with TRANSACTION_COUNT > 1000 get: too many DML rows 10001
        Database.insert(atts);
        atts.clear();
        System.assertEquals(0, [select count() from Transaction_Header__c where Id in :headerIds and IsValid__c = true], 
                'Expected exactly ZERO valid transactions' );

        for(Transaction_Header__c header : trans) {
            atts.addAll(initAttachments(header, '', FULL_XML));
        }
        Database.insert(atts);
        Test.stopTest();


        System.assertEquals(TRANSACTION_COUNT, [select count() from Transaction_Header__c where Id in :headerIds and IsValid__c = true], 
                'Expected exactly ' + TRANSACTION_COUNT + ' valid transactions' );
    }

    /**
     *  NOTE:
     *  THIS TEST IS NOT RECOMMENDED FOR DEPLOYMENT IN PROD because it is very
     *  slow, especially if TRANSACTION_COUNT is high
     *  
     *  2014-11-10 - MJL-1421 - AG
     *  check what happens when lots of attachments are inserted like so
     *  a) Create 3000 transaction headers
     *  b) Insert 3000 short xml attachments
     *  c) Delete all short XML attachments
     *  d) Insert 3000 short xml attachments
     *  e) Insert 3000 full xml attachments
     *  f) check that the isvalid flag is set correctly
     *  
     */
    static testMethod void testAttachmentValidationInBulkWithDelete () {
        final Integer TRANSACTION_COUNT = IS_SANDBOX ? 300 : 20;//reduce the time it test takes to run in Prod
        final Integer TRANSACTIONS_PER_CARD = IS_SANDBOX ? 5 : 2;
        final Integer TRANSACTIONS_TO_UPDATE = TRANSACTION_COUNT;
        
        List<Loyalty_Card__c> cards = generateMYJL_LoyaltyCards(TRANSACTION_COUNT / TRANSACTIONS_PER_CARD);
        final List<Transaction_Header__c> trans = new List<Transaction_Header__c>();
        for(Integer i=0; i < TRANSACTION_COUNT; i++) {
            Integer card_i = Math.mod(i, TRANSACTIONS_PER_CARD);
            Transaction_Header__c header = new Transaction_Header__c(
                    Receipt_Barcode_Number__c = 'TR0_' + i, Card_Number__c = cards[card_i].Id, Transaction_Date_Time__c = System.now());
            
            trans.add(header);
            
        }
        Database.insert(trans);

        final Set<Id> headerIds = new Set<Id>();
        
        //generate XML attachments
        final List<Attachment> atts = new List<Attachment>();
        final List<Attachment> attachmentsToDelete = new List<Attachment>();
        for(Transaction_Header__c header : trans) {
            atts.addAll(initAttachments(header, SHORT_XML, ''));//IsValid__c = false, missing full XML
            headerIds.add(header.Id);
            //
            //if (Math.random() > 0.5 && attachmentsToDelete.size() < TRANSACTIONS_TO_UPDATE) {
                attachmentsToDelete.add(atts.get(atts.size() -1));
            //}
        }
        
        Database.insert(atts);
        atts.clear();
        
        Test.startTest();//this is important, otherwise with TRANSACTION_COUNT > 1000 get: too many DML rows 10001

        //delete attachmentsToDelete
        Database.delete(attachmentsToDelete);
        System.assert(!attachmentsToDelete.isEmpty(), 'attachmentsToDelete must not be empty. Check test parameters');
        System.assertEquals(TRANSACTION_COUNT - attachmentsToDelete.size(), [select count() from Attachment where ParentId in :headerIds], 'Incorrect number of attachments');
        
        //add extra attachments in place of deleted
        for(Attachment att : attachmentsToDelete) {
            atts.add(att.clone(false, true));
        }

        System.assertEquals(0, [select count() from Transaction_Header__c where Id in :headerIds and IsValid__c = true], 
                'Expected exactly ZERO valid transactions' );

        for(Transaction_Header__c header : trans) {
            atts.addAll(initAttachments(header, '', FULL_XML));
        }
        
        Database.insert(atts);
        Test.stopTest();


        System.assertEquals(TRANSACTION_COUNT, [select count() from Transaction_Header__c where Id in :headerIds and IsValid__c = true], 
                'Expected exactly ' + TRANSACTION_COUNT + ' valid transactions' );

    }
}