/**
 *  @Author  : Yousuf Mohammad
 *  @Date    : 10-06-2016
 *  @Purpose : This class used as vo object for interfacing between salesforce and external systems for Case Creation.
 */
global class  ExceptionServiceVo {
        global String  exceptionType                 {get;set;}
        global String  exceptionId                   {get;set;} 
        global String  exceptionDescription          {get;set;} 
        global String  fulfillmentOptionId           {get;set;}
        global String  customerId                    {get;set;} 
        global String  orderNumber                   {get;set;}
        global String  caseCategory                  {get;set;}
        public String  branch                        {get;set;}
        public String  caseOrigin                    {get;set;}
        public String  initiatingSource              {get;set;}
        public String  contactReason                 {get;set;}
        public String  reasonDetail                  {get;set;}
        public String  queryDescription              {get;set;}
        public String  queryType                     {get;set;}
        public String  queryTypeDetail               {get;set;}
        public String  assignQueryToQue              {get;set;}
        global String paymentReference               {get;set;}
        global String paymentType                    {get;set;}
        global String giftCardNumber                 {get;set;}
        global String furtherInformation             {get;set;}
        global String IsGuestOrder                   {get;set;}
        global String smartPayRefundAmount           {get;set;}
        global String transactionId                  {get;set;}
        global String errorCode                      {get;set;}
        global String errorDescription               {get;set;}
        global String rejectReasonCode               {get;set;}
        global String rejectReasonComment            {get;set;}  
        global Date collectionDate    		         {get;set;}
        
        // Changes for COPT-5843
        global String singleSupplier				 {get;set;}
        // End Changes for COPT-5843
        public ServiceResult serviceResult           {get;set;}
        
        public String  toString                      {get{
            return 'Exception Type >>> '+exceptionType==null ? 'NULL' : exceptionType +
                           'Exception ID >>> '+exceptionId==null ? 'NULL' : exceptionId +
                           'Exception Description >>> '+exceptionDescription==null ? 'NULL' : exceptionDescription +
                           'Fullfillment Option Id >>> '+fulfillmentOptionId==null ? 'NULL' : fulfillmentOptionId +
                           'Customer ID >>> '+customerId==null ? 'NULL' : customerId +
                           'Order Number >>> '+orderNumber==null ? 'NULL' : orderNumber ;
        }}
    	global List<OrderLineItem> orderLineItems   {get;set;}
    	global class supplierOrder{        
                global String orderLineNo {get;set;}
                global String orderNo {get;set;}
                global String supplierName {get;set;} 
                
                // Changes for COPT-5843
		        global String supplierNo {get;set;}
		        // End Changes for COPT-5843
            }        
         
        global class OrderLineItem  implements Comparable {
            global String orderLineItemId               {get;set;}
            global String productDescription            {get;set;}
            global String productCode                   {get;set;}   
	        global String deliveryNumber                {get;set;}
	        global string fulfilmentOptionName          {get;set;}
	        global Date   deliveryDate                  {get;set;}
	        global String  PORejectedErrorCode          {get;set;}
	        global String  PORejectedErrorDescription   {get;set;}
	        global String  status                       {get;set;}  
            global String  fullLocationID               {get;set;}
            global Date collectionDate    		        {get;set;}
            
            // Changes for COPT-5843
	        global String quantity {get;set;}
	        // End Changes for COPT-5843
        
            // Changes for COPT-5834
            global String orderReleaseLeadTime	{get;set;}
            // End Changes for COPT-5834
         //   global supplierOrder supplierOrders {get;set;}
            
            global List<supplierOrder> supplierOrders   {get;set;}
            
	        global Integer compareTo(Object compareTo) {
	         	OrderLineItem compareToOppy = (OrderLineItem)compareTo;
	
		        // The return value of 0 indicates that both elements are equal.
		        Integer returnValue = 0;
		        if (this.deliveryDate > compareToOppy.deliveryDate) {
		            // Set return value to a positive value.
		            returnValue = 1;
		        } else if (this.deliveryDate < compareToOppy.deliveryDate) {
		            // Set return value to a negative value.
		            returnValue = -1;
		        }
            
		        return returnValue;       
		    }
		    
        }
        
        
        global class ServiceResult  {
	        public String  errorMessage                  {get;set;}
	        public String  errorLabel                    {get;set;}
	        public String  caseNumber                    {get;set;}
	        public String  caseID                        {get;set;}
	        public Boolean hasError                      {get { if(hasError==null) {return false;} else {return hasError;}} set;}
        }
}