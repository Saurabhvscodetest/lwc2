/******************************************************************************
* @author       Matt Povey
* @date         1 Mar 2016
* @description  Collection of static methods and variables common to classes
*               used in case operations.
*
* EDIT RECORD
*
* LOG   DATE        Author  JIRA        COMMENT      
* 001   01/03/2016  MP      COPT-654    Original code
* 002   28/11/2016  MP      COPT-1383   Add multi-dimensional map for Task and Case record types.
* 003   10/06/2019  KM      COPT-4681   Code Refactoring
*******************************************************************************
*/
public without sharing class CommonStaticUtils {

    // System Constants
    public static final Boolean IS_SANDBOX = setIsSandbox();
    public static final Boolean IS_PRODUCTION = !IS_SANDBOX;
    // Adding a booleab flag to prevent trigger run
    public static Boolean runTrigger = true;

    // Case Constants
    public static final String CASE_OBJECT = 'Case';
    // Statuses
    public static final String CASE_STATUS_NEW = 'New'; 
    public static final String CASE_STATUS_ACTIVE = 'Active';   
    public static final String CASE_STATUS_ACTIVE_AWAITING = 'Active - Awaiting on customer update';
    public static final String CASE_STATUS_ACTIVE_FOLLOWUP = 'Active - Follow up required';
    public static final String CASE_STATUS_ACTIVE_RETURN = 'Active - Return to action list';
    public static final String CASE_STATUS_PENDING_INVALID_CONTACT = 'Pending - Invalid contact number';
    public static final String CASE_STATUS_PENDING_PENDING_NO_ANSWER = 'Pending - No answer from contact number';
    public static final String CASE_STATUS_PENDING_WAITING_FOR_CUSTOMER = 'Pending - Waiting customer instruction';
    public static final String CASE_STATUS_CLOSED_DUPLICATE = 'Closed - Duplicate';
    public static final String CASE_STATUS_CLOSED_RESOLVED =  'Closed - Resolved';
    public static final String CASE_STATUS_CLOSED_UNRESOLVED = 'Closed - Unresolved';
    public static final String CASE_STATUS_READY_FOR_CLOSURE  = 'Ready for Closure';    
    public static final String CASE_STATUS_CLOSE_CASE = 'Close Case';
    public static final String CASE_STATUS_CLOSED_SPAM = 'Closed - SPAM';
    public static final String CASE_STATUS_CLOSED_NO_RESPONSE = 'Closed - No Response Required';
    public static final String CASE_STATUS_CLOSED_ASSIGNED_TO_ANOTHER_BRANCH = 'Closed - Assigned to Another Branch';
    // Record Types
    public static final String CASE_RT_NAME_QUERY = 'Query';
    /* COPT-5272
    public static final String CASE_RT_NAME_SUB_QUERY = 'PSE Sub-case';
	COPT-5272 */
    public static final String CASE_RT_NAME_COMPLAINT = 'Complaint';
    public static final String CASE_RT_NAME_PRODUCT_STOCK_ENQUIRY = 'Product Stock Enquiry';
    /* COPT-5272
    public static final String CASE_RT_NAME_EMAIL_TRIAGE = 'Email Triage';
    public static final String CASE_RT_NAME_WEB_TRIAGE = 'Web Triage';
    COPT-5272 */
	public static final String CASE_RT_NAME_NKU = 'NKU';
    public static final String CASE_RT_NAME_OE = 'Order Exception';
    public static final String CASE_RT_NAME_MYJL = 'myJL Request';
    //<<003>>
     public static final String CASE_RT_New_Case = 'New Case'; 
    //<<003>>
    public static final String CONTACT_RT_DEVELEPER_NAME_OC = 'Ordering_Customer';
    public static final String CONTACT_RT_DEVELEPER_NAME_S = 'Superseded';
    
    // Task Constants
    public static final String TASK_OBJECT = 'Task';
    public static final String TASK_STATUS_CLOSED_RESOLVED = 'Closed - Resolved';
    public static final String TASK_SUBJECT_EMAIL_LOG = 'Email Log';
    public static final String TASK_RT_NAME_EMAIL_LOG = 'Email Log';
    public static final String TASK_SUBJECT_CALL_LOG = 'Call log';
    public static final String TASK_RT_NAME_CALL_LOG = 'Call Log';
    
    // Webform Constants
    public static final String CONTACT_US_WEBFORM = 'Contact Us';
    public static final String NKU_WEBFORM = 'NKU';
    
    // ID Prefixes
    public static final String CASE_ID_PREFIX = '500';
    public static final String USER_ID_PREFIX = '005';
    public static final String GROUP_ID_PREFIX = '00G';
    
    // Miscellaneous
    public static final Integer MAX_SEARCH_TEXT_LENGTH = 38;

    public static Boolean ALLOW_EDIT_ORDERING_CUST_FOR_VOC_UDPATE = false;
    // Static User Variables
    private static Map<String, Map<String, RecordType>> allRecordTypesMap = new Map<String, Map<String, RecordType>>(); // <<002>>
    public static Map<String, RecordType> contactRecordTypesMap = new Map<String, RecordType>();

    private static Map<Id, UserRole> userRoleMap = new Map<Id, UserRole>();
    private static User currentUser;
    private static Profile userProfile;
    public static Map<String, BusinessHours> businessHoursMap = new Map<String, BusinessHours>();

    // COPT-5780 Changes
    public static String CREATE_PSE_TAB_PS_NAME = System.Label.Create_PSE_Tab_PS_Name;

    /**
     * Get case record type record from map - retained for legacy calls but refactored
     * to use new getRecordType(String rtObject, String rtName) method.
     * @params: String rtName       The name of the record type to retrieve.
     * @rtnval: RecordType          The RecordType record that corresponds to the rtName string.
     */
    public static RecordType getRecordType(String rtName) {
        return getRecordType(CASE_OBJECT, rtName);
    }

    /**
     * Get record type record from multi-dimensioned map
     * @params: String rtObject     The object the required record type relates to.
     *          String rtName       The name of the record type to retrieve.
     * @rtnval: RecordType          The RecordType record that corresponds to the rtName string.
     */
    public static RecordType getRecordType(String rtObject, String rtName) {
        if (allRecordTypesMap.isEmpty() || !allRecordTypesMap.containsKey(rtObject)) {
            List<RecordType> listRecTypes = [SELECT Id, SObjectType, DeveloperName, Name FROM RecordType WHERE SObjectType = :rtObject];

            if (!listRecTypes.isEmpty()) {
                Map<String, RecordType> objectRecordTypesMap = new Map<String, RecordType>();
                for (RecordType rt : listRecTypes) {
                    objectRecordTypesMap.put(rt.DeveloperName, rt);
                }
                allRecordTypesMap.put(rtObject, objectRecordTypesMap);
            }
        }
        if (allRecordTypesMap.containsKey(rtObject) && allRecordTypesMap.get(rtObject).containsKey(rtName)) {
            return allRecordTypesMap.get(rtObject).get(rtName);
        }
        return null;
    }

    public static Id getRecordTypeID(String objectName, String recordTypeName){

        return Schema.getGlobalDescribe().get(objectName).getDescribe().getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
         
    }

    public static String getRecordTypeName(String objectName, Id recordTypeId){
        return Schema.getGlobalDescribe().get(objectName).getDescribe().getRecordTypeInfosById().get(recordTypeId).getName();
    }
    public static RecordType getContactRecordType(String rtName) {
        if (contactRecordTypesMap.isEmpty()) {
            List<RecordType> listRecTypes = [SELECT Id, SObjectType, DeveloperName, Name FROM RecordType WHERE SObjectType = 'Contact'];

            for (RecordType rt : listRecTypes) {
                contactRecordTypesMap.put(rt.DeveloperName, rt);
            }
        }
        return contactRecordTypesMap.get(rtName);
    }
    
    /**
     * Get current user's additional details (currently only tier)
     * Can be used to retrieve additional fields on the user record not held in UserInfo
     * @params: None
     * @rtnval: User    Current user's record.
     */
    public static User getCurrentUser() {
        if (currentUser == null) {
            currentUser = [SELECT Id, Tier__c, Team__c, Username, Profile.Name FROM User WHERE id = :UserInfo.getUserId()];
        }
        return currentUser;
    }
    
    /**
     * Get current user's profile
     * @params: profileName - profile name to be checked in the query
     * @rtnval: true if the user has the named profile
     */
     
    public static Boolean UserHasProfileNamed(String profileName) {

        profileName = '%' + profileName + '%';

        Id userProfileId = UserInfo.getProfileId();
        List<Profile> profiles = [SELECT Id FROM Profile WHERE Id = :userProfileId AND Name LIKE :profileName];
        Boolean isProfileIncluded = !profiles.isEmpty();

        if(isProfileIncluded){
            return true;
        } 
        else {
            return false;
        }   
    }
    

    /**
     * Get record type record from map
     * @params: String roleId       Id of the UserRole record to retrieve.
     * @rtnval: UserRole            The UserRole record that corresponds to the roleId Id in the Map.
     */
    public static UserRole getUserRole(String roleId) {
        if (userRoleMap.isEmpty())  {
            List<UserRole> listUserRoles = [select Id, Name from UserRole];

            for (UserRole ur: listUserRoles) {
                userRoleMap.put(ur.Id, ur);
            }
        }
        return userRoleMap.get(roleId);
    }
    
    /**
     * Return a boolean value based on whether the Id specified corresponds to a Case.
     * @params: Id idToCheck        Id to be checked to see if it matches the Case prefix.
     * @rtnval: Boolean             Set to true if id corresponds to a Case, false if not.
     */
    public static Boolean isCaseId(Id idToCheck) {
        return isIdOfType(idToCheck, CASE_ID_PREFIX);
    }
    
    /**
     * Return a boolean value based on whether the Id specified corresponds to a User.
     * @params: Id idToCheck        Id to be checked to see if it matches the User prefix.
     * @rtnval: Boolean             Set to true if id corresponds to a User, false if not.
     */
    public static Boolean isUserId(Id idToCheck) {
        return isIdOfType(idToCheck, USER_ID_PREFIX);
    }
    
    /**
     * Return a boolean value based on whether the Id specified corresponds to a Group (Queue).
     * @params: Id idToCheck        Id to be checked to see if it matches the Group prefix.
     * @rtnval: Boolean             Set to true if id corresponds to a Group, false if not.
     */
    public static Boolean isGroupId(Id idToCheck) {
        return isIdOfType(idToCheck, GROUP_ID_PREFIX);
    }

    /**
     * Return a boolean value based on whether the Id specified has the specified prefix.
     * Returns true if the id matches the given prefix.
     * Returns false if the id is null or does not match the given prefix.
     * @params: Id idToCheck        Id to be checked to see if it matches the given prefix.
     *          String idPrefix     The prefix to be used in the check.
     * @rtnval: Boolean             Set to true if prefix matches the id, false if not.
     */
    public static Boolean isIdOfType(Id idToCheck, String idPrefix) {
        String idCheckString = idToCheck;
        return (String.isNotBlank(idCheckString) && idCheckString.startsWith(idPrefix));
    }

    /**
     * Return a boolean value based on whether a given user has a permission set assigned to them or not.
     * Returns true if user has that permission set. False if Not.
     * @params: Id userIdToCheck            User Id to be checked to see if it matches the given prefix.
     *          String permissionSetName    The name of the permission set.
     *          Id permissionSetId          The Id of the permission set.
     * @rtnval: Boolean                     Set to true if user has the permission set assigned to it.
     */
    public static Boolean isPermissionSetAssignedToSingleUserByName(Id userIdToCheck, String permissionSetName) {
        Boolean result = false;     
        if (userIdToCheck != null) {            
            List<PermissionSetAssignment> permissionSetAssignments = [SELECT AssigneeId, PermissionSet.Name FROM PermissionSetAssignment WHERE AssigneeId = :userIdToCheck AND PermissionSet.Name = :permissionSetName];    
            System.debug('permissionSetAssignments: ' + permissionSetAssignments);
            if (!permissionSetAssignments.isEmpty()) {  
                return true;
            } else {
                System.debug('permissionSetAssignments empty returning false');
                return false;
            }           
        } else {
            return false;
        }       
    }
    
    /**
    * @description  Retrieves the Business Hours based on the  Name
    * @author       @AJohn
    * @param        businessHoursName String of businessHoursName name
    * @return       Id of Entitlement record
    */

    public static BusinessHours getBusinessHours(String businessHoursName) {
        if(businessHoursMap.isEmpty()) {
            businessHoursMap =  new Map<String, BusinessHours>();
            List<BusinessHours> businessHoursList = new List<BusinessHours> ([SELECT Id, Name, IsDefault FROM BusinessHours]);
            system.debug('BusinessHours' + businessHoursList);
            for(BusinessHours bh: businessHoursList) {
                businessHoursMap.put(bh.Name.toUpperCase(), bh);
            }
        }
        return businessHoursMap.get(businessHoursName.toUpperCase());
    }

    public static void allowEditOfOrderingCustomerForVocUpdate() {
        ALLOW_EDIT_ORDERING_CUST_FOR_VOC_UDPATE = true;
    }
    public static boolean canEditOrderingCustomerForVocUpdate() {
        return ALLOW_EDIT_ORDERING_CUST_FOR_VOC_UDPATE;
    }
   /**
     * Returns true if current environment is a Sandbox. False if Production.
     * @params: None
     * @rtnval: Boolean     Set to true if current environment is a Sandbox.
     */
    private static boolean setIsSandbox() {
        List<Organization> orgList = [SELECT Id, IsSandbox FROM Organization];
        if (!orgList.isEmpty()) {
            Organization org = orgList.get(0);
            return org.IsSandbox;
        }       
        return false; // Assume live if no record found as this is safer.
    }


    public static String getValue(String constantRecordKey , String defaultValue){
        String retVal = defaultValue;
        Constants__c  constantRec = Constants__c.getInstance(constantRecordKey);
        if (constantRec != null ){
            retVal = constantRec.Value__c;
        }
        return retVal;
    }
    
    //Extract last 10 digits of contact
    public static String removeContactPrefix(String phoneNumber){
        System.debug(phoneNumber.right(10));
        return phoneNumber.right(10);      
    }
    
    /**
     Changes for COPT-5780 
     
     * Return a Boolean value based on whether a given user has a permission set assigned to them or not.
     * Returns true if user has that permission set 'Create_PSE_Case_PS' and profile named contains 'Capita'. False if Not.
     * @params:  None        
     * @return value : Boolean Set to true if user has the permission set assigned to it and profile contains 'Capita'.
     */


    public static Boolean isCreatePSETabEnabledUser ( ) {
        User currentUser = CommonStaticUtils.getCurrentUser();
        Id userIdToCheck = currentUser.Id;
        String permissionSetName = CREATE_PSE_TAB_PS_NAME ;
                    
        if (userIdToCheck != null) {            
            List<PermissionSetAssignment> permissionSetAssignments = [SELECT AssigneeId, PermissionSet.Name FROM PermissionSetAssignment WHERE AssigneeId = :userIdToCheck AND PermissionSet.Name = :permissionSetName];    
            System.debug('permissionSetAssignments: ' + permissionSetAssignments);
            if (!permissionSetAssignments.isEmpty()) {  
                return false;
            }else {
               System.debug('permissionSetAssignments empty returning false');
               return true;
            }           
        } else {
            return false;
        }       
    }
    // End Changes for COPT-5780- Ragesh
    
}