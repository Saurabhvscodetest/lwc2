/**
 *  @author  : Yousuf Mohammad
 *  @Date    : 10-06-2016
 *  @Purpose : This class is used as parent class so that we can dynamically return the results.
 *             Output format for success and failure is different but webservice can return only one type format.
 *             So encapsulation is used for returning different results. This class should be extneded for all the 
 *             services responses in the future.
 *
 *
 */
global with sharing virtual class ServiceResponse {
    
}