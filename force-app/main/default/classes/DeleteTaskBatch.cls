/******************************************************************************

* @author		Stuart Barber
* @date			06/03/2017
* @description	Class that enables batching of old tasks (older than x amount of days) to be deleted

******************************************************************************/

global class DeleteTaskBatch implements Database.Batchable<sObject> {

    public String taskRecordType;
    public Integer daysToRetain;
    
    public Database.QueryLocator start(Database.BatchableContext BC){

		DateTime earliestDateToRetain = DateTime.now();
		earliestDateToRetain = earliestDateToRetain.addDays(daysToRetain * -1);

		return Database.getQueryLocator([SELECT Id, WhatId, WhoId
										 FROM Task
										 WHERE Subject like : taskRecordType
                                         and CreatedDate < :earliestDateToRetain]);
	}
    
    public void execute(Database.BatchableContext bc, List<Task> scope){
        database.delete(scope);
    }
    
    public void finish(Database.BatchableContext BC){}   
}