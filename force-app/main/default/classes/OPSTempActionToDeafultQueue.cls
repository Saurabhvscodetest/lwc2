global class OPSTempActionToDeafultQueue implements Database.Batchable<sObject>,Database.Stateful
{   
    global Map<Id,String> ErrorMap = new Map<Id,String>();
    global integer result=0;
    Datetime currentTime = Datetime.now();
    Datetime currentTimeMinus30min = currentTime.addMinutes(-30);
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        //System.debug('currentTimeMinus30min ----- '+currentTimeMinus30min );   
        return Database.getQueryLocator([select CaseNumber, Status, Case_Owner__c, jl_Customer_Response_By__c, Next_Case_Activity_Due_Date_Time__c 
                        from Case where Case_Owner__c like '%ACTIONED%'
                        and Status not IN ('Closed - Resolved','Closed - Duplicate','Closed - Unresolved','Closed - SPAM','Closed - No Response Required', 'Closed - Assigned to Another Branch') 
                        and jl_Customer_Response_By__c < :currentTimeMinus30min and CreatedDate > 2019-09-24T00:00:00.000+0000 
                        and Next_Case_Activity_Due_Date_Time__c != NULL]);                 
    }
    
    global void execute(Database.BatchableContext BC, List<Case> scope)
    {
        
        List<Group> queueList = [Select Id, Name, DeveloperName, Type from Group where Type = 'Queue'];
        //System.debug('Sixe - '+scope.size());
        If(scope.size()>0){
                Map<Case,String> caseOwnerUpdateMap = new Map<Case,String>();
                Set<Case> updatedOwnerList = new Set<Case>();
                List<Case> finalCaseList = new List<Case>();
                try{
                    for(Case case1 : scope){
                        //System.debug('Case Owner - '+case1.Case_Owner__c+'Case Number - '+case1.CaseNumber);
                        caseOwnerUpdateMap.put(case1,case1.Case_Owner__c.remove(' ACTIONED'));
                    }
                    //System.debug('caseOwnerUpdateMap - '+ caseOwnerUpdateMap);
                    updatedOwnerList = caseOwnerUpdateMap.keySet();
                    for(Case value1: updatedOwnerList){
                        //System.debug('value1 --- '+value1);
                        String updatedqueue = caseOwnerUpdateMap.get(value1);
                        //System.debug('Updated Name - '+updatedqueue);
                        for(Group queuedetail : queueList){
                            if((queuedetail.Name).equals(updatedqueue)){
                                //System.debug(queuedetail.Name+' '+queuedetail.Id);
                                value1.ownerId = queuedetail.Id;
                                //finalCaseList.put(value1,queuedetail.Id);
                                finalCaseList.add(value1);
                            }
                        }
                    }
                    List<Database.SaveResult> srList = Database.update(finalCaseList, false);
                    if(srList.size()>0){
                        for(Integer counter = 0; counter < srList.size(); counter++){
                            if (srList[counter].isSuccess()){
                                result++;
                            }else{
                                if(srList[counter].getErrors().size()>0){
                                    for(Database.Error err : srList[counter].getErrors()){
                                        ErrorMap.put(srList.get(counter).id,err.getMessage());
                                    }
                                }                                
                            }
                        }
                    }
                }catch(Exception e){
                    EmailNotifier.sendNotificationEmail('Exception from OPSTempActionToDeafultQueue', e.getMessage());
                }
        }
    }
    global void finish(Database.BatchableContext BC){
            String textBody = '';
            Set<Id> errorid = new Set<ID>();
            textBody+= result +' cases updated successfully '+'\n';
            if(!ErrorMap.isEmpty()){
                for(Id recordids : ErrorMap.KeySet()){
                        errorid.add(recordids);
                }
                textBody+='Error Log: '+errorid+'\n';
            }
            //System.debug('textBody ----- '+textBody);
            EmailNotifier.sendNotificationEmail('Batch Executed Successfully', textbody);
            OPSTempCreateMissingEmailPromise emailpromise = new OPSTempCreateMissingEmailPromise();
            Database.executeBatch(emailpromise,25);
    }
}