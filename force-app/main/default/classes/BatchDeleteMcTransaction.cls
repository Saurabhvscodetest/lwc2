global class BatchDeleteMcTransaction implements Database.Batchable<sObject>,Database.Stateful {
    @TestVisible private Set<Id> masterSetOfRecordDeleted = new Set<Id>();
    @TestVisible private Map<Id, String> successfulRecordMap = new Map<Id, String>();
    @TestVisible private Map<Id, String> unsuccessfulRecordMap = new Map<Id, String>();
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('select Id from MC_Transactions__c');
    }
    
    global void execute(Database.BatchableContext bc, List<MC_Transactions__c> scope){
        List<Database.deleteResult> deletedMCRecords = Database.delete(scope, false);
        
        for(Integer i = 0;i < deletedMCRecords.size(); i++){
             for(MC_Transactions__c mc : scope){
                masterSetOfRecordDeleted.add(mc.id);
            }
                // If record is successfully deleted
                if(deletedMCRecords[i].isSuccess()){
                    successfulRecordMap.put(deletedMCRecords[i].getId(), 'Successully deleted Record');
                }else{ //if error
                    String errorsConcatenated = '';
                    
                    for(Database.Error error : deletedMCRecords[i].getErrors()) {
                        if(String.isEmpty(errorsConcatenated)) {
                            errorsConcatenated = error.getMessage();
                        } else {
                            errorsConcatenated = errorsConcatenated + ' | ' + error.getMessage();
                        }
                    }
                    
                    unsuccessfulRecordMap.put((String)scope[i].Id, errorsConcatenated);
                }
            }
        
    }
    
     global void finish(Database.BatchableContext BC){
        try{
            String emailBody = '';
            
            emailBody += 'BatchDeleteMcTransaction job has run: \n\n\n';
            emailBody += 'Total number of records deleted :\n';
            emailBody += masterSetOfRecordDeleted.size();
            emailBody += '\n\n';
            emailBody += 'The following records were deleted succesfully: \n\n';
            emailBody += successfulRecordMap.size();
            emailBody += '\n\n';
            emailBody += 'The following  records encountered errors: \n\n';
            emailBody += unsuccessfulRecordMap.size();
            system.debug('+++ emailBody: ' + emailBody);
            EmailNotifier.sendNotificationEmail('MC Transactions Deletion Results', emailBody);
        }catch(Exception e){
            System.debug('@@@ | BatchDeleteMcTransaction | masterSetOfCardTypeUpdated: ' + masterSetOfRecordDeleted);
       		System.debug('@@@ | BatchDeleteMcTransaction | successfulCardTypeMap: ' + successfulRecordMap);
        	System.debug('@@@ | BatchDeleteMcTransaction | Number of successfulUpdate: ' + successfulRecordMap.size());
        	System.debug('@@@ | BatchDeleteMcTransaction | unsuccessfulCardTypeMap: ' + unsuccessfulRecordMap);
        	System.debug('@@@ | BatchDeleteMcTransaction | Number of unsuccessfulUpdate : ' + unsuccessfulRecordMap.size());
            System.debug('@@@ | Error Message : ' +e.getMessage());
        }
    }
}