@istest
public with sharing class CustomerComsPreviewController_TEST {
	
	@testSetup static void setup() {
         UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
    }

    @IsTest
    static void testEmailsAreDisplayed() {


        List<User> users = [SELECT Id FROM User WHERE IsActive = true LIMIT 1];
        Id userId = users[0].Id;

        Contact con = UnitTestDataFactory.createContact();
        insert con;

        Case c = UnitTestDataFactory.createNormalCase(con);
        c.OwnerId = userId;

        insert c;

        EmailMessage em = new EmailMessage();

        em.parentid = c.id;
        em.TextBody = 'Body of email';
        em.FromAddress = 'testemailaddress@jl.co.uk';
        
        List<EmailMessage> emailMessages = new List<EmailMessage>();
        emailMessages.add(em);
        insert emailMessages;

	    Test.startTest();
        ApexPages.StandardController sc = new ApexPages.standardController(c);
        CustomerComsPreviewController comsController = new CustomerComsPreviewController(sc);
        comsController.LoadDefaultContentOnError(emailMessages);
	    Test.stopTest();
        System.assertEquals(1, comsController.EmailActivityCollection.size(), 'Collection should be 1, as wwe have an email message against the case.');
    }

    @IsTest
    static void testEmailsWontBeDisplayed() {

        List<User> users = [SELECT Id FROM User WHERE IsActive = true LIMIT 1];
        Id userId = users[0].Id;

        Contact con = UnitTestDataFactory.createContact();
        insert con;

        Case c = UnitTestDataFactory.createNormalCase(con);
        c.OwnerId = userId;

        insert c;

        EmailMessage em = new EmailMessage();

        em.parentid = c.id;
        em.TextBody = 'Body of email';
        em.FromAddress = 'no_reply@johnlewis.co.uk';
        insert em;

	    Test.startTest();
        ApexPages.StandardController sc = new ApexPages.standardController(c);
        CustomerComsPreviewController comsController = new CustomerComsPreviewController(sc);
	    Test.stopTest();

        System.assertEquals(0, comsController.EmailActivityCollection.size(), 'Collection should be zero, as the from email address is no_reply');
    }
}