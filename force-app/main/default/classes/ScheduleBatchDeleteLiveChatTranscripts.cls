global class ScheduleBatchDeleteLiveChatTranscripts implements Schedulable{

     global void execute(SchedulableContext sc){
        GDPR_BatchDeleteLiveChatTranscripts obj = new GDPR_BatchDeleteLiveChatTranscripts();
        Database.executebatch(obj);
    }

}