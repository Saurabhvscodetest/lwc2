/*
* @File Name   : DelTrackConstants 
* @Description : Constant class to related to MyJLChatBot Delivery Tracking functionalities  
* @Copyright   : Zensar
* @Jira #      : #5174
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0       28-JAN-2020        Vijay A                       Created
*/

@Istest
Public class DelTrackConstantsTest {

    @Istest
    Public Static void testMethodDelTrackConstants(){
        
        Test.startTest();
        DelTrackConstants.getCaseListOrderNumber('12345678');
        DelTrackConstants.getCaseListDeliveryNumber('12345678');
        DelTrackConstants.currentTimeCheckWithEightPM();
        DelTrackConstants.twoHourARTimeCompare('12:00');
        DelTrackConstants.twoHourARTimeCompare('00:00');
        DelTrackConstants.twoHourARTimeCompare('00');
        DelTrackConstants.twoHourARTimeCompare('12345:12345');
    	Test.stopTest();
    }
    
}