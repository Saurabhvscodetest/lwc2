/**
 *	@author: Andrey Gavrikov (westbrook)
 *	@date: 2014-12-08 
 *	@description:
 *	    remove Transaction_Header__c records older than configured period
 *
 *	    This class requires 2 configurations
 *      1. Set number of days to retain transactions in the system
 *      - Setup > Develop > Custom Settings
 *			Find custom setting: MyJL Control Panel (MyJL Settings__c), click Manage
 *			Set value of Delete_Transactions_Older_than_N_Days field as required.
 *			e.g. to delete transactions older than 5 days enter 5.
 *      
 *      2. Schedule MyJLKD_TransactionCleanerBatch
 *      - Setup > Develop > Custom Settings
 *      Find custom setting: House Keeping Config, click Manage
 *      If not already added - add new record like so:
 *			Name: MyJLKD_TransactionCleanerBatch
 *			Class Name: MyJLKD_TransactionCleanerBatch
 *			Run Frequency (Days): 1
 *			Order: 2
 *			Description: removes transactions older than NN days, as specified in MyJL Control Panel
 * 
 *	
 *	Version History :   
 *	2014-12-08 - MJL-1182 - AG
 *	remove headers older than - MyJL Control Panel: Delete_Transactions_Older_than_N_Days
 *		
 */
public with sharing class MyJLKD_TransactionCleanerBatch implements HouseKeeperBatch.Processor {
	//name of the process specified in House_Keeping_Config__c custom setting
	public String getName() {
		return 'MyJLKD_TransactionCleanerBatch';
	}

	public Database.QueryLocator start(Database.BatchableContext bc) {
		final CustomSettings.Record config = CustomSettings.getMyJLSetting();
		System.assertNotEquals(null, config.getNumber('Delete_Transactions_Older_than_N_Days__c'), 'MyJL Settings__c.Delete_Transactions_Older_than_N_Days__c is not configured');
		final Integer days = config.getNumber('Delete_Transactions_Older_than_N_Days__c').intValue();
		Datetime cutOffDate = Datetime.newInstanceGmt(1970, 01, 01);//by default do not delete anything
		
        if(null != days ) {
			cutOffDate = System.now().addDays(-1 * days);
		}
		
		return Database.getQueryLocator([select Id from Transaction_Header__c where CreatedDate < :cutOffDate]);

	}
	public void execute(Database.BatchableContext bc, List<SObject> scope) {
		Database.delete(scope);
	}
	
	public void finish(Database.BatchableContext bc) {}

	public Integer getBatchSize() {
		return 200;
	}
}