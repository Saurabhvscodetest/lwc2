@isTest
private class clsScheduleCopyAttachments_TEST {

  @testSetup static void setup() {

    UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
  }

  @isTest static void testSchedulingOfCopyAttachments() {
      
    Set<Id> allCases = new Set<Id>();
    Contact testContact;
    Case testCase;
    String CRON_EXP = '0 0 0 15 3 ? 2022'; 

    testContact = UnitTestDataFactory.createContact();
    testContact.Email = 'stuart@little.com';
    insert testContact;
    
    testCase = UnitTestDataFactory.createCase(testContact.Id);
    testCase.jl_NumberOutstandingTasks__c = 2;
    insert testCase;
    
    system.assertNotEquals(null, testCase.Id);
      
    EmailMessage ems = UnitTestDataFactory.createOutboundEmailMessage(testCase.Id);
    testCase.Pending_Email_Messages__c = ems.id;
    
    update testCase;

    allCases.add(testCase.Id);
  
    Test.startTest();

    String jobId = System.schedule('clsScheduleCopyAttachments', CRON_EXP, new clsScheduleCopyAttachments(allCases));  
    CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
    
    System.assertEquals(CRON_EXP, ct.CronExpression);
    System.assertEquals(0, ct.TimesTriggered);
    System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
    
    Test.stopTest();

   }
}