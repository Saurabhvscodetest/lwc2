public class SearchPicklistControllerLEX {

    @AuraEnabled
    public static Map<String,Map<String,List<String>>> masterFieldDependencyMap {get; set; } 
    
    @AuraEnabled 
    public static List<String> primaryPicklist {get; set; }
    
    @AuraEnabled 
    public static Map<String,Map<String,List<String>>> secondaryPicklistMap {get; set;}
    
    @AuraEnabled
    public static Map<String,Map<String,Map<String,String>>> masterKeyAndValueMap {get; set;}
    
    @AuraEnabled 
    public static Map<String,Map<String,Map<String,String>>> cachePicklistValues(String searchTerm, String searchType){
        masterKeyAndValueMap = new Map<String,Map<String,Map<String,String>>>();
        if(searchType == 'Call Log'){
            masterKeyAndValueMap.put('Call Log',prepareMasterMap(NewCustomerContactLEXController.initiateLevelOnePicklist(), NewCustomerContactLEXController.generateTaskDependentFieldMap(), 'Call Log',searchTerm));
        }
        if(searchType == 'Case Assignment'){
            masterKeyAndValueMap.put('Case Assignment',prepareMasterMap(NewCustomerContactLEXController.initiateCaseAssignmentLevelOnePicklist(), NewCustomerContactLEXController.generateCaseAssignmentFieldMap(), 'Case Assignment',searchTerm));
        }
        if(searchType == 'Case Categorization'){
            masterKeyAndValueMap.put('Case Categorization',prepareMasterMap(CloseCaseLExController.initiateLevelOnePicklist(), CloseCaseLExController.generateCaseCategorizationDependentFieldMap(), 'Case Categorization',searchTerm));
        }
        return masterKeyAndValueMap;
    }
    
    @AuraEnabled 
    public static String getSearchResults(String searchTerm, String searchType){
        masterKeyAndValueMap = new Map<String,Map<String,Map<String,String>>>();
        masterKeyAndValueMap = cachePicklistValues(searchTerm,searchType);
        
        String searchResultJSON = '';
        masterFieldDependencyMap = new Map<String,Map<String,List<String>>>();
        Map<String,List<String>> fieldMapForCallLog = new Map<String,List<String>>();
        Map<String,List<String>> fieldMapForCaseAssignment = new Map<String,List<String>>();
        Map<String,List<String>> fieldMapForCaseCategorization = new Map<String,List<String>>();
        fieldMapForCallLog.put(null, new List<String>{'Level-1'});
        fieldMapForCallLog.put('Level-1', new List<String>{'Level-2'});
        fieldMapForCallLog.put('Level-2', new List<String>{'Level-3'});
        fieldMapForCallLog.put('Level-3', new List<String>{'Level-4'});
        fieldMapForCallLog.put('Level-4', new List<String>{'Level-5'});
        
        fieldMapForCaseAssignment.put(null, new List<String>{'Branch'});
        fieldMapForCaseAssignment.put('Branch', new List<String>{'ContactReason'});
        fieldMapForCaseAssignment.put('ContactReason', new List<String>{'ReasonDetail'});
        fieldMapForCaseAssignment.put('ReasonDetail', new List<String>{'CDH','BranchDepartment'});
        
        fieldMapForCaseCategorization.put(null, new List<String>{'primaryCategory'});
        fieldMapForCaseCategorization.put('primaryCategory', new List<String>{'areaOfTheBusiness','primaryReason'});
        fieldMapForCaseCategorization.put('primaryReason', new List<String>{'primaryReasonDetail','buyingDirectorate'});
        fieldMapForCaseCategorization.put('buyingDirectorate', new List<String>{'productGroup'});
        fieldMapForCaseCategorization.put('primaryReasonDetail', new List<String>{'primaryDetailExplanation'});
        
        masterFieldDependencyMap.put('Call Log',fieldMapForCallLog);
        masterFieldDependencyMap.put('Case Assignment',fieldMapForCaseAssignment);
        masterFieldDependencyMap.put('Case Categorization',fieldMapForCaseCategorization);
        
        List<PicklistSearchResult> returnList = new List<PicklistSearchResult>();
        
        
        for(String matchedKey : masterKeyAndValueMap.get(searchType).keySet()){
            Map<String,String> matchedValues = new Map<String,String>();
            matchedValues = masterKeyAndValueMap.get(searchType).get(matchedKey);
            Map<String,String> fieldValueMap = new Map<String,String>();
            fieldValueMap.put(generateKey(searchType).get('level_1_Key'), matchedValues.get(generateKey(searchType).get('level_1_Key')));
            fieldValueMap.put(generateKey(searchType).get('level_2_Key'), matchedValues.get(generateKey(searchType).get('level_2_Key')));
            fieldValueMap.put(generateKey(searchType).get('level_3_Key'), matchedValues.get(generateKey(searchType).get('level_3_Key')));
            fieldValueMap.put(generateKey(searchType).get('level_4_Key'), matchedValues.get(generateKey(searchType).get('level_4_Key')));
            fieldValueMap.put(generateKey(searchType).get('level_5_Key'), matchedValues.get(generateKey(searchType).get('level_5_Key')));
            if(searchType == 'Case Categorization'){ //Case Categorization has 7 dependent picklists instead of 5 for Call Log or Case Assignment
                fieldValueMap.put(generateKey(searchType).get('level_6_Key'), matchedValues.get(generateKey(searchType).get('level_6_Key')));
                fieldValueMap.put(generateKey(searchType).get('level_7_Key'), matchedValues.get(generateKey(searchType).get('level_7_Key')));
            }
            returnList.add(new PicklistSearchResult(fieldValueMap, searchType));
        }
        searchResultJSON = JSON.serialize(returnList);
        return searchResultJSON;
    }
    
    public static Map<String,String> generateKey(String searchType){
        Map<String,String> returnMap = new Map<String,String>();
        if(searchType == 'Call Log'){
            returnMap.put('level_1_Key','Level-1');
            returnMap.put('level_2_Key','Level-2');
            returnMap.put('level_3_Key','Level-3');
            returnMap.put('level_4_Key','Level-4');
            returnMap.put('level_5_Key','Level-5');
        }
        if(searchType == 'Case Assignment'){
            returnMap.put('level_1_Key','Branch');
            returnMap.put('level_2_Key','ContactReason');
            returnMap.put('level_3_Key','ReasonDetail');
            returnMap.put('level_4_Key','CDH');
            returnMap.put('level_5_Key','BranchDepartment');
        }
        if(searchType == 'Case Categorization'){
            returnMap.put('level_1_Key','primaryCategory');
            returnMap.put('level_2_Key','areaOfTheBusiness');
            returnMap.put('level_3_Key','primaryReason');
            returnMap.put('level_4_Key','buyingDirectorate');
            returnMap.put('level_5_Key','primaryReasonDetail');
            returnMap.put('level_6_Key','productGroup');
            returnMap.put('level_7_Key','primaryDetailExplanation');
        }
        return returnMap;
    }
    
    public static Map<String,Map<String,String>> prepareMasterMap(List<String> level1Values, Map<String,Map<String,List<String>>> level2Onwards, String searchType, String searchTerm){
        
        if(searchType == 'Call Log'){
            return prepareMasterMapForCallLog(level1Values,level2Onwards,searchType,searchTerm);
        }
        if(searchType == 'Case Assignment'){
            return prepareMasterMapForCaseAssignment(level1Values,level2Onwards,searchType,searchTerm);
        }
        if(searchType == 'Case Categorization'){
            return prepareMasterMapForCaseCategorization(level1Values,level2Onwards,searchType,searchTerm);
        }
        return new Map<String,Map<String,String>>();
    }
    
    public static Map<String,Map<String,String>> prepareMasterMapForCallLog(List<String> level1Values, Map<String,Map<String,List<String>>> level2Onwards, String searchType, String searchTerm){
        Map<String,Map<String,String>> masterKeyAndValueMap = new Map<String,Map<String,String>>();
        String level_1_key = generateKey(searchType).get('level_1_Key');
        String level_2_Key = generateKey(searchType).get('level_2_Key');
        String level_3_Key = generateKey(searchType).get('level_3_Key');
        String level_4_Key = generateKey(searchType).get('level_4_Key');
        String level_5_Key = generateKey(searchType).get('level_5_Key');
        String complexKey = '';
        
        for(String levelOne : level1Values){
            if(levelOne != NULL && level2Onwards.get(level_2_Key).containsKey(levelOne) && level2Onwards.get(level_2_Key).get(levelOne).size()>0){
                for(String levelTwo : level2Onwards.get(level_2_Key).get(levelOne)){
                    if(level2Onwards.get(level_3_Key).containsKey(levelTwo) && level2Onwards.get(level_3_Key).get(levelTwo).size()>0){
                        for(String levelThree : level2Onwards.get(level_3_Key).get(levelTwo)){
                            if(level2Onwards.get(level_4_Key).containsKey(levelThree) && level2Onwards.get(level_4_Key).get(levelThree).size()>0){
                                for(String levelFour : level2Onwards.get(level_4_Key).get(levelThree)){
                                    if(level2Onwards.get(level_5_Key).containsKey(levelFour) && level2Onwards.get(level_5_Key).get(levelFour).size()>0){
                                        for(String levelFive : level2Onwards.get(level_5_Key).get(levelFour)){
                                            complexKey = prepareKey(levelOne,levelTwo,levelThree,levelFour,levelFive,null,null);
                                            if(keywordSearch(complexKey, searchTerm)){
                                                masterKeyAndValueMap.put(complexKey,prepareValue(levelOne,levelTwo,levelThree,levelFour,levelFive,null,null,searchType));
                                            }
                                        }
                                    }
                                    else{
                                        complexKey = prepareKey(levelOne,levelTwo,levelThree,levelFour,null,null,null);
                                        if(keywordSearch(complexKey, searchTerm)){
                                            masterKeyAndValueMap.put(complexKey,prepareValue(levelOne,levelTwo,levelThree,levelFour,null,null,null,searchType));
                                        }
                                    }
                                }
                            }
                            else{
                                complexKey = prepareKey(levelOne,levelTwo,levelThree,null,null,null,null);
                                if(keywordSearch(complexKey, searchTerm)){
                                    masterKeyAndValueMap.put(complexKey,prepareValue(levelOne,levelTwo,levelThree,null,null,null,null,searchType));
                                }
                            }
                        }
                    }
                    else{
                        complexKey = prepareKey(levelOne,levelTwo,null,null,null,null,null);
                        if(keywordSearch(complexKey, searchTerm)){
                            masterKeyAndValueMap.put(complexKey,prepareValue(levelOne,levelTwo,null,null,null,null,null,searchType));
                        }
                    }
                }
            }
            else{
                complexKey = prepareKey(levelOne,null,null,null,null,null,null);
                if(keywordSearch(complexKey, searchTerm)){
                    masterKeyAndValueMap.put(complexKey,prepareValue(levelOne,null,null,null,null,null,null,searchType));
                }
            }
        }
        return masterKeyAndValueMap;
    }
    
    public static Map<String,Map<String,String>> prepareMasterMapForCaseAssignment(List<String> level1Values, Map<String,Map<String,List<String>>> level2Onwards, String searchType, String searchTerm){
        Map<String,Map<String,String>> masterKeyAndValueMap = new Map<String,Map<String,String>>();
        String level_1_key = generateKey(searchType).get('level_1_Key');
        String level_2_Key = generateKey(searchType).get('level_2_Key');
        String level_3_Key = generateKey(searchType).get('level_3_Key');
        String level_4_Key = generateKey(searchType).get('level_4_Key');
        String level_5_Key = generateKey(searchType).get('level_5_Key');
        String complexKey = '';
        
        for(String levelOne : level1Values){
            if(levelOne != NULL && level2Onwards.get(level_2_Key).containsKey(levelOne) && level2Onwards.get(level_2_Key).get(levelOne).size()>0){
                for(String levelTwo : level2Onwards.get(level_2_Key).get(levelOne)){
                    if(level2Onwards.get(level_3_Key).containsKey(levelTwo) && level2Onwards.get(level_3_Key).get(levelTwo).size()>0){
                        for(String levelThree : level2Onwards.get(level_3_Key).get(levelTwo)){
                            if(level2Onwards.get(level_4_Key).containsKey(levelThree) && level2Onwards.get(level_4_Key).get(levelThree).size()>0){
                                for(String levelFour : level2Onwards.get(level_4_Key).get(levelThree)){
                                    complexKey = prepareKey(levelOne,levelTwo,levelThree,levelFour,null,null,null);
                                    if(keywordSearch(complexKey, searchTerm)){
                                        masterKeyAndValueMap.put(complexKey,prepareValue(levelOne,levelTwo,levelThree,levelFour,null,null,null,searchType));
                                    }
                                }
                            }
                            else if(level2Onwards.get(level_5_Key).containsKey(levelThree) && level2Onwards.get(level_5_Key).get(levelThree).size()>0){
                                for(String levelFive : level2Onwards.get(level_5_Key).get(levelThree)){
                                    complexKey = prepareKey(levelOne,levelTwo,levelThree,null,levelFive,null,null);
                                    if(keywordSearch(complexKey, searchTerm)){
                                        masterKeyAndValueMap.put(complexKey,prepareValue(levelOne,levelTwo,levelThree,null,levelFive,null,null,searchType));
                                    }
                                }
                            }
                            else{
                                complexKey = prepareKey(levelOne,levelTwo,levelThree,null,null,null,null);
                                if(keywordSearch(complexKey, searchTerm)){
                                    masterKeyAndValueMap.put(complexKey,prepareValue(levelOne,levelTwo,levelThree,null,null,null,null,searchType));
                                }
                            }
                        }
                    }
                    else{
                        complexKey = prepareKey(levelOne,levelTwo,null,null,null,null,null);
                        if(keywordSearch(complexKey, searchTerm)){
                            masterKeyAndValueMap.put(complexKey,prepareValue(levelOne,levelTwo,null,null,null,null,null,searchType));
                        }
                    }
                }
            }
            else{
                complexKey = prepareKey(levelOne,null,null,null,null,null,null);
                if(keywordSearch(complexKey, searchTerm)){
                    masterKeyAndValueMap.put(complexKey,prepareValue(levelOne,null,null,null,null,null,null,searchType));
                }
            }
        }
        return masterKeyAndValueMap;
    }
    
    public static Map<String,Map<String,String>> prepareMasterMapForCaseCategorization(List<String> level1Values, Map<String,Map<String,List<String>>> level2Onwards, String searchType, String searchTerm){
        Map<String,Map<String,String>> masterKeyAndValueMap = new Map<String,Map<String,String>>();
        String level_1_key = generateKey(searchType).get('level_1_Key');
        String level_2_Key = generateKey(searchType).get('level_2_Key');
        String level_3_Key = generateKey(searchType).get('level_3_Key');
        String level_4_Key = generateKey(searchType).get('level_4_Key');
        String level_5_Key = generateKey(searchType).get('level_5_Key');
        String level_6_Key = generateKey(searchType).get('level_6_Key');
        String level_7_Key = generateKey(searchType).get('level_7_Key');
        String complexKey = '';
        
        for(String levelOne : level1Values){
            if(levelOne != NULL && level2Onwards.get(level_2_Key).containsKey(levelOne) && level2Onwards.get(level_2_Key).get(levelOne).size()>0){ //Level-2
                for(String levelTwo : level2Onwards.get(level_2_Key).get(levelOne)){
                    complexKey = prepareKey(levelOne,levelTwo,null,null,null,null,null);
                    if(keywordSearch(complexKey, searchTerm)){
                        masterKeyAndValueMap.put(complexKey,prepareValue(levelOne,levelTwo,null,null,null,null,null,searchType));
                    }
                }
            }
            else if(level2Onwards.get(level_3_Key).containsKey(levelOne) && level2Onwards.get(level_3_Key).get(levelOne).size()>0){ //Level-3
                for(String levelThree : level2Onwards.get(level_3_Key).get(levelOne)){
                    if(level2Onwards.get(level_4_Key).containsKey(levelThree) && level2Onwards.get(level_4_Key).get(levelThree).size()>0){ //Level-4
                        for(String levelFour : level2Onwards.get(level_4_Key).get(levelThree)){
                            if(level2Onwards.get(level_6_Key).containsKey(levelFour) && level2Onwards.get(level_6_Key).get(levelFour).size()>0){ //Level-6
                                for(String levelSix : level2Onwards.get(level_6_Key).get(levelFour)){
                                    complexKey = prepareKey(levelOne,null,levelThree,levelFour,null,levelSix,null);
                                    if(keywordSearch(complexKey, searchTerm)){
                                        masterKeyAndValueMap.put(complexKey,prepareValue(levelOne,null,levelThree,levelFour,null,levelSix,null,searchType));
                                    }
                                }
                            }
                            else{
                                complexKey = prepareKey(levelOne,null,levelThree,levelFour,null,null,null);
                                if(keywordSearch(complexKey, searchTerm)){
                                    masterKeyAndValueMap.put(complexKey,prepareValue(levelOne,null,levelThree,levelFour,null,null,null,searchType));
                                }
                            }
                        }
                    }
                    else if(level2Onwards.get(level_5_Key).containsKey(levelThree) && level2Onwards.get(level_5_Key).get(levelThree).size()>0){ //Level-5
                        for(String levelFive : level2Onwards.get(level_5_Key).get(levelThree)){
                            if(level2Onwards.get(level_7_Key).containsKey(levelFive) && level2Onwards.get(level_7_Key).get(levelFive).size()>0){ //Level-7
                                for(String levelSeven : level2Onwards.get(level_7_Key).get(levelFive)){
                                    complexKey = prepareKey(levelOne,null,levelThree,null,levelFive,null,levelSeven);
                                    if(keywordSearch(complexKey, searchTerm)){
                                        masterKeyAndValueMap.put(complexKey,prepareValue(levelOne,null,levelThree,null,levelFive,null,levelSeven,searchType));
                                    }
                                }
                            }
                            else{
                                complexKey = prepareKey(levelOne,null,levelThree,null,levelFive,null,null);
                                if(keywordSearch(complexKey, searchTerm)){
                                    masterKeyAndValueMap.put(complexKey,prepareValue(levelOne,null,levelThree,null,levelFive,null,null,searchType));
                                }
                            }
                        }
                    }
                    else{
                        complexKey = prepareKey(levelOne,null,levelThree,null,null,null,null);
                        if(keywordSearch(complexKey, searchTerm)){
                            masterKeyAndValueMap.put(complexKey,prepareValue(levelOne,null,levelThree,null,null,null,null,searchType));
                        }
                    }
                }
            }
            else{
                complexKey = prepareKey(levelOne,null,null,null,null,null,null);
                if(keywordSearch(complexKey, searchTerm)){
                    masterKeyAndValueMap.put(complexKey,prepareValue(levelOne,null,null,null,null,null,null,searchType));
                }
            }
        }
        return masterKeyAndValueMap;
    }
    
    public static String prepareKey(String levelOne, String levelTwo, String levelThree, String levelFour, String levelFive, String levelSix, String levelSeven){
        String returnKey = !String.isEmpty(levelOne) ? levelOne : '';
        if(!String.isEmpty(levelTwo)){
            returnKey+='->'+levelTwo;
        }
        if(!String.isEmpty(levelThree)){
            returnKey+='->'+levelThree;
        }
        if(!String.isEmpty(levelFour)){
            returnKey+='->'+levelFour;
        }
        if(!String.isEmpty(levelFive)){
            returnKey+='->'+levelFive;
        }
        if(!String.isEmpty(levelSix)){
            returnKey+='->'+levelSix;
        }
        if(!String.isEmpty(levelSeven)){
            returnKey+='->'+levelSeven;
        }
        return returnKey;
    }
    
    public static Map<String,String> prepareValue(String levelOne, String levelTwo, String levelThree, String levelFour, String levelFive, String levelSix, String levelSeven, String searchType){
        Map<String,String> returnMap = new Map<String,String>();
        String level_1_key = generateKey(searchType).get('level_1_Key');
        String level_2_Key = generateKey(searchType).get('level_2_Key');
        String level_3_Key = generateKey(searchType).get('level_3_Key');
        String level_4_Key = generateKey(searchType).get('level_4_Key');
        String level_5_Key = generateKey(searchType).get('level_5_Key');
        String level_6_Key = searchType == 'Case Categorization' ? generateKey(searchType).get('level_6_Key') : null;
        String level_7_Key = searchType == 'Case Categorization' ? generateKey(searchType).get('level_7_Key') : null;
        
        returnMap.put(level_1_key,levelOne != null ? levelOne : '');
        if(!String.isEmpty(levelTwo)){
            returnMap.put(level_2_Key,levelTwo);
        }
        if(!String.isEmpty(levelThree)){
            returnMap.put(level_3_Key,levelThree);
        }
        if(!String.isEmpty(levelFour)){
            returnMap.put(level_4_Key,levelFour);
        }
        if(!String.isEmpty(levelFive)){
            returnMap.put(level_5_Key,levelFive);
        }
        if(!String.isEmpty(levelSix)){
            returnMap.put(level_6_Key,levelSix);
        }
        if(!String.isEmpty(levelSeven)){
            returnMap.put(level_7_Key,levelSeven);
        }
        return returnMap;
    }
    
    public static Boolean keywordSearch(String input, String searchQuery)
    {
        Boolean containsFlag = true;
        for(String searchStringValue : searchQuery.split(' ')){
            containsFlag = containsFlag && input.containsIgnoreCase(searchStringValue);
        }
        return containsFlag;
    }
    
    public class PicklistSearchResult{
        //Call Log picklist fields
        public String levelOne {get;set;}
        public String levelTwo {get;set;} 
        public String levelThree {get;set;} 
        public String levelFour {get;set;} 
        public String levelFive {get;set;}
        
        //Case Assignment picklist fields
        public String branch {get;set;}
        public String contactReason {get;set;} 
        public String reasonDetail {get;set;} 
        public String CDH {get;set;} 
        public String branchDepartment {get;set;} 
        
        //Case Categorization picklist fields
        public String primaryCategory {get;set;}
        public String primaryReason {get;set;} 
        public String areaOfTheBusiness {get;set;} 
        public String primaryReasonDetail {get;set;} 
        public String primaryDetailExplanation {get;set;}
        public String buyingDirectorate {get;set;} 
        public String productGroup {get;set;}
        
        PicklistSearchResult(Map<String,String> fieldValueMap, String searchType){
            if(searchType == 'Call Log'){
                this.levelOne = !String.isEmpty(fieldValueMap.get(generateKey(searchType).get('level_1_Key'))) ? fieldValueMap.get(generateKey(searchType).get('level_1_Key')) : '';
                this.levelTwo = !String.isEmpty(fieldValueMap.get(generateKey(searchType).get('level_2_Key'))) ? fieldValueMap.get(generateKey(searchType).get('level_2_Key')) : '';
                this.levelThree = !String.isEmpty(fieldValueMap.get(generateKey(searchType).get('level_3_Key'))) ? fieldValueMap.get(generateKey(searchType).get('level_3_Key')) : '';
                this.levelFour = !String.isEmpty(fieldValueMap.get(generateKey(searchType).get('level_4_Key'))) ? fieldValueMap.get(generateKey(searchType).get('level_4_Key')) : '';
                this.levelFive = !String.isEmpty(fieldValueMap.get(generateKey(searchType).get('level_5_Key'))) ? fieldValueMap.get(generateKey(searchType).get('level_5_Key')) : '';
            }
            if(searchType == 'Case Assignment'){
                this.branch = !String.isEmpty(fieldValueMap.get(generateKey(searchType).get('level_1_Key'))) ? fieldValueMap.get(generateKey(searchType).get('level_1_Key')) : '';
                this.contactReason = !String.isEmpty(fieldValueMap.get(generateKey(searchType).get('level_2_Key'))) ? fieldValueMap.get(generateKey(searchType).get('level_2_Key')) : '';
                this.reasonDetail = !String.isEmpty(fieldValueMap.get(generateKey(searchType).get('level_3_Key'))) ? fieldValueMap.get(generateKey(searchType).get('level_3_Key')) : '';
                this.CDH = !String.isEmpty(fieldValueMap.get(generateKey(searchType).get('level_4_Key'))) ? fieldValueMap.get(generateKey(searchType).get('level_4_Key')) : '';
                this.branchDepartment = !String.isEmpty(fieldValueMap.get(generateKey(searchType).get('level_5_Key'))) ? fieldValueMap.get(generateKey(searchType).get('level_5_Key')) : '';
            }
            if(searchType == 'Case Categorization'){
                this.primaryCategory = !String.isEmpty(fieldValueMap.get(generateKey(searchType).get('level_1_Key'))) ? fieldValueMap.get(generateKey(searchType).get('level_1_Key')) : '';
                this.areaOfTheBusiness = !String.isEmpty(fieldValueMap.get(generateKey(searchType).get('level_2_Key'))) ? fieldValueMap.get(generateKey(searchType).get('level_2_Key')) : '';
                this.primaryReason = !String.isEmpty(fieldValueMap.get(generateKey(searchType).get('level_3_Key'))) ? fieldValueMap.get(generateKey(searchType).get('level_3_Key')) : '';
                this.buyingDirectorate = !String.isEmpty(fieldValueMap.get(generateKey(searchType).get('level_4_Key'))) ? fieldValueMap.get(generateKey(searchType).get('level_4_Key')) : '';
                this.primaryReasonDetail = !String.isEmpty(fieldValueMap.get(generateKey(searchType).get('level_5_Key'))) ? fieldValueMap.get(generateKey(searchType).get('level_5_Key')) : '';
                this.productGroup = !String.isEmpty(fieldValueMap.get(generateKey(searchType).get('level_6_Key'))) ? fieldValueMap.get(generateKey(searchType).get('level_6_Key')) : '';
                this.primaryDetailExplanation = !String.isEmpty(fieldValueMap.get(generateKey(searchType).get('level_7_Key'))) ? fieldValueMap.get(generateKey(searchType).get('level_7_Key')) : '';
            }
        }
    }
}