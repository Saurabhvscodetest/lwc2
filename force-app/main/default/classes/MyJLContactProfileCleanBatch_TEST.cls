/**
 * Unit tests for MyJLContactProfileCleanBatch class methods.
 */
@isTest
private class MyJLContactProfileCleanBatch_TEST {

	/**
	 * Code coverage for MyJLContactProfileCleanBatch_TEST.execute(SchedulableContext sc)
	 */
	static testMethod void testSchedulerExecuteMethod() {
    	system.debug('TEST START: testStartMethod');
		MyJL_Cleanup_Process_Settings__c c1 = new MyJL_Cleanup_Process_Settings__c(Name = MyJLContactProfileCleanBatch.CLASS_NAME, Record_Updates_Enabled__c = true, Email_Report_Recipient_List__c = 'matthew.povey@johnlewis.co.uk', Email_Report_Enabled__c = true, Process_Enabled__c = true);
		insert c1;

		SchedulableContext sc = null;
		MyJLContactProfileCleanBatch cpdBatch = new MyJLContactProfileCleanBatch();

		Test.startTest();
		cpdBatch.execute(sc);
		Test.stopTest();

    	system.debug('TEST END: testStartMethod');
	}
	
	/**
	 * Code coverage for MyJLContactProfileCleanBatch.start
	 */
	static testMethod void testStartMethod() {
    	system.debug('TEST START: testStartMethod');

       	Contact testCon = UnitTestDataFactory.createContact(1, true);
		Contact_Profile__c testContactProfile1 = new Contact_Profile__c(Contact__c = testCon.Id, Name = 'TEST PROFILE 1', Email__c = testCon.Email);
		Contact_Profile__c testContactProfile2 = new Contact_Profile__c(Contact__c = testCon.Id);
		Contact_Profile__c testContactProfile3 = new Contact_Profile__c(Contact__c = testCon.Id);
		List<Contact_Profile__c> cpList = new List<Contact_Profile__c> {testContactProfile1, testContactProfile2, testContactProfile3};
		insert cpList;
		
		Database.BatchableContext bc = null;
		MyJLContactProfileCleanBatch cpdBatch = new MyJLContactProfileCleanBatch();
		Test.startTest();
    	Database.QueryLocator dql = cpdBatch.start(bc);	
		Test.stopTest();

    	system.debug('TEST END: testStartMethod');
	}

	/**
	 * Test MyJLContactProfileCleanBatch.execute
	 */
	static testMethod void testExecuteMethod() {
    	system.debug('TEST START: testExecuteMethod');

		MyJL_Cleanup_Process_Settings__c c1 = new MyJL_Cleanup_Process_Settings__c(Name = MyJLContactProfileCleanBatch.CLASS_NAME, Record_Updates_Enabled__c = true, Email_Report_Recipient_List__c = 'matthew.povey@johnlewis.co.uk', Email_Report_Enabled__c = true, Process_Enabled__c = true);
		insert c1;
       	Contact testCon = UnitTestDataFactory.createContact(1, true);
		Contact_Profile__c testContactProfile1 = new Contact_Profile__c(Contact__c = testCon.Id, Name = 'TEST PROFILE 1', Email__c = testCon.Email);
		Contact_Profile__c testContactProfile2 = new Contact_Profile__c(Contact__c = testCon.Id);
		Contact_Profile__c testContactProfile3 = new Contact_Profile__c(Contact__c = testCon.Id);
		List<Contact_Profile__c> cpList = new List<Contact_Profile__c> {testContactProfile1, testContactProfile2, testContactProfile3};
		insert cpList;
		Loyalty_Account__c la1 = new Loyalty_Account__c(Customer_Profile__c = testContactProfile1.Id, IsActive__c = true, Scheme_Joined_Date_Time__c = system.now().addMonths(-6), Activation_Date_Time__c = system.now().addMonths(-6), Email_Address__c = testCon.Email);
		Loyalty_Account__c la2 = new Loyalty_Account__c(Customer_Profile__c = testContactProfile1.Id, IsActive__c = true, Scheme_Joined_Date_Time__c = system.now().addMonths(-3), Activation_Date_Time__c = system.now().addMonths(-3), Email_Address__c = testCon.Email);
		Loyalty_Account__c la3 = new Loyalty_Account__c(Customer_Profile__c = testContactProfile2.Id, IsActive__c = true, Scheme_Joined_Date_Time__c = system.now().addMonths(-6), Activation_Date_Time__c = system.now().addMonths(-6), Email_Address__c = testCon.Email);
		Loyalty_Account__c la4 = new Loyalty_Account__c(Customer_Profile__c = testContactProfile2.Id, IsActive__c = true, Scheme_Joined_Date_Time__c = system.now().addMonths(-3), Activation_Date_Time__c = system.now().addMonths(-3), Email_Address__c = testCon.Email);
		List<Loyalty_Account__c> laList = new List<Loyalty_Account__c> {la1, la2, la3, la4};
		insert laList;
		
		List<Contact_Profile__c> testCPList0 = [SELECT Id, Name, Contact__c, Mailing_Street__c, 
   	    											   Contact__r.Name, Shopper_ID__c
       											FROM Contact_Profile__c 
       											WHERE Id IN :cpList];
		system.assertEquals(3, testCPList0.size());

		List<Contact_Profile__c> testCPList1 = [SELECT Id, Name, Contact__c, Mailing_Street__c, 
   	    											   Contact__r.Name, Shopper_ID__c, 
   	    											   (SELECT Id, Name, Membership_Number__c, Scheme_Joined_Date_Time__c, Activation_Date_Time__c, Deactivation_Date_Time__c, IsActive__c, Channel_ID__c FROM MyJL_Accounts__r)
       											FROM Contact_Profile__c 
       											WHERE Name LIKE 'a00%' AND Mailing_Street__c = null AND Contact__r.Contact_Profile_Count__c > 1 AND Id IN :cpList];

		system.assertEquals(2, testCPList1.size());
		
		Database.BatchableContext bc = null;
		MyJLContactProfileCleanBatch cpdBatch = new MyJLContactProfileCleanBatch();
		
		test.startTest();
		cpdBatch.execute(bc, testCPList1);
		test.stopTest();
		
		List<Contact_Profile__c> testCPList2 = [SELECT Id, Name, Contact__c, Mailing_Street__c, 
   	    											   Contact__r.Name, Shopper_ID__c
       											FROM Contact_Profile__c 
       											WHERE Id IN :cpList];
		system.assertEquals(1, testCPList2.size());
		
		Contact_Profile__c testCP = testCPList2.get(0);
		system.assertEquals(testContactProfile1.Id, testCP.Id);

    	system.debug('TEST END: testExecuteMethod');
	}

    /**
     * Code coverage for MyJLContactProfileCleanBatch.finish()
     */
    static testMethod void testFinishMethod() {
    	system.debug('TEST START: testFinishMethod');

		MyJL_Cleanup_Process_Settings__c c1 = new MyJL_Cleanup_Process_Settings__c(Name = MyJLContactProfileCleanBatch.CLASS_NAME, Record_Updates_Enabled__c = true, Email_Report_Recipient_List__c = 'matthew.povey@johnlewis.co.uk', Email_Report_Enabled__c = true, Process_Enabled__c = true);
		insert c1;
    	Config_Settings__c defaultNotificationEmail = new Config_Settings__c(Name = 'EMAIL_NOTIFICATION_DEFAULT_ADDRESS', Text_Value__c = 'Matthew.Povey@johnlewis.co.uk');
		insert defaultNotificationEmail;

		Database.BatchableContext bc = null;
		MyJLContactProfileCleanBatch cpdBatch = new MyJLContactProfileCleanBatch();
		cpdBatch.totalRecordCount = 10;

		Test.startTest();
		cpdBatch.finish(bc);
		Test.stopTest();

    	system.debug('TEST END: testFinishMethod');
    }

    /**
     * Code coverage for MyJLCleanBatchBase.sendErrorEmail()
     */
    static testMethod void testSendErrorEmail() {
    	system.debug('TEST START: testSendErrorEmail');

		MyJL_Cleanup_Process_Settings__c c1 = new MyJL_Cleanup_Process_Settings__c(Name = MyJLContactProfileCleanBatch.CLASS_NAME, Record_Updates_Enabled__c = true, Email_Report_Recipient_List__c = 'matthew.povey@johnlewis.co.uk', Email_Report_Enabled__c = true, Process_Enabled__c = true);
		insert c1;
    	Config_Settings__c defaultNotificationEmail = new Config_Settings__c(Name = 'EMAIL_NOTIFICATION_DEFAULT_ADDRESS', Text_Value__c = 'Matthew.Povey@johnlewis.co.uk');
		insert defaultNotificationEmail;

		Database.BatchableContext bc = null;
		MyJLContactProfileCleanBatch cpdBatch = new MyJLContactProfileCleanBatch();

		Test.startTest();
		cpdBatch.sendErrorEmail('TEST SUBJECT', 'TEST MESSAGE');
		Test.stopTest();

    	system.debug('TEST END: testSendErrorEmail');
    }
}