//
// There are three dates on a Case
// - Last Call Contact (dtm1)
// - Last Email Contact (dtm2)
// - Last Chat Contact (dtm3)
//
// These feed in to the formula that calculates the Last Customer Contact
//
// The pattern that is used is that we compare Call and Email in the Formula
// - Last Email or Call Contact
//
// And then we take the result and compare the Last Chat Contact in
// - Last Customer Contact
//
// In other words we are comparing dtm1 and dtm2 in one formula and then feeding the result into the final formula and comparing with 
// dtm3
//
@isTest
private class LastContact_TEST {

	private static final DateTime EARLIEST_DTM = system.today() - 2;
	private static final DateTime MID_DTM = system.today() - 1;
	private static final DateTime RECENT_DTM = system.today();
	
	//
	// Tests where dtm1 is null 
	//

    // test dtm2 > dtm3
    private static testMethod void test_23() {
		test(null, RECENT_DTM, EARLIEST_DTM, RECENT_DTM);
    }
    
    // test dtm3 > dtm2
    private static testMethod void test_32() {
		test(null, EARLIEST_DTM, RECENT_DTM, RECENT_DTM);
    }
    
    
	//
	// Tests where dtm2 is null 
	//

    // test dtm1 > dtm3
    private static testMethod void test_13() {
		test(RECENT_DTM, null, EARLIEST_DTM, RECENT_DTM);
    }
    
    // test dtm3 > dtm1
    private static testMethod void test_31() {
		test(EARLIEST_DTM, null, RECENT_DTM, RECENT_DTM);
    }

	//
	// Tests where dtm3 is null 
	//

    // test dtm1 > dtm2
    private static testMethod void test_12() {
		test(RECENT_DTM, EARLIEST_DTM, null, RECENT_DTM);
    }
    
    // test dtm2 > dtm1
    private static testMethod void test_21() {
		test(EARLIEST_DTM, RECENT_DTM, null, RECENT_DTM);
    }

	//
	// Tests where dtm1 and dtm2 are null 
	//

    // test dtm3
    private static testMethod void test_3() {
		test(null, null, RECENT_DTM, RECENT_DTM);
    }
	
	//
	// Tests where dtm1 and dtm3 are null 
	//

    // test dtm2
    private static testMethod void test_2() {
		test(null, RECENT_DTM, null, RECENT_DTM);
    }

	//
	// Tests where dtm2 and dtm3 are null 
	//

    // test dtm1
    private static testMethod void test_1() {
		test(RECENT_DTM, null, null, RECENT_DTM);
    }

	//
	// Tests where dtm1 and dtm2 and dtm3 are null 
	//
	private static testMethod void test() {		
		test(null, null, null, null);
	}

	//
	// Tests where all dates are NON NULL
	//

    // test dtm1 > dtm2 > dtm3
    private static testMethod void test_123() {
		test(RECENT_DTM, MID_DTM, EARLIEST_DTM, RECENT_DTM);
    }
    
    // test dtm1 > dtm3 > dtm2
    private static testMethod void test_132() {
		test(RECENT_DTM, EARLIEST_DTM, MID_DTM, RECENT_DTM);
    }
    
    // test dtm2 > dtm1 > dtm3
    private static testMethod void test_213() {
		test(MID_DTM, RECENT_DTM, EARLIEST_DTM, RECENT_DTM);
    }  
    
    // test dtm2 > dtm3 > dtm1
    private static testMethod void test_231() {
		test(EARLIEST_DTM, RECENT_DTM, MID_DTM, RECENT_DTM);
    }
    
    // test dtm3 > dtm1 > dtm2
    private static testMethod void test_312() {
		test(MID_DTM, EARLIEST_DTM, RECENT_DTM, RECENT_DTM);
    } 
    
    // test dtm3 > dtm2 > dtm1
    private static testMethod void test_321() {
		test(EARLIEST_DTM, MID_DTM, RECENT_DTM, RECENT_DTM);
    } 
    
    // the test
    private static void test(DateTime lastCallDtm, DateTime lastEmailDtm, DateTime lastChatDtm, DateTime expectedDtm) {
    	UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
    	// create a case
		UnitTestDataFactory.createTeamAndContactCentre();
		UnitTestDataFactory.createEmailContactCentre();

        Contact con = UnitTestDataFactory.createContact();
        con.Email = 'a@bb.com';
        insert con;
        
        Contact_Profile__c cp = UnitTestDataFactory.createContactProfile(con);
        cp.Email__c = 'a@bb.co.uk';
        
        insert cp;
        
        Case c = UnitTestDataFactory.createNormalCase(new Contact());
        c.SuppliedEmail = 'a@bb.co.uk';
        c.Origin = 'Email2Case';
		if (lastCallDtm != null) {
			c.Last_Call_Contact__c = lastCallDtm;
		}
		if (lastEmailDtm != null) {
			c.Last_Email_Contact__c = lastEmailDtm;
		}
		if (lastChatDtm != null) {
			c.Last_Chat_Contact__c = lastChatDtm;
		}
		c.RecordTypeId = getRecordTypeId('Query');
		
		Test.StartTest();

        insert c;
    	
		Test.StopTest();

    	// Requery
    	List<Case> caseList = [SELECT Last_Customer_Contact__c FROM Case WHERE Id=:c.Id];
		system.assertEquals(1, caseList.size());
		
		// check date and time
		system.assertEquals(expectedDtm, caseList[0].Last_Customer_Contact__c);
		
    }

	static Id getRecordTypeId(String recordTypeName) {
		List<RecordType> caseRecordTypes = [Select Id, SobjectType, DeveloperName From RecordType WHERE SobjectType='Case' AND DeveloperName=:recordTypeName];
		system.assertEquals(1, caseRecordTypes.size());
		return caseRecordTypes[0].Id;		
	}
                     
}