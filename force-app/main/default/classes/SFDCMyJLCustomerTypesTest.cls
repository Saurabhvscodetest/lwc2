/**
 * Test for the Web Service Interface
 */
@isTest
private class SFDCMyJLCustomerTypesTest {

    static testMethod void InteractionContent() {
    	SFDCMyJLCustomerTypes.InteractionContent ctObj = new SFDCMyJLCustomerTypes.InteractionContent();
    	system.assertNotEquals(null, ctObj);
     }
    
    static testMethod void PreferenceType() {
   		SFDCMyJLCustomerTypes.PreferenceType ctObj = new SFDCMyJLCustomerTypes.PreferenceType();
    	system.assertNotEquals(null, ctObj);
    }
    
    static testMethod void PartyRoleInteractionThread() {
   		SFDCMyJLCustomerTypes.PartyRoleInteractionThread ctObj = new SFDCMyJLCustomerTypes.PartyRoleInteractionThread();
    	system.assertNotEquals(null, ctObj);
    }
    
    static testMethod void CustomerIDType() {
   		SFDCMyJLCustomerTypes.CustomerIDType ctObj = new SFDCMyJLCustomerTypes.CustomerIDType();
    	system.assertNotEquals(null, ctObj);
    }
    
    static testMethod void CustomerPreference() {
   		SFDCMyJLCustomerTypes.CustomerPreference ctObj = new SFDCMyJLCustomerTypes.CustomerPreference();
    	system.assertNotEquals(null, ctObj);
    }
    static testMethod void PartyRoleAssociation() {
   		SFDCMyJLCustomerTypes.PartyRoleAssociation ctObj = new SFDCMyJLCustomerTypes.PartyRoleAssociation();
    	system.assertNotEquals(null, ctObj);
    }
    static testMethod void CustomerOccasionType() {
   		SFDCMyJLCustomerTypes.CustomerOccasionType ctObj = new SFDCMyJLCustomerTypes.CustomerOccasionType();
    	system.assertNotEquals(null, ctObj);
    }
    static testMethod void CustomerAccountCardAssignment() {
   		SFDCMyJLCustomerTypes.CustomerAccountCardAssignment ctObj = new SFDCMyJLCustomerTypes.CustomerAccountCardAssignment();
    	system.assertNotEquals(null, ctObj);
    }
    static testMethod void PartyRoleInteractionThreadStatus() {
   		SFDCMyJLCustomerTypes.PartyRoleInteractionThreadStatus ctObj = new SFDCMyJLCustomerTypes.PartyRoleInteractionThreadStatus();
    	system.assertNotEquals(null, ctObj);
    }
    static testMethod void CustomerOccasion() {
   		SFDCMyJLCustomerTypes.CustomerOccasion ctObj = new SFDCMyJLCustomerTypes.CustomerOccasion();
    	system.assertNotEquals(null, ctObj);
    }
    static testMethod void PartyType() {
   		SFDCMyJLCustomerTypes.PartyType ctObj = new SFDCMyJLCustomerTypes.PartyType();
    	system.assertNotEquals(null, ctObj);
    }
    static testMethod void Flag() {
   		SFDCMyJLCustomerTypes.Flag ctObj = new SFDCMyJLCustomerTypes.Flag();
    	system.assertNotEquals(null, ctObj);
    }
    static testMethod void Money() {
   		SFDCMyJLCustomerTypes.Money ctObj = new SFDCMyJLCustomerTypes.Money();
    	system.assertNotEquals(null, ctObj);
    }
    static testMethod void PartyRoleInteractionType() {
   		SFDCMyJLCustomerTypes.PartyRoleInteractionType ctObj = new SFDCMyJLCustomerTypes.PartyRoleInteractionType();
    	system.assertNotEquals(null, ctObj);
    }
    static testMethod void Party() {
   		SFDCMyJLCustomerTypes.Party ctObj = new SFDCMyJLCustomerTypes.Party();
    	system.assertNotEquals(null, ctObj);
    }
    static testMethod void Location() {
   		SFDCMyJLCustomerTypes.Location ctObj = new SFDCMyJLCustomerTypes.Location();
    	system.assertNotEquals(null, ctObj);
    }
    static testMethod void PartyRoleInteraction() {
   		SFDCMyJLCustomerTypes.CustomerIDType ctObj = new SFDCMyJLCustomerTypes.CustomerIDType();
    	system.assertNotEquals(null, ctObj);
    }
    static testMethod void PartyRole() {
   		SFDCMyJLCustomerTypes.PartyRole ctObj = new SFDCMyJLCustomerTypes.PartyRole();
    	system.assertNotEquals(null, ctObj);
    }
    static testMethod void PartyRoleInteractionConfigItem() {
   		SFDCMyJLCustomerTypes.PartyRoleInteractionConfigItem ctObj = new SFDCMyJLCustomerTypes.PartyRoleInteractionConfigItem();
    	system.assertNotEquals(null, ctObj);
    }
    static testMethod void PartyRoleType() {
   		SFDCMyJLCustomerTypes.PartyRoleType ctObj = new SFDCMyJLCustomerTypes.PartyRoleType();
    	system.assertNotEquals(null, ctObj);
    }
    static testMethod void CustomerGroupType() {
   		SFDCMyJLCustomerTypes.CustomerGroupType ctObj = new SFDCMyJLCustomerTypes.CustomerGroupType();
    	system.assertNotEquals(null, ctObj);
    }
    static testMethod void PartyRoleInteractionThreadType() {
   		SFDCMyJLCustomerTypes.PartyRoleInteractionThreadType ctObj = new SFDCMyJLCustomerTypes.PartyRoleInteractionThreadType();
    	system.assertNotEquals(null, ctObj);
    }
    static testMethod void GetCustomerRequestType() {
   		SFDCMyJLCustomerTypes.GetCustomerRequestType ctObj = new SFDCMyJLCustomerTypes.GetCustomerRequestType();
    	system.assertNotEquals(null, ctObj);
    }
    static testMethod void MoneyShortCost() {
   		SFDCMyJLCustomerTypes.MoneyShortCost ctObj = new SFDCMyJLCustomerTypes.MoneyShortCost();
    	system.assertNotEquals(null, ctObj);
    }
    static testMethod void ContactMethodType() {
   		SFDCMyJLCustomerTypes.ContactMethodType ctObj = new SFDCMyJLCustomerTypes.ContactMethodType();
    	system.assertNotEquals(null, ctObj);
    }         
}