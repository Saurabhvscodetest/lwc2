@isTest private class CommonStaticUtilsTests {


	static testmethod void testGetValueWithConstatValue(){
  
  		try{
            Test.loadData(Constants__c.sObjectType, 'Constants');
        }
        catch(Exception e){
        	System.debug('Exception' + e.getMessage() );
        }       

    	string CASE_ORIGIN_PICKLIST_DEFAULT_VALUE = CommonStaticUtils.getValue('CASE_ORIGIN_PICKLIST_DEFAULT_VALUE', 'Not the correct value');
    	string expectedValue = 'Phone';

    	System.assertEquals(expectedValue, CASE_ORIGIN_PICKLIST_DEFAULT_VALUE);
	}

	static testmethod void testGetValueWithoutConstatValue(){
    
    	string CASE_ORIGIN_PICKLIST_DEFAULT_VALUE = CommonStaticUtils.getValue('CASE_ORIGIN_PICKLIST_DEFAULT_VALUE', 'Correct Value');
    	string expectedValue = 'Correct Value';

    	System.assertEquals(expectedValue, CASE_ORIGIN_PICKLIST_DEFAULT_VALUE);
	}

    @isTest static void TestUserHasCapitaProfile(){

        User capitaUser = UnitTestDataFactory.getFrontOfficeUser('CapitaUser');
            
        System.runAs(capitaUser) {

            String capitaUserProfile = 'Capita';

            Test.startTest();
            CommonStaticUtils.UserHasProfileNamed(capitaUserProfile);
            Test.stopTest();

            System.assertEquals(true, CommonStaticUtils.UserHasProfileNamed(capitaUserProfile));

        }

    }

    @isTest static void TestUserHasNonCapitaProfile(){

        User nonCapitaUser = UnitTestDataFactory.getSystemAdministrator('NonCapitaUser');
            
        System.runAs(nonCapitaUser) {

            String capitaUserProfile = 'Capita';

            Test.startTest();
            CommonStaticUtils.UserHasProfileNamed(capitaUserProfile);
            Test.stopTest();

            System.assertEquals(false, CommonStaticUtils.UserHasProfileNamed(capitaUserProfile));

        }

    }
    
    @isTest static void getRecordTypeTest() {
		Id RecordTypeIdContactPrim = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Ordering Customer').getRecordTypeId();
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
            insert CustomSettingTestDataFactory.createConfigSettings();
        }        
        Contact primCon = new Contact();
        primCon.FirstName = 'fName';
        primCon.LastName = 'lName';
        primCon.Email = 'test@test.com';
        primCon.RecordTypeId = RecordTypeIdContactPrim;
        insert primCon;        
        Case cas = CaseTestDataFactory.createCase(primCon.Id);
        cas.Bypass_Standard_Assignment_Rules__c = true;
        insert cas;
        
        Test.startTest();
        	CommonStaticUtils.getRecordType(cas.RecordType.Name);
        Test.stopTest();
    }	
    
    @isTest static void getRecordTypeNameTest() {
		Id RecordTypeIdContactPrim = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Ordering Customer').getRecordTypeId();
        Id recordTypeId = CommonStaticUtils.getRecordTypeId('Case', 'New Case');
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
            insert CustomSettingTestDataFactory.createConfigSettings();
        }        
        Contact primCon = new Contact();
        primCon.FirstName = 'fName';
        primCon.LastName = 'lName';
        primCon.Email = 'test@test.com';
        primCon.RecordTypeId = RecordTypeIdContactPrim;
        insert primCon;        
        Case cas = CaseTestDataFactory.createCase(primCon.Id);
        cas.Bypass_Standard_Assignment_Rules__c = true;
        cas.RecordTypeId = recordTypeId;
        insert cas;
        
        Test.startTest();
        	CommonStaticUtils.getRecordTypeName('Case',cas.RecordTypeId);
        Test.stopTest();
    }
    
    @isTest static void getContactRecordTypeTest() {
		Id RecordTypeIdContactPrim = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Ordering Customer').getRecordTypeId();
        Id recordTypeId = CommonStaticUtils.getRecordTypeId('Case', 'New Case');
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
            insert CustomSettingTestDataFactory.createConfigSettings();
        }        
        Contact primCon = new Contact();
        primCon.FirstName = 'fName';
        primCon.LastName = 'lName';
        primCon.Email = 'test@test.com';
        primCon.RecordTypeId = RecordTypeIdContactPrim;
        insert primCon;        
        Case cas = CaseTestDataFactory.createCase(primCon.Id);
        cas.Bypass_Standard_Assignment_Rules__c = true;
        cas.RecordTypeId = recordTypeId;
        insert cas;
        
        Test.startTest();
        	CommonStaticUtils.getContactRecordType(cas.RecordType.Name);
        Test.stopTest();
    }
    
    @isTest static void getCurrentUserTest() {
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        
        Test.startTest();
        	CommonStaticUtils.getCurrentUser();
        Test.stopTest();
    }
    
    @isTest static void isCaseIdTest() {
		Id RecordTypeIdContactPrim = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Ordering Customer').getRecordTypeId();
        Id recordTypeId = CommonStaticUtils.getRecordTypeId('Case', 'New Case');
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
            insert CustomSettingTestDataFactory.createConfigSettings();
        }        
        Contact primCon = new Contact();
        primCon.FirstName = 'fName';
        primCon.LastName = 'lName';
        primCon.Email = 'test@test.com';
        primCon.RecordTypeId = RecordTypeIdContactPrim;
        insert primCon;        
        Case cas = CaseTestDataFactory.createCase(primCon.Id);
        cas.Bypass_Standard_Assignment_Rules__c = true;
        cas.RecordTypeId = recordTypeId;
        insert cas;
        
        Test.startTest();
        	CommonStaticUtils.isCaseId(cas.Id);
        Test.stopTest();
    }
    
    @isTest static void getBusinessHoursTest() {
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        
        List<BusinessHours> bhs=[select id, name from BusinessHours where IsDefault=true];
        System.assert(bhs.size()==1);
        
        Test.startTest();
        	CommonStaticUtils.getBusinessHours(bhs[0].Name);
        Test.stopTest();
    }
    
    @isTest static void allowEditOfOrderingCustomerForVocUpdateTest() {
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        
        Test.startTest();
        	CommonStaticUtils.allowEditOfOrderingCustomerForVocUpdate();
        Test.stopTest();
    }
    
    @isTest static void removeContactPrefixTest() {
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        
        Test.startTest();
        	CommonStaticUtils.removeContactPrefix('09999999999');
        Test.stopTest();
    }
}