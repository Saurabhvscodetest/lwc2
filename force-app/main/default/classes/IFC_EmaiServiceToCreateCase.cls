global class IFC_EmaiServiceToCreateCase implements Messaging.InboundEmailHandler {
    public Blob csvFileBody;
    public string csvFileName;
    public boolean NOCSVAttachment = true;    
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        Messaging.InboundEmail.BinaryAttachment[] binaryAttachments = email.BinaryAttachments;
        
        if(binaryAttachments != null)
        {
            for(integer k=0; k < binaryAttachments.size(); k++){
                Messaging.InboundEmail.binaryAttachment binaryattachment = binaryAttachments[k];
                If(binaryattachment.Filename.endsWith('.csv'))
                {
                    NOCSVAttachment = false;
                    //Reading CSV File
                    try{
                        csvFileName = binaryattachment.Filename;
                        csvFileBody = binaryattachment.body;
                        String csvAsString = csvFileBody.toString();
                        String [] csvFileLines = csvAsString.split('\n'); 
                        String[] csvRecordColumn = csvFileLines[0].split(',');
                        System.debug('***Total Column CSV*** '+csvRecordColumn.size());
                        System.debug('***CSV Column Name*** '+csvRecordColumn);
                        
                        Map<String, IFC_CSVFieldMapping__c> mapOfCustomSetting = IFC_CSVFieldMapping__c.getAll(); 
                        System.debug('***MAP Custom Setting*** '+mapOfCustomSetting.keySet());
                        
                        if(!csvRecordColumn.isEmpty()){
                            List<String> lstOfFieldName = new List<String>();
                            Set<Integer> setOfFieldIndexNA = new Set<Integer>();
                            
                            for(Integer i=0; i < csvRecordColumn.size(); i++){
                                if(mapOfCustomSetting.containsKey(csvRecordColumn[i].trim()) && mapOfCustomSetting.get(csvRecordColumn[i].trim()).Feild_Name__c != null)
                                    lstOfFieldName.add(mapOfCustomSetting.get(csvRecordColumn[i].trim()).Feild_Name__c);
                                else{
                                    lstOfFieldName.add(null);
                                    setOfFieldIndexNA.add(i);
                                }        
                            }                    
                            System.debug('***Column Mapped with Custom Setting Fields*** '+lstOfFieldName);
                            System.debug('***CSV Column Index Not Mapped*** '+setOfFieldIndexNA);
                            
                            //Iterate CSV file lines
                            List<SObject> sObjectList = new List<SObject>();
                            Id QueryRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Query').getRecordTypeId();
                            if(csvRecordColumn.size() != setOfFieldIndexNA.size()){
                                for(Integer i=1; i < csvFileLines.size(); i++){
                                    SObject sObj = new Case() ;
                                    String[] csvRecordData = csvFileLines[i].split(',');
                                    sObj.put('RecordTypeId',QueryRecordTypeId) ;
                                    sObj.put('Origin','IFC Queries Sheet') ;
                                    sObj.put('Subject', email.subject);
                                    
                                    for(Integer j=0; j<csvRecordData.size(); j++){
                                        if(!setOfFieldIndexNA.contains(j)){
                                            if(lstOfFieldName[j] == 'Date_Processed__c' && csvRecordData[j].trim() != null){
                                                sObj.put(lstOfFieldName[j], date.parse(csvRecordData[j].trim()));
                                            }
                                            else if(lstOfFieldName[j] == 'Amount__c' && csvRecordData[j].trim() != null){
                                                sObj.put(lstOfFieldName[j], Decimal.valueOf(csvRecordData[j].trim()));
                                            }
                                            else{
                                                sObj.put(lstOfFieldName[j],csvRecordData[j].trim()) ;
                                            }
                                        }
                                    }                                                                          
                                    sObjectList.add(sObj);   
                                }
                                
                                //Inserting Records in Ojects
                                if(sObjectList.size()>0){
                                    Database.SaveResult[] saveResultList = Database.insert(sObjectList, false);
                                    String messageBody = '';
                                    Integer successCount = 0;
                                    Integer failCount = 0;
                                    for (Database.SaveResult sr : saveResultList) {
                                        if (sr.isSuccess()) {                              
                                            System.debug('Successfully inserted Record. Record ID: ' + sr.getId()); 
                                            successCount++;
                                        } else {
                                            for(Database.Error objErr : sr.getErrors()) {
                                                messageBody += 'Status Code : '+ objErr.getStatusCode();
                                                messageBody += '<br/>Message : ' + objErr.getMessage();
                                                messageBody += '<br/>Object field which are affected by the error: ' + objErr.getFields()+'<br/><br/>';
                                                failCount++;
                                            }
                                        }
                                    }
                                    system.debug('***Error Message*** '+messageBody);
                                    system.debug('***SuccessCount: '+successCount+ '***FailCount: '+failCount);
                                    if(failCount>0)
                                        sendEmail(messageBody,successCount,failCount);
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        sendEmail('An error has occured while importing data. Please refer the error message. <br/> Error Message: '+e.getMessage()+'<br/><br/>', 0, 0);
                        system.debug('Exception***** '+e);
                    }  
                }
            }
            if(NOCSVAttachment == true){
                sendEmail('An error has occured while importing data. No CSV file as an attachment found.<br/>', 0, 0);
            }
        }
        else{
            sendEmail('An error has occured while importing data. Please make sure to attach CSV file only.<br/>', 0, 0);
        }
        result.success = true;
        return result;
    }
    
    //Method to send an email on failure/Exceptions
    public void sendEmail(string body, Integer successCount, Integer failCount){
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        IFC_ErrorEmail__c obj = IFC_ErrorEmail__c.getvalues('Error Email Id');
        if(obj.value__c != null){
            mail.setToAddresses(new List<string>{obj.value__c});
            mail.setReplyTo('noreplay@Connex.com');
            mail.setSenderDisplayName('Connex Application');  
            mail.setSubject('IFC Queries Sheet - Exceptions');
            String emailbody = 'Dear Team,<br/><br/>';
            emailbody += 'Following error has been occured while processing the attached CSV File.<br/><br/>';
            emailbody += body;
            emailbody += 'Number of record successfully inserted :'+successCount+'<br/>';
            emailbody += 'Number of record failed due to error :'+failCount+'<br/>';
            emailbody += '<br/><br/> Regards, <br/> Connex Team';
            mail.setHtmlBody(emailbody);
            //Attaching CSV File
            if(csvFileBody !=null){
                Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
                attach.Body = csvFileBody;
                attach.setFileName(csvFileName);
                attach.setContentType('text/csv');
                mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach });
            }
            //Send email
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
}