/**
 *  @author  : Yousuf Mohammad
 *  @Date    : 10-06-2016
 *  @Purpose : This class will be used to return as an Success service object where CaseId and 
 *             Case Number are wrapped together.
 */
global with sharing class ExceptionServiceSuccessResponse extends ServiceResponse {
    //case Id
    global String id									{get;set;}
    //Case Number
    global String caseNumber							{get;set;}
    
    /*
     * Constructor
     *
     * @param caseId : case Id as input parameter.
     */
    global ExceptionServiceSuccessResponse(String caseId, String caseNumber)	{
        this.id = caseId;
        this.caseNumber = caseNumber;
    }
    
}