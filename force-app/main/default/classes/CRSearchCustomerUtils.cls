/*
* @File Name   : CRSearchCustomerUtils 
* @Description : Utility class to related to MyJLChatBot Delivery Tracking functionalities  
* @Copyright   : Zensar
* @Jira #      : #5914
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0       04-DEC-20          RAGESH G                      Created
*/


public class CRSearchCustomerUtils {
	
	
	public static final Id primaryRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Ordering Customer').getRecordTypeId();
	public static final Id secondaryRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Normal').getRecordTypeId();
	
    public static Map < String, Contact > attributeToContactMap = new Map < String, Contact > (); 
    
    
    public static void searchCustomerRecord ( Map < Id, LiveChatTranscript > chatTranscriptMap  ) {
		
		List < String > lastNameList = new List < String > ();
        List < String > firstNameList = new List < String > ();
        List < String > emailList = new List < String > ();
        
        for ( LiveChatTranscript liveChatRecord : [ SELECT Id, FirstName__c, LastName__c, Email__c FROM LiveChatTranscript 
                                                    WHERE Id IN :chatTranscriptMap.values () ] ) {
			if ( liveChatRecord.LastName__c != null ) {
				lastNameList.add ( liveChatRecord.LastName__c );
			}
			if ( liveChatRecord.FirstName__c != null) {
				firstNameList.add ( liveChatRecord.FirstName__c );
			}
			if ( liveChatRecord.Email__c != null) {
				emailList.add ( liveChatRecord.Email__c);
			}            
                        
        }
        List < Contact > contactUpdateList ;
        if ( emailList != null && firstNameList != null && lastNameList != null ) {
            
            System.debug( 'Check Inside the null');
            contactUpdateList = fetchCustomerList ( firstNameList, lastNameList, emailList );
        }
        for ( Contact contactRecord : contactUpdateList ) {

            String attributeKey = contactRecord.Email;
            String attributeKey2 = contactRecord.Email +';'+ contactRecord.LastName;
            String attributeKey3;
            if ( contactRecord.FirstName != null ) {
                attributeKey3 = contactRecord.Email +';'+ contactRecord.LastName +';'+ contactRecord.FirstName ;
                attributeToContactMap.put ( attributeKey3, contactRecord );
            }
            
            attributeToContactMap.put ( attributeKey, contactRecord );
            attributeToContactMap.put ( attributeKey2, contactRecord );            
        }
        
        System.debug(' attributeToContactMap ' + attributeToContactMap);
        List < LiveChatTranscript > chatTranscriptList  = new List < LiveChatTranscript > ();
        Contact contactRecord = new Contact ();
        
        if ( contactUpdateList.size () == 0 || contactUpdateList.size () > 1) {
            
            if(lastNameList.size() > 0 && firstNameList.size() > 0 && emailList.size() > 0){
                
            contactRecord.LastName = lastNameList[0];
            contactRecord.FirstName = firstNameList [0];
            contactRecord.Email = emailList[0];
            contactRecord.recordTypeId = secondaryRecordTypeId;
            
            try {
                Insert contactRecord;
            } 
            catch ( Exception Ex ) 
                {
                System.debug(' Exception Occured ' + Ex.getMessage() ) ;
            }
                
                }
            
            if ( contactRecord != null ) {
                for ( LiveChatTranscript liveChatRecord :  [ SELECT Id, FirstName__c, LastName__c, Email__c, ContactId FROM LiveChatTranscript 
                                                    	 	 WHERE Id IN :chatTranscriptMap.values () ] ) {
                    liveChatRecord.ContactId = contactRecord.Id;
                    chatTranscriptList.add ( liveChatRecord );
                }    
            }
            
        } else {
            
             for ( LiveChatTranscript liveChatRecord :  [ SELECT Id, FirstName__c, LastName__c, Email__c, ContactId FROM LiveChatTranscript 
                                                    WHERE Id IN :chatTranscriptMap.values () ] ) {
                Contact contactRecordNew = fetchMatchingCustomerRecord ( liveChatRecord );
                if ( contactRecordNew != null ) {
                    liveChatRecord.ContactId = contactRecordNew.Id;
                    chatTranscriptList.add ( liveChatRecord );
                } else {
                    liveChatRecord.ContactId = contactRecord.Id;
                    chatTranscriptList.add ( liveChatRecord );
                }
                
            }
            
        }       
        
        try {
            Database.update ( chatTranscriptList ); 
        } catch ( Exception Ex ) {
            System.debug(' Exception Occured while updating record'+ Ex.getMessage() );
        }
		
	}
    
    public static List < Contact > fetchCustomerList ( List < String > firstNameList, List < String > lastNameList, List < String > emailList ) {
        
        String firstLevelQuery  = 'SELECT Id, FirstName, LastName, Email FROM Contact WHERE FirstName IN :firstNameList AND LastName IN :lastNameList AND Email IN :emailList AND RecordTypeId =: primaryRecordTypeId';
        String secondLevelQuery = 'SELECT Id, FirstName, LastName, Email FROM Contact WHERE LastName IN :lastNameList AND Email IN :emailList AND RecordTypeId =: primaryRecordTypeId';
        String thirdLevelQuery  = 'SELECT Id, FirstName, LastName, Email FROM Contact WHERE Email IN :emailList AND RecordTypeId =: primaryRecordTypeId';
		
        String secondaryFirstLevelQuery = 'SELECT Id, FirstName, LastName, Email FROM Contact WHERE FirstName IN :firstNameList AND LastName IN :lastNameList AND Email IN :emailList AND RecordTypeId =: secondaryRecordTypeId';
        String secondarySecondLevelQuery = 'SELECT Id, FirstName, LastName, Email FROM Contact WHERE LastName IN :lastNameList AND Email IN :emailList AND RecordTypeId =: secondaryRecordTypeId';
		String secondaryThirdLevelQuery = 'SELECT Id, FirstName, LastName, Email FROM Contact WHERE Email IN :emailList AND RecordTypeId =: secondaryRecordTypeId';

        List < Contact > contactList =  Database.query( firstLevelQuery );
        
        if ( contactList.size () == 0  ) {
			contactList = Database.query( secondLevelQuery );
            if ( contactList.size() == 0 ) {
                contactList = Database.query( thirdLevelQuery );
                if ( contactList.size () == 0 ) {
                    contactList = Database.query( secondaryFirstLevelQuery );
                    if ( contactList.size () == 0 ) {
                        contactList = Database.query( secondarySecondLevelQuery );
                        if ( contactList.size () == 0 ) {
                            contactList = Database.query( secondaryThirdLevelQuery );
                        }
                    } else if ( contactList.size () == 1  ) {
                        return contactList;
                    }
                } else if ( contactList.size () == 1  ) {
                    return contactList;
                }
            }	else if ( contactList.size () == 1  ) {
                return contactList;
            }
        } else if ( contactList.size () == 1  ) {
            return contactList;
        }
        return contactList;
    }
    
    public static Contact fetchMatchingCustomerRecord ( LiveChatTranscript liveChatRecord ) {
	
        Contact contactRecordVal = null ;
        String attributeKeyVal1 = liveChatRecord.Email__c;
        String attributeKeyVal2 = liveChatRecord.Email__c +';'+ liveChatRecord.LastName__c;
        String attributeKeyVal3 = liveChatRecord.Email__c +';'+ liveChatRecord.LastName__c +';'+ liveChatRecord.FirstName__c ;
        
        if ( ! attributeToContactMap.isEmpty() ) {
            if ( attributeToContactMap.containsKey ( attributeKeyVal3 ) ) {
                contactRecordVal = attributeToContactMap.get ( attributeKeyVal3 );
                System.debug(' Key Matching ' + attributeKeyVal3 ) ;
                return contactRecordVal;
            } else if ( attributeToContactMap.containsKey ( attributeKeyVal2 )  ) {
                contactRecordVal = attributeToContactMap.get ( attributeKeyVal2 );
                System.debug(' Key Matching ' + attributeKeyVal2 ) ;
                return contactRecordVal;
            } else if ( attributeToContactMap.containsKey ( attributeKeyVal1 ) ) {
                contactRecordVal = attributeToContactMap.get ( attributeKeyVal1 );
                System.debug(' Key Matching ' + attributeKeyVal1 ) ;
                return contactRecordVal;
            }
        }
	
	return contactRecordVal;
}

}