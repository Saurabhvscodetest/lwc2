public class PromiseUtil
{
    public static Case_Promise_Configuration__c getCasePromiseConfiguration(Case caseObj , String promiseName){
        List<Case_Promise_Configuration__c> casePromiseALLList  = Case_Promise_Configuration__c.getall().values();
        Case_Promise_Configuration__c retVal = null;
        List<Case_Promise_Configuration__c> AllFourValList = new List<Case_Promise_Configuration__c>();
        List<Case_Promise_Configuration__c> AllThreeValList = new List<Case_Promise_Configuration__c>();
        List<Case_Promise_Configuration__c> AllTwoValList = new List<Case_Promise_Configuration__c>();
        List<Case_Promise_Configuration__c> AllOneValList = new List<Case_Promise_Configuration__c>();
        List<Case_Promise_Configuration__c> DefaulValtList = new List<Case_Promise_Configuration__c>();
        
        for(Case_Promise_Configuration__c objCPC : casePromiseALLList )
        {
            string repoenPromiseSuffix  = objCPC.ReopenBy__c!=null ? ' by '+objCPC.ReopenBy__c : '';
            repoenPromiseSuffix = objCPC.Promise_Type__c + repoenPromiseSuffix;
            repoenPromiseSuffix.trim();
            if((promiseName == objCPC.Promise_Type__c || promiseName == repoenPromiseSuffix || promiseName == objCPC.name) && 
            (objCPC.Case_Type__c==null || objCPC.Case_Type__c=='Any' || objCPC.Case_Type__c==caseObj.jl_Case_Type__c)
            )
            {
                integer fieldCount = 0;
                if(objCPC.Case_Source__c!=null && objCPC.Case_Source__c!='Any')
                {
                    fieldCount++;
                }
                if(objCPC.Initiating_Source__c!=null && objCPC.Initiating_Source__c!='Any')
                {
                    fieldCount++;
                }
                if(objCPC.Reason_Detail__c!=null && objCPC.Reason_Detail__c!='Any')
                {
                    fieldCount++;
                }
                if(objCPC.Reference_Type__c!=null && objCPC.Reference_Type__c!='Any')
                {
                    fieldCount++;
                }
                if(fieldCount==4)
                {
                    AllFourValList.add(objCPC);
                }
                else if(fieldCount==3)
                {
                    AllThreeValList.add(objCPC);
                }
                else if(fieldCount==2)
                {
                    AllTwoValList.add(objCPC);
                }
                else if(fieldCount==1)
                {
                    AllOneValList.add(objCPC);
                }else 
                {
                    DefaulValtList.add(objCPC);
                }
            }
        } // Case_Source__c , Reason_Detail__c , Initiating_Source__c , Reference_Type__c
        system.debug('****AllFourValList **> '+AllFourValList);
        system.debug('****AllThreeValList **> '+AllThreeValList);
        system.debug('****AllTwoValList **> '+AllTwoValList);
        system.debug('****AllOneValList **> '+AllOneValList);
        system.debug('****DefaulValtList **> '+DefaulValtList);
                        
        if(AllFourValList.size()>0)
        {
            retVal = searchInCPList(caseObj , AllFourValList);
        }
        if(retVal==null && AllThreeValList.size()>0)
        {
            retVal = searchInCPList(caseObj , AllThreeValList);
        }
        if(retVal==null && AllTwoValList.size()>0)
        {
            retVal = searchInCPList(caseObj , AllTwoValList);
        }
        if(retVal==null && AllOneValList.size()>0)
        {
            retVal = searchInCPList(caseObj , AllOneValList);
        }
        if(DefaulValtList.size()>0 && retVal==null)
        {
            if(DefaulValtList.size()>1)
            {
                for(Case_Promise_Configuration__c obj : DefaulValtList)
                {
                    if(obj.Case_Type__c!=null && (obj.Case_Type__c==caseObj.jl_Case_Type__c))
                    {
                        retVal = obj;
                        break;
                    }   
                }   
            }
            if(retVal==null)
            {
                retVal = DefaulValtList[0];
            }
            
            system.debug('Final Retval **>'+retVal);
            return retVal;
        }
        system.debug('Final Retval **>'+retVal);
        return retVal;
    }
    
    public static Case_Promise_Configuration__c searchInCPList(Case caseObj , List<Case_Promise_Configuration__c> cpList)
    {
        Case_Promise_Configuration__c retVal = null;
        for(Case_Promise_Configuration__c objCPC : cpList)
            {
                if((objCPC.Case_Source__c==null || objCPC.Case_Source__c=='Any' || (objCPC.Case_Source__c==caseObj.Origin)) && (objCPC.Reference_Type__c==null || objCPC.Reference_Type__c=='Any' || (caseObj.Reference_Type__c==objCPC.Reference_Type__c)) && (objCPC.Reason_Detail__c==null || objCPC.Reason_Detail__c=='Any' || (caseObj.Reason_Detail__c==objCPC.Reason_Detail__c)) && (objCPC.Initiating_Source__c==null || objCPC.Initiating_Source__c=='Any' || (caseObj.jl_InitiatingSource__c == objCPC.Initiating_Source__c)))
                {
                    retVal = objCPC;
                    break;
                }
            }      
        return retVal;
   }
    
}