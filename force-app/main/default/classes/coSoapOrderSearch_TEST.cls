/**
 * Unit tests for coSoapOrderSearch class.
 */
@isTest
private class coSoapOrderSearch_TEST {

    /**
     * Code coverage method for coSoapOrderSearch.getSoapRequest()
     */
	static testMethod void testGetSoapRequest() {
    	system.debug('TEST START: testGetSoapRequest');
    	coSoapOrderSearch orderSearch = new coSoapOrderSearch();
    	orderSearch.CustomerId = 'TEST12345';
		Test.startTest();
		String xmlResponse = orderSearch.getSoapRequest();
		Test.stopTest();
		system.assertNotEquals(null, xmlResponse);
    	system.debug('TEST END: testGetSoapRequest');
	}

    /**
     * Code coverage method for coSoapOrderSearch.setSoapResponse()
     */
	static testMethod void testSetSoapResponse() {
    	system.debug('TEST START: testSetSoapResponse');
    	String serverEndPoint = 'http://soa.johnlewispartnership.com/';
		String viewcordNS = serverEndPoint + 'service/es/Transaction/CustomerOrder/CustomerOrder/ViewCustomerOrder/v2';
		String txnNS = serverEndPoint + 'schema/ebo/Transaction/v1';
		String cmnNS = serverEndPoint + 'schema/ebo/Common/v1';
		String cmnEBMNS = serverEndPoint + 'schema/ebm/CommonEBM/v1';
		String viewcordEBMNS = serverEndPoint + 'schema/ebm/Transaction/CustomerOrder/CustomerOrder/v1';
    	String soapenvNS = 'http://schemas.xmlsoap.org/soap/envelope/';
		String xsiNS = 'http://www.w3.org/2001/XMLSchema-instance';
		String wsaNS = 'http://www.w3.org/2005/08/addressing';

		String xmlResp = '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body>';
		xmlResp += '<viewcord:ShowCustomerOrder xmlns:viewcord="' + viewcordNS + '"><viewcord:ApplicationArea><MessageID>b8cbf9d7-73f6-4c00-9a83-342f592e60a2</MessageID><TrackingID>9f6cad6b-88bc-19b9-a198-e08b813a3df</TrackingID></viewcord:ApplicationArea>';
		xmlResp += '<viewcord:DataArea xmlns:viewcord="' + viewcordEBMNS + '"><viewcord:Show responseCode="Success" xmlns:viewcord="' + viewcordEBMNS + '"/>';
		
		xmlResp += '<CustomerOrder>';
		xmlResp += '<viewcord:CustomerOrderID xmlns:viewcord="' + txnNS + '"><viewcord:xrefIdentity ID="12345678"xmlns:viewcord="' + cmnNS + '"/></viewcord:CustomerOrderID>';
		xmlResp += '<viewcord:CustomerOrderLineItem xmlns:viewcord="' + txnNS + '"><viewcord:CustomerOrderChargePriceModifier xmlns:viewcord="' + txnNS + '"><viewcord:Amount amount="100"xmlns:viewcord="' + txnNS + '"/></viewcord:CustomerOrderChargePriceModifier></viewcord:CustomerOrderLineItem>';
		xmlResp += '<CustomerOrderStateAssignment>';
		xmlResp += '<viewcord:CustomerOrderStateTypeCode xmlns:viewcord="' + txnNS + '"><viewcord:xrefCode code="code1"xmlns:viewcord="' + cmnNS + '"/></viewcord:CustomerOrderStateTypeCode>';
		xmlResp += '<viewcord:ValidFrom xmlns:viewcord="' + txnNS + '">27/01/2015</viewcord:ValidFrom>';
		xmlResp += '</CustomerOrderStateAssignment>';
		xmlResp += '</CustomerOrder>';
		
		xmlResp += '</viewcord:DataArea></viewcord:ShowCustomerOrder></soapenv:Body></soapenv:Envelope>';
    	coSoapOrderSearch orderSearch = new coSoapOrderSearch();
		Test.startTest();
		orderSearch.setSoapResponse(xmlResp);
		Test.stopTest();
		system.assertEquals('Success', orderSearch.Status);
		system.assertNotEquals(null, orderSearch.Orders);
		coSoapOrderSearch.OrderSearchResult osr = orderSearch.Orders.get(0);
		system.assertEquals('12345678', osr.Id);
		system.assertEquals(100, osr.TotalValue);
		system.assertEquals('code1', osr.Status);
    	system.debug('TEST END: testSetSoapResponse');
	}

    /**
     * Code coverage method for coSoapOrderSearch.setSoapResponse() with a fault
     */
	static testMethod void testSetSoapResponseFault() {
    	system.debug('TEST START: testSetSoapResponseFault');
    	String serverEndPoint = 'http://soa.johnlewispartnership.com/';
		String viewcordNS = serverEndPoint + 'service/es/Transaction/CustomerOrder/CustomerOrder/ViewCustomerOrder/v2';
		String txnNS = serverEndPoint + 'schema/ebo/Transaction/v1';
		String cmnNS = serverEndPoint + 'schema/ebo/Common/v1';
		String cmnEBMNS = serverEndPoint + 'schema/ebm/CommonEBM/v1';
		String viewcordEBMNS = serverEndPoint + 'schema/ebm/Transaction/CustomerOrder/CustomerOrder/v1';
    	String soapenvNS = 'http://schemas.xmlsoap.org/soap/envelope/';
		String xsiNS = 'http://www.w3.org/2001/XMLSchema-instance';
		String wsaNS = 'http://www.w3.org/2005/08/addressing';

		String xmlResp = '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body>';
		xmlResp += '<viewcord:Fault xmlns:viewcord="' + soapenvNS + '">';
		xmlResp += '<faultcode>fault123</faultcode>';
		xmlResp += '<faultstring>faultstring</faultstring>';
		xmlResp += '</viewcord:Fault></soapenv:Body></soapenv:Envelope>';
    	coSoapOrderSearch orderSearch = new coSoapOrderSearch();
		Test.startTest();
		orderSearch.setSoapResponse(xmlResp);
		Test.stopTest();
		system.assertEquals('Error', orderSearch.Status);
		system.assertEquals('fault123', orderSearch.Error.ErrorCode);
		system.assertEquals(null, orderSearch.Error.ErrorType);
		system.assertEquals('faultstring', orderSearch.Error.ErrorDescription);
    	system.debug('TEST END: testSetSoapResponseFault');
	}

    /**
     * Code coverage method for coSoapOrderSearch.setSoapResponse() with an error and ActionStatusRecord
     */
	static testMethod void testSetSoapResponseErrorActionStatus() {
    	system.debug('TEST START: testSetSoapResponseErrorActionStatus');
    	String serverEndPoint = 'http://soa.johnlewispartnership.com/';
		String viewcordNS = serverEndPoint + 'service/es/Transaction/CustomerOrder/CustomerOrder/ViewCustomerOrder/v2';
		String txnNS = serverEndPoint + 'schema/ebo/Transaction/v1';
		String cmnNS = serverEndPoint + 'schema/ebo/Common/v1';
		String cmnEBMNS = serverEndPoint + 'schema/ebm/CommonEBM/v1';
		String viewcordEBMNS = serverEndPoint + 'schema/ebm/Transaction/CustomerOrder/CustomerOrder/v1';
    	String soapenvNS = 'http://schemas.xmlsoap.org/soap/envelope/';
		String xsiNS = 'http://www.w3.org/2001/XMLSchema-instance';
		String wsaNS = 'http://www.w3.org/2005/08/addressing';

		String xmlResp = '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body>';
		xmlResp += '<viewcord:ShowCustomerOrder xmlns:viewcord="' + viewcordNS + '"><viewcord:ApplicationArea><MessageID>b8cbf9d7-73f6-4c00-9a83-342f592e60a2</MessageID><TrackingID>9f6cad6b-88bc-19b9-a198-e08b813a3df</TrackingID></viewcord:ApplicationArea>';
		xmlResp += '<viewcord:DataArea xmlns:viewcord="' + viewcordEBMNS + '"><viewcord:Show responseCode="Error" hasActionStatusRecords="true" xmlns:viewcord="' + viewcordEBMNS + '">';
		xmlResp += '<viewcord:ActionStatusRecord xmlns:viewcord="' + cmnEBMNS + '">';
		xmlResp += '<viewcord:ServiceError errorCode="404" errorType="Network" errorDescription="Broken" xmlns:viewcord="' + cmnEBMNS + '"/>';
		xmlResp += '</viewcord:ActionStatusRecord>';
		xmlResp += '</viewcord:Show></viewcord:DataArea></viewcord:ShowCustomerOrder></soapenv:Body></soapenv:Envelope>';
    	coSoapOrderSearch orderSearch = new coSoapOrderSearch();
		Test.startTest();
		orderSearch.setSoapResponse(xmlResp);
		Test.stopTest();
		system.assertEquals('Error', orderSearch.Status);
		system.assertEquals('404', orderSearch.Error.ErrorCode);
		system.assertEquals('Network', orderSearch.Error.ErrorType);
		system.assertEquals('Broken', orderSearch.Error.ErrorDescription);
    	system.debug('TEST END: testSetSoapResponseErrorActionStatus');
	}

    /**
     * Code coverage method for coSoapOrderSearch.setSoapResponse() with an error and no ActionStatusRecord
     */
	static testMethod void testSetSoapResponseErrorNoActionStatus() {
    	system.debug('TEST START: testSetSoapResponseErrorNoActionStatus');
    	String serverEndPoint = 'http://soa.johnlewispartnership.com/';
		String viewcordNS = serverEndPoint + 'service/es/Transaction/CustomerOrder/CustomerOrder/ViewCustomerOrder/v2';
		String txnNS = serverEndPoint + 'schema/ebo/Transaction/v1';
		String cmnNS = serverEndPoint + 'schema/ebo/Common/v1';
		String cmnEBMNS = serverEndPoint + 'schema/ebm/CommonEBM/v1';
		String viewcordEBMNS = serverEndPoint + 'schema/ebm/Transaction/CustomerOrder/CustomerOrder/v1';
    	String soapenvNS = 'http://schemas.xmlsoap.org/soap/envelope/';
		String xsiNS = 'http://www.w3.org/2001/XMLSchema-instance';
		String wsaNS = 'http://www.w3.org/2005/08/addressing';

		String xmlResp = '<?xml version="1.0" encoding="UTF-8"?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Body>';
		xmlResp += '<viewcord:ShowCustomerOrder xmlns:viewcord="' + viewcordNS + '"><viewcord:ApplicationArea><MessageID>b8cbf9d7-73f6-4c00-9a83-342f592e60a2</MessageID><TrackingID>9f6cad6b-88bc-19b9-a198-e08b813a3df</TrackingID></viewcord:ApplicationArea>';
		xmlResp += '<viewcord:DataArea xmlns:viewcord="' + viewcordEBMNS + '"><viewcord:Show responseCode="Error" hasActionStatusRecords="false" xmlns:viewcord="' + viewcordEBMNS + '">';
		xmlResp += '<viewcord:ServiceError errorCode="404" errorType="Network" errorDescription="Broken" xmlns:viewcord="' + cmnEBMNS + '"/>';
		xmlResp += '</viewcord:Show></viewcord:DataArea></viewcord:ShowCustomerOrder></soapenv:Body></soapenv:Envelope>';
    	coSoapOrderSearch orderSearch = new coSoapOrderSearch();
		Test.startTest();
		orderSearch.setSoapResponse(xmlResp);
		Test.stopTest();
		system.assertEquals('Error', orderSearch.Status);
		system.assertEquals('404', orderSearch.Error.ErrorCode);
		system.assertEquals('Network', orderSearch.Error.ErrorType);
		system.assertEquals('Broken', orderSearch.Error.ErrorDescription);
    	system.debug('TEST END: testSetSoapResponseErrorNoActionStatus');
	}
}