@IsTest
public with sharing class EmailMessageTriggerHandler_TEST
{
	public static final String CONTACT_CENTRE_TEAM_NAME = 'Hamilton/Didsbury - CST';
	public static final String CONTACT_CENTRE_QUEUE_NAME = 'CST_Hamilton_Didsbury';

    @Istest
    public static void testNewEmailToOldCaseLinkedToNewCase()
    {
        UnitTestDataFactory.setRunValidationRules(false);

        Integer caseAgeDays = CustomSettingsManager.getConfigSettingIntegerVal(CustomSettingsManager.NEW_CASE_FROM_EMAIL_CLOSED_DAYS);

        Contact con = [select Id from Contact LIMIT 1].get(0);
        Case c = [SELECT Id, CaseNumber, Status, IsClosed, ClosedDate, RecordTypeId, Origin, jl_NumberOutstandingTasks__c,
                  jl_Last_Queue__c, ContactId, Contact.Email, SuppliedEmail, CreatedDate,JL_Email_Address__c 
                  FROM Case where Status = 'Closed - Resolved' LIMIT 1].get(0);
        EmailMessage em = new EmailMessage();
        em.ParentId = c.Id;
        em.TextBody = 'Body of email';
        Test.startTest();
        insert em;
        Test.stopTest();
    }


    /**
* @descriptionTest Changing of Case_Status__c to 'Closed - Customer contacted' on Email record applies update to it's parent Case record
* @author@stuartbarber
*/

    @IsTest
    public static void testClosedCustomerContactedCaseStatusOnEmailMessageIsReflectedOnRelatedCase(){

        User runningUser = UserTestDataFactory.getRunningUser();

        User contactCentreUser;

        Contact testContact;
        EmailMessage testEmail;
        Case testCase;

        System.runAs(runningUser) {

            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
        }

        System.runAs(contactCentreUser) {

            testCase = [SELECT Id, CaseNumber, Status, IsClosed, ClosedDate, RecordTypeId, Origin, jl_NumberOutstandingTasks__c,
                        jl_Last_Queue__c, ContactId, Contact.Email, SuppliedEmail, CreatedDate,JL_Email_Address__c 
                        FROM Case where Status != 'Closed - Resolved' LIMIT 1].get(0);

			Test.startTest();

				testEmail = EmailTestDataFactory.createOutboundEmailMessage(testCase.Id);
				testEmail.Case_Status__c = 'Closed - Customer contacted';
				insert testEmail;

			Test.stopTest();

			Case updatedCase = [SELECT Status, jl_Action_Taken__c FROM Case WHERE Id = :testCase.Id LIMIT 1];

			System.assertEquals('Closed - Resolved', updatedCase.Status);
			System.assertEquals('Customer contacted', updatedCase.jl_Action_Taken__c);
		}
	}

    /**
* @descriptionTest Changing of Case_Status__c to 'Closed - Customer contacted' on Email record applies update to it's parent Case record
* @author@stuartbarber
*/

    @IsTest
    public static void testClosedNoCustomerContactRequiredCaseStatusOnEmailMessageIsReflectedOnRelatedCase(){

        User runningUser = UserTestDataFactory.getRunningUser();

        User contactCentreUser;

        Contact testContact;
        EmailMessage testEmail;
        Case testCase;

        System.runAs(runningUser) {

            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
        }

        System.runAs(contactCentreUser) {

            testCase = [SELECT Id, CaseNumber, Status, IsClosed, ClosedDate, RecordTypeId, Origin, jl_NumberOutstandingTasks__c,
                        jl_Last_Queue__c, ContactId, Contact.Email, SuppliedEmail, CreatedDate,JL_Email_Address__c 
                        FROM Case where Status != 'Closed - Resolved' LIMIT 1].get(0);

            Test.startTest();

            testEmail = EmailTestDataFactory.createOutboundEmailMessage(testCase.Id);
            testEmail.Case_Status__c = 'Closed - Customer contacted';
            insert testEmail;

            Test.stopTest();

            Case updatedCase = [SELECT Status, jl_Action_Taken__c FROM Case WHERE Id = :testCase.Id LIMIT 1];

            System.assertEquals('Closed - Resolved', updatedCase.Status);
            //System.assertEquals('No customer contact required', updatedCase.jl_Action_Taken__c);
        }
    }

    /**
* @descriptionTest Changing of Case_Status__c to 'No change to Case Status' on Email record applies no update to it's parent Case record's status
* @author@stuartbarber
*/

    @IsTest
    public static void testNoChangeToRelatedCaseIfCaseStatusIsNoChangeToCaseStatusOnEmailRecord(){

		User runningUser = UserTestDataFactory.getRunningUser();

		User contactCentreUser;

		Contact testContact;
		EmailMessage testEmail;
		Case testCase;

		System.runAs(runningUser) {

            Email_Filters__c emFilter = new Email_Filters__c(Name='Sample',ErrorMessage__c='Prohibited string found in message content. Please amend the word/phrase \'sample\'. If you feel this word/phrase is being blocked in error please raise a ticket through your standard IT support process.');
            insert emFilter;
            system.assert(!String.isBlank(Email_Filters__c.getAll().get('Sample').ErrorMessage__c));
			contactCentreUser = UserTestDataFactory.createContactCentreUser();
			insert contactCentreUser;
		}

        System.runAs(contactCentreUser) {

            testCase = [SELECT Id, CaseNumber, Status, IsClosed, ClosedDate, RecordTypeId, Origin, jl_NumberOutstandingTasks__c,
                        jl_Last_Queue__c, ContactId, Contact.Email, SuppliedEmail, CreatedDate,JL_Email_Address__c,jl_Action_Taken__c 
                        FROM Case where Status != 'Closed - Resolved' LIMIT 1].get(0);

            Test.startTest();

            testEmail = EmailTestDataFactory.createOutboundEmailMessage(testCase.Id);
            testEmail.Case_Status__c = 'No change to Case Status required';
            testEmail.Internal_Email__c = false;
            insert testEmail;

            Test.stopTest();

            Case updatedCase = [SELECT Status, jl_Action_Taken__c FROM Case WHERE Id = :testCase.Id LIMIT 1];

            System.assertNotEquals(testCase.Status, updatedCase.Status);
            System.assertNotEquals(testCase.jl_Action_Taken__c, updatedCase.jl_Action_Taken__c);
        }
    }


    @IsTest
    public static void testCloneCaseDetails(){
        User runningUser = UserTestDataFactory.getRunningUser();

        User contactCentreUser;

        Contact testContact;
        EmailMessage testEmail;
        Case testCase;

        System.runAs(runningUser) {

            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
        }

        System.runAs(contactCentreUser) {
            Test.startTest();
            testCase = [SELECT Id, CaseNumber, Status, IsClosed, ClosedDate, RecordTypeId, Origin, jl_NumberOutstandingTasks__c,
                        jl_Last_Queue__c, ContactId, Contact.Email, SuppliedEmail, CreatedDate,JL_Email_Address__c 
                        FROM Case where Status != 'Closed - Resolved' LIMIT 1].get(0);
			clsEmailMessageTriggerHandler emailHandlerClass = new clsEmailMessageTriggerHandler();
            Case clonedCase = emailHandlerClass.cloneCaseDetails(testCase);
            system.assertEquals('New', clonedCase.Status);
            system.assertEquals('New case created', clonedCase.jl_Action_Taken__c);

            Test.stopTest();
        }
    }

    /*
    @IsTest
    public static void testInboundMessage(){

		User runningUser = UserTestDataFactory.getRunningUser();

		User contactCentreUser;

		Contact testContact;
		EmailMessage testEmail;
		Case testCase;

		System.runAs(runningUser) {

CustomSettingTestDataFactory.initialiseTeamCustomSettings(CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_QUEUE_NAME);

contactCentreUser = UserTestDataFactory.createContactCentreUser();
contactCentreUser.userRoleid = UserTestDataFactory.getRoleId('JLP:CC:Management:TeamManagers:T3');
insert contactCentreUser;
}

System.runAs(contactCentreUser) {

testContact = CustomerTestDataFactory.createContact();
insert testContact;

testCase = CaseTestDataFactory.createQueryCase(testContact.Id);
            testCase.Status = 'New';//Closed - Resolved
insert testCase;

            Test.startTest();
            testEmail = EmailTestDataFactory.createInboundEmailMessage(testCase.Id);
            testEmail.Case_Status__c = 'No change to Case Status required';
            insert testEmail;

            Test.stopTest();
testCase = [SELECT Id, Status, jl_Action_Taken__c FROM Case WHERE Id = :testCase.Id LIMIT 1];
System.assertEquals(testCase.Status, 'New');//Closed - Resolved
}
}
    */
    /*
    @IsTest
    public static void testInsertAnonymousMessageLastMessageInbound(){
        User runningUser = UserTestDataFactory.getRunningUser();

User contactCentreUser;

		Contact testContact;
		EmailMessage testEmail;
		Case testCase;

		System.runAs(runningUser) {

			CustomSettingTestDataFactory.initialiseTeamCustomSettings(CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_QUEUE_NAME);

			contactCentreUser = UserTestDataFactory.createContactCentreUser();
			contactCentreUser.userRoleid = UserTestDataFactory.getRoleId('JLP:CC:Management:TeamManagers:T3');
			insert contactCentreUser;
		}

        System.runAs(contactCentreUser) {
            clsEmailMessageTriggerHandler emailHandlerClass = new clsEmailMessageTriggerHandler();
            testContact = CustomerTestDataFactory.createContact();
			insert testContact;

			testCase = CaseTestDataFactory.createQueryCase(testContact.Id);
            testCase.Status = 'Closed - Resolved';
			insert testCase;
			Test.startTest();
            testCase = [SELECT Id, Status, jl_Action_Taken__c FROM Case WHERE Id = :testCase.Id LIMIT 1];
            system.runAs(new User(Id=UserInfo.getUserId())){
                List<EmailMessage> emailsToBeAdded = new List<EmailMessage>();
                testEmail = EmailTestDataFactory.createInboundEmailMessage(testCase.Id);
				testEmail.Case_Status__c = 'No change to Case Status required';
                testEmail.Internal_Email__c = false;
                emailsToBeAdded.add(testEmail);
            insert emailsToBeAdded;
            }
Test.stopTest();
            clsEmailMessageTriggerHandler.insertAnonymousMessage(new Set<Id>{testCase.Id});

            system.assert([select Id,FromAddress from EmailMessage where FromAddress ='salesforce_null@johnlewis.co.uk'].size()==0);
        }
    }
    */

    @IsTest
    public static void testInsertAnonymousMessageLastMessageOutbound(){
        User runningUser = UserTestDataFactory.getRunningUser();

		User contactCentreUser;

		Contact testContact;
		EmailMessage testEmail;
		Case testCase;

        System.runAs(runningUser) {

            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
        }

        System.runAs(contactCentreUser) {
            clsEmailMessageTriggerHandler emailHandlerClass = new clsEmailMessageTriggerHandler();
            testCase = [SELECT Id, CaseNumber, Status, IsClosed, ClosedDate, RecordTypeId, Origin, jl_NumberOutstandingTasks__c,
                         jl_Last_Queue__c, ContactId, Contact.Email, SuppliedEmail, CreatedDate,JL_Email_Address__c 
                         FROM Case where Status != 'Closed - Resolved' LIMIT 1].get(0);
            /*system.runAs(new User(Id=UserInfo.getUserId())){
                
                List<EmailMessage> emailsToBeAdded = new List<EmailMessage>();
                EmailMessage outbound = EmailTestDataFactory.createOutboundEmailMessage(testCase.Id);
                outbound.Internal_Email__c = false;
                emailsToBeAdded.add(outbound);
                insert emailsToBeAdded;
            }*/

            Test.startTest();
            clsEmailMessageTriggerHandler.insertAnonymousMessage(new Set<Id>{testCase.Id});
            Test.stopTest();
            //system.assert([select Id,FromAddress from EmailMessage where FromAddress ='salesforce_null@johnlewis.co.uk'].size()>0);
        }
    }


    @IsTest
    public static void testInsertAnonymousMessageNoEmailsFound(){
        User runningUser = UserTestDataFactory.getRunningUser();

        User contactCentreUser;

        Contact testContact;
        EmailMessage testEmail;
        Case testCase;

        System.runAs(runningUser) {

            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
        }

        System.runAs(contactCentreUser) {
            clsEmailMessageTriggerHandler emailHandlerClass = new clsEmailMessageTriggerHandler();
            testCase = [SELECT Id, CaseNumber, Status, IsClosed, ClosedDate, RecordTypeId, Origin, jl_NumberOutstandingTasks__c,
                         jl_Last_Queue__c, ContactId, Contact.Email, SuppliedEmail, CreatedDate,JL_Email_Address__c 
                         FROM Case where Status != 'Closed - Resolved' LIMIT 1].get(0);
            Test.startTest();
            clsEmailMessageTriggerHandler.insertAnonymousMessage(new Set<Id>{testCase.Id});
            Test.stopTest();
            system.assert([select Id,FromAddress from EmailMessage where FromAddress='noreply@johnlewis.co.uk'].size()>0);
        }
    }


    @IsTest
    public static void testEmailDeleteValidation(){
        User runningUser = UserTestDataFactory.getRunningUser();

        User contactCentreUser;

        Contact testContact;
        EmailMessage testEmail;
        Case testCase;

        System.runAs(runningUser) {

            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            //contactCentreUser.userRoleid = UserTestDataFactory.getRoleId('JLP:CC:Management:TeamManagers:T3');
            insert contactCentreUser;
        }
        
        testCase = [SELECT Id, CaseNumber, Status, IsClosed, ClosedDate, RecordTypeId, Origin, jl_NumberOutstandingTasks__c,
                            jl_Last_Queue__c, ContactId, Contact.Email, SuppliedEmail, CreatedDate,JL_Email_Address__c 
                            FROM Case where Status = 'Closed - Resolved' LIMIT 1].get(0);
		
        //System.runAs(contactCentreUser) {
            clsEmailMessageTriggerHandler emailHandlerClass = new clsEmailMessageTriggerHandler();
            testContact = [select Id from Contact LIMIT 1].get(0);
            List<Config_Settings__c> CSList = new List<Config_Settings__c>();
            CSList.add(new Config_Settings__c(Name = 'HOCS_EMAIL_ADDRESSES',Text_Value__c = 'ayan.hore@johnlewis.co.uk'));
            CSList.add(new Config_Settings__c(Name = 'EmailDeleteProfiles',Text_Value__c='JL: Unrestricted Profile (5 Users Max),JL: IT Operations Administrator'));
            insert CSList;
            //Id NKURecordTypeId = CommonStaticUtils.getRecordTypeId('Case', 'NKU');
            
            //testCase = [SELECT Id, Status, jl_Action_Taken__c FROM Case WHERE Id = :testCase.Id LIMIT 1];
        
            //system.runAs(new User(Id=UserInfo.getUserId())){
                List<EmailMessage> emailsToBeAdded = new List<EmailMessage>();
                Test.startTest();
                
                EmailMessage outbound = EmailTestDataFactory.createOutboundEmailMessage(testCase.Id);
                outbound.Internal_Email__c = false;
                emailsToBeAdded.add(outbound);
                insert emailsToBeAdded;
                emailHandlerClass.emailDeleteValidation(emailsToBeAdded);
                clsEmailMessageTriggerHandler.copyToAddressOnCase(emailsToBeAdded);
                //system.assertEquals('ayan.hore@johnlewis.co.uk', [select Id,JL_Email_Address__c,JL_CC_Email_Address__c from Case LIMIT 1].get(0).JL_Email_Address__c);
                Test.stopTest();
            //}


        //}
    }

    /**
* @descriptionTest validation rule firing when NKU Resolution is not selected
*/
    @IsTest
    public static void testUpdateRelatedCaseNKUFieldsWithNoNKUResolution(){
        
        User runningUser = UserTestDataFactory.getRunningUser();
        
        User contactCentreUser;
        
        Contact testContact;
        EmailMessage testEmail;
        Case testCase;
        
        System.runAs(runningUser) {

            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
        }

        System.runAs(contactCentreUser) {

            testContact = [select Id from Contact LIMIT 1].get(0);

            Id NKURecordTypeId = CommonStaticUtils.getRecordTypeId('Case', 'NKU');
            testCase = [SELECT Id, CaseNumber, Status, IsClosed, ClosedDate, RecordTypeId, Origin, jl_NumberOutstandingTasks__c,
                         jl_Last_Queue__c, ContactId, Contact.Email, SuppliedEmail, CreatedDate,JL_Email_Address__c 
                         FROM Case where Status != 'Closed - Resolved' AND RecordTypeId = :NKURecordTypeId LIMIT 1].get(0);
            

            Test.startTest();
            try{
                testEmail = EmailTestDataFactory.createOutboundEmailMessage(testCase.Id);
                testEmail.Case_Status__c = 'Closed - Customer contacted';
                testEmail.NKU_Resolution__c = null;
                insert testEmail;
            }Catch(Exception ex){
                system.assert(ex.getMessage().containsIgnoreCase('NKU Resolution cannot be set to &quot;None&quot; when closing a NKU case.'));
            }

            Test.stopTest();
        }
    }

    /**
* @descriptionMake sure One and Done fields are set NKU Resolution is selected
*/
    @IsTest
    public static void testUpdateRelatedCaseNKUFieldsWithAcceptNKUResolution(){
        
        User runningUser = UserTestDataFactory.getRunningUser();
        
        User contactCentreUser;
        
        Contact testContact;
        EmailMessage testEmail;
        Case testCase;

        System.runAs(runningUser) {

            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
        }
        
        System.runAs(contactCentreUser) {

            testContact = [select Id from Contact LIMIT 1].get(0);
			Id NKURecordTypeId = CommonStaticUtils.getRecordTypeId('Case', 'NKU');
            testCase = [SELECT Id, CaseNumber, Status, IsClosed, ClosedDate, RecordTypeId, Origin, jl_NumberOutstandingTasks__c,
                         jl_Last_Queue__c, ContactId, Contact.Email, SuppliedEmail, CreatedDate,JL_Email_Address__c 
                         FROM Case where Status != 'Closed - Resolved' AND RecordTypeId = :NKURecordTypeId LIMIT 1].get(0);

            Test.startTest();
            testEmail = EmailTestDataFactory.createOutboundEmailMessage(testCase.Id);
            testEmail.Case_Status__c = 'Closed - Customer contacted';
            testEmail.NKU_Resolution__c = 'Accept';
            insert testEmail;
            Test.stopTest();

            Case updatedCase = [SELECT jl_One_and_Done_Case_Type_Detail__c, jl_One_and_Done_case_type__c FROM Case WHERE Id = :testCase.Id LIMIT 1];
            System.assertEquals(updatedCase.jl_One_and_Done_Case_Type_Detail__c, 'Post-Purchase '+testEmail.NKU_Resolution__c);
        }
    }

    /**
* @descriptionMake sure One and Done fields are set when purshase is set to false
*/
    @IsTest
    public static void testUpdateRelatedCaseNKUFieldsWithoutPurshase(){

        User runningUser = UserTestDataFactory.getRunningUser();
        
        User contactCentreUser;
        
        Contact testContact;
        EmailMessage testEmail;
        Case testCase;

        System.runAs(runningUser) {

            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
        }

        System.runAs(contactCentreUser) {

            //testContact = [select Id from Contact LIMIT 1].get(0);
            //insert testContact;

            Id NKURecordTypeId = CommonStaticUtils.getRecordTypeId('Case', 'NKU');
            testCase = [SELECT Id, CaseNumber, Status, IsClosed, ClosedDate, RecordTypeId, Origin, jl_NumberOutstandingTasks__c,
                         jl_Last_Queue__c, ContactId, Contact.Email, SuppliedEmail, CreatedDate,JL_Email_Address__c 
                         FROM Case where Status != 'Closed - Resolved' AND RecordTypeId = :NKURecordTypeId LIMIT 1].get(0);

            Test.startTest();
            testEmail = EmailTestDataFactory.createOutboundEmailMessage(testCase.Id);
            testEmail.Case_Status__c = 'Closed - Customer contacted';
            testEmail.NKU_Resolution__c = 'Decline';
            
            insert testEmail;
            Test.stopTest();

            //Case updatedCase = [SELECT jl_One_and_Done_Case_Type_Detail__c, jl_One_and_Done_case_type__c FROM Case WHERE Id = :testCase.Id LIMIT 1];
            //System.assertEquals(updatedCase.jl_One_and_Done_Case_Type_Detail__c, 'Pre-Purchase '+testEmail.NKU_Resolution__c);
        }
    }

    /**
*** @descriptionSend an inbound email message and create an email promise on an open case
**/

    @IsTest
    public static void testCreateEmailPromiseOnInboundEmailsToOpenCase(){
        
        User runningUser = UserTestDataFactory.getRunningUser();
        runningUser.Team__c = 'Hamilton/Didsbury - CST';
		update runningUser;

        User contactCentreUser;
        
        Contact testContact;
        EmailMessage inboundEmail;
        Case testCase;
        
        System.runAs(runningUser) {
			contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
        }

        System.runAs(contactCentreUser) {
			testCase = [SELECT Id, CaseNumber, Status, IsClosed, ClosedDate, RecordTypeId, Origin, jl_NumberOutstandingTasks__c,
                         jl_Last_Queue__c, ContactId, Contact.Email, SuppliedEmail, CreatedDate,JL_Email_Address__c 
                         FROM Case where Status != 'Closed - Resolved' LIMIT 1].get(0);

            Test.startTest();
            inboundEmail = EmailTestDataFactory.createInboundEmailMessage(testCase.Id);
            inboundEmail.Case_Status__c = 'No change to Case Status required';
            insert inboundEmail;
            system.assert(inboundEmail.Id != NULL);
            /*
            Case_Activity__c caseActivity = new Case_Activity__c();
            caseActivity = [SELECT Id, Activity_Description__c, Customer_Promise_Type__c,
                            Activity_Date__c, Activity_Start_Time__c,
                            Activity_End_Time__c
                            FROM Case_Activity__c WHERE Customer_Promise_Type__c = 'Email Promise' AND Activity_Completed_Date_Time__c = NULL LIMIT 1];
			
            System.assertEquals('New Email', caseActivity.Activity_Description__c);
            System.assertEquals('Email Promise', caseActivity.Customer_Promise_Type__c);
            System.assertEquals(System.now().date(), caseActivity.Activity_Date__c);*/
			Test.stopTest();
		}
		    

        
    }

    /**
*** @descriptionSend an inbound email message and create an reopen promise
**/
    @IsTest
    public static void testCreateReopenPromiseOnInboundEmailsToReopenAClosedCase(){

        User runningUser = UserTestDataFactory.getRunningUser();
        runningUser.Team__c = 'Hamilton/Didsbury - CST';
		update runningUser;

        User contactCentreUser;


        EmailMessage inboundEmail;
        EmailMessage outboundEmail;
        Case testCase = [SELECT Id, CaseNumber, Status, IsClosed, ClosedDate, RecordTypeId, Origin, jl_NumberOutstandingTasks__c,
                         jl_Last_Queue__c, ContactId, Contact.Email, SuppliedEmail, CreatedDate,JL_Email_Address__c 
                         FROM Case where Status = 'Closed - Resolved' LIMIT 1].get(0);

        System.runAs(runningUser) {
            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
        }
        
        System.runAs(contactCentreUser) {


            ClsCaseTriggerHandler.IS_UPDATE_IS_BEFORE_FIRST_CASE_INTERACTION_ASSIGNMENTS = true;
            ClsCaseTriggerHandler.EMAIL_LOG_TASK_CREATED = true;
            ClsCaseTriggerHandler.FIRST_AFTER_TRIGGER_RUN = false;
            ClsCaseTriggerHandler.FIRST_BEFORE_TRIGGER_RUN = false;
    		Test.startTest();
            inboundEmail = EmailTestDataFactory.createInboundEmailMessage(testCase.Id);
            insert inboundEmail;
            Test.stopTest();
            List<Case_Activity__c> caseActivity = [SELECT Id, Activity_Completed_Date_Time__c, Customer_Promise_Type__c
											FROM Case_Activity__c WHERE Case__c = :testCase.Id AND Activity_Completed_Date_Time__c = NULL];

            //System.assertEquals(1, caseActivity.size());
           // System.assertEquals('Reopen Promise', caseActivity[0].Customer_Promise_Type__c);


        }
    }
    @testSetup
    static void setup(){
        CustomSettingTestDataFactory.initialiseTeamCustomSettings(CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_QUEUE_NAME);

        Case testCase;
        Contact testContact;
        testContact = CustomerTestDataFactory.createContact();
        insert testContact;
		List<Case> caseList = new List<Case>();
        testCase = CaseTestDataFactory.createQueryCase(testContact.Id);
        testCase.Bypass_Standard_Assignment_Rules__c = true;
        testCase.Status = 'Closed - Resolved';
        testCase.jl_Action_Taken__c = 'Customer contacted';
        testCase.jl_Case_RestrictedAddComment__c = 'Comment';
        testCase.Final_Case_Activity_Resolution_Method__c = 'No action required';
        caseList.add(testCase);
        testCase = CaseTestDataFactory.createQueryCase(testContact.Id);
        caseList.add(testCase);
        testCase = CaseTestDataFactory.createNKUCase(testContact.Id);
        testCase.AlreadyPurchased__c = true;
        caseList.add(testCase);
        insert caseList;
        system.assertEquals(3,[select Id from Case].size());
    }
}