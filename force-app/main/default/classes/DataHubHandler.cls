public class DataHubHandler {
	
	public static final String OPENSPAN_INBOUND_ACTION = 'CREATECASE';
	public static final String ORIGIN_OPENSPAN = 'OPENSPAN';
	
	public static final String PROCESSING_FAILED = 'Fail';
	
	public static void mainEntry(Boolean isBefore, 
                               Boolean isDelete, Boolean isAfter, 
                               Boolean isInsert, Boolean isUpdate, 
                               Boolean isExecuting, List<Data_Hub__c> newlist, 
                               Map<ID,sObject> newmap, List<Data_Hub__c> oldlist, 
                               Map<Id,sObject> oldmap)
    {
        System.debug('@@entering DataHubHandler');
        
        /*****************************************************************************
		* sort the new records by origin to be passed on to appropriate transformers *
		******************************************************************************/
		
		// execute sorting code on every context change to keep up with the record changes
		List<Data_Hub__c> openSpanDataHubRecords = new List<Data_Hub__c>();
		if(newlist != null){
			for(Data_Hub__c dataRecord : newlist){
				if(dataRecord.Data_Hub_Origin__c != null &&
						dataRecord.Data_Hub_Origin__c.equalsIgnoreCase(ORIGIN_OPENSPAN)){
					if(dataRecord.Data_Hub_Action__c.equalsIgnoreCase(OPENSPAN_INBOUND_ACTION)){
						openSpanDataHubRecords.add(dataRecord);
					}
					// if there are any other OpenSpan actions to be implemented, they should be added here
				}
				// if there are any other uses of the Data Hub object, data for their handlers should be collected here
			}
		}
		
		/**********************************
		* Validate the data (if required) *
		**********************************/
		
		/*if(isBefore && isInsert){
			for(Data_Hub__c rec : openSpanDataHubRecords){
				rec = OpenSpanUtilities.validateCase(rec);
			}
		}*/
		
		
		/*********************************************************
		* Tranform the data (execute appropriate business logic) *
		*  Run the transformation in try-catch block to keep the *
		*  data in case of any runtime exceptions                *
		**********************************************************/
        
		// on insert, create a case and map it to an existing customer 
		// (if customer is matched by provided data, otherwise create an orphan case)
		if(isAfter && isInsert){
			
			try{
				
				if(!openSpanDataHubRecords.isEmpty()){
					Map<Id, Data_Hub__c> newDHMap = (Map<Id,Data_Hub__c>) newmap;
					OpenSpanDataHubTransformer transformer = new OpenSpanDataHubTransformer(openSpanDataHubRecords, newDHMap);
		        	transformer.transformToSFDC();
		        }
		        
			} catch (Exception e){
				
				updateAsFailed(openSpanDataHubRecords, e.getStackTraceString());
				
			}
        
		}

        System.debug('@@exiting DataHubHandler');
    }
    
    private static void updateAsFailed(List<Data_Hub__c> failedRecords, String errorMessage){
    	List<Data_Hub__c> recordsToUpdate = new List<Data_Hub__c>();
		for(Data_Hub__c rec : failedRecords){
			Data_Hub__c recToUpdate = new Data_Hub__c(Id = rec.Id, Data_Hub_Processing_Result__c = PROCESSING_FAILED, Error_Messages__c=errorMessage);
			recordsToUpdate.add(recToUpdate);
		}
		update recordsToUpdate;
    }

}