@isTest
private class coOrderHeader_TEST {
	
	static testmethod void searchOrderTest() {

		List<coOrderHeader> headerList = new List<coOrderHeader>();

		headerList.add(new coOrderHeader('123', null, 'Dave', null, 'steve@here.com', 'HX7 6DJ', null, Date.newInstance(2016, 12, 9), null));
		headerList.add(new coOrderHeader('456', null, 'Bob', null, 'steve@here.com', 'HX7 6DJ', null,Date.newInstance(2016, 11, 7), null));
		headerList.add(new coOrderHeader('789', null, 'John', null, 'john@here.com', 'L25 0NQ', null,Date.newInstance(2016, 9, 6), null));

		system.debug('unsorted list [' + headerList + ']');

		headerList.sort();

		system.debug('sorted list [' + headerList + ']');

		system.assertEquals('Dave', headerList[0].FirstName);
		system.assertEquals('Bob', headerList[1].FirstName);
		system.assertEquals('John', headerList[2].FirstName);
	}
}