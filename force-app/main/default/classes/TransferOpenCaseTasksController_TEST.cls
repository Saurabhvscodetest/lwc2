@IsTest
public with sharing class TransferOpenCaseTasksController_TEST 
{
 
 	@testSetup static void setup() {
         UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
    }
 
	/* 
	we limit the number of tasks selected for this test. The problem is that whilst the task update is properly bulkified the case updates are not
	This is not critical for production as users should only be assigning a few tasks to themselves and the test class shows that 20+ are ok
	In the long-term it might be wise to do more work on bulkifying cases but to date there is no urgent call to improve this 
	However, IF THIS TEST STARTS TO FAIL THEN CASE BULKIFICATION NEEDS TO BE ADDRESSED
	
	*/ 
		 


	@IsTest
	public static void testTransferOpenCaseTasks()
	{
		
		// Stop these rules firing..
		UnitTestDataFactory.setRunAssignments(false);
		UnitTestDataFactory.setRunValidationRules(false);
		UnitTestDataFactory.setRunWorkflowRules(false);
		
		// Create an Account
		Account acc = UnitTestDataFactory.createAccount('Assign Case Test');
		insert acc;
		
		// Create a Contact
		Contact con = UnitTestDataFactory.createContact(acc);
		insert con;
		
		List<Case> listc = new List<Case>();

		// Create an Open Case
		Case c = UnitTestDataFactory.createNormalCase(con);
		listc.add(c);
		
		Case cc = UnitTestDataFactory.createNormalCase(con);
		cc.Status = 'Closed - Resolved';
		listc.add(cc);
		
		insert listc;
		
		User testUser = UnitTestDataFactory.findStandardActiveUser();
		
		// Create Some tasks on this case for someone else
		Task t = new Task();
		t.WhatId = c.Id;
		t.OwnerId = testUser.Id;
		insert t;
		
		// Create task on an open case -- Cannot add tasks to closed cases!
		Task tcc = new Task();
		tcc.WhatId = cc.Id;
		tcc.OwnerId = testUser.Id;
		
		// insert tcc;
		
		Test.startTest();
		
		TransferOpenCaseTasksController controller = new TransferOpenCaseTasksController();
		
		// Specify a user!
		controller.taskForOwner.OwnerId = testUser.Id;
		
		// Find tasks
		controller.findTasksForUser();
	
		List<TransferOpenCaseTasksController.TaskWrapper> openCaseTasks = controller.getOpenCaseTaskWrappersForUser();
		
		Integer openCaseTasksSize = openCaseTasks.size();
		System.debug('size of openCaseTasks: ' + openCaseTasksSize);
		
		List<TransferOpenCaseTasksController.TaskWrapper> closedCaseTasks = controller.getClosedCaseTaskWrappersForUser();

		Integer closedCaseTasksSize = closedCaseTasks.size();
		System.debug('size of closedCaseTasks: ' + closedCaseTasksSize);
		
		List<Id> taskIds = new List<Id>();

		Integer looper = 0;
		for (TransferOpenCaseTasksController.TaskWrapper tw: openCaseTasks)
		{
			if (looper < 4)
			{
				tw.isSelected = true;
				taskIds.add(tw.task.Id);
			}
			looper++;
		}
		
		looper = 0;
		for (TransferOpenCaseTasksController.TaskWrapper tw: closedCaseTasks)
		{
			if (looper < 4)
			{
				tw.isSelected = true;
				taskIds.add(tw.task.Id);
			}
			looper++;
		}
		
		controller.openCaseTaskWrappersForUser = openCaseTasks;
		controller.closedCaseTaskWrappersForUser = closedCaseTasks;
		
		controller.transferSelectedTasksToMe();
		
		Test.stopTest();
		
		// Test that all tasks we found are assigned to me
		
		ID myId = UserInfo.getUserId();
		
		for (Task tt: [Select Id, OwnerId from Task where Id in : taskIds])
		{
			System.assert(JLHelperMethods.idsAreEqual(tt.OwnerId,myId),'Task was not assigned to me!');
		}
	}
	
	
}