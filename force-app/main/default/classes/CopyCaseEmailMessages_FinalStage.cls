/********************************************
**@author Ramachandran
** 
**  Edit    Date            By          Comment
**  001     15/12/15       Ram          Created. Base Chained Batch Apex Code.Constructor code to accept the parameters.
**  002     17/12/15       Ram          Edited some sections of the code so that they do not execute under Test Execution
**
**/
global class CopyCaseEmailMessages_FinalStage implements Database.batchable<SObject>,Database.stateful{
// Case Error Map to store the errors and send in emails if we encounter errors.
global Map<Id,String> caseErrorMap = new Map<Id,String>();
   
  // Variable to receive all the case id required to process and null the Pending Email Message field.
    global Set<id> allCaseId{get;set;}
     
      global CopyCaseEmailMessages_FinalStage(Set<id> lcaseId)
      {
           allCaseId =   lcaseId;            
      }
      
    // Start Method to get all cases which needs to be updated.
    global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator( [select Id, Pending_Email_Messages__c from Case where id IN :allCaseId ]);
   }

    // Execute methods to clear the pending email message field and store the errors under each batch
   global void execute(Database.BatchableContext BC, List<sObject> scope){
   List<Case> allCases= scope;
     for (Case c : allCases)
     {
         c.Pending_Email_Messages__c ='';
     }
      //This is where error can occure
        Database.SaveResult[] results = Database.update(allCases,false);
        Integer i =0;
        for(Database.SaveResult sr: results)
        {
            
            if (!sr.isSuccess())
            {
                for (Database.Error er : sr.getErrors())
                {
                    //Failed Record 
                    caseErrorMap.put(allCases.get(i).id,er.getMessage());
                }
            }
            i++;
        }
    }

// Finish Method to send in email of all the cases impacted which could not be updated.
   global void finish(Database.BatchableContext BC){
   // Send Final Email if there is an caseErroMap
   
       
           String textBody ='The Following cases with their ids have failed during copyEmailAttachment Batch Process  \n';
           String toAddress ='connex.it.support@johnlewis.co.uk';
           List<String> recepients = new List<String>();
           recepients.add(toAddress);
           String subject ='Error Encountered in Processing Batch - Copy Email Attachments ';
           if (!caseErrorMap.isEmpty()){
                  for (Id csId : caseErrorMap.KeySet())
                  {
                      textBody+= 'Salesforce Errored Record Id :'+ csId + ' Error is: '+caseErrorMap.get(csId)+'\n';
                  }   
                  if(!Test.isRunningTest())
                  {      
                      EmailNotifier.sendNotificationEmail(recepients, subject, textBody);
                   }
           }
           
           
    }
}