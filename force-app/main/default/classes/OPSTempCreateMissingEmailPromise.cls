global class OPSTempCreateMissingEmailPromise implements Database.Batchable<sObject>,Database.Stateful
{   
    global Map<Id,String> ErrorMap = new Map<Id,String>();
    global integer result=0;
	global String Email_Promise = 'Email Promise';
    Datetime currentTime = Datetime.now();
    Datetime cutoffTime = currentTime.addMinutes(-30);
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        System.debug('Cutoff Time ----- '+cutoffTime );   

        return Database.getQueryLocator([
                select Id, IsClosed, Origin
                  from Case 
                  where Case_Owner__c like '%ACTIONED%'
                    and Status not IN ('Closed - Resolved','Closed - Duplicate',
                                       'Closed - Unresolved',
                                       'Closed - SPAM','Closed - No Response Required', 
                                       'Closed - Assigned to Another Branch') 
                    and IsClosed = false
                    and jl_Customer_Response_By__c < :cutoffTime 
                    and CreatedDate > 2019-09-24T00:00:00.000+0000 
                    and Next_Case_Activity_Due_Date_Time__c = NULL]);
    }
    
    global void execute(Database.BatchableContext BC, List<Case> caseList)
    {
        If(caseList.size()>0){
            Set<Id> caseIds = new Set<Id>();
            for(Case obj : caseList){
                caseIds.add(obj.Id);
            //    System.debug('Case Ids to process -' + obj.Id );
            }

            System.debug('Number of caseIds to select -' + caseIds.size());
            Map<Id,List<Case_Activity__c>> case2ActivityMap = new Map<Id,List<Case_Activity__c>>();

            //Search for open Initial Customer contact promise as we don't want to create a new 
            //promise if an open one exists
            for(Case_Activity__c obj : [select Id,Case__c from Case_Activity__c                                     
                                        where Case__c IN :caseIds 
                                          and Activity_Completed_Date_Time__c = NULL 
                                          and Customer_Promise_Type__c = 'Initial Customer Contact Promise']){
                if(case2ActivityMap.containsKey(obj.Case__c)){
                    case2ActivityMap.get(obj.Case__c).add(obj);
                }
                else{
                    case2ActivityMap.put(obj.Case__c,new List<Case_Activity__c>{obj} );
                }
            }

            //System.debug('case2ActivityMap ' + case2ActivityMap);
            List<Case_Activity__c> emailPromisesToInsert = new List<Case_Activity__c>();
            for(Case obj : caseList){
                if(!case2ActivityMap.containsKey(obj.Id) )
                {
                    Integer slaHoursEmail = 6; 
                    Case_Promise_Configuration__c promiseConfiguration = Case_Promise_Configuration__c.getValues(Email_Promise);
                    if(promiseConfiguration != null)  
                    {
                        slaHoursEmail = (Integer)promiseConfiguration.SLA_hours__c;
                    }
                   
                    System.debug('Activity record '+ CaseActivityUtils.createNewCaseActivityRecord(obj, 'New Email', 'Email Promise', slaHoursEmail));
                    emailPromisesToInsert.add(CaseActivityUtils.createNewCaseActivityRecord(obj, 'New Email', 'Email Promise', slaHoursEmail));
                }
            }
            System.debug(' Acitvities to insert ' + emailPromisesToInsert.size());
            if(emailPromisesToInsert.size() > 0){
                try{
                    List<Database.SaveResult> srList = Database.insert(emailPromisesToInsert, false);
                    if(srList.size()>0){
                        for(Integer counter = 0; counter < srList.size(); counter++){
                            if (srList[counter].isSuccess()){
                                result++;
                            }else{
                                if(srList[counter].getErrors().size()>0){
                                    for(Database.Error err : srList[counter].getErrors()){
                                        ErrorMap.put(srList.get(counter).id,err.getMessage());
                                    }
                                }                                
                            }
                        }
                    }
                }catch(Exception ex){
                    EmailNotifier.sendNotificationEmail('An error has occured when inserting email promise', ex.getMessage());
                }
            } 
        }
    }
    global void finish(Database.BatchableContext BC){
            String textBody = '';
            Set<Id> errorid = new Set<ID>();
            textBody+= result +' cases updated successfully '+'\n';
            if(!ErrorMap.isEmpty()){
                for(Id recordids : ErrorMap.KeySet()){
                        errorid.add(recordids);
                }
                textBody+='Error Log: '+errorid+'\n';
            }
            System.debug('textBody ----- '+textBody);
            EmailNotifier.sendNotificationEmail('Batch Executed Successfully', textbody);
    }
}