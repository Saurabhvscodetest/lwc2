public class coSoapManager {

    // last error web service error
    public String Status {get; private set;}
    public String ErrorCode {get; private set;}
    public String ErrorType {get; private set;}
    public String ErrorDescription {get; private set;}

    public Integer CustomersFound {get; private set;}
    public Integer CustomersSearched {get; private set;}
    // need to expose the total number of customers to the caller as an indication that there could be more orders
    public Integer TotalCustomers {
        get {
            return this.Customers != null ? this.Customers.size() : 0;
        }
    }

    public Boolean MoreCustomers {
        get {
            return this.CustomerIndex < this.TotalCustomers - 1;
        }
    }

    // last customer index in the customer list
    private Integer CustomerIndex {get; set;}
    // list to hold retrieved customers
    private List<Customer> Customers {get; set;}
    // stateful search criteria
    private Customer SearchCriteria {get; set;}

    // default constructor
    public coSoapManager() {

        initialiseClass();
    }

    private void initialiseClass() {

        // default the CustomerIndex to zero
        this.CustomerIndex = 0;
        this.CustomersSearched = 0;
        // set up an empty list of customers
        this.Customers = new List<Customer>();
    }

    // clears any search results    
    public void ClearCustomers() {

        initialiseClass();
    }

    // this method is called if the user has provided an order id in their search data - we don't need anything else
    public List<coOrderHeader> getOrderHeaders(String orderId) {

        wipeLastError();

        List<coOrderHeader> result = new List<coOrderHeader>();

        // call the get order method as though we wanted the details and create an order header out of the information
        coOrderDetail orderDetail = getOrder(orderId);

        if (this.Status == 'Success') {

            system.debug('orderDetail [' + orderDetail + ']');

            coOrderHeader orderHeader = new coOrderHeader();
            orderHeader.OrderNumber = orderDetail.OrderNumber;
            orderHeader.Salutation = orderDetail.BillingAddress.Title;
            orderHeader.FirstName = orderDetail.BillingAddress.FirstName;
            orderHeader.LastName = orderDetail.BillingAddress.LastName; 
            orderHeader.Email = orderDetail.BillingAddress.Email;
            orderHeader.Postcode= orderDetail.BillingAddress.Postcode;
            orderHeader.OrderStatus = orderDetail.OrderStatusCode+' : '+orderDetail.OrderStatusDescription;
            orderHeader.DatePlaced = orderDetail.OrderDate;
            orderHeader.TotalOrderValue = orderDetail.TotalOrderValue;

            result.add(orderHeader);
        }

        return result;
    }


    // this method builds up the customer orders list to display on the customer orders inline vf page
    public List<coOrderHeader> getOrderHeaders(List<coOrderHeader> currentHeaders, Integer maxCallouts, Customer searchCriteria) {

        wipeLastError();

        List<coOrderHeader> result = currentHeaders;

        // if this is the first time here then we first need to populate the customers using coSoapCustomerSearch.
        if (this.CustomerIndex == 0) {
            this.SearchCriteria = searchCriteria;
            populateCustomers();

            system.debug('this.TotalCustomers [' + this.TotalCustomers + ']');
        }

        // if we find no customers then we need to return to the caller - there may be an error description
        if (this.TotalCustomers == 0) return result;

        Integer loopLimit = (this.CustomerIndex + maxCallouts > this.TotalCustomers - this.CustomerIndex) ? 
                                this.TotalCustomers - this.CustomerIndex : 
                                    maxCallouts;

        system.debug('loopLimit [' + loopLimit + ']');

        // ok, so we have work to do
        // we should loop based on loop limit
        for (Integer index = 0; index < loopLimit; index++) {

            system.debug('index [' + index + '] against CustomerIndex [' + this.CustomerIndex + '] out of [' + this.Customers.size() + '] remaining Customers');

            // break out the loop if we have nothing to search on
            if (this.CustomerIndex > this.Customers.size() - 1) break;

            // increase the customers searched each time we use one
            this.CustomersSearched++;

            // make a web service call and deal with the result
            coSoapOrderSearch orderSearch = new coSoapOrderSearch();
            orderSearch.CustomerId = this.Customers.get(this.CustomerIndex).CustomerId;

            // generate the soap request
            String reqXML = orderSearch.getSoapRequest();
            // get the endpoint
            String url = getWSEndpointUrl('SearchCustomerOrders');
            // call the web service
            HttpResponse res = invokeWS(url, reqXML, 'POST');
      
            // parse the response
            orderSearch.setSoapResponse(res.getBody());

            // always store the last error if there is one in case we get no results and no more to show
            this.Status = orderSearch.Status;

            if (this.Status != 'Success') {
                this.ErrorCode = orderSearch.Error.ErrorCode;
                this.ErrorType = orderSearch.Error.ErrorType;
                this.ErrorDescription = orderSearch.Error.ErrorDescription;
            }

            // first decide if we up the customer index or remove the customer from the list
            // if we get a success then we got at least 1 order for this customer
            if (this.Status == 'Success') {

                // get the oders for this customer and add them to the order header list to be returned
                for (coSoapOrderSearch.OrderSearchResult order : orderSearch.Orders) {

                    coOrderHeader orderHeader = new coOrderHeader();
                    orderHeader.OrderNumber = order.Id;
                    orderHeader.Salutation = Customers.get(this.CustomerIndex).Salutation;
                    orderHeader.FirstName = Customers.get(this.CustomerIndex).FirstName;
                    orderHeader.LastName = Customers.get(this.CustomerIndex).LastName;
                    orderHeader.Email = Customers.get(this.CustomerIndex).Email;
                    orderHeader.Postcode = Customers.get(this.CustomerIndex).Postcode;
                    orderHeader.OrderStatus = order.Status;
                    orderHeader.DatePlaced = Date.valueOf(order.DateTimePlaced); // just need the date part here
                    orderHeader.TotalOrderValue = order.TotalValue;

                    // add it into the existing headers
                    result.add(orderHeader);
                }

                // increase the customer index to the next customer
                this.CustomerIndex++;

            // if we got an error or warning then we have no orders for this customer
            } else {

                // we need to remove them from the customer list 
                this.Customers.remove(this.CustomerIndex);
            }
        }

        // check to see if we have any results or another chance to get any
        // if we do then switch the status to success
        if ( this.Status != 'Error' && 
            ( result.size() > 0 || this.CustomerIndex <= this.TotalCustomers)) {
            this.Status = 'Success';
        }

        return result;

    }

    private void populateCustomers() {

        // clear the last error
        wipeLastError();

        coSoapCustomerSearch customerSearch = new coSoapCustomerSearch();
        customerSearch.FirstName = this.SearchCriteria.FirstName;
        customerSearch.LastName = this.SearchCriteria.LastName;
        customerSearch.Postcode = this.SearchCriteria.Postcode;
        customerSearch.Email = this.SearchCriteria.Email;

        // generate the soap request
        String reqXML = customerSearch.getSoapRequest();
        // get the endpoint
        String url = getWSEndpointUrl('SearchCustomer');
        // call the web service
        HttpResponse res = invokeWS(url, reqXML, 'POST');

        // parse the response
        customerSearch.setSoapResponse(res.getBody());

        // if we had an error then return
        this.Status = customerSearch.Status;

        if (this.Status != 'Success') {
            this.ErrorCode = customerSearch.Error.ErrorCode;
            this.ErrorType = customerSearch.Error.ErrorType;
            this.ErrorDescription = customerSearch.Error.ErrorDescription;

            return;
        }

        // make sure we have some customers and then store them locally
        if (customerSearch.Customers != null) {

            for (coSoapCustomerSearch.CustomerSearchResult customer : customerSearch.Customers) {

                Customer newCustomer = new Customer();
                newCustomer.Salutation = customer.Salutation;
                newCustomer.FirstName = customer.FirstName;
                newCustomer.LastName = customer.LastName;
                newCustomer.CustomerId = customer.CustomerId;
                newCustomer.Postcode = customer.Postcode;
                newCustomer.Email = customer.Email;

                this.Customers.add(newCustomer);
            }
        }

        // this gets set once for the calling class
        this.CustomersFound = this.TotalCustomers;
        system.debug('this.Customers [' + this.Customers + ']');
    }

    public coOrderDetail getOrder(String OrderId) {

        wipeLastError();

        coSoapOrderView orderView = new coSoapOrderView();
        orderView.OrderId = OrderId;

        // generate the soap request
        String reqXML = orderView.getSoapRequest();
        // get the endpoint
        String url = getWSEndpointUrl('ViewCustomerOrders1');
        
        // call the web service
        HttpResponse res = invokeWS(url, reqXML, 'POST');
        
        // parse the response
        orderView.setSoapResponse(res.getBody());

        system.debug('orderView [' + orderView + ']');

        // if we had an error then return
        this.Status = orderView.Status;

        if (this.Status != 'Success') {
            this.ErrorCode = orderView.Error.ErrorCode;
            this.ErrorType = orderView.Error.ErrorType;
            this.ErrorDescription = orderView.Error.ErrorDescription;
        }        

        system.debug('orderView.Order [' + orderView.Order + ']');

        // send back the order to the controller - this may be null based on wether there was an error or not
        return orderView.Order;
    }

    public String getEmailContent(String EmailQueueId) {

        wipeLastError();

        coSoapEmailContent emailContent = new coSoapEmailContent();
        emailContent.EmailQueueId = EmailQueueId;

        // generate the soap request
        String reqXML = emailContent.getSoapRequest();
        // get the endpoint
        String url = getWSEndpointUrl('EmailContent');
        // call the web service
        HttpResponse res = invokeWS(url, reqXML, 'POST');
        // parse the response
        emailContent.setSoapResponse(res.getBody());

        // if we had an error then return
        this.Status = emailContent.Status;

        if (this.Status != 'Success') {
            this.ErrorCode = emailContent.Error.ErrorCode;
            this.ErrorType = emailContent.Error.ErrorType;
            this.ErrorDescription = emailContent.Error.ErrorDescription;
        }        

        // send back the content to the controller - this may be null based on wether there was an error or not
        return emailContent.EmailContent;        
    }

    //Future Method to make WS calls to post Comments
    @Future(callout=true)
    public static void postCaseComments(Set<Id> caseIdSet, Map<Id, String> orderIdByCaseIdMap, String userName, String userRole) {

        // any errors generated are lost as there is no static audit manager at present
        List<CaseComment> caseCommentList = [select Id, CommentBody, ParentId, Parent.CaseNumber from CaseComment where ParentId IN :caseIdSet];

        postCaseComments(caseCommentList, orderIdByCaseIdMap, userName, userRole);
    }

    @Future(callout=true)
    public static void postCaseCommentsOnUpdate(Set<Id> caseCommentID, Map<Id, String> orderIdByCaseIdMap, String userName, String userRole) {

        // any errors generated are lost as there is no static audit manager at present
        List<CaseComment> caseCommentList = [select Id, CommentBody, ParentId, Parent.CaseNumber from CaseComment where Id IN :caseCommentID];
        postCaseComments(caseCommentList, orderIdByCaseIdMap, userName, userRole);
    }

    public static void postCaseComments(List<CaseComment> caseCommentList, Map<Id, String> orderIdByCaseIdMap, String userName, String userRole) {

        coSoapPostCaseComment postCaseComment = new coSoapPostCaseComment();
        postCaseComment.CaseCommentList = caseCommentList;
        postCaseComment.OrderIdByCaseIdMap = orderIdByCaseIdMap;
        postCaseComment.UserName = userName;
        postCaseComment.UserRole = userRole;

        if (Test.isRunningTest())
            return;
        //03/06/2015-END
            
        // generate the soap request
        String reqXML = postCaseComment.getSoapRequest();
        // get the endpoint
        String url = getWSEndpointUrl('CaseComment');
        // call the web service
        HttpResponse res = invokeWS(url, reqXML, 'POST');
        // parse the response
        postCaseComment.setSoapResponse(res.getBody());
    }
    
   
    // Method that makes the web service callout.
    private static HttpResponse invokeWS(String url, String xmlBody, String httpMethod) {

        HttpResponse res;
        // Instantiate a new http object
        Http h = new Http();
        // Instantiate a new HTTP request, specify the method (POST) as well as the endpoint
        HttpRequest req = new HttpRequest();
        req.setBody(xmlBody);
        req.setMethod(httpMethod);  
        req.setTimeout(120000);
        
        req.setClientCertificateName('SFDC_OCE_TO_JL_ORDERMGMT');
        
        req.setEndpoint(url);
        if(url.containsIgnoreCase('ccOrderManagement_v1')){
            req.setHeader('SOAPAction', 'urn:johnlewis:service:order:getClickAndCollectOrderEmailDetails');
        }

        system.debug('soap request [' + req.getBody() + ']');
    
        res = h.send(req);
        system.debug('res.getHeaderKeys() [' + res.getHeaderKeys() + ']');
        system.debug('res.getStatusCode() [' + res.getStatusCode() + ']');
        system.debug('soap response [' + res.getBody() + ']');

        return res;
    }    

    // Get the endpoint based on the Web service name. Picked from custom settings
    private static String getWSEndpointUrl(String obj) {

        system.debug('endpoint [' + obj + ']');
       
        String serverEndPoint = CustomSettingsManager.getConfigSettingStringVal(CustomSettingsManager.WS_Endpoint_SearchCustomer);
        
        if (obj == 'CaseComment') {
            return CustomSettingsManager.getConfigSettingStringVal(CustomSettingsManager.WS_Endpoint_ManageCustomerComments);
        } else if (obj == 'SearchCustomer') {
            return CustomSettingsManager.getConfigSettingStringVal(CustomSettingsManager.WS_Endpoint_SearchCustomer);
        } else if (obj == 'SearchCustomerOrders') { 
            return CustomSettingsManager.getConfigSettingStringVal(CustomSettingsManager.WS_Endpoint_ViewCustomerOrdersV2);
        } else if (obj == 'ViewCustomerOrders1') {
            return CustomSettingsManager.getConfigSettingStringVal(CustomSettingsManager.WS_Endpoint_ViewCustomerOrdersV1);
        } else if (obj == 'EmailContent') {
            return CustomSettingsManager.getConfigSettingStringVal(CustomSettingsManager.WS_Endpoint_EmailContent);
        }
        
        return '--';
    }

    private void wipeLastError() {
        this.Status = '';
        this.ErrorCode = '';
        this.ErrorType = '';
        this.ErrorDescription = '';
    }

    // public sub class to hold the returned shoppers from the customer search.
    // this will only be needed if we don't search with an order number but needs to be kept in state
    public class Customer {
        
        public String Salutation {get; set;}
        public String FirstName {get; set;}
        public String LastName {get; set;}
        public String CustomerId {get; set;}
        public String Postcode {get; set;}
        public String Email {get; set;}     
    }
}