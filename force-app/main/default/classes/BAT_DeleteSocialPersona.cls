/* Description : Delete SocialPersona Records
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0  29/03/2019        Vijay Ambati                      Created              COPT-4362
*
*/
global class BAT_DeleteSocialPersona implements Database.Batchable<sObject>,Database.Stateful{
    global Map<Id,String> ErrorMap = new Map<Id,String>();
    global integer result=0;
    private static final DateTime CREATED_DATE = System.now().addYears(-5);
    global String query;
    global Database.QueryLocator start(Database.BatchableContext BC){
        
	GDPR_Record_Limit__c gDPRLimit = GDPR_Record_Limit__c.getValues('BAT_DeleteSocialPersona');
    if(gDPRLimit.Record_Limit__c != NULL){
	String recordLimit = gDPRLimit.Record_Limit__c;
    String limitSpace = ' ' + 'Limit' + ' '; 
	query = 'Select Id from SocialPersona where CreatedDate <: CREATED_DATE' + limitSpace + recordLimit;
        }
        else{
            query = 'Select Id from SocialPersona where CreatedDate <: CREATED_DATE';
        }
		
      return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, list<SocialPersona> scope){
        List<Id> recordstoDelete = new List<Id>();
        try{
             for(SocialPersona recordScope : scope){
                   recordstoDelete.add(recordScope.id);
             }
            
            list<Database.deleteResult> srList = Database.delete(recordstoDelete, false);
            for(Integer counter = 0; counter < srList.size(); counter++) {
                if (srList[counter].isSuccess()) {
                    result++;
                }else {
                    for(Database.Error err : srList[counter].getErrors()) {
                        ErrorMap.put(srList.get(counter).id,err.getMessage());
                    }
                }
            }
            Database.emptyRecycleBin(scope);
        }Catch(exception e){
            EmailNotifier.sendNotificationEmail('Exception from BAT_DeleteSocialPersona', e.getMessage());
        }
    }
    global void finish(Database.BatchableContext BC) {
        String textBody ='';
        set<Id> allIds = new set<Id>();
        textBody+= result +' records deleted in object Social Persona'+'\n';
        if (!ErrorMap.isEmpty()){
            for (Id recordids : ErrorMap.KeySet())
            {
                allIds.add(recordids);
            } 
            textBody+= 'Error log: '+allIds+'\n';
        }
        EmailNotifier.sendNotificationEmail('GDPR Delete Social Persona Records', textBody);
    }
}