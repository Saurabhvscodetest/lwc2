Public class CookbookBot_GetTranscriptVariables {

  Public class TranscriptInput {
    @InvocableVariable(required=true)
    Public ID routableID;
  }
  
  Public class VisitorNameOutput {
    @InvocableVariable(required=true)
    Public String sFirstName;
      
    @InvocableVariable(required=true)
    Public String sOrderNumber;
      
  }

  @InvocableMethod(label='Get User Name')
  Public static List<VisitorNameOutput> getUserName(List<TranscriptInput> transcripts) {

      List<VisitorNameOutput> names = new List<VisitorNameOutput>();
      
      if(transcripts != null){
          
          try{
              
              for (TranscriptInput transcript : transcripts) {
      
      LiveChatTranscript transcriptRecord = [SELECT FirstName__c,OrderNumber__c
                                             FROM LiveChatTranscript 
                                             WHERE Id = :transcript.routableID 
                                             LIMIT 1];
      
      VisitorNameOutput nameData = new VisitorNameOutput();
      nameData.sFirstName = transcriptRecord.FirstName__c;
      nameData.sOrderNumber = transcriptRecord.OrderNumber__c;      
      
      names.add(nameData);
              
              }}
          
          catch(exception e){
              
              System.debug('@@@ Exception ' + e.getMessage());
          }
          
      }

    System.debug('@@@ Inside CookBook Class' + names);
      
    return names;
  }
}