/* Description  : Delete case feed records when cases deleted from COPT-4293
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   20/02/2019        Vignesh Kotha                   Created               COPT-4293
*
*/
global class BAT_DeleteCaseFeedback implements Database.Batchable<sObject>,Database.Stateful {
    global Map<Id,String> ErrorMap = new Map<Id,String>();
    global integer result=0;
    global String query;
    global Database.QueryLocator start(Database.BatchableContext BC){        
        return Database.getQueryLocator([select id,Case__c from Case_Feedback__c where Case__c = NULL]);
    }
    global void execute(Database.BatchableContext BC, list<Case_Feedback__c> scope){
        List<Id> recordstoDelete = new List<Id>();
        if(scope.size()>0){
            try{
                for(Case_Feedback__c recordScope : scope){
                    recordstoDelete.add(recordScope.id);
                }
                
                list<Database.deleteResult> srList = Database.delete(recordstoDelete, false);
                for(Integer counter = 0; counter < srList.size(); counter++) {
                    if (srList[counter].isSuccess()) {
                        result++;
                    }else {
                        for(Database.Error err : srList[counter].getErrors()) {
                            ErrorMap.put(srList.get(counter).id,err.getMessage());
                        }
                    }
                }
                Database.emptyRecycleBin(scope);
            }Catch(exception e){
                EmailNotifier.sendNotificationEmail('Exception from BAT_DeleteCaseFeedback', e.getMessage());
            }
        }
    }
    global void finish(Database.BatchableContext BC) {
        String textBody ='';
        set<Id> allIds = new set<Id>();
        textBody+= result +' records deleted in object Case Feedback'+'\n';
        if (!ErrorMap.isEmpty()){
            for (Id recordids : ErrorMap.KeySet())
            {
                allIds.add(recordids);
            } 
            textBody+= 'Error log: '+allIds+'\n';
        }
        EmailNotifier.sendNotificationEmail('GDPR Delete Case Feedback Records', textBody);
    }
}