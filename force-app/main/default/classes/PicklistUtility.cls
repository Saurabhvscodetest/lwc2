/******************************************************************************
* @author       Ayan Hore
* @date         14-Mar-2018
* @description  Utility class that validates all necessary dependent picklist values are present when the case record type is "Complaint" and Status is "Closed - Resolved"
*******************************************************************************
*/
public class PicklistUtility {
    
    private Map<String, Integer> base64Vals = null;
    public Map<String,List<Field_Dependency_Information__mdt>> dependencyMetadataPerObjectMap = new Map<String,List<Field_Dependency_Information__mdt>>();
    private String returnVal = 'Please add values to these field(s) to proceed : ';
    
    public PicklistUtility(){
        //Prepare a map of all dependent field information categorized by object type
        for(Field_Dependency_Information__mdt obj : [SELECT Object_API__c, Record_Type_Name__c, Type__c, Controlling_Field__c, Dependent_Field__c FROM Field_Dependency_Information__mdt]){
            if(dependencyMetadataPerObjectMap.containsKey(obj.Object_API__c)){
                dependencyMetadataPerObjectMap.get(obj.Object_API__c).add(obj);
            }
            else{
                dependencyMetadataPerObjectMap.put(obj.Object_API__c,new List<Field_Dependency_Information__mdt>{obj});
            }
        }
    }
    
    private List<Integer> ConvertB64ToInts(string b64Str){
        if (base64Vals == null) {
            base64Vals = new Map<String, Integer> {
                'A'=>00,'B'=>01,'C'=>02,'D'=>03,'E'=>04,'F'=>05,'G'=>06,'H'=>07,'I'=>08,'J'=>09,
                    'K'=>10,'L'=>11,'M'=>12,'N'=>13,'O'=>14,'P'=>15,'Q'=>16,'R'=>17,'S'=>18,'T'=>19,
                    'U'=>20,'V'=>21,'W'=>22,'X'=>23,'Y'=>24,'Z'=>25,'a'=>26,'b'=>27,'c'=>28,'d'=>29,
                    'e'=>30,'f'=>31,'g'=>32,'h'=>33,'i'=>34,'j'=>35,'k'=>36,'l'=>37,'m'=>38,'n'=>39,
                    'o'=>40,'p'=>41,'q'=>42,'r'=>43,'s'=>44,'t'=>45,'u'=>46,'v'=>47,'w'=>48,'x'=>49,
                    'y'=>50,'z'=>51,'0'=>52,'1'=>53,'2'=>54,'3'=>55,'4'=>56,'5'=>57,'6'=>58,'7'=>59,
                    '8'=>60,'9'=>61,'+'=>62,'/'=>63
                    };
                        }
        List<Integer> ints = new List<Integer>();
        for (Integer idx = 0; idx< b64Str.length(); ++idx) {
            String c = String.fromCharArray(new List<Integer> { b64Str.charAt(idx)});
            ints.add(base64Vals.get(c));
        }
        return ints;
    }
    
    
    /**
* GetDependentOptions method called from the validateComplaintCaseCloseForm method
* @params: Object Name, Controlling Field Name, Dependent Field Name 
* @rtnval: Map of Controlling picklist values and respective Dependent picklist values
**/    
    
    public Map<String,List<String>> GetDependentOptions(String pObjName, String pControllingFieldName, String pDependentFieldName) {
        Map<String,List<String>> mapResults = new Map<String,List<String>>();
        
        //verify/get object schema
        Schema.SObjectType pType = Schema.getGlobalDescribe().get(pObjName);
        if(pType == null){ 
            return mapResults;
        }
        Map<String, Schema.SObjectField> objFieldMap = pType.getDescribe().fields.getMap();
        
        //verify field names
        if (!objFieldMap.containsKey(pControllingFieldName) || !objFieldMap.containsKey(pDependentFieldName)){
            return mapResults;
        }
        
        //get the control & dependent values
        List<Schema.PicklistEntry> ctrl_ple = objFieldMap.get(pControllingFieldName).getDescribe().getPicklistValues();
        List<Schema.PicklistEntry> dep_ple = objFieldMap.get(pDependentFieldName).getDescribe().getPicklistValues();
        
        //initialize results mapping
        for(Integer pControllingIndex=0; pControllingIndex < ctrl_ple.size(); pControllingIndex++) {
            mapResults.put( ctrl_ple[pControllingIndex].getLabel(), new List<String>());
        }
        
        //serialize dep entries
        List<TPicklistEntry> objDS_Entries = new List<TPicklistEntry>();
        objDS_Entries = (List<TPicklistEntry>)JSON.deserialize(JSON.serialize(dep_ple), List<TPicklistEntry>.class);
        
        for (TPicklistEntry objDepPLE : objDS_Entries){
            List<Integer> bitValues = ConvertB64ToInts(objDepPLE.validFor);
            
            Integer bitMask = 32; // Ignore highest 2 bits
            Integer intIndex = 0;
            Integer bitIdx = 0;
            for (Integer numBits = bitValues.size() * 6; numBits > 0; --numBits) {
                Integer bits = bitValues[intIndex];
                if ((bits & bitMask) == bitMask) {
                    mapResults.get( ctrl_ple[bitIdx].getLabel() ).add( objDepPLE.label );
                }
                bitMask = bitMask >>> 1;
                ++bitIdx;
                
                if (bitMask == 0) {
                    bitMask = 32;
                    intIndex = intIndex + 1;
                }
            }
        }
        return mapResults;
    }
    
    private class TPicklistEntry{
        //public string active {get;set;}
        //public string defaultValue {get;set;}
        public string label {get;set;}
        //public string value {get;set;}
        public string validFor {get;set;}
        //public TPicklistEntry(){}
        
    }
    
    public Map<String,List<String>> addUpdatePicklistValues(Map<String,List<String>> tempMap,String controllingField, String DependentField){
        if(tempMap.containsKey(controllingField)){
            tempMap.get(controllingField).add(DependentField);
        }
        else{
            tempMap.put(controllingField,new List<String>{DependentField});
        }
        return tempMap;
    }
    
    /**
* getDependentFieldMetadataMap method called from the validateDependentFields method
* @params: Object Name, Field Dependency Information metadata type(list), record type developer name 
* @rtnval: Map of Controlling picklist values and respective Dependent picklist values
**/    
    
    public Map<String,List<String>> getDependentFieldMetadataMap(String objectName, List<Field_Dependency_Information__mdt> dependentFieldMetadataList, String recordTypeDevName){
        //Record Type -->(Controlling field --> [Dependent picklists])
        Map<String,List<String>> caseMetadataMap = new Map<String,List<String>>();
        
        for(Field_Dependency_Information__mdt obj : dependentFieldMetadataList){
            if(obj.Record_Type_Name__c == recordTypeDevName){
                caseMetadataMap.putAll(addUpdatePicklistValues(
                    caseMetadataMap,
                    obj.Type__c == 'Master Controller Picklist' ? obj.Type__c : obj.Controlling_Field__c,//The master controller picklist does not have a controlling field
                    obj.Dependent_Field__c));
            }
            
        }
        return caseMetadataMap;
    }
    
    public Map<String,String> getFieldLabelMap(String objectName, List<Field_Dependency_Information__mdt> dependentFieldMetadataList){
        //Field API --> Field Label
        Map<String,String> fieldLabelMap = new Map<String,String>();
        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);
        Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();
        List<String> dependentPicklistAPIList = new List<String>();
        for(Field_Dependency_Information__mdt obj : dependentFieldMetadataList){
            dependentPicklistAPIList.add(obj.Dependent_Field__c);
        }
        for(String api : dependentPicklistAPIList){
            fieldLabelMap.put(api,objFieldMap.get(api).getDescribe().getLabel());
        }
        return fieldLabelMap;
    }
    
    /**
* validateComplaintCaseCloseForm method called from the clsCaseTriggerHandler (Before Insert/Update context)
* @params: Standard trigger list of objects 
* @rtnval: Map of case Id and respective validation error messages (if any)
**/ 
    public Map<Id,String> validateDependentFields(String objectName, String recordTypeDevName, List<SObject> triggerValues){
        Map<Id,String> caseErrorMap = new Map<Id,String>();
        List<String> missingFields = new List<String>();
        List<Case> caseTriggerValues = new List<Case>();
        
        //Add checks and processing for more objects when introduced
        if(objectName == 'Case'){
            for(SObject sobj : triggerValues){
                Case placeholderObject = (Case) sobj;
                caseTriggerValues.add(placeholderObject);
            }
        }
        if(dependencyMetadataPerObjectMap.containsKey(objectName)){    
            //Field API --> Field Label
            Map<String,String> fieldLabelMap = new Map<String,String>();
            fieldLabelMap = getFieldLabelMap(objectName,dependencyMetadataPerObjectMap.get(objectName));
            //Record Type -->(Controlling field --> [Dependent picklists])
            Map<String,List<String>> controllingAndDependentPicklistMap = new Map<String,List<String>>(); 
            controllingAndDependentPicklistMap = getDependentFieldMetadataMap(objectName, dependencyMetadataPerObjectMap.get(objectName), recordTypeDevName); //Refine the map to fetch only current record type related information
            
            Map<String,Map<String,List<String>>> dependentAndDependentValueMap = new Map<String,Map<String,List<String>>>(); //Dependent field API --> (Control value --> Dependent value List)
            
            for(String controllingField : controllingAndDependentPicklistMap.keySet()){
                if(controllingAndDependentPicklistMap.get(controllingField).size()>0){
                    for(String dependentField : controllingAndDependentPicklistMap.get(controllingField)){
                        if(controllingField != 'Master Controller Picklist'){
                            dependentAndDependentValueMap.put(dependentField,GetDependentOptions(objectName,controllingField,dependentField));
                        }
                    }
                }
            }
            
            for(Case obj : caseTriggerValues){
                missingFields.clear();
                for(String controllingField : controllingAndDependentPicklistMap.keySet()){
                    if(controllingField == 'Master Controller Picklist'){
                        String initiatingFieldAPI = controllingAndDependentPicklistMap.get(controllingField).get(0);
                        if(obj.get(initiatingFieldAPI) == NULL){
                            missingFields.add(fieldLabelMap.get(initiatingFieldAPI));
                        }
                    }
                    else if(obj.get(controllingField) != NULL && controllingAndDependentPicklistMap.get(controllingField).size()>0){
                        String controlValue = String.valueOf(obj.get(controllingField));
                        for(String dependentField : controllingAndDependentPicklistMap.get(controllingField)){
                            if(dependentAndDependentValueMap.get(dependentField).containsKey(controlValue) && dependentAndDependentValueMap.get(dependentField).get(controlValue).size()>0 && obj.get(dependentField) == NULL){
                                missingFields.add(fieldLabelMap.get(dependentField));
                            }
                        }
                    }
                }
                if(!missingFields.isEmpty()){
                    caseErrorMap.put(obj.Id,returnVal+String.join(missingFields, ', '));
                }
                else{
                    caseErrorMap.put(obj.Id,null);
                }
            }
        }
        return caseErrorMap;
    }
}