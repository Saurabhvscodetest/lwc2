@isTest
public with sharing class CaseTestDataFactory {

	public static Case newCase = new Case();

	public static Case createCase(Id contactId) {

	  Case newCase = new Case();
	  newCase.ContactId = contactId;
	  return newCase;

	}

	public static Case createQueryCase(Id contactId){

		Id recordTypeId = CommonStaticUtils.getRecordTypeId('Case', 'Query');

		newCase = createCase(contactId);
		newCase.Description = 'Test Query Case Created';
		newCase.jl_Branch_master__c = 'Aberdeen';
		newCase.Contact_Reason__c = 'Partner/Agent Behaviour';
		newCase.Reason_Detail__c = 'Appreciation';
		newCase.Assign_To__c = 'Hamilton/Didsbury - CST';
		newCase.RecordTypeId = recordTypeId;
		newCase.jl_Action_Taken__c = 'New Case Created';
		newCase.status = 'New';

		return newCase;
	}

	public static Case createComplaintCase(Id contactId){

		Id recordTypeId = CommonStaticUtils.getRecordTypeId('Case', 'Complaint');

		newCase = createCase(contactId);
		newCase.Description = 'Test Complaint Case Created';
		newCase.jl_Branch_master__c = 'Aberdeen';
		newCase.Contact_Reason__c = 'Partner/Agent Behaviour';
		newCase.Reason_Detail__c = 'Partner/Agent Behaviour Failure';
		newCase.Assign_To__c = 'Hamilton - CRD';
		newCase.RecordTypeId = recordTypeId;
		newCase.jl_Action_Taken__c = 'New Case Created';
		newCase.status = 'New';

		return newCase;
	}

	public static Case createPSECase(Id contactId){

		Id recordTypeId = CommonStaticUtils.getRecordTypeId('Case', 'Product Stock Enquiry');

		newCase = createCase(contactId);
		newCase.Description = 'Test PSE Case Created';
		newCase.Assign_To_New_Branch__c = 'Aberdeen';
		newCase.RecordTypeId = recordTypeId;

		return newCase;
	}
	
	public static Case createNKUCase(Id contactId){

		Id recordTypeId = CommonStaticUtils.getRecordTypeId('Case', 'NKU');

		newCase = createCase(contactId);
		newCase.Description = 'Test NKU Case Created';
		newCase.jl_Branch_master__c = 'Aberdeen';
		newCase.Contact_Reason__c = 'Partner/Agent Behaviour';
		newCase.Reason_Detail__c = 'Appreciation';
		newCase.Assign_To__c = 'Hamilton/Didsbury - CST';
		newCase.RecordTypeId = recordTypeId;
		newCase.jl_Action_Taken__c = 'New Case Created';
		newCase.status = 'New';

		return newCase;
	}
}