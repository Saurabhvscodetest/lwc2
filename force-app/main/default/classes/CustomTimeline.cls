public class CustomTimeline {
    @AuraEnabled
    public Case_Activity__c promise {get;set;} { promise = new Case_Activity__c(); }
    @AuraEnabled
    public String status {get;set;} { status = ''; } //Possible values : Active, Pending, Completed, Overdue (for Active, Future categories), Failed (only for Completed category), Cancelled
    @AuraEnabled
    public String category {get;set;} { category = ''; } //Possible values : Active & Completed 
    @AuraEnabled
    public String recordType {get;set;} { recordType=''; } //Possible values: Customer Promise & Internal Action
    @AuraEnabled
    public List<String> resolutionOptions {get;set;} { resolutionOptions = new List<String>(); } 
    @AuraEnabled
    public Boolean isActive {get;set;} { isActive = false; }
    @AuraEnabled
    public Boolean isLast {get;set;} { isLast = false; }
    
    public CustomTimeline(Case_Activity__c caseActivity,Map<String,List<String>> customerPromiseResolutionMap){
        this.promise = caseActivity;
        this.category = calculateCategory(caseActivity.Activity_Completed_Date_Time__c,caseActivity.Activity_Start_Date_Time__c);
        this.status = calculateStatus(caseActivity.Resolution_Method__c, this.category, caseActivity.Activity_Completed_Date_Time__c, caseActivity.Activity_End_Date_Time__c);
        this.recordType = Schema.getGlobalDescribe().get('Case_Activity__c').getDescribe().getRecordTypeInfosById().get(caseActivity.RecordTypeId).getName();
        this.resolutionOptions = customerPromiseResolutionMap.containsKey(this.recordType) ? customerPromiseResolutionMap.get(this.recordType) : new List<String>();
    }
    
    public static String calculateCategory(DateTime completedDateTime, DateTime startDateTime){
        String category = 'Active';//Default value
        if(completedDateTime != NULL){
            category = 'Completed';
        }
        else{
            category = 'Active';
        }
        return category;
    }
    
    public static String calculateStatus(String resolutionOption, String category, DateTime completedDateTime, DateTime endDateTime){
        String status = 'Active';//Default value
        if(category == 'Completed'){
            if(resolutionOption == 'No action required'){
                status = 'Cancelled';
            }
            else if(completedDateTime > endDateTime){
                status = 'Failed';
            }
            else{
                status = 'Completed';
            }
        }
        else{
            status = (System.now() > endDateTime) ? 'Overdue' : 'Active';
        }
        return status;
    }
}