/**
 * Unit tests for BatchInteractionCloseCorrection class methods.
 */
@isTest
private class BatchInteractionCloseCorrection_TEST {

	/**
	 * Code coverage for BatchInteractionCloseCorrection.start
	 */
	static testMethod void testStartMethod() {
		system.debug('TEST START: testStartMethod');

		initialiseSettings();
		Account testAccount = UnitTestDataFactory.createAccount(1, true);
		Contact testCon = UnitTestDataFactory.createContact(1, testAccount.Id, true);
		Case testCase = UnitTestDataFactory.createQueryCase(testCon.Id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, true);
		List<Interaction__c> intList = [SELECT Id, Name FROM Interaction__c WHERE Case__c = :testCase.Id];
		system.assertEquals(3, intList.size()); // Should be a Case, Contact and Agent interaction.

		Database.BatchableContext bc = null;
		BatchInteractionCloseCorrection intCloseCorrectionBatch = new BatchInteractionCloseCorrection();
		Test.startTest();
		Database.QueryLocator dql = intCloseCorrectionBatch.start(bc);	
		Test.stopTest();

		system.debug('TEST END: testStartMethod');
	}

	/**
	 * Test BatchInteractionCloseCorrection.execute
	 */
	static testMethod void testExecuteMethod() {
		system.debug('TEST START: testExecuteMethod');

		initialiseSettings();
		User testUser = [SELECT Id FROM User WHERE IsActive = true AND Id != :UserInfo.getUserId() LIMIT 1];
		Account testAccount = UnitTestDataFactory.createAccount(1, true);
		Contact testCon = UnitTestDataFactory.createContact(1, testAccount.Id, true);
		Case testCase = UnitTestDataFactory.createQueryCase(testCon.Id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, true);
		List<Interaction__c> intList = [SELECT Id, Name FROM Interaction__c WHERE Case__c = :testCase.Id];
		system.assertEquals(3, intList.size()); // Should be a Case, Contact and Agent interaction.
		
		Interaction__c int1 = [SELECT Id, Name, Event_Type__c, Case__c, Start_DT__c, End_DT__c
							   FROM Interaction__c
							   WHERE Case__c = :testCase.Id AND Event_Type__c IN :BatchInteractionCloseCorrection.CASE_OWNERSHIP_TYPES];
		int1.Start_DT__c = system.now().addDays(-5);
		int1.End_DT__c = system.now();
		int1.Closing_Agent__c = testUser.Id;
		update int1;
		Test.setCreatedDate(int1.Id, int1.Start_DT__c);
		Interaction__c int2 = new Interaction__c(Event_Type__c = 'Agent', Case__c = testCase.Id, Start_DT__c = system.now().addDays(-4), End_DT__c = system.now(), Closing_Agent__c = testUser.Id);
		insert int2;
		Test.setCreatedDate(int2.Id, int2.Start_DT__c);
		Interaction__c int3 = new Interaction__c(Event_Type__c = 'Queue', Case__c = testCase.Id, Start_DT__c = system.now().addDays(-3), End_DT__c = system.now(), Closing_Agent__c = testUser.Id);
		insert int3;
		Test.setCreatedDate(int3.Id, int3.Start_DT__c);
		Interaction__c int4 = new Interaction__c(Event_Type__c = 'Agent', Case__c = testCase.Id, Start_DT__c = system.now().addDays(-2));
		insert int4;
		Test.setCreatedDate(int4.Id, int4.Start_DT__c);

		Database.BatchableContext bc = null;
		BatchInteractionCloseCorrection intCloseCorrectionBatch = new BatchInteractionCloseCorrection();
		
		List<Interaction__c> intsToUpdate = [SELECT Id, Name, Case__c
											 FROM Interaction__c
											 WHERE Case__c = :testCase.Id
											 AND Event_Type__c IN :BatchInteractionCloseCorrection.CASE_OWNERSHIP_TYPES];

		system.assertEquals(4, intsToUpdate.size());
		
		test.startTest();
		intCloseCorrectionBatch.execute(bc, intsToUpdate);
		test.stopTest();
		
		List<Interaction__c> intsToCheck = [SELECT Id, Name, Case__c, Start_DT__c, End_DT__c, Closing_Agent__c
											FROM Interaction__c
											WHERE Case__c = :testCase.Id
											AND Event_Type__c IN :BatchInteractionCloseCorrection.CASE_OWNERSHIP_TYPES
											AND Closing_Agent__c != null];

		system.assertEquals(3, intsToCheck.size());
		
		for (Interaction__c interaction : intsToCheck) {
			system.assertNotEquals(testUser.Id, interaction.Closing_Agent__c);
			if (interaction.Id == int1.Id) {
				system.assertNotEquals(int1.End_DT__c, interaction.End_DT__c);
			} else if (interaction.Id == int2.Id) {
				system.assertNotEquals(int2.End_DT__c, interaction.End_DT__c);
			} else if (interaction.Id == int3.Id) {
				system.assertNotEquals(int3.End_DT__c, interaction.End_DT__c);
			} else {
				system.assert(false);
			}
		}

		system.debug('TEST END: testExecuteMethod');
	}
    
	/**
	 * Code coverage for BatchInteractionCloseCorrection.finish()
	 */
	static testMethod void testFinishMethod() {
		system.debug('TEST START: testFinishMethod');

		Config_Settings__c defaultNotificationEmail = new Config_Settings__c(Name = 'EMAIL_NOTIFICATION_DEFAULT_ADDRESS', Text_Value__c = 'Matthew.Povey@johnlewis.co.uk');
		insert defaultNotificationEmail;
		Database.BatchableContext bc = null;
		BatchInteractionCloseCorrection intCloseCorrectionBatch = new BatchInteractionCloseCorrection();
		Test.startTest();
		intCloseCorrectionBatch.finish(bc);
		Test.stopTest();

		system.debug('TEST END: testFinishMethod');
	}

	/**
	 * Custom setting initialisation
	 */
	private static void initialiseSettings() {
		UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
		DateTime startDateTime = system.now().addDays(-5);
		DateTime endDateTime = system.now().addDays(1);
		String startDateString = startDateTime.year() + '-' + startDateTime.month() + '-' + startDateTime.day() + ' 00:00:00';
		String endDateString = endDateTime.year() + '-' + endDateTime.month() + '-' + endDateTime.day() + ' 23:00:00';
		Config_Settings__c startSetting = new Config_Settings__c(Name = 'INTERACTION_CORRECTION_START_DATE', Text_Value__c = startDateString);
		Config_Settings__c endSetting = new Config_Settings__c(Name = 'INTERACTION_CORRECTION_END_DATE', Text_Value__c = endDateString);
		List<Config_Settings__c> configList = new List<Config_Settings__c> {startSetting, endSetting};
		insert configList;
	}
}