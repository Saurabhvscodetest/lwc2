/*
* @File Name   : GVFWebServiceUtils
* @Description : Webservice Utility class created for invoking all the integration related services
* @Copyright   : Zensar
* @Jira #      : #4277
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0       22-May-19        Ragesh G                     Created
2.0       20-May-19        Vignesh Kotha                Modified
*/

Public class GVFWebServiceUtils {
    
    /**
*@purpose : Method used for checking the connection between Connex and ESB
*@param   : Void 
*@return  : String
**/
    static final Date CURRENT_DATE = Date.today();
    static final Date START_DATE = Date.today().addYears(-10); 
    static final String Username;
    static final String Password;
    static final string Company;
    public static final Datetime CURRENT_DATE_TIME = Datetime.now(); 
    static{
        Callout_GVF_Credentials__mdt[] calloutSettings = [SELECT UserName__c,Password__c,DeveloperName,Company_Name__c FROM Callout_GVF_Credentials__mdt where DeveloperName ='production'];
        for(Callout_GVF_Credentials__mdt settings : calloutSettings){
            Username = settings.UserName__c;
            Password = settings.Password__c;
            Company = settings.Company_Name__c;
        }
    }
    
    /**
*@purpose : Method used for parsing the response coming from the external system
*@param   : Void 
*@return  : List < String >
**/
    
    public static List < String > parsePictureValuesFromResponse ( String reponseXML ) {
        List < String > signaturesPictureValueList = new List < String > ();
        System.debug('reponseXMLreponseXML'+reponseXML);
        // Remove the below code which uses the static resource to parse the string with reponseXML - variable coming through as parameter
        // StaticResource srObject = [select id,body from StaticResource Where Name = 'XML_Request'];
        //String xml= srObject.toString(); // use the reponseXML string here after connection is successful
        Dom.Document doc = new Dom.Document();
        doc.load(reponseXML);            
        //Dom.XMLNode root = doc.getRootElement();
        Dom.XMLNode commandElement= doc.getRootElement();
        //Dom.XMLNode commandElement = RouteRespStringElement.getChildElement('Command', null);
        Dom.XMLNode executeElement = commandElement.getChildElement('Execute', null);
        //System.debug ( ' executeElement*** ' + executeElement);
        Dom.XMLNode docResponseNode = executeElement.getChildElement('Status', null).getChildElement('DocResponse', null).getChildElement('STADMessage', null ).getChildElement('DocMasterBOL', null).getChildElement ('DocBOL', null ).getChildElement('DocStop', null);
        System.debug ( ' Child Element *** ' + docResponseNode.getChildren());  
        for(Dom.XMLNode child : docResponseNode.getChildren()) { 
            System.debug ('child.getName () ' + child.getName ()); 
            if ( child.getName () == 'DocRouteHist' ) {
                Dom.XMLNode pictureElement = child.getChildElement('DocFieldDataHist',null);
                System.debug ( ' Picture Element' + pictureElement );
                if ( pictureElement != null) {         
                    String pictureValue = pictureElement.getAttributeValue('PictureValue',null);
                    System.debug ( ' Picture Value ' + pictureValue );
                    if ( pictureValue != null ) {
                        // pictureValue = pictureValue.toUpperCase();
                        signaturesPictureValueList.add (pictureValue.replaceAll('/[^A-Fa-f0-9]/g', ''));
                    }                    
                }             
            }             
            if ( child.getName () == 'DocOrderLine' ) {
                for(Dom.XMLNode childdocfield : child.getchildren()) {  
                    if(childdocfield.getName()=='DocRouteHist'){
                        for(Dom.XMLNode docfldhis : childdocfield.getchildren()){
                            if(docfldhis.getName() == 'DocFieldDataHist'){
                                String pictureValueDocOrderValue = docfldhis.getAttributeValue('PictureValue',null);
                                signaturesPictureValueList.add (pictureValueDocOrderValue);                                
                            }
                        }
                    }                 
                }
            }
        }
        return signaturesPictureValueList;     
    }
    
    /**
*@purpose : Method used for buliding the XML Request so that it can be used to make the POST request.
*@param   : Void 
*@return  : List < String >
**/
    
    public static String buildXMLImageRequest ( String orderKey ) { 
        Xmlstreamwriter xmlW = new Xmlstreamwriter();
        xmlW.writeStartElement(null,'DocFWImport', null);
        xmlW.writeStartElement(null,'Header', null);
        xmlW.writeAttribute(null,null,'senderID','CUSTOMER');
        xmlW.writeAttribute(null,null,'ReceiverID','DESCARTES');
        xmlW.writeAttribute(null,null,'SendDateTime',string.valueOf(CURRENT_DATE_TIME));
        xmlW.writeAttribute(null,null,'extDocControlID','1');
        xmlW.writeAttribute(null,null,'CompanyName',Company);
        xmlW.writeAttribute(null,null,'LoginName',Username);
        xmlW.writeAttribute(null,null,'Password',Password);
        xmlW.writeEndElement(); //Close XML document
        xmlW.writeStartElement(null,'Request', null);
        xmlW.writeStartElement(null,'DocRequestTask', null);
        xmlW.writeAttribute(null,null,'ScheduleKey','');
        xmlW.writeAttribute(null,null,'MessagePurpose','1005');
        xmlW.writeAttribute(null,null,'ProcessCode','26');        
        xmlW.writeStartElement(null,'DocCriteria', null);        
        xmlW.writeStartElement(null,'DocMasterBOL', null);        
        xmlW.writeAttribute(null,null,'OrderKey', orderKey );
        xmlW.writeAttribute(null,null,'LocationKey','');        
        xmlW.writeAttribute(null,null,'StartDate',string.valueOf(START_DATE));
        xmlW.writeAttribute(null,null,'EndDate',string.valueOf(CURRENT_DATE));
        xmlW.writeAttribute(null,null,'ReturnImage','1');
        xmlW.writeAttribute(null,null,'SkipRouteHist','0');        
        xmlW.writeEndElement(); // End DocMasterBOL        
        xmlW.writeEndElement(); // End DocCriteria        
        xmlW.writeEndElement(); // End DocRequestTask
        xmlW.writeEndElement();// End Request   
        xmlW.writeEndElement(); //Close DocFWImport
        String xmlStringxmlRes = xmlW.getXmlString();
        System.debug('The XML :'+xmlW.getXmlString());  
        return xmlStringxmlRes;      
    }
    public static String buildXMLImageRequestDelivery( String deliverykey ) {    
        Xmlstreamwriter xmlW = new Xmlstreamwriter();
        xmlW.writeStartElement(null,'DocFWImport', null);
        xmlW.writeStartElement(null,'Header', null);
        xmlW.writeAttribute(null,null,'senderID','CUSTOMER');
        xmlW.writeAttribute(null,null,'ReceiverID','DESCARTES');
        xmlW.writeAttribute(null,null,'SendDateTime',string.valueOf(CURRENT_DATE_TIME));
        xmlW.writeAttribute(null,null,'extDocControlID','1');
        xmlW.writeAttribute(null,null,'CompanyName',Company);
        xmlW.writeAttribute(null,null,'LoginName',Username);
        xmlW.writeAttribute(null,null,'Password',Password);
        xmlW.writeEndElement(); //Close XML document
        xmlW.writeStartElement(null,'Request', null);
        xmlW.writeStartElement(null,'DocRequestTask', null);
        xmlW.writeAttribute(null,null,'ScheduleKey','');
        xmlW.writeAttribute(null,null,'MessagePurpose','1005');
        xmlW.writeAttribute(null,null,'ProcessCode','26');        
        xmlW.writeStartElement(null,'DocCriteria', null);        
        xmlW.writeStartElement(null,'DocMasterBOL', null);        
        xmlW.writeAttribute(null,null,'OrderKey', '' );
        xmlW.writeAttribute(null,null,'LocationKey',deliverykey);
        xmlW.writeAttribute(null,null,'StartDate',string.valueOf(START_DATE));
        xmlW.writeAttribute(null,null,'EndDate',string.valueOf(CURRENT_DATE));
        xmlW.writeAttribute(null,null,'ReturnImage','1');
        xmlW.writeAttribute(null,null,'SkipRouteHist','0');        
        xmlW.writeEndElement(); // End DocMasterBOL        
        xmlW.writeEndElement(); // End DocCriteria        
        xmlW.writeEndElement(); // End DocRequestTask
        xmlW.writeEndElement();// End Request   
        xmlW.writeEndElement(); //Close DocFWImport
        String xmlStringxmlRes = xmlW.getXmlString();
        System.debug('The XML deli:'+xmlW.getXmlString());  
        return xmlStringxmlRes;      
    }
}