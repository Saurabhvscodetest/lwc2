public with sharing class HouseKeeperBatchController {

    public static final String DEFAULT_BATCH_NAME = 'HouseKeeperBatch';
    public String batchName {get; set;}

    public PageReference stop() {
        if (jobTriggers.size() == 1) {
            system.abortJob(jobTriggers[0].Id);
        } 
        running = false;
        return null;
    }


    public PageReference start() {
        system.schedule(batchName, '0 0 1 * * ?', new HouseKeeperBatch());
        running = true;
        return null;  
    }

    public List<House_Keeping_Config__c> jobs { get {
        if (null == jobs) {
            jobs = [SELECT Id, Name, Order__c, Run_Frequency_Days__c, Last_Run_Time__c FROM House_Keeping_Config__c ORDER BY Order__c];
        }
        return jobs;
    } set; }

    public Boolean running { get {
        return (status == STATUS_ON);
    } set; }

    public static final String STATUS_ON = 'Running';
    public static final String STATUS_OFF = 'Stopped';

    public String status { get {
        if (jobTriggers.size() > 0) {
            status = STATUS_ON;            
        } else { 
            status = STATUS_OFF;
        }
        return status;
    } private set; }
    
    public List<CronTrigger> jobTriggers {
        get {
            List<CronTrigger> jobTriggers = new List<CronTrigger>();
            
            List<CronJobDetail> jobDetails = [Select Name, JobType, Id From CronJobDetail c WHERE Name = :batchName];
            if (jobDetails.Size() == 1) {
                jobTriggers = [SELECT Id, StartTime, EndTime, CronExpression, TimesTriggered, State, PreviousFireTime FROM CronTrigger WHERE CronJobDetailId=:jobDetails[0].Id]; 
            }
            return jobTriggers;     
        }
        private set;
    }

    public List<MyJL_Settings__c> myjlSettings {
        get {
            if (null == myjlSettings) {
                myJLSettings = [SELECT Name, Delete_Transactions_older_than_n_Days__c FROM MyJL_Settings__c];
            }
            return myjlSettings;
        } private set;
    }
    
    public List<Log_Configuration__c> logConfigs {
        get {
            if (null == logConfigs) {
                logConfigs = [SELECT Name, Enabled__c, Delete_After_N_Days__c FROM Log_Configuration__c];
            }
            return logConfigs;
        } private set;
    }
    
    public HouseKeeperBatchController () {
        batchName = DEFAULT_BATCH_NAME;
        //status = STATUS_OFF;
        running = (status == STATUS_ON);
    }
}