@isTest
private class Sharing_TEST {
    
    @testSetup static void setup() {
        
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();

        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        List<User> users = new List<User>();

        System.runAs(runningUser) {

            users.add(UnitTestDataFactory.createGenericUser('user1', 'JL: Case Profile', 'JLP:Branch:ABERDEEN:CustomerSupport:T2', 'Branch - CST Aberdeen'));
            users.add(UnitTestDataFactory.createGenericUser('user2', 'JL: Case Profile', 'JLP:Branch:ABERDEEN:CustomerSupport:T2', 'Branch - CST Aberdeen'));
            insert users;

            Contact testContact = UnitTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;

        }
    }

    // Test Internal Comments blows up sharing in a private sharing model
    @IsTest static void testInternalCommentBreaksSharing() {

        Organization org = [Select Id, DefaultCaseAccess from Organization];
        String strCassAccess = org.DefaultCaseAccess;
        Case testCase;

        User u1 = [SELECT Id FROM User WHERE FirstName = 'user1' LIMIT 1];
        User u2 = [SELECT Id FROM User WHERE FirstName = 'user2' LIMIT 1];
        Contact testContact = [SELECT Id FROM Contact WHERE Email = 'stuart@little.com'];
        String ps1 = 'JL_MYJL_Phase_3_Contact_Center_Permissions';
        UserTestDataFactory.assignPermissionSetToUser(u1.id,ps1);
        Test.startTest();
                
        System.runAs (u1) {
            // Create a Case - assign it to a queue
            testCase = UnitTestDataFactory.createLimitedCase(testContact);
            testCase.jl_Access__c = 'Limited';
            testCase.jl_Branch_master__c = 'Newbury';
            testCase.jl_Query_type__c = 'Appreciation';
            testCase.jl_Query_type_Detail__c = 'Appreciation';
            testCase.jl_Assign_query_to_queue__c = 'Hamilton/Didsbury - CST';
            testCase.jl_Transfer_case__c = true;
            insert testCase;
                
            CaseComment cc = new CaseComment(parentid = testCase.Id);
            cc.CommentBody =  'This comment will be ok';
            insert cc;
        }        

        System.runAs (u2) {
            try {
                CaseComment cc = new CaseComment(parentid = testCase.Id);
                cc.CommentBody =  'This comment will blow up';
                insert cc;
                
                // if we reach here there has been no exception thrown which is wrong if the sharing model is private

                // check that access to Cases is private (otherwise nothing to test)
                if (strCassAccess == 'None') {
                    System.assertEquals(0,1);
                }
            } catch (Exception e) {
                System.debug('Exception captured');
                System.assert(e.getMessage().contains('INSUFFICIENT_ACCESS_ON_CROSS_REFERENCE_ENTITY'));
            }
        }        

        Test.stopTest();
    }
    

    @IsTest static void testLimitedAccessRead(){
        
        Organization org = [Select Id, DefaultCaseAccess from Organization];
        String strCassAccess = org.DefaultCaseAccess;
        Case testCase;

        User u1 = [SELECT Id FROM User WHERE FirstName = 'user1' LIMIT 1];
        User u2 = [SELECT Id FROM User WHERE FirstName = 'user2' LIMIT 1];
        Contact testContact = [SELECT Id FROM Contact WHERE Email = 'stuart@little.com'];
        String ps1 = 'JL_MYJL_Phase_3_Contact_Center_Permissions';
        UserTestDataFactory.assignPermissionSetToUser(u1.id,ps1);        
        Test.startTest();
        
        System.runAs (u1) {
            // Create a Case - assign it to a queue
            testCase = UnitTestDataFactory.createLimitedCase(testContact);
            testCase.jl_Access__c = 'Limited';
            testCase.jl_Branch_master__c = 'Newbury';
            testCase.jl_Query_type__c = 'Appreciation';
            testCase.jl_Query_type_Detail__c = 'Appreciation';
            testCase.jl_Assign_query_to_queue__c = 'Hamilton/Didsbury - CST';
            testCase.jl_Transfer_case__c = true;
            insert testCase;
            
            // check the case is visible to the creating user
            List<Case> caseList = [select Ownerid from Case where id = :testCase.Id];
            System.assertEquals(1,caseList.size());

            String owneridstring = (String) caseList[0].ownerid;
            
            System.assertEquals('00G',owneridstring.substring(0,3));
        }        

        // First check that access to Cases is private (otherwise nothing to test
        System.runAs (u2) {
            Integer i = [select count() from Case where Id = :testCase.Id];
            if (strCassAccess != 'None') {
                // in a public sharing model we should be able to see the case
                System.assertEquals(1,i);
            } else {           
                // in a private sharing model we should not be able to see the case
                System.assertEquals(0,i);
            }     
        } 

        Test.stopTest();    
    }
    
   // Test for the manualShareRead method
   @IsTest static void testManualShare() {

        Case testCase;
        User u1 = [SELECT Id FROM User WHERE FirstName = 'user1' LIMIT 1];
        User u2 = [SELECT Id FROM User WHERE FirstName = 'user2' LIMIT 1];
        Contact testContact = [SELECT Id FROM Contact WHERE Email = 'stuart@little.com'];

        testCase = UnitTestDataFactory.createNormalCase(testContact);
        testCase.OwnerId = u1.Id;
        insert testCase;
   
        Test.startTest();

        // Insert manual share for user who is not record owner.
        CaseShare csRec = Sharing.createManualCaseShare(testCase.Id, u2.Id, Sharing.EDIT_ACCESS_LEVEL, true);
        System.assertNotEquals(null, csRec.Id);
   
        // Query job sharing records.
        List<CaseShare> jShrs = [SELECT Id, UserOrGroupId, CaseAccessLevel, RowCause FROM CaseShare WHERE CaseId = :testCase.Id AND UserOrGroupId= :u2.Id];
  
        // Test for only one manual share on job.
        System.assertEquals(jShrs.size(), 1, 'Set the object\'s sharing model to Private.');
          
        // Test attributes of manual share.
        System.assertEquals(jShrs[0].CaseAccessLevel, 'Edit');
        System.assertEquals(jShrs[0].RowCause, 'Manual');
        System.assertEquals(jShrs[0].UserOrGroupId, u2.Id);

        Test.stopTest();
   } 
}