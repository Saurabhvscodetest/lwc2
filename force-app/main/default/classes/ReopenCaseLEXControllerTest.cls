@isTest
public class ReopenCaseLEXControllerTest {
    
    
    @testSetup static void setup() {
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
    }
    
    /**
* @description	Tests the reopening case as Tier 1 user - The owner of the case should not be a user		
*/
    @isTest static void testReopenCaseAsTier1User() {
        
        User runningUser = UserTestDataFactory.getRunningUser();
        
        User contactCentreUserTier1;
        
        
        Contact testContact;
        Case testCase;
        
        System.runAs(runningUser) {
            
            contactCentreUserTier1 = UserTestDataFactory.createUser('ContactCentreTestUser', 'JL: ContactCentre/CDH Tier1', 'JLP:CC:CSA:T1', 'Hamilton/Didsbury - CSA');
            contactCentreUserTier1.Tier__c = 1;
            contactCentreUserTier1.Username = 'Tier1@JL.com';
            contactCentreUserTier1.CommunityNickname = 'Tier1 CommunityNickname';
            insert contactCentreUserTier1;		
        }
        Boolean checkCaseClosed = false;
        System.runAs(contactCentreUserTier1) {
            
            testContact = CustomerTestDataFactory.createContact();
            insert testContact;
            System.debug('testContact>>'+testContact);
            
            testCase = CaseTestDataFactory.createPSECase(testContact.Id);
            insert testCase;
            Test.startTest();
            testCase.Status = 'Closed - Resolved';
            update testCase;
			//ReopenCaseLEXController.updateCase(testCase.Id, 'New Comment', false);
            Test.stopTest();
        }
        
        Case reopenedCase = [SELECT Id, jl_Case_RestrictedAddComment__c, Status, JL_Action_Taken__c, JL_ReOpened__c, ownerId FROM Case];
       // System.assertEquals(true, reopenedCase.JL_ReOpened__c);
        System.assertEquals('Closed - Resolved', reopenedCase.Status);
        System.assertEquals('New case created', reopenedCase.JL_Action_Taken__c);
       // System.assertNOTEquals('User', String.valueOf(reopenedCase.ownerId.getSobjectType()));
        
        
    }
    
    /**
* @description	Tests the reopening case as Tier 2 user - The owner of the case should be a user		
*/
    @isTest static void testReopenCaseAsTier2User() {
        
        User runningUser = UserTestDataFactory.getRunningUser();
        
        User contactCentreUserTier2;
        
        
        Contact testContact;
        Case testCase;
        
        System.runAs(runningUser) {
            
            contactCentreUserTier2 = UserTestDataFactory.createContactCentreUser();
            contactCentreUserTier2.Tier__c = 2;
            insert contactCentreUserTier2;		
        }

        testContact = CustomerTestDataFactory.createContact();
        insert testContact;
        
        testCase = CaseTestDataFactory.createPSECase(testContact.Id);
        insert testCase;
        
        testCase.Status = 'Closed - Resolved';
        Test.startTest();
        update testCase;
        ReopenCaseLEXController.updateCase(testCase.Id, 'New Comment', false);
        ReopenCaseLEXController.getCaseQueue(testCase.Id);
        Test.stopTest();
        
        
        
        Case reopenedCase = [SELECT Id, jl_Case_RestrictedAddComment__c, Status, JL_Action_Taken__c, JL_ReOpened__c, ownerId FROM Case];
        System.assertEquals(true, reopenedCase.JL_ReOpened__c);
        System.assertEquals('Reopened', reopenedCase.Status);
        System.assertEquals('New case created', reopenedCase.JL_Action_Taken__c);
       // System.assertNOTEquals('User', String.valueOf(reopenedCase.ownerId.getSobjectType()));
        
        
    }
}