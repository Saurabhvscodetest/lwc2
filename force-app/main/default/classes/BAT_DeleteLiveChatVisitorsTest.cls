/* Description  : Delete LiveChat Visitor records which don't have Livechat Transcript records associated with it.
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   12/02/2019        Vignesh Kotha                   Created                COPT-4425  
*
*/
@isTest
Public class BAT_DeleteLiveChatVisitorsTest{
    static testmethod void testDeleteLiveChatVisitors(){        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        // Running user Context
        System.runAs(runningUser){
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;       
            insert CustomSettingTestDataFactory.createConfigSettings();
        }        
        System.runAs(testUser){            
            contact testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'vignesh@gmail.com';
            insert testContact;            
            case testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            insert testCase;            
            LiveChatVisitor testvisitor=new LiveChatVisitor();
            testvisitor.CreatedDate=System.today();
            insert testvisitor;  
            LiveChatTranscript testTranscript = new LiveChatTranscript();
            testTranscript.CreatedDate=System.today();
            testTranscript.LiveChatVisitorId = testvisitor.id;
            insert testTranscript;
            Test.startTest();
            List<LiveChatVisitor> livechatVisitorlist=new  List<LiveChatVisitor>();
            for(integer i=0; i<99; i++){
                livechatVisitorlist.add(new LiveChatVisitor(CreatedDate=System.now().addYears(-6)));
            }       
            insert livechatVisitorlist;   
            Database.BatchableContext BC;
            BAT_DeleteLiveChatVisitors obj=new BAT_DeleteLiveChatVisitors();
            Database.DeleteResult[] Delete_Result = Database.delete(livechatVisitorlist, false);
            obj.start(BC);
            obj.execute(BC,livechatVisitorlist);
            obj.finish(BC);
            DmlException expectedException;
            Test.stopTest();
        }  
    }
}