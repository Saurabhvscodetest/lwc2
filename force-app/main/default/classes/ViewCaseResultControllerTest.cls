@isTest
public class ViewCaseResultControllerTest {
	
    @isTest static void testfetchCaseInfoSearchTerm(){
        Contact testContact;
        Case testCase;
        testContact = CustomerTestDataFactory.createContact();
        testContact.Email = 'stuart@little.com';
        insert testContact;
        testCase = CaseTestDataFactory.createCase(testContact.Id);
       
        List<Case> CaseList = [Select Id, CaseNumber, Contact.Name, Case_Owner__c, jl_Case_Type__c, LastViewedDate, jl_OrderManagementNumber__c,jl_DeliveryNumber__c from Case where id =: testCase.id LIMIT 30 ];
        
        String searchTerm = 'CASE';
        ViewCaseResultController.fetchCaseRecordList ( searchTerm );
    } 
    
    @isTest static void testfetchCaseInfoNoSearchTerm(){
        Contact testContact;
        Case testCase;
        testContact = CustomerTestDataFactory.createContact();
        testContact.Email = 'stuart@little.com';
        insert testContact;
        testCase = CaseTestDataFactory.createCase(testContact.Id);
       
        List<Case> CaseList = [Select Id, CaseNumber, Contact.Name, Case_Owner__c, jl_Case_Type__c, LastViewedDate, jl_OrderManagementNumber__c,jl_DeliveryNumber__c from Case where id =: testCase.id LIMIT 30];
        
        String searchTerm = '';
        ViewCaseResultController.fetchCaseRecordList ( searchTerm );
    } 
}