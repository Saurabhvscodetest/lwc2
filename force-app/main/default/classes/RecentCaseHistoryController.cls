/******************************************************************************
* @author       Shion Abdillah
* @date         July 2016
* @description  Controller class for Recent Case History component.
*
* EDIT RECORD
*
* LOG   DATE            AUTHOR  JIRA        COMMENT      
* 001   nn/07/2016      SA      COPT-nnn    Original code
* 002   07/08/2016      MP      COPT-nnn    Pagination
*
*******************************************************************************
*/
public class RecentCaseHistoryController extends DataTablePaginator {
	
	// public Contact ContactRecord {set;get;}
	private Contact contactRecordLoc;
	private Id previousCaseContactId;
	private Id previousPSEContactId;
	@TestVisible private Contact contactWithCases;
    private static Boolean contactHasNoCases = false;

    public String CaseId {get; set;}
    public Integer totalRecordCount {get; private set;}

	// PSE Pagination Vars
	public List<SObject> fullDataList2 {get; set;}
	public Integer noOfRecords2 {get; set;}
	public Integer pageSize2 {get; set;}
    public String sortField2 {get; set;}
    public String previousSortField2 {get; set;}
    public Integer totalRecordCount2 {get; private set;}
    
    private static final Integer DEFAULT_DAYS_FOR_CLOSED_CASES = 30;
    @TestVisible private static final Integer DEFAULT_PAGE_SIZE = 5;
    private static final String OTHER_CASE_TYPE = 'Other';
    private static final Id PSE_RECORD_TYPE_ID = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonStaticUtils.CASE_RT_NAME_PRODUCT_STOCK_ENQUIRY).getRecordTypeId();
    private DateTime earliestClosedCaseDate;

    /**
     * No Args Constructor
     * @params: None
     * @rtnval: N/A
     */
	public RecentCaseHistoryController() {
		earliestClosedCaseDate = DateTime.newInstance(system.now().year(), system.now().month(), system.now().day(), 0, 0, 0);
		earliestClosedCaseDate = earliestClosedCaseDate.addDays(DEFAULT_DAYS_FOR_CLOSED_CASES * -1);

        pageSize = DEFAULT_PAGE_SIZE;
        noOfRecords = 0;
        pageSize2 = DEFAULT_PAGE_SIZE;
        noOfRecords2 = 0;
		retrieveCases(OTHER_CASE_TYPE);
		retrieveCases(CommonStaticUtils.CASE_RT_NAME_PRODUCT_STOCK_ENQUIRY);
	}
	
    /**
     * Contact getter / setter.
     * @params: None
     * @rtnval: Contact ContactRecord
     */
	public Contact ContactRecord {
		set {
			contactRecordLoc = value;
			if (previousCaseContactId == null || contactRecordLoc.Id != previousCaseContactId) {
				previousCaseContactId = contactRecordLoc.Id;
				contactWithCases = null;

				retrieveCases(OTHER_CASE_TYPE);
				retrieveCases(CommonStaticUtils.CASE_RT_NAME_PRODUCT_STOCK_ENQUIRY);
			}
		}
		get {
			return contactRecordLoc;
		}
	}

    /**
     * Retrieve list of cases for Customer.  Blank out setCons and fullDataLists so that data is reset.
     * @params: None
     * @rtnval: void
     */
	public void retrieveCases(String caseType) {
		if (caseType == CommonStaticUtils.CASE_RT_NAME_PRODUCT_STOCK_ENQUIRY) {
			resetPSEVariables();
		} else {
			resetCaseVariables();
		}
		
		if (contactRecordLoc != null) {
			if (contactWithCases == null) {
				contactWithCases = [SELECT Id, 
										(SELECT CaseNumber, LastModifiedDate, RecordType.Name, Contact_Reason__c, RecordTypeId,
												jl_Customer_Response_By__c , Reason_Detail__c, IsClosed, ClosedDate,
												Contact.Name, Owner.Name, jl_Customer_Response__c, CreatedDate, 
												jl_pse1_Stock_Number__c, pse1_Fulfilled_By__c, Status,jl_pse1_Item_Price__c,
												Description FROM Cases ORDER BY CaseNumber DESC LIMIT 100) 
									FROM Contact WHERE Id = :contactRecordLoc.Id LIMIT 1];
			}
			
			List<Case> allCaseList = contactWithCases.Cases;
			if (!allCaseList.isEmpty()) {
				for (Case c : allCaseList) {
					if (!c.IsClosed || c.ClosedDate >= earliestClosedCaseDate) {
						if (c.RecordTypeId == PSE_RECORD_TYPE_ID && caseType == CommonStaticUtils.CASE_RT_NAME_PRODUCT_STOCK_ENQUIRY) {
							fullDataList2.add(c);
						} else if (c.RecordTypeId != PSE_RECORD_TYPE_ID && caseType == OTHER_CASE_TYPE) {
							fullDataList.add(c);
						}
					}
				}
				
				if (caseType == CommonStaticUtils.CASE_RT_NAME_PRODUCT_STOCK_ENQUIRY) {
					totalRecordCount2 = fullDataList2.size();
				} else {
					totalRecordCount = fullDataList.size();
				}
			} else {
				contactHasNoCases = true;
			}
		}
	}
	
    /**
     * Retrieve list of Non-PSE cases for Customer.
     * @params: None
     * @rtnval: List<Case> RecentCases
     */
	public List<Case> RecentCases {
		set {}
		get {
			List<Case> caseList = new List<Case>();
			if (!contactHasNoCases && (fullDataList == null || fullDataList.isEmpty())) {
				retrieveCases(OTHER_CASE_TYPE);
			}
			for (Case c : (List<Case>)setCon.getRecords()) {
				caseList.add(c);
			}
			noOfRecords = caseList.size();
			return caseList;
		}
	}
	
    /**
     * Retrieve list of PSE cases for Customer.
     * @params: None
     * @rtnval: List<Case> RecentCasesPSE
     */
	public List<Case> RecentCasesPSE {
		set {}
		get {
			List<Case> caseList = new List<Case>();
			if (!contactHasNoCases && (fullDataList2 == null || fullDataList2.isEmpty())) {
				retrieveCases(CommonStaticUtils.CASE_RT_NAME_PRODUCT_STOCK_ENQUIRY);
			}
			for (Case c : (List<Case>)setCon2.getRecords()) {
				caseList.add(c);
			}
			noOfRecords2 = caseList.size();
			return caseList;
		}
	}

 
    /**
     * Return an integer value of the number of pages needed to display all selected records
     * based on page size.
     * @params: None
     * @rtnval: Integer     Total number of pages
     */
    public Integer getTotalPages(){
        Decimal totalSize = setCon.getResultSize();
        Decimal pageSize = setCon.getPageSize();
        Decimal pages = totalSize/pageSize;
        return (Integer)pages.round(System.RoundingMode.CEILING);
    }
    	
    /**
     * Return an integer value of the number of pages needed to display all selected records
     * based on page size.
     * @params: None
     * @rtnval: Integer     Total number of pages
     */
    public Integer getTotalPages2(){
        Decimal totalSize2 = setCon2.getResultSize();
        Decimal pageSize2 = setCon2.getPageSize();
        Decimal pages2 = totalSize2/pageSize2;
        return (Integer)pages2.round(System.RoundingMode.CEILING);
    }

	/**
	 * Duplicates of methods from DataTablePaginator as this class has two fullDataList variables
	 * one for each table.
	 */
	public ApexPages.Standardsetcontroller setCon2 {
		get {
			if (setCon2 == null) {
				setCon2 = new ApexPages.Standardsetcontroller(fullDataList2);
				setCon2.setPageSize(pageSize2);
				setCon2.setPageNumber(1);

				System.Debug('SetCon2 is ==== ' + setCon2);
			}
			return setCon2;
		}
		set;
	}

	public Boolean hasNext2 {
		get {
			return setCon2.getHasNext();
		}
		set;
	}
	
	public Boolean hasPrevious2 {
		get {
			return setCon2.getHasPrevious();
		}
		set;
	}
	
	public Integer pageNumber2 {
		get {
			return setCon2.getPageNumber();
		}
		set;
	}
	
	public void first2() {
		setCon2.first();
	}
	
	public void last2() {
		setCon2.last();
	}
	
	public void previous2() {
		setCon2.previous();
	}
	
	public void next2() {
		setCon2.next();
	}

    public void doSort2(){
        if (!fullDataList2.isEmpty()) {
			String order = 'asc';
			
			// This checks to see if the same header was click two times in a row, if so
			// it switches the order.
			if (previousSortField2 == sortField2) {
				order = 'desc';
				previousSortField2 = null;
			} else {
				previousSortField2 = sortField2;
			}

			//To sort the table we simply need to use this one line, nice!
			SuperSort.sortList(fullDataList2, sortField2, order);
			setCon2 = null;
        } else {
        	sortField2 = null;
        	previousSortField2 = null;
        }
	}

    /**
     * Reset lists and variables for the Non-PSE cases.
     * @params: None
     * @rtnval: void
     */
    private void resetCaseVariables() {
        setCon = null;
        fullDataList = new List<Case>();
        totalRecordCount = 0;
        sortField = null;
        previousSortField = null;
    }

    /**
     * Reset lists and variables for the PSE cases.
     * @params: None
     * @rtnval: void
     */
    private void resetPSEVariables() {
        setCon2 = null;
        fullDataList2 = new List<Case>();
        totalRecordCount2 = 0;
        sortField2 = null;
        previousSortField2 = null;
    }
}