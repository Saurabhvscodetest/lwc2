@isTest
private class ChangeOwnerButtonControllerTest {

	@testSetup static void initEntitlementTestData() {
		MilestoneUtilsTest.initEntitlementTestData();
	}


	@isTest static void testOpenCase() {
		
		User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		User userWithPermission;

		System.runAs(runningUser) {
			userWithPermission = UnitTestDataFactory.getFrontOfficeUser('TestUser');
			insert userWithPermission;
			addChangeOwnerPermissionToUser(userWithPermission);
		}

		System.runAs(userWithPermission) {

			// Create an open case
			Case openCase = UnitTestDataFactory.createCase(UnitTestDataFactory.createContact().Id);
			insert openCase;

			// Assert its open
			openCase = [SELECT IsClosed FROM Case WHERE Id = :openCase.Id];

			System.assertEquals(false, openCase.IsClosed);

			// Now press Change Owner button
			ApexPages.StandardController standardController = new ApexPages.StandardController(openCase);
			ChangeOwnerButtonController controllerExtension = new ChangeOwnerButtonController(standardController);

			// Mimic the onload action
			PageReference returnedPage = controllerExtension.validate();

			String returnedPageUrl = returnedPage.getUrl();
			String expectedUrl = '/apex/ChangeOwner?Id='+openCase.Id+'&isdtp=vw';
			
			System.assertEquals(expectedUrl, returnedPageUrl);

		}

	}
	@isTest static void testWithNadInPastNonEntitlement() {

		User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		User userWithPermission;
		Case nonEntitlementCase;

		System.runAs(runningUser) {

			userWithPermission = UnitTestDataFactory.getFrontOfficeUser('TestUser');
			insert userWithPermission;
			addChangeOwnerPermissionToUser(userWithPermission);

		}

		System.runAs(userWithPermission) {

			nonEntitlementCase = createCase();
			nonEntitlementCase.Bypass_Standard_Assignment_Rules__c = true;
			nonEntitlementCase.JL_Branch_master__c = 'Aberdeen';
			nonEntitlementCase.Contact_Reason__c = 'Core Home Services';
			nonEntitlementCase.Reason_Detail__c = 'CHS Fitted Flooring Order';
			//nonEntitlementCase.Branch_Department__c = 'Customer Support';

			// Set NAD in past
			nonEntitlementCase.JL_Next_Response_By__c = Datetime.now().addDays(-1);
			insert nonEntitlementCase;

			// Assert its has no Entitlement
			nonEntitlementCase = [SELECT EntitlementId FROM Case WHERE Id = :nonEntitlementCase.Id];
			System.assertEquals(null, nonEntitlementCase.EntitlementId);

			// Now press Change Owner button
			Test.startTest();

				PageReference currentPage = Page.ChangeOwnerButtonPage;
				Test.setCurrentPage(currentPage);

				ApexPages.StandardController standardController = new ApexPages.StandardController(nonEntitlementCase);
				ChangeOwnerButtonController controllerExtension = new ChangeOwnerButtonController(standardController);

				// Mimic onload action
				PageReference returnedPage = controllerExtension.validate();

				// Assert no page reference (redirect) was returned
				System.assertEquals(null, returnedPage);

				// Assert message was added
				List<ApexPages.Message> pageMessages = ApexPages.getMessages();
				System.assertEquals(1, pageMessages.size());
				System.assertEquals('You cannot transfer a Case with a Next Action date and time in the past', pageMessages[0].getDetail());

			Test.stopTest();


		}


	}





	@isTest static void testUserWithPermission() {
	
		User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		User userWithPermission;
		Case closedCase;

		System.runAs(runningUser) {

			userWithPermission = UnitTestDataFactory.getFrontOfficeUser('TestUser');
			insert userWithPermission;
			addChangeOwnerPermissionToUser(userWithPermission);

		}

		System.runAs(userWithPermission) {

			// Insert case
			closedCase = createCase();
			insert closedCase;

			// Close it
			closedCase.Status = 'Closed - Resolved';
			update closedCase;

			// Assert its closed
			closedCase = [SELECT IsClosed FROM Case WHERE Id = :closedCase.Id];
			System.assertEquals(true, closedCase.IsClosed);

			// Now press change owner button
			Test.startTest();

				PageReference currentPage = Page.ChangeOwnerButtonPage;
				Test.setCurrentPage(currentPage);

				ApexPages.StandardController standardController = new ApexPages.StandardController(closedCase);
				ChangeOwnerButtonController controllerExtension = new ChangeOwnerButtonController(standardController);

				// Mimic onload action
				PageReference returnedPage = controllerExtension.validate();

				// Assert no page reference (redirect) was returned
				System.assertEquals(null, returnedPage);

				// Assert message was added
				List<ApexPages.Message> pageMessages = ApexPages.getMessages();
				System.assertEquals(1, pageMessages.size());
				System.assertEquals('You are trying to change the owner of a closed case, would you like to reopen it?', pageMessages[0].getDetail());

				// Reset Trigger variables - there is some odd behaviour on second iteration.
				ClsCaseTriggerHandler.resetCaseTriggerStaticVariables();
				// Flush this list from previous execution, otherwise it will attempt to update the close milestones
				MilestoneHandler.caseMilestoneListToBeUpdated.clear();

				PageReference changeOwnerPage = controllerExtension.reopenCase();
				String returnedPageUrl = changeOwnerPage.getUrl();
				String expectedUrl = '/apex/ChangeOwner?Id='+closedCase.Id+'&isdtp=vw';

				System.assertEquals(expectedUrl, returnedPageUrl);

			Test.stopTest();


		}

	}



	@isTest static void testUserWithoutPermission() {

		User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		User userWithoutPermission;
		Case closedCase;

		System.runAs(runningUser) {
			userWithoutPermission = UnitTestDataFactory.getFrontOfficeUser('TestUser');
			insert userWithoutPermission;
		}

		System.runAs(userWithoutPermission) {

			// Insert Case
			closedCase = UnitTestDataFactory.createCase(UnitTestDataFactory.createContact().Id);
			insert closedCase;

			// Close case
			closedCase.Status = 'Closed - Resolved';
			update closedCase;

			// Assert its closed
			closedCase = [SELECT IsClosed FROM Case WHERE Id = :closedCase.Id];
			System.assertEquals(true, closedCase.IsClosed);

			Test.startTest();
				PageReference currentPage = Page.ChangeOwnerButtonPage;
				Test.setCurrentPage(currentPage);

				ApexPages.StandardController standardController = new ApexPages.StandardController(closedCase);
				ChangeOwnerButtonController controllerExtension = new ChangeOwnerButtonController(standardController);

				// Mimic onload action
				PageReference returnedPage = controllerExtension.validate();

				// Assert no page reference (redirect) was returned
				System.assertEquals(null, returnedPage);

				// Assert message was added
				List<ApexPages.Message> pageMessages = ApexPages.getMessages();
				System.assertEquals(1, pageMessages.size());
				System.assertEquals('You do not have the required permission to change a record owner.', pageMessages[0].getDetail());


			Test.stopTest();

		}

	}
	


	@isTest static void testErrorSurfacedFromReopenUtility() {


		User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		User testUser;
		Case testCase;

		System.runAs(runningUser) {
			testUser = UnitTestDataFactory.getFrontOfficeUser('TestUser');
			testUser.Team__c = '';
			insert testUser;
		}



		System.runAs(testUser) {
			
			// Create Case
			testCase = createCase();
			insert testCase;

			// Take ownership of Case, Remove the 'Last Queue', Close Case
			testCase.OwnerId = UserInfo.getUserId();
			update testCase; // Have to update first, otherwise Last Queue will get re-populated when we remove it.

			testCase.Status = 'Closed - Resolved';
			testCase.JL_Last_Queue_Name__c = '';
			testCase.JL_Last_Queue__c = '';
			update testCase;

			// Flush this list from previous execution, otherwise it will attempt to update the close milestones
			MilestoneHandler.caseMilestoneListToBeUpdated.clear();

			Test.startTest();

				PageReference currentPage = Page.ChangeOwnerButtonPage;
				Test.setCurrentPage(currentPage);

				ApexPages.StandardController standardController = new ApexPages.StandardController(testCase);
				ChangeOwnerButtonController controllerExtension = new ChangeOwnerButtonController(standardController);
	
				// Reset Trigger variables - there is some odd behaviour on second iteration.
				ClsCaseTriggerHandler.resetCaseTriggerStaticVariables();
				// Flush this list from previous execution, otherwise it will attempt to update the close milestones
				MilestoneHandler.caseMilestoneListToBeUpdated.clear();

				// Mimic onload action + reopen
				controllerExtension.validate();
				PageReference returnedPage = controllerExtension.reopenCase();

				// Assert message was added
				List<ApexPages.Message> pageMessages = ApexPages.getMessages();
				Set<String> errorMessages = new Set<String>();

				for(ApexPages.Message pageMessage : pageMessages) {
					errorMessages.add(pageMessage.getDetail());
				}

				System.assert(errorMessages.contains('You cannot re-open a case as a Tier 1 user if you are not assigned to a Primary Team.'));
				System.assert(errorMessages.contains('You do not have the required permission to change a record owner.'));

			Test.stopTest();

		}

	}


	/* Helper */

	private static void addChangeOwnerPermissionToUser(User testUser) {

		Id changeOwnerPermissionSetId = [SELECT Id FROM PermissionSet WHERE Name = :ChangeOwnerButtonController.CHANGE_OWNER_PERMISSION LIMIT 1].Id;

		PermissionSetAssignment permissionSetAssignment = new PermissionSetAssignment();
		permissionSetAssignment.AssigneeId = testUser.Id;
		permissionSetAssignment.PermissionSetId = changeOwnerPermissionSetId;

		insert permissionSetAssignment;

	}

	private static Case createCase() {

		Case newCase = UnitTestDataFactory.createCase(UnitTestDataFactory.createContact().Id);
		newCase.Bypass_Standard_Assignment_Rules__c = true;
		newCase.JL_Branch_master__c = 'Oxford Street';
		newCase.Contact_Reason__c = 'Pre-Sales';
		newCase.Reason_Detail__c = 'Buying Office Enquiry';
		
		return newCase;

	}


}