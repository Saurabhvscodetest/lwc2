/* Description  : 
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                         Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   24/01/2019        Vignesh Kotha                   Created               COPT-4260
*
*/
public class FetchCaseCustomersController {    
    @AuraEnabled
    public static List<FetchCaseCustomersController.CustomerDetails> getCaseCustomers(string recordId){
        Set<String> contactids = new Set<String>();
        List<FetchCaseCustomersController.CustomerDetails> updatedlist = new List<FetchCaseCustomersController.CustomerDetails>();
        List<case> casedata = [select id,Contact_ID_List__c from case where id=:recordId];
        if(casedata.size()>0){
            String ContactRecords = casedata[0].Contact_ID_List__c;
            for(String str:ContactRecords.split(',')){
                contactids.add(str);
            }           
        }  
        for(Contact contactlist : [SELECT Id,FirstName,LastName,Email,MobilePhone,MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry,(select id, status from Cases where status!='closed' ) FROM Contact where Id IN : contactids]){
            FetchCaseCustomersController.CustomerDetails customerWrapper = new FetchCaseCustomersController.CustomerDetails();
            customerWrapper.Contactid = contactlist.Id;
            customerWrapper.CFirstName = contactlist.FirstName;
            customerWrapper.CLastName = contactlist.LastName;
            customerWrapper.OpenCases = contactlist.cases.size();
            customerWrapper.Emailadd = contactlist.Email;
            customerWrapper.Phonenum = contactlist.MobilePhone;
            customerWrapper.Mailstreet = contactlist.MailingStreet;
            customerWrapper.Mailcity = contactlist.MailingCity;
            customerWrapper.Mailstate = contactlist.MailingState;
            customerWrapper.Mailpost = contactlist.MailingPostalCode;
            customerWrapper.Mailcountry = contactlist.MailingCountry;
            updatedlist.add(customerWrapper);
        } 
        System.debug('updatedlist'+updatedlist);
        return updatedlist;        
    }
     @AuraEnabled
    public static void UpdateCaseRecord(String contactrec,string caserec){
       List<case> caselist = [select id,contactid from case where id=:caserec];
        caselist[0].contactid = contactrec;
        update caselist;
    }
    public class CustomerDetails{
        @AuraEnabled public string CFirstName;
        @AuraEnabled public string CLastName;
        @AuraEnabled public Integer OpenCases;
        @AuraEnabled public Id Contactid;
        @AuraEnabled public String Phonenum;
        @AuraEnabled public String Emailadd;
        @AuraEnabled public String Mailstreet;
        @AuraEnabled public String Mailcity;
        @AuraEnabled public String Mailstate;
        @AuraEnabled public String Mailpost;
        @AuraEnabled public String Mailcountry;
    }
}