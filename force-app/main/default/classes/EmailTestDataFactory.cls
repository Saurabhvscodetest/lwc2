@isTest
public with sharing class EmailTestDataFactory {

	public static EmailMessage createEmailMessage(Id caseId, Boolean isInboundEmail){

		EmailMessage email = new EmailMessage();

		email.Incoming = isInboundEmail;
		email.FromAddress = (isInboundEmail == TRUE) ? 'inbound_user@test.com' : 'salesforce_null@johnlewis.co.uk';
		email.ToAddress = (isInboundEmail == TRUE) ? 'salesforce_null@johnlewis.co.uk' : 'inbound_user@test.com';
		email.Subject = 'Email Subject';
		email.TextBody = 'Email Body';
		email.ParentId = caseId;

		return email;
	}

	public static EmailMessage createInboundEmailMessage(Id caseId){
		return createEmailMessage(caseId, TRUE);
	}

	public static EmailMessage createOutboundEmailMessage(Id caseId){
		return createEmailMessage(caseId, FALSE);
	}
	
}