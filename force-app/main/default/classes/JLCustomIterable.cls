/******************************************************************************
* @author       Ranjan Samal
* @date         21/09/2017
* @description  Iterator Class used for Pagination

******************************************************************************/
global class  JLCustomIterable implements Iterator<list<JLUserWrapper>> {
    
    global Integer setPageSize                        {get; set;} 
           list<JLUserWrapper> innerLists             {get; set;}
           list<JLUserWrapper> requestedWrapperLists  {get; set;}        
           Integer             i                      {get; set;}
    
    /**
    * @description  Constructor
    * @author       @rsamal 
    * @param        List of user Wrapper - JLUserWrapper
    */
    global JLCustomIterable(List<JLUserWrapper> lstUsrWr) {
        innerLists = new list<JLUserWrapper >(); 
        requestedWrapperLists = new list<JLUserWrapper >();     
        innerLists = lstUsrWr;
        setPageSize = 10;
        i = 0; 
    }
    
    /**
    * @description  Creating a boolean function hasNext
    * @author       @rsamal 
    * @return       boolean 
    */
    global boolean hasNext() { 
        if(i >= innerLists.size()) {
            return false; 
        } else {
            return true; 
        }
    } 
    
    /**
    * @description  Creating a boolean function hasPrevious
    * @author       @rsamal 
    * @return       boolean 
    */
    global boolean hasPrevious() { 
        if(i <= setPageSize) {
            return false; 
        } else {
            return true; 
        }
    } 

    /**
    * @description  Creating a next fuction which is returning the list of JLUserWrapper 
    * @author       @rsamal 
    * @return       list of JLUserWrapper   
    */
    global list<JLUserWrapper > next() {       
        //constructing a new list 
        requestedWrapperLists = new list<JLUserWrapper >();        
        integer startNumber;
        //getting the size of the list InnerList
        integer size = innerLists.size();
        //hasNext boolean function in if statement
        if(hasNext()) { 
            //setPageSize is the number of records that will be shown in a page 
            if(size <= (i + setPageSize)){
                startNumber = i;
                i = size;
            }
            else {
                i = (i + setPageSize);
                startNumber = (i - setPageSize);
            }
            for(integer start = startNumber; start < i; start++) {
                requestedWrapperLists.add(innerLists[start]);
            }
        } 
        return requestedWrapperLists;
    } 
    
    /**
    * @description  Creating a previous fuction which is returning the list of JLUserWrapper 
    * @author       @rsamal 
    * @return       list of JLUserWrapper   
    */
    global list<JLUserWrapper > previous() {  

        requestedWrapperLists = new list<JLUserWrapper >(); 
        integer size = innerLists.size(); 
        if(i == size) {
            if(math.mod(size, setPageSize) > 0) {    
                i = size - math.mod(size, setPageSize);
            }
            else {
                i = (size - setPageSize);
            } 
        }
        else {
            i = (i - setPageSize);
        }

        for(integer start = (i - setPageSize); start < i; ++start) {
            requestedWrapperLists.add(innerLists[start]);
        } 
        return requestedWrapperLists;
    }   
}