@isTest
public with sharing class QueueTestDataFactory {
	
	public static final Boolean OMNI_ENABLED = TRUE;
	public static final Boolean OMNI_DISABLED = FALSE;
	public static final String GROUP_QUEUE_TYPE = 'Queue';
	public static final String CASE_TYPE = 'Case';
    //Commented and modified below as a part of COPT-3585
	//public static Set<String> relatedQueueSuffixSet = new Set<String>{'_WARNING', '_FAILED', '_ACTIONED'};
	public static Set<String> relatedQueueSuffixSet = new Set<String>{'_FAILED', '_ACTIONED'};
	public static List<Group> groupsToBeInserted = new List<Group>();
	public static List<QueueSObject> queuesObjectsToBeInserted = new List<QueuesObject>();
	
	/**
    * @description   Logic to setup Groups and associated QueueSObject for a given Queue       
    * @author        @stuartbarber
    */
	public static List<QueueSObject> createQueues(String queueName, Boolean createRelatedQueues){

		groupsToBeInserted = createQueueGroups(queueName, createRelatedQueues);
		insert groupsToBeInserted;

		queuesObjectsToBeInserted = createQueuesObjects(groupsToBeInserted);
		return queuesObjectsToBeInserted;
	}

	
	/**
    * @description   Logic to setup Group records for Queues. If createRelatedQueues = TRUE, setup default and related (warning,failed,actioned) queues        
    * @author        @stuartbarber
    */
	public static List<Group> createQueueGroups(String queueName, Boolean createRelatedQueues){

		groupsToBeInserted = (createRelatedQueues == TRUE) ? createRelatedQueueGroups(queueName) : createDefaultQueueGroup(queueName);

		return groupsToBeInserted;
	}

	/**
    * @description   Logic to setup Groups for a Default Queue       
    * @author        @stuartbarber
    */
	public static List<Group> createDefaultQueueGroup(String queueName){

		Group defaultGroup = new Group();
		defaultGroup.Name = queueName;
		defaultGroup.Type = GROUP_QUEUE_TYPE;
		groupsToBeInserted.add(defaultGroup);

		return groupsToBeInserted;

	}

	/**
    * @description   Logic to setup Groups for the Related (Warning, Failed, Actioned) Queues associated to the Default Queue Group       
    * @author        @stuartbarber
    */
	public static List<Group> createRelatedQueueGroups(String queueName){
		groupsToBeInserted = createDefaultQueueGroup(queueName);

		for(String relateQueueSuffix : relatedQueueSuffixSet){

			Group relatedGroup = new Group();
			relatedGroup.Name = queueName + relateQueueSuffix;
			relatedGroup.Type = GROUP_QUEUE_TYPE;
			groupsToBeInserted.add(relatedGroup);
		}

		return groupsToBeInserted;
	}

	/**
    * @description   Logic to setup QueueSObject records for the Group records setup for a given Queue       
    * @author        @stuartbarber
    */
	public static List<QueuesObject> createQueuesObjects(List<Group> groupsToBeInserted){

		for(Group grp : groupsToBeInserted){

			QueuesObject qObject = new QueuesObject();
			qObject.QueueId = grp.Id;
			qObject.SobjectType = CASE_TYPE;
			queuesObjectsToBeInserted.add(qObject);

		}

		return queuesObjectsToBeInserted;
	}
}