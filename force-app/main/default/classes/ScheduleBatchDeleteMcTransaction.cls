global class ScheduleBatchDeleteMcTransaction implements Schedulable{

     global void execute(SchedulableContext sc){
        BatchDeleteMcTransaction obj = new BatchDeleteMcTransaction();
        Database.executebatch(obj);
    }
    
}