public class CustomerContactController {
    // Call logging parameters
    public static final Id CALL_LOG_TASK_RECORD_TYPE_ID = Schema.SObjectType.Task.getRecordTypeInfosByName().get(CommonStaticUtils.TASK_RT_NAME_CALL_LOG).getRecordTypeId();
    private static final Config_Settings__c ENABLE_CUSTOMER_CONTACT_FOR_ALL_CONFIG = Config_Settings__c.getInstance('ENABLE_CUSTOMER_CONTACT_FOR_ALL_USERS');
    private static final Boolean ENABLE_CUSTOMER_CONTACT_FOR_ALL_USERS = ENABLE_CUSTOMER_CONTACT_FOR_ALL_CONFIG != null ? ENABLE_CUSTOMER_CONTACT_FOR_ALL_CONFIG.Checkbox_Value__c : false;
    private static final string CASE_ORIGIN_PICKLIST_DEFAULT_VALUE = CommonStaticUtils.getValue('CASE_ORIGIN_PICKLIST_DEFAULT_VALUE', 'Phone');
    private static final Boolean IS_USER_ASSIGNED_TO_CAPITA_PROFILE = CommonStaticUtils.UserHasProfileNamed('Capita');


    public List<Task> taskLst {get;set;}

    // Single Data members
    public Boolean isCreateNewCustomerEnabled { get; set; }
    public Case CaseRecord {get;set;}
    private string caseRecordIdProperty;
    public Contact ContactRecord {get;set;}
    public Contact NewContactRecord {get;set;}
    public Contact previousContactRecord {get;set;}
    public string ContactRecordId {get;set;}
    public Task TaskRecord {get;set;}

    // Define variable that is true if there are duplicate records
    public boolean hasDuplicateResult {get;set;}

    public String CaseDescription {set;get;}

    // Collection Data members
    public List<Case> CaseCollection {get;set;}
    public List<Contact> ContactCollection {get;set;}
    public List<Contact> primaryContactCollection {get;set;}
    public Integer noOfOrderingCustomers{get;set;}
    public Integer noOfNonOrderingCustomers{get;set;}
    public List<Contact> duplicateContactRecords {set;get;} //Initialize a list to hold any duplicate records

    //Visual Members
    public Boolean ShowCaptureBasicInformationAndSearch {get;set;}
    public Boolean ShowCustomerInformation {get;set;}  
    public Boolean ShowPSERecordPanel {get;set;}
    public Boolean ShowCaseRecordPanel {get;set;}
    public Boolean UserHasCallLoggingAccessEnabled {get;set;}
    public Boolean ShowContactResults {get;set;}
    public Boolean ShowCaseResults {get;set;}

    public Boolean ShowAddNewCustomerPanel {get;set;}
    // FIXME: Remove mis-spelled property
    public Boolean ShowAddNewCustomerPannel {get;set;}

    //Record Search Members
    public string SearchCustomersText {get;set;}
    public string SearchCasesText {get;set;}

    // Assignment Option Members
    public Boolean RetainCase {get;set;}
    public String selectedTab {get;set;}
    public String AssignmentOverride {get; set;}
    public List<SelectOption> ManagementAssigmentOverrides {get; private set;}
    public Boolean ShowRetainCase {get; private set;}
    public Boolean ShowActionTaken {get; private set;}
    public List<SelectOption> ActionTakenOptions {get; set;}
    public string ChosenActionTakenOption {get; set;}
    public Boolean ShowStatus {get; private set;}
    public List<SelectOption> StatusOptions {get; set;}
    public string ChosenStatusOption {get; set;}
    public Boolean ShowManagementAssignmentOverrides {get; private set;}
    @TestVisible private Boolean IsContact = false;
    public String isGuest{get; set;}
    public String sourceOfRequest{get; set;}


    //Users Members
    public Integer UserTier {get; private set;}
    @TestVisible private Id userTeamQueueId;
    public static final Boolean userHasCallLoggingAccess = ENABLE_CUSTOMER_CONTACT_FOR_ALL_USERS ? ENABLE_CUSTOMER_CONTACT_FOR_ALL_USERS : CommonStaticUtils.isPermissionSetAssignedToSingleUserByName(UserInfo.getUserId(), 'Call_Logging_Access');
    public static final User tempUser = CommonStaticUtils.getCurrentUser();

    public CustomerContactController() {

        // Definition of options for Action Taken dropdown list
        ActionTakenOptions = new List<SelectOption>();
        ActionTakenOptions.add(new SelectOption('New case created', 'New case created'));
        ActionTakenOptions.add(new SelectOption('Customer contacted', 'Customer contacted'));
        ActionTakenOptions.add(new SelectOption('No customer contact required', 'No customer contact required'));
        ChosenActionTakenOption = 'New case created';

        // Definition of options for Status dropdown list
        StatusOptions = new List<SelectOption>();
        StatusOptions.add(new SelectOption('New', 'New'));
        StatusOptions.add(new SelectOption('Active - Awaiting on customer update', 'Active - Awaiting on customer update'));
        StatusOptions.add(new SelectOption('Active - Follow up required', 'Active - Follow up required'));
        ChosenStatusOption = 'New';
        
        // Initialise Assignment variables and permissions <<004>>
        ManagementAssigmentOverrides = new List<SelectOption>(); 
        ManagementAssigmentOverrides.add(new SelectOption('DEFAULT', 'Use Assignment Rules')); 
        ManagementAssigmentOverrides.add(new SelectOption('RETAIN', 'Retain Case')); 
        ManagementAssigmentOverrides.add(new SelectOption('MYTEAM', 'Assign To My Team')); 
        AssignmentOverride = 'DEFAULT';
        ShowRetainCase = false;
        ShowActionTaken = false;
        ShowStatus = false;
        ShowManagementAssignmentOverrides = false;
        UserTier = tempUser.Tier__c == null ? 1 : tempUser.Tier__c.intValue();
        // Management users with teams get full overrides.  Tier 2 and managers with no team
        // can retain cases <<004>>
        if (UserTier > 2 && String.isNotBlank(tempUser.Team__c)) {
            userTeamQueueId = TeamUtils.getQueueIdForTeam(tempUser.Team__c);
            if (userTeamQueueId != null) {
                ShowManagementAssignmentOverrides = true;
            } else {
                ShowRetainCase = true;
            }
        } else if (UserTier > 1) {
            ShowRetainCase = true;
        }

        if (UserTier > 1) {
                ShowActionTaken = true;
            ShowStatus = true;
        }

        this.duplicateContactRecords = new List<Contact>();
        IsContact = false;
        ContactRecord = new Contact();
        NewContactRecord  = new Contact();
        ContactCollection = new List<Contact>();
        primaryContactCollection = new List<Contact>();
        noOfOrderingCustomers=0;
        noOfNonOrderingCustomers=0;
        isGuest= 'false';
        sourceOfRequest = 'connex';
        
        CaseCollection = new List<Case>();
        RetainCase = false;
        hideAllPanels();

        ShowCaptureBasicInformationAndSearch = true;

        if(apexpages.currentpage().getparameters().get('contactId') != null){
            string contactId = apexpages.currentpage().getparameters().get('contactId');
            PerformSearchOnContactID(contactId);
        }
       

       
        if(IsContact){
            ShowCustomerInformation = true;
        }
        ShowContactResults = false;
        ShowCaseResults = false;
        ShowAddNewCustomerPanel = false;
        if (userHasCallLoggingAccess) {
            UserHasCallLoggingAccessEnabled = true;
        } else {
            UserHasCallLoggingAccessEnabled = false;
        }
        isCreateNewCustomerEnabled = false;
        isCreateNewCustomerEnabled = false;
        hasDuplicateResult = false;
        CaseRecord = new Case();
        CaseRecord.Origin = CASE_ORIGIN_PICKLIST_DEFAULT_VALUE;
        setDefaultInitiatingSource();       
        TaskRecord = new Task(Subject = CommonStaticUtils.TASK_SUBJECT_CALL_LOG, Status = CommonStaticUtils.TASK_STATUS_CLOSED_RESOLVED, RecordTypeId = CALL_LOG_TASK_RECORD_TYPE_ID);

        
        if(apexpages.currentpage().getparameters().get('source') != null && apexpages.currentpage().getparameters().get('source').equals('orderUI')){
            sourceOfRequest = apexpages.currentpage().getparameters().get('source');

            if(apexpages.currentpage().getparameters().get('isGuest') != null){
                isGuest = apexpages.currentpage().getparameters().get('isGuest');
            }
        
            if(apexpages.currentpage().getparameters().get('shopperId') != null && !isGuest.equals('true')){
                string shopperId = apexpages.currentpage().getparameters().get('shopperId');
                PerformSearchOnShopperID(shopperId);
            }
            

            
            SetupNewCasePanels();

            if(apexpages.currentpage().getparameters().get('orderNumber') != null){
                CaseRecord.jl_OrderManagementNumber__c = apexpages.currentpage().getparameters().get('orderNumber');
            }
            
        }
    }

    @TestVisible
    private void hideAllPanels() {
        ShowCaptureBasicInformationAndSearch = false;
        ShowCustomerInformation = false;
        ShowPSERecordPanel = false;
        ShowCaseRecordPanel = false;
        // UserHasCallLoggingAccessEnabled = false;
        ShowContactResults = false;
        ShowCaseResults = false;
        ShowAddNewCustomerPanel  = false;
    }

    public PageReference SOSLSearchContacts() {
        ShowContactResults = true;
        ShowCaseResults = false;
        PerformSearch('Contacts', true);
        return null;
    }

    public PageReference SOSLSearchCases() {
        ShowCaseResults = true;
        ShowContactResults = false;
        PerformSearch('Cases',true);
        return null;
    }

    public PageReference SetupNewCasePanels() {
        CaseRecord.contactId = contactRecord.id;
        hideAllPanels();
        ShowCaseRecordPanel = true;
        CaseRecord.jl_pse1_Stock_Number__c = null;
        return null;
    }

    public PageReference SetUpPSEPanel() {
        CaseRecord.contactId = contactRecord.id;
        CaseRecord.jl_CRNumber__c = null;
        CaseRecord.jl_DeliveryNumber__c = null;
        CaseRecord.jl_OrderManagementNumber__c = null;
        hideAllPanels();
        ShowPSERecordPanel = true;
        return null;
    }

    public PageReference LoadCustomerDetails() {
        PerformSearchOnContactID(ContactRecordId);
        ShowCustomerInformation = true; 
        ShowCaptureBasicInformationAndSearch = true;
        ShowAddNewCustomerPanel = false;    
        return null;
    }

    public void PerformSearchOnContactID(String ContactId) {
        ContactRecord = [SELECT Id, FirstName, Name, Description, LastName, Email, Prefered_method_of_Contact__c,Active_Status__c,RecordType.DeveloperName,
                                MobilePhone, HomePhone, MailingAddress, MailingStreet, MailingCity,Customer_Segmentation__c ,Shopper_ID__c,
                                MailingState, MailingPostalCode, MailingCountry, (SELECT Id, CreatedDate, Owner.Name, Subject, RecordTypeId, Level_1__c, Level_2__c, Level_3__c, Level_4__c, Level_5__c,
                                CRNumber__c, Delivery_Number__c, Order_Number__c, Product_Number__c  FROM Tasks where RecordTypeId = : CALL_LOG_TASK_RECORD_TYPE_ID ORDER BY CreatedDate DESC LIMIT 5)
                         FROM Contact WHERE Id = : ContactId];
        if (ContactRecord != null) {
            IsContact = true;
            ContactRecordId = ContactId;
            taskLst = ContactRecord.Tasks;
        }
    }

    public void PerformSearchOnShopperID(String shopperId) {
            
        List<Contact>ContactRecordList = [SELECT Id, FirstName, Name, Description, LastName, Email, Prefered_method_of_Contact__c,Active_Status__c,RecordType.DeveloperName,
                                MobilePhone, HomePhone, MailingAddress, MailingStreet, MailingCity,Customer_Segmentation__c , Shopper_ID__c ,
                                MailingState, MailingPostalCode, MailingCountry, (SELECT Id, CreatedDate, Owner.Name, Subject, RecordTypeId, Level_1__c, Level_2__c, Level_3__c, Level_4__c, Level_5__c,
                                CRNumber__c, Delivery_Number__c, Order_Number__c, Product_Number__c  FROM Tasks where RecordTypeId = : CALL_LOG_TASK_RECORD_TYPE_ID ORDER BY CreatedDate DESC LIMIT 5)
                         FROM Contact WHERE Shopper_ID__c = : shopperId Limit 1];
                         
        if (ContactRecordList != null && ContactRecordList.size() > 0 && ContactRecordList[0] != null) {
            IsContact = true;
            ContactRecord = ContactRecordList[0];
            ContactRecordId = ContactRecord.id;
            taskLst = ContactRecordList[0].Tasks;
        }else{
         //Customer not found will be treated as guest
         isGuest= 'true';
        }
    }


    @TestVisible
    private List<Contact> findCustomers(String searchText) {

        String escapedSearchString = String.escapeSingleQuotes(searchText);
        List<Contact> contactCollection = new List<Contact>();

        string DynamicSOSL = 'FIND \'' + escapedSearchString + '\' IN ALL FIELDS RETURNING Contact(RecordType.DeveloperName,Name,Salutation,Email,HomePhone,MobilePhone,MailingPostalCode,MailingStreet,CreatedDate,MailingCity,LastModifiedDate WHERE Primary_Record__c = \'Yes\' LIMIT 200)';
        List<List<sObject>> QueryResults = search.query(DynamicSOSL);

        if (QueryResults.size() > 0){
            system.debug('QueryResults ===>' + QueryResults.size());
            // get the first object out of the query results
            List<sObject> results = QueryResults[0];

            for(sObject item : results){
                Contact castItem = (Contact)item;
                ContactCollection.add(castItem);
            }
        }

        return contactCollection;
    }

    private void PerformSearch(String SearchType, Boolean IsSOSL) {

        isCreateNewCustomerEnabled = true;
        String DynamicSOSL;
        List<List<sObject>> QueryResults;

        //CaseRecord = null;
        ContactRecord = null;
        NewContactRecord = null;
        previousContactRecord = null;
        ShowCustomerInformation = false;

        if (SearchType == 'Contacts' && SearchCustomersText != '') {
            List<Contact> allContactResults = new List<Contact>();
            allContactResults = findCustomers(SearchCustomersText);

            primaryContactCollection = new List<Contact>();
            ContactCollection = new List<Contact>();
            noOfOrderingCustomers=0;
            noOfNonOrderingCustomers=0;

            String orderingCustomerRecordTypeName = Config_Settings__c.getInstance('ORDERING_CUSTOMER_RECORDTYPE').Text_Value__c;
            for(Contact con : allContactResults){
                if(con.RecordTypeId != null && con.RecordType.DeveloperName.equalsIgnoreCase(orderingCustomerRecordTypeName)){
                    primaryContactCollection.add(con);
                    noOfOrderingCustomers++;
                }else{
                    ContactCollection.add(con);
                    noOfNonOrderingCustomers++;
                }
            }
            selectedTab = noOfOrderingCustomers >0 ?'Primary':'secondary';        
        }else if(SearchType == 'Cases' && SearchCasesText != ''){
            CaseCollection = new List<Case>();
            if (IsSOSL) {
                DynamicSOSL = 'FIND \'' + SearchCasesText + '\' IN ALL FIELDS RETURNING Case(CaseNumber,Type__c,RecordType.Name,jl_Customer_Response_By__c ,Type_Detail__c ,Contact.Name,Owner.Name,jl_Customer_Response__c,CreatedDate,Description,DPA_Checked__c,Customer_Last_Name__c, Contact_Reason__c,Reason_Detail__c,Status LIMIT 200)';
                QueryResults = search.query(DynamicSOSL);
                if (QueryResults.size() > 0) {
                    // get the first object out of the query results
                    List<sObject> results = QueryResults[0];
                    for (sObject item : results) {
                        Case castItem = (Case)item;
                        CaseCollection.add(castItem);
                    }
                    ShowCaseResults = true;
                }
            }
        }
    }

    public void SaveContact() {

        hideAllPanels();
        ShowCustomerInformation = true;
        ShowCaptureBasicInformationAndSearch = true;           
        ApexPages.getMessages().clear();
        ApexPages.currentPage().setRedirect(false);

        Boolean isCurrentRecordNotModified = ((previousContactRecord  == null) || (
                ((newContactRecord.FirstName == null && previousContactRecord.FirstName == null) || ((newContactRecord.FirstName !=null) && newContactRecord.FirstName.equals(previousContactRecord.FirstName)) )
                && ((newContactRecord.LastName == null && previousContactRecord.LastName == null) || ((newContactRecord.LastName !=null) && newContactRecord.LastName .equals(previousContactRecord.LastName )) )
                && ((newContactRecord.Email== null && previousContactRecord.Email== null) || ((newContactRecord.Email!=null) && newContactRecord.Email .equals(previousContactRecord.Email)) )
                && ((newContactRecord.MobilePhone== null && previousContactRecord.MobilePhone== null) || ((newContactRecord.MobilePhone!=null) && newContactRecord.MobilePhone.equals(previousContactRecord.MobilePhone)))) );

        previousContactRecord = new Contact();
        previousContactRecord.FirstName = NewContactRecord.FirstName ;
        previousContactRecord.LastName = NewContactRecord.LastName ;
        previousContactRecord.Email = NewContactRecord.Email;
        previousContactRecord.MobilePhone = NewContactRecord.MobilePhone;

                
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.allowSave = this.hasDuplicateResult && isCurrentRecordNotModified;
        dml.DuplicateRuleHeader.runAsCurrentUser = true;

        List<sObject> duplicateRecords = new List<sObject>();
        this.hasDuplicateResult = false;

        Database.SaveResult sr = Database.insert(NewContactRecord, dml);

        List<Id> duplicateContactIds = new List<Id>();
        if (!sr.isSuccess()) {
            for (Database.Error duplicateError : sr.getErrors()) {
                if (String.valueOf(duplicateError.getStatusCode()).equalsIgnoreCase('DUPLICATES_DETECTED')) {
                    ApexPages.getMessages().clear();
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, duplicateError.getMessage()));
                    Datacloud.DuplicateResult duplicateResult = ((Database.DuplicateError)duplicateError).getDuplicateResult();
                    for(Datacloud.MatchResult duplicateMatchResult : duplicateResult.getMatchResults()) {
                        List<Datacloud.MatchRecord> matchRecords = duplicateMatchResult.getMatchRecords();
                        system.debug('##MATCHING RECORDS ' +matchRecords );
                        // Now you have list of all matching records. Explore Datacloud.MatchRecord> and you have option to collect all those records and show according to you requirment
                        for (Datacloud.MatchRecord matchRecord : matchRecords) {
                            System.debug('MatchRecord: ' + matchRecord.getRecord());
                            duplicateRecords.add(matchRecord.getRecord());
                            duplicateContactIds.add((Id)matchRecord.getRecord().get('Id'));
                        }
                        this.hasDuplicateResult = !duplicateRecords.isEmpty();
                         duplicateContactRecords = [select id, Firstname, lastname, Name, Salutation, MailingPostalCode, Email, MobilePhone,Active_Status__c,Owner.Name,LastModifiedDate from Contact where id IN: duplicateContactIds];
                    }
                }
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, duplicateError.getMessage()));
            }
        }

        if(sr.isSuccess()) {
            //insert ContactRecord;
            ContactRecord = NewContactRecord ;
            ContactRecordId = ContactRecord.id;
            system.debug('New record ID ::' + ContactRecord.id);
            isCreateNewCustomerEnabled = false;
            previousContactRecord = null;
            taskLst = ContactRecord.Tasks;
            System.debug('Inside ShowCaptureBasicInformationAndSearch END =======: ' + ShowCaptureBasicInformationAndSearch);
        } else {
            system.debug('duplicate records - '+ this.hasDuplicateResult + '===' + duplicateContactRecords );
            ShowAddNewCustomer();
        }
    }

    public void ShowAddNewCustomer(){
        hideAllPanels();
        ShowAddNewCustomerPanel = true;
        ShowCaptureBasicInformationAndSearch = false;
    }

    public void NavigateToAddNewCustomer() {
        NewContactRecord  = new Contact();
        ShowAddNewCustomer();
        this.hasDuplicateResult = false;
        this.duplicateContactRecords.clear();
    }

    public void NavigateBackfromAddNewCustomer() {
        hideAllPanels();
        ShowAddNewCustomerPanel = false;
        ShowCaptureBasicInformationAndSearch = true;
    }

    public void NavigateBackfromCaseScreen() {
        hideAllPanels();
        ShowCaptureBasicInformationAndSearch = true;
        ShowCustomerInformation = true;
    }

    public PageReference AssignStandardCase() {

        try{

            if(CaseRecord.Contact_Reason__c == null){
                CaseRecord.Contact_Reason__c.addError('Please select a Contact Reason.');
                return null;
            }
            else{

                CaseRecord.jl_Transfer_case__c = !RetainCase && AssignmentOverride == 'DEFAULT';

                if (AssignmentOverride == 'MYTEAM') {
                    CaseRecord.OwnerId = userTeamQueueId;
                }
                CaseRecord.jl_Action_Taken__c = ChosenActionTakenOption;
                CaseRecord.Status = ChosenStatusOption;
                CaseRecord.id = null;
                insert CaseRecord;
                insertCustomerContactTask();
                PageReference pageRef = new PageReference('/' + CaseRecord.id );
                return pageRef;
            }
        }
        catch (Exception e) {
           //[Added By: Ranjan - COPT-2457 - To display valid error messages on the page to the user]
            ApexPages.addMessages(e);       
            System.debug('CustomerContactController - AssignCase - Exception' + e);
            return null;           
        }
    }

    public PageReference CloseCase() {
        Savepoint sp = Database.setSavepoint();
        try {
            insert CaseRecord;
            insertCustomerContactTask();
            String returnURL = '/'+ CaseRecord.id;
            PageReference pageRef = new PageReference('/' + CaseRecord.id + '/s');
            pageRef.getParameters().put('retURL', returnURL);
            return pageRef;
        }
        catch (Exception e) {
            Database.rollback(sp);
            System.debug('CustomerContactController - CloseCase - Exception' + e);
            return null;
        }
    }


    public PageReference AssignPSECase() {
        PageReference pageRef;

        try {
            CaseRecord.id = null;
            CaseRecord.recordtypeid = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonStaticUtils.CASE_RT_NAME_PRODUCT_STOCK_ENQUIRY).getRecordTypeId();
            insert CaseRecord;
            insertCustomerContactTask();
            pageRef = new PageReference('/' + CaseRecord.id);
            return pageRef;
        }
        catch (Exception e) {
            System.debug('CustomerContactController - AssignPSECase - Exception' + e);
            return null;
        }
    }

    /**
     * TODO Update Header.
     * @params: None
     * @rtnval: void
     */
    public void saveCaseDescription() {
         CaseRecord.description = caseDescription;
    }

    public void saveCaseDescrition() {
        // FIXME: remove mis-spelled method.
    }


    /**
     * FIXME: Obsolete now that we will ALWAYS display these fields remove once buttons
     * have been deleted from page.
     * Only show Capture Contact button if user has permissions and panel not displayed.
     * @params: None
     * @rtnval: Boolean     Display button or not.
     */
    public Boolean getShowCaptureContactButton() {
        return false;
        /*
        if (userHasCallLoggingAccess && !UserHasCallLoggingAccessEnabled) {
            return true;
        } else {
            return false;
        }
        */
    }


    public PageReference createCustomerContactTask() {
        try {
            insertCustomerContactTask();
            PageReference pr = new PageReference('/apex/CustomerContact');
            pr.setRedirect(true);
            return pr;
        } catch (DmlException e) {
            ApexPages.addMessages(e);
            return null;
        } catch (Exception e) {
            ApexPages.addMessages(e);
            return null;
        }
    }

    /**
     * <<006>>
     * Insert a new Customer Contact task, either against the customer, case and customer (new case only)
     * or a standalone task.
     * ONLY DO THIS IF ShowTaskRecordPanel = TRUE, I.E. User has CallLoggingAccess permission set.
     * @params: None
     * @rtnval: void
     */
    public void insertCustomerContactTask() {

        if (UserHasCallLoggingAccessEnabled) {
            TaskRecord.Description = CaseRecord.Description;
            TaskRecord.ActivityDate = System.today();
            TaskRecord.CRNumber__c = CaseRecord.jl_CRNumber__c;
            TaskRecord.Delivery_Number__c = CaseRecord.jl_DeliveryNumber__c;
            TaskRecord.Order_Number__c =  CaseRecord.jl_OrderManagementNumber__c;
            TaskRecord.Product_Number__c = CaseRecord.jl_pse1_Stock_Number__c;
            if (ContactRecord != null) {
                TaskRecord.WhoId = ContactRecord.Id;
            }
            if (CaseRecord != null) {
				TaskRecord.Case__c = CaseRecord.Id;
            }
            upsert TaskRecord;
        }
    }

    public void setDefaultInitiatingSource(){

        String userTeam = tempUser.Team__c;

        if(userTeam == null || !userTeam.containsIgnoreCase('Branch')){
            CaseRecord.jl_InitiatingSource__c = 'Contact Centre';
        } else {
            CaseRecord.jl_InitiatingSource__c = 'Branch';
        }   
    }

    public boolean isCreatePseButtonAvailable {

        get {           
            return ContactRecord == null ? false : (ContactRecord.Id != null && !IS_USER_ASSIGNED_TO_CAPITA_PROFILE);
        }
        private set;
    }

    public boolean isCreateCaseEnabled {

        get {   
            return ContactRecord == null ? false : ContactRecord.Id != null;
        }
        private set;
    }
}