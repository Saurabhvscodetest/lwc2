public class CaseDetailPageController {
	
    @AuraEnabled(cacheable=true)
    public static String fetchRelatedCaseRecordId( String liveCTRecId ) {
		String caseIdValue = '';        
        List<LiveChatTranscript> liveChatTSList = [ SELECT CaseId,ChatKey FROM LiveChatTranscript WHERE Id =: liveCTRecId];
        if(liveChatTSList.size()>0) { 
            System.debug(' Transcript' + liveChatTSList[0].CaseId);
            caseIdValue = ''+liveChatTSList[0].CaseId; 
        }
        return caseIdValue;
    }
}