@isTest
private class MyJLChatBotInvocableClassTest {
  private static final String SHOPPER_ID = '34567894561';
  private static final String LOYALTY_MEMBERSHIP_NUMBER = '34567894561';
  private static final String REQUEST_TYPE = 'Replacement Card';
  
  public static List< MyJLChatBotInvocableClass.MyJLChatBotRequest > chatbotInputs = new List< MyJLChatBotInvocableClass.MyJLChatBotRequest > ();

    
    Private static final Contact customer;
    Private static final Case cs;
    static{
    BaseTriggerHandler.addSkipReason(CalloutHandler.SKIP_ALL_NOTIFICATIONS_CODE, 'disable outbound events in this test class');
    }
    @testSetup
    static void setup(){
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        
        List<Loyalty_Account__c> loyaltyAccounts = new List<Loyalty_Account__c>();
		loyaltyAccounts.add(new Loyalty_Account__c(Name = LOYALTY_MEMBERSHIP_NUMBER ,ShopperId__c=SHOPPER_ID,IsActive__c = true,Membership_Number__c = LOYALTY_MEMBERSHIP_NUMBER));
		loyaltyAccounts.add(new Loyalty_Account__c( Name = 'Test Account' ,
													ShopperId__c='12345678910',
													IsActive__c = false,
													Membership_Number__c = '12345678910'));

		insert loyaltyAccounts;
        
		List<Loyalty_Card__c> cards = new List<Loyalty_Card__c>();
		cards.add(new Loyalty_Card__c(Name = LOYALTY_MEMBERSHIP_NUMBER,Disabled__c = false ,card_type__c = MyJL_Const.MY_JL_CARD_TYPE, Loyalty_Account__c = loyaltyAccounts[0].Id));
		cards.add(new Loyalty_Card__c(Name = LOYALTY_MEMBERSHIP_NUMBER,Disabled__c = false ,card_type__c = MyJL_Const.MY_JL_CARD_TYPE, Loyalty_Account__c = loyaltyAccounts[1].Id));

		Database.insert(cards);
	
		MyJLChatBotInvocableClass.MyJLChatBotRequest chatBotRequest = new MyJLChatBotInvocableClass.MyJLChatBotRequest ();
	
		chatBotRequest.loyaltyMembershipNumber = LOYALTY_MEMBERSHIP_NUMBER;
		chatBotRequest.shopperID = SHOPPER_ID;
		chatbotInputs.add (chatBotRequest);
    }
        
  
    @isTest static void testServiceWithNoPartnershipCardNumber() {
    
    Test.startTest();
    chatbotInputs = new List< MyJLChatBotInvocableClass.MyJLChatBotRequest > ();
	MyJLChatBotInvocableClass.MyJLChatBotRequest chatBotRequest = new MyJLChatBotInvocableClass.MyJLChatBotRequest ();
	
	chatBotRequest.loyaltyMembershipNumber = '';
	chatBotRequest.shopperID = SHOPPER_ID;
	chatbotInputs.add (chatBotRequest);
    MyJLChatBotInvocableClass.myJLChatBotInvokeProcess( chatbotInputs );
    Test.stopTest();        
        
    
  }
    
  @isTest static void testServiceWithNoShopperProvided() {


    chatbotInputs = new List< MyJLChatBotInvocableClass.MyJLChatBotRequest > ();
    MyJLChatBotInvocableClass.MyJLChatBotRequest chatBotRequest = new MyJLChatBotInvocableClass.MyJLChatBotRequest ();
	
	chatBotRequest.loyaltyMembershipNumber = LOYALTY_MEMBERSHIP_NUMBER;
	chatBotRequest.shopperID = '';
	chatbotInputs.add (chatBotRequest);
    Test.startTest();
    MyJLChatBotInvocableClass.myJLChatBotInvokeProcess( chatbotInputs );
    Test.stopTest();
      
    chatbotInputs = new List< MyJLChatBotInvocableClass.MyJLChatBotRequest > ();
    MyJLChatBotInvocableClass.MyJLChatBotRequest chatBotRequest1 = new MyJLChatBotInvocableClass.MyJLChatBotRequest ();
	
	chatBotRequest1.loyaltyMembershipNumber = LOYALTY_MEMBERSHIP_NUMBER;
	chatBotRequest1.shopperID = '121212122122112';
	chatbotInputs.add (chatBotRequest1);
	
    MyJLChatBotInvocableClass.myJLChatBotInvokeProcess( chatbotInputs );
    
    chatbotInputs = new List< MyJLChatBotInvocableClass.MyJLChatBotRequest > ();
    MyJLChatBotInvocableClass.MyJLChatBotRequest chatBotRequest2 = new MyJLChatBotInvocableClass.MyJLChatBotRequest ();
	
	chatBotRequest2.loyaltyMembershipNumber = LOYALTY_MEMBERSHIP_NUMBER;
	chatBotRequest2.shopperID = SHOPPER_ID;
	chatbotInputs.add (chatBotRequest2);
	
	Loyalty_Account__c account = [select IsActive__c From Loyalty_Account__c where ShopperId__c = :SHOPPER_ID Limit 1];

    if( account != null){
      account.IsActive__c = false;
      update account;
    }
    
    
    MyJLChatBotInvocableClass.myJLChatBotInvokeProcess( chatbotInputs );
    
  }  
    
    @isTest static void testServiceWithDeactivatedAccount() {

    Loyalty_Account__c account = [select IsActive__c,Membership_Number__c,ShopperId__c From Loyalty_Account__c where IsActive__c = false Limit 1];
	
   
	List< MyJLChatBotInvocableClass.MyJLChatBotRequest > chatbotInputDeactivateList = new List< MyJLChatBotInvocableClass.MyJLChatBotRequest > ();
	
    MyJLChatBotInvocableClass.MyJLChatBotRequest chatBotRequest = new MyJLChatBotInvocableClass.MyJLChatBotRequest ();
	
	chatBotRequest.loyaltyMembershipNumber = account.Membership_Number__c;
	chatBotRequest.shopperID = account.ShopperId__c;
	chatbotInputDeactivateList.add (chatBotRequest);
	
	Test.startTest();
    System.debug(' Shopper Active ' + account.IsActive__c);
    System.debug(' loyaltyMembershipNumber ' + account.Membership_Number__c);
    System.debug(' Shopeer Id  ' + account.ShopperId__c);
    MyJLChatBotInvocableClass.myJLChatBotInvokeProcess( chatbotInputDeactivateList );
    Test.stopTest();
  }
    
    @isTest static void testServiceWithDisabledCard() {
    List<Loyalty_Account__c> accountList = [select id From Loyalty_Account__c where ShopperId__c = :SHOPPER_ID Limit 1];
        for(Loyalty_Account__c lc : accountList){
            lc.Membership_Number__c = LOYALTY_MEMBERSHIP_NUMBER;
        }
        update accountList;
        
    Loyalty_Card__c card = [Select id,Disabled__c, card_type__c from Loyalty_Card__c where Name =:LOYALTY_MEMBERSHIP_NUMBER Limit 1];
        
    if( card != null){
      card.Disabled__c = true;
       update card;      
    }
    
    chatbotInputs = new List< MyJLChatBotInvocableClass.MyJLChatBotRequest > ();
    
    MyJLChatBotInvocableClass.MyJLChatBotRequest chatBotRequest = new MyJLChatBotInvocableClass.MyJLChatBotRequest ();
	
	chatBotRequest.loyaltyMembershipNumber = LOYALTY_MEMBERSHIP_NUMBER;
	chatBotRequest.shopperID = SHOPPER_ID;
	chatbotInputs.add (chatBotRequest);
    Test.startTest();
    MyJLChatBotInvocableClass.myJLChatBotInvokeProcess( chatbotInputs );
    Test.stopTest();
        
        chatbotInputs = new List< MyJLChatBotInvocableClass.MyJLChatBotRequest > ();
	MyJLChatBotInvocableClass.MyJLChatBotRequest chatBotRequest3 = new MyJLChatBotInvocableClass.MyJLChatBotRequest ();

    chatBotRequest3.loyaltyMembershipNumber = '1234567812345678';
	chatBotRequest3.shopperID = SHOPPER_ID;
	chatbotInputs.add (chatBotRequest3);        
    MyJLChatBotInvocableClass.myJLChatBotInvokeProcess( chatbotInputs );
    

  }

  @isTest static void testServiceWithShopper() {
        
   
    	Test.startTest();
        Contact customer = MyJL_TestUtil.createCustomer('Ordering_Customer');
        customer.Shopper_ID__c = SHOPPER_ID;
        update customer;
        
        MyJL_Outbound_Staging__c JL = new MyJL_Outbound_Staging__c();
        
        chatbotInputs = new List< MyJLChatBotInvocableClass.MyJLChatBotRequest > ();
        MyJLChatBotInvocableClass.MyJLChatBotRequest chatBotRequest = new MyJLChatBotInvocableClass.MyJLChatBotRequest ();
	
		chatBotRequest.loyaltyMembershipNumber = LOYALTY_MEMBERSHIP_NUMBER;
		chatBotRequest.shopperID = SHOPPER_ID;
		chatbotInputs.add (chatBotRequest);
		
		List<String> Output = MyJLChatBotInvocableClass.myJLChatBotInvokeProcess( chatbotInputs );
		
		MyJLChatBotResponseModel response = (MyJLChatBotResponseModel)JSON.deserialize( Output [0], MyJLChatBotResponseModel.class);
		
		System.debug ('response' + response.CaseNumbers);

        List<Case> getcase = new list<Case>([SELECT id,despatch_status__c,status,Address_Line_1__c,Address_Line_2__c, Address_Line_3__c,Address_Line_4__c  FROM Case Where CaseNumber =: response.CaseNumbers]);        
        List<MyJL_Outbound_Staging__c> getOutst = new list<MyJL_Outbound_Staging__c>([select id,Case_Reference_ID__c,Address_LIne_1__c, Address_Line_2__c, Address_Line_3__c, Address_Line_4__c  from MyJL_Outbound_Staging__c where Case_Reference_ID__c =:getcase[0].Id]);
        
       
       
    	system.assert(!getcase.isEmpty(), 'Case should be created' );
    	Case actualCase = getcase[0];        
       
        
        system.assertequals(1,getOutst.size());
        MyJL_Outbound_Staging__c outboundStagingRecord = getOutst[0]; 
        
        system.assertequals(customer.Mailing_Street__c,outboundStagingRecord.Address_Line_1__c); 
        system.assertequals(customer.Mailing_Address_Line2__c,outboundStagingRecord.Address_Line_2__c); 
        system.assertequals(customer.Mailing_Address_Line3__c,outboundStagingRecord.Address_Line_3__c); 
        system.assertequals(customer.Mailing_Address_Line4__c,outboundStagingRecord.Address_Line_4__c); 
  
    Test.stopTest();

  }

  @isTest static void testServiceForCatchBlock() {

    Test.startTest();
    chatbotInputs = new List< MyJLChatBotInvocableClass.MyJLChatBotRequest > ();
    MyJLChatBotInvocableClass.MyJLChatBotRequest chatBotRequest = new MyJLChatBotInvocableClass.MyJLChatBotRequest ();
	
	chatBotRequest.loyaltyMembershipNumber = LOYALTY_MEMBERSHIP_NUMBER;
	chatBotRequest.shopperID = SHOPPER_ID;
	chatbotInputs.add (chatBotRequest);
	
    MyJLChatBotInvocableClass.myJLChatBotInvokeProcess( chatbotInputs );
    Test.stopTest();
  }
    @isTest static void testServiceWithEmptyAccount() {

    Loyalty_Account__c account = [select IsActive__c From Loyalty_Account__c where ShopperId__c = :SHOPPER_ID Limit 1];

    if( account != null){
      account.IsActive__c = false;
      update account;
    }
    chatbotInputs = new List< MyJLChatBotInvocableClass.MyJLChatBotRequest > ();
    
    MyJLChatBotInvocableClass.MyJLChatBotRequest chatBotRequest1 = new MyJLChatBotInvocableClass.MyJLChatBotRequest ();
	
	chatBotRequest1.loyaltyMembershipNumber = LOYALTY_MEMBERSHIP_NUMBER;
	chatBotRequest1.shopperID = SHOPPER_ID;
	chatbotInputs.add (chatBotRequest1);
	
    Test.startTest();
    MyJLChatBotInvocableClass.myJLChatBotInvokeProcess( chatbotInputs );
    Test.stopTest();
  
  }
    @isTest static void testServiceWithMultipleCard() {
          
    Loyalty_Card__c card = [Select id,Disabled__c, card_type__c from Loyalty_Card__c where Name =:LOYALTY_MEMBERSHIP_NUMBER Limit 1];
        
    if( card != null && card.Card_Type__c == 'Replacement Card'){
      card.Disabled__c = true;
       update card;      
    }
    
    chatbotInputs = new List< MyJLChatBotInvocableClass.MyJLChatBotRequest > ();
	MyJLChatBotInvocableClass.MyJLChatBotRequest chatBotRequest2 = new MyJLChatBotInvocableClass.MyJLChatBotRequest ();

    chatBotRequest2.loyaltyMembershipNumber = LOYALTY_MEMBERSHIP_NUMBER;
	chatBotRequest2.shopperID = SHOPPER_ID;
	chatbotInputs.add (chatBotRequest2);        
    Test.startTest();
    MyJLChatBotInvocableClass.myJLChatBotInvokeProcess( chatbotInputs );
    MyJLChatBotResponseModel responseJSON = new MyJLChatBotResponseModel();
	responseJSON.setFailureResponse( 500, 'Error Occured While inserting My Outbound Message records ', 'Error' ); 

    Test.stopTest();

  }
}