/* Holds the EventHub Constants */
public class EHConstants {
    public static final String DT_ERROR_DESC_TECHNICAL = getValue('DT_ERROR_DESC_TECHNICAL',  'Connex is unable to retrieve the delivery information from Event Hub at this time. Please try again and if error persists contact your System Support');
    public static final String DT_ERROR_DESC_BUSSINESS = getValue('DT_ERROR_DESC_BUSSINESS', 'Delivery Tracking is unable to retrieve the required information at this time. Please try again and if the error persists contact system support');
    public static final String RT_ERROR_MESSAGE_TECHNICAL = getValue('RT_ERROR_MESSAGE_TECHNICAL', 'Returns Tracking is unable to retrieve the required information at this time. Please try again and if the error persists contact system support');
    public static final String RT_ERROR_MESSAGE_BUSINESS = getValue('RT_ERROR_MESSAGE_BUSINESS', 'Returns Tracking cannot locate the details of the return requested. Please check the number and try again');
    public static final String RT_MESSAGE_PROVIDE_ATLEAST_ONE_INPUT	 = getValue('RT_MESSAGE_PROVIDE_ATLEAST_ONE_INPUT', 'Please enter an Order ID or Tracking ID');
    public static final String RT_MESSAGE_PROVIDE_ONLY_ONE_INPUT = getValue('RT_MESSAGE_PROVIDE_ONLY_ONE_INPUT', 'Please enter either an Order ID or Tracking ID');
    public static final String DT_MESSAGE_PROVIDE_ATLEAST_ONE_INPUT		 = getValue('DT_MESSAGE_PROVIDE_ATLEAST_ONE_INPUT', 'Please enter an Order ID or Delivery ID');
    public static final String DT_MESSAGE_PROVIDE_ONLY_ONE_INPUT = getValue('DT_MESSAGE_PROVIDE_ONLY_ONE_INPUT', 'Please enter either an Order ID or Delivery ID');
    public static final String RECORD_TYPE_CONTACT_SUPERSEDED = 'Superseded';
	public static final String DUPLICATE_VALUE = getValue('DUPLICATE_VALUE', 'DUPLICATE_VALUE');
    /* GVF Delivery Type */
    public static final String GVF = getValue('GVF', '2ManGVF');
    /* Click and Collect Delivery Type */
    public static final String CLICK_AND_COLLECT = getValue('CLICK_AND_COLLECT', 'ClickAndCollect');
    /* Supplier Direct Delivery Type */
    public static final String SUPPLIER_DIRECT = getValue('SUPPLIER_DIRECT', 'Supplier Direct');
    /*John Lewis Click and Collect */
    public static final String JL_CLICK_AND_COLLECT = getValue('JL_CLICK_AND_COLLECT', 'JLClickAndCollect');
    public static final string ERROR_TYPE_TECHNICAL = getValue('ERROR_TYPE_TECHNICAL', 'TECHNICAL');
    public static final string ERROR_TYPE_BUSINESS = getValue('ERROR_TYPE_BUSINESS', 'BUSINESS');
    public static final string END_POINT = getValue('END_POINT', 'Endpoint__c');
    public static final string TIME_OUT = getValue('TIME_OUT', 'Timeout_Milliseconds__c');
    public static final string CERTIFICATE_NAME = getValue('CERTIFICATE_NAME', 'Certificate_Name__c');
    public static final string EVENT_HUB_GET_EVENTS = getValue('EVENT_HUB_GET_EVENTS', 'EVENT_HUB_GET_EVENTS');
    public static final String CONTENT_TYPE = getValue('CONTENT_TYPE', 'Content-type');
    public static final String GET_HTTP_METHOD =getValue('GET_HTTP_METHOD', 'GET');
    public static final String CONTENT_TYPE_JSON = getValue('CONTENT_TYPE_JSON', 'application/json');
    public static final String CONNECTION = getValue('CONNECTION', 'Connection');
    public static final String MESSAGE_ID = getValue('MESSAGE_ID', 'Message-Id');
    public static final String TRACKING_ID = getValue('TRACKING_ID', 'Tracking-Id');
    public static final String SENDER_ID = getValue('SENDER_ID', 'Sender');
    public static final String CHANNEL_ID = getValue('CHANNEL_ID', 'Channel');
    public static final String SENDER_ID_VAL = getValue('SENDER_ID_VAL', 'FulfilmentEvents-CNX');
    public static final String SPLITTER = getValue('SPLITTER', '-');
    public static final String INPUT_PARAM_PREFIX = getValue('INPUT_PARAM_PREFIX', '?');
    public static final String INPUT_PARAM_SEPERATOR = getValue('INPUT_PARAM_SEPERATOR', '&');
    public static final String REQUEST_PARAM = getValue('REQUEST_PARAM', '=');
    public static final String ORDER_ID = getValue('ORDER_ID', 'orderId');
    public static final String DELIVERY_ID = getValue('DELIVERY_ID', 'deliveryId');
    public static final String PACKAGE_ID = getValue('PACKAGE_ID', 'packageId');
    public static final String NEW_LINE = getValue('NEW_LINE', '\n\r');
    public static final String CONNECTION_VAL = getValue('CONNECTION_VAL', 'close');
    public static final String RESPONSE_STATUS = getValue('RESPONSE_STATUS', 'REPONSE STATUS CODE');
    public static final String RESPONSE_HEADER = getValue('RESPONSE_HEADER', 'REPONSE HEADER');
    public static final String RESPONSE_BODY = getValue('RESPONSE_BODY', 'REPONSE BODY');
    public static final String ERROR_MESSAGE_CONNEX_TO_ESB = getValue('ERROR_MESSAGE_CONNEX_TO_ESB', 'Failure between Connex and ESB');
    public static final String ERROR_MESSAGE_ESB_TO_EVENTHUB = getValue('ERROR_MESSAGE_ESB_TO_EVENTHUB', 'Failure between ESB and Event Hub');
    public static final String ERROR_MESSAGE_JSON_PARSING = getValue('ERROR_MESSAGE_JSON_PARSING', 'Error in processing JSON response from Event Hub');
    public static final String ERROR_MESSAGE_UNEXPECTED = getValue('ERROR_MESSAGE_UNEXPECTED', 'Error in processing inputs from Event Hub into Connex');
    public static final String EMAIL_SUBJECT = getValue('EMAIL_SUBJECT', 'Delivery Tracking - Internal Error');
    public static final String EMAIL_REQ_HEADING = getValue('EMAIL_REQ_HEADING', 'Request Details:');
    public static final String EMAIL_ERROR_HEADING = getValue('EMAIL_ERROR_HEADING', 'Error Details:');
    public static final String EMAIL_HEADER_MESSAGE =getValue('EMAIL_HEADER_MESSAGE', 'Error encountered in when quering Delivery Details on EventHub - please report to development team.');
    public static final String TIME_FORMAT = getValue('TIME_FORMAT', '');
    public static final String RECORD_TYPE_TASK_EVENT_HUB_GVF = getValue('RECORD_TYPE_TASK_EVENT_HUB_GVF',  'EventHub CC');
    public static final String RECORD_TYPE_TASK_EVENT_HUB_CC = getValue('RECORD_TYPE_TASK_EVENT_HUB_CC',  'EventHub GVF');
    public static final String RECORD_TYPE_TASK_EVENT_HUB_SD = getValue('RECORD_TYPE_TASK_EVENT_HUB_SD',  'EventHub SupplierDirect');
    public static final String EVENT_HUB = getValue('EVENT_HUB',  'EventHub');
    public static final String RECORD_TYPE_CASE_QUERY = getValue('RECORD_TYPE_CASE_QUERY',  'Query');
    public static final String RECORD_TYPE_CASE_COMPLAINT = getValue('RECORD_TYPE_CASE_COMPLAINT',  'Complaint');
    public static final String CASE_LITERAL = getValue('CASE_LITERAL', 'Case');
    public static final String CC_INFO = getValue('CC_INFO', 'Click And Collect Info');
    public static final String SUPPLIER_DIRECT_INFO = getValue('SUPPLIER_DIRECT_INFO', 'Supplier Direct Info');
    public static final String GVF_INFO = getValue('GVF_INFO', 'GVF Info');
    public static final String STATUS_NEW = getValue('STATUS_NEW', 'New');
    public static final String REOPENED_STATUS = getValue('REOPENED_STATUS', 'Reopened');
    public static final String RESPONSE_SUCCESS = getValue('RESPONSE_SUCCESS', 'SUCCESS');
    public static final String RESPONSE_ERROR = getValue('RESPONSE_ERROR', 'ERROR');
    public static final String MANDATORY_PARAMETER_MISSING =  getValue('MANDATORY_PARAMETER_MISSING', 'Mandatory Parameter Missing -');
    public static final String EXCEPTION_REQUEST = getValue('EXCEPTION_REQUEST', 'EXCEPTION_REQUEST');
    public static final String EXCEPTION_ID = getValue('EXCEPTION_ID', 'EXCEPTION_ID');
    public static final String EXCEPTION_NAME = getValue('EXCEPTION_NAME', 'EXCEPTION_NAME');
    public static final String DELIVERY_TYPE = getValue('DELIVERY_TYPE', 'DELIVERY_TYPE');
    public static final String INVALID_PARAMETER = getValue('INVALID_PARAMETER', 'Invalid Parameter');
    public static final String CARRIER_TEAM = getValue('CARRIER_TEAM', 'CARRIER_TEAM');
    public static final String PACKAGE_NO = getValue('PACKAGE_NO', 'PACKAGE_NO');
    public static final String DELIVERY_SLOT_TO =getValue( 'DELIVERY_SLOT_TO', 'DELIVERY SLOT TO');
    public static final String DELIVERY_CDH = getValue('DELIVERY_CDH', 'DELIVERY CDH');
    public static final String NEW_CASE_CREATED = getValue('NEW_CASE_CREATED', 'New case created');
    //public static final String NEW_CASE_DESCRIPTION = getValue('NEW_CASE_DESCRIPTION', '');
    public static final String CASE_CREATED_DAYS_CRITERIA_VAL = getValue('CASE_CREATED_DAYS_CRITERIA', '28');
    public static Integer CASE_CREATED_DAYS_CRITERIA = Integer.valueOf(CASE_CREATED_DAYS_CRITERIA_VAL);
    public static final String SOURCE = getValue('SOURCE', 'MODE OF TRANSPORT');
    public static final String JL_ONLINE = getValue('JL_ONLINE', 'JL.COM');
    public static final String DATE_TIME_FORMAT = getValue('DATE_TIME_FORMAT', 'dd MMM yyyy HH:mm');
    public static final String DATE_FORMAT = getValue('DATE_FORMAT', 'dd MMM yyyy');
    public static final String TIME_ZONE = getValue('TIME_ZONE', 'UTC');
    public static final String DATE_REPLACE_LITERAL = getValue('DATE_REPLACE_LITERAL', 'T');
    public static final Integer NO_DATA_FOUND_STATUS_CODE = Integer.valueOf(getValue('NO_DATA_FOUND_STATUS_CODE', '404'));
    public static final Integer SUCCESS_STATUS = Integer.valueOf(getValue('SUCCESS_STATUS', '200'));
    
    //Gets the default value if the value is null
    public static String getValue(String constantRecordKey , String defaultValue){
    	return CommonStaticUtils.getValue(constantRecordKey, defaultValue);
    }
}