/* Controller for handling the Delivery tracking. */
public  class DeliveryTrackingController {
    public static final string DELIVERY_TRACKING_MOCK_ENABLED = 'DELIVERY TRACKING MOCK ENABLED';
    
    /* Error Message to be displayed on the screen */
    public String errorMessage {get; set;}
    
    /* Order Id - for the request */
    public String orderId {get; set;}
    
    /* delivery Id - for the request */
    public String deliveryId {get; set;}
    
    /* Package Id - for the request */
    public String trackingId {get; set;}
	
	/* Delivery Tracking Information */    
    public DeliveryTrackingVO delTrackingVO {get; set;}
    
    public String selectedVal{
    	get{
    		String val = '';
    		if(String.isBlank(this.selectedVal) && (delTrackingVO != null) ){
    			Map<String, String> fullfillmentInfoMap = delTrackingVO.getFullfillmentInfo();
    			for(String key : fullfillmentInfoMap.keySet()){
    				//get the first element
    				val= key;
    				break;
    			}
    		} else {
    			val = this.selectedVal;
    		}
    		return val;
    	}
    	set;
    }  // This will hold the selected value

	public List<SelectOption> fullfillmentInfo{
		get {
	        List<SelectOption> optns = new List<Selectoption>();
	        // before getting here you must populate your queryResult list with required fields
	        if(delTrackingVO != null){
	        	Map<String, String> fullfillmentInfoMap = delTrackingVO.getFullfillmentInfo();
		        for(String key : fullfillmentInfoMap.keySet()){
		           optns.add(new selectOption(key, fullfillmentInfoMap.get(key)));
		        }
		        if(!optns.isEmpty()) {
		        	currentFulfillment = DeliveryTrackingVO.getFullfillmentType(optns[0].getValue());
		        	currentFulfillmentIndex = 0;
		        }            
	        }
		    return optns;
		}
		set;
	}
	
	public String currentFulfillment {
		get{
			return DeliveryTrackingVO.getFullfillmentType(selectedVal);
		}
		set;
	}
	
	public Integer currentFulfillmentIndex {
		get{
	    	return DeliveryTrackingVO.getFullfillmentTypeIndex(selectedVal);
		}
		set;
	}
	
    
    /* Get Events Service */
    EHGetEventsInterface service = null;
        
    public DeliveryTrackingController(){
    	//Check if the service mock is enabled and populate the appropriate service. 
        Config_Settings__c mockConfig = Config_Settings__c.getInstance(DELIVERY_TRACKING_MOCK_ENABLED);
        service = (mockConfig != null && mockConfig.Checkbox_Value__c) ? (EHGetEventsInterface)new DeliveryTrackingVOUserInputMock(): (EHGetEventsInterface)new EHGetEventsService();
    }
    

    public DeliveryTrackingVO searchDeliveryLex(String orderId, String deliveryId){
        delTrackingVO = service.getDeliveryTrackingInfo(orderId, deliveryId);
   		return delTrackingVO;
      }
    
    public void searchDelivery() {
		resetDeliveryDetails();
        errorMessage  = getDeliveryTrackingErrorMessage(orderId, deliveryId);
        if(String.isNotBlank(errorMessage)) {
             delTrackingVO = null;
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage));
             return;
        }
          
        delTrackingVO = service.getDeliveryTrackingInfo(orderId, deliveryId);
        system.debug('DELIVERY TRACKING INFORMATION' + delTrackingVO);
		errorMessage  = null;
        //Check if the response has any errors.
        if(!delTrackingVO.success) {
        	errorMessage = (EHConstants.ERROR_TYPE_BUSINESS.equalsIgnoreCase(delTrackingVO.errorInfo.errorType) ? EHConstants.DT_ERROR_DESC_BUSSINESS: EHConstants.DT_ERROR_DESC_TECHNICAL)  ;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage));
        }
    }
    
    public void searchReturns() {
        resetDeliveryDetails();
        errorMessage  = getReturnsTrackingErrorMessage(orderId, trackingId);
        if(String.isNotBlank(errorMessage)) {
             delTrackingVO = null;
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage));
             return;
        }
        
        delTrackingVO = service.getReturnsTrackingInfo(orderId, trackingId);
        system.debug('RETURNS TRACKING INFORMATION' + delTrackingVO);
        //Check if the response has any errors.
        errorMessage  = null;
        if(!delTrackingVO.success) {
        	errorMessage = (EHConstants.ERROR_TYPE_BUSINESS.equalsIgnoreCase(delTrackingVO.errorInfo.errorType) ? EHConstants.RT_ERROR_MESSAGE_BUSINESS: EHConstants.RT_ERROR_MESSAGE_TECHNICAL)  ;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage));  
        }
    }
    
    public string getGVFScreenVal(){ return DeliveryTrackingVO.GVF_SCREEN; }
    public string getCCScreenVal(){ return DeliveryTrackingVO.CC_SCREEN; }
    public string getCarrierScreenVal(){ return DeliveryTrackingVO.CARRIER_SCREEN; }
    public string getSplitter(){ return DeliveryTrackingVO.SEPARATOR; }
    public string getSupplierDirectScreenVal(){ return DeliveryTrackingVO.SUPPLIER_DIRECT; }
    
    //remove after october 2017 release
    public string getDefaultScreenVal(){ return ''; }

    /* Reset all the Delivery data. */
    public void resetDeliveryDetails() {
        fullfillmentInfo.clear();
        selectedVal = null;
        currentFulfillment = null;
        currentFulfillmentIndex = null;
    }
    /* Reset all the data. */
    public void clearAllData() {
    	resetDeliveryDetails();
        delTrackingVO = null;
        fullfillmentInfo = null;
        deliveryId = null;
        orderId = null;
        trackingId = null;
        errorMessage  = null;
    }
    
       /* Handle the validations on the Inputs and populate the error message if required */
    public static String getDeliveryTrackingErrorMessage(String orderId, String deliveryId) {
    	String errorMessage = null;
        if(String.isBlank(orderId) && (String.isBlank(deliveryId))){
           errorMessage =  EHConstants.DT_MESSAGE_PROVIDE_ATLEAST_ONE_INPUT;          
        } else if ((String.isNotBlank(orderId) && (String.isNotBlank(deliveryId)))) {
           errorMessage =  EHConstants.DT_MESSAGE_PROVIDE_ONLY_ONE_INPUT;
        } 
        return errorMessage;
    }
    
    /* Handle the validations on the Inputs and populate the error message if required */
    public static String getReturnsTrackingErrorMessage(String orderId, String trackingId) {
    	String errorMessage = null;
        if(String.isBlank(orderId) && (String.isBlank(trackingId))){
           errorMessage =  EHConstants.RT_MESSAGE_PROVIDE_ATLEAST_ONE_INPUT;          
        } else if ((String.isNotBlank(orderId) && (String.isNotBlank(trackingId)))) {
           errorMessage =  EHConstants.RT_MESSAGE_PROVIDE_ONLY_ONE_INPUT;
        } 
        return errorMessage;
    }

}