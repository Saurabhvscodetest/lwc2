@isTest
public class InFlightCaseTest {
	public static final String DATE_TYPE = 'RANDOM';
	
	static testMethod void testInflightCasesSorting() {
		InFlightCase obj1 = new InFlightCase('2', dateTime.now().addminutes(5), DATE_TYPE);
		InFlightCase obj2 = new InFlightCase( '1', dateTime.now(), DATE_TYPE);
		InFlightCase obj3 = new InFlightCase( '3', dateTime.now().addminutes(-5), DATE_TYPE);
		List<InFlightCase> inFlightCaseList = new List<InFlightCase>{obj1,obj3, obj2};
		inFlightCaseList.sort();
		System.assertEquals(obj3, inFlightCaseList.get(0));
		System.assertEquals(obj2, inFlightCaseList.get(1));
		System.assertEquals(obj1, inFlightCaseList.get(2));
	}
	
	
	static testMethod void testInflightComparisions() {
		Datetime testTime = Datetime.now();
		InFlightCase obj1 = new InFlightCase('1', testTime, DATE_TYPE);
		InFlightCase obj2 = new InFlightCase( '1', testTime, DATE_TYPE);
		InFlightCase obj4 = new InFlightCase( '1', testTime.addminutes(5), DATE_TYPE);
		System.assertEquals(obj1, obj2);
		System.assert(obj1 != obj4);
	}
    
}