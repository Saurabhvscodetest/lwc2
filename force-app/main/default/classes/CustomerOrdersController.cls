/// Controller claas for that CustomerOrders VF Page 
//		to display Orders related to this customer 
//
// Edit    Date        By            Comments
//  001    28/05/14    Vikram Middha    Initial Version

public with sharing class CustomerOrdersController {

	public List<CustomerOrder> orderList {get;set;}
	public String orderId = '';
	public CustomerOrder selectedOrder {get;set;}
	
	public CustomerOrdersController(){
		orderId = ApexPages.currentPage().getParameters().get('orderId');
		init();
	}

	public CustomerOrdersController(ApexPages.StandardController std) {

		init();
		
	}


	public void init(){
		
		// Order Line Item Page is calling this function. 
		if(orderId != ''){
			selectedOrder = CustomerOrderWSHandler.getOrderLineItems();
		}else{
			orderList = CustomerOrderWSHandler.getCustomerOrders();
		}
		
	}

}