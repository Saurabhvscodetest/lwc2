//------------------------------------------------------------------------------
// An apex page controller that Truster User/Agent Registration functionality
// @author : Usha Panwar  - 06/05/2014 

/* EDIT RECORD

LOG     DATE        Author	JIRA	COMMENT      
001     MAY/2015    ES      		CAPITA - DROP 1 - Introduced Simplified Password Reset for CAPITA end users
002     02/06/15    ES      		Identifying points in the code where Simplified ACTIVATION/RE-ACTIVATION for CAPITA needs to be relaxed
003     12/06/15    MP		CMP-263 Implementation of 002 above.


*/
public class JLActivationController {
    
    
    public String firstName { get; set; }
    public String lastName { get; set; }
    public boolean confirmUser{get; set; }
    public String partnerId { get; set; }
    public boolean IsEditMode{get; set;}
    public String question{get; set;}
    public String answer{get; set;}
    public String agentUsername {get; set;}
    public String agentpassword {get;set;}
    public boolean useSimplifiedPasswordReset {get; private set;}
    
    private List<User> jlUser;
    
    private String loggedinUserName;
    
    private integer counter =1 ;
    

    
    public JLActivationController(){
        IsEditMode = false;
        counter =1;
    }
    
    // to show the Agent screen
    public PageReference continueToAgentScreen(){
        
        
            system.debug('value of checkbox is :' + confirmUser);
            
                
            if(firstName != null || lastName != null || confirmUser != false ){
            
            IsEditMode = true;
            
            
          }if (!confirmUser){
            
            IsEditMode = false;
            
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, system.label.ActSite_user_confirm));  
          
          }
        return null;    
    }
    
    public PageReference checkUserDetails(){
        
        system.debug('first name is :' + firstName);
        system.debug('last name is :' + lastName);
        system.debug('partner id is :' + partnerId);
        
        PageReference ref = null;
        //ES:02:06:2015
        // HERE IS THE LOGIC THAT NEEDS TO BE IMPLEMENTED
        // IF CURRENT USER IS A CAPITA TEAM MANAGER AND the USER BEING ACTIVATED/RE-ACTIVATED IS A CAPITA USER (HAS CAPITA PROFILE)
        // THEN RUN THE PHASE 1 SOQL 
        // OTHERWISE RUN THE CAPITA DROP 2 SOQL 

        //THIS IS THE PHASE 1 SOQL//
        /*
        jlUser =
        [
            SELECT Id, UserName, Title, FirstName, LastName,Trusted_User__c,Net_Dom_ID__c,IsActive,SSO_Security_Question__c,SSO_Answer__c,
            SST_UserName__c,SST_DateTime__c,Lock_Agent__c,ProfileId  FROM User
            WHERE FirstName = :firstName and LastName = :lastName and Net_Dom_ID__c =:partnerId.toUpperCase() and IsActive = true and Lock_Agent__c = false
        ];
        */

         //THIS IS THE CAPITA DROP 2 SOQL//
        jlUser =
        [
            SELECT Id, UserName, Title, FirstName, LastName,Trusted_User__c,Net_Dom_ID__c,IsActive,SSO_Security_Question__c,SSO_Answer__c,
            SST_UserName__c,SST_DateTime__c,Lock_Agent__c,ProfileId  FROM User
            WHERE FirstName = :firstName and LastName = :lastName and Net_Dom_ID__c =:partnerId.toUpperCase() and IsActive = true
        ];
        

        // PHASE 1 CODE RESUMES

        // No user exists in the system
        
        // <<003>> Code with some reformatting of existing code
        if (jlUser.size() < 1){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, system.label.ActSite_user_no_user_exists));  
        } else {
			User jlUserRec = jlUser.get(0);
			Boolean isLockedAgent = jlUserRec.Lock_Agent__c;
			// If the user being activated and the activating user are both CAPITA we can skip the security question validation
			Boolean isCapitaCurrentUser = JL_Activation_Unlock_Users__c.getInstance().Allow_Unlock__c;
			Boolean isCapitaAgent = JL_Activation_Unlock_Users__c.getInstance(jlUserRec.Id).Allow_Unlock__c;
			Boolean isCapita = isCapitaCurrentUser && isCapitaAgent;
			
			if (!isCapita && isLockedAgent) {
				// These would have originally been excluded by phase 1 SOQL so put that logic back in
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, system.label.ActSite_user_no_user_exists));  
			} else {
				if (isCapita || (jlUserRec.SSO_Security_Question__c == null && jlUserRec.SSO_Answer__c == null)) {
					ref = Page.JLAgentManageSecurity;
				} else {
					ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, system.label.ActSite_user_security_question_answered));
				}
			} // <<003>> End
              
              //ES:02/06/2015 - BY PASSING security question check and let password reset continue
              
              //TEMPORARILY  allow to proceed to next page
              // <<003>> comment out ref = Page.JLAgentManageSecurity;

              //ES:02/06/2015  - ENDS


              /* 02/06/2015 - THIS WAS THE PHASE 1 CODE. NOTE
              THAT THE CHECK FOR SECURITY QUESTION BELOW NEEDS TO BE CARRIED OUT FOR PHASE 1 USERS
              BUT NOT FOR CAPITA USERS (E.G. CAPITA MANAGER ACTIVATING/RE-ACTIVATING CAPITA AGENT)

              //PHASE 1 CODE HERE
              
 
               if(jlUser.get(0).SSO_Security_Question__c == null &&  jlUser.get(0).SSO_Answer__c == null)
                {
                    
                    ref = Page.JLAgentManageSecurity;
                    
                }else {
                    
                    ApexPages.addMessage
                    (
                            new ApexPages.Message
                            (
                                    ApexPages.Severity.Error,
                                    system.label.ActSite_user_security_question_answered
                            )
                    );  
            
                ref = null;
                    
                    
                
                }
                */
        }
        return ref;
    }
    
    
        //--------------------------------------------------------------------------
    // Get available questions
    //--------------------------------------------------------------------------
    public  List<SelectOption> getSecurityQuestions()
    {
        List<SelectOption> securityQuestions = new List<SelectOption>();
        securityQuestions.add(new SelectOption('', '---None---'));
        securityQuestions.add(new SelectOption(system.label.ActSite_pet_name, system.label.ActSite_pet_name));
        securityQuestions.add(new SelectOption(system.label.ActSite_maiden_name, system.label.ActSite_maiden_name));
        securityQuestions.add(new SelectOption(system.label.ActSite_city_born, system.label.ActSite_city_born));
        securityQuestions.add(new SelectOption(system.label.ActSite_city_job, system.label.ActSite_city_job));
        securityQuestions.add(new SelectOption(system.label.ActSite_facourite_friend, system.label.ActSite_facourite_friend));
        securityQuestions.add(new SelectOption(system.label.ActSite_meet_your_spouse, system.label.ActSite_meet_your_spouse));
        securityQuestions.add(new SelectOption(system.label.ActSite_childhood_nickname, system.label.ActSite_childhood_nickname));
        
        
        return securityQuestions;
    }
    
    public PageReference saveUserSecurityDetails(){
    
        system.debug('security question selected by user is :' + question);
        system.debug('security answer entered by user is :' + answer);
        
        id agentId = null;
        
        
        // fetch the logged in user details
        
        loggedinUserName = UserInfo.getUserName();
        
        system.debug('logged in user detail is :' + loggedinUserName);
        
        if(question ==  null ){
            
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Please select a Security Question.'));  
                
          
          return null;
        
        }
        else {
            
                 // Update the Agents security question and answer along with SST details for audit trail
                
                for(User user : jlUser){
                    
                    system.debug('security question is :' + question);
                    
                    user.SSO_Security_Question__c = question;
                    user.SSO_Answer__c = answer;
                    user.SST_UserName__c =loggedinUserName;
                    user.SST_DateTime__c = Datetime.now();
                    
                    agentUsername =  user.Username;
                    agentId= user.id;
                    
                    System.debug('agent username is :' + agentUsername);
                }
                 update jlUser;
                 
                 
                 System.ResetPasswordResult rpr = null;
            
                rpr = system.resetPassword(agentId, false);
                
                agentpassword= rpr.getPassword();
                
                system.debug('reset password is :' + rpr.getPassword());
                
                return Page.JLAgentUsername; 
        
        
        }
        
    }
    
    
    
    
        public PageReference checkAgentDetails(){
        
        system.debug('first name is :' + firstName);
        system.debug('last name is :' + lastName);
        system.debug('partner id is :' + partnerId);
        
        PageReference ref = null;
        
        /* 02/06/2015-ES - TEMPORARILY REMOVING LOCK AGENT CHECK OTHERWISE, USER ACTIVATION WILL NOT RUN
        THIS IS THE PHASE 1 SOQOL
        jlUser =
        [
            SELECT Id, UserName, Title, FirstName, LastName,Trusted_User__c,Net_Dom_ID__c,IsActive,SSO_Security_Question__c,SSO_Answer__c,
            SST_UserName__c,SST_DateTime__c,Lock_Agent__c,Lock_Agent_DateTime__c FROM User
            WHERE FirstName = :firstName and LastName = :lastName and Net_Dom_ID__c =:partnerId.toUpperCase() and IsActive = true  and Lock_Agent__c = false
        ];
        */
        //THIS IS THE CAPITA SOQL

        jlUser =
        [
            SELECT Id, UserName, Title, FirstName, LastName,Trusted_User__c,Net_Dom_ID__c,IsActive,SSO_Security_Question__c,SSO_Answer__c,
            SST_UserName__c,SST_DateTime__c,Lock_Agent__c,Lock_Agent_DateTime__c FROM User
            WHERE FirstName = :firstName and LastName = :lastName and Net_Dom_ID__c =:partnerId.toUpperCase() and IsActive = true
        ];

        //THE ABOVE LOGIC SHOULD BE RE-IMPLEMENTED AS FOLLOWS IF
        //IF CURRENT USER IS IN-HOUSE (PHASE 1) MANAGER THEN RUN ABOVE PHASE 1 SOQL
        //IF CURRENT USER IS CAPITA MANAGER UNLOCKING CAPITA AGENT THEN RUN CAPITA SOQL

        //02:06:2015 END

        //PHASE 1 CODE RESUMES

        // No user exists in the system
        
        if(jlUser.size() < 1){
            
            
            
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,
                                // system.label.ActSite_three_attempts_failed [commented out by Kevin Burke on behalf of JJ - wrong label used]
                                system.label.ActSite_user_no_user_exists
                        ));  
                
          
          ref = null;
            
        
        }else {

            useSimplifiedPasswordReset = Password_Reset_Settings__c.getInstance(jlUser[0].id).Use_Simplified_Password_Reset__c;
            question = jlUser[0].SSO_Security_Question__c ;
            ref = Page.JLAgentResetSecurity;
        
        }
        
        return  ref;
    }
    
        
         public PageReference verifyAgentSecurityDetails(){
            
            PageReference ref = null;
            
            
            Boolean matches =  false;
            
                //
                // Remove white space
                //
                String userAnswer = answer.replaceAll('\\s+', '');
                String correctAnswer = jlUser[0].SSO_Answer__c.replaceAll('\\s+', '');
                
                system.debug('useranswer is :' + userAnswer);
                system.debug('correctAnswer is :' + correctAnswer);
    
                matches = userAnswer.equalsIgnoreCase(correctAnswer);

                String maxattempts = CustomSettingsManager.ActSite_Maximum_Security_Trials;
                
                // starting from 0
                if(counter == CustomSettingsManager.getConfigSettingIntegerVal(maxattempts)){
                
                    IsEditMode = true;
                    
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, system.label.ActSite_three_attempts_failed));
                                    
                    for(User user : jlUser){
            
                        system.debug('security question is :' + question);
                        
                        user.Lock_Agent__c = true;
                        user.Lock_Agent_DateTime__c = Datetime.now();
                        
                        System.debug('agent username is :' + agentUsername);
                        
                        }
                     update jlUser;
                                    
                
                                    
                     
                }
                if(counter ==CustomSettingsManager.getConfigSettingIntegerVal(CustomSettingsManager.ActSite_Maximum_Warning_Trails)){
                
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, system.label.ActSite_agent_trails + ' ' +  CustomSettingsManager.getConfigSettingIntegerVal(CustomSettingsManager.ActSite_Maximum_Warning_Trails) + ' ' +  'of' + ' ' +  CustomSettingsManager.getConfigSettingIntegerVal(maxattempts) + ' ' + system.label.ActSite_agent_maximum_attempts_msg ));

                 }
                 
                 if(counter > CustomSettingsManager.getConfigSettingIntegerVal(CustomSettingsManager.ActSite_Maximum_Warning_Trails)  && (counter < CustomSettingsManager.getConfigSettingIntegerVal(maxattempts))){
                 
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, system.label.ActSite_agent_trails + ' ' + counter + ' ' +  'of' + ' ' +  CustomSettingsManager.getConfigSettingIntegerVal(maxattempts) + ' ' + system.label.ActSite_agent_maximum_attempts_msg ));
                    
                 } 
            
                if (matches &&  counter < CustomSettingsManager.getConfigSettingIntegerVal(CustomSettingsManager.ActSite_Maximum_Security_Trials))
                {
    
                    matches = true;
                    
                    System.ResetPasswordResult rpr = null;
    
                    rpr = system.resetPassword(jlUser[0].id, false);
                    
                    agentpassword= rpr.getPassword();
                    
                    system.debug('reset password is :' + rpr.getPassword());
                    
                    agentUsername = jlUser[0].Username;
                    
                    ref = Page.JLAgentUsernamePassword; 
                } 
                else if(userAnswer == ''){
    
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, system.label.ActSite_user_provide_security));
    
                }else{
    
                    //
                    // Incorrect answer
                    //
                    
                    counter++;
                    
                    system.debug('counter value is :' + counter);
                    
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, system.label.ActSite_security_answer_nomatch));
                }
                
                
               return ref;
                
            }
    

    
            
            
        public PageReference Cancel(){
       
        PageReference ref =  null;
        ref=new PageReference(system.label.ActSite_home_page_url);
      
        ref.setredirect(true);
        return ref;
    }
            
    

}