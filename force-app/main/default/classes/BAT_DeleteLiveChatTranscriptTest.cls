/* Description  : Delete Live chat Transcripts when criteria satisfy in  COPT-4293
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   20/02/2019        Vignesh Kotha                   Created               COPT-4293
*
*/
@IsTest
Public class BAT_DeleteLiveChatTranscriptTest {
    
    static testmethod void testDeleteLiveChatVisitors(){        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        // Running user Context
        System.runAs(runningUser){
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;       
            insert CustomSettingTestDataFactory.createConfigSettings();
        }        
        System.runAs(testUser){            
            contact testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'vignesh@gmail.com';
            insert testContact;    
            
            List<Case> caseList = new List<Case>();
            case testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            caseList.add(testCase); 
            
            case testCase1 = CaseTestDataFactory.createCase(testContact.Id);
            testCase1.Bypass_Standard_Assignment_Rules__c = true;
            caseList.add(testCase1);   
            Insert caseList;
            
            LiveChatVisitor testvisitor=new LiveChatVisitor();
            testvisitor.CreatedDate=System.today();
            insert testvisitor;  
            
            List<LiveChatTranscript> testTranscript = new List<LiveChatTranscript>();
            LiveChatTranscript lCT1 = new LiveChatTranscript(createddate = system.now(),LiveChatVisitorId = testvisitor.Id,CaseId = caseList[0].id);
            LiveChatTranscript lCT2 = new LiveChatTranscript(createddate = system.now(),LiveChatVisitorId = testvisitor.Id,CaseId = caseList[0].id);
            testTranscript.add(lCT1);
            testTranscript.add(lCT2);
            
            Insert testTranscript;  
            
            GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
            gDPR.Name = 'BAT_DeleteCaseRecords';
            gDPR.GDPR_Class_Name__c = 'BAT_DeleteCaseRecords';
            gDPR.Record_Limit__c = '5000'; 
            Insert gDPR;
            GDPR_Record_Limit__c gDPR1 = new GDPR_Record_Limit__c();
            gDPR1.Name = 'BAT_DeleteLiveChatTranscript';
            gDPR1.GDPR_Class_Name__c = 'BAT_DeleteLiveChatTranscript';
            gDPR1.Record_Limit__c = '5000'; 
            Insert gDPR1;
            Test.startTest();
            Database.BatchableContext BC;
            BAT_DeleteLiveChatTranscript obj=new BAT_DeleteLiveChatTranscript();
            obj.start(BC);
            obj.execute(BC,caseList);
            Database.DeleteResult[] Delete_Result = Database.delete(caseList, false);
            obj.finish(BC);
            DmlException expectedException;
            Test.stopTest();
        }  
    }
    
    @isTest
    Public static void testSchedule(){
        GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
        gDPR.Name = 'BAT_DeleteLiveChatTranscript';
        gDPR.GDPR_Class_Name__c = 'BAT_DeleteLiveChatTranscript';
        gDPR.Record_Limit__c = '5000';
        
        GDPR_Record_Limit__c gDPR1 = new GDPR_Record_Limit__c();
        gDPR1.Name = 'BAT_DeleteCaseRecords';
        gDPR1.GDPR_Class_Name__c = 'BAT_DeleteCaseRecords';
        gDPR1.Record_Limit__c = '5000'; 
        
        Test.startTest();
        Insert gDPR1;
        Insert gDPR;
        ScheduleBatchDeleteLiveChatTrans obj = NEW ScheduleBatchDeleteLiveChatTrans();
        obj.execute(null);  
        Test.stopTest();
    } 
    
}