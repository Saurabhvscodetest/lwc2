global class CaseFieldIdWebService{
    // Get object key prefix.
    webservice static String getObjectKeyPrefix(String object_name){
    	if(!String.isEmpty(object_name)){
    		Schema.DescribeSObjectResult[] descResults = Schema.describeSObjects(new String[]{object_name});
    		if(!descResults.isEmpty()){
    			Schema.DescribeSObjectResult descRresult = descResults.iterator().next();
    			return ('/' + descRresult.getKeyPrefix() + '/e?nooverride=1');
    		}
    	}
        return ('/');
    }
     
    // Get field Id of field by Its field label
    webservice static String getFieldId(String object_name, String field_label){
        // Obtain the magic ids
        if(!String.isEmpty(object_name)){
    		Schema.DescribeSObjectResult[] descResults = Schema.describeSObjects(new String[]{object_name});
    		if(!descResults.isEmpty()){
    			Schema.DescribeSObjectResult descRresult = descResults.iterator().next();
    			PageReference p = new PageReference('/' + descRresult.getKeyPrefix() + '/e?nooverride=1');
    			String html;
    			if(Test.isRunningTest()){
    				html = '<label for="test_field_id">'+field_label+'</label>';
    			} else {
    				html = p.getContent().toString();
    			}
		        Map<String, String> labelToId = new Map<String, String>();
		        System.debug('html: '+html);
		        Matcher m = Pattern.compile('<label for="(.*?)">(<span class="requiredMark">\\*</span>)?(.*?)</label>').matcher(html);
		        while (m.find()) {
		            String label = m.group(3);
		            String id = m.group(1);
		            System.debug('label: '+label);
		            System.debug('id: '+id);
		            if(label.equalsIgnoreCase(field_label))
		                return id; // return field Id.
		        }
    		}
        }
        return '';
    }
}