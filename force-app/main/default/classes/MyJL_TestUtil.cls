/**
 *  @author: Andrey Gavrikov (westbrook)
 *  @date: 2014-06-20 11:51:42 
 *  @description:
 *      unit test untilities for MyJL_XXX test classes
 *  
 *  Version History :   
 *       **No Earlier History**
 
          002     11/08/16    YM      As part of OCCO-19503, updating the test data to add Marketing Preferences of the Customer.
 *      
 */
@isTest
public with sharing class MyJL_TestUtil {
    /**
     * @return request with random message Id
     */
    public static SFDCMyJLCustomerTypes.CustomerRequestType prepareBlankRequest() {
        SFDCMyJLCustomerTypes.CustomerRequestType request = new SFDCMyJLCustomerTypes.CustomerRequestType();
        SFDCMyJLCustomerTypes.RequestHeaderType requestHeader = new SFDCMyJLCustomerTypes.RequestHeaderType();
        requestHeader.MessageId = String.valueOf(System.currentTimeMillis());
        request.RequestHeader = requestHeader;
        return request;
    }

    public static SFDCMyJLCustomerTypes.ResultStatus getResponseStatus(SFDCMyJLCustomerTypes.CustomerResponseType response) {
        return response.ResponseHeader.ResultStatus;
    }
    
    public static SFDCMyJLCustomerTypes.CustomerRecordResultType getCustomerResponseStatus(SFDCMyJLCustomerTypes.CustomerResponseType response, Integer index) {
        System.assert(response.ActionRecordResults.size() > index, 'Number of Customer Result records is lower than provided index');
        return response.ActionRecordResults[index];
    }
    
    /**
     *
     * @initialValues - map of initial values. 
     * When null is explicitly provided in the map that means that the key must not be created on the resulting Address instance.
     * e.g. initialValues.put('EmailAddress', 'test@example.com') - will result in 
     *       <sfd1:PartyContactMethod>
     *          <sfd1:EmailAddress>
     *              <sfd1:EmailAddress>update_positive3@test.com</sfd1:EmailAddress>
     *          </sfd1:EmailAddress>
     *       </sfd1:PartyContactMethod>
     * e.g. initialValues.put('EmailAddress', null) - will result in NO xml keys created at all
     */
    public static SFDCMyJLCustomerTypes.Customer getCustomer(final String shopperId, final Map<String, Object> initialValues) {
        SFDCMyJLCustomerTypes.Customer customer = new SFDCMyJLCustomerTypes.Customer();

		MyJL_Util.setShopperId(customer, shopperId);

        if (!isNullProvided(initialValues, 'AnonymousFlag')) {
			customer.AnonymousFlag = (Boolean)initialValues.get('AnonymousFlag');
		}
		//Start - <<002>> ::::  Adding Marketing Preferences for the Customer
		List<SFDCMyJLCustomerTypes.CustomerPreference> customPrefList = new List<SFDCMyJLCustomerTypes.CustomerPreference>();
		if (!isNullProvided(initialValues, 'JohnLewisMarketingPreference')) {
			
			SFDCMyJLCustomerTypes.CustomerPreference custPreference = new SFDCMyJLCustomerTypes.CustomerPreference();
            custPreference.value = String.valueOf(initialValues.get('JohnLewisMarketingPreference'));
            
            SFDCMyJLCustomerTypes.Code 			xRefCode  = new SFDCMyJLCustomerTypes.Code();
			xRefCode.id='JohnLewisMarketingOptIn';
			List<SFDCMyJLCustomerTypes.Code>    xRefCodes = new List<SFDCMyJLCustomerTypes.Code>();
			xRefCodes.add(xRefCode);
			
			SFDCMyJLCustomerTypes.Code preferenceTypeCode = new SFDCMyJLCustomerTypes.Code();
			preferenceTypeCode.xrefCode = xRefCodes;
			
			custPreference.PreferenceTypeCode=preferenceTypeCode;
		    
            customPrefList.add(custPreference);
		    
		    
		}
		if (!isNullProvided(initialValues, 'WaitroseMarketingPreference')) {
			SFDCMyJLCustomerTypes.CustomerPreference custPreference = new SFDCMyJLCustomerTypes.CustomerPreference();
			custPreference.value = String.valueOf(initialValues.get('WaitroseMarketingPreference'));
			SFDCMyJLCustomerTypes.Code 			xRefCode  = new SFDCMyJLCustomerTypes.Code();
			xRefCode.id='WaitroseMarketingOptIn';
			List<SFDCMyJLCustomerTypes.Code>    xRefCodes = new List<SFDCMyJLCustomerTypes.Code>();
			xRefCodes.add(xRefCode);
			
			SFDCMyJLCustomerTypes.Code preferenceTypeCode = new SFDCMyJLCustomerTypes.Code();
			preferenceTypeCode.xrefCode = xRefCodes;
			
			custPreference.PreferenceTypeCode=preferenceTypeCode;
		    
            customPrefList.add(custPreference);
		}

        if (!isNullProvided(initialValues, 'VoucherPreference')) {
            SFDCMyJLCustomerTypes.CustomerPreference custPreference = new SFDCMyJLCustomerTypes.CustomerPreference();
            custPreference.value = String.valueOf(initialValues.get('VoucherPreference'));
            custPreference.valueType = 'String';
            SFDCMyJLCustomerTypes.Code          xRefCode  = new SFDCMyJLCustomerTypes.Code();
            xRefCode.id = MyJL_Const.MY_JL_VOUCHER_PREFERENCE_KEY;
            List<SFDCMyJLCustomerTypes.Code>    xRefCodes = new List<SFDCMyJLCustomerTypes.Code>();
            xRefCodes.add(xRefCode);

            SFDCMyJLCustomerTypes.Code preferenceTypeCode = new SFDCMyJLCustomerTypes.Code();
            preferenceTypeCode.xrefCode = xRefCodes;

            custPreference.PreferenceTypeCode=preferenceTypeCode;

            customPrefList.add(custPreference);
        }

		if (!isNullProvided(initialValues, 'JLFinanceMarketingPreference')) {
			SFDCMyJLCustomerTypes.CustomerPreference custPreference = new SFDCMyJLCustomerTypes.CustomerPreference();
			custPreference.value = String.valueOf(initialValues.get('JLFinanceMarketingPreference'));
			SFDCMyJLCustomerTypes.Code 			xRefCode  = new SFDCMyJLCustomerTypes.Code();
			xRefCode.id='FinanceMarketingOptIn';
			List<SFDCMyJLCustomerTypes.Code>    xRefCodes = new List<SFDCMyJLCustomerTypes.Code>();
			xRefCodes.add(xRefCode);
			
			SFDCMyJLCustomerTypes.Code preferenceTypeCode = new SFDCMyJLCustomerTypes.Code();
			preferenceTypeCode.xrefCode = xRefCodes;
			
			custPreference.PreferenceTypeCode=preferenceTypeCode;
		    
            customPrefList.add(custPreference);
		}
		
		customer.CustomerPreference = customPrefList; 
		//End - <<002>> ::::  Adding Marketing Preferences for the Customer
        if (!isNullProvided(initialValues, 'CustomerGroup')) {
            String groupName = 'Online';
            if (initialValues.containsKey('CustomerGroup')) {
                groupName = (String)initialValues.get('CustomerGroup');
            }       
            //CustomerGroup
            SFDCMyJLCustomerTypes.CustomerGroup custGroup = new SFDCMyJLCustomerTypes.CustomerGroup();
            custGroup.Name = groupName;
            //CustomerAffiliation
            SFDCMyJLCustomerTypes.CustomerAffiliation customerAffiliation = new SFDCMyJLCustomerTypes.CustomerAffiliation();
            customerAffiliation.CustomerGroup = new List<SFDCMyJLCustomerTypes.CustomerGroup>{custGroup};  
            customer.CustomerAffiliation = new List<SFDCMyJLCustomerTypes.CustomerAffiliation>{customerAffiliation};

        }
        
        if (!isNullProvided(initialValues, 'EmailAddress')) {
            String email = null;
            if (initialValues.containsKey('EmailAddress')) {
                email = (String)initialValues.get('EmailAddress');
            }       
            SFDCMyJLCustomerTypes.EmailAddress emailAddress = new SFDCMyJLCustomerTypes.EmailAddress();
            emailAddress.EmailAddress = email;
            
            final List<SFDCMyJLCustomerTypes.PartyContactMethod> partyContactMethods = new List<SFDCMyJLCustomerTypes.PartyContactMethod>();
            SFDCMyJLCustomerTypes.PartyContactMethod emailContactMethod = new SFDCMyJLCustomerTypes.PartyContactMethod();
            emailContactMethod.EmailAddress = emailAddress;
            partyContactMethods.add(emailContactMethod);
            
            customer.PartyContactMethod = partyContactMethods;
        }

        if (!isNullProvided(initialValues, 'TelephoneNumber')) {
            String val = null;
            if (initialValues.containsKey('TelephoneNumber')) {
                val = (String)initialValues.get('TelephoneNumber');
            }       
            SFDCMyJLCustomerTypes.Telephone telephone = new SFDCMyJLCustomerTypes.Telephone();
            telephone.TelephoneNumber = val;

            final List<SFDCMyJLCustomerTypes.PartyContactMethod> partyContactMethods = null;
            if(customer.PartyContactMethod != null) {
                partyContactMethods = customer.PartyContactMethod;
            } else {
                partyContactMethods = new List<SFDCMyJLCustomerTypes.PartyContactMethod>(); 
            }
            
            SFDCMyJLCustomerTypes.Code purposeTypeCode = new SFDCMyJLCustomerTypes.Code();
            purposeTypeCode.Code = 'Primary';

            SFDCMyJLCustomerTypes.Code methodTypeCode = new SFDCMyJLCustomerTypes.Code();
            methodTypeCode.Code = 'Telephone';

            SFDCMyJLCustomerTypes.PartyContactMethod phoneContactMethod = new SFDCMyJLCustomerTypes.PartyContactMethod();
            phoneContactMethod.ContactMethodTypeCode = methodTypeCode;
            phoneContactMethod.ContactPurposeTypeCode = purposeTypeCode;
            phoneContactMethod.Telephone = telephone;

            partyContactMethods.add(phoneContactMethod);
            customer.PartyContactMethod = partyContactMethods;
        }

        SFDCMyJLCustomerTypes.Person person = null;
        if (!isNullProvided(initialValues, 'FirstName')) {
            person = new SFDCMyJLCustomerTypes.Person();
            person.FirstName = (String)initialValues.get('FirstName'); 
        }
        if (!isNullProvided(initialValues, 'LastName')) {
            person = null == person? new SFDCMyJLCustomerTypes.Person() : person;
            person.LastName = (String)initialValues.get('LastName'); 
        }
        if (null != person) {
            customer.Person = person;
        }

        if (!isNullProvided(initialValues, 'CustomerLoyaltyAccountCardId')) {

			if (null == customer.CustomerAccount) {
				customer.CustomerAccount = new List<SFDCMyJLCustomerTypes.CustomerAccount>();
			}
			if (customer.CustomerAccount.isEmpty()) {
				SFDCMyJLCustomerTypes.CustomerAccount account = new SFDCMyJLCustomerTypes.CustomerAccount();
				customer.CustomerAccount.add(account);
			}
			SFDCMyJLCustomerTypes.CustomerAccount account = customer.CustomerAccount[0];
			//Card
			SFDCMyJLCustomerTypes.CustomerLoyaltyAccountCard card = new SFDCMyJLCustomerTypes.CustomerLoyaltyAccountCard();
			account.CustomerLoyaltyAccountCard = new List<SFDCMyJLCustomerTypes.CustomerLoyaltyAccountCard>();
			account.CustomerLoyaltyAccountCard.add(card);
			card.CustomerLoyaltyAccountCardId = (String)initialValues.get('CustomerLoyaltyAccountCardId');
		}

		if (!isNullProvided(initialValues, 'BillingAddress')) {
            String val = null;
            if (initialValues.containsKey('BillingAddress')) {
                val = (String)initialValues.get('BillingAddress');
            }
            
            final List<SFDCMyJLCustomerTypes.PartyContactMethod> partyContactMethods = null;
            if(customer.PartyContactMethod != null) {
                partyContactMethods = customer.PartyContactMethod;
            } else {
                partyContactMethods = new List<SFDCMyJLCustomerTypes.PartyContactMethod>(); 
            }

			//Integer pcmSize = partyContactMethods.size();
			//system.debug('pcmSize: '+pcmSize);

            SFDCMyJLCustomerTypes.Address address = new SFDCMyJLCustomerTypes.Address();
			/*
            if (pcmSize > 0) {
            	address = partyContactMethods[pcmSize-1].Address;
            } else {
	            address = new SFDCMyJLCustomerTypes.Address();
            }
            */
            // this is slight kludge, an address line1 indicates some form of address was supplied      
            //system.debug('address1: '+address);
	        address.AddressLine1 = val;            	            	
            system.debug('address2: '+address);

            SFDCMyJLCustomerTypes.PartyContactMethod addressContactMethod = new SFDCMyJLCustomerTypes.PartyContactMethod();
            addressContactMethod.Address = address;
            
            SFDCMyJLCustomerTypes.Code code = new SFDCMyJLCustomerTypes.Code();
			code.code = 'Billing';

 			addressContactMethod.ContactMethodTypeCode = code;
            partyContactMethods.add(addressContactMethod);
            system.debug('addressContactMethod: '+addressContactMethod);
		}
		
		if (!isNullProvided(initialValues, 'OtherAddress')) {
			// TBD
		}
		
		system.debug('customer: '+customer);
        return customer;
    }

    /**
     * add address to existing customer intance using provided initial values
     * @addressType: Billing | Other 
     * @initialValues - map of initial values. 
     * When null is explicitly provided in the map that means that the key must not be created on the resulting Address instance.
     * e.g. initialValues.put('HouseName', 'seasons') - will result in 
     *      <sfd1:HouseName>seasons</sfd1:HouseName>
     * e.g. initialValues.put('HouseName', null) - will result in NO xml key created at all
     *
     */
    public static SFDCMyJLCustomerTypes.Address addAddress(final SFDCMyJLCustomerTypes.Customer customer, final String addressType, 
                                                        final Map<String, Object> initialValues) {
        SFDCMyJLCustomerTypes.Address address = new SFDCMyJLCustomerTypes.Address();
        final List<SFDCMyJLCustomerTypes.PartyContactMethod> partyContactMethods = null == customer.PartyContactMethod? 
                                                                                    new List<SFDCMyJLCustomerTypes.PartyContactMethod>() : customer.PartyContactMethod;

        SFDCMyJLCustomerTypes.PartyContactMethod partyContactMethod = new SFDCMyJLCustomerTypes.PartyContactMethod();
        partyContactMethods.add(partyContactMethod);
		partyContactMethod.ContactPurposeTypeCode = new SFDCMyJLCustomerTypes.Code();
		partyContactMethod.ContactPurposeTypeCode.code = addressType;
		
		partyContactMethod.ContactMethodTypeCode = new SFDCMyJLCustomerTypes.Code();
		partyContactMethod.ContactMethodTypeCode.code = 'Address';
		
		

        partyContactMethod.Address = new SFDCMyJLCustomerTypes.Address();


        if (!isNullProvided(initialValues, 'HouseNumber')) {
            partyContactMethod.Address.HouseNumber = (Integer)initialvalues.get('HouseNumber');
        }
        if (!isNullProvided(initialValues, 'HouseName')) {
            partyContactMethod.Address.HouseName = (String)initialvalues.get('HouseName');
        }
        if (!isNullProvided(initialValues, 'AddressLine1')) {
            partyContactMethod.Address.AddressLine1 = (String)initialvalues.get('AddressLine1');
        }
        if (!isNullProvided(initialValues, 'AddressLine2')) {
            partyContactMethod.Address.AddressLine2 = (String)initialvalues.get('AddressLine2');
        }
        if (!isNullProvided(initialValues, 'AddressLine3')) {
            partyContactMethod.Address.AddressLine3 = (String)initialvalues.get('AddressLine3');
        }
        if (!isNullProvided(initialValues, 'AddressLine4')) {
            partyContactMethod.Address.AddressLine4 = (String)initialvalues.get('AddressLine4');
        }
        if (!isNullProvided(initialValues, 'City')) {
            partyContactMethod.Address.City = (String)initialvalues.get('City');
        }
        if (!isNullProvided(initialValues, 'County')) {
            partyContactMethod.Address.County = (String)initialvalues.get('County');
        }
        if (!isNullProvided(initialValues, 'ISOCountry')) {
            partyContactMethod.Address.IsoCountry = (String)initialvalues.get('ISOCountry');
        }
        if (!isNullProvided(initialValues, 'PostalCode')) {
            partyContactMethod.Address.PostalCode = (String)initialvalues.get('PostalCode');
        }
        return address;
    }

    /**
     * when null is explicitly provided as part of initialValues map this means that value must not be initiated at all
     */
    static Boolean isNullProvided(final Map<String, Object> initialValues, final String key) {
        return initialValues.containsKey(key) && null == initialValues.get(key);
        
    }
    
    private static final String TEST_BARCODE = '80185054106';
    private static final String TEST_EPOS_CHANNEL = 'EPOS';
    private static final String TEST_ONLINE_CHANNEL = 'johnlewis.com';
    private static final String TEST_FULL_XML = 'Transaction Full XML';
    private static final String TEST_BRIEF_XML = 'Transaction Short XML';
    private static final String TEST_NAME = 'Unit Test Settings';
    
    public static void createCustomSettings() {
		 createControlPanel(true, true, TEST_BARCODE, false, true, TEST_EPOS_CHANNEL, TEST_FULL_XML, TEST_BRIEF_XML, TEST_NAME, TEST_ONLINE_CHANNEL, false,
		 					true, true, true, true);   	
    }
    
    public static void createControlPanel(Boolean activateAdmin, Boolean activateKD, String barcode, Boolean canUpdateContact, Boolean vfOverride, String eposChannel,
    									  String fullXML, String shortXML, String name, String onlineChannel, Boolean populateAudit, Boolean runAssignmentRules,
    									  Boolean runTriggers, Boolean runValidationRules, Boolean runWorkflow) {
		CustomSettings.TestRecord myjlsettings = new CustomSettings.TestRecord();
		myjlsettings.put('Activate_Account_Admin__c', activateAdmin);
    	myjlsettings.put('Activate_Kitchen_Drawer__c', activateKD);
    	myjlsettings.put('Barcode_Number_Prefix__c', barcode);
		myjlsettings.put('Can_Update_JL_com_Contact__c', canUpdateContact);
		myjlsettings.put('Enable_MyJL_VF_Page_Overrides__c', vfOverride);
    	myjlsettings.put('EPOS_Channel_Name__c', eposChannel);
    	myjlsettings.put('KD_Full_XML_File_Name__c', fullXML);
    	myjlsettings.put('KD_Short_XML_File_Name__c', shortXML);
    	myjlsettings.put('Name', name);
    	myjlsettings.put('Online_Channel_Name__c', onlineChannel);
    	myjlsettings.put('Populate_Audit_fields__c', populateAudit);
    	myjlsettings.put('Run_MyJL_Assignment_Rules__c', runAssignmentRules);
    	myjlsettings.put('Run_MyJL_Triggers__c', runTriggers);
    	myjlsettings.put('Run_MyJL_Validation_Rules__c', runValidationRules);
    	myjlsettings.put('Run_MyJL_Workflow__c', runWorkflow);
		CustomSettings.setTestMyJLSetting(UserInfo.getUserId(), myjlsettings);
    	
		system.assertNotEquals(null, CustomSettings.getMyJLSetting());
		
		// Check the admin got set as small check its ok
		system.assertEquals(activateAdmin, CustomSettings.getMyJLSetting().getBoolean('Activate_Account_Admin__c'));
    }

	/**
	 * generate and return test user account which can be later used in System.runAs(...)
	 * @param: randomString - can be anything, but test method name is recommended to make it unique
	 */
	public static User getTestUser(final String randomString) {
        Profile prof = [Select Id, Name From Profile Where Name = 'System Administrator'];
		User usr = new User(FirstName = randomString, LastName = 'Tester', Username=randomString+'test@test.com.test', 
							Email=randomString+'test.test@test.com.test', CommunityNickname=randomString, 
							Alias=randomString.abbreviate(8) ,TimeZoneSidKey='Europe/London', 
							LocaleSidKey='en_GB', EmailEncodingKey='ISO-8859-1', 
							ProfileId=prof.Id, LanguageLocaleKey='en_US', isActive=true
						);
		return usr;

	}

	 /**
	 * Create a new Customer(Ordering/Non-Ordering)
	 *
	 */
	 public static contact createCustomer(String customerType){
    	Contact customer = new Contact();
    		customer.firstname= 'abc';
    		customer.lastname='corp';
    		customer.email='abccorp@test.com';

    		if(customerType.equalsIgnoreCase('Ordering_Customer')){
    			customer.Shopper_ID__c = 'ShopperId'+Math.Random();
    			customer.RecordTypeId = CommonStaticUtils.getContactRecordType('Ordering_Customer').id;
    		}
	        customer.SourceSystem__c = 'johnlewis.com';
	        customer.Mailing_House_No_Text__c = '15';
	        customer.Mailing_House_Name__c = 'Wixford';
	        customer.Mailing_Street__c = '15 Oakfield';
	        customer.Mailing_Address_Line2__c = 'Block A';
	        customer.Mailing_Address_Line3__c = 'Square C';
	        customer.Mailing_Address_Line4__c = 'Area E';
	        customer.mailingcity = 'Liverpool';
	        customer.mailingstate = 'Merseyside';
	        customer.mailingcountry = 'United Kingdom';
	        customer.MailingCountryCode='GB';
	        customer.mailingpostalcode = 'L4 2QH';
	        insert(customer);
        return customer;
     }

      /**
	 * Create a new Case(Email / Web)
	 *
	 */
     /* COPT-5272
     public  static case createCase(String type){
    	Case testCase1 = new Case();
    		testCase1.Subject = 'Test Case';
    		testCase1.Status = 'New';
    		testCase1.SuppliedEmail='abccorp@test.com';
    		if(type.equalsIgnoreCase('web')){
    			   testCase1.RecordTypeId = CommonStaticUtils.getRecordType('Web_Triage').Id;
    			   testCase1.origin = 'web';
    		} else if(type.equalsIgnoreCase('email')){
    			   testCase1.RecordTypeId = CommonStaticUtils.getRecordType('Email_Triage').Id;
    			   testCase1.origin = 'email';
    		}

		insert testCase1;
		return testCase1;
    }
	COPT-5272 */
}