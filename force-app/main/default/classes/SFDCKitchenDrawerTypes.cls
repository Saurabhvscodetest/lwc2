global with sharing class SFDCKitchenDrawerTypes {
    
    global enum SortDirections {ASCENDING, DESCENDING}

    global enum SortByFields {TRANSACTION_DATE, TRANSACTION_NUMBER}

    
    global class RequestHeader {
        webservice String messageId;
    }
    
    global class ActionResponse {
        webservice String messageId;
        webservice Boolean success;
        webservice String resultType;//can not use 'type' because it is a reserved word
        webservice String message; //error description
        webservice String code;// error code
		webservice SearchResultHeader searchResultHeader;
        webservice ActionResult[] results;
    }
    
	global class SearchResultHeader {
		webservice Integer recordSetTotal;
		webservice Integer recordSetCount;
		webservice Boolean recordSetCompleteIndicator;
	}
	
    global class KDTransaction { //can not use 'Transaction' because it is a reserved word
        webservice String transactionId;
        webservice String voidingTransactionId; //not blank if this transaction is "voiding" transaction
        webservice DateTime transactionDate;
        webservice String channel;
        webservice Decimal transactionAmount;
        webservice String branchNumber;
        webservice String cardNumber;
        webservice String fullXml;
        webservice String shortXml;
        webservice String transactionType;
        webservice String[] associatedTransactionIds;
    }
    
    global class ActionResult {
        webservice Boolean success;
        webservice String code;
        webservice String resultType;//can not use 'type' because it is a reserved word
        webservice String message;
        webservice KDTransaction kdTransaction;
    }
    
    global class TransactionSearchCriteria {
        webservice Integer maxItems;
        webservice Integer recordSetStartNumber;//begins from 0
        webservice Boolean isFullXml;
        webservice SortByFields sortByField;
        webservice SortDirections sortDirection;
        webservice String email;
        webservice String customerId;
        webservice String membershipNumber;
        webservice String channel;
        webservice String branchNumber;
        webservice DateTime transactionsStartDate;
        webservice DateTime transactionsEndDate;
        webservice String cardNumber;
        webservice String transactionId;
    }
}