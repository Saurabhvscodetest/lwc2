/******************************************************************************
* @author       Llyr Jones
* @date         07.12.2016
* @description  Extension class for Close Duplicate Case
* Edit    Date        Author      Comment
* 001     07/12/17    LLJ         Initial Vetsion

******************************************************************************/


public with sharing class CloseDuplicateCaseController {

	private Case myCase;
    private ApexPages.StandardController controller;


    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public CloseDuplicateCaseController(ApexPages.StandardController stdController) {
        this.myCase = (Case)stdController.getRecord();
        this.controller = stdController;
    }

    public PageReference Save() {
        this.myCase.Status = 'Closed - Duplicate';
        this.myCase.jl_Action_Taken__c = 'No response required';
        return this.controller.Save();
    }
}