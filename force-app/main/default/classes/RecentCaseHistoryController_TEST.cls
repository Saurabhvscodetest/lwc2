@isTest
public class RecentCaseHistoryController_TEST {

	
    private static final Id pseCaseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonStaticUtils.CASE_RT_NAME_PRODUCT_STOCK_ENQUIRY).getRecordTypeId();

    @testSetup static void setup() {
         UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
    }

    @isTest 
    static void test_RecentCaseHistoryController(){
        
        Case c;		     
        Account acc = UnitTestDataFactory.createAccount(1, true);
        Contact ctc = UnitTestDataFactory.createContact(1, acc.id, true);
		List<Case> caseNonPSEList = new List<Case>();

        for (Integer i = 1; i <= 10; i++) {
            Case testCase = UnitTestDataFactory.createQueryCase(ctc.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, false);		       	
            caseNonPSEList.add(testCase);
        }
        insert caseNonPSEList;

        List<Case> casePSEList = new List<Case>();
        
        for (Integer i = 1; i <= 10; i++) {		       	
            Case testCase = UnitTestDataFactory.createNormalCase(ctc);
            testCase.Assign_To_New_Branch__c = 'Aberdeen'; 
            testCase.RecordTypeId = pseCaseRTId;
            casePSEList.add(testCase);
        }
        insert casePSEList;
				
       	Test.startTest();
		
			RecentCaseHistoryController recentCaseHistoryController = new RecentCaseHistoryController();
	        recentCaseHistoryController.ContactRecord = ctc;
			System.assertEquals(10, caseNonPSEList.size());
			System.assertEquals(10, casePSEList.size());		
			System.assertEquals(5, recentCaseHistoryController.RecentCases.size());	
			System.assertEquals(5, recentCaseHistoryController.RecentCasesPSE.size());	
        	recentCaseHistoryController.getTotalPages();
        	recentCaseHistoryController.getTotalPages2();
        	boolean returnBooleanResult = recentCaseHistoryController.hasNext2;
        	returnBooleanResult = recentCaseHistoryController.hasPrevious2;
    	    returnBooleanResult = recentCaseHistoryController.hasPrevious2;
	        Integer returnIntResult = recentCaseHistoryController.pageNumber2;
	        recentCaseHistoryController.first2();
        	recentCaseHistoryController.last2();

		Test.stopTest();      
    }
}