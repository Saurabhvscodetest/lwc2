/**
 * Unit tests for MyJLLoyaltyAccountCleanBatch class methods.
 * 001     08/07/16    NA      Decommission Contact Profile  
 */
@isTest
private class MyJLLoyaltyAccountCleanBatch_TEST {

	/**
	 * Code coverage for MyJLLoyaltyAccountCleanBatch.execute(SchedulableContext sc)
	 */
	static testMethod void testSchedulerExecuteMethod() {
    	system.debug('TEST START: testStartMethod');
		MyJL_Cleanup_Process_Settings__c c1 = new MyJL_Cleanup_Process_Settings__c(Name = MyJLLoyaltyAccountCleanBatch.CLASS_NAME, Record_Updates_Enabled__c = true, Email_Report_Recipient_List__c = 'matthew.povey@johnlewis.co.uk', Email_Report_Enabled__c = true, Process_Enabled__c = true);
		insert c1;

		SchedulableContext sc = null;
		MyJLLoyaltyAccountCleanBatch ladBatch = new MyJLLoyaltyAccountCleanBatch();

		Test.startTest();
		ladBatch.execute(sc);
		Test.stopTest();

    	system.debug('TEST END: testStartMethod');
	}
	
	/**
	 * Code coverage for MyJLLoyaltyAccountCleanBatch.start
	 */
	static testMethod void testStartMethod() {
    	system.debug('TEST START: testStartMethod');
       	Contact testCon = UnitTestDataFactory.createContact(1, true);
		//Contact_Profile__c testContactProfile1 = new Contact_Profile__c(Contact__c = testCon.Id, Name = 'TEST PROFILE 1', Email__c = testCon.Email);
		//insert testContactProfile1;
		Loyalty_Account__c la1 = new Loyalty_Account__c(contact__c = testCon.Id, IsActive__c = true, Scheme_Joined_Date_Time__c = system.now().addMonths(-6), Activation_Date_Time__c = system.now().addMonths(-6), Email_Address__c = testCon.Email);
		Loyalty_Account__c la2 = new Loyalty_Account__c(contact__c = testCon.Id, IsActive__c = true, Scheme_Joined_Date_Time__c = system.now().addMonths(-3), Activation_Date_Time__c = system.now().addMonths(-3), Email_Address__c = testCon.Email);
		List<Loyalty_Account__c> laList = new List<Loyalty_Account__c> {la1, la2};
		insert laList;
		
		Database.BatchableContext bc = null;
		MyJLLoyaltyAccountCleanBatch ladBatch = new MyJLLoyaltyAccountCleanBatch();
		Test.startTest();
    	Database.QueryLocator dql = ladBatch.start(bc);	
		Test.stopTest();

    	system.debug('TEST END: testStartMethod');
	}

	/**
	 * Test MyJLLoyaltyAccountCleanBatch.execute
	 */
	static testMethod void testExecuteMethod() {
    	system.debug('TEST START: testExecuteMethod');

		MyJL_Cleanup_Process_Settings__c c1 = new MyJL_Cleanup_Process_Settings__c(Name = MyJLLoyaltyAccountCleanBatch.CLASS_NAME, Record_Updates_Enabled__c = true, Email_Report_Recipient_List__c = 'matthew.povey@johnlewis.co.uk', Email_Report_Enabled__c = true, Process_Enabled__c = true);
		insert c1;
       	Contact testCon1 = UnitTestDataFactory.createContact(1, false);
       	Contact testCon2 = UnitTestDataFactory.createContact(2, false);
       	List<Contact> conList = new List<Contact> {testCon1, testCon2};
       	insert conList;
		//Contact_Profile__c testCP1 = new Contact_Profile__c(Contact__c = testCon1.Id, Name = 'TEST PROFILE 1', Email__c = testCon1.Email); //<<001>>
		//Contact_Profile__c testCP2 = new Contact_Profile__c(Contact__c = testCon2.Id, Name = 'TEST PROFILE 2', Email__c = testCon2.Email); //<<001>>
		//List<Contact_Profile__c> cpList = new List<Contact_Profile__c> {testCP1, testCP2};
		//insert cpList;
		Loyalty_Account__c la1 = new Loyalty_Account__c(contact__c = testCon1.Id, IsActive__c = true, Scheme_Joined_Date_Time__c = system.now().addMonths(-6), Activation_Date_Time__c = system.now().addMonths(-6), Email_Address__c = testCon1.Email); //<<001>
		Loyalty_Account__c la2 = new Loyalty_Account__c(contact__c = testCon1.Id, IsActive__c = true, Scheme_Joined_Date_Time__c = system.now().addMonths(-3), Activation_Date_Time__c = system.now().addMonths(-3), Email_Address__c = testCon1.Email); //<<001>
		Loyalty_Account__c la3 = new Loyalty_Account__c(contact__c = testCon2.Id, IsActive__c = true, Scheme_Joined_Date_Time__c = system.now().addMonths(-6), Activation_Date_Time__c = system.now().addMonths(-6), Email_Address__c = testCon2.Email); //<<001>
		Loyalty_Account__c la4 = new Loyalty_Account__c(contact__c = testCon2.Id, IsActive__c = true, Scheme_Joined_Date_Time__c = system.now().addMonths(-3), Activation_Date_Time__c = system.now().addMonths(-3), Email_Address__c = testCon2.Email); //<<001>
		List<Loyalty_Account__c> laList = new List<Loyalty_Account__c> {la1, la2, la3, la4};
		insert laList;
		Loyalty_Card__c lc1 = new Loyalty_Card__c(Loyalty_Account__c = la1.Id, Card_Number__c = '1234567891');
		Loyalty_Card__c lc2 = new Loyalty_Card__c(Loyalty_Account__c = la2.Id, Card_Number__c = '1234567892');
		Loyalty_Card__c lc3 = new Loyalty_Card__c(Loyalty_Account__c = la3.Id, Card_Number__c = '1234567893');
		Loyalty_Card__c lc4 = new Loyalty_Card__c(Loyalty_Account__c = la4.Id, Card_Number__c = '1234567894');
		List<Loyalty_Card__c> lcList = new List<Loyalty_Card__c> {lc1, lc2, lc3, lc4};
		insert lcList;
		Transaction_Header__c th1 = new Transaction_Header__c(Card_Number__c = lc2.Id, Receipt_Barcode_Number__c = '123456789200001', Transaction_Amount__c = 10.00);
		Transaction_Header__c th2 = new Transaction_Header__c(Card_Number__c = lc3.Id, Receipt_Barcode_Number__c = '123456789300001', Transaction_Amount__c = 20.00);
		Transaction_Header__c th3 = new Transaction_Header__c(Card_Number__c = lc3.Id, Receipt_Barcode_Number__c = '123456789300002', Transaction_Amount__c = 30.00);
		Transaction_Header__c th4 = new Transaction_Header__c(Card_Number__c = lc4.Id, Receipt_Barcode_Number__c = '123456789400001', Transaction_Amount__c = 10.00);
		Transaction_Header__c th5 = new Transaction_Header__c(Card_Number__c = lc4.Id, Receipt_Barcode_Number__c = '123456789400002', Transaction_Amount__c = 20.00);
		Transaction_Header__c th6 = new Transaction_Header__c(Card_Number__c = lc4.Id, Receipt_Barcode_Number__c = '123456789400003', Transaction_Amount__c = 30.00);
		List<Transaction_Header__c> thList = new List<Transaction_Header__c> {th1, th2, th3, th4, th5, th6};
		insert thList;

		List<Loyalty_Account__c> testLAList0 = [SELECT Id, Name, IsActive__c FROM Loyalty_Account__c WHERE IsActive__c = true];
		system.assertEquals(4, testLAList0.size());
		
		List<contact> testCPList1 = [SELECT Id, Name,  (SELECT Id, Name, IsActive__c, Join_Date__c, ShopperId__c FROM MyJL_Accounts__r WHERE IsActive__c = true ORDER BY Join_Date__c)
        										FROM contact
        										WHERE Id IN (SELECT contact__c FROM Loyalty_Account__c WHERE IsActive__c = true)];   //<<001>
		system.assertEquals(2, testCPList1.size());

		Database.BatchableContext bc = null;
		MyJLLoyaltyAccountCleanBatch ladBatch = new MyJLLoyaltyAccountCleanBatch();
		
		test.startTest();
  	    	ladBatch.execute(bc, testCPList1);
		test.stopTest();
		
		List<Loyalty_Account__c> testLAList1 = [SELECT Id, Name, IsActive__c FROM Loyalty_Account__c WHERE IsActive__c = true];
		system.assertEquals(2, testLAList1.size());
		
		for (Loyalty_Account__c la : testLAList1) {
			system.assert(la.Id == la1.Id || la.Id == la3.Id);
		}

    	system.debug('TEST END: testExecuteMethod');
	}

    /**
     * Code coverage for MyJLLoyaltyAccountCleanBatch.finish()
     */
    static testMethod void testFinishMethod() {
    	system.debug('TEST START: testFinishMethod');

		MyJL_Cleanup_Process_Settings__c c1 = new MyJL_Cleanup_Process_Settings__c(Name = MyJLLoyaltyAccountCleanBatch.CLASS_NAME, Record_Updates_Enabled__c = true, Email_Report_Recipient_List__c = 'matthew.povey@johnlewis.co.uk', Email_Report_Enabled__c = true, Process_Enabled__c = true);
		insert c1;
    	Config_Settings__c defaultNotificationEmail = new Config_Settings__c(Name = 'EMAIL_NOTIFICATION_DEFAULT_ADDRESS', Text_Value__c = 'Matthew.Povey@johnlewis.co.uk');
		insert defaultNotificationEmail;

		Database.BatchableContext bc = null;
		MyJLLoyaltyAccountCleanBatch ladBatch = new MyJLLoyaltyAccountCleanBatch();
		ladBatch.totalRecordCount = 10;

		Test.startTest();
		ladBatch.finish(bc);
		Test.stopTest();

    	system.debug('TEST END: testFinishMethod');
    }

}