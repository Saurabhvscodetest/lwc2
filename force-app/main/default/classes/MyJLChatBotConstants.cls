/*
* @File Name   : MyJLChatBotConstants
* @Description : Constants class to declare all the variables which is used  
* @Copyright   : Zensar
* @Jira #      : #5066
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0       19-Sep-19         Ragesh G                     Created
*/
    
public class MyJLChatBotConstants {
	
	public static String SERVICE_NAME_FOR_ERROR		 		= 'MyJL Chatbot Error';
	public static String MISSING_LOYALTY_NUMBER 			= 'MISSING_LOYALTY_NUMBER';
	public static String MISSING_JL_SHOPPER_ID 				= 'MISSING_JL_SHOPPER_ID';
	public static String NO_LOYALTY_ACCOUNTS_FOUND 			= 'NO_LOYALTY_ACCOUNTS_FOUND';
	public static String ACCOUNT_NOT_ACTIVE 				= 'ACCOUNT_NOT_ACTIVE';
	public static String LOYALTY_CARD_IS_NOT_ACTIVE 		= 'LOYALTY_CARD_IS_NOT_ACTIVE';
	public static String MULTIPLE_CARDS_ISSUE				= 'MULTIPLE_CARDS_ISSUE';
	public static String LOYALTY_NUMBER_NOT_ASSOCIATED_WITH_SHOPPER_ID = 'LOYALTY_NUMBER_NOT_ASSOCIATED_WITH_SHOPPER_ID';
	
	public static String LOYALTYNUMBER_NOT_PROVIDED	 = 'loyaltyNumber: is not provided';
	public static String SHOPPERID_NOT_PROVIDED	 	 = 'shopperId: is not provided';
	public static String NO_LOYALTY_ACCOUNT_FOUND	 = 'No Loyalty accounts found for shopperId';
	
	public static String ACCOUNT_IS_NOT_ACTIVE = 'Account is not active';
	public static String LOYALTY_CARD_IS_NOT_ACTIVE_MSG = 'Loyalty Card is not active';
	public static String MULTIPLE_ACTIVE_MYPTR_CARD = 'Multiple Active MyPTR Card';
	
	public static String LOYALTY_NUMBER_NOT_ASSOCIATED_WITH_SHOPPER_ID_MSG = 'Loyalty Card Number is not associated to shopperId';
	
	
	public static final String MY_PARTNERSHIP_CARD_TYPE = 'My Partnership';
    public static final String MY_JL_CARD_TYPE = 'My John Lewis';
    
    public static final String CASE_CREATED_SUCCESS_MESSAGE='Case Record has been successfully created';
	
	public enum ERRORCODE {
        INACTIVE_SERVICE,
        MESSAGEID_NOT_PROVIDED,
        MESSAGEID_DUPLICATE,
        CUSTOMERID_NOT_PROVIDED,
        CUSTOMERID_NOT_FOUND,
        CUSTOMERID_NOT_UNIQUE,
        CUSTOMERID_ALREADY_USED,
        EMAIL_NOT_PROVIDED,
        EMAIL_NOT_UNIQUE,
        EMAIL_ALREADY_USED,
        FAILED_TO_PERSIST_DATA,
        NO_CUSTOMER_DATA,
        UNHANDLED_EXCEPTION,
        /* Join */
        INCORRECT_JOIN_REQUEST,
        BARCODE_ALREADY_USED,
        EMAIL_TOO_LONG,
        CUSTOMER_ALREADY_JOINED,
        /* Leave */
        LOYALTY_ACCOUNT_NOT_FOUND,
        NO_ACTIVE_LOYALTY_ACCOUNT_FOUND,
        /* KD GET */
        TRANSACTIONID_NOT_PROVIDED,
        TRANSACTIONID_ALREADY_USED,
        TRANSACTIONID_NOT_FOUND,
        NO_TRANSACTION_DATA,
        /* KD Search */
        INCORRECT_SEARCH_REQUEST,
        /* KD Create */
        TRANSACTIONID_NOT_UNIQUE,
        CARD_NUMBER_NOT_PROVIDED,
        ASSOCIATED_TRANSACTION_NOT_FOUND,
        CARD_NUMBER_NOT_FOUND,
        FULL_XML_NOT_PROVIDED,
        SHORT_XML_NOT_PROVIDED,
        TRANSACTION_AMOUNT_NOT_PROVIDED,
        TRANSACTION_DATE_NOT_PROVIDED,
        VOIDEDTRANSACTIONID_NOT_FOUND,
        MY_PARTNERSHIP_JOIN_REST_ERROR,
        MY_PARTNERSHIP_MISSING_DATA,
        MY_LOYALTY_NUMBER_MISSING_DATA
            
    }
    
}