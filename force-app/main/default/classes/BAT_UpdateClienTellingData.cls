/*
* @File Name   : BAT_UpdateClienTellingData
* @Description : Batch class used to fetch the contact records from the database based on the field - GDPR_Data_Deletion__c
* @Copyright   : Zensar
* @Jira #      : #4277
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0       16-April-19        Ragesh G                     Created
*/
global class BAT_UpdateClienTellingData implements Database.Batchable<sObject>,Database.Stateful{

    global Map<Id,String> exceptionMap = new Map<Id,String>();
    String EMAIL_CONSTANT_STRING = 'Email:%';
    String CALL_LOG_STRING = 'Call Log';
    global Integer result=0;
    private static final DateTime GDPR_UPDATE_CT_DATE = ConstantValues.FIVE_YEARS_AGO;
    global String contactSOQL = 'SELECT Id FROM Contact WHERE GDPR_Data_Deletion__c !=null AND GDPR_Data_Deletion__c <: GDPR_UPDATE_CT_DATE';
    global String contactWithoutGDPRFieldSOQL = 'SELECT id,Createddate,Customer_Record_Type_Lex__c,Email, (SELECT id,IsActive__c FROM MyJL_Accounts__r), (SELECT id,Subject FROM tasks WHERE Subject LIKE: EMAIL_CONSTANT_STRING OR Subject =:CALL_LOG_STRING ), (SELECT id FROM ReportCards__r) FROM Contact WHERE ID NOT IN (SELECT ContactId FROM Case) AND ID NOT IN (SELECT ContactId FROM LiveChatTranscript) AND Createddate <= : GDPR_UPDATE_CT_DATE AND isGDPRCriteriaEmpty__c =true';
  
   /**
    *@purpose : Interface Method needs to be implemented to get the batch started, querying the required records from the contact with GDPR_Data_Deletion__c date 
        5 years ago with the custom settings record limit.
    *@param   : Database.BatchableContext BC 
    *@return  : Database.QueryLocator
    **/
  
    global Database.QueryLocator start( Database.BatchableContext BC ){
        
    GDPR_Record_Limit__c gDPRLimit = GDPR_Record_Limit__c.getValues(ConstantValues.CLIENT_TELLING_UPDATE_BATCH_NAME);
      if( gDPRLimit != NULL ){
          String recordLimit = ' ' + 'Limit' + ' ' + gDPRLimit.Record_Limit__c;
          contactSOQL  += recordLimit;
      }
        
      if ( Database.query ( contactSOQL ).size () == 0 ) {
          contactSOQL = contactWithoutGDPRFieldSOQL;
      } 
      return Database.getQueryLocator( contactSOQL );
    }
  
   /**
    *@purpose : Interface Method needs to be implemented to get the batch executed, process the records coming through the 
          scope list and check whether the all the conditions are satisfied 
    *@param   : Database.BatchableContext BC, List < SObject > - In this case list of Contacts 
    *@return  : void
    **/
    global void execute( Database.BatchableContext BC, list<Contact> contactRecordList ){
        
        if ( contactRecordList == null ) {
            contactRecordList = Database.query ( contactWithoutGDPRFieldSOQL );
        }
        List < Contact > contactListToUpdate = new List < Contact > ();
        try{
      for ( Contact contactRecord : contactRecordList ) {
        // Nullifying the Personal Styling Section fields
        
        contactRecord.Clienteling_Booking_Reason__c      = null;
        contactRecord.Clienteling_Hair_Colour__c      = null;
        contactRecord.Clienteling_Height__c          = null;
        contactRecord.Clienteling_Brands_Purchased__c    = null;
        contactRecord.Clienteling_Products_Liked__c      = null;
        contactRecord.CT_Appointment_date__c        = null;
        contactRecord.Clienteling_Booking_Bug_Reference__c  = null;
        contactRecord.Clienteling_Eye_Colour__c        = null;
        contactRecord.Customer_Body_Picture__c        = null;
        contactRecord.Clienteling_Brands_Not_Liked__c    = null;
        contactRecord.Clienteling_Anything_Else__c      = null;
        // Nullifying the Women's Sizes Section fields
        contactRecord.Clienteling_Dress_Size__c        = null;
        contactRecord.Clienteling_Trouser_Size__c      = null;
        contactRecord.Clienteling_Shoe_Size__c        = null;
        contactRecord.Clienteling_Top_Size__c        = null;
        contactRecord.Clienteling_Jeans_Size__c        = null;
        contactRecord.Shirt_Size__c              = null;
        // Nullifying the Women's Sizes Section fields
        contactRecord.Suit_Jacket_Size__c          = null;
        contactRecord.Clienteling_Shoe_Size__c        = null;
        contactRecord.Jumper_TShirts__c            = null;
        contactRecord.Trouser_Size__c            = null;
        // Marketing Preferences Section
         contactRecord.Clienteling_Permission__c       = 'No';
         contactRecord.Clienteling_Privacy__c       = false;
         contactRecord.Assign_Customer_to_Me__c        = false;
        
        // To skip all the validations under contact record 
        contactRecord.Skip_Batch_Validation__c         = true;
        contactListToUpdate.add (contactRecord );
      }
      Database.update ( contactListToUpdate );
        }Catch(exception e){
            EmailNotifier.sendNotificationEmail( ConstantValues.CT_UPDATE_BATCH_EXECUTION_FAILED, e.getMessage());
        }
    }
  
  /**
    *@purpose : Interface Method needs to be implemented post batch execution finishes 
          sending a notification with the number of records successfully updated and number of failed records 
    *@param   : Database.BatchableContext BC
    *@return  : void
    **/
    global void finish(Database.BatchableContext BC) {
        String textBody ='';
        set<Id> allIds = new set<Id>();
        textBody+= result +ConstantValues.CT_UPDATE_BATCH_EXECUTION_SUCCESS;
        if (!exceptionMap.isEmpty()){
            for (Id recordids : exceptionMap.KeySet()){
                allIds.add(recordids);
            } 
            textBody+= 'Error log: '+allIds+'\n';
        }
        EmailNotifier.sendNotificationEmail(ConstantValues.CT_GDPR_DELETE_LOG_SUBJECT, textBody);
    }
}