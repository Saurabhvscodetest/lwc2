@isTest
public class Test_EventHubCaseExceptions {
    @isTest
    public static void Eventhubdata(){
       // Create an Account
        Account acc = UnitTestDataFactory.createAccount('Test Account');
        insert acc;		
        // Create a Contact
        Contact con = UnitTestDataFactory.createContact(acc);
        insert con;		
        Case c = UnitTestDataFactory.createNormalCase(con);        
        User testUser = UnitTestDataFactory.findStandardActiveUser();        
        // Create Some tasks on this case for someone else
        Task t = new Task();
        t.WhatId = c.Id;
        t.OwnerId = testUser.Id;
        insert t;
        Test.startTest();
        EventHubCaseExceptions.fetchtaskdata(c.id);
        Test.stopTest();
        
    }
}