global class JLMoveUserBackToPrimaryTeamBatch implements Database.Batchable<sObject> {
	
	global JLMoveUserBackToPrimaryTeamBatch() {
		
	}
	

	global Database.QueryLocator start(Database.BatchableContext BC) {

		//CHECK WHICH PROFILES HAVE BEEN ENABLED FOR AUTO RETURN TO PERMANENT TEAM
		Set <String> profileNames = new Set<String> ();
		List<Config_Settings__c> allSettings = Config_Settings__c.getall().values();
		for (Config_Settings__c s : allSettings) {
			if (s.name!=null && s.name.contains('AUTO_RETURN_TO_TEAM_PROFILE') && !String.IsBlank(s.Text_Value__c)) {
				profileNames.add(s.Text_Value__c);
			}
		}

		Map<Id,Profile> profilesMap = new Map<Id,Profile> ([select id from Profile where name in :profileNames]);

		//Now GET number of MINUTES 
		Config_Settings__c autoReturnMinutesSetting = Config_Settings__c.getInstance('AUTO_RETURN_TO_TEAM_MINUTES');
		Integer minutes  = (Integer) autoReturnMinutesSetting.Number_Value__c ;
		Datetime lastLoginDT = DateTime.now().addMinutes(minutes * -1);

		//ES:26/06/15 - Restricting auto returns to teams enabled in the Team custom Setting
		Set <String> teamsEnableToAutoReturn = new Set<String> ();
		List<Team__c> allTeams = Team__c.getall().values();

		for (Team__c t : allTeams) {
			if (t.Available_In_House_Team_Management_Page__c || t.Available_on_Team_Management_Page__c) {
				teamsEnableToAutoReturn.add(t.name);
			}
		}

		return Database.getQueryLocator([select id, team__c, My_Team_Manager__c, My_Team_Manager__r.team__c, LastLoginDate from User where isActive=True and Is_User_Team_Manager__c=false and profileid in :profilesMap.keySet() and (LastLoginDate < :lastLoginDT or LastLoginDate = null ) and team__c in :teamsEnableToAutoReturn]);
	}

	global void execute(Database.BatchableContext BC, List<User> scope) {
	
		List<User> usersToUpdate = new List<User>();
		for (User u : scope) {
			if (u.My_Team_Manager__c!=null && !String.IsBlank(u.My_Team_Manager__r.team__c)) {
				if(u.Team__c != u.My_Team_Manager__r.team__c){
					u.team__c = u.My_Team_Manager__r.team__c;
					usersToUpdate.add(u);
				}
			}
		}
		update usersToUpdate;
	}
	
	global void finish(Database.BatchableContext BC) {
		//Chaining batch job execution. next run in (number of minutes in Custom Setting)
		Config_Settings__c autoReturnReRunSetting = Config_Settings__c.getInstance('AUTO_RETURN_TO_TEAM_BATCH_RERUN');
		Integer rerunAfter  = (Integer) autoReturnReRunSetting.Number_Value__c ;

		if(!Test.isRunningTest()){
			System.scheduleBatch(new JLMoveUserBackToPrimaryTeamBatch(), 'Auto-Return to Permanent Team Batch Job', rerunAfter);
		}
	}
}