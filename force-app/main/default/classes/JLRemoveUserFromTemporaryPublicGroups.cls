/******************************************************************************
* @author       Ranjan Samal             
* @date         27/09/2017
* @description  Removal of Users From Temporary Public Group after 8 hours 
******************************************************************************/
global class JLRemoveUserFromTemporaryPublicGroups implements Database.Batchable<sObject> {
  
  global JLRemoveUserFromTemporaryPublicGroups() {
    
  }

  global Database.QueryLocator start(Database.BatchableContext BC) {
    
    //CHECK WHICH PROFILES HAVE BEEN ENABLED FOR AUTO RETURN TO PERMANENT TEAM
    Set <String> profileNames = new Set<String> ();
    List<Config_Settings__c> allSettings = Config_Settings__c.getall().values();
    for (Config_Settings__c s : allSettings) {
        if (s.name!=null && s.name.contains('AUTO_RETURN_TO_TEAM_PROFILE') && !String.IsBlank(s.Text_Value__c)) {
            profileNames.add(s.Text_Value__c);
        }
    }

    Map<Id,Profile> profilesMap = new Map<Id,Profile> ([SELECT ID 
                                                          FROM Profile 
                                                         WHERE Name IN :profileNames]);

    //Now GET number of MINUTES 
    Config_Settings__c autoReturnMinutesSetting = Config_Settings__c.getInstance('AUTO_RETURN_TO_TEAM_MINUTES');
    Integer minutes  = (Integer) autoReturnMinutesSetting.Number_Value__c ;
    Datetime lastLoginDT = DateTime.now().addMinutes(minutes * -1);
    
    if(Test.isRunningTest()) {
        return Database.getQueryLocator([SELECT ID,
                                            Temporary_Public_Group__c
                                       FROM USER
                                      WHERE Temporary_Public_Group__c != null
                                        AND (LastLoginDate < :lastLoginDT OR LastLoginDate = null)
                                        AND profileid in :profilesMap.keySet() LIMIT 1]) ;
    } else {
    return Database.getQueryLocator([SELECT ID,
                                            Temporary_Public_Group__c
                                       FROM USER
                                      WHERE Temporary_Public_Group__c != null
                                        AND (LastLoginDate < :lastLoginDT OR LastLoginDate = null)
                                        AND profileid in :profilesMap.keySet()]) ;
    
    }
    }

    global void execute(Database.BatchableContext BC, List<User> scope) {
        Set<ID> setGroupId = new Set<Id>();
        Set<ID> setUserOrGroupId = new Set<Id>();
        List<User> users = new List<User>();
        //Fetching the Group IDs and User IDs
        for (User user : scope) {
            LIST<ID> pbGroupIDs = user.Temporary_Public_Group__c.split(',');
            for (ID pbID: pbGroupIDs){
                setGroupId.add(pbID);
            }
            setUserOrGroupId.add(user.Id);
            user.Temporary_Public_Group__c = '';
            users.add(user);
        }
        //Deleting the members from the public groups by deleting the corresponding group members
        if( setGroupId.size() > 0 && setUserOrGroupId.size() > 0 ) {
            List<GroupMember> groupMembers = [ SELECT Id 
                                                 FROM GroupMember 
                                                WHERE UserOrGroupID IN :setUserOrGroupId 
                                                  AND GroupId  IN :setGroupId ];
            if(groupMembers.size() > 0) {
                try{
                    delete groupMembers;
                    update users;
                } catch(exception exp) {
                    
                }
            }
        }
    }
  
  global void finish(Database.BatchableContext BC) {
    //Chaining batch job execution. next run in (number of minutes in Custom Setting)
    Config_Settings__c autoReturnReRunSetting = Config_Settings__c.getInstance('AUTO_RETURN_TO_TEAM_BATCH_RERUN');
    Integer rerunAfter  = (Integer) autoReturnReRunSetting.Number_Value__c ;

    if(!Test.isRunningTest()){
      System.scheduleBatch(new JLRemoveUserFromTemporaryPublicGroups(), 'Removal of Users From Temporary Public Group', rerunAfter);
    }
  }
}