/******************************************************************************
* @author
* @date
* @description  Controller class for CustomerInformationPanel Component.
*
* EDIT RECORD
*
* LOG   DATE           AUTHOR       JIRA            COMMENT
* 001	16/07/2016		AJ			COPT-1448	 Added the customer segmentation
*******************************************************************************
*/
public class CustomerInformationPanelController {
	//<<001> Customer segmentation
	private  static  final Boolean userHasUserSegmentationAccess = CommonStaticUtils.isPermissionSetAssignedToSingleUserByName(UserInfo.getUserId(), 'Customer_Segmentation_View');

	// Data members
	public String ContactFullAddress{set;get;}
	private Contact contactRecordLoc;
	public Boolean getuserHasUserSegmentationAccess() {
		return userHasUserSegmentationAccess;
	}

	public Contact ContactRecord{
		set{
			contactRecordLoc = value;
			string tempAdd;
			if(contactRecordLoc != null){
				if(contactRecordLoc.MailingStreet != null){tempAdd = contactRecordLoc.MailingStreet;}
				if(contactRecordLoc.MailingStreet != null){tempAdd = tempAdd + ', ';}
				if(contactRecordLoc.MailingCity != null){tempAdd = tempAdd + contactRecordLoc.MailingCity;}
				if(contactRecordLoc.MailingState != null){tempAdd = tempAdd + ', ';}
				if(contactRecordLoc.MailingState != null){tempAdd = tempAdd + contactRecordLoc.MailingState;}
				if(contactRecordLoc.MailingPostalCode != null){tempAdd = tempAdd + ', ';}
				if(contactRecordLoc.MailingPostalCode != null){tempAdd = tempAdd + contactRecordLoc.MailingPostalCode;}
				if(contactRecordLoc.MailingCountry != null){tempAdd = tempAdd + ', ';}
				if(contactRecordLoc.MailingCountry != null){tempAdd = tempAdd + contactRecordLoc.MailingCountry;}
				ContactFullAddress = tempAdd;

			}
		}
		get{
			return contactRecordLoc;
		}
	}

	public boolean isPrimaryContact{
		get {
			if(contactRecordLoc.recordTypeId == CommonStaticUtils.getContactRecordType(CommonStaticUtils.CONTACT_RT_DEVELEPER_NAME_OC).Id){
				return true;
			}else{
				return false;
			}
		}
	}
}