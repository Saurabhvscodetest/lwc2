@isTest
private class OpenSpanUtilities_TEST {

    static testMethod void testHelperMethods() {
    	
    	String inputValue = 'TeSt Stri_ng 432%$£d\r\n';
    	String expectedOutputValue = 'teststri_ng432%$£d';
    	
    	String outputValue = OpenSpanUtilities.cleanMatchingValue(inputValue);
    	System.assertEquals(expectedOutputValue, outputValue);
    	
    	inputValue = null;
    	expectedOutputValue = '';
    	
    	outputValue = OpenSpanUtilities.cleanMatchingValue(inputValue);
    	System.assertEquals(expectedOutputValue, outputValue);
    	
    	inputValue = 'TeSt Stri_ng 432%$£d';
    	expectedOutputValue = 'TeSt Stri_ng 432%$£d';
    	
    	outputValue = OpenSpanUtilities.cleanNullValue(inputValue);
    	System.assertEquals(expectedOutputValue, outputValue);
    	
    	inputValue = null;
    	expectedOutputValue = '';
    	
    	outputValue = OpenSpanUtilities.cleanNullValue(inputValue);
    	System.assertEquals(expectedOutputValue, outputValue);
    	
    }
    
    static testMethod void testValidateCase() {
    	
    	OS_CaseRoutingMapping__c mapping = UnitTestDataFactory.createOpenSpan2CaseManagementMapping();
    	insert mapping;
    	
    	// valid dataHub case on first name, last name, email and os2cm custom setting mapping and dropdowns
    	Data_Hub__c dataHubRecord = UnitTestDataFactory.createValidDataHubRecord();
    	
    	Data_Hub__c outputValue = OpenSpanUtilities.validateCase(dataHubRecord);
    	
    	System.assertEquals('Active', outputValue.Status__c);
    	
    	dataHubRecord.email__c = null;
    	dataHubRecord.Address1__c = 'addr';
    	dataHubRecord.Post_Code__c = 'po5 1cod';
    	
    	outputValue = OpenSpanUtilities.validateCase(dataHubRecord);
    	
    	System.assertEquals('Active', outputValue.Status__c);
    	
    	mapping.CM_Record_Type_Name__c = CommonStaticUtils.CASE_RT_NAME_QUERY;
    	update mapping;
    	
    	outputValue = OpenSpanUtilities.validateCase(dataHubRecord);
    	
    	// TODO: assert
    	
    	//dataHubRecord.Post_Code__c = null;
    	dataHubRecord.Email__c = 'badFormat';
    	
    	outputValue = OpenSpanUtilities.validateCase(dataHubRecord);
    	
    	System.assertEquals('Rejected', outputValue.Status__c);
    	
    }
    
    static testMethod void testContactMatching(){
    	
    	insert UnitTestDataFactory.createOpenSpan2CaseManagementMapping();
    	
    	Data_Hub__c dataHubRecord = UnitTestDataFactory.createValidDataHubRecord();
    	
    	Case outputValue = OpenSpanUtilities.matchCaseByKey1(dataHubRecord);
    	
    	System.assertEquals('fnamelnameemail@address.com', outputValue.contact.Case_Matching_Key_1__c);
    	
    	outputValue = OpenSpanUtilities.matchCaseByKey2(dataHubRecord);
    	
    	System.assertEquals('fnamelnameaddrqwe123', outputValue.contact.Case_Matching_Key_2__c);
    	
    }
    
    static testMethod void testContactProfileMatching(){
    	
    	OS_CaseRoutingMapping__c mapping = UnitTestDataFactory.createOpenSpan2CaseManagementMapping();
    	mapping.CM_Record_Type_Name__c = CommonStaticUtils.CASE_RT_NAME_QUERY;
    	insert mapping;
    	
    	Data_Hub__c dataHubRecord = UnitTestDataFactory.createValidDataHubRecord();
    	
    	Case outputValue = OpenSpanUtilities.matchCaseByKey3(dataHubRecord);
    	
    	System.assertEquals('fnamelnameemail@address.com', outputValue.contact_profile__r.Case_Matching_Key_1__c);
    	
    	outputValue = OpenSpanUtilities.matchCaseByKey4(dataHubRecord);
    	
    	System.assertEquals('fnamelnameaddrqwe123', outputValue.contact_profile__r.Case_Matching_Key_2__c);
    	
    }
    
    
    static testMethod void testCreateOrphanCase(){

		Config_Settings__c cs = new Config_Settings__c();
		cs.Name = 'OpenSpan_Default_PSE_Owner';
		cs.Text_Value__c = UserInfo.getUserName();
		insert cs;
    	
    	OS_CaseRoutingMapping__c mapping = UnitTestDataFactory.createOpenSpan2CaseManagementMapping();
    	mapping.CM_Record_Type_Name__c = CommonStaticUtils.CASE_RT_NAME_PRODUCT_STOCK_ENQUIRY;
    	insert mapping;
    	
    	Data_Hub__c dataHubRecord = UnitTestDataFactory.createValidDataHubRecord();
    	
    	// just run through this method
    	OpenSpanUtilities.validateCase(dataHubRecord);
    	
    	Case outputValue = OpenSpanUtilities.createOrphanCase(dataHubRecord);
    	
    	System.assert(outputValue.Contact == null);
    	System.assert(outputValue.Contact_Profile__c == null);
    	
    }
    
    static testMethod void testDataHubRecordRejection(){
    	
    	insert UnitTestDataFactory.createOpenSpan2CaseManagementMapping();
    	
    	Data_Hub__c dataHubRecord = UnitTestDataFactory.createValidDataHubRecord();
    	dataHubRecord.First_Name__c = null;
    	
    	Data_Hub__c outputValue = OpenSpanUtilities.validateCase(dataHubRecord);
    	
    	// TODO: assert
    	
    	dataHubRecord.First_Name__c = 'fName';
    	dataHubRecord.Query_For_Branch__c = 'someInvalidValue';
    	
    	outputValue = OpenSpanUtilities.validateCase(dataHubRecord);
    	
    	// TODO: assert
    }
    
    static testMethod void testCaseComments(){
    	
    	Data_hub__c dataHubRecord = UnitTestDataFactory.createValidDataHubRecord();
    	//insert dataHubRecord;
    	
    	OpenSpanUtilities.createCaseComment(null, dataHubRecord);
    	
    }
    
    static testMethod void testToDecimal(){
    	
    	Decimal decimalValue = OpenSpanUtilities.toDecimal('33');
    	System.assertEquals(33, decimalValue);
    	
    }
    
}