// Edit     Date            By      Comment
//  001     07/05/15        NTJ     Add outbound notification
//  002     09/06/15        NTJ     MJL-2060 - Check whether the contact profile lookup is non null before sending a notification. If it is not-null then whoever set it
//                                  has already sent the notification.
//  003     11/06/15        NTJ     Try a few times just in case timing is an issue i.e. Update has not happened yet
//  004     15/06/15        NTJ     ATGT-16603 - only update contact profile on LA if it is null
//  005     27/06/16        NA      Decommissioning contact profile 

public with sharing class myJL_PostJoinHandler implements Queueable {

    private List<Loyalty_Account__c> accounts;
    private Boolean REQUERY = true;
    
    private static final Integer MAX_POSTJOIN_RETRIES = 5;
    private Integer attempts;

    public myJL_PostJoinHandler(List<Loyalty_Account__c> accounts, Integer attemptNo) {
        system.debug('accounts: '+accounts);
        this.accounts = accounts;
        this.attempts = attemptNo;
    }

    public void execute(QueueableContext context) {
        List<Loyalty_Account__c> requeriedAccounts = requery(accounts);

        Map<String, Loyalty_Account__c> shopper2LaMap = new Map<String, Loyalty_Account__c>();
        Set<String> shopperIdSet = new Set<String>();
        for (Loyalty_Account__c la:requeriedAccounts) {
            shopperIdSet.add(la.ShopperId__c);
            shopper2LaMap.put(la.ShopperId__c, la);
        }
        //system.debug('shopperIdSet: '+shopperIdSet);
        
        List<Loyalty_Account__c> updateList = new List<Loyalty_Account__c>();
        List<Loyalty_Account__c> notifList = new List<Loyalty_Account__c>();
        for (contact cp:[SELECT Id, Shopper_ID__c FROM contact WHERE Shopper_ID__c IN :shopperIdSet]) {  //<<005>>          
            // get the la
            if (String.isNotBlank(cp.Shopper_ID__c)) {
                // 003 - remove from set to indicate that we saw it
                shopperIdSet.remove(cp.Shopper_ID__c);

                Loyalty_Account__c la = (Loyalty_Account__c) shopper2LaMap.get(cp.Shopper_ID__c);
                if (la != null) {
                    // test code - to simulate an unlinked la
                    //la.Customer_Profile__c = null;
                    system.debug('cp: '+cp+' la: '+la);
                    if (la.contact__c == null) {    //<<005>>
                        notifList.add(la);
                        la.contact__c = cp.Id;    //<<005>>
                        la.adopted__c = true;
                        updateList.add(la);
                    }
                }
            }
        }
        if (updateList.isEmpty() == false) {
            update updateList;
            // send updateList
            if (REQUERY && (notifList.isEmpty() == false)) {
                MyJL_LoyaltyAccountHandler.sendJoinOrLeaveNotification(BaseTriggerHandler.toIdSet(notifList));    //<<005>>                                       
            } else if (!REQUERY) {
                MyJL_LoyaltyAccountHandler.sendJoinOrLeaveNotification(BaseTriggerHandler.toIdSet(updateList));  //<<005>>                                                        
            }
        }
        
        // 003 - do we need to try again for Potential Customers that are not yet Contact Profiles?
        resubmit(shopperIdSet, shopper2LaMap, requeriedAccounts);
    }
    
    private List<Loyalty_Account__c> requery(List<Loyalty_Account__c> inputList) {
        Set<Id> laIdSet = BaseTriggerHandler.toIdSet(inputList);
        system.debug('laIdSet: '+laIdSet);
        
        List<Loyalty_Account__c> laList = [SELECT Id, contact__c, ShopperId__c FROM Loyalty_Account__c WHERE Id IN :laIdSet];    //<<005>>
        return laList;
    }
    
    private void resubmit(Set<String> shopperIdSet, Map<String, Loyalty_Account__c> shopper2LaMap, List<Loyalty_Account__c> las) {
        // Do we have any retries left?
        if (attempts < MAX_POSTJOIN_RETRIES) {
            system.debug('attempts: '+attempts);
            attempts++;
            // are there any ShopperIds we have not yet seen in a Contact Profile?
            if (shopperIdSet.isEmpty() == false) {
                // yes
                // make a list for the new call
                List<Loyalty_Account__c> remainingLas = new List<Loyalty_Account__c>();
                // loop through the shopperId we have not fixed yet
                for (String shopperId:shopperIdSet) {
                    system.debug('shopperId: '+shopperId);

                    if (String.isNotBlank(shopperId)) {
                        // find the Loyalty Account
                        Loyalty_Account__c la = (Loyalty_Account__c) shopper2LaMap.get(shopperId);
                        if (la != null) {
                            system.debug('la: '+la);
                            remainingLas.add(la);
                        }                       
                    }
                }
                // if we have work to do - resubmit
                if (remainingLas.isEmpty() == false) {
                    ID jobID = System.enqueueJob(new myJL_PostJoinHandler(remainingLas, attempts));
                    system.debug('jobID: '+jobID);                  
                }
            }
        }
    }

}