@isTest 
private class CustomerInformationPanelController_TEST {
    
   
    @istest static void getPrimaryContactRecordType(){
    	boolean IS_PRIMARY_CONTACT = true; 
    	boolean IS_NOT_PRIMARY_CONTACT = false; 
        
        Contact ctc = UnitTestDataFactory.createContact(1, true);
        ctc.recordTypeId = CommonStaticUtils.getContactRecordType(CommonStaticUtils.CONTACT_RT_DEVELEPER_NAME_OC).Id;
        update ctc;
            
        CustomerInformationPanelController controller = new CustomerInformationPanelController();
        controller.ContactRecord = ctc;
        System.assertEquals(IS_PRIMARY_CONTACT, controller.isPrimaryContact);
            
        ctc.recordTypeId = CommonStaticUtils.getContactRecordType(CommonStaticUtils.CONTACT_RT_DEVELEPER_NAME_S).Id;
        update ctc;
        System.assertEquals(IS_NOT_PRIMARY_CONTACT, controller.isPrimaryContact);
        
    }
    
     private static User getFrontOfficeUser(){
        User sysAdmin       = UnitTestDataFactory.getSystemAdministrator('admin1');
        User frontOfficeUser = UnitTestDataFactory.getFrontOfficeUser('Tester');
        List<User> uList = new List<User> {sysAdmin, frontOfficeUser};
            insert uList;
        
        System.runAs(sysAdmin) {
            jl_runvalidations__c jrv = new jl_runvalidations__c(Name = frontOfficeUser.Id, Run_Validations__c = true);
            insert jrv;     
        }
        return frontOfficeUser;
    }
    
}