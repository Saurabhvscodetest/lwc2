/******************************************************************************
* This Apex Batch is for updating SourceSystem__c field  on Contact object with johnlewis.com
* 
*******************************************************************************
*/
global class MYJLOrderingCustomerBatch implements Database.Batchable<SObject>{
   
    public static final String sourceSytem = 'johnlewis.com';
     
    global Database.QueryLocator start(Database.BatchableContext BC){
       
        System.debug('MYJLOrderingCustomerBatch - @Start');
       
        String RecordTypeName = CustomSettingsManager.getConfigSetting('ORDERING_CUSTOMER_RECORDTYPE').Text_Value__c;
        
        Config_Settings__c dataMigrationBatchLimitConfig = Config_Settings__c.getInstance('DATAMIGRATION_BATCHSIZE');
        
        String BatchLimit  =  dataMigrationBatchLimitConfig != null && dataMigrationBatchLimitConfig.Text_Value__c != null?
                                    dataMigrationBatchLimitConfig.Text_Value__c : '100000';
                                    
          //Add more filters                          
        String query = 'SELECT Id,SourceSystem__c FROM CONTACT WHERE RecordType.DeveloperName =:RecordTypeName  AND SourceSystem__c= NULL   ORDER BY Shopper_Id__c  LIMIT '+BatchLimit; 
        
        System.debug('MYJLOrderingCustomerBatch - @Query'+query);
        return Database.getQueryLocator(query);
    }
    
     
    global void execute(Database.BatchableContext BC, List<Contact> oCustomers){
         
        System.debug('MYJLOrderingCustomerBatch - @Execute');
         
       
        
        for(Contact con : oCustomers){
        	con.SourceSystem__c = sourceSytem;
        }
        
        update oCustomers;
        
    }
     
     
     global void finish(Database.BatchableContext bc){
        
         System.debug('MYJLOrderingCustomerBatch -  @Finish');
        
        String notificationBody = 'MYJLOrderingCustomerBatch job has run successfully';       
     
        Config_Settings__c notificationConfig = Config_Settings__c.getInstance('DATAMIGRATION_NOTIFICATION_ADDRESS');
        String notificationRecipientAddress = notificationConfig != null ? notificationConfig.Text_Value__c : 'connex.it.support@johnlewis.co.uk';
        List<String> reciepients = notificationRecipientAddress.split(';');
        
        EmailNotifier.sendNotificationEmail(reciepients,'MYJLOrderingCustomerBatch Result',notificationBody);
        
        
    }     
    
}