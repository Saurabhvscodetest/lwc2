/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 *
 *  001     13/11/14    NTJ MJL-1294    Add some more tests
 *  002     15/01/15    AG  MJL-1441    added check for AnonymousFlag
 *  003     26/02/15    NTJ MJL-1584    Ensure isMyJLRegistered gets set
 *  004     29/06/15    NTJ     MJL-2100 - Use Contact_Profile__c.AnonymousFlag2__c
 *  005     08/07/16    NA              Decommission Contact Profile 
 */
@isTest 
private class MyJL_GetCustomerHandlerTest {
    static {
        BaseTriggerHandler.addSkipReason(CalloutHandler.SKIP_ALL_NOTIFICATIONS_CODE, 'disable outbound events in this test class');
    }

    private static Potential_Customer__c createLimitedCustomer(String customerId, String eMailAddress, Boolean anonymousFlag) {
        Potential_Customer__c limitedCustomer = new Potential_Customer__c(Shopper_ID__c = customerId, Email__c = eMailAddress, Source_System__c = Label.MyJL_JL_comSourceSystem);
        if (null != anonymousFlag) {
            limitedCustomer.AnonymousFlag__c = anonymousFlag;
        }
        insert limitedCustomer;
        
        return limitedCustomer;
    }
    
    private static Contact createContactProfile(String customerId, String eMailAddress, String firstName, String lastName, Date dateOfBirth, Boolean anonymousFlag) {
        Contact contact = new Contact(FirstName = firstName, LastName = lastName,email=emailAddress,shopper_id__c = customerid,birthdate = dateofBirth,sourcesystem__c='johnlewis.com');  //<<005>> 
        if (null != anonymousFlag) { //<<005>> 
            contact.AnonymousFlag2__c = anonymousFlag; //<<005>> 
        } //<<005>> 
        insert contact;
        return contact;
        
       //<<005>>        
       /* Contact_Profile__c contactProfile = new Contact_Profile__c(Contact__c = contact.Id, Shopper_ID__c = customerId, Email__c = eMailAddress, FirstName__c = firstName, LastName__c = lastName, Date_of_Birth__c = dateOfBirth, SourceSystem__c = Label.MyJL_JL_comSourceSystem);
        if (null != anonymousFlag) {
            contactProfile.AnonymousFlag2__c = anonymousFlag;
        }
        insert contactProfile;
        
        return contactProfile;*/  //<<005>> 
    }
    
    //Validate messageId mirroring from request header
    static testMethod void requestHeaderPositiveTest() { 
        SFDCMyJLCustomerTypes.Customer[] testCustomerArray = new SFDCMyJLCustomerTypes.Customer[]{getTestCustomer('123')};
        
        list<String> sampleMessageIds = new list<String>{'a', 'A', '1', '1234567890', 'ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂº', '  abcdefghijklmnopqrstuvwxyz  ', '123456789 123456789 123456789 123456789 123456789'};
        
        //Test for all messageIds in List the same value is mirrored
        for(String sampleMessageId : sampleMessageIds) {
            SFDCMyJLCustomerTypes.RequestHeaderType requestHeader = new SFDCMyJLCustomerTypes.RequestHeaderType();
            requestHeader.MessageId = getUniqueMessageId(sampleMessageId);
            
            SFDCMyJLCustomerTypes.CustomerRequestType fakeRequest = new SFDCMyJLCustomerTypes.CustomerRequestType();
            fakeRequest.RequestHeader = requestHeader;
            fakeRequest.Customer = testCustomerArray;
            
            //Call code
            SFDCMyJLCustomerTypes.GetCustomerResponseType fakeResponse = MyJL_GetCustomerHandler.process(fakeRequest);
            
            //Assert we get the same value back with a Success status of true
            System.assertEquals(fakeResponse.ResponseHeader.ResultStatus.Success, true);
            System.assertEquals(requestHeader.MessageId, fakeResponse.ResponseHeader.MessageId);
        }     
    }
    
    //Test missing request header
    static testMethod void requestHeaderNegativeTest1() {
        SFDCMyJLCustomerTypes.Customer[] testCustomerArray = new SFDCMyJLCustomerTypes.Customer[]{getTestCustomer('123')};
        
        SFDCMyJLCustomerTypes.CustomerRequestType fakeRequest = new SFDCMyJLCustomerTypes.CustomerRequestType();
        fakeRequest.Customer = testCustomerArray;
        //fakeRequest.RequestHeader.MessageId = 'fake-message-id';
        
        //Call code
        SFDCMyJLCustomerTypes.GetCustomerResponseType fakeResponse = MyJL_GetCustomerHandler.process(fakeRequest);
        
        //Assert we get an error back when sending no messageId
        System.assertEquals(fakeResponse.ResponseHeader.ResultStatus.Success, false);
        System.assertEquals(fakeResponse.ResponseHeader.ResultStatus.Code, MyJL_Const.ERRORCODE.MESSAGEID_NOT_PROVIDED.name());
    }
    
    //Test with empty or invalid messageIDs
    static testMethod void requestHeaderNegativeTest2() {
        SFDCMyJLCustomerTypes.Customer[] testCustomerArray = new SFDCMyJLCustomerTypes.Customer[]{getTestCustomer('123')};
        
        //Empty, tab, space, null
        list<String> sampleMessageIds = new list<String>{'', '  ', ' ', null};  
        
        //Test for all messageIds in List the value is not accepted
        for(String sampleMessageId : sampleMessageIds) {
            SFDCMyJLCustomerTypes.RequestHeaderType requestHeader = new SFDCMyJLCustomerTypes.RequestHeaderType();
            requestHeader.MessageId = sampleMessageId;
            
            SFDCMyJLCustomerTypes.CustomerRequestType fakeRequest = new SFDCMyJLCustomerTypes.CustomerRequestType();
            fakeRequest.RequestHeader = requestHeader;
            fakeRequest.Customer = testCustomerArray;
            
            //Call code
            SFDCMyJLCustomerTypes.GetCustomerResponseType fakeResponse = MyJL_GetCustomerHandler.process(fakeRequest);
            
            //Assert we get an error back when sending an empty messageId
            System.assertEquals(fakeResponse.ResponseHeader.ResultStatus.Success, false);
            System.assertEquals(fakeResponse.ResponseHeader.ResultStatus.Code, MyJL_Const.ERRORCODE.MESSAGEID_NOT_PROVIDED.name());
        }     
    }
    
    //Test number of customers going in is identical to number of customers/statuses coming back
    static testMethod void requestCustomerCountTest() {
        //Customer without CustomerID value
        SFDCMyJLCustomerTypes.Customer testCustomer1 = new SFDCMyJLCustomerTypes.Customer();
        
        //Customer with CustomerID value but no ID in it
        SFDCMyJLCustomerTypes.Customer testCustomer2 = new SFDCMyJLCustomerTypes.Customer();
        testCustomer2.CustomerID = new SFDCMyJLCustomerTypes.Identity();
        
        //Customer with CustomerID, and an ID field in it, which is empty
        SFDCMyJLCustomerTypes.Customer testCustomer3 = new SFDCMyJLCustomerTypes.Customer();
        MyJL_Util.setShopperId(testCustomer3, '');
                            
        //Customer with CustomerID, and an ID field in it, with a non-existing value
        SFDCMyJLCustomerTypes.Customer testCustomer4 = new SFDCMyJLCustomerTypes.Customer();
        MyJL_Util.setShopperId(testCustomer4, '-1');
        
        //Put all 4 test customers in an array
        SFDCMyJLCustomerTypes.Customer[] testCustomerArray = new SFDCMyJLCustomerTypes.Customer[]{testCustomer1, testCustomer2, testCustomer3, testCustomer4};
        
        //Generate new messageId (hopefully unique) by taking current time in ms
        SFDCMyJLCustomerTypes.RequestHeaderType requestHeader = new SFDCMyJLCustomerTypes.RequestHeaderType();
        requestHeader.MessageId = '' + (Datetime.now()).getTime();
            
        SFDCMyJLCustomerTypes.CustomerRequestType fakeRequest = new SFDCMyJLCustomerTypes.CustomerRequestType();
        fakeRequest.RequestHeader = requestHeader;
        fakeRequest.Customer = testCustomerArray;
            
        //Call code
        SFDCMyJLCustomerTypes.GetCustomerResponseType fakeResponse = MyJL_GetCustomerHandler.process(fakeRequest);
            
        //Assert the call was succesfull and we get the same number of records back
        System.assertEquals(fakeResponse.ResponseHeader.ResultStatus.Success, true);
        System.assertEquals(fakeResponse.ResponseHeader.MessageId, requestHeader.MessageId);
        System.assertEquals(fakeResponse.ActionRecordResults.size(), fakeRequest.Customer.size());
    }
    
    //Validate we get the customer back we requested even if the customer doesn't exist - negative path
    static testMethod void requestCustomerLimitedCustomer() {
        //Prepare test data
        String customerId = 'requestCustomerLimitedCustomer_1';
        String eMailAddress = 'requestCustomerLimitedCustomer_2@example.com';
        createLimitedCustomer(customerId, eMailAddress, true);
        
        //Generate new messageId (hopefully unique) by taking current time in ms
        SFDCMyJLCustomerTypes.RequestHeaderType requestHeader = new SFDCMyJLCustomerTypes.RequestHeaderType();
        requestHeader.MessageId = getUniqueMessageId(null);
            
        SFDCMyJLCustomerTypes.CustomerRequestType fakeRequest = new SFDCMyJLCustomerTypes.CustomerRequestType();
        fakeRequest.RequestHeader = requestHeader;
        SFDCMyJLCustomerTypes.Customer customer = getTestCustomer(customerId);

        fakeRequest.Customer = new SFDCMyJLCustomerTypes.Customer[]{customer};
            
        //Call code
        SFDCMyJLCustomerTypes.GetCustomerResponseType fakeResponse = MyJL_GetCustomerHandler.process(fakeRequest);
        
        //Assert the call was succesfull, wth the same messageId mirrored in the header, with the requested Limited customer including birthdate
        System.assertEquals(fakeResponse.ResponseHeader.ResultStatus.Success, true, 'Header was expected to have a Success state of true');
        System.assertEquals(fakeResponse.ResponseHeader.MessageId, requestHeader.MessageId, 'Header was expected to mirror the input messageId back');
        System.assertEquals(fakeResponse.ActionRecordResults.size(), fakeRequest.Customer.size(), 'The number of returned Customer array records was expected to be the same as the number of input records');
        System.assertEquals(fakeResponse.ActionRecordResults[0].Success, true, 'Detail was expected to have a Success state of true');

        SFDCMyJLCustomerTypes.CustomerRecordResultType res = fakeResponse.ActionRecordResults[0];
        System.assertEquals(true, res.Customer.AnonymousFlag, 'Expected AnonymousFlag to be set in response for this customer');
        
        
        //TODO check if we get emailaddress back as well
    }
    
    static testMethod void requestCustomerContactProfile() {
        //Prepare test data
        String customerId = 'requestCustomerContactProfile_1';
        String eMailAddress = 'requestCustomerContactProfile_1@example.com';
        String firstName = 'John';
        String lastName = 'Doe';
        Date dateOfBirth = Date.newInstance(1978, 04, 17);
        createContactProfile(customerId, eMailAddress, firstName, lastName, dateOfBirth, true);
        
        //Generate new messageId (hopefully unique) by taking current time in ms
        SFDCMyJLCustomerTypes.RequestHeaderType requestHeader = new SFDCMyJLCustomerTypes.RequestHeaderType();
        requestHeader.MessageId = getUniqueMessageId(null);
            
        SFDCMyJLCustomerTypes.CustomerRequestType fakeRequest = new SFDCMyJLCustomerTypes.CustomerRequestType();
        fakeRequest.RequestHeader = requestHeader;
        SFDCMyJLCustomerTypes.Customer customer = getTestCustomer(customerId);
        
        fakeRequest.Customer = new SFDCMyJLCustomerTypes.Customer[]{customer};
            
        //Call code
        SFDCMyJLCustomerTypes.GetCustomerResponseType fakeResponse = MyJL_GetCustomerHandler.process(fakeRequest);
        system.debug('fakeResponse.ActionRecordResults'+fakeResponse.ActionRecordResults);
        system.debug('fakeResponse.ActionRecordResults[0].Success'+fakeResponse.ActionRecordResults[0].Success);
        
        //Assert the call was succesfull, wth the same messageId mirrored in the header, with the requested Limited customer including birthdate
        System.assertEquals(fakeResponse.ResponseHeader.ResultStatus.Success, true, 'Header was expected to have a Success state of true');
        System.assertEquals(fakeResponse.ResponseHeader.MessageId, requestHeader.MessageId, 'Header was expected to mirror the input messageId back');
        System.assertEquals(fakeResponse.ActionRecordResults.size(), fakeRequest.Customer.size(), 'The number of returned Customer array records was expected to be the same as the number of input records');
        System.assertEquals(fakeResponse.ActionRecordResults[0].Success, true, 'Detail was expected to have a Success state of true');
        
        SFDCMyJLCustomerTypes.CustomerRecordResultType res = fakeResponse.ActionRecordResults[0];
        System.assertEquals(true, res.Customer.AnonymousFlag, 'Expected AnonymousFlag to be set in response for this customer');

        //TODO check if we get firstname/lastname back as well
    }
    
    
    private static SFDCMyJLCustomerTypes.Customer getTestCustomer(String customerId) {
        SFDCMyJLCustomerTypes.Customer customer = new SFDCMyJLCustomerTypes.Customer();
        MyJL_Util.setShopperId(customer, customerId);
        
        return customer;
    }
    
    private static String getUniqueMessageId(String input) {
        String output = (input != null ? input : '') + '_' + (Datetime.now()).getTime();
        
        return output;
    }
    
    // 001 new tests
    private static testmethod void test_getStackTrace() {
        MyJL_GetCustomerHandler.GetCustomerWrapper gcw = new MyJL_GetCustomerHandler.GetCustomerWrapper();
        
        gcw.setStackTrace('test');
        system.assertEquals('test',gcw.getStackTrace());
    }
    
    private static testmethod void test_constructor() {
        MyJL_GetCustomerHandler gch = new MyJL_GetCustomerHandler();
        system.assertNotEquals(null, gch);
    }
     
    private static testmethod void test_queryLoyaltyCards() {
        Contact contact = new Contact(LastName = 'Test');
        Database.insert(contact);
        system.assertNotEquals(null, contact);

        Contact_Profile__c profile = new Contact_Profile__c(Contact__c = contact.Id, SourceSystem__c = 'johnlewis.com');
        Database.insert(profile);
        system.assertNotEquals(null, profile);

        Loyalty_Account__c account = new Loyalty_Account__c(IsActive__c = false, Customer_Profile__c = profile.Id);
        Database.insert(account);
        system.assertNotEquals(null, account);
        
        Loyalty_Card__c card = new Loyalty_Card__c(Loyalty_Account__c = account.Id, Name = 'test-card-id');
        Database.insert(card);
        system.assertNotEquals(null, card);

        // make a set
        Set<Id> loyaltyAccountSet = new Set<Id>();
        loyaltyAccountSet.add(account.Id);

        Loyalty_Card__c[] lcList = MyJL_GetCustomerHandler.queryLoyaltyCards(loyaltyAccountSet);
        system.assertEquals(1, lcList.size());
        system.assertEquals(card.Id, lcList[0].Id);     
    }
    
    private static testmethod void test_isMyJLRegistered() {
        String customerId = 'xyz1';
        
        Contact contact = new Contact(LastName = 'TestRegistered');
        Database.insert(contact);
        system.assertNotEquals(null, contact);

        Contact_Profile__c profile = new Contact_Profile__c(Contact__c = contact.Id, SourceSystem__c = 'johnlewis.com', Shopper_ID__c=customerId);
        Database.insert(profile);
        system.assertNotEquals(null, profile);

        Loyalty_Account__c account = new Loyalty_Account__c(IsActive__c = false, Customer_Profile__c = profile.Id, ShopperID__c=customerId);
        Database.insert(account);
        system.assertNotEquals(null, account);
        
        Loyalty_Card__c card = new Loyalty_Card__c(Loyalty_Account__c = account.Id, Name = 'test-card-id');
        Database.insert(card);
        system.assertNotEquals(null, card);

        SFDCMyJLCustomerTypes.RequestHeaderType requestHeader = new SFDCMyJLCustomerTypes.RequestHeaderType();
        requestHeader.MessageId = getUniqueMessageId('unique1');

        SFDCMyJLCustomerTypes.CustomerRequestType fakeRequest = new SFDCMyJLCustomerTypes.CustomerRequestType();
        fakeRequest.RequestHeader = requestHeader;
        SFDCMyJLCustomerTypes.Customer customer = getTestCustomer(customerId);
        
        fakeRequest.Customer = new SFDCMyJLCustomerTypes.Customer[]{customer};
            
        //Call code
        SFDCMyJLCustomerTypes.GetCustomerResponseType fakeResponse = MyJL_GetCustomerHandler.process(fakeRequest);
        
        //Assert the call was succesfull, wth the same messageId mirrored in the header, with the requested Limited customer including birthdate
        System.assertEquals(fakeResponse.ResponseHeader.ResultStatus.Success, true, 'Header was expected to have a Success state of true');
        System.assertEquals(fakeResponse.ResponseHeader.MessageId, requestHeader.MessageId, 'Header was expected to mirror the input messageId back');
        System.assertEquals(fakeResponse.ActionRecordResults.size(), fakeRequest.Customer.size(), 'The number of returned Customer array records was expected to be the same as the number of input records');
        System.assertEquals(fakeResponse.ActionRecordResults[0].Success, true, 'Detail was expected to have a Success state of true');
        
        SFDCMyJLCustomerTypes.CustomerRecordResultType res = fakeResponse.ActionRecordResults[0];
        System.assertEquals(false, res.Customer.IsMyJLRegistered, 'Expected IsMyJLRegistered to be set false in response for this customer');
        
        // now positive test
        account.IsActive__c = true;
        update account;
        
       //Call code
        fakeResponse = MyJL_GetCustomerHandler.process(fakeRequest);
        
        //Assert the call was succesfull, wth the same messageId mirrored in the header, with the requested Limited customer including birthdate
        System.assertEquals(fakeResponse.ResponseHeader.ResultStatus.Success, true, 'Header was expected to have a Success state of true');
        System.assertEquals(fakeResponse.ResponseHeader.MessageId, requestHeader.MessageId, 'Header was expected to mirror the input messageId back');
        System.assertEquals(fakeResponse.ActionRecordResults.size(), fakeRequest.Customer.size(), 'The number of returned Customer array records was expected to be the same as the number of input records');
        System.assertEquals(fakeResponse.ActionRecordResults[0].Success, true, 'Detail was expected to have a Success state of true');
        
        res = fakeResponse.ActionRecordResults[0];
        System.assertEquals(true, res.Customer.IsMyJLRegistered, 'Expected IsMyJLRegistered to be set true in response for this customer');
        
        
    }
}