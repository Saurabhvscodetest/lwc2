/**
 *  @author  : Yousuf Mohammad
 *  @Date    : 10-06-2016
 *  @Purpose : This class will be used to return as an faiulure service object where error code and error messages are wrapped together.
 */
global with sharing class ExceptionServiceFailResponse  extends ServiceResponse 	{ 
	    //Error Message
        global String message					{get;set;} 
        //Error Code
        global String errorCode					{get;set;}
      
         /*
         * Constructor
         *
         * @param errorCode : error code as input parameter.
         * @param message   : error message as input parameter.
         */
        global ExceptionServiceFailResponse (String errorCode, String message)	{
            this.message = message;
            this.errorCode = errorCode;
        }
}