/******************************************************************************
* @author       Antony John
* @date         10/10/2015
* @description  Base class for Exception Handlers.
*
* LOG     DATE        Author    JIRA                            COMMENT
* ===     ==========  ======    ========================        ============ 
* 001     10/10/2015  AJ        CMP-39, CMP-42                  Initial code
*
*/
public abstract with sharing class EHBaseExceptionHandler { 
    public static final Id queryCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(EHConstants.RECORD_TYPE_CASE_QUERY).getRecordTypeId();
    public static final Id complaintCaseRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(EHConstants.RECORD_TYPE_CASE_COMPLAINT).getRecordTypeId();
    public static final Id eventHubGVFTaskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get(EHConstants.RECORD_TYPE_TASK_EVENT_HUB_GVF).getRecordTypeId();
    public static final Id eventHubCCTaskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get(EHConstants.RECORD_TYPE_TASK_EVENT_HUB_CC).getRecordTypeId();
    public static final Id eventHubSDTaskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get(EHConstants.RECORD_TYPE_TASK_EVENT_HUB_SD).getRecordTypeId();
	public static final Id supersededRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get(EHConstants.RECORD_TYPE_CONTACT_SUPERSEDED).getRecordTypeId();
	public static String cList;
    
    public static Map<ID, RecordType> caseRecordTypeIds = null; 
    
    static {
        List<String> caseRecordTypeList  = new List<String>{EHConstants.RECORD_TYPE_CASE_QUERY, EHConstants.RECORD_TYPE_CASE_COMPLAINT};
        caseRecordTypeIds = new Map<ID, RecordType>([select id from RecordType where DeveloperName in :caseRecordTypeList]);
    }
    
    /* Abstract methods which the child class implements to hanlde the exception based on the delivery type */
    public abstract EventHubVO.EHExceptionResponse handleEventHubExceptions(EventHubVO.EHExceptionRequest request);
    public abstract EventHubVO.EHExceptionResponse addTaskToExistingcase(EventHubVO.EHExceptionRequest request);
    
    /*Provides a new Task with basic Information*/
    public static Task getTask(Case caseObj, EventHubVO.EHExceptionRequest request) {
    	Task t = new Task();
        t.WhatId = caseObj.id;
        t.WhoId = caseObj.contactid;
        t.ActivityDate = Date.today();
        t.status = EHConstants.STATUS_NEW;
        t.jl_created_from_case__c = false;
        //t.EH_KEY__c = request.ehExceptionId;
        t.External_Exception_ID__c = request.ehExceptionId;
        t.External_Exception_Name__c =  request.ehExceptionName;
        t.Customer_Full_Name__c = request.customerName;
        t.type = EHConstants.EVENT_HUB ;
        t.subject = EHConstants.EVENT_HUB + EHConstants.SPLITTER + request.ehExceptionName;
        EHExceptionMapper.EHExceptionToCaseInformation  exceptionInfo = EHExceptionMapper.getDescriptionForException(request.deliveryType,request.ehExceptionName);
        t.Description = (exceptionInfo != null) ? exceptionInfo.taskDescription : '';
        t.jl_Description__c = (exceptionInfo != null) ? exceptionInfo.taskDescription : '';
        return t;
    }
    
    /* Returns an existing case for the matching criteria */
    public Case getExistingCase(String uniqueKey){
         List<case> caseList = new List<case>([SELECT Id, contactid, isClosed FROM Case WHERE EH_KEY__c =: uniqueKey]);
         return caseList[0];
    }
    
    /*Provides a new case with basic Information*/
    public static Case populateCase(String expName, String deliveryType, EventHubVO.EHExceptionRequest request){  
        Case c = new Case();
        EHExceptionMapper.EHExceptionToCaseInformation  exceptionToCaseInfo = EHExceptionMapper.getDescriptionForException(deliveryType,expName);
        c.Subject = EHConstants.EVENT_HUB + ' ' +EHConstants.CASE_LITERAL;
        c.RecordTypeId = (exceptionToCaseInfo != null && EHExceptionMapper.CaseType.QUERY.equals(exceptionToCaseInfo.ehCasetype)) ? queryCaseRecordTypeId: complaintCaseRecordTypeId;
        c.Reference_Type__c = deliveryType;
        c.Origin = EHConstants.EVENT_HUB;
        c.jl_Action_Taken__c = EHConstants.NEW_CASE_CREATED;
        c.jl_InitiatingSource__c = EHConstants.EVENT_HUB;
        c.EH_Exception_Name__c = deliveryType + EHConstants.SPLITTER  +  expName;
        c.setOptions(getDmlOptions());
    
       try{
        //Check for contact associated with the case
        List<Contact> contactList = searchContactsRelatedToCase(request);
            if(contactList.size() > 0 ){
                c = getContactForAssigningCase(contactList, c);
            }else{
                c = createNewContactforCase(c,request);
            }
        }catch(Exception e){
            return c;
        }
        return c;
    }
    
    public static Case getContactForAssigningCase(List<Contact> contactList, Case c){
                 
        if (contactList.size() == 1){
            c.ContactId = contactList[0].id;
            return c;
        }
        
        for(Contact con :contactList){
            if(String.isBlank(cList)){
                cList = con.id;
                continue;
            }
            cList = cList +','+con.id;   
        }
        c.Contact_ID_List__c = cList;
      
        return c;
    }
    
    
    /*Gets the success response for the provided task*/
     public static EventHubVO.EHExceptionResponse getSucessResponse(Task t, String exceptionID) {
        EventHubVO.EHExceptionResponse response = new EventHubVO.EHExceptionResponse();
         try{
        if (t != null) {
            response.taskId = t.id;
        	List<case> caselst = [Select CaseNumber, (Select WhatID from Tasks) from Case WHERE Id =: t.WhatId];
        
            if(caselst[0].CaseNumber != NULL){
               response.caseId = caselst[0].CaseNumber; 
            }
   
        }
             
        if(exceptionID != NULL){
           response.exceptionId = exceptionID; 
        }
               
         }
            Catch (Exception e){
           response.exceptionId = exceptionID; 
        System.debug('Exception Occurred in EHBaseExceptionHandler');
            }
        
        response.status = EHConstants.RESPONSE_SUCCESS;
        return response;                
        
    }


    /*Provides the basic DML operations for the assignment rules */
    public static Database.DMLOptions getDmlOptions(){
        List<AssignmentRule> ARList = new List<AssignmentRule>();
        ARList = [select id from AssignmentRule where SobjectType =: EHConstants.CASE_LITERAL and Active = true limit 1];
        
        //Creating the DMLOptions for "Assign using active assignment rules" checkbox
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        
        if(ARList.size() > 0){
            dmlOpts.assignmentRuleHeader.assignmentRuleId= ARList.get(0).id;
        }
        return dmlOpts;
    }
    
    /*Gets the error response */
    public static EventHubVO.EHExceptionResponse getErrorResponse(String message, String exceptionId){
        EventHubVO.EHExceptionResponse response = null;
        if(String.isNotBlank(message)){
            response = new EventHubVO.EHExceptionResponse();
            response.status = EHConstants.RESPONSE_ERROR ;
            response.exceptionId = exceptionId;
            response.errorCode = EHConstants.STATUS_NEW;
            response.errorDescription = message;
            
        }
        return response;
    }
    
    /*Gets the Contact associated with the Case*/
    public static List<Contact> searchContactsRelatedToCase(EventHubVO.EHExceptionRequest request){
        String emailString;
        String shopperString;
        String lastName;
        String postCode;
        List<Contact> conList = new List<Contact>();
        if(request.contactInfo != null){
        if(!String.isBlank(request.contactInfo.customerEmail)){
             emailString = request.contactInfo.customerEmail;
        }
        
        if(!String.isBlank(request.contactInfo.shopperId)){
             shopperString = request.contactInfo.shopperId;
        }
        
        if(!String.isBlank(request.contactInfo.lastName)){
             lastName = request.contactInfo.lastName;
        }
        
        if(!String.isBlank(request.contactInfo.customerPostCode)){
             postCode = request.contactInfo.customerPostCode;
        }
        String phoneHome = null;
        String phoneOther = null;
        String phoneMobile = null;
        
        
        List<String> numberSearch = new List<String>();
        
        //Extract last 10 numbers
        if(!String.isBlank(request.contactInfo.customerPhoneHome)){
            phoneHome = CommonStaticUtils.removeContactPrefix(request.contactInfo.customerPhoneHome);
            numberSearch.add('HomePhone LIKE \'%'+phoneHome+'\'');
        }
        if(!String.isBlank(request.contactInfo.customerPhoneOther)){
            phoneOther = CommonStaticUtils.removeContactPrefix(request.contactInfo.customerPhoneOther);
            numberSearch.add('OtherPhone LIKE \'%'+phoneOther+'\'');
        } 
        
        if(!String.isBlank(request.contactInfo.customerPhoneMobile)){
            phoneMobile = CommonStaticUtils.removeContactPrefix(request.contactInfo.customerPhoneMobile);
            numberSearch.add('MobilePhone LIKE \'%'+phoneMobile+'\'');
        }

        string realPhoneNumberSearch = String.join(numberSearch, ' OR ');
        
         String queryString = null;
        
         //If Shopper Id is Present
        if(!String.isBlank(shopperString)){        
            List<Contact> shopperList = [Select id from Contact where Shopper_ID__c = :shopperString AND RecordTypeId != :supersededRecordTypeId];
            if(!shopperList.isEmpty()){
                return shopperList;
            }
        }
        
        //If Email is Present
        if(!String.isBlank(emailString) && lastName != null){
            List<Contact> emailList = [Select id from Contact where Email =: emailString AND lastName =: lastName AND RecordTypeId != :supersededRecordTypeId];
            if(!emailList.isEmpty()){
                return emailList;
            }
        }
        
        //IF all other contact information fields are present
        if(lastName != null && postCode != null && (request.contactInfo.customerPhoneHome != null || request.contactInfo.customerPhoneOther != null || request.contactInfo.customerPhoneMobile != null)){
            queryString = 'Select id from Contact where lastName = :lastName AND MailingPostalCode = :postCode AND RecordTypeId != :supersededRecordTypeId AND ('+ realPhoneNumberSearch +')';
            conList = Database.query(queryString);
        }
       }
        return conList;
    }

    
    public static Case createNewContactforCase(Case c, EventHubVO.EHExceptionRequest request ){
        try{
            Contact con = new Contact();
            con.Salutation = request.contactInfo.salutation;
            con.lastName = request.contactInfo.lastName;
            con.FirstName= request.contactInfo.firstName;
            con.Email = request.contactInfo.customerEmail;
            con.MailingPostalCode = request.contactInfo.customerPostCode;
            con.MailingStreet = request.contactInfo.customerMailingStreet;
            con.MailingCity = request.contactInfo.customerMailingCity;
            con.MailingCountry = request.contactInfo.customerMailingCountry;
            con.MailingState = request.contactInfo.customerMailingState;
            con.HomePhone = request.contactInfo.customerPhoneHome;
            con.OtherPhone = request.contactInfo.customerPhoneOther;
            con.MobilePhone = request.contactInfo.customerPhoneMobile;
            insert con;
            
            c.ContactId = con.id;
            return c;
        }catch(Exception e){
            System.debug('The following exception has occurred: ' + e.getMessage());
            return c;   
        }
        return c;
    }
}