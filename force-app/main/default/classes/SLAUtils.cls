/*******************************************************************************
* @author       ????
* @date         ??
* @description  Utility Class used to set Case SLA - COPIED FROM ORIGINAL clsSLA class  
*
Edit    Date        Author      Comment
001     ??/??/??    ??          Original Class
002     17/04/15    Ed Sandoval CMP-65 Email Triage cases for CAPITA need to have 24-hour elapsed SLA and ignore business hours
003     05/10/15    MP			COPT-312 Set SLA for PSE Triage Cases.
004     24/11/15    SA			COPT-477/323 Extended Business Hours Considered
005     02/03/16    MP			COPT-655 Refactoring

******************************************************************************/
public class SLAUtils {

	private static Map<String, Boolean> actionResetsSLASettings = new Map<String, Boolean>();
	private static Map<String, BusinessHours> businessHoursMapByName = new Map<String, BusinessHours>();
	private static final String DEFAULT_BUSINESS_HOURS_NAME = 'Default';
	private static final String WEB_TRIAGE_JL_CONTACT_CENTRE = 'Web Triage JL.com';
	private static final String WEB_TRIAGE_BRANCH_CONTACT_CENTRE = 'Web Triage Branch';
	private static final String WEB_TRIAGE_NKU_CONTACT_CENTRE = 'NKU';
	private static final String JL_BRANCH = 'JohnLewis.com';
	private static final String SLA_BREACH = 'Breach';
	private static final String SLA_WARN = 'Warn';
	private static final String ACTION_CLEARING_NEXT_RESPONSE = 'Case resolved';
	private static final Id queryCaseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonStaticUtils.CASE_RT_NAME_QUERY).getRecordTypeId();
    private static final Id complaintCaseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonStaticUtils.CASE_RT_NAME_COMPLAINT).getRecordTypeId();
    private static final Id pseCaseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonStaticUtils.CASE_RT_NAME_PRODUCT_STOCK_ENQUIRY).getRecordTypeId();
     private static final Id oeCaseRTId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(CommonStaticUtils.CASE_RT_NAME_OE).getRecordTypeId();

	private static final Set<Id> SLA_RECORD_TYPES = new Set<Id> {queryCaseRTId,complaintCaseRTId,pseCaseRTId,oeCaseRTId};

	public static final Datetime CURRENT_DATE_TIME = Datetime.now(); // Provide a consistent 'now'

	/**
	 * Set the SLA for the given case using the Team, User and details supplied.
	 * @params:	Case thisCase			The case needing the SLA setting. 
	 * 			String team				The team to find the SLA for. 
	 * 			Id userid				The detail string to be added to the task. 
	 * 			Boolean isNewCase		Denotes if the case is a new or existing one. 
	 * 			Boolean isNewReopen		Denotes if the case is newly reopened. 
	 * @rtnval:	void
	 */
	public static void setSLA(Case thisCase, String team, Id userid, Boolean isNewCase, Boolean isNewReopen) {
		// Contact Us Webform Web Triage SLA
		if (thisCase.Web_Form__c != null && thisCase.Web_Form__c == CommonStaticUtils.CONTACT_US_WEBFORM && thisCase.RecordTypeId != pseCaseRTId) {
			Contact_Centre__c emailContactCentre;
			
			if (thisCase.jl_Branch_master__c != null && thisCase.jl_Branch_master__c == JL_BRANCH) {
				emailContactCentre = Contact_Centre__c.getValues(WEB_TRIAGE_JL_CONTACT_CENTRE);
			} else {
				emailContactCentre = Contact_Centre__c.getValues(WEB_TRIAGE_BRANCH_CONTACT_CENTRE);
			}
			thisCase.jl_First_Response_By__c = addSLAForContactCentre (CURRENT_DATE_TIME, emailContactCentre, SLA_BREACH);
			thisCase.jl_First_Response_By_Warning__c = addSLAForContactCentre (CURRENT_DATE_TIME, emailContactCentre, SLA_WARN);
			return;
		}

		// <<003>> if (thisCase.Web_Form__c != null && thisCase.Web_Form__c == 'NKU') 
		// NKU Webform Web Triage SLA
		if (thisCase.Web_Form__c != null && thisCase.Web_Form__c == CommonStaticUtils.NKU_WEBFORM && thisCase.RecordTypeId != pseCaseRTId) {
			Contact_Centre__c emailContactCentre = Contact_Centre__c.getValues(WEB_TRIAGE_NKU_CONTACT_CENTRE);
			thisCase.jl_First_Response_By__c = addSLAForContactCentre (CURRENT_DATE_TIME, emailContactCentre, SLA_BREACH);
			thisCase.jl_First_Response_By_Warning__c = addSLAForContactCentre (CURRENT_DATE_TIME, emailContactCentre, SLA_WARN);
			return;
		}
		
		if (!SLA_RECORD_TYPES.contains(thisCase.RecordTypeId)) {
			return; // no SLAs are relevant
		}		
		
		if (String.isNotBlank(team)) {
			thisCase.jl_First_Response_By__c = addSLAForTeam (CURRENT_DATE_TIME, team, SLA_BREACH);
			thisCase.jl_First_Response_By_Warning__c = addSLAForTeam (CURRENT_DATE_TIME, team, SLA_WARN);
		} else if (userid != null) {
			thisCase.jl_First_Response_By__c = addSLAForUser (CURRENT_DATE_TIME, userid, SLA_BREACH);
			thisCase.jl_First_Response_By_Warning__c = addSLAForUser (CURRENT_DATE_TIME, userid, SLA_WARN);
		} else {
			thisCase.jl_First_Response_By__c = addSLAForTeam (CURRENT_DATE_TIME, TeamUtils.DEFAULT_TEAM_NAME, SLA_BREACH);
			thisCase.jl_First_Response_By_Warning__c = addSLAForTeam (CURRENT_DATE_TIME, TeamUtils.DEFAULT_TEAM_NAME, SLA_WARN);
		}

		thisCase.SLATimeStamp__c = CURRENT_DATE_TIME;

		system.debug('@@exiting setSLA');
	}

	/**
	 * Add the SLA (warn or breach) date to the given case based on the supplied team.
	 * Use the default team if a match is not found in the Team__c custom setting.
	 * @params:	DateTime dt				Current DateTime set at beginning of setSLA. 
	 * 			String teamName			The team to find the SLA for. 
	 * 			String warnOrBreach		The Warn or Breach SLA being set. 
	 * @rtnval:	DateTime				The Warn or Breach date time value.
	 */
	private static DateTime addSLAForTeam(DateTime dt, String teamName, String warnOrBreach) {
		// if the team has an associated contact centre then we use the contact centre's SLA and Business Hours
		Contact_Centre__c contactCentre = TeamUtils.getContactCentreForTeam(teamName);
		if (contactCentre != null) {
			return addSLAForContactCentre (dt, contactCentre, warnOrBreach);
		}
		// default to returning the input datetime
		return addSLAForTeam(dt, TeamUtils.DEFAULT_TEAM_NAME, warnOrBreach);
	}

	/**
	 * Add the SLA (warn or breach) date to the given case based on the supplied user id.
	 * Use the default team if user has no team.
	 * @params:	DateTime dt				Current DateTime set at beginning of setSLA. 
	 * 			Id userid				The user to find a team and set the SLA. 
	 * 			String warnOrBreach		The Warn or Breach SLA being set. 
	 * @rtnval:	DateTime				The Warn or Breach date time value.
	 */
	private static DateTime addSLAForUser(DateTime dt, Id userid, String warnOrBreach) {
		system.debug('@@entering addSLAForUser');
		
		// getTeamForUser will return the default team details if no specific is defined
		Team__c team = TeamUtils.getTeamForUser(userid);
		
		if (team == null) {
			return addSLAForTeam(dt,'Default', warnOrBreach);
		}

		return addSLAForTeam (dt, team.name, warnOrBreach);
	}

	/**
	 * Add the SLA (warn or breach) date to the given case based on the supplied user id.
	 * Use the default team if user has no team.
	 * @params:	String action		The action to be checked for SLA reset.
	 * @rtnval:	Boolean				Yay or Nay for whether the action should reset the SLA.
	 */
	public static boolean actionResetsSLA(String action) {
		if (String.isBlank(action)) {
			return false;
		}

		String lowerAction = action.toLowerCase();
		
		// Construct static map if it's empty
		if (actionResetsSLASettings.isEmpty()) {
			Map<String,Config_Settings__c> allSettings = CustomSettingsManager.getAllConfigSettings();
			for (String s: allSettings.keySet()) {
				if (s.startsWith('CASE_ACTION:')) {
					String lowerPLValue = allSettings.get(s).Text_Value__c.toLowerCase();
					actionResetsSLASettings.put(lowerPLValue,allSettings.get(s).Checkbox_Value__c);
				}
			}
		}

		if (actionResetsSLASettings.containsKey(lowerAction)) {
 			return actionResetsSLASettings.get(lowerAction);
 		} else {
 			return false;
 		}
	}

	/**
	 * Returns true if the supplied action means the case is ready for closure.
	 * Called by clsCaseTriggerHandler.
	 * Use the default team if user has no team.
	 * @params:	String action		The action to be checked if it clears the next response fields.
	 * @rtnval:	Boolean				Yay or Nay for whether the action should reset the next response fields.
	 */
	public static boolean actionClearsNextResponse(String action) {
		System.debug('@@calling actionClearsNextResponse for action ' + action);
		
		if (String.isNotBlank(action) && ACTION_CLEARING_NEXT_RESPONSE.equals(action)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Returns the DateTime represented by the current date time (dt) plus the warn or breach minutes for the
	 * specified Contact Centre.
	 * Called by CaseTriggerHandler and this class.
	 * ES:17/04/15-CMP-65 - Introducing Option to ignore business hours when setting SLAs
	 * @params:	DateTime dt							The DateTime value to use for the calculation - usually DateTime.now().
	 * 			Contact_Centre__c contactCentre		The Contact Centre to use for the SLA calculation.
	 * 			String warnOrBreach					The Warn or Breach SLA being set.
	 * @rtnval:	DateTime							The Warn or Breach SLA value to be set.
	 */
	public static DateTime addSLAForContactCentre(DateTime dt, Contact_Centre__c contactCentre, String warnOrBreach) {
		String businessHoursName = contactCentre.Business_Hours__c;
		Integer intMinutes = 0;
			
		if (warnOrBreach == SLA_WARN) {
			intMinutes = (integer) contactCentre.SLA_Initial_Minutes_Warning__c;
		} else if (warnOrBreach == SLA_BREACH) {
			intMinutes = (integer) contactCentre.SLA_Initial_Minutes__c;
		}
		
		if (contactCentre != null && contactCentre.Ignore_Business_Hours_for_SLA_Minutes__c) {
			return (dt.addMinutes(intMinutes));
		} else {
			return addBusinessMinutes(dt, intMinutes, businessHoursName);
		}		
	}

	/**
	 * Returns the DateTime represented by the current date time (dt) plus the specified number of minutes
	 * taking into account the supplied business hours.
	 * @params:	DateTime dt							The DateTime value to use for the calculation - usually DateTime.now().
	 * 			Integer intMinutes					The Number of Minutes from the Contact Centre SLA (supplied by calling method).
	 * 			String businessHoursName			The Name of the Business Hours record to use. 
	 * @rtnval:	DateTime							The SLA value to be set.
	 */
    @testVisible
	private static DateTime addBusinessMinutes(DateTime dt, Integer intMinutes, String businessHoursName) {
		
		// if -ve value for intMinutes then we just do a straight calculation
		if (intMinutes < 0){
			return dt.addMinutes(intMinutes);
		}

		if (String.isBlank(businessHoursName)) {
			businessHoursName = DEFAULT_BUSINESS_HOURS_NAME;
		}
        system.debug('BUSINESS HOURS NAME: ' + businessHoursName);       							        								 										
		
		DateTime returnDateTime = null;
		//<04> 
        Date weekStartingDate = System.today().toStartofWeek();
		BusinessHours currentBusinessHours = getBusinessHours(businessHoursName, System.today());		
		Integer remainingRequiredMins = intMinutes; 		
		Integer remainingMinsInThisWeek = calculateRemainingBusinessHoursForWeekInMinutes(currentBusinessHours, CURRENT_DATE_TIME);		
        DateTime currentDateTime = dt;		
		do {								
			if (remainingRequiredMins <= remainingMinsInThisWeek) {				                                               
                returnDateTime = BusinessHours.addGmt(currentBusinessHours.id, currentDateTime, remainingRequiredMins * 60 * 1000); 
				break;					
			} else {
				remainingRequiredMins = remainingRequiredMins - remainingMinsInThisWeek;
				weekStartingDate = weekStartingDate.addDays(7).toStartofWeek();
				currentBusinessHours = getBusinessHours(businessHoursName, weekStartingDate);
				Time weekStartingTime = currentBusinessHours.MondayStartTime != null ? currentBusinessHours.MondayStartTime : Time.newInstance(0,0,0,0); 
				currentDateTime = DateTime.newInstance(weekStartingDate, weekStartingTime); 
				remainingMinsInThisWeek= calculateRemainingBusinessHoursForWeekInMinutes(currentBusinessHours,currentDateTime);
			}
		} while(true);
		
		Datetime paddedDatetime = Datetime.newInstance(returnDateTime.year(), returnDateTime.month(), returnDateTime.day(),
														returnDateTime.hour(), returnDateTime.minute(), 59);

		return paddedDatetime;
		//</04>
	}

	/**
	 * Creates a DateTime value based on the given Date and String (time) passed in.
	 * @params:	Date d							The Date to be used in the new DateTime value.
	 * 			String strTime					The Time to be used in the new DateTime value.
	 * @rtnval:	DateTime						The resulting DateTime value.
	 */
	public static DateTime createDateTime(Date d, String strTime) {
		if (d != null) {		
			Integer year = d.year();
			Integer month = d.month();
			Integer day = d.day();
			Integer hour = 0;
			Integer minute = 0;

			if (String.isNotBlank(strTime)) {
				String strHours = strTime.substring(0,2);
				String strMinutes = strTime.substring(3,5);
				hour = Integer.valueOf(strHours);
				minute = Integer.valueOf(strMinutes);
			}

			// Set 59 seconds padding to cater for edge Cases eg: Case created (SLA Timestamp) at 16:00:45 and user
			// selects next update for 18:00:{59}
			Datetime dt = DateTime.newInstance(year, month, day, hour, minute, 59); 
			return dt; 
		}
		return null;
	}

	/**
	 * Updates the First Response and Next Response fields on the supplied Case.
	 * If a new response date is suggested then this will determine if it's the first or subsequent response times that need updating.
	 * @params:	Case thisCase	The Case for which First Response and Next Response fields should be updated.
	 * @rtnval:	void
	 */
	public static void setNextResponse(Case thisCase) {
		if (thisCase.jl_Next_Response_Date__c != null) {
			System.debug('@@jl_First_Response_At__c is set - move response time for next response');
			DateTime dtNextResponse = SLAUtils.createDateTime(thisCase.jl_Next_Response_Date__c, thisCase.jl_Next_Response_Time__c);
			DateTime dtNextResponseWarning = dtNextResponse.addMinutes(-30);
			if (thisCase.jl_First_Response_At__c != null || SLAUtils.actionResetsSLA(thisCase.jl_Action_Taken__c)) {
				// we are past SLA checks, now just customer negotiated
				thisCase.jl_Next_Response_By__c = dtNextResponse;
				thisCase.jl_Next_Response_By_Warning__c = dtNextResponseWarning;
			} else {
				// we set SLA and next action statuses
				thisCase.jl_First_Response_By__c = dtNextResponse;
				thisCase.jl_First_Response_By_Warning__c = dtNextResponseWarning;
				thisCase.jl_Next_Response_By__c = dtNextResponse;
				thisCase.jl_Next_Response_By_Warning__c = dtNextResponseWarning;
			}
		}

		thisCase.SLATimeStamp__c = CURRENT_DATE_TIME;
	}

	public static void setFirstResponseAt(Case thisCase){

        if (thisCase.jl_First_Response_At__c == null && SLAUtils.actionResetsSLA(thisCase.jl_Action_Taken__c)) {
            thisCase.jl_First_Response_At__c = CURRENT_DATE_TIME;
        }
    }
	
	/**
	 * Returns the Business Hours record relating to the currentDate value.  This may be the record that
	 * matches the businessHoursName or a businessHoursName_day_month_year record (if non standard hours apply).
	 * @params:	String businessHoursName			The Name of the Business Hours record to use.
	 * 			Date currentDate					The Current Date to use to see if special business hours are required.
	 * @rtnval:	BusinessHours						The matching BusinessHours record.
	 */
	private static BusinessHours getBusinessHours(String businessHoursName, Date currentDate) {
		
		if (businessHoursMapByName.isEmpty()) {
			List<BusinessHours> businessHoursList = [SELECT Id, Name, TimeZoneSidKey, MondayStartTime, MondayEndTime, TuesdayStartTime, TuesdayEndTime, WednesdayStartTime, WednesdayEndTime, ThursdayStartTime, ThursdayEndTime, FridayStartTime, FridayEndTime, SaturdayStartTime, SaturdayEndTime, SundayStartTime, SundayEndTime FROM BusinessHours WHERE IsActive = true];       		

       		businessHoursMapByName = new Map<string, BusinessHours>();
       		for(BusinessHours bhItem : businessHoursList) {				
				businessHoursMapByName.put(bhItem.Name, bhItem);	
       		}       		
       	}
       	
       	Date weekStartDate = currentDate.toStartofWeek();		 
       	String dayFormat = weekStartDate.day() < 10 ? '0' + String.valueOf(weekStartDate.day()) : String.ValueOf(weekStartDate.day());
       	String monthFormat = weekStartDate.month() < 10 ? '0' + String.valueOf(weekStartDate.month()) : String.ValueOf(weekStartDate.month());
       	String extBusinessHoursName = businessHoursName + '_' + dayFormat + monthFormat + weekStartDate.year();       	       
       	if (businessHoursMapByName.containsKey(extBusinessHoursName)) {
       		system.debug('BH USING: ' + extBusinessHoursName);
       		return businessHoursMapByName.get(extBusinessHoursName);
       	} else if (businessHoursMapByName.containsKey(businessHoursName)) {
       		system.debug('BH USING: ' + businessHoursName);
       		return businessHoursMapByName.get(businessHoursName);
       	} else {
       		system.debug('BH USING: ' + DEFAULT_BUSINESS_HOURS_NAME);
       		return businessHoursMapByName.get(DEFAULT_BUSINESS_HOURS_NAME);
       	}
	}

	/**
	 * Returns an Integer representing the number of remaining business hours in minutes for the current week.
	 * @params:	String pBusinessHours			The Business Hours record to use for the calculations.
	 * 			Date currentDate				The DateTime value to base the calculation on.
	 * @rtnval:	Integer							The number of minutes remaining for the week.
	 */
	private static Integer calculateRemainingBusinessHoursForWeekInMinutes(BusinessHours pBusinessHours, DateTime startDateTime){				
		DateTime weekEndDateTime = DateTime.newInstance(startDateTime.Date().toStartofWeek().addDays(7), Time.newInstance(0,0,0,0)); 										
		return Integer.ValueOf((BusinessHours.diff(pBusinessHours.Id, startDateTime, weekEndDateTime) / 1000 / 60)); 	  			
	}
}