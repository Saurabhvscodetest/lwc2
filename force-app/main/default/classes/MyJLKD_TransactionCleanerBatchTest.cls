/**
 *	@author: Andrey Gavrikov (westbrook)
 *	@date: 2014-12-08 12:42:10 
 *	@description:
 *	    Test methods for MyJLKD_TransactionCleanerBatch
 *	
 *	Version History :   
 *	2014-12-08 - MJL-1182 - AG
 *	remove headers older than - MyJL Control Panel: Delete_Transactions_Older_than_N_Days
 *		
 */
@isTest
public class MyJLKD_TransactionCleanerBatchTest  {
	static {
		//prepare configuration
		Database.insert(new House_Keeping_Config__c(Name = 'MyJLKD_TransactionCleanerBatch', Class_Name__c = 'MyJLKD_TransactionCleanerBatch', 
													Order__c = 0, Run_Frequency_Days__c = 1));
	}

	/**
	 * check that transactions are deleted when custom setting is set
	 */
	static testMethod void testWithDelete () {
		System.runAs(MyJL_TestUtil.getTestUser('testWithDelete')) {
			CustomSettings.TestRecord rec = new CustomSettings.TestRecord();
			rec.put('Delete_Transactions_Older_than_N_Days__c', -1);
			CustomSettings.setTestMyJLSetting(UserInfo.getUserId(), rec);

			Database.insert(new Transaction_Header__c()); 

			Test.startTest();
			Database.executeBatch(new HouseKeeperBatch(new MyJLKD_TransactionCleanerBatch(), true));
			Test.stopTest();

			System.assertEquals(0, [select count() from Transaction_Header__c], 'Did not expect to find any transaction headers');
		}
	}

	/**
	 * check that transactions are NOT deleted when they are newer than configured threshold
	 */
	static testMethod void testWithoutDelete () {
		System.runAs(MyJL_TestUtil.getTestUser('testWithDelete')) {
			CustomSettings.TestRecord rec = new CustomSettings.TestRecord();
			rec.put('Delete_Transactions_Older_than_N_Days__c', 10);
			CustomSettings.setTestMyJLSetting(UserInfo.getUserId(), rec);

			Database.insert(new Transaction_Header__c()); 

			Test.startTest();
			final MyJLKD_TransactionCleanerBatch batch = new MyJLKD_TransactionCleanerBatch();
			Database.executeBatch(new HouseKeeperBatch(batch, true), batch.getBatchSize());
			Test.stopTest();

			System.assertEquals(1, [select count() from Transaction_Header__c], 'Did not expect to find that transaction is deleted');
		}
	}
}