/*
* @File Name   : DelTrackChatbotOrderNumberInvoke 
* @Description : Chatbot Invoke class to related to Delivery Queries Bot
* @Copyright   : Zensar
* @Trello #    : #TrackMyDelivery
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0       18-OCT-19        Vijay Ambati                   Created
*/

Public class DelTrackChatbotOrderNumberInvoke{
  
  Public class DelTrackChatBotRequest {
     @InvocableVariable(required= true)
     public String orderNumberInput;
   }
    
  Public class DelTrackChatBotResponse {
    
     @InvocableVariable(required= true)
       public String CaseNumber;            //CaseList
     @InvocableVariable(required= true)
       public String DeliverySlotFrom;
     @InvocableVariable(required= true)
       public String DeliverySlotTo;  
     @InvocableVariable(required= true)
       public String DeliveryDate;   
     @InvocableVariable(required= true)
       public String PostCode; 
     @InvocableVariable(required= true)
       public String DeliveryType; 
     @InvocableVariable(required= true)
       public String ExceptionStatus;     
     @InvocableVariable(required= true)
       public String CombinedOrder;    
     @InvocableVariable(required= true)
       public String CustomerName;
     @InvocableVariable(required= true)
       public String CurrentDateCheck;
     @InvocableVariable(required= true)
       public String CurrentDateTimeCheck;
     @InvocableVariable(required= true)
       public String CCAfterTimeCheck;
     
     @InvocableVariable(required= true)    //GVF Two Hour Slot variables
       public String GVFtwoHourSlot;
     @InvocableVariable(required= true)
       public String GVFtwoHourDPDate;
     @InvocableVariable(required= true)
       public String GVFtwoHourDPTime;
     @InvocableVariable(required= true)
       public String GVFtwoHourARTime;
    
     @InvocableVariable(required= true)    //DeliveryStatus
       public String GVFDeliveryStatus;
     @InvocableVariable(required= true)
       public String ActualCaseNumber;
     @InvocableVariable(required= true)
       public String GVFEightPMCheck;
     @InvocableVariable(required= true)
       public String GVFtwoHourARTimeCompare;
   }
    
    @Invocablemethod(Label ='Chatbot Order Number Method' description='Invoke Process Delivery Number Method')
    Public static List<DelTrackChatBotResponse> DelTrackChatBotProcess(List<DelTrackChatBotRequest > chatbotInputReqList){
        List<DelTrackChatBotResponse>   chatBotDelResponseList = new List<DelTrackChatBotResponse>();
        DelTrackChatBotResponse chatBotDelResponse = new DelTrackChatBotResponse();
         try{
           List<String> chatbotInputs = new List<String>();
            for( DelTrackChatBotRequest chatbotReq : chatbotInputReqList ){
           chatbotInputs.add ( chatbotReq.orderNumberInput );
         }
        
        String jsonResponse =  DelTrackChatBotUtils.chatBotFetchDeliveryDetails(null,chatbotInputs[0]);
        system.debug('@@@ jsonResponse' + jsonResponse);
               
        																								//return Output;
        DelTrackChatBotUtils.WrapperResponse returnWR = new DelTrackChatBotUtils.WrapperResponse(); 
       
        returnWR = (DelTrackChatBotUtils.WrapperResponse)Json.deserialize(jsonResponse, DelTrackChatBotUtils.WrapperResponse.class);
             
       if(returnWR.errorDescription != NULL){
                chatBotDelResponse.ExceptionStatus = returnWR.errorDescription;
        }
        else{
                  chatBotDelResponse.ExceptionStatus = 'Success';
            }
        
        if(chatBotDelResponse.ExceptionStatus == 'Success'){
           																			//Check for the Combined Order 
        if(returnWR.ccData == true && returnWR.gvfData == true){
            chatBotDelResponse.CombinedOrder = 'True';
        }
        else if(returnWR.ccData == true && returnWR.cdData == true){
            chatBotDelResponse.CombinedOrder = 'True';
        }
        else if(returnWR.ccData == true && returnWR.sdData == true){
            chatBotDelResponse.CombinedOrder = 'True';
        }
        else if(returnWR.sdData == true && returnWR.gvfData == true){
            chatBotDelResponse.CombinedOrder = 'True';
        }
        else if(returnWR.cdData == true && returnWR.gvfData == true){
            chatBotDelResponse.CombinedOrder = 'True';
        }
        else if(returnWR.cdData == true && returnWR.sdData == true){
            chatBotDelResponse.CombinedOrder = 'True';
        }
        else {
            chatBotDelResponse.CombinedOrder = 'False';
        }
            
        if(chatBotDelResponse.CombinedOrder == 'False'){
                
               if(returnWR.ccData == true){
            chatBotDelResponse.DeliveryType = 'ClickAndCollect';
            chatBotDelResponse.DeliveryDate = returnWR.CCavailableForCollDateAlone;
            if ( returnWR.relatedCaseList != null && returnWR.relatedCaseList.size () > 0 ) {
                String caseList = String.valueOf(returnWR.relatedCaseList.size ());
                chatBotDelResponse.CaseNumber = caseList;
            }else{
                chatBotDelResponse.CaseNumber = '';
            }
            
            chatBotDelResponse.CurrentDateCheck = returnWR.currentDateCheck;
            chatBotDelResponse.CurrentDateTimeCheck = returnWR.currentDateTimeCheck;
            
            chatBotDelResponse.CustomerName = returnWR.CCcustomerName;
            chatBotDelResponse.CCAfterTimeCheck = returnWR.CCAfterTimeCheck;
        }
        																						// GVF
        else if(returnWR.gvfData == true){
             chatBotDelResponse.DeliveryType = '2ManGVF';
             chatBotDelResponse.DeliveryDate = returnWR.deliverySlotToFormatted ;
            
            if ( returnWR.relatedCaseList != null && returnWR.relatedCaseList.size () > 0 ) {
                String caseList = String.valueOf(returnWR.relatedCaseList.size ());
                chatBotDelResponse.CaseNumber = caseList;
                chatBotDelResponse.ActualCaseNumber = returnWR.relatedCaseList[0].CaseNumber;
            }else{
                chatBotDelResponse.CaseNumber = '';
            }
            chatBotDelResponse.DeliverySlotFrom = returnWR.deliverySlotFromFormattedTime;
            chatBotDelResponse.DeliverySlotTo = returnWR.deliverySlotToFormattedTime;
            chatBotDelResponse.PostCode = returnWR.postCode;       
            chatBotDelResponse.CustomerName = returnWR.recipientName;
            chatBotDelResponse.CurrentDateCheck = returnWR.currentDateCheck;
            chatBotDelResponse.CurrentDateTimeCheck = returnWR.currentDateTimeCheck;
            
            chatBotDelResponse.GVFtwoHourSlot = returnWR.twoHourSlot;
            chatBotDelResponse.GVFtwoHourDPDate = returnWR.twoHourDPDate;
            chatBotDelResponse.GVFtwoHourDPTime = returnWR.twoHourDPTime;   
            chatBotDelResponse.GVFtwoHourARTime = returnWR.twoHourARTime;  
            chatBotDelResponse.GVFDeliveryStatus = returnWR.deliveryStatus;
            chatBotDelResponse.GVFEightPMCheck = returnWR.twoHour8PMTimeCheck;
            chatBotDelResponse.GVFtwoHourARTimeCompare = returnWR.twoHourARTimeCompare;
        }
       																						    // Carrier Delivery
        else if(returnWR.cdData == true){
            
            chatBotDelResponse.DeliveryType = 'CarrierDelivery';
            chatBotDelResponse.DeliveryDate = '' ;
            if ( returnWR.relatedCaseList != null && returnWR.relatedCaseList.size () > 0 ) {
                String caseList = String.valueOf(returnWR.relatedCaseList.size ());
                chatBotDelResponse.CaseNumber = caseList;
            }else{
                
                chatBotDelResponse.CaseNumber = '';
            }
            
            chatBotDelResponse.DeliverySlotFrom = '';
            chatBotDelResponse.DeliverySlotTo = '';
            chatBotDelResponse.PostCode = '';  
            chatBotDelResponse.CustomerName = returnWR.CDcustomerName;
          
            
        }
     																						    // Supplier Direct
        else if(returnWR.sdData == true){
            
            chatBotDelResponse.DeliveryType = 'SupplierDirect';
            chatBotDelResponse.DeliveryDate = '' ;
            if ( returnWR.relatedCaseList != null && returnWR.relatedCaseList.size () > 0 ) {
                String caseList = String.valueOf(returnWR.relatedCaseList.size ());
                chatBotDelResponse.CaseNumber = caseList;
            }else{
                
                chatBotDelResponse.CaseNumber = '';
            }
            
            chatBotDelResponse.DeliverySlotFrom = '';
            chatBotDelResponse.DeliverySlotTo = '';
            chatBotDelResponse.PostCode = '';  
            chatBotDelResponse.CustomerName = returnWR.SDCustomerName;
        }     
                
            }
        																						// Click and Collect
        }
        
        System.debug ('chatBotDelResponse ' + chatBotDelResponse );
            
        chatBotDelResponseList.add ( chatBotDelResponse );    
  
        }
        catch(Exception e){
             chatBotDelResponse.ExceptionStatus = 'Error';
             chatBotDelResponseList.add ( chatBotDelResponse );  
            System.debug('@@@ Inside error Chatbot order Invoke');
            System.debug('@@@ DelTrackInvoke Line Number' + e.getLineNumber());
            System.debug('@@@ DelTrackInvoke Message' + e.getMessage());
            
            return chatBotDelResponseList;
        }
  
        return chatBotDelResponseList;
      
    }
    
}