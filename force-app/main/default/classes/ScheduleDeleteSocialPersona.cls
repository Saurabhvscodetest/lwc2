/* Description : Delete SocialPersona Records
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0  29/03/2019        Vijay Ambati                      Created              COPT-4362
*
*/
global class ScheduleDeleteSocialPersona implements Schedulable{

     global void execute(SchedulableContext sc){
        BAT_DeleteSocialPersona obj = new BAT_DeleteSocialPersona();
        Database.executebatch(obj);
    }
    
}