/* Description  : GDPR - Delete Accounts from System
*
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   28/03/2019      Vijay Ambati                        Created             COPT-4521
*
*/
global class ScheduleBatchToDeleteAccounts implements Schedulable{
   global void execute(SchedulableContext sc){
        BAT_DeleteAccountsForGDPR obj = new BAT_DeleteAccountsForGDPR();
        Database.executebatch(obj);
    }
   
}