@isTest
public class DisplayGoodwillAmountControllerTest {
    
    public static final String CONTACT_CENTRE_TEAM_NAME = ' Hamilton - CRD';
    public static final String CONTACT_CENTRE_QUEUE_NAME = 'CRD_Work_Allocation_Hamilton';

    @isTest static void testfetchGoodwillAmountData(){
        CustomSettingTestDataFactory.initialiseTeamCustomSettings(CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_QUEUE_NAME);

        Contact testContact = CustomerTestDataFactory.createContact();
            insert testContact;
            system.assert(testContact.Id != NULL);
            Case testCase = CaseTestDataFactory.createComplaintCase(testContact.Id);
            testCase.jl_Customer_Allowance__c = 150;
            insert testCase;
        
        DisplayGoodwillAmountController.fetchGoodwillAmountData ( testContact.Id );
        
    }
}