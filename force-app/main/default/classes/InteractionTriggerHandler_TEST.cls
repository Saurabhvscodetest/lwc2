/**
 * Unit tests for InteractionTriggerHandler class methods.
 */
@isTest
private class InteractionTriggerHandler_TEST {
	/**
	 * Test Interaction Start / End Date Text fields are set correctly
	 */
	static testMethod void testInteractionDateTextFields() {
    	system.debug('TEST START: testInteractionDateTextFields');
       	UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
       	Account testAccount = UnitTestDataFactory.createAccount(1, true);
       	Contact testCon = UnitTestDataFactory.createContact(1, testAccount.id, true);
       	
       	test.startTest();
       	Case testCase1 = UnitTestDataFactory.createQueryCase(testCon.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, true);
		test.stopTest();
		
		List<Interaction__c> interactionList = [SELECT Id, Name, Start_DT__c, Start_DT_Text__c, End_DT__c, End_DT_Text__c, Case__c
												FROM Interaction__c
												WHERE Case__c = :testCase1.Id];

		system.assertEquals(3, interactionList.size());
		
		for (Interaction__c i : interactionList) {
			String tempStartDTText = InteractionTriggerHandler.setDateTextValue(i.Start_DT__c);
			String tempEndDTText = InteractionTriggerHandler.setDateTextValue(i.End_DT__c);
			system.assertNotEquals(null, tempStartDTText);
			system.assertNotEquals(null, i.Start_DT_Text__c);
			system.assertEquals(tempStartDTText, i.Start_DT_Text__c);
			system.assertEquals(tempEndDTText, i.End_DT_Text__c);
		}
    	system.debug('TEST END: testInteractionDateTextFields');
	}
}