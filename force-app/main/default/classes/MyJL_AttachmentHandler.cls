/**
 *	@author: Andrey Gavrikov (westbrook)
 *	@date: 2014-11-10 12:27:58 
 *	@description:
 *	    handler of all trigger events on Attachment object
 *	
 *	Version History :   
 *	2014-11-10 - MJL-1276 - AG
 *	if Attachment relates to Transaction_Header__c and
 *	Transaction_Header__c now has both Full & Short XMLs then update
 *	Transaction_Header__c and set IsValid__c = true
 *	
 *	001		15/04/15	NTJ		MJL-1790 - Do not do a SELECT WHERE x IN set, if we have an empty set
 */
public without sharing class MyJL_AttachmentHandler extends MyJL_TriggerHandler {
	override public void afterInsert () {
		processTransactionXMLAttachments((List<Attachment>)Trigger.new);
	}

	override public void afterUpdate () {
		processTransactionXMLAttachments((List<Attachment>)Trigger.new);
	}

	override public void afterDelete () {
		processTransactionXMLAttachments((List<Attachment>)Trigger.old);
	}

	override public void afterUnDelete () {
		processTransactionXMLAttachments((List<Attachment>)Trigger.new);
	}

	/**
	 * MJL-1276 - if Attachment relates to Transaction_Header__c and
	 * Transaction_Header__c now has both Full & Short XMLs then update
	 * Transaction_Header__c and set IsValid__c = true
	 * 
	 * Unit tests see in MyJLKD_SearchHandlerTest.cls
	 */
	private static void processTransactionXMLAttachments(List<Attachment> recs) {
		
		final Schema.DescribeSObjectResult dsr = Transaction_Header__c.sObjectType.getDescribe();
		final String transactionIdPrefix = dsr.getKeyPrefix();
		
		final String fullXmlName = MyJL_Util.getFullXmlName(); 
		//system.debug('fullXmlName: '+fullXmlName);
		final String shortXmlName = MyJL_Util.getShortXmlName(); 
		//system.debug('shortXmlName: '+shortXmlName);

		final Set<Id> allTransactionIds = new Set<Id>();
		final List<Attachment> transactionAttachments = new List<Attachment>();
		final Set<Id> attachmentIds = new Set<Id>();

		for(Attachment att : recs) {
			//system.debug('att: raw:'+att);
			if (null != att.ParentId && String.valueOf(att.ParentId).startsWith(transactionIdPrefix)) {
				attachmentIds.add(att.Id);
				allTransactionIds.add(att.ParentId);
			}
		}

		final Set<Id> hasFullXml = new Set<Id>();//transactionId: if it has full XML attachment
		final Set<Id> hasShortXml = new Set<Id>();
		if (!allTransactionIds.isEmpty()) {
			for(Attachment att : [select Id, Name, ParentId from Attachment where ParentId in: allTransactionIds and (Name = :fullXmlName or Name = :shortXmlName) ]) {
				//system.debug('att read: '+att); 
				Id transactionId = att.ParentId; 
				if (fullXmlName == att.Name) {
					hasFullXml.add(transactionId);
				}
				if (shortXmlName == att.Name) {
					hasShortXml.add(transactionId);
				}
			}			
		}
		final List<Transaction_Header__c> transToUpdate = new List<Transaction_Header__c>();
		for(Id transactionId : hasFullXml) {
			if (hasShortXml.contains(transactionId)) {
				transToUpdate.add(new Transaction_Header__c(Id = transactionId, IsValid__c = true));
				//mark Transaction_Header__c as Valid
				allTransactionIds.remove(transactionId);
			}
		}
		
		//mark still unprocessed transactions as invalid
		for(Id transactionId : allTransactionIds) {
			transToUpdate.add(new Transaction_Header__c(Id = transactionId, IsValid__c = false));
		}

		Database.update(transToUpdate);
	}
}