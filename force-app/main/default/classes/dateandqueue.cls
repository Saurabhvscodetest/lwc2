public class dateandqueue {
		
    
    
    @AuraEnabled(cacheable=true)
    public static List<List<String>> fetchdate(String BranchValue, String contactReasonValue, String reasonDetailValue, String thisCase)
    {
        String queueNameValue = '';
        system.debug(BranchValue+contactReasonValue+reasonDetailValue);
       list<Case_Routing_Categorisation__c> lstcrc = [select Assign_To__c from Case_Routing_Categorisation__c where 
                                        Contact_Reason__c =: contactReasonValue and Reason_Detail__c =: reasonDetailValue
                                         Limit 1];  
        String queueAssignTo  = 'Queue does not exist';
		if(lstcrc != null && lstcrc.size() > 0)
        	queueAssignTo = lstcrc[0].Assign_To__c;
       
		List<Team__c> listTeam = [select Queue_Name__c from Team__c where name =: queueAssignTo];
             if(listTeam != null && listTeam.size()>0){
       		  queueNameValue = listTeam[0].Queue_Name__c ;
                 
              //return queueNameValue;
        }
        if (queueAssignTo.startsWith('Branch')){
            
                String branchMaster = queueAssignTo + ' ' +BranchValue;
                List<Team__c> listTeam1 = [select Queue_Name__c from Team__c where name =:branchMaster ];
             if(listTeam1 != null && listTeam1.size()>0){
       		  queueNameValue = listTeam1[0].Queue_Name__c ;
                 system.debug('Block2'+queueNameValue);
            }
        
        }
        
            return NextActionActivityControllerLEX.fetchAvailableSlotsDateTime(queueNameValue,'');
          
          
    }
    
    @AuraEnabled(cacheable=true)
    public static List<String> fetchAvailableSlots( String queueName ) {
        system.debug(queueName+'Block1');
        List<String> availableDates = new List<String>();
        Map < String, Map < String, Integer > > availableQueueSlotsMap = QueueCapacityUtils.fetchAllAvailableQueueSlots ( queueName );
        String lastDate = '';
        for ( String dateString : availableQueueSlotsMap.keySet() ) {
            availableDates.add( dateString);
            lastDate = dateString;
        }  
        
        
        return availableDates;
    }
}