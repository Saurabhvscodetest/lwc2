/*************************************************
coSoapCustomerSearch

class to encapsulate soap requests

Author: Steven Loftus (MakePositive)
Created Date: 11/11/2014
Modification Date: 
Modified By: 

**************************************************/
public class coSoapCustomerSearch {

	// public parameters to use in the search
	public String FirstName {get; set;}
	public String LastName {get; set;}
	public String PostCode {get; set;}
	public String Email {get; set;}

	// public result parameters
	public String Status {get; set;}
	public ErrorResult Error {get; set;}
	public List<CustomerSearchResult> Customers {get; set;}

	// default constructor
	public coSoapCustomerSearch() {

		// create an empty list if null
		if (Customers == null) {
			Customers = new List<CustomerSearchResult>();
		}
	}

	public String getSoapRequest() {

		// always need to have a first and last name
		String soapRequest = 	'<?xml version="1.0" encoding="UTF-8"?>' +
    							'<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">' +
									'<soapenv:Header/>' +
									'<soapenv:Body>' +
    								'<viewcus:SearchCustomer xmlns:viewcus="http://soa.johnlewispartnership.com/service/es/Sell/Customer/CustomerProfile/ViewCustomer/v1" xmlns:cmnEBM="http://soa.johnlewispartnership.com/schema/ebm/CommonEBM/v1" xmlns:viewcusEBM="http://soa.johnlewispartnership.com/schema/ebm/Sell/Customer/CustomerProfile/ViewCustomer/v1" xmlns:cus="http://soa.johnlewispartnership.com/schema/ebo/Customer/v1" xmlns:cmn="http://soa.johnlewispartnership.com/schema/ebo/Common/v1" xmlns:wsa="http://www.w3.org/2005/08/addressing" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">' +
        								'<cmnEBM:ApplicationArea sentDateTime="' + DateTime.now().format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'') + '">' +
            								'<cmnEBM:Channel>' +
                								'<cmnEBM:ChannelID schemeID="OCIP" schemeAgencyID="JL"/>' +
                								'<cmnEBM:ChannelName>OCIP</cmnEBM:ChannelName>' +
                								'<cmnEBM:ChannelType>Sales</cmnEBM:ChannelType>' +
            								'</cmnEBM:Channel>' +
            								'<cmnEBM:Sender>' +
							                    '<cmnEBM:LogicalID schemeID="CaseManagement" schemeAgencyID="JL"/>' +
							                '</cmnEBM:Sender>' +
							                '<wsa:MessageID>' + coSoapHelper.generateGuid() + '</wsa:MessageID>' +
							                '<cmnEBM:TrackingID>' + coSoapHelper.generateGuid() + '</cmnEBM:TrackingID>' +
							            '</cmnEBM:ApplicationArea>' +
							            '<viewcusEBM:DataArea>' +
							                '<viewcusEBM:Search/>' +
							                '<viewcusEBM:Customer xsi:type="cus:KeyCustomer">' +
							                    '<cmn:Party xsi:type="cmn:Person">' +
							                        '<cmn:FirstName>' + this.FirstName + '</cmn:FirstName>' +
							                        '<cmn:LastName>' + this.LastName+ '</cmn:LastName>' +
							                    '</cmn:Party>';

		// may have a post code
		if (String.isNotBlank(this.PostCode)) {

			soapRequest +=						'<cmn:PartyContactMethod xsi:type="cmn:Address">' +
				                  					'<cmn:ContactPurposeTypeCode code="Billing"/>' +
				                  					'<cmn:PostalCode>' +
				                     					'<cmn:Postcode>' + this.Postcode + '</cmn:Postcode>' +
				                  					'</cmn:PostalCode>' +
				               					'</cmn:PartyContactMethod>';
		}

		// may have an email adress
		if (String.isNotBlank(this.Email)) {

			soapRequest +=						'<cmn:PartyContactMethod xsi:type="cmn:EmailAddress">'+
					                        		'<cmn:EmailAddress>' + this.Email + '</cmn:EmailAddress>'+
					                    		'</cmn:PartyContactMethod>';
		}

		// complete the soap structure
		soapRequest +=						'</viewcusEBM:Customer>' +
			                        		'<viewcusEBM:CalendarPeriod>' +
					                            '<cmn:CalendarID code="OrderedDate"/>' +
			        		                    //'<cmn:EndDateTime>2014-12-04T00:12:10.321+01:00</cmn:EndDateTime>' +
			                		            '<cmn:StartDateTime>2010-01-01T00:00:00.000Z</cmn:StartDateTime>' +
					                        '</viewcusEBM:CalendarPeriod>' +
					                    '</viewcusEBM:DataArea>' +
					                '</viewcus:SearchCustomer>' +
					            '</soapenv:Body>' +
					        '</soapenv:Envelope>';

		// return the built soap request
		return soapRequest;
	}

	// called to parse the soap response from the service
	public void setSoapResponse(string soapResponseDocument) {

    	Dom.Document doc = new Dom.Document();
    	
    	doc.load(soapResponseDocument);

    	Dom.XMLNode rootNode = doc.getRootElement();

    	// get all the namespaces out of the response
    	String serverEndPoint = 'http://soa.johnlewispartnership.com/';

    	String viewcusNS = serverEndPoint + 'service/es/Sell/Customer/CustomerProfile/ViewCustomer/v1';
    	String cmnEBMNS = serverEndPoint + 'schema/ebm/CommonEBM/v1';
		String cusNS = serverEndPoint + 'schema/ebo/Customer/v1';
		String cmnNS = serverEndPoint + 'schema/ebo/Common/v1';
    	String viewcusEBMNS = serverEndPoint + 'schema/ebm/Sell/Customer/CustomerProfile/ViewCustomer/v1';
    	String soapenvNS = 'http://schemas.xmlsoap.org/soap/envelope/';
		String wsaNS = 'http://www.w3.org/2005/08/addressing';

		try {
	
			if (rootNode.getChildElement('Body', soapenvNS).getChildElement('Fault', soapenvNS) != null) {

				dealWithFaultNode(rootNode.getChildElement('Body', soapenvNS).getChildElement('Fault', soapenvNS));
				return;
			}

			// get the dataarea and show nodes
			Dom.XMLNode dataAreaNode = rootNode.getChildElement('Body', soapenvNS).getChildElement('ShowCustomer', viewcusNS).getChildElement('DataArea', viewcusEBMNS);
			Dom.XMLNode showNode = dataAreaNode.getChildElement('Show', viewcusEBMNS);

			// if there is a problem then get the error details and return
			this.Status = showNode.getAttributeValue('responseCode', '');

			if (this.Status != 'Success') {
				if (showNode.getAttributeValue('hasActionStatusRecords', '') == 'true') {

					setErrorDetails(showNode.getChildElement('ActionStatusRecord', cmnEBMNS).getChildElement('ServiceError', cmnEBMNS));
					return;

				// see if there is an internal error then get the details and return
				} else {

					setErrorDetails(showNode.getChildElement('ServiceError', cmnEBMNS));
					return;
				}
			}

			// if we get here then there is no error and we may have some customers
			for (Dom.XMLNode childNode : dataAreaNode.getChildElements()) {

				// there will be 0 to many customers
				if (childNode.getName() == 'Customer') {

                    String email = null;
                    String postCode = null; 
                    
                    // there will be at least 1 of email or postcode - as the search is for billing address not delivery
                    for(Dom.XMLNode cn : childNode.getChildElements()){
                        if (cn.getName() == 'PartyContactMethod' && cn.getChildElement('EmailAddress', cmnNS) != null) {
                            email = cn.getChildElement('EmailAddress', cmnNS).getText();
                            
                        } else if (cn.getName() == 'PartyContactMethod' && cn.getChildElement('PostalCode', cmnNS) != null) {
                            postCode = cn.getChildElement('PostalCode', cmnNS).getChildElement('Postcode', cmnNS).getText();                               
                        }
                    }

                    // by the time we get here we will have exhausted the search for email and postcode so take what we have.

                    // so we get the name details from a Party node but there may not be one so default the names to what we serahced for.
                    String firstName = this.FirstName;
                    String lastName = this.LastName;
                    String salutation = null;

                    // there will be 0 to 1 Party nodes
                    if (childNode.getChildElement('Party', cmnNS) != null) {

                    	Dom.XMLNode party = childNode.getChildElement('Party', cmnNS);
                    	// there will always be a first and last name but salutation is not guaranteed
                    	if (party.getChildElement('FirstName', cmnNS) != null) firstName = party.getChildElement('FirstName', cmnNS).getText();
                    	if (party.getChildElement('LastName', cmnNS) != null) lastName = party.getChildElement('LastName', cmnNS).getText();

                    	if (party.getChildElement('Salutation', cmnNS) != null) salutation = party.getChildElement('Salutation', cmnNS).getText();
                    }

                    // now that we have name details and email and/or postcode we need to loop the xrefIdentity node inside the CustomerID node
                    // for each on we find, we will create a new customer search result entry

                    for (Dom.XMLNode xref : childNode.getChildElement('CustomerID',cusNS).getChildElements()) {

                    	// create a new customer search results record and populate it with everything we have 
                    	// including the customer id we are about to pull out of each xref node

                    	string customerId = xref.getAttributeValue('ID','');

                    	// if we found a blank id then skip to the next one
                    	if (String.isBlank(customerId)) Continue;

                    	CustomerSearchResult customer = new CustomerSearchResult();
                    	customer.Salutation = salutation;
                    	customer.FirstName = firstName;
                    	customer.LastName = lastName;
                    	customer.CustomerId = customerId;
                    	customer.Postcode = postCode;
                    	customer.Email = email;

                    	this.customers.add(customer);
                    }
				}
			}
        } catch (Exception e) {

        	this.Status = 'Error';
			// catch all in case we try to access a node we think should be there but it is not
			setErrorDetails('INTERNAL_ERROR', 'Technical', 'An exception was generated during the parsing of the soap response: ' + e.getMessage());
        }
	}

	private void dealWithFaultNode(Dom.XMLNode faultNode) {

       	this.Status = 'Error';
		setErrorDetails(faultNode.getChildElement('faultcode', null).getText(), null, faultNode.getChildElement('faultstring', null).getText());
	}

	// override eof setErrorDetails
	private void setErrorDetails(Dom.XMLNode errorNode) {

		setErrorDetails(errorNode.getAttributeValue('errorCode', ''), errorNode.getAttributeValue('errorType', ''),errorNode.getAttributeValue('errorDescription', ''));
	}

	// set the error details passed in
	private void setErrorDetails(String code, String type, String description) {

		this.Error = new ErrorResult();
		this.Error.ErrorCode = code;
		this.Error.ErrorType = type;
		this.Error.ErrorDescription = description;			
	}

	// class to hold the error
    public class ErrorResult {

    	public String ErrorCode {get; set;}
    	public String ErrorType {get; set;}
    	public String ErrorDescription {get; set;}
    }

    // class to hold the customers found in the search
    public class CustomerSearchResult {

    	public String Salutation {get; set;}
    	public String FirstName {get; set;}
    	public String LastName {get; set;}
    	public String CustomerId {get; set;}
    	public String Postcode {get; set;}
    	public String Email {get; set;}
    }
}