/********************************************************
**@author Ramachandran
** 
**  Edit    Date            By          Comment
**  001     15/12/15       Ram          Created. Base Chained Batch Apex Code.Constructor code to accept the parameters.
**  002     17/12/15       Ram          Edited some sections of the code so that they do not execute under Test Execution
**
*/
global class CopyCaseEmailMessages_ThirdStage implements Database.batchable<SObject>, Database.stateful{

   
  // Set of all Case id to be passed on to the final Stage
    global Set<id> allCaseId{get;set;}
    //Variable to receive the emailMessageIdList from Previous Stage
      global set<String> emailMessageIdList{get;set;}
      // Variable to receive the EmailCaseMap from Previous Stage
       global Map<Id,Id> emailMessageCaseMap{get;set;}
       
       // Constructor to receive the variables from previous stage for chain to happen.
      global CopyCaseEmailMessages_ThirdStage(Set<id> lcaseId, Set<String> emIdList,Map<Id,Id> emCaseMap)
      {
           allCaseId =   lcaseId;
            emailMessageIdList = emIdList;
            emailMessageCaseMap = emCaseMap;
      }
      
      // The Previous chain invokes this Batch on size of 2 to avoid any bottle necks on the heap size.
      // This Query returns the attachment which needs to be moved from Email Message on to the case.
    global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator( [select Id, ParentId, Body, Name  from Attachment where ParentId IN :emailMessageIdList]);
   }
    
   // The execute method process all the attachment in batches of 2, where the attachment is cloned and its parented to case 
   //so as to mimic copy of attachments.
   global void execute(Database.BatchableContext BC, List<sObject> scope){
   List<Attachment> allAttachments= scope;
      List<Attachment> attToInsert = new List<Attachment>();
      for (Attachment att : allAttachments)
      {
            Attachment newAtt = att.clone();
             newAtt.parentId = emailMessageCaseMap.get(att.ParentId);
             attToInsert.add(newAtt);
      }
      insert attToInsert;
        
    }

    // Finish methods to call the Final Stage to clear the Pending Email Message on the Processed Case Records.
   global void finish(Database.BatchableContext BC){
       if (!Test.isRunningTest())
       {
           DataBase.executeBatch(new CopyCaseEmailMessages_FinalStage(allCaseId));
        }
   }

}