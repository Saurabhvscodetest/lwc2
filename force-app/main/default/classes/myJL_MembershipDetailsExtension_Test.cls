/**
* Author:       Nawaz Ahmed
* Date:         13/08/2015
* Description:  Test Class for myJL_MembershipDetailsExtension
*/
@isTest
private class myJL_MembershipDetailsExtension_Test {
    
    /**
	* Test Method for online customer
	*/
    static testMethod void testMembershipDetailsTesOnline() {
        PageReference pageRef = Page.myJL_MembershipDetails;
        Test.setCurrentPage(pageRef); 
        
        //Prepare Test Data
        Contact customer = MyJL_TestUtil.createCustomer('Ordering_Customer');  
        
        Loyalty_Account__c acc1 = new Loyalty_Account__c(Name='00000000012', Email_Address__c = 'some1@example.com', IsActive__c = false, ShopperId__c='shopper1', Contact__c =customer.id);
        
        Loyalty_Account__c LA = new Loyalty_Account__c(Name='00000000012', Email_Address__c = 'harry.hoppo@animals.org', IsActive__c = true, ShopperId__c='shopper1', Contact__c =customer.id);
        LA.Customer_Loyalty_Account_Card_ID__c = 'TestCard';
        LA.Channel_ID__c = 'johnlewis.com';
        LA.Activation_Date_Time__c = datetime.now();
        LA.Voucher_Preference__c = 'Paper';
        Insert LA;
        
        Loyalty_Card__c LC = new Loyalty_Card__c(Name = 'TestCard', Loyalty_Account__c = LA.Id);
        insert Lc;
        
        ApexPages.standardController controller = new ApexPages.standardController(customer);
        myJL_MembershipDetailsExtension pageToTest = new myJL_MembershipDetailsExtension(controller);
        myJL_MembershipDetailsExtension.ProfileWithLoyaltyAccount myJlReq = new myJL_MembershipDetailsExtension.ProfileWithLoyaltyAccount(customer.id,customer,LA);
        myJLReq.getMyJLNewRequest();
        
        List<myJL_MembershipDetailsExtension.ProfileWithLoyaltyAccount> listFromPage =  pageToTest.plaList;
        
        System.AssertEquals(1, listFromPage.size());
        System.AssertEquals('Paper', listFromPage[0].vouchPref);
    }
    
    /**
	* Test Method for Instore customer
	*/
    static testMethod void testMembershipDetailsTesInstore() {
        // TO DO: implement unit test
        PageReference pageRef = Page.myJL_MembershipDetails;
        Test.setCurrentPage(pageRef); 
        
        Contact customer = MyJL_TestUtil.createCustomer('Ordering_Customer');  
        
        Loyalty_Account__c LA = new Loyalty_Account__c(Name='00000000012', Email_Address__c = 'harry.hoppo@animals.org', IsActive__c = false, ShopperId__c='shopper1', Contact__c =customer.id);
        LA.Customer_Loyalty_Account_Card_ID__c = 'TestCard';
        LA.Channel_ID__c = 'epos';
        LA.Scheme_Joined_Date_Time__c = datetime.now();
        LA.Voucher_Preference__c = 'Paper';
        Insert LA;
        
        Loyalty_Card__c LC = new Loyalty_Card__c(Name = 'TestCard', Loyalty_Account__c = LA.Id);
        insert Lc;
        
        ApexPages.standardController controller = new ApexPages.standardController(customer);
        myJL_MembershipDetailsExtension pageToTest = new myJL_MembershipDetailsExtension(controller);
        
        List<myJL_MembershipDetailsExtension.ProfileWithLoyaltyAccount> listFromPage =  pageToTest.plaList;
        
        System.AssertEquals(1, listFromPage.size());
        System.AssertEquals('Paper', listFromPage[0].vouchPref);
    }
    
    /**
	* Test Method for No Active JL member
	*/
    static testMethod void testMembershipDetailsTesInactivemember() {
        // TO DO: implement unit test
        PageReference pageRef = Page.myJL_MembershipDetails;
        Test.setCurrentPage(pageRef); 
        
        //Prepare Test Data
        Contact con =  UnitTestDataFactory.createContact();
        Insert con;
        
        Contact_Profile__c cp = new Contact_Profile__c(FirstName__c='Harry', LastName__c='Hippo', contact__c=con.id,email__c='harry.hoppo@animals.org');
        cp.Shopper_ID__c = 'shopper1';
        cp.SourceSystem__c = 'johnlewis.com';
        cp.Mailing_House_No_Text__c = '15';
        cp.Mailing_House_Name__c = 'Wixford';
        cp.Mailing_Street__c = 'Oakfield';
        cp.Mailing_Address_Line2__c = 'Block A';
        cp.Mailing_Address_Line3__c = 'Square C';
        cp.Mailing_Address_Line4__c = 'Area E';
        cp.MailingCity__c = 'Liverpool';
        cp.Mailing_County_Name__c = 'Merseyside';
        cp.MailingCountry__c = 'UK';
        cp.MailingPostCode__c = 'L4 2QH';
        insert cp;
        
        ApexPages.standardController controller = new ApexPages.standardController(con);
        myJL_MembershipDetailsExtension pageToTest = new myJL_MembershipDetailsExtension(controller);
    }
    
    /**
	* Simulate calling MembershipDetails from a lightning component
	*/    
    static testMethod void testMembershipDetailsFromLightningComponent(){		
        Contact customer = MyJL_TestUtil.createCustomer('Ordering_Customer');      
        PageReference pageRef = Page.myJL_MembershipDetails;
        Test.setCurrentPage(pageRef); 
        ApexPages.currentPage().getParameters().put('contactId',customer.Id);         
        System.assertEquals(customer.Id, ApexPages.currentPage().getParameters().get('contactId'));
    }

    static testMethod void testMembershipDetailsForPartnershipCard() {
        CalloutHandlerTest.initCalloutSettings(MyJL_LoyaltyAccountHandler.FULL_JOIN_NOTIFY, 'MyJL_LoyaltyAccountHandler.JoinLeaveCallout', 10);

        PageReference pageRef = Page.myJL_MembershipDetails;
        Test.setCurrentPage(pageRef); 

        //Prepare Test Data
        Contact customer = MyJL_TestUtil.createCustomer('Ordering_Customer');  
        Loyalty_Account__c la = new Loyalty_Account__c(Name='00000000012', Email_Address__c = 'some1@example.com', IsActive__c = true, ShopperId__c='shopper1', Contact__c =customer.id);
       
        la.Channel_ID__c = 'johnlewis.com';
        la.Activation_Date_Time__c = datetime.now();
        la.Voucher_Preference__c = 'Paper';
        insert LA;
        

        Loyalty_Card__c card = new Loyalty_Card__c();
        card.Name = '1234567890123456';
        card.Card_Number__c = '1234567890123456';
        card.Loyalty_Account__c = la.Id;
        card.Card_Type__c = MyJL_Const.MY_PARTNERSHIP_CARD_TYPE;
        card.Date_Created__c = DateTime.now();
        card.Origin__c = 'TEST';
        card.Pack_Type__c = MyJL_Const.PACK_TYPE_DIGITAL;        
        insert card;

        ApexPages.standardController controller = new ApexPages.standardController(customer);
        myJL_MembershipDetailsExtension pageToTest = new myJL_MembershipDetailsExtension(controller);
        myJL_MembershipDetailsExtension.ProfileWithLoyaltyAccount myJlReq = new myJL_MembershipDetailsExtension.ProfileWithLoyaltyAccount(customer.id,customer,LA);
        myJLReq.getMyJLNewRequest();
        
        List<myJL_MembershipDetailsExtension.ProfileWithLoyaltyAccount> listFromPage =  pageToTest.plaList;
        
        System.AssertEquals(1, listFromPage.size());
        System.AssertEquals(card.Date_Created__c.format('dd MMMM, yyyy HH:mm'), listFromPage[0].joinedMyPartnershipDate);
        System.AssertEquals(MyJL_Const.PACK_TYPE_DIGITAL, listFromPage[0].partnershipCardPack);
        System.AssertEquals('My Partnership Barcode Number', listFromPage[0].BarcodeLabel);
        System.AssertEquals('View Partnership Information', listFromPage[0].ViewMyButtonLabel);


    }
}