/**
 *	@author: Andrey Gavrikov (westbrook)
 *	@date: 2014-07-25 17:58:46 
 *	@description:
 *	    base handler for all MyJL trigger handlers
 *	    all MyJL trigger handler shall inherit from this class
 *	
 *	Version History :   
 *		
 */
public virtual class MyJL_TriggerHandler extends BaseTriggerHandler {
	
	/**
	 * check MyJL Settings__c to see if triggers are enabled for running user
	 */
	public override Boolean skipAllEventsForCurrentUser() {
		if (super.skipAllEventsForCurrentUser()) {
			return true;
		}
		//check MyJL specific settings
		return false == CustomSettings.getMyJLSetting().getBoolean('Run_MyJL_Triggers__c');
	}
}