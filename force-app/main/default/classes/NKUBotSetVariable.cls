/*
* @File Name   : NKUBotSetVariable 
* @Description : Utility class related to NKU Chatbot functionalities  
* @Copyright   : Zensar
* @Jira #      : #5174
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0    28-JAN-2020           Vijay A                      Created
*/

Public class NKUBotSetVariable {

    Public class NKUBotSetVariableRequest {
     @InvocableVariable(required= true)
     public String NKUInput;
   }    
    
    Public class NKUBotSetVariableResponse {
     @InvocableVariable(required= true)
     public String NKUOuput;
   }
    
	@Invocablemethod(Label ='NKUVariableSet' description='NKUVariableSet')
    Public static List<NKUBotSetVariableResponse> NKUChatBotProcess(List<NKUBotSetVariableRequest > chatbotInputReqList){
        List<NKUBotSetVariableResponse>   chatBotDelResponseList = new List<NKUBotSetVariableResponse>();
        NKUBotSetVariableResponse chatBotDelResponse = new NKUBotSetVariableResponse();
         try{
           List<String> chatbotInputs = new List<String>();
            for( NKUBotSetVariableRequest chatbotReq : chatbotInputReqList ){
           chatbotInputs.add ( chatbotReq.NKUInput );
         }
      																
             if(chatbotInputs[0] != NULL ){           
                 chatBotDelResponse.NKUOuput = 'Value Exist';   //return Output;
             }else{
                 chatBotDelResponse.NKUOuput = 'Value DOnt Exist';
             }
        chatBotDelResponseList.add ( chatBotDelResponse ); 
             return chatBotDelResponseList;
        }
        catch(Exception e){
            system.debug('NKUBotSetVariable exception message' + e.getMessage());
            system.debug('NKUBotSetVariable exception Line Number' + e.getLineNumber());
            return chatBotDelResponseList;
        }
        
       
    }

}