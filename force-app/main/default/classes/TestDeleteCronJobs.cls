@isTest
Private class TestDeleteCronJobs{

static testmethod void TestCronDeleteJob(){    
    Database.BatchableContext bc = null;
    DeleteCronScheduledJobs schjob = new DeleteCronScheduledJobs();
    List<CronTrigger> cronList = [Select Id from CronTrigger Limit 2];
    Test.startTest();
    schjob.start(bc);
    schjob.execute(bc,cronList);
    schjob.finish(bc);
    Test.stopTest();
}

}