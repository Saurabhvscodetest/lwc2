/* Description  : Delete case Records As per the cirteria given in COPT-4293
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   20/02/2019        Vignesh Kotha                   Created               COPT-4293
*
*/
@isTest
Public class BAT_DeleteCaseRecordsTest{
    static testmethod void testDeleteEmailMessages(){        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        // Running user Context
        System.runAs(runningUser){
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;       
            insert CustomSettingTestDataFactory.createConfigSettings();
        }        
        System.runAs(testUser){            
            contact testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'vignesh@gmail.com';
            insert testContact;            
            case testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            insert testCase;
            GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
            gDPR.GDPR_Class_Name__c = 'BAT_DeleteCaseRecords';
            gDPR.Name = 'BAT_DeleteCaseRecords';
            gDPR.Record_Limit__c = '5000';  
            Test.startTest();
            Insert gDPR;
            List<Case> CaseList=new  List<Case>();
            for(integer i=0; i<19; i++){
                CaseList.add(new Case(Closeddate=System.now().addMonths(-18),Number_of_Email_Contacts__c=1.0,Bypass_Standard_Assignment_Rules__c = true));
            }       
            insert CaseList;   
            Database.BatchableContext BC;
            BAT_DeleteCaseRecords obj=new BAT_DeleteCaseRecords();
            obj.start(BC);
            Database.DeleteResult[] Delete_Result = Database.delete(CaseList, false);
            obj.execute(BC,CaseList);
            obj.finish(BC);
            DmlException expectedException;
            Test.stopTest();
        }  
    }
}