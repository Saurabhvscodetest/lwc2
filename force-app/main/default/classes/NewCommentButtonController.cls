/**
* @description	Controller for the Case Add Comment Button
* @author		@tomcarman		
*/

public class NewCommentButtonController {

	public Boolean isCaseClosed {get; set;}
	private ApexPages.StandardController standardController;
	private Id recordId;	
	private Case caseRecord;
	private PageReference addCommentPage;

	public NewCommentButtonController(ApexPages.StandardController standardController) {
		this.standardController = standardController;
	}

 	/**
 	* @description	Page level validation			
 	* @author		@tomcarman
 	*/

	public PageReference validate() {

		recordId = standardController.getId();
		caseRecord = [SELECT Id, IsClosed FROM Case WHERE Id = :recordId];
		
		addCommentPage = new PageReference('/00a/e?parent_id=' + recordId + '&retURL=%2F' + recordId);

		if(!caseRecord.IsClosed || CaseReopenUtilities.getCurrentUser().Tier__c > 2) {
			return addCommentPage;

		} else {
			ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.WARNING, 'You are trying to add a comment to a closed case, would you like to reopen it?'));
			isCaseClosed = true;
			return null; 
		}

	}


	/**
	* @description	Reopens case, returns either Edit page, or error		
	* @author		@tomcarman
	*/

	public PageReference reopenCase () {

		caseRecord = CaseReopenUtilities.reopenCase(caseRecord.Id);

		if(CaseReopenUtilities.errorMessages.isEmpty()) {

			update caseRecord;
			return addCommentPage;

		} else {

			for(String errorMessage : CaseReopenUtilities.errorMessages) {
				ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, errorMessage));
			}

			return null;
		}
	}


}