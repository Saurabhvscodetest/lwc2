/**
 * retrieve type services
 */
global with sharing class SFDCKitchenDrawerGet {
    /**
     * get list of transactions matching search criteria and offset
     */
    webservice static SFDCKitchenDrawerTypes.ActionResponse searchTransactions(SFDCKitchenDrawerTypes.RequestHeader requestHeader, 
                                                                                SFDCKitchenDrawerTypes.TransactionSearchCriteria searchCriteria) {
		if (!isEnabled()) return getInactiveServiceResponse(requestHeader);
        
		/*
        //dummy response, just to see what it looks like in resulting SOAP message in SOAPUI
        SFDCKitchenDrawerTypes.ActionResponse response = new SFDCKitchenDrawerTypes.ActionResponse();
        response.results = new List<SFDCKitchenDrawerTypes.ActionResult>();
        SFDCKitchenDrawerTypes.ActionResult result1 = new SFDCKitchenDrawerTypes.ActionResult();

        result1.kdTransaction = new SFDCKitchenDrawerTypes.KDTransaction();
        SFDCKitchenDrawerTypes.ActionResult result2 = new SFDCKitchenDrawerTypes.ActionResult();
        result2.kdTransaction = new SFDCKitchenDrawerTypes.KDTransaction();
        
        response.results.add(result1);
        response.results.add(result2);

        return response;
		*/
		return MyJLKD_SearchHandler.process(requestHeader, searchCriteria); 
    }

    /**
     * get list of transactions by Transaction Id
     */
    webservice static SFDCKitchenDrawerTypes.ActionResponse getTransaction(SFDCKitchenDrawerTypes.RequestHeader requestHeader, 
                                                                                String[] transactionIds) {
		if (!isEnabled()) return getInactiveServiceResponse(requestHeader);
        
        //dummy response, just to see what it looks like in resulting SOAP message in SOAPUI
        /*SFDCKitchenDrawerTypes.ActionResponse response = new SFDCKitchenDrawerTypes.ActionResponse();
        response.results = new List<SFDCKitchenDrawerTypes.ActionResult>();
        SFDCKitchenDrawerTypes.ActionResult result1 = new SFDCKitchenDrawerTypes.ActionResult();

        result1.kdTransaction = new SFDCKitchenDrawerTypes.KDTransaction();
        SFDCKitchenDrawerTypes.ActionResult result2 = new SFDCKitchenDrawerTypes.ActionResult();
        result2.kdTransaction = new SFDCKitchenDrawerTypes.KDTransaction();
        
        response.results.add(result1);
        response.results.add(result2);
        */
        SFDCKitchenDrawerTypes.ActionResponse response = MyJLKD_GetHandler.process(requestHeader,transactionIds); 

        return response;
    }
	// this variable is only used in unit test checking what happens when MyJL Settings__c.Activate_Kitchen_Drawer__c is FALSE
	@TestVisible private static Boolean TEST_ENABLE_SERVICES = Test.isRunningTest();
	/**
	 * MJL-1183 - only accept SFDCMyJLCustomerServices calls if MyJL Settings__c.Activate_Kitchen_Drawer__c == TRUE
	 */
	public static Boolean isEnabled() {
		Boolean isReadWrite = !myJL_Util.inReadOnlyMode();
		return TEST_ENABLE_SERVICES || ( (true == CustomSettings.getMyJLSetting().getBoolean('Activate_Kitchen_Drawer__c')) && isReadWrite);
	}
	public static SFDCKitchenDrawerTypes.ActionResponse getInactiveServiceResponse(SFDCKitchenDrawerTypes.RequestHeader request) {
        SFDCKitchenDrawerTypes.ActionResponse response = new SFDCKitchenDrawerTypes.ActionResponse();

		response = MyJL_Util.getSuccessActionResponse();
		response = MyJL_Util.populateKDErrorDetailsHeader(MyJL_Const.ERRORCODE.INACTIVE_SERVICE, response);
		response.MessageId = request.MessageId;
		return response;
	}
}