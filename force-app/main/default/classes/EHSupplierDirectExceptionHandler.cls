/******************************************************************************
* @author       Antony John
* @date         10/10/2015
* @description  ClickAndCollect Exception Handler.
*
* LOG     DATE        Author    JIRA                            COMMENT
* ===     ==========  ======    ========================        ============ 
* 001     10/10/2015  AJ        CMP-39, CMP-42                  Initial code
*
*/
public with sharing class EHSupplierDirectExceptionHandler extends EHBaseExceptionHandler{
	   
	public static final String DEFAULT_PRODUCT_TYPE = 'Allsorts';
	public static final String DEFAULT_PRODUCT_TYPE_KEY = 'Default';
	public static Map<String, String> SUPPLIER_NAME_CONFIG = buildSupplierNameMapping();
	public static Supplier_Name_Mapping__c defaultConfig = Supplier_Name_Mapping__c.getInstance(DEFAULT_PRODUCT_TYPE_KEY);
    public static final String DEFAULT_DESCRIPTION = 'Event Hub Exception Raised - Please review delivery tracking and contact customer';

     public override EventHubVO.EHExceptionResponse handleEventHubExceptions(EventHubVO.EHExceptionRequest ehRequest){
        Savepoint sp = Database.setSavepoint();
        EventHubVO.EHExceptionResponse ehResponse = null;
        try {
            ehResponse = checkInputRequestForError(ehRequest);
            
            //Check  if the ehResponse is null which confirms that there are no validation errors.
            if(ehResponse == null){
                Case c =  getCase(ehRequest);
                Task t =  createTaskForCase(ehRequest, c);
                System.debug('$$Main Task'+t);
                ehResponse = getSucessResponse(t, ehRequest.ehExceptionId);
            }
        } catch(Exception exp){
            Database.rollBack(sp);
            System.debug('Exception occured' + exp);
            if(exp.getMessage().containsIgnoreCase(EHConstants.DUPLICATE_VALUE)){
                ehResponse = addTaskToExistingcase(ehRequest); 
            } else {
                ehResponse = getErrorResponse(exp.getMessage(),ehRequest.ehExceptionId) ;    
            }
        }
        return ehResponse;
     }
     
     public override EventHubVO.EHExceptionResponse addTaskToExistingcase(EventHubVO.EHExceptionRequest ehRequest){
        EventHubVO.EHExceptionResponse ehResponse = null;
        try {
            String taskKey = getUniqueKey(ehRequest); 
            Case c =  getExistingCase(taskKey);
            Task t = createTaskForCase(ehRequest, c);
            ehResponse = getSucessResponse(t, ehRequest.ehExceptionId);
        } catch(Exception exp){
            System.debug('Exception occured ' + exp.getMessage()  + '\n'+ exp.getStackTraceString());
            ehResponse = getErrorResponse(exp.getMessage(),ehRequest.ehExceptionId) ;
        }
        return ehResponse;
     }
     
     /*
      - Checks if there is a existing case for the Exception ehRequest which is either "closed" status and created within the last 28 days
        or is the case is not closed.
      - If not creates a new case with the provided information from the EvenetHub.
      - If the existing closed case then reopen the same
     */ 
     public Case getCase(EventHubVO.EHExceptionRequest ehRequest){
        Case c =  null;
        List<Case> caseList = new List<Case>();
        String orderId = ehRequest.supplierDirectInfo.orderId;
        Datetime ndate = DateTime.Now().AddDays(-1 * EHConstants.CASE_CREATED_DAYS_CRITERIA);
        if(!ehRequest.supplierDirectInfo.isOnline){
            caseList =  [Select Id,contactid, isClosed from Case where jl_CRNumber__c =:orderId and RecordTypeId in(:EHBaseExceptionHandler.queryCaseRecordTypeId,:EHBaseExceptionHandler.complaintCaseRecordTypeId) and Origin =: EHConstants.EVENT_HUB and ((isClosed = true and createdDate >: ndate) or (isClosed = false)) LIMIT 1];
        } else {
           caseList =  [Select Id,contactid, isClosed from Case where jl_OrderManagementNumber__c =:orderId and RecordTypeId in(:EHBaseExceptionHandler.queryCaseRecordTypeId,:EHBaseExceptionHandler.complaintCaseRecordTypeId) and Origin =: EHConstants.EVENT_HUB and ((isClosed = true and createdDate >: ndate) or (isClosed = false)) LIMIT 1]; 
        }
        
        system.debug(LoggingLevel.INFO, 'caseList Size' + (caseList.size()));
        if(caseList.isEmpty()) {
           //Populate the details of the new case
            String supplierName = ehRequest.supplierDirectInfo.supplierName;
            c = populateCase(ehRequest.ehExceptionName, EHConstants.SUPPLIER_DIRECT, ehRequest);
            String receivedPickValueRevise = '';
            String esbBranchValue = ehRequest.supplierDirectInfo.branchName;
            if(esbBranchValue!=NULL) {
                Set<String> picklistValues = EHGVFExceptionHandler.getActivePicklistValuesForField(Case.jl_Branch_master__c.getDescribe());
                for(String pValues : picklistValues) {
                    if(esbBranchValue.containsIgnoreCase(pValues)) {
                        receivedPickValueRevise = pValues;
                    }
                }
            }
            c.Reference_Id__c = ehRequest.supplierDirectInfo.productCode;
            c.CDH_location__c = EHConstants.SUPPLIER_DIRECT;
            c.Exception_Online__c = ehRequest.supplierDirectInfo.isOnline;
            c.Supplier_Name__c = supplierName;
            c.Product_Type__c = getProductType(supplierName);
            c.isSDOnline__c = ehRequest.supplierDirectInfo.isOnline;
            c.Description = DEFAULT_DESCRIPTION;
            c.Skip_Transfer_Case_Validation__c = TRUE;
            
            //offline SD
            if(!ehRequest.supplierDirectInfo.isOnline){
                c.jl_CRNumber__c = orderId;
                if(receivedPickValueRevise != NULL && receivedPickValueRevise != '') {
                    EH_Field_Mapping__c allFieldsQueryType = EH_Field_Mapping__c.getValues('EHBranchQuerySupplierDirectMap');
                    EH_Field_Mapping__c allFieldsComplaintType = EH_Field_Mapping__c.getValues('EHBranchComplaintSupplierDirectMap');
                    c.jl_Branch_master__c = receivedPickValueRevise;
                    if(c.RecordTypeId==queryCaseRecordTypeId) {
                        c.Contact_Reason__c = allFieldsQueryType.Contact_Reason__c;
                        c.Reason_Detail__c = allFieldsQueryType.Reason_Detail__c;
                        c.CDH_Site__c = allFieldsQueryType.CDH__c;
                    }
                    else {
                        c.Contact_Reason__c = allFieldsComplaintType.Contact_Reason__c;
                        c.Reason_Detail__c = allFieldsComplaintType.Reason_Detail__c;
                        c.CDH_Site__c = allFieldsComplaintType.CDH__c;
                    }
                }
            } 
            //online SD
            else {
                c.jl_OrderManagementNumber__c = orderId;
                if(receivedPickValueRevise != NULL && receivedPickValueRevise != '') {
                    c.jl_Branch_master__c = receivedPickValueRevise;
                }
                else if(receivedPickValueRevise == NULL || receivedPickValueRevise == '') {
                    EH_Field_Mapping__c branchMapForNULL = EH_Field_Mapping__c.getValues('EHOnlineSDNULLMap');
                    c.jl_Branch_master__c = branchMapForNULL.Branch__c;
                }
                EH_Field_Mapping__c allFieldsQueryType = EH_Field_Mapping__c.getValues('EHOnlineQuerySupplierDirectMap');
                EH_Field_Mapping__c allFieldsComplaintType = EH_Field_Mapping__c.getValues('EHOnlineComplaintSupplierDirectMap');
                if(c.RecordTypeId==queryCaseRecordTypeId) {
                    c.Contact_Reason__c = allFieldsQueryType.Contact_Reason__c;
                    c.Reason_Detail__c = allFieldsQueryType.Reason_Detail__c;
                    c.CDH_Site__c = allFieldsQueryType.CDH__c;
                }
                else {
                    c.Contact_Reason__c = allFieldsComplaintType.Contact_Reason__c;
                    c.Reason_Detail__c = allFieldsComplaintType.Reason_Detail__c;
                    c.CDH_Site__c = allFieldsComplaintType.CDH__c;
                }
            }
       		c.Status = EHConstants.STATUS_NEW;
       		try {
                insert c;
            }
            catch(Exception e) {
                c.jl_Branch_master__c = NULL;
                c.Contact_Reason__c = NULL;
                c.Reason_Detail__c =  NULL;
                c.CDH_Site__c =  NULL;
                insert c;
            }
        } else {
            c = caseList[0];
            if(c.isClosed){
        		c.Status = EHConstants.STATUS_NEW;
        		update c;
            }
        }
        return c;
    }

    
    /*
      - Creates a new Task and then assciates with the Case.
    */ 
    private Task createTaskForCase(EventHubVO.EHExceptionRequest ehRequest , Case caseObj){
        Task t = getTask(caseObj, ehRequest);
        t.RecordTypeId = eventHubSDTaskRecordTypeId;
        
        //Exception Information.
        t.EH_Order_Id__c = ehRequest.supplierDirectInfo.orderId;
        t.EH_promised_delivery_Date__c = ehRequest.supplierDirectInfo.promisedDeliveryDate;
        t.Supplier_Name__c = ehRequest.supplierDirectInfo.supplierName;
        t.Branch__c = ((ehRequest.supplierDirectInfo.isOnline) ? EHConstants.JL_ONLINE:ehRequest.supplierDirectInfo.branchName) ;
        t.EH_Further_Information__c = ehRequest.supplierDirectInfo.furtherInformation;
        t.EH_Extra_Information__c = ehRequest.supplierDirectInfo.extraInformation;
        t.Purchase_Order_No__c = ehRequest.supplierDirectInfo.purchaseOrderNo;
        t.Customer_Ordered_Date__c = ehRequest.supplierDirectInfo.customerOrderedDate;
        t.Narrative__c = ehRequest.supplierDirectInfo.narrative;
        t.Product_Description__c = ehRequest.supplierDirectInfo.productDescription;
        t.Expected_Delivery_Date__c = ehRequest.supplierDirectInfo.expectedDeliveryDate;
        t.Fulfilment_Type__c = ehRequest.supplierDirectInfo.fulfilmentType;
        t.OrderLine_Status_Reason__c = ehRequest.supplierDirectInfo.orderLineStatusReason;
        t.Other_Remarks__c = ehRequest.supplierDirectInfo.otherRemarks;
        t.Remark_1__c = ehRequest.supplierDirectInfo.supplierRemark1;
        t.Remark_2__c = ehRequest.supplierDirectInfo.supplierRemark2;
        t.Remark_3__c = ehRequest.supplierDirectInfo.supplierRemark3;
        t.Supplier_Expectation__c = ehRequest.supplierDirectInfo.supplierExpectation;
        t.Status_As_Of_Date__c = ehRequest.supplierDirectInfo.statusAsOfDate;
        t.Stock_Due_In_Date__c = ehRequest.supplierDirectInfo.stockDueInDate;
        t.Times_Exp_Del_Chg__c = ehRequest.supplierDirectInfo.timesExpDelChg;
        insert t;
        return t;
    }
    
    /*
      - Validates the ehRequest to check if it contains all the ClickandCollect information 
       to create Case/Task.
    */  
    private  EventHubVO.EHExceptionResponse checkInputRequestForError(EventHubVO.EHExceptionRequest ehRequest){
        EventHubVO.EHExceptionResponse ehResponse = null;
        String errorMessage = null;
        if (EHConstants.SUPPLIER_DIRECT.equalsIgnoreCase(ehRequest.deliveryType) && (ehRequest.supplierDirectInfo == null)){
            errorMessage = EHConstants.MANDATORY_PARAMETER_MISSING + EHConstants.SPLITTER + EHConstants.SUPPLIER_DIRECT_INFO ; 
        } else if (EHConstants.SUPPLIER_DIRECT.equalsIgnoreCase(ehRequest.deliveryType) && (String.isBlank(ehRequest.supplierDirectInfo.orderId))){
            errorMessage = EHConstants.MANDATORY_PARAMETER_MISSING + EHConstants.SPLITTER + EHConstants.SUPPLIER_DIRECT_INFO + EHConstants.SPLITTER + EHConstants.ORDER_ID ;
        } else if (EHConstants.SUPPLIER_DIRECT.equalsIgnoreCase(ehRequest.deliveryType) && ehRequest.supplierDirectInfo.isOnline == null ){
            errorMessage = EHConstants.MANDATORY_PARAMETER_MISSING + EHConstants.SPLITTER + EHConstants.SUPPLIER_DIRECT_INFO + EHConstants.SPLITTER + EHConstants.JL_ONLINE ;
        } 
        return getErrorResponse(errorMessage, ehRequest.ehExceptionId);
    }
    
     
    /* get the uniqueKey */
    public static String getUniqueKey(EventHubVO.EHExceptionRequest ehRequest){
        return '' + ehRequest.supplierDirectInfo.orderId + EHConstants.SPLITTER + DateTime.now().date() + EHConstants.SPLITTER + DateTime.now().hour(); 
    }
    
    @TestVisible
    private static String getProductType(String supplierName){
    	String productType = null;
    	if(String.isNotBlank(supplierName)){
    		productType = SUPPLIER_NAME_CONFIG.get(supplierName.toUpperCase());
    	}
        //Get the default value from the custom settings
        if(String.isBlank(productType) && defaultConfig != null){
        	productType = defaultConfig.Product_Type__c;
        }
        if(String.isBlank(productType)){
        	productType = DEFAULT_PRODUCT_TYPE;
        }
        return productType;
    }
    
    
    @TestVisible
    private static Map<String, String> buildSupplierNameMapping(){
     	
     	Map<String, String> mapping = new Map<String, String>();
     	
		for(Supplier_Name_Mapping__c config : Supplier_Name_Mapping__c.getAll().values()) {
			if(config != null && config.Supplier_Name__c != null) {
				mapping.put(config.Supplier_Name__c.toUpperCase(), config.Product_Type__c);
    }
		}

		return mapping;
     }
    
}