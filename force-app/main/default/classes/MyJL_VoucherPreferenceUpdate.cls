/**
 *  @author: Ravi Telu
 *  @date: 22/11/2019 10:54 
 *  @description:
 *      REST service to handle the incoming request from Loyalty for myJL voucher_preference_update for existing customers
 *  
 *  Version History :   
 *  22/11/2019 - COPT-5202 
 */
@RestResource(urlmapping='/MyJL_VoucherPreferenceUpdate/customer/*')
global class MyJL_VoucherPreferenceUpdate {
    
    private static VoucherPreferenceUpdateWrapper jsonResponse {get;set;}
    private static RestRequest req;
    private static RestResponse res;
    private static String SERVICE_NAME_FOR_ERROR='';
    
    @HttpPost
    global static VoucherPreferenceUpdateWrapper updateVoucherPreference(String voucherPreference, String shopperId, String loyaltyNumber, String messageId) {
        return getVoucherPreferenceUpdateData(voucherPreference, shopperId, loyaltyNumber, messageId);
    }
    
    global static VoucherPreferenceUpdateWrapper getVoucherPreferenceUpdateData(String voucherPreference, String shopperId, String loyaltyNumber, String messageId) {
        SERVICE_NAME_FOR_ERROR = 'MyJL_VoucherPreferenceUpdate REST Service';
        req = RestContext.request;
        res = RestContext.response;
        
        jsonResponse = NEW VoucherPreferenceUpdateWrapper();
        
        try {
            LogUtil.startLogHeaderREST(messageId, '', 'WS_UPDATE_CUSTOMER', shopperId, 1);
            if(String.isEmpty(messageId)) {
                res.statusCode = 400;
                jsonResponse = NEW VoucherPreferenceUpdateWrapper('BAD_REQUEST_PARAMETERS','Badly formatted parameter', messageId);
                MyJL_Util.logRejection(MyJL_Const.ERRORCODE.MESSAGEID_NOT_PROVIDED, '', loyaltyNumber, SERVICE_NAME_FOR_ERROR);
            }
            else if(String.isEmpty(shopperId)) {
                res.statusCode = 400;
                jsonResponse = NEW VoucherPreferenceUpdateWrapper('BAD_REQUEST_PARAMETERS','Badly formatted parameter', messageId);
                MyJL_Util.logRejection(MyJL_Const.ERRORCODE.CUSTOMERID_NOT_FOUND, '', shopperId, SERVICE_NAME_FOR_ERROR);
            } 
            else if(String.isEmpty(loyaltyNumber)) {
                res.statusCode = 400;
                jsonResponse = NEW VoucherPreferenceUpdateWrapper('BAD_REQUEST_PARAMETERS','Badly formatted parameter', messageId);
                MyJL_Util.logRejection(MyJL_Const.ERRORCODE.MY_LOYALTY_NUMBER_MISSING_DATA, '', loyaltyNumber, SERVICE_NAME_FOR_ERROR);
            }
            else if(String.isEmpty(voucherPreference)) {
                res.statusCode = 400;
                jsonResponse = NEW VoucherPreferenceUpdateWrapper('BAD_REQUEST_PARAMETERS','Badly formatted parameter', messageId);
                MyJL_Util.logRejection(MyJL_Const.ERRORCODE.FAILED_TO_PERSIST_DATA, '', loyaltyNumber, SERVICE_NAME_FOR_ERROR);
            } 
            else {
                System.debug('Success processing starts'); 
                List<Loyalty_Account__c> loyaltyAccountsUpdateList = NEW List<Loyalty_Account__c>();
                Map<String, Loyalty_Account__c> loyaltyAccountsUpdateMap = NEW Map<String, Loyalty_Account__c>();
                
                if(MyJL_Util.isMessageIdUnique(messageId)) {
                    Set<String> receivedCustomerIds = NEW Set<String>();
                    if(shopperId!=NULL && !String.isBlank(shopperId)) {
                        receivedCustomerIds.add(shopperId);               
                    }
                    System.debug('receivedCustomerIds @@@@@@@@@@@@@ '+receivedCustomerIds);
                    Map<String, Contact> contactProfileToLoyaltyAccount = NEW Map<String, Contact>();
                    
                    Contact[] contactProfiles = queryContactProfilesWithActiveLoyaltyAccounts(receivedCustomerIds);
                    System.debug('contactProfiles @@@@@@@@@@@ '+contactProfiles);
                    
                    Set<String> loyaltyNumbers = NEW Set<String>();
                    if (contactProfiles.size()>0) {
                        for(Contact contactProfile : contactProfiles) {
                            String customerId = contactProfile.Shopper_Id__c;
                            if(contactProfile.MyJL_Accounts__r!=NULL) {                                
                                contactProfileToLoyaltyAccount.put(customerId, contactProfile);
                                for(Loyalty_Account__c la : contactProfile.MyJL_Accounts__r){
                                    loyaltyNumbers.add(la.name);
                                }
                            }
                        }
                        
                        if(contactProfileToLoyaltyAccount.containsKey(shopperId)) {
                            if(contactProfileToLoyaltyAccount.get(shopperId).MyJL_Accounts__r!=NULL && contactProfileToLoyaltyAccount.get(shopperId).MyJL_Accounts__r.size()>0 && loyaltyNumbers.size()>0 && loyaltyNumbers.contains(loyaltyNumber)) {
                                Integer counter=0;
                                for(Loyalty_Account__c la : contactProfileToLoyaltyAccount.get(shopperId).MyJL_Accounts__r) {
                                    System.debug('la @@@@@@@@@@@@@@@@@@@@@@ '+la);
                                    if(la.IsActive__c==true && !loyaltyAccountsUpdateMap.containsKey(shopperId) && la.Name==loyaltyNumber) {
                                        System.debug('la.IsActive__c '+la.IsActive__c);
                                        Loyalty_Account__c dla = updateVoucherPreferenceLoyaltyAccounts(la, voucherPreference, messageId);
                                        loyaltyAccountsUpdateMap.put(shopperId, dla);
                                        loyaltyAccountsUpdateList.add(dla);
                                        counter++;                                            
                                    }
                                }
                                if(counter == 0) {
                                    System.debug('in active');
                                    res.statusCode = 409;
                                    jsonResponse = NEW VoucherPreferenceUpdateWrapper('INACTIVE_CUSTOMER_ACCOUNT','Customer account is inactive, preference can not be updated', messageId);
                                    MyJL_Util.logRejection(MyJL_Const.ERRORCODE.NO_ACTIVE_LOYALTY_ACCOUNT_FOUND, '', loyaltyNumber, SERVICE_NAME_FOR_ERROR);
                                }                                    
                            }
                            else {
                                System.debug('no loyalty account @@@@ ');
                                res.statusCode = 404;
                                jsonResponse = NEW VoucherPreferenceUpdateWrapper('NOT_FOUND','Customer with shopperId or membership with LoyaltyNumber does not exist', messageId);
                                MyJL_Util.logRejection(MyJL_Const.ERRORCODE.LOYALTY_ACCOUNT_NOT_FOUND, '', shopperId, SERVICE_NAME_FOR_ERROR);                                
                            }
                        }
                        else {
                            jsonResponse = NEW VoucherPreferenceUpdateWrapper('NOT_FOUND','Customer with shopperId or membership with LoyaltyNumber does not exist', messageId);
                            MyJL_Util.logRejection(MyJL_Const.ERRORCODE.NO_CUSTOMER_DATA, '', shopperId, SERVICE_NAME_FOR_ERROR);
                            res.statusCode = 404;
                        }
                        if(loyaltyAccountsUpdateList.size()>0) {
							saveResults(Database.Update(loyaltyAccountsUpdateList, false), loyaltyAccountsUpdateList, messageId);
                            MyJL_ContactProfileHandler.sendUpdateNotification(contactProfiles);
                        }
                    }
                    else {
                        res.statusCode = 404;
                        jsonResponse = NEW VoucherPreferenceUpdateWrapper('NOT_FOUND','Customer with shopperId or membership with LoyaltyNumber does not exist', messageId);
                        MyJL_Util.logRejection(MyJL_Const.ERRORCODE.NO_CUSTOMER_DATA, '', shopperId, SERVICE_NAME_FOR_ERROR);
                    }
                }
                else {
                    res.statusCode = 400;
                    jsonResponse = NEW VoucherPreferenceUpdateWrapper('BAD_REQUEST_PARAMETERS','Badly formatted parameter', messageId);
                    MyJL_Util.logRejection(MyJL_Const.ERRORCODE.MESSAGEID_DUPLICATE, '', loyaltyNumber, SERVICE_NAME_FOR_ERROR);
                }
            }
        }
        catch(Exception e) {
            jsonResponse = NEW VoucherPreferenceUpdateWrapper('UNEXPECTED_ERROR', 'Error occurred when processing the join request', messageId);
            MyJL_Util.logRejection(MyJL_Const.ERRORCODE.UNHANDLED_EXCEPTION, '', '',SERVICE_NAME_FOR_ERROR);
            res.statusCode = 500;
        }        
        return jsonResponse;
    }
    
    private static Contact[] queryContactProfilesWithActiveLoyaltyAccounts(Set<String> customerIds) {
        List<Contact> contactProfiles = NEW List<Contact>();
        
        if(customerIds != null && customerIds.size() > 0) {
            for (Contact cp: [
                SELECT
                id,
                Shopper_ID__c, SourceSystem__c, Email, 
                (Select 
                 id, Name,
                 IsActive__c,
                 contact__c
                 From MyJL_Accounts__r)
                FROM 
                contact
                WHERE
                Shopper_ID__c IN :customerIds
                AND SourceSystem__c = :Label.MyJL_JL_comSourceSystem
            ]) {  
                if (String.isNotBlank(cp.Shopper_ID__c)) {
                    contactProfiles.add(cp);
                }
            }
        }
        System.debug('contactProfiles @@@@@@@@@@@@@@@@@ '+contactProfiles);
        return contactProfiles;
    }
    
    private static Loyalty_Account__c updateVoucherPreferenceLoyaltyAccounts(Loyalty_Account__c la, final String voucherPreference, final String messageId) {
        Loyalty_Account__c loyaltyAccount = new Loyalty_Account__c();
        loyaltyAccount.Id = la.Id;
        loyaltyAccount.Voucher_Preference__c = voucherPreference;
        loyaltyAccount.MyJL_ESB_Message_ID__c = messageId;
        return loyaltyAccount;
    }
    
    //save the final results for voucher preference update
	private static void saveResults(Database.SaveResult[] results, final List<Loyalty_Account__c> loyAccList, final String messageId) {
        for(Integer i=0; i<results.size(); i++) {
            if(results[i].success) {
                res.statusCode = 204;
            } 
            else {
                for(Database.Error err : results[i].getErrors()) {
                    System.debug('Error @@@@@@@ '+err.getMessage());
                    res.statusCode = 400;
                    jsonResponse = NEW VoucherPreferenceUpdateWrapper('FAILED_TO_PERSIST_DATA', 'Failed during data commit', messageId);
                    MyJL_Util.logRejection(MyJL_Const.ERRORCODE.FAILED_TO_PERSIST_DATA, '', messageId, SERVICE_NAME_FOR_ERROR);
                }
            }
        }
    } 
    
    global class VoucherPreferenceUpdateWrapper {
        
        global String errorCode { get;set; } 
        global String errorMessage { get;set; }
        global String messageId { get; set; }
        
        global VoucherPreferenceUpdateWrapper() { }
        
        global VoucherPreferenceUpdateWrapper(String errorCode, String errorMessage, String messageId) {
            this.errorCode = errorCode;
            this.errorMessage = errorMessage;
            this.messageId = messageId;
        }
    }
}