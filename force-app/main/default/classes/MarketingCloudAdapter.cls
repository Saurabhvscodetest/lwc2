global class MarketingCloudAdapter {
    static string query;
    static string clientId;
    static string clientSecret;
    static string dataExtensionName;
    static string endpoint;
    static string tokenEndpoint;
    static boolean isDevEnv;
    
    static {
        Callout_MC_Connector__mdt[] calloutSettings = [SELECT Name__c, Client_ID__c, Client_Secret__c, Data_Extension_Name__c, Endpoint__c, Token_Endpoint__c FROM Callout_MC_Connector__mdt];
        Config_Settings__c mockConfig = Config_Settings__c.getInstance('MC_Connector_Mock_Enabled');
        isDevEnv = (mockConfig != null && mockConfig.Checkbox_Value__c) ? false: true;
        
        Callout_MC_Connector__mdt setting = null;
        
        for (Callout_MC_Connector__mdt calloutSetting : calloutSettings) {
            if(calloutSetting.Name__c == 'Test' && CommonStaticUtils.IS_SANDBOX){
                setting = calloutSetting;
                break;
            }
            
            if(calloutSetting.Name__c == 'Production' && CommonStaticUtils.IS_PRODUCTION){
                setting = calloutSetting; 
                break;
            }
        }
        
        clientId = setting.Client_ID__c;
        clientSecret = setting.Client_Secret__c;
        dataExtensionName = setting.Data_Extension_Name__c;
        endpoint = setting.Endpoint__c;
        tokenEndpoint = setting.Token_Endpoint__c;
    }
    
    @future(callout=true)
    public static void sendCaseUpdate(String caseNumber) {
        if(isDevEnv){
            return;
        }
        String accessToken = getAccessToken();
        addRecordToDE(accessToken, caseNumber);
    }
    
    
    /* This method gets the authentication token to be used in all requests - you'll need to add in the client ID and secret we provide */
    /* see https://developer.salesforce.com/docs/atlas.en-us.mc-getting-started.meta/mc-getting-started/get-access-token.htm */
    public static String getAccessToken() {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(tokenEndpoint);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        request.setBody('{"clientId":"'+ clientId + '","clientSecret":"' + clientSecret + '"}');
        HttpResponse response = http.send(request);
        // Parse the JSON response
        if (response.getStatusCode() != 201) {
            System.debug('The status code returned was not expected: ' +
                         response.getStatusCode() + ' ' + response.getStatus());
        } else {
            System.debug(response);
        }
        String accesstoken = null;
        JSONParser parser = JSON.createParser(response.getBody());
        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                String fieldName = parser.getText();
                parser.nextToken();
                if (fieldName == 'accessToken') {
                    accesstoken = parser.getText();
                }
            }
        }
        return accesstoken;
    }        
    
    
    /* this method adds a record to the Data Extension */
    /* help docs: https://developer.salesforce.com/docs/atlas.en-us.mc-apis.meta/mc-apis/create.htm */
    /* https://developer.salesforce.com/docs/atlas.en-us.mc-apis.meta/mc-apis/adding_data_to_data_extension_object.htm */
    
    public static void addRecordToDE(String accessToken, String caseNumber) {
        String soapEnvelope = makeHttpHeader();
        soapEnvelope=soapEnvelope.replace('{!accessToken}',accessToken);
        soapEnvelope+=
            '<s:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">'+
            '<CreateRequest xmlns="http://exacttarget.com/wsdl/partnerAPI">'+
            '<Options/>';
        soapEnvelope+='<Objects xsi:type="DataExtensionObject">';
        soapEnvelope+='<CustomerKey>' + dataExtensionName + '</CustomerKey>';
        soapEnvelope+= '<Properties>';
        soapEnvelope+= '<Property><Name>CaseID</Name><Value>' + caseNumber + '</Value></Property>';
        soapEnvelope+= '<Property><Name>Source</Name><Value>Connex</Value></Property>';
        soapEnvelope+= '<Property><Name>FeedbackType</Name><Value>DIRECT CALL</Value></Property>';
        soapEnvelope+= '<Property><Name>Action</Name><Value>Close</Value></Property>';
        soapEnvelope+= '<Property><Name>Comments</Name><Value></Value></Property>';
        soapEnvelope+= '</Properties>';
        soapEnvelope+= '</Objects>';              
        soapEnvelope+= '</CreateRequest>'+
            '</s:Body>'+
            '</s:Envelope>';
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setTimeout(60000);
        request.setEndpoint(endpoint);
        request.setMethod('POST');
        request.setHeader('Accept','text/xml');
        request.setHeader('Content-type','text/xml');
        request.setBody(soapEnvelope);
        HttpResponse response = http.send(request);
        if (response.getStatusCode() != 200 || response.getBody().contains('<OverallStatus>Error</OverallStatus>')) {
            system.debug(Logginglevel.ERROR,'An error occurred trying to execute a Soap call: '+response.getBody() + ' - ' +response.getStatusCode()
                         + ' - ' + 'endpoint: '+endpoint + ' - ' + 'body: ' + soapEnvelope);
        }else{
            system.debug('Http POST executed successfully! - '+response.getStatusCode() +' - ' + response.getBody());
        }
    }
    
    public static String makeHttpHeader(){
        String header =
            '<?xml version="1.0" encoding="UTF-8"?>'+
            '<s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope" xmlns:a="http://schemas.xmlsoap.org/ws/2004/08/addressing" xmlns:u="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">'+
            '<s:Header>'+
            '<a:Action s:mustUnderstand="1">Create</a:Action>'+
            '<a:MessageID>urn:uuid:7e0cca04-57bd-4481-864c-6ea8039d2ea0</a:MessageID>'+
            '<a:ReplyTo>'+
            '<a:Address>http://schemas.xmlsoap.org/ws/2004/08/addressing/role/anonymous</a:Address>'+
            '</a:ReplyTo>'+
            '<a:To s:mustUnderstand="1">https://mchz9hb-l1v7rrbg9bd1fjn3zx30.soap.marketingcloudapis.com/Service.asmx</a:To>'+
            '<fueloauth xmlns="http://exacttarget.com">{!accessToken}</fueloauth>'+
            '</s:Header>';
        return header;
    }
}