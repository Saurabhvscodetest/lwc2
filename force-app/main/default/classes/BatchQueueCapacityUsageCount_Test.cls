@istest(seealldata=false)
public class BatchQueueCapacityUsageCount_Test 
{
	@isTest static void testBatchQueueCapacityUsageCount()
    {
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User testUser;
        Case testCase;
        List<Case_Activity__c> testCaseActivity;
        Contact testContact;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;

            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        
        System.runAs(testUser) {

            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;

            testCase = CaseTestDataFactory.createCase(testContact.Id);
			testCase.Bypass_Standard_Assignment_Rules__c = true;
			insert testCase;
			system.assert([select Id from Case_Activity__c where Case__c = :testCase.Id].size() == 1);
            system.assert(CustomTimelineLEXController.getCountOfOpenActivities(testCase.Id) == 1);
			Test.startTest();
			datetime dtCurrentTime = datetime.now();
			testCaseActivity = new List<Case_Activity__c>();
            Case_Activity__c tempObj = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);

            tempObj.RecordTypeId = CaseActivityUtils.customerPromiseRTId;
            tempObj.Activity_Start_Date_Time__c = datetime.newInstance(dtCurrentTime.year(), dtCurrentTime.month(), dtCurrentTime.day(), 8, 0, 0);
            tempObj.Resolution_Method__c = 'Customer contacted';
            testCaseActivity.add(tempObj);
            
            tempObj = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            tempObj.RecordTypeId = CaseActivityUtils.customerPromiseRTId;
            tempObj.Activity_Start_Date_Time__c = datetime.newInstance(dtCurrentTime.year(), dtCurrentTime.month(), dtCurrentTime.day(), 10, 0, 0);
            tempObj.Resolution_Method__c = 'Email sent to customer';
            testCaseActivity.add(tempObj);
            
            tempObj = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            tempObj.Activity_Date__c = System.today();
            tempObj.Activity_Start_Date_Time__c = datetime.newInstance(dtCurrentTime.year(), dtCurrentTime.month(), dtCurrentTime.day(), 12, 0, 0);
            testCaseActivity.add(tempObj);
            
            tempObj = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            tempObj.Activity_Start_Date_Time__c = datetime.newInstance(dtCurrentTime.year(), dtCurrentTime.month(), dtCurrentTime.day(), 14, 0, 0);
            testCaseActivity.add(tempObj);
            
            tempObj = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            tempObj.RecordTypeId = CaseActivityUtils.internalActionRTId;
            tempObj.Activity_Start_Date_Time__c = datetime.newInstance(dtCurrentTime.year(), dtCurrentTime.month(), dtCurrentTime.day(), 16, 0, 0);
            tempObj.Resolution_Method__c = 'No action required';
            testCaseActivity.add(tempObj);
            
            tempObj = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            tempObj.RecordTypeId = CaseActivityUtils.internalActionRTId;
            tempObj.Activity_Start_Date_Time__c = System.now() - 1;
            tempObj.Activity_End_Date_Time__c = System.now() - 1;
            testCaseActivity.add(tempObj);
            
            tempObj = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            tempObj.RecordTypeId = CaseActivityUtils.internalActionRTId;
            tempObj.Activity_Start_Date_Time__c = System.now() + 1;
            tempObj.Activity_End_Date_Time__c = System.now() + 1;
            testCaseActivity.add(tempObj);
            
            tempObj = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            tempObj.RecordTypeId = CaseActivityUtils.internalActionRTId;
            tempObj.Activity_Start_Date_Time__c = System.today();
            tempObj.Activity_End_Date_Time__c = System.today();
            tempObj.Activity_Completed_Date_Time__c = System.now() - 1;
            testCaseActivity.add(tempObj);
            for(Case_Activity__c act:testCaseActivity)
            {
                act.Slot_Queue_Name__c = 'OSD';
                system.debug('@@ Activity_Date__c' + act.Activity_Date__c);
            }
            insert testCaseActivity;
            
        }
        
        
         list<Queue_Capacity_Limit__c> lstQCLmt = new list<Queue_Capacity_Limit__c>();
        Queue_Capacity_Limit__c QCLmt1 = new Queue_Capacity_Limit__c(Slot_Date__c= date.today(),Queue_Name__c='OSD',Slot1__c=100,Slot2__c=100,Slot3__c=100,Slot4__c=100,Slot5__c=100);
        lstQCLmt.add(QCLmt1);
        Queue_Capacity_Limit__c QCLmt2 = new Queue_Capacity_Limit__c(Slot_Date__c= date.today().adddays(2),Queue_Name__c='OSD',Slot1__c=100,Slot2__c=100,Slot3__c=100,Slot4__c=100,Slot5__c=100);
        lstQCLmt.add(QCLmt2);
        upsert lstQCLmt;
        
        Queue_Usage_Last_Run__c settings = Queue_Usage_Last_Run__c.getOrgDefaults();
		settings.BatchSize__c = 200;
        settings.QueueCapacityBatchCloseDate__c = system.now().adddays(-2);
        settings.QueueCapacityBatchStartDate__c = system.now().adddays(-2);
        settings.Slot_Time_1_Interval__c = '08:12';
        settings.Slot_Time_2_Interval__c = '10:14';
        settings.Slot_Time_3_Interval__c = '12:16';
        settings.Slot_Time_4_Interval__c = '14:18';
        settings.Slot_Time_5_Interval__c = '16:20';
        settings.Queue_Capacity_All_Slots__c = 'Test';
		upsert settings custSettings__c.Id;
        
        Config_Settings__c emailEnabledForBatch = new Config_Settings__c ();Config_Settings__c.getInstance('Enable Email Logging for Batch');
        emailEnabledForBatch.Name = 'Enable Email Logging for Batch';
        emailEnabledForBatch.Checkbox_Value__c = true;
        insert emailEnabledForBatch;
        
          ScheduleBatchQueueCapacityUsageCount sch = new ScheduleBatchQueueCapacityUsageCount();
        sch.execute(null);
        
        settings.QueueCapacityBatchCloseDate__c = system.now().adddays(-2);
        settings.QueueCapacityBatchStartDate__c = system.now().adddays(-2);
        upsert settings custSettings__c.Id;
        for(Case_Activity__c act1:testCaseActivity)
            {
                if(act1.Activity_Start_Date_Time__c != null)
                {
                    datetime stActTime = act1.Activity_Start_Date_Time__c;
                    stActTime.addMinutes(180);
                     
                    act1.Activity_Start_Date_Time__c = stActTime;
                    //act1.Activity_Completed_Date_Time__c =stActTime.addHours(-12);
                }
                act1.Slot_Queue_Name__c = 'OSD';
                system.debug('@@ Activity_Start_Date_Time__c' + act1.Activity_Start_Date_Time__c);
            }
            upsert testCaseActivity;
        
        
        sch.execute(null);
         try{
            integer x=1/0;
        }catch(Exception ex){
            sch.sendNotification('ScheduleBatchQueueCapacityUsageCount', 'Queue capacity usage upsert',  ex);
        }
        
        //BatchQueueCapcityUsageCount objBatch1 = new BatchQueueCapcityUsageCount();
        
        
        //Database.executeBatch(objBatch1, iBatchSize);
        
    }
}