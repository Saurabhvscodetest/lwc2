@isTest
public class MyJL_JoinRequestTest {

    private static final String shopperId = 'RaviTeluShopperId2020';
    private static final String loyaltyNumber = '60020488866';
    private static final String cardNumber = '8018505410660204905765';
    private static final String messageId = 'RaviTeluMessageId20';
    
    static {
    	BaseTriggerHandler.addSkipReason(CalloutHandler.SKIP_ALL_NOTIFICATIONS_CODE, 'disable outbound events in this test class');
    }
    
    @testSetup
    static void testData() {
        
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        
        Contact c = MyJL_TestUtil.createCustomer('Ordering_Customer');
        c.Shopper_ID__c = shopperId;
        update c;
        
        final List<Loyalty_Account__c> loyAccList = NEW List<Loyalty_Account__c>();
        loyAccList.add(NEW Loyalty_Account__c(Name=loyaltyNumber, ShopperId__c=shopperId, IsActive__c=true, Membership_Number__c=loyaltyNumber, Contact__c=c.Id));
        insert loyAccList;
        
        final List<Loyalty_Card__c> loyCardList = NEW List<Loyalty_Card__c>();
        loyCardList.add(NEW Loyalty_Card__c(Name=cardNumber, Disabled__c=false, Loyalty_Account__c=loyAccList[0].Id));
        Database.insert(loyCardList);
    }
    
    @isTest static void missingShopperId() {
                
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        RestContext.request = req;
        RestContext.response = res;        
        req.requestURI = '/MyJL_JoinRequest/customer/' ;
        req.httpMethod = 'POST';
        
        Test.startTest();
            try {
                MyJL_JoinRequest.JoinResponseWrapper joinRejoinRequest = MyJL_JoinRequest.processJoinRejoinRequest(NULL, messageId);
            }
            catch(Exception e) {}
        Test.stopTest();
    }
    
    @isTest static void missingMessageId() {
                
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        RestContext.request = req;
        RestContext.response = res;        
        req.requestURI = '/MyJL_JoinRequest/customer/' ;
        req.httpMethod = 'POST';
        
        Test.startTest();
            try {
                MyJL_JoinRequest.JoinResponseWrapper joinRejoinRequest = MyJL_JoinRequest.processJoinRejoinRequest(shopperId, NULL);
            }
            catch(Exception e) {}
        Test.stopTest();
    }
    
    @isTest static void noCustomerAvailable() {        
                
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        RestContext.request = req;
        RestContext.response = res;        
        req.requestURI = '/MyJL_JoinRequest/customer/' ;
        req.httpMethod = 'POST';
        
        Test.startTest();
            try {
                MyJL_JoinRequest.JoinResponseWrapper joinRejoinRequest = MyJL_JoinRequest.processJoinRejoinRequest('shopperId', messageId);
            }
            catch(Exception e) {}
        Test.stopTest();
    }
    
    @isTest static void noCustomerAvailableCaseSensitivity() {        
                
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        RestContext.request = req;
        RestContext.response = res;        
        req.requestURI = '/MyJL_JoinRequest/customer/' ;
        req.httpMethod = 'POST';
        
        Test.startTest();
            try {
                MyJL_JoinRequest.JoinResponseWrapper joinRejoinRequest = MyJL_JoinRequest.processJoinRejoinRequest('RaviteluShopperId2020', messageId);
            }
            catch(Exception e) {}
        Test.stopTest();
    }
    
    	    
    @isTest static void duplicateMessageId() {        
                
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        RestContext.request = req;
        RestContext.response = res;        
        req.requestURI = '/MyJL_JoinRequest/customer/' ;
        req.httpMethod = 'POST';
        
        //non unique message Id
		CustomSettings.setTestLogHeaderConfig(true, -1);
		CustomSettings.setTestLogDetailConfig(true, -1);

		Database.insert(new Log_Header__c(Message_ID__c = 'TEST-Duplicate_1', Service__c = MyJL_Util.SOURCE_SYSTEM));
        
        Test.startTest();
            try {
                MyJL_JoinRequest.JoinResponseWrapper joinRejoinRequest = MyJL_JoinRequest.processJoinRejoinRequest(shopperId, 'TEST-Duplicate_1');
            }
            catch(Exception e) {}
        Test.stopTest();
        try {
            MyJL_JoinRequest.JoinResponseWrapper joinRejoinRequest2 = MyJL_JoinRequest.processJoinRejoinRequest(shopperId, 'TEST-Duplicate_1');
        }
        catch(Exception e) {}
    }
    
    @isTest static void successProcessingRejoining() {        
                
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        RestContext.request = req;
        RestContext.response = res;        
        req.requestURI = '/MyJL_JoinRequest/customer/' ;
        req.httpMethod = 'POST';
        
        List<Loyalty_Account__c> laList = [ SELECT Id, IsActive__c, ShopperId__c FROM Loyalty_Account__c WHERE IsActive__c=true AND ShopperId__c = 'RaviTeluShopperId2020' LIMIT 1 ];
        laList[0].IsActive__c = false;
        update laList;
        
        Test.startTest();
            try {
                MyJL_JoinRequest.JoinResponseWrapper joinRejoinRequest = MyJL_JoinRequest.processJoinRejoinRequest(shopperId, messageId);
            }
            catch(Exception e) {}
        Test.stopTest();
    }
    
    @isTest static void successProcessingJoin() {        
                
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        RestContext.request = req;
        RestContext.response = res;        
        req.requestURI = '/MyJL_JoinRequest/customer/';
        req.httpMethod = 'POST';
        
        Contact c = [ SELECT Id, Shopper_ID__c FROM Contact WHERE Shopper_ID__c =: shopperId];
        delete [ SELECT Id FROM Loyalty_Account__c WHERE Contact__c =: c.Id ];
        
        Test.startTest();
            try {
                MyJL_JoinRequest.JoinResponseWrapper joinRejoinRequest = MyJL_JoinRequest.processJoinRejoinRequest(shopperId, messageId);
            }
            catch(Exception e) {}
        Test.stopTest();
    }
    
    @isTest static void unhandledException() {        
                
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        RestContext.request = req;
        RestContext.response = NULL;        
        req.requestURI = '/MyJL_JoinRequest/customer/';
        req.httpMethod = 'POST';
        
        Test.startTest();
            try {
                MyJL_JoinRequest.JoinResponseWrapper joinRejoinRequest = MyJL_JoinRequest.processJoinRejoinRequest(shopperId, messageId);
            }
        	catch(Exception e) {}
        Test.stopTest();
    }
}