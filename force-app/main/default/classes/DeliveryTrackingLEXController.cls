public class DeliveryTrackingLEXController {
    
    @AuraEnabled
    public static List<Case> fetchCaseInfo(String recordId){
        List<Case> CaseList = [Select id,jl_OrderManagementNumber__c,jl_DeliveryNumber__c from Case where id =: recordId AND (jl_OrderManagementNumber__c != NULL OR jl_DeliveryNumber__c != Null) Limit 1];
        return CaseList;
    }
    
    @AuraEnabled
    public static List<Case> fetchRelatedCase(String deliveryIdInput,String orderIdInput){
        if(String.isEmpty(deliveryIdInput)){
            List<Case> OrderCaseList = [Select id,jl_Case_Type__c,CaseNumber, RecordType.Name,status,createddate,OwnerId ,jl_DeliveryNumber__c, Show_Case__c from case where jl_OrderManagementNumber__c =: orderIdInput];
            return OrderCaseList;
        }
        else{
            List<Case> DeliveryCaseList = [Select id,jl_Case_Type__c,CaseNumber, RecordType.Name,status,createddate,OwnerId ,jl_DeliveryNumber__c, Show_Case__c from case where jl_DeliveryNumber__c =: deliveryIdInput];
            return DeliveryCaseList;
        }
    }
    
    
    @AuraEnabled
    public static WrapperResponse invokeDeliveryTracking(String deliveryIdInput,String orderIdInput){
        DeliveryTrackingController serviceClass = new DeliveryTrackingController();
        DeliveryTrackingVO result = serviceClass.searchDeliveryLex(orderIdInput,deliveryIdInput);
        WrapperResponse returnWR = new WrapperResponse();
        
        if(result!=null){
            if(result.ErrorInfo!= null){
                returnWR.errorType = result.ErrorInfo.errorType;
                returnWR.errorDescription = (EHConstants.ERROR_TYPE_BUSINESS.equalsIgnoreCase(returnWR.errorType) ? EHConstants.DT_ERROR_DESC_BUSSINESS: EHConstants.DT_ERROR_DESC_TECHNICAL)  ;
                return returnWR;
            }
            
            if(result.gvfDelInfList!= null && result.gvfDelInfList.size() > 0){
                String deliverySlotFrom = result.gvfDelInfList[0].deliverySlotFrom != null ?  result.gvfDelInfList[0].deliverySlotFrom.format('d MMM yyyy HH:mm')   : '';
                String deliverySlotTo = result.gvfDelInfList[0].deliverySlotTo != null ?  result.gvfDelInfList[0].deliverySlotTo.format('d MMM yyyy HH:mm')   : '';
                String deliveryPlannedDepartureTime = result.gvfDelInfList[0].deliveryPlannedDepartureTime != null ?  result.gvfDelInfList[0].deliveryPlannedDepartureTime.format('d MMM yyyy HH:mm')   : '';
                String deliveryPlannedArrivalTime = result.gvfDelInfList[0].deliveryPlannedArrivalTime != null ?  result.gvfDelInfList[0].deliveryPlannedArrivalTime.format('d MMM yyyy HH:mm')   : '';
                String deliveryTimeOnStatus = result.gvfDelInfList[0].deliveryTimeOnStatus != null ?  result.gvfDelInfList[0].deliveryTimeOnStatus.format('d MMM yyyy HH:mm')   : '';
                
                returnWR.gvfData = true;
                returnWR.recipientName = result.gvfDelInfList[0].recipientName;
                returnWR.deliverySlotFrom = deliverySlotFrom;
                returnWR.deliverySlotTo = deliverySlotTo;
                returnWR.deliveryPlannedDepartureTime = deliveryPlannedDepartureTime;
                returnWR.deliveryPlannedArrivalTime = deliveryPlannedArrivalTime;
                returnWR.deliveryTimeOnStatus = deliveryTimeOnStatus;
                returnWR.deliveryCDHLocation = String.valueOf( result.gvfDelInfList[0].deliveryCDHLocation);
                returnWR.deliveryStatus = String.valueOf( result.gvfDelInfList[0].deliveryStatus);
                returnWR.deliveryID = result.gvfDelInfList[0].deliveryID;
                returnWR.postCode = result.gvfDelInfList[0].recipientPostcode;
                returnWR.delTrack = result.gvfDelInfList[0].parcelInformationList;
            }
            
            If(result.ccOrderInf != null){
                returnWR.ccData = true;
                returnWR.CCcustomerName = result.ccOrderInf.customerName;
                returnWR.CCorderNumber = orderIdInput;
                returnWR.CCavailableForCollDate = result.ccOrderInf.availableForCollDate != null ?  result.ccOrderInf.availableForCollDate.format('d MMM yyyy HH:mm')   : '';
                returnWR.CCorderStatus = result.ccOrderInf.orderStatus;
                returnWR.CCmodeTransport = result.ccOrderInf.modeTransport;
                returnWR.CCexpectedLocation = result.ccOrderInf.expectedLocation;
                returnWR.CCtimeOfTheSatus = result.ccOrderInf.timeOfTheSatus != null  ?  result.ccOrderInf.timeOfTheSatus.format('d MMM yyyy HH:mm')   : '';
                returnWR.CCdelTrack = result.ccOrderInf.parcelInformationList;
            }
            
            If(result.cdOrderInf != null){
                returnWR.cdData = true;
                returnWR.CDOrderNumber = orderIdInput;
                returnWR.CDcustomerName = result.cdOrderInf.customerName;
                returnWR.CDbranchName = result.cdOrderInf.branchName;
                returnWR.CDdelTrack = result.cdOrderInf.parcelInformationList;
            }
            
            If(result.sdOrderInfList!= null && result.sdOrderInfList.size() > 0){
                returnWR.sdData = true;
                returnWR.SDCustomerName = result.sdOrderInfList[0].customerName;
                returnWR.SDPostcode = result.sdOrderInfList[0].postCode;
                returnWR.SDOrderNumber = orderIdInput;
                returnWR.SDBranch = result.sdOrderInfList[0].branchName;
                returnWR.SDSupplierName  =  result.sdOrderInfList[0].supplierName;
                returnWR.SDPurchaseOrderNumber =  result.sdOrderInfList[0].purchaseOrderNo;
                returnWR.SDdelTrack = result.sdOrderInfList[0].orderLineInformationList;
            }
            return returnWR;
        }
        else{
            return null;
        }
    }
    
    @AuraEnabled        
    public static list<String> fetchCurrentSignatureList(string Orderid){       
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setHeader('content-type', 'text/xml; charset=utf-8');
        req.setEndpoint('callout:NetScalar_NC/service/pfs/v1/routePlanner');
        req.setTimeout(90000);
        Http http = new Http();
        String SOAPXML= GVFWebServiceUtils.buildXMLImageRequest(Orderid);
        req.setBody(SOAPXML);
        HTTPResponse response = http.send(req); 
        if (response.getStatusCode() != 200) {
            system.debug(Logginglevel.ERROR,'An error occurred trying to execute a Soap call: '+response.getBody() + ' - ' +response.getStatusCode());
        }else{
            system.debug('Http POST executed successfully! - '+response.getStatusCode() +' - ' + response.getBody());
        }
        List<String> imagerespose = new List<string>();
        imagerespose = GVFWebServiceUtils.parsePictureValuesFromResponse(response.getBody());
        return imagerespose;                    
    }
    public class WrapperResponse{
        @AuraEnabled public List<DeliveryTrackingVO.ParcelInformation> delTrack {set;get;}
        @AuraEnabled public List<DeliveryTrackingVO.SDOrderLineItemInformation> SDdelTrack {set;get;}
        @AuraEnabled public List<DeliveryTrackingVO.ParcelInformation> CCdelTrack {set;get;}
        @AuraEnabled public List<DeliveryTrackingVO.CarrierParcelInformation> CDdelTrack {set;get;}
        @AuraEnabled public List<DeliveryTrackingVO.ParcelEvent> ParcelEventList {set;get;}
        @AuraEnabled public List<DeliveryTrackingVO.ErrorInfo> errors {set;get;}
        @AuraEnabled public String recipientName {set;get;}
        @AuraEnabled public String deliverySlotFrom {set;get;}
        @AuraEnabled public String deliverySlotTo {set;get;}
        @AuraEnabled public String deliveryPlannedDepartureTime {set;get;}
        @AuraEnabled public String deliveryPlannedArrivalTime {set;get;}
        @AuraEnabled public String deliveryCDHLocation {set;get;}
        @AuraEnabled public String deliveryStatus {set;get;}
        @AuraEnabled public String deliveryTimeOnStatus {set;get;}
        @AuraEnabled public String deliveryID {set;get;}
        @AuraEnabled public String postCode {set;get;}
        @AuraEnabled public String CRCustomerName {set;get;}
        @AuraEnabled public String CROrderNumber {set;get;}
        @AuraEnabled public String CRPostcode  {set;get;}
        @AuraEnabled public String CRBranch  {set;get;}
        @AuraEnabled public String CCcustomerName {set;get;}
        @AuraEnabled public String CCorderNumber {set;get;}
        @AuraEnabled public String CCavailableForCollDate  {set;get;}
        @AuraEnabled public String CCorderStatus  {set;get;}
        @AuraEnabled public String CCmodeTransport  {set;get;}
        @AuraEnabled public String CCexpectedLocation  {set;get;}
        @AuraEnabled public String CCtimeOfTheSatus  {set;get;}
        @AuraEnabled public String CDcustomerName  {set;get;}
        @AuraEnabled public String CDbranchName  {set;get;}
        @AuraEnabled public String CDOrderNumber  {set;get;}
        @AuraEnabled public String SDCustomerName  {set;get;}
        @AuraEnabled public String SDPostcode  {set;get;}
        @AuraEnabled public String SDOrderNumber  {set;get;}
        @AuraEnabled public String SDBranch  {set;get;}
        @AuraEnabled public String SDSupplierName  {set;get;}
        @AuraEnabled public String SDPurchaseOrderNumber  {set;get;}
        @AuraEnabled public boolean gvfData = false; 
        @AuraEnabled public boolean sdData = false;
        @AuraEnabled public boolean ccData = false;
        @AuraEnabled public boolean cdData = false;
        @AuraEnabled public String errorType;
        @AuraEnabled public String errorDescription ;
    }
    
}