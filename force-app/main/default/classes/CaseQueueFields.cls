/**
* @description  Class to set values on Case object      
*/

public class CaseQueueFields {

    /**
    * @description  Sets the First Queue field on Cases based on either a) the first Queue to own the Case, 
    *        or the Primary Team of the first User to own the Case      
    * @contexts   BeforeInsert, BeforeUpdate
    * @param    newCaseList     List of new Cases
    */
    public static void setFirstQueueFields(List<Case> newCaseList) {
        
        List<Case> casesOwnedByQueue = new List<Case>();
        List<Case> casesOwnedByUser = new List<Case>();
        for(Case newCase : newCaseList) {
            if(String.isEmpty(newCase.JL_First_Queue__c)) {
                if(newCase.Origin != 'Email' || (newCase.Origin == 'Email' && !String.isEmpty(newCase.JL_Email_Address__c))) {
                    if(newCase.OwnerId != null && newCase.OwnerId.getSObjectType() == User.SObjectType) {
                        // Set First Queue as User Primary Team
                        casesOwnedByUser.add(newCase);
                    } 
                    else if (newCase.OwnerId != null && newCase.OwnerId.getSObjectType() == Group.SObjectType) {
                        // Set First Queue as Owner
                        casesOwnedByQueue.add(newCase);
                    }
                }
            }
        }
        setFirstQueueFieldsQueueOwner(casesOwnedByQueue);
        setFirstQueueFieldsUserOwner(casesOwnedByUser);
    }

    private static void setFirstQueueFieldsQueueOwner(List<Case> casesOwnedByQueue) {
        Set<Id> ownerIds = new Set<Id>();
        Map<Id, Group> ownerQueueGroups = NEW Map<Id, Group>();
        for(Case aCase : casesOwnedByQueue) {
            ownerIds.add(aCase.OwnerId);
        }
        if(ownerIds.size()>0) {
            ownerQueueGroups = new Map<Id, Group>([SELECT Id, DeveloperName FROM Group WHERE Id IN :ownerIds]);
            
            for(Case aCase : casesOwnedByQueue) {
                aCase.JL_First_Queue__c = aCase.OwnerId;
                
                if(ownerQueueGroups.containsKey(aCase.OwnerId)) {
                    aCase.JL_First_Queue_Name__c = ownerQueueGroups.get(aCase.OwnerId).DeveloperName;
                }
            }
        }
    }

    private static void setFirstQueueFieldsUserOwner(List<Case> casesOwnedByUser) {
        Set<Id> ownerIds = new Set<Id>();
        
        for(Case aCase : casesOwnedByUser) {
            ownerIds.add(aCase.OwnerId);
        }
        
        if(ownerIds.size()>0) {
            Map<Id, Team__c> teamMap = TeamUtils.getTeamForUserIDs(ownerIds);
            
            for(Case aCase : casesOwnedByUser) {
                if(teamMap.containsKey(aCase.OwnerId) && teamMap.get(aCase.OwnerId) != null) { // Map can have null values
                    aCase.JL_First_Queue_Name__c = teamMap.get(aCase.OwnerId).Queue_Name__c;
                    aCase.JL_First_Queue__c = TeamUtils.getQueueIdForTeam(teamMap.get(aCase.OwnerId).Name);
                }
            }
        }
    }
}