public without sharing class PostChatUpdateTranscript {

	public static void processRecords(LiveChatTranscript lct) { 
		try {
			update lct;
		} catch (Exception ex) {
			system.debug('warning: problem updating transcript: '+ex);
		}		
	}
}