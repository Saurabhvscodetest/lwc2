/**
* @description  Unit tests for Log p A Call Page / Controller
* @author       @tomcarman
* @created      2017-03-15
*/


@isTest
private class LogACallControllerTest {


    /**
    * @description  Set up dependent custom settings        
    * @author       @tomcarman  
    */

    @testSetup static void initTestData() {

        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();

        JL_RunValidations__c runValidationSetting = JL_RunValidations__c.getOrgDefaults();
        runValidationSetting.Run_Validations__c = true;
        upsert runValidationSetting JL_RunValidations__c.Id;

    }



    /**
    * @description  Check that controller init correctly            
    * @author       @tomcarman  
    */

    @isTest static void testControllerInitialisedCorrectly() {

        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User frontOfficeUser;

        Contact testContact;
        Case testCase;

        System.runAs(runningUser) {

            frontOfficeUser = UnitTestDataFactory.getFrontOfficeUser('TEST_USER');
            frontOfficeUser.Team__c = 'Hamilton/Didsbury - CST';
            insert frontOfficeUser;
            String ps1 = 'JL_MYJL_Phase_3_Contact_Center_Permissions';
            UserTestDataFactory.assignPermissionSetToUser(frontOfficeUser.id,ps1);

        }   


        System.runAs(frontOfficeUser) {

            testContact = UnitTestDataFactory.createContact();
            testContact.Email = 'tom.riddle@test.com';
            insert testContact;

            testCase = UnitTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            testCase.Create_Case_Activity__c = true;
            insert testCase;

            PageReference pageRef = Page.LogACall;
            Test.setCurrentPage(pageRef);

            System.currentPageReference().getParameters().put('caseId', testCase.Id);

            LogACallController controller = new LogACallController();

            System.assertEquals(testCase.Id, controller.relatedCase.Id, 'The relatedCase should have been set in controller via the URL param caseId');
            System.assertNotEquals(null, controller.relatedCase.CaseNumber, 'The relatedCase.CaseNumber should not be null, as the controller should query the values');

        }
    }



    /**
    * @description  Test a succesful save of Task and related Case. Ensure Case values are updated.         
    * @author       @tomcarman  
    */

    @isTest static void testSaveSuccess() {

        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        User frontOfficeUser;

        Contact testContact;
        Case testCase;

        System.runAs(runningUser) {

            frontOfficeUser = UnitTestDataFactory.getFrontOfficeUser('TEST_USER');
            frontOfficeUser.Team__c = 'Hamilton/Didsbury - CST';
            insert frontOfficeUser;
            String ps1 = 'JL_MYJL_Phase_3_Contact_Center_Permissions';
            UserTestDataFactory.assignPermissionSetToUser(frontOfficeUser.id,ps1);
        }


        System.runAs(frontOfficeUser) {

            testContact = UnitTestDataFactory.createContact();
            testContact.Email = 'tom.riddle@test.com';
            insert testContact;

            testCase = UnitTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            testCase.Number_of_Repeat_Contacts__c = 1;
            testCase.Repeat_Contact_from_Customer__c = false;
            testCase.Create_Case_Activity__c = true;
            insert testCase;

            PageReference pageRef = Page.LogACall;
            Test.setCurrentPage(pageRef);

            System.currentPageReference().getParameters().put('caseId', testCase.Id);

            LogACallController controller = new LogACallController();

            // Fill values
            controller.RepeatContactFromCustomer = true;

            controller.aTask.Level_1__c = 'JohnLewis.com';
            controller.aTask.Level_2__c = 'JL.Com After Sales';
            controller.aTask.Level_3__c = 'JL.Com Payment Enquiries';
            controller.aTask.Level_4__c = 'JL.Com Post-Payment';
            controller.aTask.Level_5__c = 'JL.Com Duplicate Receipt';

            controller.aTask.Customer_Chasing__c = true;
            controller.relatedCase.JL_DeliveryNumber__c = '88888888';
    
            controller.doSave();

            System.assert(controller.saveSuccess);

        }


        Case savedCase = [SELECT Id, JL_DeliveryNumber__c, Number_of_Repeat_Contacts__c, Repeat_Contact_from_Customer__c FROM Case WHERE Id = :testCase.Id];
        Task savedTask = [SELECT Id, Level_1__c, Level_2__c, Level_3__c, Level_4__c, Level_5__c, Customer_Chasing__c 
                            FROM Task 
                            WHERE WhoId = :testContact.Id AND WhatId = :testCase.Id];


        System.assertEquals(2, savedCase.Number_of_Repeat_Contacts__c);
        System.assertEquals(true, savedCase.Repeat_Contact_from_Customer__c);
        System.assertEquals('88888888', savedCase.JL_DeliveryNumber__c);
        System.assertEquals('JohnLewis.com', savedTask.Level_1__c);
        System.assertEquals('JL.Com After Sales', savedTask.Level_2__c);
        System.assertEquals('JL.Com Payment Enquiries', savedTask.Level_3__c);
        System.assertEquals('JL.Com Post-Payment', savedTask.Level_4__c);
        System.assertEquals('JL.Com Duplicate Receipt', savedTask.Level_5__c);
        System.assertEquals(true, savedTask.Customer_Chasing__c);

    }



    /**
    * @description  Test a save cycle that doesnt pass validation. Ensure any Case values are rolled back.      
    * @author       @tomcarman
    */

    @isTest static void testSaveError() {

        User runningUser = [SELECT Id FROM USer WHERE Id = :UserInfo.getUserId()];

        User frontOfficeUser;
        Contact testContact;
        Case testCase;

        System.runAs(runningUser) {
            frontOfficeUser = UnitTestDataFactory.getFrontOfficeUser('TEST_USER');
            frontOfficeUser.Team__c = 'Hamilton/Didsbury - CST';
            insert frontOfficeUser;
            String ps1 = 'JL_MYJL_Phase_3_Contact_Center_Permissions';
            UserTestDataFactory.assignPermissionSetToUser(frontOfficeUser.id,ps1);
        }

        System.runAs(frontOfficeUser) {

            testContact = UnitTestDataFactory.createContact();
            testContact.Email = 'tom@riddle.com';
            insert testContact;

            testCase = UnitTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            testCase.Create_Case_Activity__c = true;
            insert testCase;

            PageReference pageRef = Page.LogACall;
            Test.setCurrentPage(pageRef);
            System.currentPageReference().getParameters().put('caseId', testCase.Id);

            LogACallController controller = new LogACallController();

            // Dont fill all values
            controller.aTask.Level_1__c = 'JohnLewis.Com';
            controller.relatedCase.JL_DeliveryNumber__c = '88888';

            // Try to save
            controller.doSave();

            Case savedCase = [SELECT Id, JL_DeliveryNumber__c FROM Case WHERE Id = :testCase.Id];

            System.assertEquals(false, controller.saveSuccess);
            System.assertEquals(null, controller.aTask.Id);
            System.assertEquals(null, savedCase.JL_DeliveryNumber__c);

        }
    }

        /**
    * @description  Test the number of repeat contact does not exceed the maximum amount - capped at 99.            
    * @author       @stuartbarber   
    */

    @isTest static void testMaxNoOfRepeatContacts() {

        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        User frontOfficeUser;

        Contact testContact;
        Case testCase;

        System.runAs(runningUser) {

            frontOfficeUser = UnitTestDataFactory.getFrontOfficeUser('TEST_USER');
            frontOfficeUser.Team__c = 'Hamilton/Didsbury - CST';
            insert frontOfficeUser;
            String ps1 = 'JL_MYJL_Phase_3_Contact_Center_Permissions';
            UserTestDataFactory.assignPermissionSetToUser(frontOfficeUser.id,ps1);
        }

        System.runAs(frontOfficeUser) {

            testContact = UnitTestDataFactory.createContact();
            testContact.Email = 'tom.riddle@test.com';
            insert testContact;

            testCase = UnitTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            testCase.Number_of_Repeat_Contacts__c = 99;
            testCase.Repeat_Contact_from_Customer__c = false;
            testCase.Create_Case_Activity__c = true;
            insert testCase;

            PageReference pageRef = Page.LogACall;
            Test.setCurrentPage(pageRef);

            System.currentPageReference().getParameters().put('caseId', testCase.Id);

            LogACallController controller = new LogACallController();

            // Fill values
            controller.RepeatContactFromCustomer = true;

            controller.aTask.Level_1__c = 'JohnLewis.com';
            controller.aTask.Level_2__c = 'JL.Com After Sales';
            controller.aTask.Level_3__c = 'JL.Com Payment Enquiries';
            controller.aTask.Level_4__c = 'JL.Com Post-Payment';
            controller.aTask.Level_5__c = 'JL.Com Duplicate Receipt';

            controller.aTask.Customer_Chasing__c = true;

            controller.relatedCase.JL_DeliveryNumber__c = '88888888';
    
            controller.doSave();

            System.assert(controller.saveSuccess);

        }

        Case savedCase = [SELECT Id, JL_DeliveryNumber__c, Number_of_Repeat_Contacts__c, Repeat_Contact_from_Customer__c FROM Case WHERE Id = :testCase.Id];

        System.assertEquals(99, savedCase.Number_of_Repeat_Contacts__c);
    }

}