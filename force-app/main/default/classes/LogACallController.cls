/**
* @description  Controller for the Log A Call page
* @author       @tomcarman
* @created      2017-03-16
*/

public with sharing class LogACallController {
    
    private List<Task> previousTask;

    public Task aTask {get; set;}
    public Case relatedCase {get; set;}
    public Boolean saveSuccess {get; set;} // Boolean to report to the page if the save cycle was succesful or not - needed when closing tabs with JS in console
    public Boolean RepeatContactFromCustomer {get;set;}
    
    private static final Id CALL_LOG_TASK_RECORD_TYPE_ID = Schema.SObjectType.Task.getRecordTypeInfosByName().get(CommonStaticUtils.TASK_RT_NAME_CALL_LOG).getRecordTypeId();
    private Id relatedCaseId;



    /**
    * @description   Standard constructor. Instantiates a new Task. Retreives related Case based on caseId passed as URL param.            
    * @author        @tomcarman      
    */

    public LogACallController() {
        
        aTask = new Task();
        relatedCaseId = ApexPages.currentPage().getParameters().get('caseId');
        relatedCase = [SELECT Id, CaseNumber, ContactId, Description, JL_CRNumber__c, JL_OrderManagementNumber__c, JL_DeliveryNumber__c, JL_pse1_Stock_Number__c, Number_of_Repeat_Contacts__c, Repeat_Contact_from_Customer__c FROM Case WHERE Id = :relatedCaseId];

        // Get most recent Task and prepopulate Levels with same values
        
        previousTask = [SELECT Id, Level_1__c, Level_2__c, Level_3__c, Level_4__c, Level_5__c FROM Task WHERE WhatId = :relatedCaseId ORDER BY CreatedDate DESC LIMIT 1];

        if(!previousTask.isEmpty()) {
            aTask.Level_1__c = previousTask[0].Level_1__c;
            aTask.Level_2__c = previousTask[0].Level_2__c;
            aTask.Level_3__c = previousTask[0].Level_3__c;
            aTask.Level_4__c = previousTask[0].Level_4__c;
            aTask.Level_5__c = previousTask[0].Level_5__c;
        }


    }



    /**
    * @description   Logic to save Task and update related Case if required.          
    * @author        @tomcarman     
    */

    public void doSave() {
        
        saveSuccess = true;
        Savepoint sp = Database.setSavepoint();
             
        try {

            update relatedCase;

        } catch (DmlException e) {

            List<String> fieldErrors = e.getDmlFieldNames(0);

            for(String fieldName : fieldErrors) {
                relatedCase.put(fieldName, null);
            }

            saveSuccess = false;
        }


        if(saveSuccess) {

            try {
                // Set default vals
                aTask.WhatId = relatedCase.Id;
                aTask.WhoId = relatedCase.ContactId;
                aTask.Subject = CommonStaticUtils.TASK_SUBJECT_CALL_LOG;
                aTask.Status = CommonStaticUtils.TASK_STATUS_CLOSED_RESOLVED;
                aTask.RecordTypeId = CALL_LOG_TASK_RECORD_TYPE_ID;
                aTask.Description = aTask.JL_Description__c;
                aTask.ActivityDate = Date.today();
                aTask.Repeat_Contact_from_Customer__c = RepeatContactFromCustomer;

                insert aTask;
        
            } catch (Exception e) {
                Database.rollback(sp);
                saveSuccess = false;
                throw e;

            }
        }
    }

}