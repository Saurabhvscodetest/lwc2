@isTest
private class MyPanPartnershipSchemeTest {
	private static final String SHOPPER_ID = '1234abcdefg';
	private static final String PARTNERSHIP_CARD_NUMBER = '3456789456123';
    Private static final Contact customer;
    Private static final Case cs;
    static{
    BaseTriggerHandler.addSkipReason(CalloutHandler.SKIP_ALL_NOTIFICATIONS_CODE, 'disable outbound events in this test class');
    }
    @testSetup
    static void setup(){
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        
        final List<Loyalty_Account__c> loyaltyAccounts = new List<Loyalty_Account__c>();
		loyaltyAccounts.add(new Loyalty_Account__c(Name = PARTNERSHIP_CARD_NUMBER ,ShopperId__c=SHOPPER_ID,IsActive__c = true,Membership_Number__c = PARTNERSHIP_CARD_NUMBER));
		insert loyaltyAccounts;
        
		final List<Loyalty_Card__c> cards = new List<Loyalty_Card__c>();
		cards.add(new Loyalty_Card__c(Name = PARTNERSHIP_CARD_NUMBER,Disabled__c = false , Loyalty_Account__c = loyaltyAccounts[0].Id));
		Database.insert(cards);
        
    }
        
  
    @isTest static void testServiceWithNoPartnershipCardNumber() {

		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = '/MyPanPartnershipScheme/RequestCard/' + SHOPPER_ID;
		req.httpMethod = 'PUT';

		Test.startTest();
		MyPanPartnershipScheme.ResponseWrapper rs = MyPanPartnershipScheme.getPanPartnershipCard(null);
		Test.stopTest();
		
        
        integer statusCode = res.statusCode;
        System.assertEquals(400, statusCode);
        System.assertEquals('MISSING_PARTNERSHIP_NUMBER',rs.errorCode);
        System.assertEquals('partnershipCardNumber: is not provided',rs.errorMessage);
        
		
	}

	
	@isTest static void testServiceWithNoShopperProvided() {

		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = '/MyPanPartnershipScheme/RequestCard/';
		req.httpMethod = 'PUT';

		Test.startTest();
		MyPanPartnershipScheme.ResponseWrapper rs = MyPanPartnershipScheme.getPanPartnershipCard(PARTNERSHIP_CARD_NUMBER );
		Test.stopTest();
        
		integer statusCode = res.statusCode;
        System.assertEquals(400, statusCode);
        System.assertEquals('MISSING_JL_SHOPPER_ID',rs.errorCode);
        System.assertEquals('shopperId: is not provided',rs.errorMessage);
		
	}

	@isTest static void testServiceWithNoShopperNotFound() {

		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = '/MyPanPartnershipScheme/RequestCard/1234567';
		req.httpMethod = 'PUT';

		Test.startTest();
		MyPanPartnershipScheme.ResponseWrapper rs = MyPanPartnershipScheme.getPanPartnershipCard(PARTNERSHIP_CARD_NUMBER );
		Test.stopTest();
        
        integer statusCode = res.statusCode;
        System.assertEquals(404, statusCode);
        System.assertEquals('NO_LAYOUT_ACCOUNTS_FOUND',rs.errorCode);
        System.assertEquals('No Loyalty accounts found for shopperId',rs.errorMessage);

		
	}

	@isTest static void testServiceWithDeactivatedAccount() {

		Loyalty_Account__c account = [select IsActive__c From Loyalty_Account__c where ShopperId__c = :SHOPPER_ID Limit 1];

		if( account != null){
			account.IsActive__c = false;
			update account;
		}

		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = '/MyPanPartnershipScheme/RequestCard/' + SHOPPER_ID;
		req.httpMethod = 'PUT';

		Test.startTest();
		MyPanPartnershipScheme.ResponseWrapper rs = MyPanPartnershipScheme.getPanPartnershipCard(PARTNERSHIP_CARD_NUMBER );
		Test.stopTest();
		
        integer statusCode = res.statusCode;
        System.assertEquals(409, statusCode);
        System.assertEquals('ACCOUNT_NOT_ACTIVE',rs.errorCode);
        System.assertEquals('Account is not active',rs.errorMessage);
	}

    @isTest static void testServiceWithDifferentMembershipNumber() { // check

		Loyalty_Account__c account = [select Membership_Number__c From Loyalty_Account__c where ShopperId__c = :SHOPPER_ID Limit 1];

		if( account != null){
			account.Membership_Number__c =  null;
			update account;
		}

		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = '/MyPanPartnershipScheme/RequestCard/' + SHOPPER_ID;
		req.httpMethod = 'PUT';

		Test.startTest();
		MyPanPartnershipScheme.ResponseWrapper rs = MyPanPartnershipScheme.getPanPartnershipCard(PARTNERSHIP_CARD_NUMBER );
		Test.stopTest();
        
        integer statusCode = res.statusCode;
        System.assertEquals(409, statusCode);
        System.assertEquals('PARTNERSHIP_NUMBER_NOT_ASSOCIATED_WITH_SHOPPER_ID',rs.errorCode);
        System.assertEquals('Partnership Card Number is not associated to shopperId',rs.errorMessage);
		
	}

    @isTest static void testServiceWithDisabledCard() {
		List<Loyalty_Account__c> accountList = [select id From Loyalty_Account__c where ShopperId__c = :SHOPPER_ID Limit 1];
        for(Loyalty_Account__c lc : accountList){
            lc.Membership_Number__c = PARTNERSHIP_CARD_NUMBER;
        }
        update accountList;
        
		Loyalty_Card__c card = [Select id,Disabled__c from Loyalty_Card__c where Name =:PARTNERSHIP_CARD_NUMBER Limit 1];
        
		if( card != null){
			card.Disabled__c = true;
			update card;
		}
        
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = '/MyPanPartnershipScheme/RequestCard/' + SHOPPER_ID;
		req.httpMethod = 'PUT';

		Test.startTest();
		MyPanPartnershipScheme.ResponseWrapper rs = MyPanPartnershipScheme.getPanPartnershipCard(PARTNERSHIP_CARD_NUMBER);
		Test.stopTest();
		
        integer statusCode = res.statusCode;
        System.assertEquals(409, statusCode);
        System.assertEquals('LOYALTY_CARD_IS_NOT_ACTIVE',rs.errorCode);
        System.assertEquals('Loyalty Card is not active',rs.errorMessage);

	}

	@isTest static void testServiceWithShopper() {
        
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = '/MyPanPartnershipScheme/RequestCard/' + SHOPPER_ID;
		req.httpMethod = 'PUT';

		Test.startTest();
        Contact customer = MyJL_TestUtil.createCustomer('Ordering_Customer');
        customer.Shopper_ID__c = SHOPPER_ID;
        update customer;
        
        MyJL_Outbound_Staging__c JL = new MyJL_Outbound_Staging__c();
		MyPanPartnershipScheme.ResponseWrapper rs = MyPanPartnershipScheme.getPanPartnershipCard(PARTNERSHIP_CARD_NUMBER);
        List<Case> getcase = new list<Case>([SELECT id,despatch_status__c,status,Address_Line_1__c,Address_Line_2__c, Address_Line_3__c,Address_Line_4__c  FROM Case]);        
        List<MyJL_Outbound_Staging__c> getOutst = new list<MyJL_Outbound_Staging__c>([select id,Case_Reference_ID__c,Address_LIne_1__c, Address_Line_2__c, Address_Line_3__c, Address_Line_4__c  from MyJL_Outbound_Staging__c where Case_Reference_ID__c =:getcase[0].Id]);
        
       
       
		system.assert(!getcase.isEmpty(), 'Case should be created' );
		Case actualCase = getcase[0];        
        system.assertequals('Closed - No Response Required',actualCase.status); 
        system.assertequals(customer.Mailing_Street__c,actualCase.Address_Line_1__c); 
        system.assertequals(customer.Mailing_Address_Line2__c,actualCase.Address_Line_2__c); 
        system.assertequals(customer.Mailing_Address_Line3__c,actualCase.Address_Line_3__c); 
        system.assertequals(customer.Mailing_Address_Line4__c,actualCase.Address_Line_4__c); 
        system.assertequals('Processing',actualCase.despatch_status__c); 
        
        system.assertequals(1,getOutst.size());
        MyJL_Outbound_Staging__c outboundStagingRecord = getOutst[0]; 
        
        system.assertequals(customer.Mailing_Street__c,outboundStagingRecord.Address_Line_1__c); 
        system.assertequals(customer.Mailing_Address_Line2__c,outboundStagingRecord.Address_Line_2__c); 
        system.assertequals(customer.Mailing_Address_Line3__c,outboundStagingRecord.Address_Line_3__c); 
        system.assertequals(customer.Mailing_Address_Line4__c,outboundStagingRecord.Address_Line_4__c); 
  
		Test.stopTest();

	}

	@isTest static void testServiceForCatchBlock() {

		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = null;
		req.httpMethod = 'PUT';

		Test.startTest();
		MyPanPartnershipScheme.ResponseWrapper rs = MyPanPartnershipScheme.getPanPartnershipCard(PARTNERSHIP_CARD_NUMBER );
		Test.stopTest();

        integer statusCode = res.statusCode;
        System.assertEquals(500, statusCode);
        System.assertEquals('CONNEX_SERVER_ERROR',rs.errorCode);
	}
    
    
}