@isTest
public class IFC_EmaiServiceToCreateCase_Test {
    //Test Method 1
    static testMethod void IFC_EmaiServiceToCreateCase_Test_MTD1() {
        //Custom Setting
        IFC_CSVFieldMapping__c setting1 = new IFC_CSVFieldMapping__c();
        setting1.Name = 'IFC Department';
        setting1.Feild_Name__c = 'IFC_Department__c';
        insert setting1;
        
        IFC_CSVFieldMapping__c setting2 = new IFC_CSVFieldMapping__c();
        setting2.Name = 'Branch Number';
        setting2.Feild_Name__c = 'Branch_Number__c';
        insert setting2;
        
        IFC_CSVFieldMapping__c setting3 = new IFC_CSVFieldMapping__c();
        setting3.Name = 'Financial Reporting Period';
        setting3.Feild_Name__c = 'Financial_Reporting_Period__c';
        insert setting3;
        
        IFC_CSVFieldMapping__c setting4 = new IFC_CSVFieldMapping__c();
        setting4.Name = 'Type of Entry Outstanding';
        setting4.Feild_Name__c = 'Type_of_Entry_Outstanding__c';
        insert setting4;
        
        IFC_CSVFieldMapping__c setting5 = new IFC_CSVFieldMapping__c();
        setting5.Name = 'Date Processed';
        setting5.Feild_Name__c = 'Date_Processed__c';
        insert setting5;
        
        IFC_CSVFieldMapping__c setting6 = new IFC_CSVFieldMapping__c();
        setting6.Name = 'Error Description';
        setting6.Feild_Name__c = 'Error_Description__c';
        insert setting6;
        
        IFC_CSVFieldMapping__c setting7 = new IFC_CSVFieldMapping__c();
        setting7.Name = 'Amount';
        setting7.Feild_Name__c = 'Amount__c';
        insert setting7;
        
        IFC_ErrorEmail__c setting8 = new IFC_ErrorEmail__c();
        setting8.Name = 'Error Email Id';
        setting8.Value__c = 'sd@sd.com';
        insert setting8;
        
        Config_Settings__c setting9 = new Config_Settings__c();
        setting9.Name = 'EMAIL_TO_CASE_USER_ID';
        setting9.Text_Value__c = '005b0000001DnKM';
        insert setting9;
        
        Team__c setting10 = new Team__c();
        setting10.Name = 'Branch - CCLA Aberdeen';
        setting10.Contact_Centre__c = 'Branch ABERDEEN';
        setting10.Queue_Name__c = 'Branch ABERDEEN';
        setting10.Task_Assignee__c = 'to.be.assigned@johnlewis.com';
        insert setting10;
        
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        string csvString = 'IFC Department,Branch Number,Financial Reporting Period,Type of Entry Outstanding,Date Processed,Till number - Transaction or Provider,Customer Name,Loan Ref,Post Code / Ref,Cash Management Reference,Amount,Error Description Electronics,\n,1,PD08-20,Branch Loan,04/09/2019,B020995855,Churchill,1234,E6 1AR,Furniture,675,Missing sale through the till Electrical,\n,2,PD08-20,CC Loan,21/09/2019,B310890368,Clark-Wilson,1234,SE16 7GA,Furniture,3052.66,Missing sale through the till';
        attachment.body = blob.valueOf(csvString);
        attachment.fileName = 'test.csv';
        attachment.mimeTypeSubType = 'text/csv';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] { attachment };
            
        email.subject = 'IFC Queries Sheet';
        email.plainTextBody = 'Please see attached CSV';
        env.fromAddress = 'Test@Test.com';
        IFC_EmaiServiceToCreateCase EmailServiceObj = new IFC_EmaiServiceToCreateCase();
        EmailServiceObj.handleInboundEmail(email, env ); 
    }
}