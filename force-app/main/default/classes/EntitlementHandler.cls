public class EntitlementHandler {

    public static Boolean FIRST_EXECUTION = true;

    public static Set<Id> casesForRecalculating_first = new Set<Id>(); // Set of Cases which will be re-processed with future call
    public static Set<Id> casesForRecalculating_second = new Set<Id>();// Set of Cases which will be re-processed with future call
    public static final Integer NO_OF_MINUTES_TO_MINUS = -1;


    /**
    * @description  Main entry for processing Cases in from Trigger.Before          
    * @author       @tomcarman
    * @param        oldCaseMap      Map of Cases from Trigger.oldMap
    * @param        newCases        List of Cases from Trigger.new
    */

    public static void processIncomingCasesBefore(Map<Id, Case> oldCaseMap, List<Case> newCases) {

        if(Trigger.isInsert || Trigger.isUpdate) {
            Map<Id,Case> caseMap = NEW Map<Id,Case>();
            for(Case cas : newCases) {
                //skipped for google sheet case processing
                if(cas.Origin!='Google Sheet' && cas.Origin!='IFC Queries Sheet' && cas.Origin!='MP3 Exceptions' && cas.Origin!='MP3 Queries' && cas.Origin!='Incorrect Services' && cas.Origin!='7 Day Report Complaints' && cas.Origin!='All Directorate Exceptions' && cas.Origin!='Backorder Exceptions') {
                    caseMap.put(cas.Id,cas);
                }
            }
            if(caseMap.values().size()>0) {
                EntitlementHandlerSupport.getReleatedContacts(newCases);
            }
            setEntitlement(oldCaseMap, newCases);

            setNextActionDateEntryTime(oldCaseMap, newCases);
        }

        if(Trigger.isUpdate) {
            validateNextActionDateOnCaseWhenApplyingEntitlement(oldCaseMap, newCases);
        }

    }


    /**
    * @description  Main entry for processing Cases in from Trigger.After           
    * @author       @tomcarman
    * @param        oldCaseMap      Map of Cases from Trigger.oldMap
    * @param        newCases        List of Cases from Trigger.new
    */

    public static void processIncomingCasesAfter(Map<Id, Case> oldCaseMap, List<Case> newCases) {

        if(Trigger.isInsert || Trigger.isUpdate) {

            EntitlementHandlerSupport.filterCasesWhichNeedToHaveMilestonsRecalculated(oldCaseMap, newCases);
            
            if(FIRST_EXECUTION) {
                if(clsCaseTriggerHandler.CASE_OLDER_THAN_30_DAYS_REOPENED) {
                    MilestoneHandler.setCasePriorityAfterExecutionNonFuture(casesForRecalculating_first);
                }
                else {
                MilestoneHandler.setCasePriorityAfterExecution(casesForRecalculating_first);
                }
            } else {
                if(clsCaseTriggerHandler.CASE_OLDER_THAN_30_DAYS_REOPENED) {
                    MilestoneHandler.setCasePriorityAfterExecutionNonFuture(casesForRecalculating_second);
                }
                else {
                MilestoneHandler.setCasePriorityAfterExecution(casesForRecalculating_second);
                }
            }
        }

        if(Trigger.isUpdate) {

            // update only
            identifyMilestonesToBeClosed(oldCaseMap, newCases);
            identifyMilestonesToBeReopened(oldCaseMap, newCases);
            MilestoneHandler.commitAllMilestones(); 
        }

        FIRST_EXECUTION = false;
    }

    


    /*** BUSINESS LOGIC ***/

    /**
    * @description  Business logic for assigning an Entitlement to a Case           
    * @author       @tomcarman
    * @param        oldCaseMap   Map of trigger.old Cases (optional)
    * @param        newCases  List of trigger.new Cases 
    */

    private static void setEntitlement(Map<Id, Case> oldCases, List<Case> newCases) {

        if(EntitlementHandlerSupport.entitlementsDisabled()) { // If Entitlements are disabled for the org, exit
            return;
        }

        List<Case> filteredCases = EntitlementHandlerSupport.filterCasesWhichNeedEntitlementEvaluated(oldCases, newCases);

        Map<Id, Team__c> ownerToTeamMap = EntitlementHandlerSupport.getOwnerToTeamMap(newCases); // Map of OwnerId (User or Queue) to the Team__c object

        for(Case newCase : filteredCases) {

            if(ownerToTeamMap.containsKey(newCase.OwnerId) && ownerToTeamMap.get(newCase.OwnerId) != null) { // Null check because sometimes TeamUtils populates map with key but null value...
                String teamName = ownerToTeamMap.get(newCase.OwnerId).Name;
                Entitlement entitlement;
                entitlement = EntitlementHandlerSupport.getEntitlement(teamName);
                // If Entitlement/BusinessHours were found, set its Id on Case, otherwise null the value (remove the Entitlement/BusinessHours from the Case)
                newCase.EntitlementId = (entitlement != null) ? entitlement.Id : null;
                
               	system.debug('Saurabh Yadav----->'+newCase.EntitlementId);
                newCase.BusinessHoursId = (entitlement != null && entitlement.BusinessHoursId != null) ? entitlement.BusinessHoursId : null;
                system.debug('Saurabh Yadav-->'+newCase.BusinessHoursId);
            }
        }

    }



    /**
    * @description  Business logic to set or re-evaluate the time which a Case has entered a NAD Milestone          
    * @author       @tomcarman
    * @param        oldCaseMap   Map of trigger.old Cases (optional)
    * @param        newCases  List of trigger.new Cases 
    * @return       
    */

    private static void setNextActionDateEntryTime(Map<Id, Case> oldCaseMap, List<Case> newCases) {

        if(oldCaseMap == null) { // This is an Insert. 

            for(Case newCase : newCases) {
                // Rule 1: If a NAD has been set and its in the future, then the NAD Entry time should be set
                if(newCase.JL_Next_Response_By__c != null && newCase.JL_Next_Response_By__c > Datetime.now()) { 
                    newCase.Next_Action_Date_Entry__c = Datetime.now();
                }
            }

        } else {

            for(Case newCase : newCases) { // For each Case, get its old value
                if(oldCaseMap.get(newCase.Id) != null) {
                    Case oldCase = oldCaseMap.get(newCase.Id);

                    // Rule 2: If the NAD has been changed, and its in the future, re-evaluate the NAD Entry time to now
                    if(oldCase.JL_Next_Response_By__c != newCase.JL_Next_Response_By__c && newCase.JL_Next_Response_By__c > Datetime.now()) {
                        newCase.Next_Action_Date_Entry__c = Datetime.now();

                    // Rule 3: If the Case has had an Entitlement applied, and the NAD is set
                        // Note: Only if Entitlement has gone from null-to-populated. It is assumed that all Entitlements will have the NAD 
                        // milestone, and therefore switching Entitlement would not require the NAD entry to be re-evaluated    
                    } else if (oldCase.EntitlementId == null && newCase.EntitlementId != null && newCase.jl_Next_Response_By__c != null && newCase.jl_Next_Response_By__c > Datetime.now()) {
                        newCase.Next_Action_Date_Entry__c = Datetime.now();
                    }
                    if(newCase.Status.contains('Closed') && !oldCase.Status.contains('Closed')){
                        newCase.Next_Case_Activity_Due_Date_Time__c = null;
                    }
                }
            }
        }
    }




    /**
    * @description  Method to handle Cases which are being transferred and have a NAD that is in the past           
    * @author       @tomcarman
    * @param        oldCaseMap  Map of trigger.old Cases
    * @param        newCases    List of trigger.new Cases       
    */

    private static void validateNextActionDateOnCaseWhenApplyingEntitlement(Map<Id, Case> oldValues, List<Case> newCases) {

        for(Case newCase : newCases) {

            Case oldCase = oldValues.get(newCase.Id);
            // 17.8 release temp solution for inflight cases.
            //If inflight cases (for which NAD entry time is not set) which  are getting transferred between teams.
            //this is to handle a scenario for cases whose owner is in a team and OMNI not configured.We have not done a dummy update for those cases.
            //So when these cases get transferred between teams we set a NAD which will expire immediately 
            boolean isEntitlementNewlyAppliedOnCase =  (oldCase.EntitlementId == null && newCase.EntitlementId != null);
            boolean isNCAExpired = (newCase.Next_Case_Activity_Due_Date_Time__c != null && (newCase.Next_Case_Activity_Due_Date_Time__c < Datetime.now()));
            if(isEntitlementNewlyAppliedOnCase && isNCAExpired ) { 
                // Rule 1: If its a Bulk case transfer, create a 1 minute NAD which will then expire and correctly allocate the Case to the required Queue (hack to allow users to bulk transfer)
                if(EntitlementHandlerSupport.isBulkTransfer(oldCase, newCase) || EntitlementConstants.USER_HAS_IN_FLIGHT_CASES_UPDATE_PERMISSION_SET) {
                    newCase.Next_Case_Activity_Due_Date_Time__c = newCase.Next_Case_Activity_Due_Date_Time__c.addMinutes(NO_OF_MINUTES_TO_MINUS);
                // Rule 2: If its a regular transfer, block the transaction until user sets a NAD in future.
                } else {
                    newCase.addError('You cannot transfer a Case with a Case Activity in the past');
                }
            }  
        }
    }        



    /**
    * @description  Business logic to close Milestones          
    * @author       @tomcarman
    * @param        oldCaseMap  Map of Cases from Trigger.oldMap
    * @param        newCases    List of Cases from Trigger.new 
    */

     private static void identifyMilestonesToBeClosed(Map<Id, Case> oldCaseMap, List<Case> newCases) {

        Set<Id> firstContactMilestonesToBeCompletedSet = new   Set<Id>();
        Set<Id> nextActionDateMilestonesToBeCompleted = new Set<Id>();
        Set<Id> reopenMilestonesToBeCompleted = new Set<Id>();
        Set<Id> emailMilestonesToBeCompleted = new Set<Id>();
        Set<Id> nextCaseActivityMilestonesToBeCompleted = new Set<Id>();

        Set<Id> allClosedCases = new Set<Id>();

        for(Case newCase : newCases) {

            Case oldCase = oldCaseMap.get(newCase.Id);

            /* First Contact Milestone */

            // Rule 1: If Customer has been contacted, or Agent has said they don't need contacting, close the First Contact Milestone
            if(EntitlementHandlerSupport.customerHasHadContact(newCase)) {
                firstContactMilestonesToBeCompletedSet.add(newCase.Id);
            }

            /* NAD Milestone */

            // Rule 2: If a new NAD has been set, close any existing NAD Milestones. Cache it for later (TODO: Can't remeber why)
            if(EntitlementHandlerSupport.newNextActionDateHasBeenSet(oldCase, newCase)) {
                nextActionDateMilestonesToBeCompleted.add(newCase.Id);
                EntitlementHandlerSupport.casesWithClosedNADInThisExecution.add(newCase.Id);

            }

            /* Re-Open Milestone */

            // NOTE: This is not 'Reopening' milestones, this is to CLOSE the 'Case Reopened' Milestone Type
            // Rule 3: If a Case has been reopened, and now the Customer has been contacted, close the ReOpen milestone.
            if(EntitlementHandlerSupport.customerHasHadContact(newCase) && newCase.JL_ReOpened__c) {
                reopenMilestonesToBeCompleted.add(newCase.Id);
            }

            /* Email Milestone */
            if(oldCase.Inbound_Emails_Not_Handled__c == true && newCase.Inbound_Emails_Not_Handled__c == false) {
                emailMilestonesToBeCompleted.add(newCase.Id);
            }

            /* Next Case Activity Milestone */

            // Rule 4: TODO: Enter criteria for closing down Next  milestone
            if(oldCase.Next_Case_Activity_Due_Date_Time__c != null && newCase.Next_Case_Activity_Due_Date_Time__c!= null && (oldCase.Next_Case_Activity_Due_Date_Time__c != newCase.Next_Case_Activity_Due_Date_Time__c))
            {
                nextCaseActivityMilestonesToBeCompleted.add(newCase.Id);    
            }

            /* Closed Case */

            // Rule 5: Close all open Milestones when a Case is closed
            if(!oldCase.IsClosed && newCase.Status.contains('Closed')) { // If the Case has been closed in this execution
                allClosedCases.add(newCase.Id);
            }

        }
       
        Map<String, Set<Id>> milestonesToBeCompleted = new Map<String, Set<Id>>();

        if(!firstContactMilestonesToBeCompletedSet.isEmpty()) {
            milestonesToBeCompleted.put(MilestoneHandlerSupport.FIRST_CONTACT_MILESTONE, firstContactMilestonesToBeCompletedSet);
        }

        if(!nextActionDateMilestonesToBeCompleted.isEmpty()) {
            milestonesToBeCompleted.put(MilestoneHandlerSupport.NEXT_ACTION_DATE, nextActionDateMilestonesToBeCompleted);
        }

        if(!reopenMilestonesToBeCompleted.isEmpty()) {
            milestonesToBeCompleted.put(MilestoneHandlerSupport.REOPEN_MILESTONE, reopenMilestonesToBeCompleted);
        }

        if(!emailMilestonesToBeCompleted.isEmpty()) {
            milestonesToBeCompleted.put(MilestoneHandlerSupport.EMAIL_CONTACT, emailMilestonesToBeCompleted);
        }

        if(!nextCaseActivityMilestonesToBeCompleted.isEmpty()) {
            milestonesToBeCompleted.put(MilestoneHandlerSupport.NEXT_CASE_ACTIVITY, nextCaseActivityMilestonesToBeCompleted);
            /// New Milestone which change owner to failed queue should also be closed.
            milestonesToBeCompleted.put(MilestoneHandlerSupport.MILESTONE_VIOLATION, nextCaseActivityMilestonesToBeCompleted);
            // Warning also closed
            milestonesToBeCompleted.put(MilestoneHandlerSupport.MILESTONE_WARNING , nextCaseActivityMilestonesToBeCompleted);
        }
        // Store Milestones which need to be completed
        if(milestonesToBeCompleted.size() > 0) {
            MilestoneHandler.closeMilestoneByType(milestonesToBeCompleted);
        }

        // Store Milestones which need to be closed because the Case is Closed
        if(!allClosedCases.isEmpty()){
            MilestoneHandler.closeAllMilestones(allClosedCases);
        }

    }



    /**
    * @description  Business logic to handle the Re-Opening of Milestones
    * @author       @tomcarman
    * @param        oldCaseMap  Map of Cases from Trigger.oldMap
    * @param        newCases    List of Cases from Trigger.new
    **/

    private static void identifyMilestonesToBeReopened(Map<Id, Case> oldCaseMap, List<Case> newCases) {

        Set<Id> reOpenedCaseIds = new Set<Id>();

        for(Case newCase: newCases) {

            Case oldCase = oldCaseMap.get(newCase.Id);

            if(oldCase.IsClosed && !newCase.Status.Contains('Closed')) {
                reOpenedCaseIds.add(newCase.Id);
            }
        }

        // Store Milestones which need to be ReOpened because the Case has been reopened
        if(!reOpenedCaseIds.isEmpty()){
            MilestoneHandler.reOpenMilestones(reOpenedCaseIds);
        }

    }


}