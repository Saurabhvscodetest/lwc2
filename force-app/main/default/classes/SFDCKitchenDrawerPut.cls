/**
 * create/update type services
 */
global with sharing class SFDCKitchenDrawerPut {
    /**
     * create transactions using provided data
     */
    webservice static SFDCKitchenDrawerTypes.ActionResponse createTransaction(final SFDCKitchenDrawerTypes.RequestHeader requestHeader, 
                                                                                final SFDCKitchenDrawerTypes.KDTransaction[] kdTransactions) {
        
		if (!SFDCKitchenDrawerGet.isEnabled()) return SFDCKitchenDrawerGet.getInactiveServiceResponse(requestHeader);
        /*
        //dummy response, just to see what it looks like in resulting SOAP message in SOAPUI
        SFDCKitchenDrawerTypes.ActionResponse response = new SFDCKitchenDrawerTypes.ActionResponse();
        response.results = new List<SFDCKitchenDrawerTypes.ActionResult>();
        SFDCKitchenDrawerTypes.ActionResult result1 = new SFDCKitchenDrawerTypes.ActionResult();

        result1.kdTransaction = new SFDCKitchenDrawerTypes.KDTransaction();
        SFDCKitchenDrawerTypes.ActionResult result2 = new SFDCKitchenDrawerTypes.ActionResult();
        result2.kdTransaction = new SFDCKitchenDrawerTypes.KDTransaction();
        
        response.results.add(result1);
        response.results.add(result2);

        return response;
        */
        return MyJLKD_CreateHandler.process(requestHeader, kdTransactions); 
    }
}