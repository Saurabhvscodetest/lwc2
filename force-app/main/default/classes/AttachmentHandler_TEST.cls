@isTest
public class AttachmentHandler_TEST{
    
	@testSetup static void setup() {
         UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
    }
    
    @IsTest
    static void emailAttachmentVisibleOnParentTest() {

        User runningUser = UserTestDataFactory.getRunningUser();
        
        User contactCentreUser;
        
        Contact testContact;
        Case testCase;
        
        System.runAs(runningUser) {

            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;

            testContact = CustomerTestDataFactory.createContact();
            insert testContact;               
        }

        System.runAs(contactCentreUser) { 
            
            testCase = CaseTestDataFactory.createQueryCase(testContact.Id);
            insert testCase;
			Test.startTest();
            EmailMessage email = new EmailMessage();
        
            email.parentid = testCase.id;
            email.TextBody = 'Body of email';
            insert email;
        
            Attachment a = new Attachment();
            a.name = 'test attachment';
            a.body = blob.valueof('attachment body');
            a.parentid = email.id;            
            
            insert a;  

            List<ContentDocumentLink> documents = [SELECT ContentDocumentId, LinkedEntityId  FROM ContentDocumentLink where LinkedEntityId = :testCase.id ];
            System.assertEquals( 1, documents.size() );
            Test.stopTest();
        }    
    }
}