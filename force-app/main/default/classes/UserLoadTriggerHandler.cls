public with sharing class UserLoadTriggerHandler {

	// a global to ensure batches are only kicked once

    public static void mainEntry(Boolean isBefore, 
                               Boolean isDelete, Boolean isAfter, 
                               Boolean isInsert, Boolean isUpdate, 
                               Boolean isExecuting, List<Load_User__c> newlist, 
                               Map<ID,Sobject> newmap, List<Load_User__c> oldlist, 
                               Map<Id,Sobject> oldmap)
    {
        System.debug('@@entering UserLoadTriggerHandler');

		boolean RUN_BATCH = false;

        if (isBefore && (isInsert || isUpdate))
        {

        	for (Load_User__c lu : newlist)
        	{
				if (!lu.done__c && !lu.has_error__c)
				{
					RUN_BATCH = true;
				}
        		
        		// Selling Assistant is a synonym for CustomerSupport
        		if (lu.function__c == 'Selling Assistant')
        		{
        			lu.function__c = 'CustomerSupport';
        		}
        		
        		String strTier = UserLoad.calculateTier(lu);
        		if (strTier != null)
        		{
        			lu.Tier__c = strTier;
        		}
        		
        		String strProfile = UserLoad.calculateProfile(lu);
        		if (strProfile != null)
        		{
        			lu.profile__c = strProfile;
        		}

        		String strRole = UserLoad.calculateRole(lu);
        		if (strRole != null)
        		{
        			lu.Role__c = strRole;
        		}

        		String strPrimaryTeam = UserLoad.calculatePrimaryTeam(lu);
        		if (strPrimaryTeam != null)
        		{
        			lu.Primary_Team__c = strPrimaryTeam;
        		}

        		String strJLLicenseType = UserLoad.getJLLicenseType(strRole, lu.contract__c);
        		if (strJLLicenseType != null)
        		{
        			lu.jl_license_type__c = strJLLicenseType;
        		}
        		
        	}

	        // this will need to change when Ping goes live
	        if (!Test.isRunningTest())
	        {
		        if (RUN_BATCH)
		        {
		        	Database.executeBatch(new UserLoadBatch(), 1);
		        }
	        }
        	
        }
        
	    System.debug('@@exiting UserLoadTriggerHandler');
        
    }
        


}