/* Description  : Delete Contact Profile Records one time.
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   21/12/2018        Vignesh Kotha                   Created                 COPT-4271
*
*/
global class GDPR_BatchDeleteContactProfiles implements Database.Batchable<sObject>,Database.Stateful {
   global Map<Id,String> ErrorMap = new Map<Id,String>();
   global integer result=0;
   global String query;
   global Database.QueryLocator start(Database.BatchableContext BC){
   GDPR_Record_Limit__c gDPRLimit = GDPR_Record_Limit__c.getValues('GDPR_BatchDeleteContactProfiles');
    if(gDPRLimit.Record_Limit__c != NULL){
	String recordLimit = gDPRLimit.Record_Limit__c;
    String limitSpace = ' ' + 'Limit' + ' '; 
	query = 'SELECT Id FROM Contact_Profile__c' + limitSpace + recordLimit;
        }
        else{
            query = 'SELECT Id FROM Contact_Profile__c';
        }
        return Database.getQueryLocator(query);
   }
   global void execute(Database.BatchableContext BC, list<Contact_Profile__c> scope){
       List<id> recordstoDelete = new List<id>();
       if(scope.size()>0){
           try{
               for(Contact_Profile__c recordScope : scope){
                   recordstoDelete.add(recordScope.id);
               }
               list<Database.deleteResult> srList = Database.delete(recordstoDelete, false);
               for(Integer counter = 0; counter < srList.size(); counter++) {
                   if (srList[counter].isSuccess()) {
                       result++;
                   }else {
                       for(Database.Error err : srList[counter].getErrors()) {
                           ErrorMap.put(srList.get(counter).id,err.getMessage());
                       }
                   }
               }
               /* Delete records from Recyclebin */
               Database.emptyRecycleBin(scope);
           }Catch(exception e){
               EmailNotifier.sendNotificationEmail('Exception from GDPR_BatchDeleteContactProfiles ', e.getMessage());
           }
       }
   }
   global void finish(Database.BatchableContext BC) {
       String textBody ='';
       set<Id> allIds = new set<Id>();
      textBody+= result +' records deleted in object Contact Profile'+'\n';
       if (!ErrorMap.isEmpty()){
           for (Id recordids : ErrorMap.KeySet())
           {
               allIds.add(recordids);
           } 
           textBody+= 'Error log: '+allIds+'\n';
       }
       EmailNotifier.sendNotificationEmail('GDPR Delete Contact Profile Records', textBody);
   }
   
}