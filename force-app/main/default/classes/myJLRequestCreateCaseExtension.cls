/**
* Author:       Nawaz Ahmed
* Date:         13/08/2015
* Description:  Controller for myJL_MembershipDetails.page, added to detail page of contact layouts to view
loyalty account and card details
*/
public with sharing class myJLRequestCreateCaseExtension {
    
    public string address1 {get;set;}
    public string address2 {get;set;}
    public string address3 {get;set;}
    public string address4 {get;set;}
    public string cname {get;set;}
    public string ccountry {get;set;}
    public string ccounty {get;set;}
    public string pcode {get;set;}
    public string membershipnumber {get;set;}
    public string cardnumber {get;set;}
    public string contid {get;set;}
    public string loyid {get;set;}
    public string mainid {get;set;}
    public string conname {get;set;}
    public string conemail {get;set;}
    public String selectedRequest {get; set;}
    
    public Case cs;
    ApexPages.StandardController controller;
    
    public myJLRequestCreateCaseExtension(ApexPages.StandardController con)
    {
        controller = con;
        this.cs = (Case) con.getRecord();
        address1 = ApexPages.currentPage().getParameters().get(myJL_InformationPageController.ADDRESS_LINE_1);
        address2 = ApexPages.currentPage().getParameters().get(myJL_InformationPageController.ADDRESS_LINE_2);
        address3 = ApexPages.currentPage().getParameters().get(myJL_InformationPageController.ADDRESS_LINE_3);
        address4 = ApexPages.currentPage().getParameters().get(myJL_InformationPageController.ADDRESS_LINE_4);
        cname = ApexPages.currentPage().getParameters().get('cname');
        ccountry = ApexPages.currentPage().getParameters().get('ccountry');
        ccounty = ApexPages.currentPage().getParameters().get('ccounty');
        pcode = ApexPages.currentPage().getParameters().get('postcode');
        membershipnumber = ApexPages.currentPage().getParameters().get('jlMemNumber');
        cardnumber = ApexPages.currentPage().getParameters().get('jlCarNumber');
        contid= ApexPages.currentPage().getParameters().get('conId');
        loyid= ApexPages.currentPage().getParameters().get('laid');
        mainid= ApexPages.currentPage().getParameters().get('sourcecontactid');
        conname = ApexPages.currentPage().getParameters().get('sourcecontactname');
        conemail = ApexPages.currentPage().getParameters().get('sourcecontactemail');
        conemail = ApexPages.currentPage().getParameters().get('sourcecontactemail');
    }
    
    public boolean isPartnershipMember {
        get {
            List<Loyalty_Account__c> loyaltyAccounts = [SELECT Number_Of_Partnership_Cards__c 
                                                        FROM Loyalty_Account__c 
                                                        WHERE Id = :loyid AND Number_Of_Partnership_Cards__c > 0 LIMIT 1 ];
            return loyaltyAccounts.size() > 0 ? true : false;
        }
        set{}
    }
    
    public List<SelectOption> getRequestOptions() {
        List<SelectOption> requests = new List<SelectOption>();
        requests.add(new selectOption('','--None--'));
        if(isPartnershipMember){
            requests.add(new selectOption('Pan Partnership card','Pan Partnership card'));
            requests.add(new selectOption('Replacement vouchers','Replacement vouchers'));
        }
        else{
            requests.add(new selectOption('Replacement card','Replacement card'));
            requests.add(new selectOption('Replacement vouchers','Replacement vouchers'));
            requests.add(new selectOption('Replacement welcome pack','Replacement welcome pack'));   
        }
        return requests;
    }
    
    //Method to overwrite the standard save method
    public void Save()
    {
        Contact updateContact = [SELECT Id, Email FROM Contact WHERE Id = :mainid LIMIT 1];
        
        //Update email from contact profile
        if(updatecontact.email == null){
            updatecontact.email =  conemail;
            update updatecontact;
        }
        
        //Create Case
        id myjlrecordtypeid = [select id from recordtype where developername ='myJL_Request' and SobjectType ='Case' limit 1].id;
        this.cs.address_line_1__c =address1;
        this.cs.address_line_2__c =address2;
        this.cs.address_line_3__c =address3;
        this.cs.address_line_4__c =address4;
        this.cs.Mailing_City__c =cname;
        this.cs.Mailing_Country__c =ccountry;
        this.cs.Mailing_County__c =ccounty;
        this.cs.Post_Code__c =pcode;
        this.cs.myJL_Membership_Number__c =membershipnumber;
        this.cs.MYJL_Barcode_Number__c =cardnumber;
        this.cs.recordtypeid = myjlrecordtypeid;
        this.cs.status ='Closed - No Response Required';
        this.cs.Contactid = mainid;
        this.cs.Despatch_Status__c = 'Processing';
        this.cs.description = 'mY JL Request';
        if(!Test.isRunningTest()){
            this.cs.Request_Type__c = selectedRequest ;
        }
        
        controller.Save();
        
        //Retreive the Case id
        Id pageid = controller.getId();
        
        //Insert date to Outbound Staging table for Abinitio to Pick up
        
        //Get the case details
        Case caseforStaging = [select id,owner.firstname,Integration_Ref_Holder__c,MyJL_Request_Record_Locked__c,owner.lastname,jl_Authorised_Person_Comments__c,
                               Mailing_City__c,Mailing_Country__c,Mailing_County__c,fast_track__c,Request_Type__c from case where id =:pageid];
        
        //Get contact profile, loyalty account and card details
        Contact  cpforStaging = [select Id, Name, Shopper_ID__c, Customer_Group_Name__c, birthdate,
                                 firstname, lastname, salutation, SourceSystem__c, 
                                 mailingstreet, otherstreet, 
                                 mailingcity, othercity, mailingstate, otherstate,
                                 mailingcountry, othercountry, mailingpostalcode, otherpostalcode,
                                 title, mobilephone, email, AnonymousFlag2__c, 
                                 Mailing_Street__c, Mailing_Address_Line2__c, Mailing_Address_Line3__c, Mailing_Address_Line4__c
                                 from contact where id =:mainid];
        
        Loyalty_Account__c laforstaging =  [select Id, contact__c, Name,
                                            Activation_Date_Time__c, Channel_ID__c, Customer_Loyalty_Account_Card_ID__c,
                                            Deactivation_Date_Time__c, Email_Address__c, IsActive__c, LastModifiedById,
                                            LastModifiedDate, LastModifiedDateMS__c, Registration_Business_Unit_ID__c, Registration_Email_Sent_Flag__c,
                                            Registration_Workstation_ID__c, Scheme_Joined_Date_Time__c, Welcome_Email_Sent_Flag__c
                                            from Loyalty_Account__c where Id = :loyid];
        
        String title ='';
        string initials = '';
        
        if(cpforStaging.title!=null){
            title = cpforStaging.title;
        }
        
        if(cpforStaging.salutation!=null){
            title = cpforStaging.salutation;
        }
        
        if(cpforStaging.firstname!=null){
            initials = cpforStaging.firstname.substring(0,1).touppercase();
        }                                                           
        
        //insert record in staging table for Abinitio to pick up                                       
        MyJL_Outbound_Staging__c insertMyjstage = new MyJL_Outbound_Staging__c();
        insertMyjstage.Activation_Date_Time__c = laforstaging.Activation_Date_Time__c;
        insertMyjstage.Address_LIne_1__c = cpforStaging.Mailing_Street__c;
        insertMyjstage.Address_Line_2__c = cpforStaging.Mailing_Address_Line2__c;
        insertMyjstage.Address_Line_3__c = cpforStaging.Mailing_Address_Line3__c;
        insertMyjstage.Address_Line_4__c = cpforStaging.Mailing_Address_Line4__c;
        insertMyjstage.Agent_Id__c = caseforStaging.owner.firstname +' '+caseforStaging.owner.lastname;
        insertMyjstage.Case_Reference_ID__c = pageid;
        insertMyjstage.Case_Request_Cancelled__c = false;
        insertMyjstage.Change__c = '';
        insertMyjstage.Comments__c = caseforStaging.jl_Authorised_Person_Comments__c;
        insertMyjstage.County__c = cpforStaging.mailingstate;
        insertMyjstage.Created_Date_Holder__c = datetime.now().format('YYYY-MM-dd HH:mm:ss.SSS','UTC')+'Z';
        insertMyjstage.Deactivation_Date_Time__c = laforstaging.Deactivation_Date_Time__c;
        insertMyjstage.Email_Address__c = cpforStaging.email;
        insertMyjstage.Fast_track__c = caseforStaging.fast_track__c;
        insertMyjstage.First_Name__c = cpforStaging.firstname;
        insertMyjstage.Initials__c = initials;
        insertMyjstage.Is_Active__c = laforstaging.IsActive__c;
        insertMyjstage.Last_Modified_Date_Holder__c = insertMyjstage.Created_Date_Holder__c;
        insertMyjstage.Last_Name__c = cpforStaging.lastname;
        insertMyjstage.MYJL_Barcode_NUmber__c = laforstaging.Customer_Loyalty_Account_Card_ID__c;
        insertMyjstage.MYJL_Membership_Number__c = laforstaging.name;
        insertMyjstage.Post_Code__c = cpforStaging.mailingpostalcode;
        insertMyjstage.Request_Type__c = selectedRequest;
        insertMyjstage.Scheme_Joined_Date_Time__c = laforstaging.Scheme_Joined_Date_Time__c;
        insertMyjstage.Shopper_ID__c = cpforStaging.Shopper_ID__c;
        insertMyjstage.Title__c = title;
        insertMyjstage.Town__c = cpforStaging.mailingcity;
        
        Insert insertMyjstage;
        
        //Update the case with outbound staging reference
        caseforStaging.Integration_Ref_Holder__c = insertMyjstage.id;
        caseforStaging.MyJL_Request_Record_Locked__c = true;
        update caseforStaging;
        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Request have been raised successfully!'));

    }
    
    
    
    //Method to go back to the initial page
    public pagereference cancel(){
        pagereference pagecancel = new pagereference('/apex/myJL_Information');
        pagecancel.getParameters().put('id',mainid);
        pagecancel.getParameters().put('laid',loyid);
        pagecancel.getParameters().put('conId',contid);
        pagecancel.setredirect(true);
        return pagecancel;
    }
    
}