public class CloseCaseLExController {
	
	@AuraEnabled
	public static Case getCaseDetails(String caseId){
		// Changes for COPT-5408 : CreatedDate added to SOQL
        return [SELECT RecordType.Name, RecordTypeId, Status, CreatedDate FROM Case WHERE Id = :caseId LIMIT 1][0];
	}

	@AuraEnabled
	public static Boolean isPermSetAssignedToUser(String permSetName){
		User currentUser = CommonStaticUtils.getCurrentUser();
		return CommonStaticUtils.isPermissionSetAssignedToSingleUserByName(currentUser.Id, permSetName);
	}
    
    @AuraEnabled
	public static void closeselectedCaseActivities(String caIdListStr, String resolutionMethod){
		//Read case activity selected and create case activity list
		system.debug('ccc caIdListStr'+caIdListStr);
        system.debug('resolutionMethod'+resolutionMethod);
		List<Id> caIdList = (List<Id>) JSON.deserialize(caIdListStr, Type.forName('List<Id>'));
        system.debug('ccc caIdList'+caIdList);
        List<string> iaResolutionMethod = new List<String>();
        /*iaResolutionMethod.add('Carrier contacted');
        iaResolutionMethod.add('Internal team contacted');
        iaResolutionMethod.add('Supplier / Manufacturer contacted');
        iaResolutionMethod.add('Unable to complete action');*/
        List<Customer_Promise_Resolution_Dependency__mdt> CPRD_mdt = new List<Customer_Promise_Resolution_Dependency__mdt>();
        CPRD_mdt = [SELECT Id, Promise_Status__c, Record_Type_Name__c FROM Customer_Promise_Resolution_Dependency__mdt WHERE Record_Type_Name__c = 'Internal Action' AND Promise_Status__c != 'No action required' ORDER BY Promise_Status__c];
        for (Customer_Promise_Resolution_Dependency__mdt cprd : CPRD_mdt){           
            iaResolutionMethod.add(cprd.Promise_Status__c);
        }
        List<Case_Activity__c> caList = [select id, Customer_Promise_Type__c from  Case_Activity__c where id in :caIdList];
        /*List of resolution method for Internal action
        -------------------------------------------------
        Carrier contacted
        Internal team contacted        
        Supplier / Manufacturer contacted
        Unable to complete action
        No action required

        List of resolution method for customer promise 
        -------------------------------------------------
        Customer contacted
        Letter sent to customer
        Email sent to customer
        Unable to contact customer
        No action required
        Voicemail left with customer
        */        
        //List<Case_Activity__c> updatecalist = new List<Case_Activity__c>();
        for(Case_Activity__c ca : calist){           
            //calist.add(new Case_Activity__c(Id=caid,Resolution_Method__c=resolutionMethod,Activity_Completed_Date_Time__c=system.now()));
            ca.Activity_Completed_Date_Time__c = system.now();
            //if customer promise type is internal action and resolution method is not No action required and not in internal action list
            if (ca.Customer_Promise_Type__c == 'Internal Action' && !iaResolutionMethod.contains(resolutionMethod))
                ca.Resolution_Method__c = 'No action required';
            else
                ca.Resolution_Method__c = resolutionMethod;           
          
        }	
        system.debug('closeselectedCaseActivities >>>>>>'+ calist.size());
		//update case activity to close in try catch block       
		if(calist.size() > 0)
           update calist;       
		
    }
    
    @AuraEnabled
	public static List<CloseCaseLExController.WrapperCaseActivity> getRelatedWrapperCaseActivityDetails(String caseId){
		List<case_activity__c> caList =  [SELECT Id, Customer_Promise_Type__c, Activity_End_Date_Time__c, Activity_Start_Date_Time__c, Case__c, RecordTypeId, Activity_Completed_Date_Time__c FROM Case_Activity__c WHERE Case__c = :caseId AND Activity_Completed_Date_Time__c = NULL ORDER BY Activity_End_Date_Time__c];
        List<CloseCaseLExController.WrapperCaseActivity> wcaList = null;
        if (caList.size() >0 ){
            wcaList = new List<CloseCaseLExController.WrapperCaseActivity>();
            if(caList.size() == 1){
                // Always make caseactivity selected in UI 
                CloseCaseLExController.WrapperCaseActivity wca = new CloseCaseLExController.WrapperCaseActivity( caList[0].Id, caList[0].Customer_Promise_Type__c, caList[0].Activity_End_Date_Time__c, caList[0].Activity_Start_Date_Time__c, caList[0].RecordTypeId, caList[0].Activity_Completed_Date_Time__c, true );
                wcaList.add(wca);
                system.debug('11111'+wcaList);
            } else {
                //failed promise and ongoing promises(end date on the same date) must be checked by default
                //Date tdate = System.today();

                for(case_activity__c ca : caList){

                    date actenddate = ca.Activity_End_Date_Time__c.date();
                    if (actenddate > System.today()){
                        CloseCaseLExController.WrapperCaseActivity wca = new CloseCaseLExController.WrapperCaseActivity( ca.Id, ca.Customer_Promise_Type__c, ca.Activity_End_Date_Time__c, ca.Activity_Start_Date_Time__c, ca.RecordTypeId, ca.Activity_Completed_Date_Time__c, false );
                        wcaList.add(wca);

                    } else {
                        CloseCaseLExController.WrapperCaseActivity wca = new CloseCaseLExController.WrapperCaseActivity( ca.Id, ca.Customer_Promise_Type__c, ca.Activity_End_Date_Time__c, ca.Activity_Start_Date_Time__c, ca.RecordTypeId, ca.Activity_Completed_Date_Time__c, true );
                        wcaList.add(wca);

                    }
                }

            }

        }
        system.debug('2222'+wcaList);
        return wcaList;

        
	}

    public class WrapperCaseActivity{
        //@AuraEnabled public string Activity_Description_c;
        @AuraEnabled public Id Id;
        @AuraEnabled public string Customer_Promise_Type_c;
        @AuraEnabled public Datetime Activity_End_Date_Time_c;
        @AuraEnabled public Datetime Activity_Start_Date_Time_c;
        @AuraEnabled public Id RecordTypeId;
        @AuraEnabled public Datetime Activity_Completed_Date_Time_c;
        @AuraEnabled public Boolean selected;

        public WrapperCaseActivity( Id Id, string Customer_Promise_Type_c,Datetime Activity_End_Date_Time_c,Datetime Activity_Start_Date_Time_c,Id RecordTypeId,Datetime Activity_Completed_Date_Time_c, Boolean selected){
            //this.Activity_Description_c = Activity_Description_c;
            this.Id = Id;
            this.Customer_Promise_Type_c = Customer_Promise_Type_c;
            this.Activity_End_Date_Time_c = Activity_End_Date_Time_c;
            this.Activity_Start_Date_Time_c = Activity_Start_Date_Time_c;
            this.RecordTypeId = RecordTypeId;
            this.Activity_Completed_Date_Time_c = Activity_Completed_Date_Time_c;
            this.selected = selected;

        }


    }

	@AuraEnabled
	public static void closeOutstandingTasksWhenCaseClosed(String caseId){
		List<Case> currentCase = [SELECT Id, jl_NumberOutstandingTasks__c, RecordType.Name
							      FROM Case 
							      WHERE Id =: caseId AND jl_NumberOutstandingTasks__c > 0 LIMIT 1];

		if(currentCase.size()>0 && (currentCase[0].RecordType.Name == 'Complaint' || currentCase[0].RecordType.Name == 'Query'
			 || currentCase[0].RecordType.Name == 'Product Stock Enquiry' ) ){
			List<Task> outstandingTasks = [SELECT Id, Description
									       FROM Task 
									   	   WHERE IsClosed = FALSE AND WhatId = :caseId LIMIT 100];

			if(outstandingTasks.size() > 0){
				for(Task aTask: outstandingTasks){
					aTask.Status = 'Closed - Resolved';
					aTask.Description = 'Task closed as part of bulk closure';
				}
				System.debug('inside task closure');
				try{
					currentCase[0].jl_NumberOutstandingTasks__c = 0;
					update currentCase;

					update outstandingTasks;

				}catch(Exception ex){
						System.debug('An error has occured when closing outstandingTasks from action case component '+ex.getMessage());
				}
			}else{
				currentCase[0].jl_NumberOutstandingTasks__c = 0;
				try{
					update currentCase;
				}catch(Exception ex){
						System.debug('An error has occured when updating case from action case component '+ex.getMessage());
			}
		}
		}					      	
	}

	@AuraEnabled
	public static List<Case_Activity__c> getRelatedCaseActivityDetails(String caseId){
		return [SELECT Case__c, RecordTypeId, Activity_Completed_Date_Time__c FROM Case_Activity__c WHERE Case__c = :caseId AND Activity_Completed_Date_Time__c = NULL];
	}

	@AuraEnabled(cacheable=true)
	public static List<Customer_Promise_Resolution_Dependency__mdt> initiateCaseActivityResolutionMethodPicklist(Id recordTypeId){
		//Modified by Subramanian Jayaram for COPT-5571
        List<Customer_Promise_Resolution_Dependency__mdt> CPRD_mdt = new List<Customer_Promise_Resolution_Dependency__mdt>();
        if(String.isNotEmpty(recordTypeId)){ 
            String caseActivtyRecordTypeName = CommonStaticUtils.getRecordTypeName('Case_Activity__c', recordTypeId);            
            CPRD_mdt = [SELECT Id, Promise_Status__c, Record_Type_Name__c FROM Customer_Promise_Resolution_Dependency__mdt WHERE Record_Type_Name__c = :caseActivtyRecordTypeName ORDER BY Promise_Status__c];
        }    
            
        return CPRD_mdt; 
        //End COPT-5571
	}
    
    @AuraEnabled
    public static List<String> initiateLevelOnePicklist(){
        List<String> returnList = new List<String>();
        List<Schema.PicklistEntry> level_1_ple = Schema.getGlobalDescribe().get('Case').getDescribe().fields.getMap().get('jl_Primary_Category__c').getDescribe().getPicklistValues();
        for(Schema.PicklistEntry obj : level_1_ple){
            returnList.add((String)obj.getValue());
        }
        return returnList;
    }
    
    @AuraEnabled
    public static Map<String,Map<String,List<String>>> generateCaseCategorizationDependentFieldMap(){
        PicklistUtility pick = new PicklistUtility();
        Map<String,Map<String,List<String>>> mastermap = new Map<String,Map<String,List<String>>>();
        String objectName = 'Case', recordTypeDevName='Complaint';
        Map<String,String> fieldLabelMap = new Map<String,String>();
        fieldLabelMap = pick.getFieldLabelMap(objectName,pick.dependencyMetadataPerObjectMap.get(objectName));
        Map<String,List<String>> controllingAndDependentPicklistMap = new Map<String,List<String>>(); 
        controllingAndDependentPicklistMap = pick.getDependentFieldMetadataMap(objectName, pick.dependencyMetadataPerObjectMap.get(objectName), recordTypeDevName); //Refine the map to fetch only current record type related information
        
        Map<String,Map<String,List<String>>> dependentAndDependentValueMap = new Map<String,Map<String,List<String>>>(); //Dependent field API --> (Control value --> Dependent value List)
        
        for(String controllingField : controllingAndDependentPicklistMap.keySet()){
            if(controllingAndDependentPicklistMap.get(controllingField).size()>0){
                for(String dependentField : controllingAndDependentPicklistMap.get(controllingField)){
                    if(controllingField != 'Master Controller Picklist'){
                        dependentAndDependentValueMap.put(dependentField,pick.GetDependentOptions(objectName,controllingField,dependentField));
                    }
                }
            }
        }
        Map<String,String> fieldAPIMap = new Map<String,String>();
        fieldAPIMap.put('jl_AreaBusiness_pri__c','areaOfTheBusiness');
        fieldAPIMap.put('jl_Primary_Reason__c','primaryReason');
        fieldAPIMap.put('jl_Primary_Reason_Detail__c','primaryReasonDetail');
        fieldAPIMap.put('Buying_Directorate_PR__c','buyingDirectorate');
        fieldAPIMap.put('Product_Group_PR__c','productGroup');
        fieldAPIMap.put('jl_Primary_Detail_Explanation__c','primaryDetailExplanation');
        for(String keyOne : dependentAndDependentValueMap.keySet()){
            String parentKey = fieldAPIMap.get(keyOne);
            Map<String,List<String>> interimMap = new Map<String,List<String>>();
            for(String keyTwo : dependentAndDependentValueMap.get(keyOne).keySet()){
                for(String value : dependentAndDependentValueMap.get(keyOne).get(keyTwo)){
                    if(interimMap.containsKey(keyTwo)){
                        interimMap.get(keyTwo).add(value);
                    }
                    else{
                        interimMap.put(keyTwo, new List<String>{value});
                    }
                }
            }
            if(interimMap.values().size()>0){
                mastermap.put(parentKey,interimMap);
            }
        }
        return mastermap;
    }
}