/*
* @File Name   : MyJLChatBotUtils
* @Description : Utility class to related to MyJLChatBot related functionalities  
* @Copyright   : Zensar
* @Jira #      : #5066
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0       18-Sep-19         Ragesh G                     Created
*/

public with sharing class MyJLChatBotUtils {
    
    /**
    *@purpose : Method to fetch loyalty card records based on the account and card type field 
    *@param   : accountList  - Account List comes in 
    *@return  : Map < Id, Map < String, List < Loyalty_Card__c >  >>
    **/
    public static String myJLChatBotProcessInput(String loyaltyMembershipNumber,String shopperID){
        
        String JSONResponseString = '';
        
        MyJLChatBotResponseModel responseJSON = new MyJLChatBotResponseModel();
        String RequestType;
        
        if(loyaltyMembershipNumber.length() == 11){
            RequestType = 'Replacement card';
        }else{
          RequestType = 'Pan Partnership card';            
        }
        
        List < MyJL_Rejection__c > logRejectionRecordList = new List < MyJL_Rejection__c > ();
        // responseJSON.setResponseModel();
        
        try{                        
            if( String.isEmpty(loyaltyMembershipNumber)){
                responseJSON.setResponseModel( 400, MyJLChatBotConstants.MISSING_LOYALTY_NUMBER, MyJLChatBotConstants.LOYALTYNUMBER_NOT_PROVIDED );
                logRejectionRecordList.add ( logRejection(MyJLChatBotConstants.ERRORCODE.MY_LOYALTY_NUMBER_MISSING_DATA, '', '', MyJLChatBotConstants.SERVICE_NAME_FOR_ERROR) );
            } else if( String.isEmpty(shopperId)){
                responseJSON.setResponseModel( 400, MyJLChatBotConstants.MISSING_JL_SHOPPER_ID, MyJLChatBotConstants.SHOPPERID_NOT_PROVIDED );
                logRejectionRecordList.add ( logRejection(MyJLChatBotConstants.ERRORCODE.CUSTOMERID_NOT_FOUND, '', shopperId, MyJLChatBotConstants.SERVICE_NAME_FOR_ERROR) );
            } else {
                List< Loyalty_Account__c > loyaltyAccountList = MyLoyaltyServiceHelper.getLoyaltyAccountsWithShopperId( shopperId );
                if( loyaltyAccountList.isEmpty() ){
                    responseJSON.setResponseModel( 404, MyJLChatBotConstants.NO_LOYALTY_ACCOUNTS_FOUND, MyJLChatBotConstants.NO_LOYALTY_ACCOUNT_FOUND ); 
                    logRejectionRecordList.add ( logRejection(MyJLChatBotConstants.ERRORCODE.LOYALTY_ACCOUNT_NOT_FOUND, '', shopperId,MyJLChatBotConstants.SERVICE_NAME_FOR_ERROR) );
                }
                else{
                    Map < Id, Map < String, List < Loyalty_Card__c >  >> loyaltyCardListMap = fetchLoyaltyAccountCardMap ( loyaltyAccountList );
                    String cardType;
                    if( loyaltyMembershipNumber.length() == 11 ){
                        cardType =  MyJL_Const.MY_JL_CARD_TYPE ;
                    } else if ( loyaltyMembershipNumber.length() == 16 ) {
                        cardType = MyJL_Const.MY_PARTNERSHIP_CARD_TYPE ;
                    }
                    Contact  cpforStaging = [ SELECT Id, Name,Customer_Group_Name__c, birthdate,
                                             firstname, lastname, salutation, SourceSystem__c, 
                                             mailingstreet, otherstreet,Mailing_House_No_Text__c,Mailing_House_Name__c,
                                             mailingcity, othercity, mailingstate, otherstate,
                                             mailingcountry, othercountry, mailingpostalcode, otherpostalcode,
                                             title, mobilephone, email, AnonymousFlag2__c, 
                                             Mailing_Street__c, Mailing_Address_Line2__c, Mailing_Address_Line3__c, Mailing_Address_Line4__c, Shopper_ID__c
                                             FROM  contact 
                                             WHERE Shopper_ID__c =:shopperId];
                    
                    System.debug('@@@ case'+cpforStaging);
                    
                    //fetching LoyaltyAccount through Shopper ID
                    Loyalty_Account__c laforstaging =  [ SELECT Id, contact__c, Name,Activation_Date_Time__c, Channel_ID__c, Customer_Loyalty_Account_Card_ID__c,
                                                        Deactivation_Date_Time__c, Email_Address__c, IsActive__c, LastModifiedById,
                                                        LastModifiedDate, LastModifiedDateMS__c, Registration_Business_Unit_ID__c, Registration_Email_Sent_Flag__c,
                                                        Registration_Workstation_ID__c, Scheme_Joined_Date_Time__c, Welcome_Email_Sent_Flag__c
                                                        FROM   Loyalty_Account__c 
                                                        WHERE  ShopperId__c = :shopperId];
                    Id myjlrecordtypeid = [ SELECT Id 
                                           FROM recordtype 
                                           WHERE developername ='myJL_Request' AND SobjectType ='Case' LIMIT 1].id;
                    
                    List < Case > caseRecordList = new List < Case > ();
                    String title ='';
                    String initials = '';
                    System.debug (' loyaltyAccountList ' + loyaltyAccountList );
                    for( Loyalty_Account__c loyaltyAccRecord : loyaltyAccountList ){
                        List<Loyalty_Card__c> cards = new List < Loyalty_Card__c > ();
                        
                        if (  loyaltyCardListMap.containsKey ( loyaltyAccRecord.Id ) ) {
                            
                            if ( loyaltyCardListMap.get( loyaltyAccRecord.Id ).containsKey (  cardType) ) {
                                cards = loyaltyCardListMap.get( loyaltyAccRecord.Id ).get (  cardType ) ;
                            }
                        }
                        
                        
                        if(!loyaltyAccRecord.IsActive__c){ 
                            responseJSON.setResponseModel( 409, MyJLChatBotConstants.ACCOUNT_NOT_ACTIVE, MyJLChatBotConstants.ACCOUNT_IS_NOT_ACTIVE ); 
                            logRejectionRecordList.add ( logRejection(MyJLChatBotConstants.ERRORCODE.NO_ACTIVE_LOYALTY_ACCOUNT_FOUND, '', shopperId,MyJLChatBotConstants.SERVICE_NAME_FOR_ERROR) );
                            JSONResponseString = JSON.serialize( responseJSON);
                            return JSONResponseString ;
                        }
                        if(loyaltyAccRecord.Membership_Number__c != loyaltyMembershipNumber){
                                responseJSON.setResponseModel( 409, MyJLChatBotConstants.LOYALTY_NUMBER_NOT_ASSOCIATED_WITH_SHOPPER_ID_MSG, MyJLChatBotConstants.LOYALTY_NUMBER_NOT_ASSOCIATED_WITH_SHOPPER_ID ); 
                                logRejectionRecordList.add ( logRejection(MyJLChatBotConstants.ERRORCODE.NO_ACTIVE_LOYALTY_ACCOUNT_FOUND, '', shopperId,MyJLChatBotConstants.SERVICE_NAME_FOR_ERROR) ); 
                                JSONResponseString = JSON.serialize( responseJSON);
                                return JSONResponseString ;
                            } 
                        System.debug (' cards' + cards);
                        if (cards.size() == 1){
                            if(cards[0].Disabled__c == true){
                                responseJSON.setResponseModel( 409, MyJLChatBotConstants.LOYALTY_CARD_IS_NOT_ACTIVE, MyJLChatBotConstants.LOYALTY_CARD_IS_NOT_ACTIVE_MSG ); 
                                logRejectionRecordList.add ( logRejection(MyJLChatBotConstants.ERRORCODE.NO_ACTIVE_LOYALTY_ACCOUNT_FOUND, '', shopperId,MyJLChatBotConstants.SERVICE_NAME_FOR_ERROR));   
                                JSONResponseString = JSON.serialize( responseJSON);
                                return JSONResponseString ;
                            }
                            
                        }
                        if (cards.size() > 1){
                            if(cards[0].Disabled__c == true){
                                responseJSON.setResponseModel( 409, MyJLChatBotConstants.LOYALTY_CARD_IS_NOT_ACTIVE, MyJLChatBotConstants.LOYALTY_CARD_IS_NOT_ACTIVE_MSG ); 
                                logRejectionRecordList.add ( logRejection(MyJLChatBotConstants.ERRORCODE.NO_ACTIVE_LOYALTY_ACCOUNT_FOUND, '', shopperId,MyJLChatBotConstants.SERVICE_NAME_FOR_ERROR));   
                            }
                            if(loyaltyAccRecord.Membership_Number__c != loyaltyMembershipNumber){
                                responseJSON.setResponseModel( 409, MyJLChatBotConstants.LOYALTY_NUMBER_NOT_ASSOCIATED_WITH_SHOPPER_ID_MSG, MyJLChatBotConstants.LOYALTY_NUMBER_NOT_ASSOCIATED_WITH_SHOPPER_ID ); 
                                logRejectionRecordList.add ( logRejection(MyJLChatBotConstants.ERRORCODE.NO_ACTIVE_LOYALTY_ACCOUNT_FOUND, '', shopperId,MyJLChatBotConstants.SERVICE_NAME_FOR_ERROR) );                        
                            } 
                            if(cards[0].card_type__c == MyJL_Const.MY_PARTNERSHIP_CARD_TYPE){
                                responseJSON.setResponseModel( 409, MyJLChatBotConstants.MULTIPLE_CARDS_ISSUE, MyJLChatBotConstants.MULTIPLE_ACTIVE_MYPTR_CARD ); 
                                logRejectionRecordList.add ( logRejection(MyJLChatBotConstants.ERRORCODE.MY_LOYALTY_NUMBER_MISSING_DATA, '', '', MyJLChatBotConstants.SERVICE_NAME_FOR_ERROR));
                            }
                            if(cards[0].card_type__c == MyJLChatBotConstants.MY_JL_CARD_TYPE){
                                responseJSON.setResponseModel( 409, MyJLChatBotConstants.MULTIPLE_CARDS_ISSUE, MyJLChatBotConstants.MULTIPLE_ACTIVE_MYPTR_CARD ); 
                                logRejectionRecordList.add ( logRejection(MyJLChatBotConstants.ERRORCODE.MY_LOYALTY_NUMBER_MISSING_DATA, '', '', MyJLChatBotConstants.SERVICE_NAME_FOR_ERROR));
                            }
                        }else{                             
                            // Create a new case
                            Case caseRecord = createCaseRecord ( RequestType, cpforStaging, laforstaging,  myjlrecordtypeid);
                            caseRecordList.add ( caseRecord );                            
                        }
                        
                    }
                    
                    List < Id > caseRecordIdList = new List < Id > ();
                    try {
                        Database.SaveResult[] saveResultList = Database.insert( caseRecordList , false);
                        for (Database.SaveResult saveRes : saveResultList) {
                            if ( saveRes.isSuccess() ) {
                                caseRecordIdList.add ( saveRes.getId() );
                            }
                        }
                        List < String > caseNumberList = new List < String > ();
                        for ( Case caseRrecord : [ SELECT Id, CaseNumber 
                                                   FROM Case 
                                                   WHERE Id IN : caseRecordIdList]) {
                            caseNumberList.add ( caseRrecord.CaseNumber );
                        }
                        
                        if ( caseNumberList.size () > 0 ) {
                            responseJSON.setSuccessResponse ( 200, 'CASE_CREATED', ''+caseNumberList ); 
                        }
                        
                        
                        List < MyJL_Outbound_Staging__c > myOutBoundStagingList = new List < MyJL_Outbound_Staging__c > ();
                        List < Case > caseStagingUpdateList = new List < Case > ();
                        Map < Id, Case > caseRecordMap = new Map < Id, Case> ();
                        
                        for ( Case caseforStaging : [ SELECT id,owner.firstname,Integration_Ref_Holder__c,MyJL_Request_Record_Locked__c,owner.lastname,jl_Authorised_Person_Comments__c,
                                                      Mailing_City__c,Mailing_Country__c,Mailing_County__c,fast_track__c,Request_Type__c 
                                                      FROM Case 
                                                      WHERE id =:caseRecordIdList ]) {
                                                         //insert record in staging table for Abinitio to pick up                                       
                            MyJL_Outbound_Staging__c insertMyjstage = createNewMyJLOutboundStaging ( cpforStaging, laforstaging, caseforStaging );
                            myOutBoundStagingList.add ( insertMyjstage );    
                            caseRecordMap.put ( caseforStaging.Id, caseforStaging);                           
                        }
                        
                        try {
                            Database.SaveResult[] saveResultInsertList = Database.insert( myOutBoundStagingList , false);
                            List < Id > myOutBoundStagingSuccessList = new List < Id> ();
                            
                            for (Database.SaveResult saveRes : saveResultInsertList ) {
                                if ( saveRes.isSuccess() ) {
                                    myOutBoundStagingSuccessList.add ( saveRes.getId() );
                                }
                             }                          
                            for ( MyJL_Outbound_Staging__c myOutboundStageRecord : [ SELECT id, Case_Reference_ID__c 
                                                                                     FROM MyJL_Outbound_Staging__c 
                                                                                     WHERE Id IN : myOutBoundStagingSuccessList]) {
                                //Update the case with outbound staging reference
                                Case caseforStaging ;
                                if ( caseRecordMap.containsKey ( myOutboundStageRecord.Case_Reference_ID__c) ) {
                                    caseforStaging = caseRecordMap.get ( myOutboundStageRecord.Case_Reference_ID__c) ;
                                    caseforStaging.Integration_Ref_Holder__c = myOutboundStageRecord.id;
                                    caseforStaging.MyJL_Request_Record_Locked__c = true;
                                    caseStagingUpdateList.add ( caseforStaging );
                                }
                                
                                
                            }
                            
                        } catch ( Exception Ex ) {
                            responseJSON.setFailureResponse( 500, 'Error Occured While inserting My Outbound Message records ', ex.getMessage () ); 
                        }
                        try {
                            Database.SaveResult[] saveResultUpdateList = Database.update( caseStagingUpdateList , false);
                            
                        } catch ( Exception Ex ) {
                            responseJSON.setFailureResponse( 500, 'Error Occured While updating Case Records ', ex.getMessage () ); 
                        }
                        
                    } catch ( Exception Ex ) {
                        responseJSON.setFailureResponse( 500, 'Error Occured While inserting case records ', ex.getMessage () ); 
                    }                    
                }
                
             }
        }
        
        catch(Exception ex){
            responseJSON.setResponseModel( 500, 'CONNEX_SERVER_ERROR ', ex.getMessage () ); 
            logRejectionRecordList.add ( logRejection(MyJLChatBotConstants.ERRORCODE.UNHANDLED_EXCEPTION, '', '',MyJLChatBotConstants.SERVICE_NAME_FOR_ERROR));
        }
        
        
        JSONResponseString = JSON.serialize( responseJSON);
        
        try {
            Database.SaveResult[] saveResultList = Database.insert( logRejectionRecordList , false);
            
        } catch ( Exception ex ) {
            System.debug ('Error Occured while inserting the log rejection records ' + JSONResponseString);
        }
        
        System.debug ('JSONResponseString' + JSONResponseString);
        return JSONResponseString;
        
    }
    
    
    /**
    *@purpose : Method to fetch loyalty card records based on the account and card type field 
    *@param   : accountList  - Account List comes in 
    *@return  : Map < Id, Map < String, List < Loyalty_Card__c >  >>
    **/
    
    public static Map < Id, Map < String, List < Loyalty_Card__c >  >> fetchLoyaltyAccountCardMap  ( List< Loyalty_Account__c > accountList ) {
        Map < Id, Map < String, List < Loyalty_Card__c >  >> loyaltyCardMap = new Map < Id, Map < String, List < Loyalty_Card__c >  >>  ();
        for (  Loyalty_Card__c loyaltyCardRec : [ SELECT Card_Number__c,Card_Type__c,Delete_Entry__c,Disabled__c,Extract_ConcatReference__c,Id,Loyalty_Account__c,MyJL_ESB_Message_ID__c,Name,Origin__c,Pack_Type__c
                                                  FROM Loyalty_Card__c 
                                                  WHERE Loyalty_Account__c IN : accountList]) {
            if( loyaltyCardMap.containsKey( loyaltyCardRec.Loyalty_Account__c )){
                if( loyaltyCardMap.get( loyaltyCardRec.Loyalty_Account__c ).containsKey( loyaltyCardRec.Card_Type__c )){   
                    loyaltyCardMap.get(loyaltyCardRec.Loyalty_Account__c).get(loyaltyCardRec.Card_Type__c).add( loyaltyCardRec );
                }else {
                     loyaltyCardMap.get(loyaltyCardRec.Loyalty_Account__c).put(loyaltyCardRec.Card_Type__c, new List < Loyalty_Card__c > {loyaltyCardRec});
                 }                    
            } else {
                loyaltyCardMap.put(loyaltyCardRec.Loyalty_Account__c, new Map<String,List<Loyalty_Card__c>> { loyaltyCardRec.Card_Type__c => new List < Loyalty_Card__c > {loyaltyCardRec}});
              }
           }
        
        
        System.debug (' loyaltyCardMap'  + loyaltyCardMap);     
        return loyaltyCardMap;
        
    }
    
    /**
    *@purpose : Method to create the Case Record based on the parameters passed 
    *@param   : String RequestType, ==> Request type passed from the Chatbot
                Contact  cpforStaging, ==> Contact record with the currentShopperId
                Loyalty_Account__c laforstaging, 
                Id myjlrecordtypeid
    *@return  : Case
    **/
    public static Case createCaseRecord ( String RequestType, Contact  cpforStaging, Loyalty_Account__c laforstaging, Id myjlrecordtypeid ) {
        Case caseRecord = new Case();
        String title='';
        String initials ='';
        caseRecord.Request_Type__c = RequestType;                            
        // For staging                            
        if( cpforStaging.title != null ){
            title = cpforStaging.title;
        }
        if( cpforStaging.salutation != null ){
            title = cpforStaging.salutation;
        }
        if( cpforStaging.firstname != null ){
            initials = cpforStaging.firstname.substring(0,1).touppercase();
        }
                            
        // need to check for record type
        caseRecord.address_Line_1__c =   cpforStaging.Mailing_Street__c;
        caseRecord.address_line_2__c =   cpforStaging.Mailing_Address_Line2__c;  
        caseRecord.address_line_3__c =   cpforStaging.Mailing_Address_Line3__c;       
        caseRecord.address_line_4__c =   cpforStaging.Mailing_Address_Line4__c;  
        caseRecord.Mailing_City__c =     cpforStaging.mailingcity;          
        caseRecord.Mailing_Country__c =  cpforStaging.mailingcountry;
        caseRecord.Mailing_County__c =   cpforStaging.mailingstate;
        caseRecord.Post_Code__c =        cpforStaging.mailingpostalcode;    
        caseRecord.myJL_Membership_Number__c =     laforstaging.name;
        caseRecord.MYJL_Barcode_Number__c =        laforstaging.Customer_Loyalty_Account_Card_ID__c;
        caseRecord.recordtypeid =   myjlrecordtypeid;   
        caseRecord.status =    'Closed - No Response Required';
        caseRecord.Contactid =      cpforStaging.Id;              
        caseRecord.Despatch_Status__c =  'Processing';      
        caseRecord.description = 'mY JL Request'; 
        caseRecord.Request_Type__c = RequestType;
        caseRecord.Fast_track__c = False;
        caseRecord.Bypass_Standard_Assignment_Rules__c = true;
         
        return caseRecord;                  
    }
    
     /**
    *@purpose : Method to create the Case Record based on the parameters passed 
    *@param   : Contact  cpforStaging, ==> Contact record with the currentShopperId
                Loyalty_Account__c laforstaging, ==> Loyalty_Account__c  record with the currentShopperId
                Case caseforStaging ==> Case Record corresponding to staging record
    *@return  : MyJL_Outbound_Staging__c
    **/
    public static MyJL_Outbound_Staging__c createNewMyJLOutboundStaging ( Contact  cpforStaging, Loyalty_Account__c laforstaging, Case caseforStaging ) {
        String title='';
        String initials ='';
        // For staging                            
        if( cpforStaging.title != null ){
            title = cpforStaging.title;
        }
        if( cpforStaging.salutation != null ){
            title = cpforStaging.salutation;
        }
        if( cpforStaging.firstname != null ){
            initials = cpforStaging.firstname.substring(0,1).touppercase();
        }
         
        MyJL_Outbound_Staging__c insertMyjstage = new MyJL_Outbound_Staging__c();
        insertMyjstage.Activation_Date_Time__c = laforstaging.Activation_Date_Time__c; 
        insertMyjstage.Address_LIne_1__c = cpforStaging.Mailing_Street__c;  
        insertMyjstage.Address_Line_2__c = cpforStaging.Mailing_Address_Line2__c; 
        insertMyjstage.Address_Line_3__c = cpforStaging.Mailing_Address_Line3__c; 
        insertMyjstage.Address_Line_4__c = cpforStaging.Mailing_Address_Line4__c;
        insertMyjstage.Address_Line_5__c = cpforStaging.Mailing_House_No_Text__c;
        insertMyjstage.Address_Line_6__c = cpforStaging.Mailing_House_Name__c; 
        insertMyjstage.Agent_Id__c = caseforStaging.owner.firstname +' '+caseforStaging.owner.lastname;
        insertMyjstage.Case_Reference_ID__c = caseforStaging.Id; 
        insertMyjstage.Case_Request_Cancelled__c = false; //
        insertMyjstage.Change__c = ''; 
        insertMyjstage.Comments__c = caseforStaging.jl_Authorised_Person_Comments__c;
        insertMyjstage.County__c = cpforStaging.mailingstate;  
        insertMyjstage.Created_Date_Holder__c = datetime.now().format('YYYY-MM-dd HH:mm:ss.SSS','UTC')+'Z'; //cwr
        insertMyjstage.Deactivation_Date_Time__c = laforstaging.Deactivation_Date_Time__c;
        insertMyjstage.Email_Address__c = cpforStaging.email;
        insertMyjstage.Fast_track__c = caseforStaging.fast_track__c;
        insertMyjstage.First_Name__c = cpforStaging.firstname; 
        insertMyjstage.Initials__c = initials; 
        insertMyjstage.Is_Active__c = laforstaging.IsActive__c;    
        insertMyjstage.Last_Modified_Date_Holder__c = insertMyjstage.Created_Date_Holder__c;
        insertMyjstage.Last_Name__c = cpforStaging.lastname;
        insertMyjstage.MYJL_Barcode_NUmber__c = laforstaging.Customer_Loyalty_Account_Card_ID__c;
        insertMyjstage.MYJL_Membership_Number__c = laforstaging.name;
        insertMyjstage.Post_Code__c = cpforStaging.mailingpostalcode; 
        insertMyjstage.Request_Type__c = caseforStaging.Request_Type__c;
        insertMyjstage.Scheme_Joined_Date_Time__c = laforstaging.Scheme_Joined_Date_Time__c;
        insertMyjstage.Shopper_ID__c = cpforStaging.Shopper_ID__c;    
        insertMyjstage.Title__c = title;  
        insertMyjstage.Town__c = cpforStaging.mailingcity;
        return insertMyjstage;
    }
    
     /**
    *@purpose : Method to create the MyJL_Rejection__c Record based on the parameters passed 
    *@param   : code ==> Error Code for the rejection record
                email ==> email to which the log has to be sent
                shopperId ==> shopper id passed 
                service ==> service type
    *@return  : MyJL_Rejection__c
    **/
    
     public static MyJL_Rejection__c logRejection(MyJLChatBotConstants.ERRORCODE code, String email, String shopperId, String service) {
        MyJL_Rejection__c rec = new MyJL_Rejection__c();
        MyJL_Util.setNotBlank(rec, 'Id__c', shopperId); //wrapper.getCustomerId()
        MyJL_Util.setNotBlank(rec, 'EmailAddress__c', email); // email
        MyJL_Util.setNotBlank(rec, 'Rejection_Code__c', code.name());
        //get description from Custom Setting
        String description = service + ': ' + getErrorMessage(code).Error_Message__c;
        description = description.abbreviate(255);
        MyJL_Util.setNotBlank(rec, 'Rejection_Description__c', description);
        return rec;
    }
    
     /**
    *@purpose : Method to create the MyJL_Error_Messages__c Record based on the error code passed 
    *@param   : code ==> Error Code for the rejection record
    *@return  : MyJL_Error_Messages__c
    **/
    
    public static MyJL_Error_Messages__c getErrorMessage(final MyJLChatBotConstants.ERRORCODE code) {
        final Map<String, MyJL_Error_Messages__c> errorMessages = MyJL_Error_Messages__c.getAll();
        if(null != errorMessages && errorMessages.containsKey(code.name())) {
            return errorMessages.get(code.name());
        } else {
            return new MyJL_Error_Messages__c(Error_Message__c = MyJL_Const.ERROR_NOT_CONFIGURED_MESSAGE, Name = null, Error_Type__c = MyJL_Const.ERROR_NOT_CONFIGURED_TYPE);
        }
    }
    
    
}