global class ScheduleBatchDeletePotentialCustomers implements Schedulable{

     global void execute(SchedulableContext sc){
        GDPR_BatchDeletePotentialCustomers obj = new GDPR_BatchDeletePotentialCustomers();
        Database.executebatch(obj);
    }
    
}