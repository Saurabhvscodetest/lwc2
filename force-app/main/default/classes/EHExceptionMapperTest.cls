@isTest
public class EHExceptionMapperTest {
    
     /* load the custom settings */  
    static {
        try{
            Test.loadData(Constants__c.sObjectType, 'Constants');
        }catch(Exception e){
          System.debug('Exception'+e.getMessage());
        }
    }
	
	static testmethod void testExcpetionMapIsPopulated() {
		boolean taskToBECreated = true;
		String sampleExceptionName = 'Sample name';
		String sampleExceptionDesc = 'XXXX';
		String index = '1';
		EH_Exception_Mapper__c exceptionMapper = new EH_Exception_Mapper__c(
			name = index,
			Case_Type__c = EHExceptionMapper.CaseType.QUERY.name(),
			Handle_Exception__c = taskToBECreated,
			Exception_Name__c = sampleExceptionName,
			Delivery_Type__c = EHConstants.GVF,
			Description__c = sampleExceptionDesc
		);
		insert exceptionMapper;
		EHExceptionMapper.EHExceptionToCaseInformation excepInfo = EHExceptionMapper.getDescriptionForException(EHConstants.GVF , sampleExceptionName);
		system.assert(excepInfo != null, 'ExceptionToCaseInfo Information must not be null');	
		system.assertEquals( EHExceptionMapper.CaseType.QUERY, excepInfo.ehCasetype , 'Case Type must match');	
		system.assertEquals(taskToBECreated, excepInfo.isTaskToBeCreated , 'Task creation must match');	
		system.assertEquals(EHConstants.GVF, excepInfo.deliveryType , 'Delivery must match');	
		system.assertEquals(sampleExceptionDesc, excepInfo.taskDescription , 'Description must match');	
	}
	
	static testmethod void testExcpetionRequiredToBeHandled() {
		boolean taskToBECreated = true;
		String sampleExceptionName = 'Sample name';
		String sampleExceptionDesc = 'XXXX';
		String index = '1';
		EH_Exception_Mapper__c exceptionMapper = new EH_Exception_Mapper__c(
			name = index,
			Case_Type__c = EHExceptionMapper.CaseType.QUERY.name(),
			Handle_Exception__c = taskToBECreated,
			Exception_Name__c = sampleExceptionName,
			Delivery_Type__c = EHConstants.GVF,
			Description__c = sampleExceptionDesc
		);
		insert exceptionMapper;
		system.assert(EHExceptionMapper.isExceptionNeededToBeHandled(EHConstants.GVF , sampleExceptionName), 'ExceptionToCaseInfo Information must not be null');	
	}

}