/* Description  : GDPR - Live Chat Transcripts  Data Deletion
*
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   26/12/2018        Chanchal Ghodki                  Created                COPT-4279
* 1.1   13/02/2019        Vignesh Kotha                    Modified               COPT-4279
*/
global class GDPR_BatchDeleteLiveChatTranscripts  implements Database.Batchable<sobject>,Database.Stateful{
    global Map<Id,String> ErrorMap = new Map<Id,String>();	
    global integer Counter=0;    
    private static final DateTime  Prior_Year = System.now().addYears(-6);  
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id ,CaseId,CreatedDate,LiveChatVisitorId FROM LiveChatTranscript where CaseId=Null AND CreatedDate <: Prior_Year');
    }    
    global void execute(Database.BatchableContext BC, List<LiveChatTranscript> records){        
        if(records.size()>0){
            try{
                list<Database.deleteResult> srList = Database.delete(records, false);
                for(Integer result = 0; result < srList.size(); result++) {
                    if (srList[result].isSuccess()) {
                        Counter++;
                    }else{
                        for(Database.Error err : srList[result].getErrors()){
                            ErrorMap.put(srList.get(result).id,err.getMessage());
                        }
                    }
                }
                Database.emptyRecycleBin(records);
            }
            Catch(exception e)
            {
                EmailNotifier.sendNotificationEmail('Exception from GDPR_BatchDeleteLiveChatTranscripts',e.getMessage());
            }
        }  
    } 
    global void finish(Database.BatchableContext BC){		  
        String textBody ='';
        set<Id> allIds = new set<Id>();
        textBody += Counter +' records deleted in object Live Chat Transcript, Live Chat Visitor'+'\n';
        if (!ErrorMap.isEmpty() || Test.isRunningTest()){
            for (Id recordids : ErrorMap.KeySet())
            {
                allIds.add(recordids);
            } 
            textBody+= 'Error log: '+allIds+'\n';
        }
        EmailNotifier.sendNotificationEmail('GDPR Live Chat Transcripts',textBody); 
        //calling LiveChatTranscipt batch class
        BAT_DeleteLiveChatVisitors deleteChatVisitors = new BAT_DeleteLiveChatVisitors();
        Database.executeBatch(deleteChatVisitors);
    }    
}