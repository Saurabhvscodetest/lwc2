/**
 *	@author: Andrey Gavrikov (westbrook)
 *	@date: 2014-08-15 08:30:02 
 *	@description:
 *	    called from custom Apex web service to allow external endpoints to
 *	    create Retail Transactions in Salesforce. 
 *	
 *	Version History :   
 *	2014-08-15 - MJL-727 - AG
 *	initial version
 *
 * 001		15/04/15	NTJ		MJL-1790 - Do not do a SELECT WHERE x IN set, if we have an empty set 
 * 002		12/05/15	NTJ		Retire the implementation as it is not being used. Always return false. To restore code
 *								1) Remove NOT IN USE COMMENTS
 *								2) Write a Unit Test for this code (it was never used - hence no unit tests available) Ab>Initio uses Partner API	
 */
global with sharing class MyJLKD_CreateHandler {

	private class Wrapper extends MyJL_Util.TransactionWrapper {

		final SFDCKitchenDrawerTypes.KDTransaction kdTransaction;
		Transaction_Header__c header;
		MyJL_Const.ERRORCODE errorCode = null;
		public Id cardId;
		private Set<Id> associatedTransactionIds = new Set<Id>();

		/* NOT IN USE
		public Wrapper(final String messageId, SFDCKitchenDrawerTypes.KDTransaction trans) {
			this.messageId = messageId;
			this.kdTransaction = trans;
		}
		NOT IN USE
		*/

    	public override SFDCKitchenDrawerTypes.KDTransaction getKDTransactionInput() {
			return kdTransaction;
		} 
		
		/* NOT IN USE
		public Boolean hasError() {
			return null != errorCode;
		}
		
		public void setError(final MyJL_Const.ERRORCODE code) {
			this.errorCode = code;
		}

		public void addAssociatedTransactionId(final Id transactionId) {
			if (null != transactionId) {
				associatedTransactionIds.add(transactionId);
			}
		}

		public String[] getAssociatedTransactionIds() {
			final List<String> associatedIdsLowerCase = new List<String>();
			String[] ids = getKDTransactionInput().associatedTransactionIds;
			if (null != ids) {
				for(String associatedId : ids) {
					if (!MyJL_Util.isBlank(associatedId)) {
						associatedIdsLowerCase.add(associatedId.toLowerCase());
					}
				}

			}
			return associatedIdsLowerCase;
			
		}

		public Boolean isVoided() {
			return false;
		}

		public List<Associated_Transaction__c> getInitialisedAssociatedTransactions() {
			System.assert(null != header && null != header.Id, 'Init Transaction_Header__c before calling getInitialisedAssociatedTransactions()');
			final List<Associated_Transaction__c> associatedTransactions = new List<Associated_Transaction__c>();
			for(Id transactionId : associatedTransactionIds) {
				associatedTransactions.add(new Associated_Transaction__c(Transaction__c = header.Id, Associated_Transaction__c = transactionId));
			    
			}
			return associatedTransactions;
		}
		NOT IN USE
		*/

		/* NOT IN USE
		public List<Attachment> getInitialisedAttachments() {
			final List<Attachment> attachments = new List<Attachment>();
			if (!MyJL_Util.isBlank(kdTransaction.fullXml)) {
				attachments.add(newAttachment(header.Id, MyJL_Util.getFullXmlName(), kdTransaction.fullXml));
			}
			if (!MyJL_Util.isBlank(kdTransaction.shortXml)) {
				attachments.add(newAttachment(header.Id, MyJL_Util.getShortXmlName(), kdTransaction.shortXml));
			}
			
			return attachments;
		}
		*/
		
		public override SFDCKitchenDrawerTypes.ActionResult getActionResult() {
			SFDCKitchenDrawerTypes.ActionResult result = new SFDCKitchenDrawerTypes.ActionResult();
			/* NOT IN USE
			result.success = !hasError();
			if (hasError()) {
				MyJL_Util.populateKDErrorDetails(this.errorCode, result);
			}
			SFDCKitchenDrawerTypes.KDTransaction t = new SFDCKitchenDrawerTypes.KDTransaction();
			t.transactionId = getTransactionId();
			result.kdTransaction = t;
			NOT IN USE */
			return result;
			
		}
	}


	private static Id logHeaderId = null;
    webservice static SFDCKitchenDrawerTypes.ActionResponse process(final SFDCKitchenDrawerTypes.RequestHeader requestHeader, 
                                                                    final SFDCKitchenDrawerTypes.KDTransaction[] kdTransactions) {
        
		logHeaderId = LogUtil.startKDLogHeader(LogUtil.SERVICE.WS_KD_CREATE_TRANSACTIONS, requestHeader, null, null);
		
		SFDCKitchenDrawerTypes.ActionResponse response = new SFDCKitchenDrawerTypes.ActionResponse();
		response.messageId = requestHeader.messageId;
		response.success = false;
/* THIS IS NOT IN USE		
		try {
			if (null != requestHeader && !MyJL_Util.isBlank(requestHeader.MessageId)) {
				if (MyJL_Util.isMessageIdUnique(requestHeader.MessageId)) {
					final List<Wrapper> wrappers = getWrappers(requestHeader.MessageId, kdTransactions);
					checkDuplicates(wrappers);
					
					loadCardIds(wrappers);

					insertTransactions(wrappers);

					//voidTransactions(wrappers);

					LogUtil.logKDDetails(logHeaderId, wrappers);
					response.success = true;
					response.results = preparePerTransactionResult(wrappers);
				} else {
					//duplicate message Id
					MyJL_Util.populateKDErrorDetailsHeader(MyJL_Const.ERRORCODE.MESSAGEID_DUPLICATE, response);
				}
			} else {
				//missing message id
				MyJL_Util.populateKDErrorDetailsHeader(MyJL_Const.ERRORCODE.MESSAGEID_NOT_PROVIDED, response);
			}
			LogUtil.updateKDLogHeader(logHeaderId, response, null);
			//updateLog(response, null);

		} catch (Exception e) {
			//responseHeader.ResultStatus.Success = false;
			//we do not have the code for this situation
			response = MyJL_Util.populateKDErrorDetailsHeader(MyJL_Const.ERRORCODE.UNHANDLED_EXCEPTION, response);
			LogUtil.updateKDLogHeader(logHeaderId, response, e.getStackTraceString());
			System.assertEquals(false, Test.isRunningTest(), 'exception thrown: ' + e + '; ' + e.getStackTraceString());
			System.debug('agX e=' + e);
			System.debug('agX e.stack=' + e.getStackTraceString());
			//Source_System__c
		}
		THIS IS NOT IN USE
		*/
		return response;
	}

/* THIS IS NOT IN USE

	private static List<Wrapper> getWrappers(final String messageId, final SFDCKitchenDrawerTypes.KDTransaction[] kdTransactions) {
		final List<Wrapper> wrappers = new List<Wrapper>();
		if (null != kdTransactions) {
			for(SFDCKitchenDrawerTypes.KDTransaction kdTrans : kdTransactions) {
				Wrapper w = new Wrapper(messageId, kdTrans);
				wrappers.add(w);
				validateInputData(w);
			}
		}
		return wrappers;

	}

	private static Boolean validateInputData(final Wrapper transactionWrapper) {
		final SFDCKitchenDrawerTypes.KDTransaction kdTransaction = transactionWrapper.getKDTransactionInput();
		if (!transactionWrapper.isVoided()) {
			if (MyJL_Util.isBlank(kdTransaction.transactionId)) {
				transactionWrapper.setError(MyJL_Const.ERRORCODE.TRANSACTIONID_NOT_PROVIDED);
			} else {
				if (!transactionWrapper.hasError() && MyJL_Util.isBlank(kdTransaction.cardNumber)) {
					transactionWrapper.setError(MyJL_Const.ERRORCODE.CARD_NUMBER_NOT_PROVIDED);
				} 			
				if (!transactionWrapper.hasError() && MyJL_Util.isBlank(kdTransaction.fullXml)) {
					transactionWrapper.setError(MyJL_Const.ERRORCODE.FULL_XML_NOT_PROVIDED);
				} 			
				if (!transactionWrapper.hasError() && MyJL_Util.isBlank(kdTransaction.shortXml)) {
					transactionWrapper.setError(MyJL_Const.ERRORCODE.SHORT_XML_NOT_PROVIDED);
				} 			
				//
				//   if (!transactionWrapper.hasError() && null == kdTransaction.transactionAmount) {
				//   transactionWrapper.setError(MyJL_Const.ERRORCODE.TRANSACTION_AMOUNT_NOT_PROVIDED);
				//   } 			
				//   if (!transactionWrapper.hasError() && null == kdTransaction.transactionDate) {
				//   transactionWrapper.setError(MyJL_Const.ERRORCODE.TRANSACTION_DATE_NOT_PROVIDED);
				//   } 				 
				//
			}
		}
		return transactionWrapper.hasError();
	}

	private static void checkDuplicates(final List<Wrapper> wrappers) {
		final Map<String, Wrapper> wrapperByTransactionId = new Map<String, Wrapper>();
		for(Wrapper w : wrappers) {
			if (w.hasError()) {
				continue;
			}
			String transactionId = w.getTransactionId();
			if (wrapperByTransactionId.containsKey(transactionId)) {
				w.setError(MyJL_Const.ERRORCODE.TRANSACTIONID_NOT_UNIQUE);
				continue;
			}
			wrapperByTransactionId.put(transactionId, w);
		}

		// MJL-1790
		if (!wrapperByTransactionId.keySet().isEmpty()) {
			for(Transaction_Header__c header : [select Id, Receipt_Barcode_Number__c, MyJL_ESB_Message_ID__c, Store_Code__c, Channel__c, Card_Number__c, Card_Number_Text_Version__c,
													Transaction_Amount__c, Transaction_Date_Time__c, Transaction_Type__c
												from Transaction_Header__c where Receipt_Barcode_Number__c in :wrapperByTransactionId.keySet()]) {
				// MJL-1790
				if (String.isNotBlank(header.Receipt_Barcode_Number__c)) {
					Wrapper w = wrapperByTransactionId.remove(header.Receipt_Barcode_Number__c.toLowerCase());
					if (w.isVoided()) {
						w.header = header;
						continue;
					}
					if (null != w) {
						w.setError(MyJL_Const.ERRORCODE.TRANSACTIONID_NOT_UNIQUE);
						continue;
					}					
				}				
			}			
		}

		//check if there are transactions (to void) which do not exist
		for(Wrapper w : wrapperByTransactionId.values()) {
			if (w.isVoided()) {
				w.setError(MyJL_Const.ERRORCODE.VOIDEDTRANSACTIONID_NOT_FOUND);
				continue;
			}
		}
	}

	private static void loadCardIds(final List<Wrapper> wrappers) {
		final Set<String> cardNumbers = new Set<String>();
		for(Wrapper w : wrappers) {
			if (w.hasError() || w.isVoided()) {
				continue;
			}
			cardNumbers.add(w.getKDTransactionInput().cardNumber);
		}

		final Map<String, Id> cardIdByNumber = new Map<String, Id>();
		// MJL-1790
		if (!cardNumbers.isEmpty()) {
			for(Loyalty_Card__c card : [select Id, Name, Loyalty_Account__c from Loyalty_Card__c where Name in :cardNumbers]) {
				// MJL-1790
				if (String.isNotBlank(card.Name)) {
					cardIdByNumber.put(card.Name, card.Id);					
				}
			}			
		}

		for(Wrapper w : wrappers) {
			if (w.hasError() || w.isVoided()) {
				continue;
			}
			String cardNumber = w.getKDTransactionInput().cardNumber;
			Id cardId = cardIdByNumber.get(cardNumber);
			if (null == cardId) {
				//w.setError(MyJL_Const.ERRORCODE.CARD_NUMBER_NOT_FOUND);
				continue;
			}
			w.cardId = cardId;

		}
	}

	private static void loadAssociatedTransactionIds(final List<Wrapper> wrappers) {
		final Set<String> transactionNumbers = new Set<String>();
		for(Wrapper w : wrappers) {
			if (w.hasError()) {
				continue;
			}
			transactionNumbers.addAll(w.getAssociatedTransactionIds());
		}
		transactionNumbers.remove(null);
		transactionNumbers.remove('');
		transactionNumbers.remove('NULL');

		final Map<String, Id> transactionIdByNumber = new Map<String, Id>();
		// MJL-1790
		if (!transactionNumbers.isEmpty()) {
			for(Transaction_Header__c header : [select Id, Receipt_Barcode_Number__c from Transaction_Header__c where Receipt_Barcode_Number__c in :transactionNumbers]) {
				// MJL-1790
				if (String.isNotBlank(header.Receipt_Barcode_Number__c)) {
					transactionIdByNumber.put(header.Receipt_Barcode_Number__c.toLowerCase(), header.Id);					
				}
			}			
		}

		for(Wrapper w : wrappers) {
			if (w.hasError()) {
				continue;
			}
			for(String associatedNumber : w.getAssociatedTransactionIds()) {
				Id associatedId = transactionIdByNumber.get(associatedNumber);
				if (null == associatedId) {
					w.setError(MyJL_Const.ERRORCODE.ASSOCIATED_TRANSACTION_NOT_FOUND);
					continue;
				}
				w.addAssociatedTransactionId(associatedId);
			}
		}
	}

	private static void insertTransactions(final List<Wrapper> wrappers) {
		final List<Transaction_Header__c> transactionsToInsert = new List<Transaction_Header__c>();
		final List<Wrapper> validWrappers = new List<Wrapper>();
		for(Wrapper w : wrappers) {
			if (w.hasError() || w.isVoided()) {
				continue;
			}
			String transactionId = w.getTransactionId();
			Transaction_Header__c tran = popilateTransactionHeader(w);
			transactionsToInsert.add(tran);
			w.header = tran;
			validWrappers.add(w);

		}

		handleErrors(Database.insert(transactionsToInsert), validWrappers);
		
		loadAssociatedTransactionIds(wrappers);
		
		insertRelatedData(wrappers);
	}

	private static void insertRelatedData(final List<Wrapper> wrappers) {
		final List<Associated_Transaction__c> associatedTransactions = new List<Associated_Transaction__c>();
		final List<Attachment> attachments = new List<Attachment>();
		
		final List<Wrapper> wrapperToAssociatedTransaction = new List<Wrapper>();
		final List<Wrapper> wrapperToAttachment= new List<Wrapper>();

		for(Wrapper w : wrappers) {
			if (w.hasError() || w.isVoided()) {
				continue;
			}
			String transactionId = w.getTransactionId();
			for(Associated_Transaction__c assTrans : w.getInitialisedAssociatedTransactions()) {
				associatedTransactions.add(assTrans);
				wrapperToAssociatedTransaction.add(w);
			}

			for(Attachment attachment: w.getInitialisedAttachments()) {
				attachments.add(attachment);
				wrapperToAttachment.add(w);
			}
		}
		handleErrors(Database.insert(attachments), wrapperToAttachment);
		handleErrors(Database.insert(associatedTransactions), wrapperToAssociatedTransaction);

	}

	private static Set<String> handleErrors(Database.SaveResult[] results, final List<Wrapper> wrappers) {
		final Set<String> transactionNumberWithError = new Set<String>();
		try {
			Integer index = 0;
			for(Database.SaveResult result : results) {
				Wrapper wrapper = wrappers[index++];
				if (!result.isSuccess()) {
					System.debug('agX result.getErrors()[0]=' + result.getErrors()[0]);
					Boolean isDuplicate = result.getErrors()[0].getStatusCode() == StatusCode.DUPLICATE_VALUE;
					if (!isDuplicate) {
						wrapper.setError(MyJL_Const.ERRORCODE.FAILED_TO_PERSIST_DATA);
					} else {
						//create failed due to duplicate unique Id
						wrapper.setError(MyJL_Const.ERRORCODE.TRANSACTIONID_NOT_UNIQUE);
					}
					transactionNumberWithError.add(wrapper.getTransactionId());
				}
			}
		} catch (Exception e) {
			//we do not log this type of errors
		}
		return transactionNumberWithError;

	}

	private static void handleErrors(Database.DeleteResult[] results, final List<Wrapper> wrappers) {
		try {
			Integer index = 0;
			for(Database.DeleteResult result : results) {
				Wrapper wrapper = wrappers[index++];
				if (!result.isSuccess()) {
					wrapper.setError(MyJL_Const.ERRORCODE.FAILED_TO_PERSIST_DATA);
				}
			}
		} catch (Exception e) {
			//we do not log this type of errors
		}
	}

	private static Transaction_Header__c popilateTransactionHeader(final Wrapper wrapper) {
		Transaction_Header__c tran = new Transaction_Header__c();
		final SFDCKitchenDrawerTypes.KDTransaction kdTransaction = wrapper.getKDTransactionInput();
		
		MyJL_Util.setNotBlank(tran, 'MyJL_ESB_Message_ID__c', wrapper.messageId);
		MyJL_Util.setNotBlank(tran, 'Receipt_Barcode_Number__c', kdTransaction.transactionId);
		MyJL_Util.setNotBlank(tran, 'Store_Code__c', kdTransaction.branchNumber);
		MyJL_Util.setNotBlank(tran, 'Channel__c', kdTransaction.channel);
		MyJL_Util.setNotBlank(tran, 'Card_Number__c', wrapper.cardId);
		MyJL_Util.setNotBlank(tran, 'Card_Number_Text_Version__c', kdTransaction.cardNumber);
		MyJL_Util.setNotBlank(tran, 'Transaction_Amount__c', kdTransaction.transactionAmount);
		MyJL_Util.setNotBlank(tran, 'Transaction_Date_Time__c', kdTransaction.transactionDate);
		MyJL_Util.setNotBlank(tran, 'Transaction_Type__c', kdTransaction.transactionType);
		return tran;

	}

	private static Attachment newAttachment(final Id parentId, final String name, String text) {
		Attachment att = new Attachment(ParentId = parentId);
		att.Body = Blob.valueOf(text);
		att.ContentType = 'text/xml';
		att.Name = name;
		return att;
	}

	private static SFDCKitchenDrawerTypes.ActionResult[] preparePerTransactionResult(List<Wrapper> wrappers) {
		final List<SFDCKitchenDrawerTypes.ActionResult> results = new List<SFDCKitchenDrawerTypes.ActionResult>();
		for(Wrapper wrapper : wrappers) {
			results.add(wrapper.getActionResult());
		}
		return results;
	}
	*/ 
}