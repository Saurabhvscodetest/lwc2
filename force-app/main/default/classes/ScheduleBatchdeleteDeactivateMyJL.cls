global class ScheduleBatchdeleteDeactivateMyJL implements Schedulable{
	global void execute(SchedulableContext sc){
        BAT_DeactivateMyJlAccount obj = new BAT_DeactivateMyJlAccount();
        Database.executebatch(obj);
    }
}