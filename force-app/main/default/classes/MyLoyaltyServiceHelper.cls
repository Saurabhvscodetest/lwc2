public with sharing class MyLoyaltyServiceHelper {


	public static List<Loyalty_Account__c> getLoyaltyAccountsWithShopperId(string shopperId){

		List<Loyalty_Account__c> loyaltyAccounts = [Select id, IsActive__c,Membership_Number__c, (Select id from MyJL_Cards__r where Card_Type__c =: MyJL_Const.MY_PARTNERSHIP_CARD_TYPE) From Loyalty_Account__c Where ShopperId__c = :shopperId];
        return loyaltyAccounts;
	}
    
    public static List<Loyalty_Card__c> getLoyaltyCardWithLoyaltyAccount(Id loyaltyAccount , string partnershipCardNumber){
    	List<Loyalty_Card__c> loyaltyCard = [Select id,Disabled__c from Loyalty_Card__c where Loyalty_Account__c =:loyaltyAccount and Name =:partnershipCardNumber];  
        return loyaltyCard;
    } 
    
    
    public static List<Loyalty_Card__c> getLoyaltyCards (Id loyaltyAccount , String loyaltyMembershipNumber){
        List <Loyalty_Card__c> loyaltyCard ;
        if(loyaltyMembershipNumber.length() == 11){
            loyaltyCard = [Select id, Disabled__c from Loyalty_Card__c where card_type__c =: MyJL_Const.MY_JL_CARD_TYPE and Loyalty_Account__c =:loyaltyAccount];
          
        }
        if (loyaltyMembershipNumber.length() == 16){
             loyaltyCard = [Select id, Disabled__c from Loyalty_Card__c where card_type__c =: MyJL_Const.MY_PARTNERSHIP_CARD_TYPE and Loyalty_Account__c =:loyaltyAccount];
        }
        return loyaltyCard; 
    }
    
}