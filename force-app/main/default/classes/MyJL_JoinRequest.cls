/**
 *  @author: Ravi Telu
 *  @date: 25/11/2019
 *  @description:
 *      REST interface to handle the incoming request from Loyalty System for myJL Join or Rejoin process 
 * 		for the existing customers
 *  
 *  Version History :   
 *  25/11/2019 - COPT-5200
 */	
@RestResource(urlmapping='/MyJL_JoinRequest/customer/*')
global class MyJL_JoinRequest {
    
    private static final String WELCOME_EMAIL = 'Welcome Email';
    private static JoinResponseWrapper jsonResponse {get;set;}
        
    @HttpPost
    global static JoinResponseWrapper processJoinRejoinRequest(String shopperId, String messageId) {
        return getJoinRejoinRequest(shopperId, messageId);
    }
    
    global static JoinResponseWrapper getJoinRejoinRequest(String shopperId, String messageId) {
        String SERVICE_NAME_FOR_ERROR = 'MyJL_JoinRequest REST Service';
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;
        
		jsonResponse = NEW JoinResponseWrapper();
        
        try {
            LogUtil.startLogHeaderREST(messageId, '', 'WS_JOIN_CUSTOMER', shopperId, 1);
            if(String.isEmpty(messageId)) {
                res.statusCode = 400;
                jsonResponse = NEW JoinResponseWrapper('BAD_REQUEST_PARAMETERS','Badly formatted messageId', messageId);
                MyJL_Util.logRejection(MyJL_Const.ERRORCODE.MESSAGEID_NOT_PROVIDED, '', shopperId, SERVICE_NAME_FOR_ERROR);
            }
            else if(String.isEmpty(shopperId)) {
                res.statusCode = 400;
                jsonResponse = NEW JoinResponseWrapper('BAD_REQUEST_PARAMETERS','Badly formatted shopperId', messageId);
                MyJL_Util.logRejection(MyJL_Const.ERRORCODE.CUSTOMERID_NOT_PROVIDED, '', shopperId, SERVICE_NAME_FOR_ERROR);
            } 
            else {
                System.debug('Success process starts here! ');
                //Data Structure for ReJoin Request
                List<Loyalty_Account__c> loyaltyAccountsReJoinList = NEW List<Loyalty_Account__c>();
                Map<String, Loyalty_Account__c> loyaltyAccountsReJoinMap = NEW Map<String, Loyalty_Account__c>();
                List<Loyalty_Account__c> accountsToSendOutboundNotifs = NEW List<Loyalty_Account__c>();
                
                //Data Structure for New Join Request
                List<Loyalty_Account__c> loyaltyAccountsJoinList = NEW List<Loyalty_Account__c>();
                List<Loyalty_Card__c> loyaltyCardJoinList = NEW List<Loyalty_Card__c>();
                Loyalty_Account__c loyaltyAccount = NEW Loyalty_Account__c();
                Loyalty_Card__c loyaltyCard = NEW Loyalty_Card__c();
                
                //Data Structure for relationship of Loyalty Account and Loyalty Card
                Map<String, Loyalty_Account__c> loyAccMap = NEW  Map<String, Loyalty_Account__c>();
                Map<String, Loyalty_Card__c> loyCardMap = NEW  Map<String, Loyalty_Card__c>();
                
                //Data Structure for final response to Loyalty
                Map<String, JoinResponseWrapper> finalResponseResult = NEW Map<String, JoinResponseWrapper>();
                
                //Duplicate messageId validation
                if(MyJL_Util.isMessageIdUnique(messageId)) {
                    Set<String> receivedCustomerIds = NEW Set<string>();
                    if(shopperId!=NULL && !String.isBlank(shopperId)) {
                        receivedCustomerIds.add(shopperId);               
                    }
                    System.debug('receivedCustomerIds @@@@@@@@@@@@ '+receivedCustomerIds);
                    
                    //get the existing contacts based on shopperId here
                    Contact[] existingContactProfiles = queryContactProfilesWithActiveLoyaltyAccounts(receivedCustomerIds);
                    System.debug('existingContactProfiles @@@@@@@@@@@@@@@ '+existingContactProfiles);
                    
                    //check whether or not existing contact is available for the shopperId
                    //get the existing contacts with the loyalty_account based on the shopperId
                    if(existingContactProfiles.size()>0) {
                        System.debug('existingContactProfiles are available for the shopperId');
                        Set<String> existingCustomerProfiles = NEW Set<String>();
                    	Map<String, Contact> contactProfileToLoyaltyAccount = NEW Map<String, Contact>();
                        
                        System.debug('entering in the existing customers having loyalty account');
                        for(Contact contactProfile : existingContactProfiles) {
                            String customerId = contactProfile.Shopper_Id__c;
                            existingCustomerProfiles.add(customerId);
                            if(contactProfile.MyJL_Accounts__r != NULL) {
                                System.debug('entering loyalty account availability');
                                contactProfileToLoyaltyAccount.put(customerId, contactProfile);                               
                            }
                        }
                        
						//Validate the shopperId
                        if(contactProfileToLoyaltyAccount.containsKey(shopperId)) {
                            System.debug('entering if contactProfileToLoyaltyAccount');
                            System.debug('entering if contactProfileToLoyaltyAccount.get(shopperId).MyJL_Accounts__r @ '+contactProfileToLoyaltyAccount.get(shopperId).MyJL_Accounts__r);
                            System.debug('entering if contactProfileToLoyaltyAccount.get(shopperId).MyJL_Accounts__r.size() @ '+contactProfileToLoyaltyAccount.get(shopperId).MyJL_Accounts__r.size());
                            
                            if(contactProfileToLoyaltyAccount.get(shopperId).MyJL_Accounts__r != NULL && contactProfileToLoyaltyAccount.get(shopperId).MyJL_Accounts__r.size() > 0 ) {
                                System.debug('entering if if contactProfileToLoyaltyAccount');
                                Integer counter=0;
                                for(Loyalty_Account__c la : contactProfileToLoyaltyAccount.get(shopperId).MyJL_Accounts__r) {
                                    System.debug('la @@@@@@@@@@@@@@@@@@@@@@ '+la);
                                    System.debug('loyaltyAccountsReJoinMap.containsKey(shopperId) @@@ '+loyaltyAccountsReJoinMap.containsKey(shopperId));
                                    if(la.IsActive__c==false && !loyaltyAccountsReJoinMap.containsKey(shopperId)) {
                                        System.debug('loyaltyAccountsReJoinList @@@@ '+loyaltyAccountsReJoinList);
                                        System.debug('Rejoin the Customer to activate the myJL account');
                                        Loyalty_Account__c loyAcct = reJoinLoyaltyAccounts(la, messageId);
                                        loyaltyAccountsReJoinMap.put(shopperId, loyAcct);
                                        loyaltyAccountsReJoinList.add(loyAcct);
                                        counter++;
                                    }
                                }
                                if(counter==0) {
                                    res.statusCode = 409;
                                    //to send the results of already joined customer from database in payload
                                    finalResponseResult = activeLoyaltyAccountResults(contactProfileToLoyaltyAccount.get(shopperId).MyJL_Accounts__r, messageId);
                                    jsonResponse = finalResponseResult.values()[0];
                                    //jsonResponse = NEW JoinResponseWrapper('CUSTOMER_ALREADY_JOINED','Customer already joined', messageId);
                                    //MyJL_Util.logRejection(MyJL_Const.ERRORCODE.CUSTOMER_ALREADY_JOINED, '', shopperId, SERVICE_NAME_FOR_ERROR);
                                }
                                //Activate the loyalty account
                                if(loyaltyAccountsReJoinList.size()>0) {
                                    finalResponseResult = saveResults(Database.Update(loyaltyAccountsReJoinList, false), loyaltyAccountsReJoinList, messageId);
                                    if(finalResponseResult!=NULL && finalResponseResult.values()!=NULL && finalResponseResult.values().size()>0) {
                                        res.statusCode = 200;
                                        jsonResponse = finalResponseResult.values()[0];
                                        System.debug('jsonResponse : @@@@@ '+jsonResponse);
                                    }
                                    sendOutboundNotifications('JoinNotification', loyaltyAccountsReJoinList, accountsToSendOutboundNotifs);
                                }
                            }
                            else {
                                System.debug('Entering into unavailable loyalty account list for shopperId');
                                for(Contact c : existingContactProfiles) {
                                    loyaltyAccountsJoinList = NEW List<Loyalty_Account__c>();
                                    loyaltyAccount = populateLoyaltyAccount(c, shopperId, messageId);
                                	loyaltyAccountsJoinList.add(loyaltyAccount);
                                }
                                generateNewMembershipNumbers(loyaltyAccountsJoinList);
                                if(loyaltyAccountsJoinList.size()>0) {                                    
                                    finalResponseResult = saveResults(Database.Insert(loyaltyAccountsJoinList, false), loyaltyAccountsJoinList, messageId);
                                    System.debug('jsonResponse @@@@@@@@@@@@@@@@ '+jsonResponse);
                                    System.debug('finalResponseResult @@@@@@@@@@@@@@@@ Accounts '+finalResponseResult);
                                    loyaltyCardJoinList = NEW List<Loyalty_Card__c>();
                                    for(Loyalty_Account__c lAcct : loyaltyAccountsJoinList) {
                                        if(lAcct.Id != NULL && lAcct.ShopperId__c==shopperId) {
                                            loyAccMap.put(shopperId, lAcct);
                                            loyaltyCard = populateLoyaltyCard(lAcct, messageId);
                                            //create relationship between Loyalty Account and Loyalty Card
                                            loyCardMap.put(shopperId, loyaltyCard);
                                            loyaltyCardJoinList.add(loyaltyCard);
                                        }
                                    }
                                    generateNewCardIds(loyAccMap, loyCardMap, false);
                                    if(loyaltyCardJoinList.size()>0) {
                                        finalResponseResult = saveResults(Database.Insert(loyaltyCardJoinList, false), loyaltyAccountsJoinList, loyaltyCardJoinList, messageId);
                                        System.debug('finalResponseResult @@@@@ Cards '+finalResponseResult);
                                        if(finalResponseResult!=NULL && finalResponseResult.values()!=NULL && finalResponseResult.values().size()>0) {
                                        	res.statusCode = 200;
                                            jsonResponse = finalResponseResult.values()[0];
                                            System.debug('jsonResponse : @@@@@ '+jsonResponse);
                                        }
                                    }
                                    sendOutboundNotifications('JoinNotification', loyaltyAccountsJoinList, accountsToSendOutboundNotifs);
                                    if(LoyaltyCardHandler.OPTIMISED_JOIN_CODE) { 
                                        schedulePostProcessing(loyaltyCardJoinList);
                                    }
                                    sendWelcomeEmails(loyaltyAccountsJoinList, WELCOME_EMAIL);
                                }
                            }
                        }
                        else {
                            System.debug('Entering into no customer availability');
                            res.statusCode = 404;
                            jsonResponse = NEW JoinResponseWrapper('NOT_FOUND','Customer with shopperId does not exist', messageId);
                            MyJL_Util.logRejection(MyJL_Const.ERRORCODE.CUSTOMERID_NOT_FOUND, '', messageId, SERVICE_NAME_FOR_ERROR);
                        }
                    }
                    else {
                        System.debug('Entering into no customer availability');
                        res.statusCode = 404;
                        jsonResponse = NEW JoinResponseWrapper('NOT_FOUND','Customer with shopperId does not exist', messageId);
                    	MyJL_Util.logRejection(MyJL_Const.ERRORCODE.CUSTOMERID_NOT_FOUND, '', messageId, SERVICE_NAME_FOR_ERROR);
                    }
                }
                else {
                    res.statusCode = 400;
                    jsonResponse = NEW JoinResponseWrapper('BAD_REQUEST_PARAMETERS','Badly formatted messageId', messageId);
                    MyJL_Util.logRejection(MyJL_Const.ERRORCODE.MESSAGEID_DUPLICATE, '', messageId, SERVICE_NAME_FOR_ERROR);
                }
            }
        }
        catch(Exception e) {
            jsonResponse = NEW JoinResponseWrapper('UNEXPECTED_ERROR', 'Error occurred when processing the join request', messageId);
            MyJL_Util.logRejection(MyJL_Const.ERRORCODE.UNHANDLED_EXCEPTION, '', '',SERVICE_NAME_FOR_ERROR);
            res.statusCode = 500;
        }        
        return jsonResponse;
    }
	
	//set the values for Rejoin loyalty account
    private static Loyalty_Account__c reJoinLoyaltyAccounts(Loyalty_Account__c la, final String messageId) {
        Loyalty_Account__c loyaltyAccount = new Loyalty_Account__c();
        loyaltyAccount.Id = la.Id;
        loyaltyAccount.IsActive__c = true;
        loyaltyAccount.Activation_Date_Time__c = System.now();
        loyaltyAccount.MyJL_ESB_Message_ID__c = messageId;
        loyaltyAccount.Welcome_Email_Sent_Flag__c = true;
        loyaltyAccount.Voucher_Preference__c = la.Voucher_Preference__c;
        loyaltyAccount.Name = la.Name;
        loyaltyAccount.Customer_Loyalty_Account_Card_ID__c = la.Customer_Loyalty_Account_Card_ID__c;
        return loyaltyAccount;
    }
    
	//get the existing contacts with loyalty accounts
    private static Contact[] queryContactProfilesWithActiveLoyaltyAccounts(Set<String> customerIds) {
        List<Contact> contactProfiles = NEW List<Contact>();
               
        if(customerIds != null && customerIds.size() > 0) {
            for (Contact cp: [
                SELECT
                id,
                Shopper_ID__c, SourceSystem__c, Email, 
                (Select 
                 id, Name, Voucher_Preference__c, Customer_Loyalty_Account_Card_ID__c,  
                 IsActive__c, ShopperId__c,
                 contact__c
                 From MyJL_Accounts__r) 
                FROM 
                contact
                WHERE
                Shopper_ID__c IN :customerIds
                AND SourceSystem__c = :Label.MyJL_JL_comSourceSystem
            ]) {  
                if (String.isNotBlank(cp.Shopper_ID__c)) {
                    contactProfiles.add(cp);
                }
            }
        }
        System.debug('contactProfiles @@@@@@@@@@@@@@@@@ '+contactProfiles);
        return contactProfiles;
    }
	    
	//set the values for loyalty accounts
    private static Loyalty_Account__c populateLoyaltyAccount(Contact c, final String shopperId, final String messageId) {
        Loyalty_Account__c acc = new Loyalty_Account__c();
        if (acc.Scheme_Joined_Date_Time__c==NULL) {
            acc.Scheme_Joined_Date_Time__c = System.now();
        }
        acc.IsActive__c = true;
        acc.Activation_Date_Time__c = System.now();
        acc.Channel_ID__c = System.Label.MyJL_JL_comSourceSystem;
        acc.MyJL_ESB_Message_ID__c = messageId;
        acc.ShopperId__c = shopperId;
        acc.Email_Address__c = c.Email;
        acc.Contact__c = c.Id;
        acc.adopted__c = true;
        
        return acc;
    }
    
	//set the values for loyalty cards
    private static Loyalty_Card__c populateLoyaltyCard(Loyalty_Account__c la, final String messageId) {
        Loyalty_Card__c card = new Loyalty_Card__c();
        card.Loyalty_Account__c = la.Id;
        card.Card_Type__c = MyJL_Const.MY_JL_CARD_TYPE;
        card.MyJL_ESB_Message_ID__c = messageId;
        return card;
    }
    
    //membership number generation
    public static void generateNewMembershipNumbers(final List<Loyalty_Account__c> loyAccList) {
        
        final List<String> membershipNumbers = new List<String>();        
        final List<MyJL_Membership_Number__c> recs = new List<MyJL_Membership_Number__c>();
		
        for(Loyalty_Account__c la : loyAccList) {
            recs.add(new MyJL_Membership_Number__c());
        }        
        if (!recs.isEmpty()) {
            Database.insert(recs, false);
            final Set<Id> recordIds = new Set<Id>();
            
			for(MyJL_Membership_Number__c rec : recs) {
                recordIds.add(rec.Id);
            }
            
            List<MyJL_Membership_Number__c> memberships = new List<MyJL_Membership_Number__c>();
            if (!recordIds.isEmpty()) {
                memberships = [select Id, Name from MyJL_Membership_Number__c where Id in :recordIds];
            }
            Integer index = 0;
            for(Loyalty_Account__c la : loyAccList) {
                if (index < memberships.size()) {
                    la.Name = memberships[index].Name + MyJL_Util.calculateChecksum(memberships[index].Name); 
                    la.Membership_Number__c = la.Name;
                    index++;
                }
            }
            cleanUpNumberGenerationObject(recordIds);           
        }
    }
    
	//delete the records in Membership Numbers object which are no longer required
    @future
    private static void cleanUpNumberGenerationObject(final Set<Id> recordIds) {
        Database.delete(new List<Id>(recordIds));
    }
    
    //new method for card number generation
    public static void generateNewCardIds(final Map<String, Loyalty_Account__c> loyAccMap, final Map<String, Loyalty_Card__c> loyCardMap, Boolean updateAccount) {
        
        String prefix = CustomSettings.getMyJLSetting().getString('Barcode_Number_Prefix__c');
        if(!Test.isRunningTest()) {
        	System.assertNotEquals(null, prefix, 'Invalid Configuration - Barcode_Number_Prefix__c in MyJL Custom_Settings is not defined');
        }
		
		List<MyJL_Online_Card_Number__c> cards = new List<MyJL_Online_Card_Number__c>();
		
        for(String shopperId : loyCardMap.keySet()) {
        	String seed='';
            Loyalty_Account__c loyAcc = loyAccMap.get(shopperId);
            //seed = loyAccMap.get(shopperId).Name;
            if(loyAcc != NULL) {
                seed = loyAcc.Name;            
                
                String cardWithoutCheckDigit = reduceMembershipNumber(seed);
                String cardWithCheckDigit = prefix + cardWithoutCheckDigit + MyJL_Util.calculateChecksum(cardWithoutCheckDigit);    
                
                Loyalty_Card__c loyCard = loyCardMap.get(shopperId);
                if (!updateAccount) {
                    loyCard.Name = cardWithCheckDigit; 
                    loyCard.Card_Number__c = loyCard.Name;
                }
                else if (loyAcc.Name != NULL) {
                    loyAcc.Customer_Loyalty_Account_Card_ID__c = loyCard.Name;
                }
            }
        }
    }
        
	//membership number generation process continued
	private static String reduceMembershipNumber(String seed) {
        String s0 = seed.substring(0,1);
        String s2to10 = seed.substring(2); 
        String cn = s0 + s2to10;        
        return cn;
    }
    
    //send the welcome emails asynchronous
    private static void sendWelcomeEmails(List<Loyalty_Account__c> laList, String emailType) {
        if (!laList.isEmpty()) {
            final Set<Id> laIdSet = new Set<Id>();
            for (Loyalty_Account__c la:laList) {
                if (String.isNotBlank(la.Email_Address__c)) {
                    laIdSet.add(la.Id);                 
                }
            }
            sendWelcomeEmailsAsync(laIdSet, emailType);         
        }
    }
    
    ////asynchronous emails sender in future
    @future
    private static void sendWelcomeEmailsAsync(final Set<Id> laIdSet, String emailType) {
        List<Log_Header__c> lhList = NEW List<Log_Header__c>();
        List<Loyalty_Account__c> laList = NEW List<Loyalty_Account__c>();
		
        for (Id laId:laIdSet) {
            if(laId != NULL){
                Loyalty_Account__c la = NEW Loyalty_Account__c(id=laId, Send_Welcome_email__c=true);
                laList.add(la);                
                lhList.add(makeLogHeader(la, emailType));
            }
        }
        
        if(laList.size() > 0){
            update laList;
        }
        if(lhList.size() > 0){
            insert lhList;
        }
    }
    
	//set the values log header 
    private static Log_Header__c makeLogHeader(Loyalty_Account__c la, String emailType) {
        Log_Header__c lh = NEW Log_Header__c();
        lh.Service__c = emailType;
        lh.Request_Data__c = la.Id;
        lh.Success__c = true;
        return lh;
    }
    
    public static void postJoinProcessCards(Set<Id> newCardSet) {
        joinProcessCards(newCardSet);
    }
    
    private static void joinProcessCards(Set<Id> newCardSet) {
		
        List<Loyalty_Card__c> cardList = [SELECT Id, Force_Orphan_Transaction_Search__c,Name, Loyalty_AccounT__c FROM Loyalty_Card__c WHERE Id IN :newCardSet];
		
        if (cardList.isEmpty() == false) {
            loyaltyCardHandler.linkOrphanedTransactions(cardList, NULL);        
        }
		
        List<Loyalty_Account__c> accountsToUpdate = new List<Loyalty_Account__c>();
        for (Loyalty_Card__c lc:cardList) {
            if (lc.Loyalty_Account__c != NULL) {
                Loyalty_Account__c la = NEW Loyalty_Account__c(id=lc.Loyalty_Account__c);
                la.Customer_Loyalty_Account_Card_ID__c = lc.Name;
                accountsToUpdate.add(la);               
            }
        }
        
        if (accountsToUpdate.isEmpty() == false) {
            try {
                update accountsToUpdate;
            } catch (Exception e) {
                system.debug('problem updating account: '+e);
                myJL_Util.logRejection(MyJL_Const.ERRORCODE.UNHANDLED_EXCEPTION , '', '', 'Join: '+e.getStackTraceString());                
            }
        }       
    }
    
	//reflect the card number to loyalty account
    private static void schedulePostProcessing(List<Loyalty_Card__c> cards) {
        Set<Id> cardIdSet = new Set<Id>();
        for (Loyalty_Card__c card:cards) {
            cardIdSet.add(card.Id);
        }
        if (cardIdSet.isEmpty() == false) {
            postJoinProcessCards(cardIdSet);            
        }
    }
    
	//send outbound notifications to external system
    private static void sendOutboundNotifications(String notification, List<Loyalty_Account__c> loyAcctList, List<Loyalty_Account__c> accountsToSendOutboundNotif) {
        BaseTriggerHandler.removeSkipReason(notification);
        accountsToSendOutboundNotif = MyJL_Util.accountsToNotify(loyAcctList);
        MyJL_LoyaltyAccountHandler.sendJoinOrLeaveNotification(BaseTriggerHandler.toIdSet(accountsToSendOutboundNotif));
    }
    
	//save the final results for Loyalty Accounts
	private static Map<String, JoinResponseWrapper> saveResults(Database.SaveResult[] results, final List<Loyalty_Account__c> loyAccList, final String messageId) {
        //Boolean hasErrors = false;
        Integer index = 0;
        Map<String, JoinResponseWrapper> responseResult = NEW Map<String, JoinResponseWrapper>();
        for(Database.SaveResult result : results) {
            Loyalty_Account__c loyAcc = loyAccList[index++];
            if (!result.isSuccess()) {
                JoinResponseWrapper errorResponse = NEW JoinResponseWrapper('FAILED_TO_PERSIST_DATA','Failed during data commit', messageId);
                responseResult.put(loyAcc.ShopperId__c, errorResponse);
            }
            else {
                JoinResponseWrapper successResponse = NEW JoinResponseWrapper(loyAcc.Name, loyAcc.Customer_Loyalty_Account_Card_ID__c, loyAcc.Voucher_Preference__c, MyJL_Const.MY_JL_CARD_TYPE, 'OK','Success', messageId);
                responseResult.put(loyAcc.ShopperId__c, successResponse);
            }
        }
        return responseResult;
    }   
    
    //save the final results for Loyalty Cards
    private static Map<String, JoinResponseWrapper> saveResults(Database.SaveResult[] results, final List<Loyalty_Account__c> loyAccList, final List<Loyalty_Card__c> loyCardList, final String messageId) {
        
        Integer index = 0;
        Map<Integer, String> indexShopperId = NEW Map<Integer, String>();
        Map<Integer, String> indexloyaltyNumber = NEW Map<Integer, String>();
        
        //to get the default value of Voucher Preference from Loyalty Account
        String voucherPreference;
        Schema.DescribeFieldResult F = Loyalty_Account__c.Voucher_Preference__c.getDescribe();
        List <Schema.PicklistEntry> pickVals = F.getPicklistValues();        
        for (Schema.PicklistEntry pv: pickVals) {
            if (pv.isDefaultValue()) {
                voucherPreference = pv.getValue();
            }    
        }
        
        for(Loyalty_Card__c loyCard : loyCardList) {
        	for(Loyalty_Account__c loyAcct : loyAccList) {
                if(loyAcct.Id==loyCard.Loyalty_Account__c) {
                    indexShopperId.put(index, loyAcct.ShopperId__c);
                    indexloyaltyNumber.put(index, loyAcct.Name);
                    index++;
                    break;
                }
            }
        }
        index=0;
        Map<String, JoinResponseWrapper> responseResult = NEW Map<String, JoinResponseWrapper>();
        for(Database.SaveResult result : results) {
            String shopperId = indexShopperId.get(index);
            String loyaltyNumber = indexloyaltyNumber.get(index);
            if (!result.isSuccess()) {
                JoinResponseWrapper errorResponse = NEW JoinResponseWrapper('FAILED_TO_PERSIST_DATA','Failed during data commit', messageId);
                responseResult.put(shopperId, errorResponse);
            }
            else {
                System.debug('shopperId @@@@@@@@@@@@@@@ '+shopperId);
                System.debug('loyaltyNumber @@@@@@@@@@@@@@@ '+loyaltyNumber);
                System.debug('loyCardList[index].Card_Number__c @@@@@@@@@@@@@@@ '+loyCardList[index].Card_Number__c);
                System.debug('else success block');
                JoinResponseWrapper successResponse = NEW JoinResponseWrapper(loyaltyNumber, loyCardList[index].Card_Number__c, voucherPreference, MyJL_Const.MY_JL_CARD_TYPE, 'OK','Success', messageId);
                responseResult.put(shopperId, successResponse);
            }
        	index++;
        }
        return responseResult;
    }
    
    //send the existing values in payload for active loyalty accounts
    private static Map<String, JoinResponseWrapper> activeLoyaltyAccountResults(List<Loyalty_Account__c> loyAccList, String messageId) {
        
        Map<String, JoinResponseWrapper> responseResult = NEW Map<String, JoinResponseWrapper>();
        for(Loyalty_Account__c la : loyAccList) {
            if(la!=NULL) {
                String shopperId = la.ShopperId__c;
                String loyaltyNumber = la.Name;
                String barcodeNumber = la.Customer_Loyalty_Account_Card_ID__c;
                String voucherPreference = la.Voucher_Preference__c;
                String loyaltyType='';
                if(la.Name.length()==11) {
                	loyaltyType = MyJL_Const.MY_JL_CARD_TYPE;
                }
                else {
                    //16 digit
                    loyaltyType = MyJL_Const.MY_PARTNERSHIP_CARD_TYPE;
                }
        		JoinResponseWrapper successResponse = NEW JoinResponseWrapper(loyaltyNumber, barcodeNumber, voucherPreference, loyaltyType, 'OK','Success', messageId);
            	responseResult.put(shopperId, successResponse);
            }
        }
        
        return responseResult;
    }
    
	//wrapper
    global class JoinResponseWrapper {
        
        global String errorCode { get;set; } 
        global String errorMessage { get;set; }
        global String messageId { get; set; }
        global String loyaltyNumber { get; set; }
        global String barcodeNumber { get; set; }
        global String voucherPreference { get; set; }
        global String loyaltyType { get; set; }
        
        global JoinResponseWrapper() { }
        
        global JoinResponseWrapper(String errorCode, String errorMessage, String messageId) {
            this.errorCode = errorCode;
            this.errorMessage = errorMessage;
            this.messageId = messageId;
        }
            
        global JoinResponseWrapper(String loyaltyNumber, String barcodeNumber, String voucherPreference, String loyaltyType, String errorCode, String errorMessage, String messageId) {
            this.errorCode = errorCode;
            this.errorMessage = errorMessage;
            this.messageId = messageId;
            this.loyaltyNumber = loyaltyNumber;
            this.barcodeNumber = barcodeNumber;
            this.voucherPreference = voucherPreference;
            this.loyaltyType = loyaltyType;
        }
    }
}