/**
 *	@author: Andrey Gavrikov (westbrook) 
 *	@date: 2014-08-28 10:55:40 
 *	@description:
 *	    handler for all MyJL trigger events on Contact_Profile__c object
 *	
 *	Version History :   
 *	2014-08-28 - MJL-746 - AG
 *	send outbound notification when "Shopper" Contact Profile Update event occurs
 *
 *	001		12/03/15		NTJ		MJL-1615 - If we get a update customer we only send a SYNC message if we have a Loyalty Account
 *  002		20/03/15		NTJ		If CreateContacts hits an error on the Database.Insert then allow the successes to happen.
 *	003		15/04/15		NTJ		MJL-1790 - Do not do a SELECT WHERE x IN set, if we have an empty set		
 *  004		20/04/15		NTJ		Remove Debug.log
 *  005		28/04/15		NTJ		MJL-1867 - Cannot send SYNC notification from Async Update WS
 *  006		20/05/15		NTJ		MJL-1957 - use LastModifiedDateMs__c as it has milliseconds 
 */
public without sharing class MyJL_ContactProfileHandler extends MyJL_TriggerHandler implements RetriableCallout {
	public static String SOURCE_SYSTEM = System.Label.MyJL_JL_comSourceSystem;

	public static final String UPDATE_NOTIFICATION_SKIP_CODE = 'Contact_Profile_Update_Notification';

	override public void beforeInsert () {
		//linkToContacts((List<contact>)Trigger.new);
	}

	override public void afterUpdate () {
		// MJL-1867 - this call is only allowed if we are not in an @future
		if (trigger.isExecuting == false) {
			// this will fail first time as you cannot call a we from a trigger, the retry will make it happen
			sendUpdateNotification((List<contact>)Trigger.new);			
		}
	}

	///////////////////////////// RetriableCallout /////////////////////////////////////////////
	private MyJL_LoyaltyAccountHandler.NotifyCustomerEventPort service = new MyJL_LoyaltyAccountHandler.NotifyCustomerEventPort();

	private String topic;
	public void setTopic(final String topic) {
		this.topic = topic;
	}
	public String serialise (final Object obj) {
		return JSON.serialize(obj);
	}
	public Object deserialise (final String val) {
		return JSON.deserialize(val, SObject.class);
	}

	public void setEndpoint(final String url) {
		//System.debug('agX setEndpoint=' + url);
		service.endpoint_x = url;
	}
	public void setTimeout(final Integer mills) {
		service.timeout_x = mills;
	}
	public void setClientCertificateName(final String uniqueName) {
		service.clientCertName_x = uniqueName;
	}

/* the original - replaced by a variant that can handle partial joined accounts
	public void callService1(final List<Object> sobjects) {
		final List<SFDCMyJLCustomerTypes.Customer> customers = new List<SFDCMyJLCustomerTypes.Customer>();
		for(Object obj : sobjects) {
			Contact_Profile__c profile = (Contact_Profile__c)obj;
			customers.add(MyJL_MappingUtil.populateCustomer(profile));
		}
		System.assert(String.isNotBlank(this.topic), 'Topic must have been defined. Check if Callout_Settings__c are configured properly');
		service.NotifyCustomerEvent(customers, this.topic);
	}
*/	
	// MJL-1615 need to get sub-lists of Loyalty_Accounts via email
	public void callService(final List<Object> sobjects) {
		final List<SFDCMyJLCustomerTypes.Customer> customers = new List<SFDCMyJLCustomerTypes.Customer>();

		// first get the list of Contact Profiles and build the lookup set
		List<contact> cpList = new List<contact>();
		Set<String> emailSet = new Set<String>();
		Map<String, Id> email2CpIdMap = new Map<String, Id>();
		
		for(Object obj : sobjects) {
			// first deserialise the contact profile and add to list
			contact profile = (contact)obj;
			cpList.add(profile);
			//system.debug('profile: '+profile);
			//system.debug('profile.MyJL_Accounts__r: '+profile.MyJL_Accounts__r);

			// second if there is no related list for loyalty accounts then do some additional work
			List<Loyalty_Account__c> las = (List<Loyalty_Account__c>)profile.MyJL_Accounts__r;
			if (las == null || las.isEmpty()) {
				// if email specified then add to the set and map
				if (String.isBlank(profile.email) == false) {
					emailSet.add(profile.email);
					email2CpIdMap.put(profile.email.toLowerCase(), profile.Id);
				}				
			}
		}
		//system.debug('emailSet: '+emailSet);
		// Do the lookup if we harvested some partial join emails
		List<Loyalty_Account__c> laList = new List<Loyalty_Account__c> ();
		if (!emailSet.isEmpty()) {
			laList = [SELECT Name, Email_Address__c, Scheme_Joined_Date_Time__c, Activation_Date_Time__c, Deactivation_Date_Time__c,
	                  Welcome_Email_Sent_Flag__c, Registration_Workstation_ID__c, Registration_Business_Unit_ID__c, Channel_ID__c,
	                  LastModifiedById, IsActive__c, LastModifiedDate, LastModifiedDateMs__c,Voucher_Preference__c,
	                  (SELECT Id, Loyalty_Account__c, Name, Loyalty_Account__r.contact__c FROM MyJL_Cards__r) FROM Loyalty_Account__c 
	                  WHERE Email_Address__c IN :emailSet];			
		}
		// Now build the list
		Map<Id, List<Loyalty_Account__c>> cp2LaMap = MyJL_MappingUtil.getLoyaltyAccountLists(laList, email2CpIdMap); 

		for(contact profile : cpList) {
			customers.add(MyJL_MappingUtil.populateCustomer2(profile, cp2LaMap));
		}
		System.assert(String.isNotBlank(this.topic), 'Topic must have been defined. Check if Callout_Settings__c are configured properly');
		service.NotifyCustomerEvent(customers, this.topic);
	}
	
	
	//////////////////////////////////////////////////////////////////////////////////////
	public static void sendUpdateNotification(final List<Contact> contactProfiles) {
		if (BaseTriggerHandler.hasSkipReason(UPDATE_NOTIFICATION_SKIP_CODE)) {
			return;
		}
		
		// MJL 1615 - Find out which ones have Loyalty Accounts
		final Set<Id> contactProfileIds = getContactProfilesWithMyJLAccounts(contactProfiles);

		if (!contactProfileIds.isEmpty()) {
			//make a call out
			system.debug(' CustomerManagement.loadContactProfiles(contactProfileIds)'+ CustomerManagement.loadContactProfiles(contactProfileIds));
			CalloutHandler.callout(LogUtil.SERVICE.OUTBOUND_CONTACT_PROFILE_UPDATE_NOTIFY.name(), CustomerManagement.loadContactProfiles(contactProfileIds));
			//sendUpdateNotificationAsync(contactProfileIds, logHeaderId);
		}
	}

	// MJL 1615 - Find out which ones have Loyalty Accounts
    private static Set<Id> getContactProfilesWithMyJLAccounts(final List<contact> contactProfiles) {
		// initialise the result
		final Set<Id> contactProfileIds = new Set<Id>();
		// initialise the set of emails we want to search for
		final Set<String> emailSet = new Set<String>();
		// a map that remembers what Id's we saw for each Contact Profile email
		final Map<String,Id> email2ContactProfileMap = new Map<String,Id>();
		// loop through the profiles and get the email addresses
		for(contact profile : contactProfiles) {
			if (SOURCE_SYSTEM == profile.SourceSystem__c) {
				if (profile.email != null) {
					emailSet.add(profile.email);
					email2ContactProfileMap.put(profile.email.toLowerCase(), profile.Id);					
				}
			}
		}
		//system.debug('emailSet: '+emailSet);

		// Look for Loyalty Addresses with those email addresses and look for Contact Profile Id. If the accounts have been JOINed then we can send SYNC notifications
		// If they have not been JOINed then we should not send the Notifications. The aim is to minimise the traffic to the ESB
		// MJL-1790
		List<Loyalty_Account__c> laList = new List<Loyalty_Account__c>(); 
		if (!emailSet.isEmpty()) {
			laList = [SELECT contact__c, Email_Address__c,activation_date_time__c,Voucher_Preference__c  FROM Loyalty_Account__c WHERE Email_Address__c IN :emailSet];			
		}

		for (Loyalty_Account__c la:laList) {
			// Find the Contact Profile that the email refers to and add to the map
			if (String.IsBlank(la.Email_Address__c) == false) {
				Id cpId = (Id) email2ContactProfileMap.get(la.Email_Address__c.toLowerCase());
				if (cpId != null && la.activation_date_time__c !=null) {
					contactProfileIds.add(cpId);	
				}			
			}
		}
		//system.debug('contactProfileIds: '+contactProfileIds);

		return contactProfileIds;   	 
    }
    
    private class CustomerWrapper extends CustomerManagement.CustomerWrapper {
		private Contact_Profile__c contactProfile; 
		private Contact contact;
	
		public CustomerWrapper(final Contact_Profile__c contactProfile) {
			this.contactProfile = contactProfile;
		}

		public void setError(Database.SaveResult sr) {
			//system.debug('sr: '+sr);
			if (sr != null) {
				if (!sr.isSuccess()) {
					List<Database.Error> errors = sr.getErrors();
					if (errors != null && errors.size() > 0) {
						//system.debug('errors: '+errors);
						contactProfile.addError(errors[0].getMessage());
					} else {
						contactProfile.addError('Could not process Contact Profile');
					}
				}
			}
		}
		
		public Boolean hasErrors() {
			return false;
		}

		public override String getAddressLine1() {
			return contactProfile.Mailing_Street__c;
		}

		public override String getPostalCode() {
			return contactProfile.MailingPostCode__c;
		}

		// MJL-1692
		public override String getCity() {
			return contactProfile.MailingCity__c;
		}

		// MJL-1693 Start
		public override Date getBirthDate() {
			return (Date) contactProfile.Date_of_Birth__c;
		}

		public override String getInitials() {
			return contactProfile.Initials__c;
		}

		public override String getCountry() {
			return contactProfile.MailingCountry__c;
		}

		public override String getState() {
			return contactProfile.MailingState__c;
		}

		public override String getSalutation() {
			return contactProfile.Salutation__c;
		}

		public override String getPhone() {
			return contactProfile.Telephone_Number__c;
		}
		
		
		// MJL-1693 End

		public override String getCustomerId() {
			return contactProfile.Shopper_ID__c;
		}

		public override String getEmailAddress() {
			return contactProfile.Email__c;
		}

		public override String getFirstName() {
			return contactProfile.FirstName__c;
		}
		public override String getLastName() {
			return contactProfile.LastName__c;
		}

		public override Contact getContact() {
			return this.contact;
		}
		public override void setContact(final Contact contact) {
			this.contact = contact;
		}	
		
		// Nice to know how we matched Contact to Contact Profile
		public override void setMatchMethod(String matchMethod) {
			//system.debug('method: '+matchMethod);
			this.contactProfile.Match_Method__c = matchMethod;
		}			
	}
	/**
	 * MJL-1081
	 * match the created Contact Profile against existing Contacts and create a
	 * new Contact if no match found, and then parent the Contact Profile with
	 * the found/created Contact.
	 */
	private static void linkToContacts(List<Contact_Profile__c> recs) {
		final List<CustomerWrapper> wrappers = new List<CustomerWrapper>();
		for(Contact_Profile__c rec : recs) {
			if (null == rec.Contact__c) {
				wrappers.add(new CustomerWrapper(rec));
			}
		}
		
		final Set<String> shopperIdsWithContact = CustomerManagement.findContacts(wrappers);
		//system.debug('agX shopperIdsWithContact=' + shopperIdsWithContact);
		//collect wrappers which do not have existing contact
		final List<CustomerWrapper> wrapperWithContactToCreate = new List<CustomerWrapper>();
		for(CustomerWrapper wrapper : wrappers) {
			if (!wrapper.hasErrors() && !shopperIdsWithContact.contains(wrapper.getCustomerId())) {
				wrapperWithContactToCreate.add(wrapper);
				wrapper.setMatchMethod(CustomerManagement.MATCH_CREATED); 
			}
		}
		createContacts(wrapperWithContactToCreate);

		for(CustomerWrapper wrapper : wrappers) {
			//system.debug('agX checking wrapper: ' + wrapper);
			if (null == wrapper.contactProfile.Contact__c && null != wrapper.getContact() 
					&& null != wrapper.getContact().Id && !wrapper.hasErrors()) {
				wrapper.contactProfile.Contact__c = wrapper.getContact().Id;
			}
		}
	}

	private static void createContacts(List<CustomerWrapper> wrappers) {
		final List<Contact > recsToCreate = new List<Contact>();
		for(CustomerWrapper wrapper : wrappers) {
			Contact rec = MyJL_MappingUtil.populateContact(wrapper);
			wrapper.setContact(rec);
			recsToCreate.add(rec);
		}
		//system.debug('agX contacts to create: ' + recsToCreate);
		// TODO: 2nd param default is true, which means allornone. Think about whether we want this behaviour. i.e. if we have 200 and 1 fails then do we want all 200 to fail?
		List<Database.SaveResult> results = Database.insert(recsToCreate, false);
		for (Integer i =0; i<recsToCreate.size(); i++) {
			if (!results[i].isSuccess()) {
				wrappers[i].setError(results[i]);	
			}
		}		
	}
}