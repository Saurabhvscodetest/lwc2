/*************************************************
coCustomerOrdersController_TEST

test class for the coCustomerOrdersController controller

Author: Steven Loftus (MakePositive)
Created Date: 18/11/2014
Modification Date: 
Modified By: 

**************************************************/
@isTest
private class coCustomerOrdersController_TEST {
    
    public static testmethod void getOrderHeadersWithoutOrderNumber() {

        // set up the config settings
        Config_Settings__c configSearch = new Config_Settings__c();
        configSearch.Name = CustomSettingsManager.WS_Endpoint_SearchCustomer;
        configSearch.Text_Value__c = 'SearchCustomer';
        insert configSearch;

        Config_Settings__c configOrders = new Config_Settings__c();
        configOrders.Name = CustomSettingsManager.WS_Endpoint_ViewCustomerOrdersV2;
        configOrders.Text_Value__c = 'ViewCustomerOrdersV2';
        insert configOrders;

        // get the page
        PageReference customerOrdersPage = Page.coCustomerOrders;
        Test.setCurrentPage(customerOrdersPage);

        // insert a contact to hang the page off
        Contact searchContact = new Contact();
        searchContact.FirstName = 'Steve';
        searchContact.LastName = 'Loftus';
        searchContact.Email = 'test@nowhere.com';
        searchContact.MailingPostalCode = 'HX7 6DJ';
        insert searchContact;

        system.debug('searchContact [' + searchContact + ']');

        // create the standard and extension controllers
        ApexPages.StandardController std = new ApexPages.StandardController(searchContact);
        coCustomerOrdersController customerOrdersController = new coCustomerOrdersController(std);

        Test.setMock(HttpCalloutMock.class, new coSoapGetOrderHeaders_MOCK());

        system.debug('contact [' + customerOrdersController.Con + ']');

        Test.startTest();
        customerOrdersController.getOrders();
        //customerOrdersController.showMore();
        Test.stopTest();        

        //system.assertNotEquals(0, customerOrdersController.OrderHeaders.Size(), 'No order headers have been found');
    }

    public static testmethod void getOrderHeadersWithoutOrderNumberOrderError() {

        // set up the config settings
        Config_Settings__c configSearch = new Config_Settings__c();
        configSearch.Name = CustomSettingsManager.WS_Endpoint_SearchCustomer;
        configSearch.Text_Value__c = 'SearchCustomer';
        insert configSearch;

        Config_Settings__c configOrders = new Config_Settings__c();
        configOrders.Name = CustomSettingsManager.WS_Endpoint_ViewCustomerOrdersV2;
        configOrders.Text_Value__c = 'ViewCustomerOrdersV2Error';
        insert configOrders;

        // get the page
        PageReference customerOrdersPage = Page.coCustomerOrders;
        Test.setCurrentPage(customerOrdersPage);

        // insert a contact to hang the page off
        Contact searchContact = new Contact();
        searchContact.FirstName = 'Steve';
        searchContact.LastName = 'Loftus';
        searchContact.Email = 'test@nowhere.com';
        searchContact.MailingPostalCode = 'HX7 6DJ';
        insert searchContact;

        system.debug('searchContact [' + searchContact + ']');

        // create the standard and extension controllers
        ApexPages.StandardController std = new ApexPages.StandardController(searchContact);
        coCustomerOrdersController customerOrdersController = new coCustomerOrdersController(std);

        Test.setMock(HttpCalloutMock.class, new coSoapGetOrderHeaders_MOCK());

        system.debug('contact [' + customerOrdersController.Con + ']');

        Test.startTest();
        customerOrdersController.getOrders();
        Test.stopTest();

        //system.assertNotEquals(0, customerOrdersController.OrderHeaders.Size(), 'No order headers have been found');
    }

    public static testmethod void getOrderHeadersWithoutOrderNumberCustomerError() {

        // set up the config settings
        Config_Settings__c configSearch = new Config_Settings__c();
        configSearch.Name = CustomSettingsManager.WS_Endpoint_SearchCustomer;
        configSearch.Text_Value__c = 'SearchCustomerError';
        insert configSearch;

        Config_Settings__c configOrders = new Config_Settings__c();
        configOrders.Name = CustomSettingsManager.WS_Endpoint_ViewCustomerOrdersV2;
        configOrders.Text_Value__c = 'ViewCustomerOrdersV2';
        insert configOrders;

        // get the page
        PageReference customerOrdersPage = Page.coCustomerOrders;
        Test.setCurrentPage(customerOrdersPage);

        // insert a contact to hang the page off
        Contact searchContact = new Contact();
        searchContact.FirstName = 'Steve';
        searchContact.LastName = 'Loftus';
        searchContact.Email = 'test@nowhere.com';
        searchContact.MailingPostalCode = 'HX7 6DJ';
        insert searchContact;

        system.debug('searchContact [' + searchContact + ']');

        // create the standard and extension controllers
        ApexPages.StandardController std = new ApexPages.StandardController(searchContact);
        coCustomerOrdersController customerOrdersController = new coCustomerOrdersController(std);

        Test.setMock(HttpCalloutMock.class, new coSoapGetOrderHeaders_MOCK());

        system.debug('contact [' + customerOrdersController.Con + ']');

        Test.startTest();
        customerOrdersController.getOrders();
        Test.stopTest();

        //system.assertNotEquals(0, customerOrdersController.OrderHeaders.Size(), 'No order headers have been found');
    }

    public static testmethod void getOrderHeadersWithOrderNumber() {

        Config_Settings__c configSearch = new Config_Settings__c();
        configSearch.Name = CustomSettingsManager.WS_Endpoint_SearchCustomer;
        configSearch.Text_Value__c = 'SearchCustomer';
        insert configSearch;

        // set up the config settings
        Config_Settings__c configOrders = new Config_Settings__c();
        configOrders.Name = CustomSettingsManager.WS_Endpoint_ViewCustomerOrdersV1;
        configOrders.Text_Value__c = 'ViewCustomerOrdersV1';
        insert configOrders;

        // get the page
        PageReference customerOrdersPage = Page.coCustomerOrders;
        Test.setCurrentPage(customerOrdersPage);

        // insert a contact to hang the page off
        Contact searchContact = new Contact();
        searchContact.FirstName = 'Steve';
        searchContact.LastName = 'Loftus';
        searchContact.Email = 'test@nowhere.com';
        searchContact.MailingPostalCode = 'HX7 6DJ';
        insert searchContact;

        // create the standard and extension controllers
        ApexPages.StandardController std = new ApexPages.StandardController(searchContact);
        coCustomerOrdersController customerOrdersController = new coCustomerOrdersController(std);

        Test.setMock(HttpCalloutMock.class, new coSoapGetOrderHeaders_MOCK());

        system.debug('contact [' + customerOrdersController.Con + ']');

        Test.startTest();
        customerOrdersController.searchOrderNumber = '12345678';
        customerOrdersController.getOrders();
        Test.stopTest();

        //system.assertEquals(1, customerOrdersController.OrderHeaders.Size(), 'No order headers have been found');
    }

    public static testmethod void getOrderHeadersNoSearchCriteria() {

        // get the page
        PageReference customerOrdersPage = Page.coCustomerOrders;
        Test.setCurrentPage(customerOrdersPage);

        // insert a contact to hang the page off
        Contact searchContact = new Contact();
        searchContact.FirstName = 'Steve';
        searchContact.LastName = 'Loftus';
        searchContact.Email = 'test@nowhere.com';
        searchContact.MailingPostalCode = 'HX7 6DJ';
        insert searchContact;

        // create the standard and extension controllers
        ApexPages.StandardController std = new ApexPages.StandardController(searchContact);
        coCustomerOrdersController customerOrdersController = new coCustomerOrdersController(std);

        // blank all the search criteria
        customerOrdersController.SearchEmail = '';
        customerOrdersController.SearchPostcode = '';
        customerOrdersController.SearchFirstName = '';
        customerOrdersController.SearchLastName = '';
        customerOrdersController.SearchOrderNumber = '';

        Test.startTest();
        customerOrdersController.getOrders();
        Test.stopTest();        

        system.assertNotEquals(0, ApexPages.getMessages().size(), 'No Error messages have been found');
    }

    public static testmethod void getOrderHeadersNoFirstOrLastName() {

        // get the page
        PageReference customerOrdersPage = Page.coCustomerOrders;
        Test.setCurrentPage(customerOrdersPage);

        // insert a contact to hang the page off
        Contact searchContact = new Contact();
        searchContact.FirstName = 'Steve';
        searchContact.LastName = 'Loftus';
        searchContact.Email = 'test@nowhere.com';
        searchContact.MailingPostalCode = 'HX7 6DJ';
        insert searchContact;

    // create the standard and extension controllers
        ApexPages.StandardController std = new ApexPages.StandardController(searchContact);
        coCustomerOrdersController customerOrdersController = new coCustomerOrdersController(std);

        // blank all the search criteria
        customerOrdersController.SearchFirstName = '';
        customerOrdersController.SearchLastName = '';
        customerOrdersController.SearchOrderNumber = '';

            Test.startTest();
        customerOrdersController.getOrders();
        Test.stopTest();        

        system.assertNotEquals(0, ApexPages.getMessages().size(), 'No Error messages have been found');
    }

    public static testmethod void getOrderHeadersNoEmailOrPostcode() {

        // get the page
        PageReference customerOrdersPage = Page.coCustomerOrders;
        Test.setCurrentPage(customerOrdersPage);

        // insert a contact to hang the page off
        Contact searchContact = new Contact();
        searchContact.FirstName = 'Steve';
        searchContact.LastName = 'Loftus';
        searchContact.Email = 'test@nowhere.com';
        searchContact.MailingPostalCode = 'HX7 6DJ';
        insert searchContact;

        // create the standard and extension controllers
        ApexPages.StandardController std = new ApexPages.StandardController(searchContact);
        coCustomerOrdersController customerOrdersController = new coCustomerOrdersController(std);

        // blank all the search criteria
        customerOrdersController.SearchEmail = '';
        customerOrdersController.SearchPostcode = '';
        customerOrdersController.SearchOrderNumber = '';

        Test.startTest();
        customerOrdersController.getOrders();
        Test.stopTest();        

        system.assertNotEquals(0, ApexPages.getMessages().size(), 'No Error messages have been found');
    }
}