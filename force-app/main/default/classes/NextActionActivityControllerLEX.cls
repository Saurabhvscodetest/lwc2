public with sharing class NextActionActivityControllerLEX {
 
    @AuraEnabled
    public static Map<String, Boolean> checkIfNKUCase(String caseId){
       Map<String, boolean> returnedMap = new Map<String, boolean>();
        Case currentCase = [SELECT Id, RecordTypeId
                            FROM Case WHERE Id = : caseId LIMIT 1];
        returnedMap.put('NKU', false);
        returnedMap.put('PSE', false);
        if(currentCase.RecordTypeId == CommonStaticUtils.getRecordTypeId('Case', 'NKU')){
            returnedMap.put('NKU', true);            
        }else if(currentCase.RecordTypeId == CommonStaticUtils.getRecordTypeId('Case', 'Product Stock Enquiry')){
            returnedMap.put('PSE', true);
        }
        return returnedMap;
    }
    
    /* COPT-5415
     @AuraEnabled
    public static boolean checkPSETransfer(String caseActNewId){
         
        try{
        List<Case> cList = [Select Recordtype.Name from case where id =: caseActNewId];
        if(cList[0].Recordtype.Name == 'Product Stock Enquiry'){
            return true;
        }else{
            return false;
        }
        }
    catch(Exception e){
        return false;
        }
        }
    COPT-5415 */
    
    @AuraEnabled
    public static String checkCaseActivityValidations(String activityDate, String activityStartTime, String activityEndTime, String caseId, DateTime nextActivityDueDateTime, Boolean transferCase, Boolean createViaCaseActivityTimeline){
        String errorMessageForCaseActivityDuration = 'Activity end time must be set to at least 4 business hour from now. Please amend the Activity Date or Time fields accordingly.';
        String noErrors = 'No errors';
        
        //Date caseActivityDateConverted  = QueueCapacityUtils.getFormattedDate ( activityDate );
        System.debug ( ' activityDate ' + activityDate);
        Date caseActivityDateConverted = Date.valueOf(activityDate);
        DateTime caseActivityStartDateTime = CaseActivityUtils.createDateTime(caseActivityDateConverted, activityStartTime);
        
        DateTime caseActivityEndDateTime = CaseActivityUtils.createDateTime(caseActivityDateConverted, activityEndTime);
        Integer minutesBetweenEndDateAndCurrentDate = DatetimeUtilities.getMinutesBetween(Datetime.now() > caseActivityStartDateTime ? Datetime.now() : caseActivityStartDateTime,caseActivityEndDateTime);
        try{
            Id caseBusinessHourId = caseId != null ? [select BusinessHoursId from Case where Id = :caseId].get(0).BusinessHoursId : [select Id from BusinessHours where Name = 'Default'].get(0).Id;
            /*if((BusinessHours.diff(caseBusinessHourId, Datetime.now() > caseActivityStartDateTime ? Datetime.now() : caseActivityStartDateTime,caseActivityEndDateTime)/1000/60).intValue()<120){
                return 'Selected time slot should have at least 2 business hours to complete. Please amend the activity time or date accordingly.';
            }*/
            if(minutesBetweenEndDateAndCurrentDate < 239){
                return errorMessageForCaseActivityDuration;
            }
            if(transferCase && nextActivityDueDateTime < DateTime.now() && !createViaCaseActivityTimeline){
                return 'Assignment cannot be updated if a Case Activity is overdue. Please update activity before reassigning.';
            }
        }
        catch(exception e){
            system.debug('ERROR : '+e.getMessage());
        }
        return noErrors;
    }
    
    @AuraEnabled
    public static string saveCaseActivity(String caseActivityJSON, string initialQueName){
        system.debug('Block3'+caseActivityJSON);
        Case_Activity__c caseActivityObject = (Case_Activity__c)JSON.deserialize(caseActivityJSON, Case_Activity__c.class);
        string strMsg = 'No error'; 
        try{
            list<Case_Activity__c> lstUpsertCaseAct = new list<Case_Activity__c>();
            lstUpsertCaseAct.add(caseActivityObject);
            upsert lstUpsertCaseAct;
            
            /* COPT-5415 TB Removed */
            /*list<Case> lstParentCase = [select id,ownerid,jl_Branch_master__c,Contact_Reason__c,Reason_Detail__c,CDH_Site__c,Branch_Department__c FROM case WHERE id =:caseActivityObject.Case__c];
            if(lstParentCase != null && lstParentCase.size() > 0)
            {
                caseActivityObject.jl_Branch_master__c = lstParentCase[0].jl_Branch_master__c;
                caseActivityObject.Case_Branch_Department__c = lstParentCase[0].Branch_Department__c;
                caseActivityObject.Case_Contact_Reason__c= lstParentCase[0].Contact_Reason__c;
                caseActivityObject.Case_Reason_Detail__c = lstParentCase[0].Reason_Detail__c;
                caseActivityObject.CDH_Site__c = lstParentCase[0].CDH_Site__c;
                caseActivityObject.Skip_NextActionIfOpen_Validation__c = true;
                if(caseActivityObject.Transfer_Case__c == true && caseActivityObject.Slot_Queue_Name__c != null && caseActivityObject.Slot_Queue_Name__c != '')
                {
                    list<Group> lstGroup = [ SELECT Id,Name,DeveloperName FROM  Group WHERE Type =: QueueCapacityConstants.QUEUE_STRING_VALUE and DeveloperName =:caseActivityObject.Slot_Queue_Name__c];
                    if(lstGroup != null && lstGroup.size() > 0)
                    {
                        lstParentCase[0].ownerid = lstGroup[0].Id;
                        update lstParentCase[0];
                    }
                }
            }*/
            
            //Start: Added InitialCQueueValue attribute for transfer case functionality. Rajendra 07 Aug 2019
               /*if(initialQueName != null && initialQueName != '' && caseActivityObject.Transfer_Case__c == true 
                  && caseActivityObject.Slot_Queue_Name__c != null && caseActivityObject.Slot_Queue_Name__c != ''
                 && caseActivityObject.Slot_Queue_Name__c != initialQueName)
               {
                   list<Case_Activity__c> lstCaseAct = [select id,Case__c,Status__c from Case_Activity__c where Case__c = :caseActivityObject.Case__c];
                   if(lstCaseAct != null && lstCaseAct.size() > 0)
                   {
                       for(Case_Activity__c caseact:lstCaseAct)
                       {
                           caseact.Status__c = 'Resolved';//Cancelled,New
                           caseact.Resolution_Method__c = 'No action required';
                           caseact.Activity_Completed_Date_Time__c = datetime.now();
                           //caseact.Transfer_Case__c = true;
                           caseact.Completed_By_V1__c = userinfo.getUserId();
                           lstUpsertCaseAct.add(caseact);
                       }
                       //update lstCaseAct;
                   }
               }
            */
               //End
            //insert caseActivityObject;
               //updatecaseActivity(caseActivityObject.id);
            //upsert lstUpsertCaseAct;
            /* COPT-5415 TB Removed */

            system.debug('@@@ caseActivityObject' + caseActivityObject);
        }catch(Exception e){
            strMsg = 'An error has occured when inserting new case activity object : '+e.getMessage();
            system.debug('An error has occured when inserting new case activity object : '+e.getMessage());
        }
        return strMsg;
    }
    
    @AuraEnabled
    public static String fetchCurrentCaseQueueForCaseOwner( String caseRecordId )
    {
        string queueNameValue = '';//'Queue Not Available';
        Case currentCase = [SELECT Id, RecordTypeId, OwnerId, Contact_Reason__c, Reason_Detail__c
                            FROM Case 
                            WHERE Id = : caseRecordId LIMIT 1];
        string ownerid = currentCase.OwnerId;
        
        if(ownerid.startswith('005'))
        {
            for ( User currentUser : [ SELECT Id, Queues__c FROM User WHERE Id =:ownerid] ) {
                if ( currentUser.Queues__c != null ) {
                    List<String> qname = currentUser.Queues__c.split(',');
                    queueNameValue = qname[0];
                }                    
            }
        }
        return queueNameValue;
    }
    
    @AuraEnabled
    public static String fetchCurrentCaseQueue( String caseRecordId ){
        String caseQueueName = 'Queue Not Available';
        
        caseQueueName = QueueCapacityUtils.fetchCaseQueueName ( caseRecordId );
        System.debug ( ' caseQueueName '  + caseQueueName );
        
        return caseQueueName;
    }
    
    /* COPT-5415 TB Removed 
     public static void updatecaseActivity(string caseActivitydata){
         case_Activity__c caseActivity = new case_Activity__c();
        for(case_Activity__c updatecase : [select id,case__r.jl_Branch_master__c,Skip_NextActionIfOpen_Validation__c,case__r.Contact_Reason__c,case__r.Reason_Detail__c,case__r.CDH_Site__c,case__r.Branch_Department__c FROM case_Activity__c WHERE id =:caseActivitydata]){
           caseActivity.Id = updatecase.id;
            caseActivity.jl_Branch_master__c = updatecase.case__r.jl_Branch_master__c;
            caseActivity.Case_Branch_Department__c = updatecase.case__r.Branch_Department__c;
            caseActivity.Case_Contact_Reason__c= updatecase.case__r.Contact_Reason__c;
            caseActivity.Case_Reason_Detail__c = updatecase.case__r.Reason_Detail__c;
            caseActivity.CDH_Site__c = updatecase.case__r.CDH_Site__c;
            caseActivity.Skip_NextActionIfOpen_Validation__c = true;
         }
         update caseActivity;
    }
    COPT-5415 TB Removed */

    @AuraEnabled
    public static Map<String, SObject> fetchCaseAssignmentDetails(String caseId){
        Map<String, SObject> returnMap = new Map<String, SObject>();
        returnMap.put('CaseAssignment',[select Id, Assign_To_New_Branch__c, jl_Branch_master__c,Contact_Reason__c,Reason_Detail__c,CDH_Site__c,Branch_Department__c,Next_Case_Activity_Due_Date_Time__c from Case where Id = :caseId LIMIT 1].get(0));
        returnMap.put('CurrentUserDetail',CommonStaticUtils.getCurrentUser());
        System.debug('returnMapreturnMapreturnMapreturnMap'+returnMap);
        return returnMap;
    }

    @AuraEnabled
    public static Boolean caseRestrictTransferPencil(String caseActivityNewId) {
        
        List<Case_Activity__c> caseActList = [ SELECT Case__c, Activity_End_Date_Time__c FROM Case_Activity__c WHERE Id =: caseActivityNewId ];
        DateTime currentActivityEndTime = caseActList[0].Activity_End_Date_Time__c;
        Id caseId = caseActList[0].Case__c;
        
        List<Case_Activity__c> pastCaseActivityList = NEW List<Case_Activity__c>();
        List<Case_Activity__c> futureCaseActivityList = NEW List<Case_Activity__c>();   
        List<Case_Activity__c> caseActivityPastFutureList = [ SELECT Id, Activity_End_Date_Time__c FROM Case_Activity__c WHERE Case__c =: caseId AND Activity_Completed_Date_Time__c = NULL];
        
        for(Case_Activity__c ca : caseActivityPastFutureList) {
            if(ca.Activity_End_Date_Time__c < System.now()) {
                pastCaseActivityList.add(ca);
            }
            if(ca.Activity_End_Date_Time__c > System.now()) {
                futureCaseActivityList.add(ca);
            }
        }
        
        /*start COPT-5287
        List<Case> caseList = [ SELECT RecordType.Name FROM Case WHERE Id =: caseId ];
        String pseRecordTypeName = caseList[0].RecordType.Name;
        
        if(pseRecordTypeName == 'Product Stock Enquiry') {
            return True;
        }
        else end */

        if(pastCaseActivityList.size()==1 && futureCaseActivityList.size()==1 && currentActivityEndTime>System.now()) {
            return True;
        }
        else if(pastCaseActivityList.size()==1 && futureCaseActivityList.size()>1 && currentActivityEndTime>System.now()) {
            return True;
        }
        else if(pastCaseActivityList.size()>1 && futureCaseActivityList.size()>1) {
            return True;
        }
        else if(pastCaseActivityList.size()>1 && futureCaseActivityList.size()==1) {
            return True;
        }
        else if(pastCaseActivityList.size()>1) {
            return True;
        }
        else {            
            return False;
        }
    }
    
    @AuraEnabled
    public static List<String> fetchAvailableSlots( String queueName, String caseRecordId ) {
        System.debug ('Queue Name ' + queueName );
        System.debug ('Case Record Id '+ caseRecordId );
        if ( caseRecordId != 'New Case' && queueName == 'Default') {
            queueName = QueueCapacityUtils.fetchCaseQueueName ( caseRecordId );
        }
        Map < String, Map < String, Integer > > availableQueueSlotsMap = QueueCapacityUtils.fetchAllAvailableQueueSlots ( queueName );
        List < String > currentDateList = new List<String> ( availableQueueSlotsMap.keySet());
        System.debug ('Before sorted list ' + currentDateList );
                 Integer i =0;
                 for ( String dateString : currentDateList ) {
                 
                    
                    for(integer j = i+1 ; j < = currentDateList.size() -1 ; j ++ ){
                      String  tempString ='' ;
                      
                      Integer firstDay = Integer.valueOf ( currentDateList[i].split ('-')[0] );
                      Integer firstMonth = Integer.valueOf ( currentDateList[i].split ('-')[1] );
                      Integer firstYear = Integer.valueOf ( currentDateList[i].split ('-')[2] );
                      
                      Integer nextDay = Integer.valueOf ( currentDateList[j].split ('-')[0] );
                      Integer nextMonth = Integer.valueOf ( currentDateList[j].split ('-')[1] );
                      Integer nextYear = Integer.valueOf ( currentDateList[j].split ('-')[2] );
                      
                      
                      if( Datetime.newInstance(firstYear, firstMonth, firstDay, 0, 0, 0) >  Datetime.newInstance(nextYear, nextMonth, nextDay , 0, 0, 0) )  {
                          tempString = currentDateList[i]  ;
                          currentDateList[i] = currentDateList[j]  ;
                          currentDateList[j]  = tempString;
                      }
                    }
                 }
                    
                
        System.debug (' sorted list ' + currentDateList);
        return currentDateList;          
    }
  // New method sf1 Date issue for PSE Case COPT-5885

    @AuraEnabled
    public static List<List<String>> fetchAvailableSlotsDateTime( String queueName, String caseRecordId )
    {
        List<List <String >> retList = new List<List<String>>();
        List <String > dateList = fetchAvailableSlots(queueName,caseRecordId );
        //COPT-6008- Calling recursive method when time slot is empty for the date.
        if(dateList.size()>0)
        {
            retList=fetchSlotsDateTimeBasedOnDateList(dateList,queueName,caseRecordId);
        }
        
        return retList;
    }
 //  
        @AuraEnabled
    public static List<String> fetchSlotsBasedOnDate ( String queueName, String currentDate, String caseRecordId ) {
        System.debug ( ' currentDate' + currentDate );
         if ( caseRecordId != 'New Case' && queueName == 'Default')  {
            queueName = QueueCapacityUtils.fetchCaseQueueName ( caseRecordId );
        }
        List<String> availableSlotList = new List<String>();
        Map < String, List < String > > availableQueueSlotsMap = QueueCapacityUtils.fetchDateToSlotMap ( queueName );
        if ( availableQueueSlotsMap.containsKey ( currentDate ) ) {
             availableSlotList = availableQueueSlotsMap.get ( currentDate );
        }       
        System.debug ('availableSlotList' + availableSlotList);
        return availableSlotList;
    }
    
    
    //COPT-6008-Method returns available date and time slots based on queue name and case recordId
    @AuraEnabled
    public static List<List<String>> fetchSlotsDateTimeBasedOnDateList(List<String> availableDates,String queueName,String caseRecordId){
        List<List <String >> retList = new List<List<String>>();
        
        if(!availableDates.isEmpty()){
            List <String > timeSlotList = new List <String >();
            timeSlotList = fetchSlotsBasedOnDate ( queueName, availableDates[0], caseRecordId );
            if(timeSlotList.isEmpty()){
                availableDates.remove(0);
                if(!availableDates.isEmpty()){
                    return fetchSlotsDateTimeBasedOnDateList(availableDates,queueName,caseRecordId);
                }
                else{
                    return retList;
                }
            }
            retList.add(timeSlotList);
            retList.add(availableDates);
        }
        return retList;
    }
}