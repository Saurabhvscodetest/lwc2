/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class jl_trigger_ContactProfile_Test {

	static {
		BaseTriggerHandler.addSkipReason(CalloutHandler.SKIP_ALL_NOTIFICATIONS_CODE, 'disable outbound events in this test class');
	}
	
    public static TestMethod void LoyaltyAccountEmailUpdate_Test(){
        
       
       /*--- Insert Contact ---*/
        Contact cont = new Contact();
        cont.FirstName = 'JL';
        cont.LastName = '.com';
        
        Insert cont;
        system.debug('*****contact id : ' + cont.id);
        /*--- Insert Contact Profile---*/
        Contact_Profile__c cp = new Contact_Profile__c ();
        cp.Name = 'test CpInsert';
        cp.Email__c = 'testamit123@test.com';
        cp.SourceSystem__c = MyJL_Util.SOURCE_SYSTEM;
        cp.Contact__c = cont.Id;
        cp.Shopper_ID__c='testAMIT123';
        
        Insert cp;
        system.debug('*****contact profile id : ' + cp.id);
        
        /*--- Insert Loyalty Accounts---*/
        Loyalty_Account__c la1 = new Loyalty_Account__c();
        la1.Name = 'Membership123';
        la1.Customer_Profile__c = cp.Id;
        la1.Email_Address__c = 'testLoyaltyAccount1@test.com';
        
        Insert la1;
        system.debug('*****loyalty account id 1 : ' + la1.id);
        
        Loyalty_Account__c la2 = new Loyalty_Account__c();
        la2.Name = 'Membership987';
        la2.Customer_Profile__c = cp.Id;
        la2.Email_Address__c = 'testLoyaltyAccount2@test.com';
        
        insert la2;
        system.debug('*****loyalty account id 2 : ' + la2.id);
        
        //Asserting the Loyalty Account email address before to contact profile email address update
        system.assertEquals(la1.Email_Address__c, 'testLoyaltyAccount1@test.com');
        system.assertEquals(la2.Email_Address__c, 'testLoyaltyAccount2@test.com');
        
        //Negative Case to test Contact profile Email address should not match loyalty account email address after insert.
        system.assertNotEquals(la1.Email_Address__c,cp.Email__c);
        system.assertNotEquals(la2.Email_Address__c,cp.Email__c);
        
        //updating email Contact Profile email address
        cp.Email__c = 'testemailIdChanges@test.com';
        Test.startTest();
        update cp;
        Test.stopTest();
        
        Loyalty_Account__c la1test = [select Email_Address__c from Loyalty_Account__c where id =: la1.id];
        Loyalty_Account__c la2test = [select Email_Address__c from Loyalty_Account__c where id =: la2.id];
       
        string contactProfileEmail = cp.Email__c;
        contactProfileEmail = contactProfileEmail.toLowerCase();
        
        string loyaltyAccountEmailaddress1 = la1test.Email_Address__c;
        loyaltyAccountEmailaddress1 = loyaltyAccountEmailaddress1.toLowerCase();
        
        string loyaltyAccountEmailaddress2 = la2test.Email_Address__c;
        loyaltyAccountEmailaddress2 = loyaltyAccountEmailaddress2.toLowerCase();
        
        //Asserting the Loyalty Account email address after contact profile email address update.
        system.assertEquals(loyaltyAccountEmailaddress1,contactProfileEmail);
        system.assertEquals(loyaltyAccountEmailaddress2,contactProfileEmail);
        
    }
    
}