//  Edit    Date        Author      Comment
//  001     09/10/14    NTJ         Initial version
//
@isTest private class ScheduleCopyAttachments_TEST {

	@testSetup static void setup() {
		 UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
	}
	

	// CRON expression: midnight on March 15.
	// Because this is a test, job executes
	// immediately after Test.stopTest().
	public static String CRON_EXP = '0 0 0 15 3 ? 2022';
   
	private static final String TEXT_WITH_REF = 'This is an auto-generated email, please do not reply ref:_00Dm011SJ._500m01dzel:ref mary had a little lamb';
	private static final String TEXT_NO_REF = 'Mary had a little lamb';
	private static final String HTML_WITH_REF = '<html><head/><body>'+TEXT_WITH_REF+'</body></html>';
	private static final String HTML_NO_REF = '<html><head/><body>'+TEXT_NO_REF+'</body></html>';
	private static final String TO_ADDRESS = 'terry453@terry453.com.tv';

	static Case createCase() {
	Account acc = UnitTestDataFactory.createAccount('name');
	insert acc;
	Contact con = UnitTestDataFactory.createContact(acc);
	insert con;
	Case c = UnitTestDataFactory.createNormalCase(con);
	c.jl_NumberOutstandingTasks__c = 2;
	
	insert c;
	system.assertNotEquals(null, c.Id);  
	return c;
  }
   
   static EmailMessage createEmailMessage(Case c, Boolean incoming, String textBody, String htmlBody, String toAddress) {
	EmailMessage em = new EmailMessage();
	em.ParentId = c.Id;
	em.Incoming = incoming;
	em.ToAddress = toAddress;
	em.Status = '3';
	if (textBody != null) {
	  em.TextBody = textBody;
	}
	if (htmlBody != null) {
	  em.htmlBody = htmlBody;
	}
	insert em;
	system.assertNotEquals(null, em.Id);
	return em;
  }

  static testmethod void test() {
	  Test.startTest();

	  // Schedule the test job
	  Case c = createCase();
	  Set<Id> allCases = new Set<Id>();
	  EmailMessage ems = createEmailMessage(c,false,TEXT_WITH_REF,HTML_WITH_REF,TO_ADDRESS);
	  String emid = ems.id;
	  c.Pending_Email_Messages__c =emid;
	  update c;
	  allCases.add(c.Id);
	  String jobId = System.schedule('ScheduleCopyAttachments', CRON_EXP, new ScheduleCopyAttachments(allCases));
		 
	  // Get the information from the CronTrigger API object
	  CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
		 NextFireTime
		 FROM CronTrigger WHERE id = :jobId];

	  // Verify the expressions are the same
	  System.assertEquals(CRON_EXP, ct.CronExpression);

	  // Verify the job has not run
	  System.assertEquals(0, ct.TimesTriggered);

	  // Verify the next time the job will run
	  System.assertEquals('2022-03-15 00:00:00', String.valueOf(ct.NextFireTime));
	  // Verify the scheduled job hasn't run yet.
	  //Merchandise__c[] ml = [SELECT Id FROM Merchandise__c WHERE Name = 'Scheduled Job Item'];
	  //System.assertEquals(ml.size(),0);
	  Test.stopTest();

	  // Now that the scheduled job has executed after Test.stopTest(),
	  //   fetch the new merchandise that got added.
	  //ml = [SELECT Id FROM Merchandise__c WHERE Name = 'Scheduled Job Item'];
	  //System.assertEquals(ml.size(), 1);

   }
}