/* Description  : Delete LiveChat Visitor records which don't have Livechat Transcript records associated with it.
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   12/02/2019        Vignesh Kotha                   Created                COPT-4425  
*
*/
@isTest
Public class BAT_DeleteEmailMessagesTest{
    static testmethod void testDeleteEmailMessages(){        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        // Running user Context
        System.runAs(runningUser){
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;       
            insert CustomSettingTestDataFactory.createConfigSettings();
        }        
        System.runAs(testUser){            
            contact testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'vignesh@gmail.com';
            insert testContact;            
            case testCase = CaseTestDataFactory.createCase(testContact.Id);
            testCase.Bypass_Standard_Assignment_Rules__c = true;
            insert testCase;  
            GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
            gDPR.GDPR_Class_Name__c = 'BAT_DeleteEmailMessages';
            gDPR.Name = 'BAT_DeleteEmailMessages';
            gDPR.Record_Limit__c = '5000';
            Test.startTest();
            List<Task> Tasklist=new  List<Task>();
            for(integer i=0; i<99; i++){
                Tasklist.add(new Task(CreatedDate=System.now().addMonths(-18),whoid=testContact.id,Subject='Email:Test'));
            }       
            insert Tasklist;
            insert gDPR;
            String data = Tasklist[0].whatid;
            Database.BatchableContext BC;
            BAT_DeleteEmailMessages obj=new BAT_DeleteEmailMessages();
            Database.DeleteResult[] Delete_Result = Database.delete(Tasklist, false);
            obj.start(BC);
            obj.execute(BC,Tasklist);
            obj.finish(BC);
            DmlException expectedException;
            Test.stopTest();
        }  
    }
}