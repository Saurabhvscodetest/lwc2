@isTest
global class MyJL_LoyaltyAccountHandlerTest  {
    private static final String SHOPPER_ID = 'shopper1';
    
    private static final String PARTNERSHIP_CARD_NUMBER = '3456789456123';
    private static final String ORIGIN = 'Unit Test';
    private static final DateTime DATE_CREATED = DateTime.NOW();    
    private static String SOURCE_SYSTEM = System.Label.MyJL_JL_comSourceSystem;
    
    global class JoinLeaveNotificationMock implements WebServiceMock { 
        global void doInvoke( Object stub, Object request, Map<String, Object> response, String endpoint,
                             String soapAction, String requestName, String responseNS, String responseName, String responseType) {
                                 
                                 SFDCMyJLCustomerTypes.PublishCustomerEventResponse responseElement = new SFDCMyJLCustomerTypes.PublishCustomerEventResponse();
                                 response.put('response_x', responseElement);
                             } 
    }
    
    /**
* Here we are simulating Partial Join Event
*/
    static testMethod void testPartialJoin () {
        // temp out
        //return;
        
        BaseTriggerHandler.addSkipReason('JoinNotification', 'disabled during test data preparation');
        
        Loyalty_Account__c account = new Loyalty_Account__c(IsActive__c = false,activation_date_time__c= system.now(), Voucher_Preference__c = 'Digital', Membership_Number__c='152152153');
        Database.insert(account);
        
        Loyalty_Card__c card = new Loyalty_Card__c(Loyalty_Account__c = account.Id, Name = 'test-card-id');
        Database.insert(card);
        
        account.IsActive__c = true;
        Test.setMock(WebServiceMock.class, new JoinLeaveNotificationMock());
        CalloutHandlerTest.initCalloutSettings(MyJL_LoyaltyAccountHandler.PARTIAL_JOIN_NOTIFY, 'MyJL_LoyaltyAccountHandler.JoinLeaveCallout', 10);
        Test.startTest();
        BaseTriggerHandler.removeSkipReason('JoinNotification');
        //this is just to fire @future method
        Database.update(account);
        Test.stopTest();
        
        //check that outbound notification log record has been inserted
        System.assertEquals(1, [select count() from Log_Header__c where Service__c = :MyJL_LoyaltyAccountHandler.PARTIAL_JOIN_NOTIFY ], 
                            'Expected log record for outbound notification we have just done');
        //
        Map<String, List<SFDCMyJLCustomerTypes.Customer>> customersByEventType = MyJL_LoyaltyAccountHandler.getCustomersByCalloutType(new Set<Id>{account.Id});
        System.assert(customersByEventType.containsKey(MyJL_LoyaltyAccountHandler.PARTIAL_JOIN_NOTIFY), 'Expected Partial Join event type to be returned');
        
    }
    
    
    /**
* Here we are simulating Full Join Event
*/
    static testMethod void testFullJoin () {
        // temp out
        //return;
        
        CalloutHandlerTest.initCalloutSettings(MyJL_LoyaltyAccountHandler.PARTIAL_JOIN_NOTIFY, 'MyJL_LoyaltyAccountHandler.JoinLeaveCallout', 10);
        CustomSettings.setTestMyJLSetting(UserInfo.getUserId(), new CustomSettings.TestRecord(new Map<String, Object>{'Send_My_JL_Accounts_without_email__c' => true}));
        
        BaseTriggerHandler.addSkipReason('JoinNotification', 'disabled during test data preparation');
        Contact contact = new Contact(LastName = 'Test');
        Database.insert(contact);
        
        Contact_Profile__c profile = new Contact_Profile__c(Contact__c = contact.Id, SourceSystem__c = SOURCE_SYSTEM, Shopper_Id__c='shopper1');
        Database.insert(profile);
        
        Loyalty_Account__c account = new Loyalty_Account__c(IsActive__c = false, Customer_Profile__c = profile.Id, ShopperId__c='shopper1',Voucher_Preference__c = 'Digital', activation_date_time__c= system.now());
        Database.insert(account);
        Set<Id> accounts = new Set<Id>();
        accounts.add(account.Id);
        
        Loyalty_Card__c card = new Loyalty_Card__c(Loyalty_Account__c = account.Id, Name = 'test-card-id');
        Database.insert(card);
        
        account.IsActive__c = true;
        Test.setMock(WebServiceMock.class, new JoinLeaveNotificationMock());
        CalloutHandlerTest.initCalloutSettings(MyJL_LoyaltyAccountHandler.FULL_JOIN_NOTIFY, 'MyJL_LoyaltyAccountHandler.JoinLeaveCallout', 10);
        Test.startTest();
        BaseTriggerHandler.removeSkipReason('JoinNotification');
        //this is just to fire @future method
        Database.update(account);
        //myJL_LoyaltyAccountHandler.sendJoinOrLeaveNotification(accounts);
        Test.stopTest();
        
        //check that outbound notification log record has been inserted
        System.assertEquals(1, [select count() from Log_Header__c where Service__c = :MyJL_LoyaltyAccountHandler.FULL_JOIN_NOTIFY ], 
                            'Expected log record for outbound notification we have just done');
        
        //
        Map<String, List<SFDCMyJLCustomerTypes.Customer>> customersByEventType = MyJL_LoyaltyAccountHandler.getCustomersByCalloutType(new Set<Id>{account.Id});
        System.assert(customersByEventType.containsKey(MyJL_LoyaltyAccountHandler.FULL_JOIN_NOTIFY), 'Expected Full Join event type to be returned');
    }
    
    static testMethod void testLeave () {
        // temp out
        // return;
        CalloutHandlerTest.initCalloutSettings(MyJL_LoyaltyAccountHandler.PARTIAL_JOIN_NOTIFY, 'MyJL_LoyaltyAccountHandler.JoinLeaveCallout', 10);
        
        BaseTriggerHandler.addSkipReason('JoinNotification', 'disabled during test data preparation');
        Contact contact = new Contact(LastName = 'Test');
        Database.insert(contact);
        
        Contact_Profile__c profile = new Contact_Profile__c(Contact__c = contact.Id, SourceSystem__c = SOURCE_SYSTEM, Shopper_Id__c='shopper1');
        Database.insert(profile);
        
        Loyalty_Account__c account = new Loyalty_Account__c(IsActive__c = true, activation_date_time__c= system.now(),Customer_Profile__c = profile.Id, ShopperId__c='shopper1');
        Database.insert(account);
        Set<Id> accounts = new Set<Id>();
        accounts.add(account.Id);
        
        Loyalty_Card__c card = new Loyalty_Card__c(Loyalty_Account__c = account.Id, Name = 'test-card-id');
        Database.insert(card);
        
        account.IsActive__c = false;
        CalloutHandlerTest.initCalloutSettings(MyJL_LoyaltyAccountHandler.LEAVE_NOTIFY, 'MyJL_LoyaltyAccountHandler.JoinLeaveCallout', 10);
        Test.startTest();
        BaseTriggerHandler.removeSkipReason('JoinNotification');
        Database.update(account);
        //myJL_LoyaltyAccountHandler.sendJoinOrLeaveNotification(accounts);
        Test.stopTest();
        
        //check that outbound notification log record has been inserted
        System.assertEquals(1, [select count() from Log_Header__c where Service__c = :MyJL_LoyaltyAccountHandler.LEAVE_NOTIFY ], 
                            'Expected log record for outbound notification we have just done');
        //
        Map<String, List<SFDCMyJLCustomerTypes.Customer>> customersByEventType = MyJL_LoyaltyAccountHandler.getCustomersByCalloutType(new Set<Id>{account.Id});
        System.assert(customersByEventType.containsKey(MyJL_LoyaltyAccountHandler.LEAVE_NOTIFY), 'Expected Leave event type to be returned');
    }
    
    static testMethod void testMyJLFullJoinInvokedFromSoapService () {
        
        BaseTriggerHandler.addSkipReason('JoinNotification', 'disabled during test data preparation');
        Contact contact = new Contact(LastName = 'Test');
        Database.insert(contact);
        
        Loyalty_Account__c account = new Loyalty_Account__c(IsActive__c = true, ShopperId__c='shopper1',Voucher_Preference__c = 'Digital', activation_date_time__c= system.now());
        Database.insert(account);
        Set<Id> accounts = new Set<Id>();
        accounts.add(account.Id);
        
        Loyalty_Card__c myJLCard = new Loyalty_Card__c(Loyalty_Account__c = account.Id, Name = 'test-card-id', Card_Type__c = MyJL_Const.MY_JL_CARD_TYPE);
        Database.insert(myJLCard);
        
        
        CalloutHandlerTest.initCalloutSettings(MyJL_LoyaltyAccountHandler.FULL_JOIN_NOTIFY, 'MyJL_LoyaltyAccountHandler.JoinLeaveCallout', 10);
        Test.startTest();
        BaseTriggerHandler.removeSkipReason('JoinNotification');
        
        Map<String, List<SFDCMyJLCustomerTypes.Customer>> calloutRequest = MyJL_LoyaltyAccountHandler.getCustomersByCalloutType(accounts);
        
        SFDCMyJLCustomerTypes.Customer customerDetailsToBeSent = calloutRequest.get(MyJL_LoyaltyAccountHandler.FULL_JOIN_NOTIFY).get(0);
        SFDCMyJLCustomerTypes.CustomerAccount customerAccountToBeSent = customerDetailsToBeSent.customerAccount[0];
        
        System.assertEquals(1, customerAccountToBeSent.CustomerLoyaltyAccountCard.size() );
        List<Loyalty_Account__c> myJLAccounts = [SELECT Customer_Loyalty_Account_Card_ID__c, Number_Of_Partnership_Cards__c
                                                 FROM Loyalty_Account__c];
        System.assertEquals(myJLCard.Name, customerAccountToBeSent.CustomerLoyaltyAccountCard[0].CustomerLoyaltyAccountCardId);
        System.assertEquals(1, customerAccountToBeSent.CustomerLoyaltyAccountCard.size());
        System.assertEquals(0, myJLAccounts[0].Number_Of_Partnership_Cards__c);
        System.assertEquals(MyJL_Const.MY_JL_CARD_TYPE, customerAccountToBeSent.customerAccountTypeCode.Code);
        Test.stopTest();
    }
    
    static testMethod void testPartnershipFullJoinInvokedFromRestService () {
        
        BaseTriggerHandler.addSkipReason('JoinNotification', 'disabled during test data preparation');
        Contact contact = new Contact(LastName = 'Test', SourceSystem__c='johnlewis.com');
        Database.insert(contact);
        
        Loyalty_Account__c account = new Loyalty_Account__c(IsActive__c = true, ShopperId__c='shopper1',Voucher_Preference__c = 'Digital', activation_date_time__c= system.now(), Contact__c = contact.Id, Membership_Number__c = '121212121', Customer_Loyalty_Account_Card_ID__c = '333333333333333333333');
        Database.insert(account);
        Set<Id> accounts = new Set<Id>();
        accounts.add(account.Id);
        
        Loyalty_Card__c myJLCard = new Loyalty_Card__c(Loyalty_Account__c = account.Id, Name = 'myJL-card-id', Card_Type__c = MyJL_Const.MY_JL_CARD_TYPE);
        Database.insert(myJLCard);
        
        
        CalloutHandlerTest.initCalloutSettings(MyJL_LoyaltyAccountHandler.FULL_JOIN_NOTIFY, 'MyJL_LoyaltyAccountHandler.JoinLeaveCallout', 10);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        RestContext.request = req;
        RestContext.response = res;
        
        req.requestURI = '/MyLoyaltyService/Subscribe/' + SHOPPER_ID;
        req.httpMethod = 'PUT';
        
        Test.startTest();
        BaseTriggerHandler.removeSkipReason('JoinNotification');
        
        MyLoyaltyService.joinShopper( PARTNERSHIP_CARD_NUMBER, ORIGIN, DATE_CREATED); 
        
        
        Map<String, List<SFDCMyJLCustomerTypes.Customer>> calloutRequest = MyJL_LoyaltyAccountHandler.getCustomersByCalloutType(accounts);
        SFDCMyJLCustomerTypes.Customer customerDetailsToBeSent = calloutRequest.get(MyJL_LoyaltyAccountHandler.FULL_JOIN_NOTIFY).get(0);
        SFDCMyJLCustomerTypes.CustomerAccount customerAccountToBeSent = customerDetailsToBeSent.customerAccount[0];
        
        System.assertEquals(1, customerAccountToBeSent.CustomerLoyaltyAccountCard.size() );
        List<Loyalty_Account__c> myJLAccounts = [SELECT Name, Customer_Loyalty_Account_Card_ID__c, Number_Of_Partnership_Cards__c, MyJL_Old_Membership_Number__c
                                                 FROM Loyalty_Account__c];
        
        List<Loyalty_Card__c> cards = [SELECT Id, card_number__c, card_type__c from loyalty_Card__c];
        System.assertEquals(2, cards.size());
        System.assertEquals(1, customerAccountToBeSent.CustomerLoyaltyAccountCard.size());
        System.assertEquals(MyJL_Const.MY_PARTNERSHIP_CARD_TYPE, customerAccountToBeSent.customerAccountTypeCode.Code);
        System.assertEquals(1, myJLAccounts[0].Number_Of_Partnership_Cards__c); 
        System.assertEquals('WelcomePack', customerDetailsToBeSent.CustomerPreference[0].Description);
        System.assertEquals('Digital', customerDetailsToBeSent.CustomerPreference[0].Value);
        System.assertEquals('Digital', customerDetailsToBeSent.CustomerPreference[1].Value);
        System.assertEquals('VoucherPreference', customerDetailsToBeSent.CustomerPreference[1].PreferenceTypeCode.xrefCode[0].id);
        System.assertEquals('String', customerDetailsToBeSent.CustomerPreference[1].ValueType);
        Test.stopTest();
        
    }
    
    static testMethod void testMyJLLeaveInvokedFromSoapService () {
        BaseTriggerHandler.addSkipReason('JoinNotification', 'disabled during test data preparation');
        Contact contact = new Contact(LastName = 'Test');
        Database.insert(contact);
        
        Loyalty_Account__c account = new Loyalty_Account__c(IsActive__c = true, ShopperId__c='shopper1',Voucher_Preference__c = 'Digital', activation_date_time__c= system.now());
        Database.insert(account);
        Set<Id> accounts = new Set<Id>();
        accounts.add(account.Id);
        
        Loyalty_Card__c myJLCard = new Loyalty_Card__c(Loyalty_Account__c = account.Id, Name = 'test-card-id', Card_Type__c = MyJL_Const.MY_JL_CARD_TYPE);
        Database.insert(myJLCard);
        
        
        account.IsActive__c = false;
        CalloutHandlerTest.initCalloutSettings(MyJL_LoyaltyAccountHandler.LEAVE_NOTIFY, 'MyJL_LoyaltyAccountHandler.JoinLeaveCallout', 10);
        Test.startTest();
        BaseTriggerHandler.removeSkipReason('JoinNotification');
        Database.update(account);
        
        
        Map<String, List<SFDCMyJLCustomerTypes.Customer>> calloutRequest = MyJL_LoyaltyAccountHandler.getCustomersByCalloutType(accounts);
        
        SFDCMyJLCustomerTypes.Customer customerDetailsToBeSent = calloutRequest.get(MyJL_LoyaltyAccountHandler.LEAVE_NOTIFY).get(0);
        SFDCMyJLCustomerTypes.CustomerAccount customerAccountToBeSent = customerDetailsToBeSent.customerAccount[0];
        
        System.assertEquals(1, customerAccountToBeSent.CustomerLoyaltyAccountCard.size() );
        List<Loyalty_Account__c> myJLAccounts = [SELECT Customer_Loyalty_Account_Card_ID__c, Number_Of_Partnership_Cards__c
                                                 FROM Loyalty_Account__c];
        System.assertEquals(myJLCard.Name, customerAccountToBeSent.CustomerLoyaltyAccountCard[0].CustomerLoyaltyAccountCardId);
        System.assertEquals(1, customerAccountToBeSent.CustomerLoyaltyAccountCard.size());
        System.assertEquals(0, myJLAccounts[0].Number_Of_Partnership_Cards__c);
        System.assertEquals(MyJL_Const.MY_JL_CARD_TYPE, customerAccountToBeSent.customerAccountTypeCode.Code);
        Test.stopTest();
    }
    static testMethod void testPartnershipLeaveInvokedFromSoapService () {
        
        BaseTriggerHandler.addSkipReason('JoinNotification', 'disabled during test data preparation');
        Contact contact = new Contact(LastName = 'Test');
        Database.insert(contact);
        
        Loyalty_Account__c account = new Loyalty_Account__c(IsActive__c = true, ShopperId__c='shopper1',Voucher_Preference__c = 'Digital', activation_date_time__c= system.now());
        Database.insert(account);
        Set<Id> accounts = new Set<Id>();
        accounts.add(account.Id);
        
        Loyalty_Card__c myJLCard = new Loyalty_Card__c(Loyalty_Account__c = account.Id, Name = 'test-card-id', Card_Type__c = MyJL_Const.MY_JL_CARD_TYPE);
        Database.insert(myJLCard);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        RestContext.request = req;
        RestContext.response = res;
        
        req.requestURI = '/MyLoyaltyService/Subscribe/' + SHOPPER_ID;
        req.httpMethod = 'PUT';
        
        
        MyLoyaltyService.joinShopper( PARTNERSHIP_CARD_NUMBER, ORIGIN, DATE_CREATED); 
        
        account.IsActive__c = false;
        CalloutHandlerTest.initCalloutSettings(MyJL_LoyaltyAccountHandler.LEAVE_NOTIFY, 'MyJL_LoyaltyAccountHandler.JoinLeaveCallout', 10);
        Test.startTest();
        BaseTriggerHandler.removeSkipReason('JoinNotification');
        Database.update(account);
        
        
        Map<String, List<SFDCMyJLCustomerTypes.Customer>> calloutRequest = MyJL_LoyaltyAccountHandler.getCustomersByCalloutType(accounts);
        
        SFDCMyJLCustomerTypes.Customer customerDetailsToBeSent = calloutRequest.get(MyJL_LoyaltyAccountHandler.LEAVE_NOTIFY).get(0);
        SFDCMyJLCustomerTypes.CustomerAccount customerAccountToBeSent = customerDetailsToBeSent.customerAccount[0];
        
        System.assertEquals(1, customerAccountToBeSent.CustomerLoyaltyAccountCard.size() );
        List<Loyalty_Account__c> myJLAccounts = [SELECT Customer_Loyalty_Account_Card_ID__c, Number_Of_Partnership_Cards__c
                                                 FROM Loyalty_Account__c];
        System.assertEquals(1, customerAccountToBeSent.CustomerLoyaltyAccountCard.size());
        System.assertEquals(1, myJLAccounts[0].Number_Of_Partnership_Cards__c);
        System.assertEquals(MyJL_Const.MY_PARTNERSHIP_CARD_TYPE, customerAccountToBeSent.customerAccountTypeCode.Code);
        Test.stopTest();
    }
}