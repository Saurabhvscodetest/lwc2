/**
 */
@isTest
private class clsContactGender_TEST {
	
	static void test(Contact c, String salutation, String expectedGender) {
		system.debug('salutation: '+salutation+' expected: '+expectedGender);
		c.salutation = salutation;
		update c;
	
		Contact c1 = [SELECT jl_gender__c,salutation FROM Contact WHERE id=:c.Id];
		system.debug(c1);
		system.assertEquals(expectedGender, c1.jl_gender__c);		
	}

    static testMethod void myUnitTest() {
		Contact c = new Contact(lastname='smith');
		insert c;
		test(c, 'Mrs','Female');
		test(c, 'Mr','Male');
		test(c, 'Ms','Female');
		test(c, 'Miss','Female');
		test(c, 'Dr', 'Unknown');
		test(c, 'Prof', 'Unknown');
		test(c, 'Lady', 'Female');
		test(c, 'Hon', 'Unknown');
		test(c, 'Sir', 'Male');
		test(c, 'Lord', 'Male');
		test(c, 'Baron', 'Male');
		test(c, 'Baroness', 'Female');
		test(c, 'Brig', 'Unknown');
		test(c, 'Capt', 'Unknown');
		test(c, 'Col', 'Unknown');
		test(c, 'Count', 'Male');
		test(c, 'Countess', 'Female');
		test(c, 'Dame', 'Female');
		test(c, 'Duchess', 'Female');
		Test.startTest();
		test(c, 'Duke', 'Male');
		test(c, 'Earl', 'Male');
		test(c, 'HRH', 'Unknown');
		test(c, 'Judge', 'Unknown');
		test(c, 'Lt', 'Unknown');
		test(c, 'Lt Col', 'Unknown');
		test(c, 'Major', 'Unknown');
		test(c, 'Marquess', 'Male');
		test(c, 'Prince', 'Male');
		test(c, 'Princess', 'Female');
		test(c, 'Revd', 'Unknown');
		test(c, 'Rt Hon', 'Unknown');
		test(c, 'Rt Revd', 'Unknown');
		test(c, 'Viscount', 'Male');
		test(c, 'Viscntess', 'Female');
		test(c, 'Wing Cdr', 'Unknown');
		//test(c, 'Mr.','Male');
		test(c, 'Ms.','Female');
		//test(c, 'Miss.','Female');
		test(c, 'Mrs.','Female');
		Test.stopTest();
    }
}