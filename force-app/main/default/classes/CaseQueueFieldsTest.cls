@isTest
public class CaseQueueFieldsTest {

    @testSetup static void initData() {
        MilestoneUtilsTest.initEntitlementTestData();
    }
    
    @isTest static void setFirstQueue_ownedByQueue() {

        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        User cstUser;
        Case newCSTCase;
        Contact testContact;

        System.runAs(runningUser) {
            cstUser = UnitTestDataFactory.getFrontOfficeUser('CSTUSER');
            cstUser.Team__c = MilestoneUtilsTest.HAMILTON_CST_TEAM_NAME;
            insert cstUser;
            //String ps = 'In_Flight_Cases_Update';
            //UserTestDataFactory.assignPermissionSetToUser(cstUser.id,ps);
            
            String ps1 = 'JL_MYJL_Phase_3_Contact_Center_Permissions';
            UserTestDataFactory.assignPermissionSetToUser(cstUser.id,ps1);


            //String ps2 = 'ContactAccess';
            //UserTestDataFactory.assignPermissionSetToUser(cstUser.id,ps2);
            
            System.debug('cstUser ::::::::::::::::: P '+cstUser);
            
            testContact = UnitTestDataFactory.createContact();
            insert testContact;
        }

        System.runAs(cstUser) {
            newCSTCase = UnitTestDataFactory.createCase(testContact.Id);
            System.debug('newCSTCase ::::: testContact.Id '+testContact.Id);
            newCSTCase.Bypass_Standard_Assignment_Rules__c = true; // Allow unit test to bypass Standard Case Assignment
            
            // Set routing parameters
            newCSTCase.JL_Branch_master__c = 'Oxford Street';
            newCSTCase.Contact_Reason__c = 'Pre-Sales';
            newCSTCase.Reason_Detail__c = 'Buying Office Enquiry';
            insert newCSTCase;
        }
        newCSTCase = [SELECT Owner.Name, OwnerId, JL_First_Queue__c, JL_First_Queue_Name__c FROM Case WHERE Id = :newCSTCase.Id];   
        Group myGroup = [SELECT Id, DeveloperName FROM Group WHERE Id = :newCSTCase.OwnerId];

        System.assertEquals(newCSTCase.OwnerId, newCSTCase.jl_First_Queue__c);
        System.assertEquals(myGroup.DeveloperName, newCSTCase.jl_First_Queue_Name__c);
    }

    @isTest static void setFirstQueue_ownedByUser() {

        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

        delete [SELECT Id FROM Case_Routing_Categorisation__c];

        User cstUser;
        Case newCSTCase;
        Contact testContact;

        System.runAs(runningUser) {
            cstUser = UnitTestDataFactory.getFrontOfficeUser('CSTUSER');
            cstUser.Team__c = MilestoneUtilsTest.HAMILTON_CST_TEAM_NAME;
            insert cstUser;
            System.debug('cstUser ::::::::::::::::: '+cstUser);
            
            
            //String ps = 'In_Flight_Cases_Update';
            //UserTestDataFactory.assignPermissionSetToUser(cstUser.id,ps);
            
            String ps1 = 'JL_MYJL_Phase_3_Contact_Center_Permissions';
            UserTestDataFactory.assignPermissionSetToUser(cstUser.id,ps1);


            //String ps2 = 'ContactAccess';
            //UserTestDataFactory.assignPermissionSetToUser(cstUser.id,ps2);
            
            
            testContact = UnitTestDataFactory.createContact();
            insert testContact;
        }
        
        System.runAs(cstUser) {
            newCSTCase = UnitTestDataFactory.createCase(testContact.Id);
            newCSTCase.Bypass_Standard_Assignment_Rules__c = true; // Allow unit test to bypass Standard Case Assignment
            
            // Set routing parameters
            newCSTCase.JL_Branch_master__c = 'Oxford Street';
            newCSTCase.Contact_Reason__c = 'Pre-Sales';
            newCSTCase.Reason_Detail__c = 'Buying Office Enquiry';
            insert newCSTCase;
        }
        
        Team__c userTeam = [SELECT Queue_Name__c FROM Team__c WHERE Name = :cstUser.Team__c];
        Group myGroup = [SELECT Id FROM Group WHERE DeveloperName = :userTeam.Queue_Name__c];

        newCSTCase = [SELECT Owner.Name, OwnerId, JL_First_Queue__c, JL_First_Queue_Name__c FROM Case WHERE Id = :newCSTCase.Id];   

        System.assertEquals(myGroup.Id, newCSTCase.JL_First_Queue__c);
        System.assertEquals(userTeam.Queue_Name__c, newCSTCase.JL_First_Queue_Name__c);
    }
}