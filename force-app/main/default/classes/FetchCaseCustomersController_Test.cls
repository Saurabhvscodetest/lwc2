/* Description  : 
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                         Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   30/01/2019        Vignesh Kotha                   Created               COPT-4260
*
*/
@isTest
public class FetchCaseCustomersController_Test {
    @isTest
    public static void FetchCustomersdata(){
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        Contact testContact;
        System.runAs(runningUser) 
        {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;       
            insert CustomSettingTestDataFactory.createConfigSettings();
        }        
        System.runAs(testUser) 
        {            
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;            
            case csc = CaseTestDataFactory.createCase(testContact.Id);
            csc.Bypass_Standard_Assignment_Rules__c = true;
            csc.Contact_ID_List__c=testContact.id;
            insert csc;
            system.assertEquals( csc.Contact_ID_List__c,testContact.id);           
            Test.startTest();
            FetchCaseCustomersController.getCaseCustomers(csc.id);
            FetchCaseCustomersController.UpdateCaseRecord(testContact.id,csc.id);
            Test.stopTest();
            
        }
    }
}