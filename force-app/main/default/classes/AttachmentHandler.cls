public class AttachmentHandler{

    public static void mainEntry(Boolean isBefore, 
                               Boolean isDelete, Boolean isAfter, 
                               Boolean isInsert, Boolean isUpdate, 
                               Boolean isExecuting, List<Attachment> newlist, 
                               Map<ID,Sobject> newmap, List<Attachment> oldlist, 
                               Map<Id,Sobject> oldmap)
    {
       if(isInsert && isAfter){
            copyEmailAttachmentsToCase(newList);
        }
    }
    

    private static Map<Id,Id> getCaseIdsMap(List<Attachment> attachments){

        Map<Id,Id> parentIdMap = new Map<Id,Id>();
        Set<Id> attachmentParentIdSet = new Set<Id>();
        
        for(Attachment a : attachments){
            attachmentParentIdSet.add(a.parentId);
        }

        for (EmailMessage message : [select Id, parentId from EmailMessage where Id IN:attachmentParentIdSet]) {
            parentIdMap.put(message.Id, message.parentId);
        }
        
        return parentIdMap;
    }

    private static List<ContentVersion> getContentDocumentVersionsFromAttachments(List<Attachment> attachments, Map<Id,Id> parentIdMap){

        List<ContentVersion> contentVersions = new List<ContentVersion>();

        for( Attachment a : attachments ) {  
            
            if( a.parentid != null ) {   
                
                String s = string.valueof( a.parentid );
                if( s.substring( 0, 3 ) == '02s' ) {  

                    ContentVersion cv = new ContentVersion();
                    cv.ContentLocation = 'S';
                    cv.PathOnClient = a.Name;
                    cv.Origin = 'H';
                    cv.OwnerId = a.OwnerId;
                    cv.Title = a.Name;
                    cv.VersionData = a.Body;
                    cv.parent_case__c = parentIdMap.get(a.parentId); 
                    contentVersions.add(cv);
                }
            }
        }
        return contentVersions;
    }


    private static List<ContentDocumentLink> getContentDocumentLinkFromContentVersion(List<ContentVersion> contentVersions ){

        List<ContentDocumentLink> documentLinks = new List<ContentDocumentLink>();

        for (ContentVersion contentVersion : [SELECT ContentDocumentId, parent_case__c FROM ContentVersion where id in: contentVersions]) {
            
            ContentDocumentLink cl = new ContentDocumentLink(LinkedEntityId = contentVersion.parent_case__c, ContentDocumentId = contentVersion.ContentDocumentId, ShareType = 'V');
            documentLinks.add(cl);
        }

        return documentLinks;
    }

    public static void copyEmailAttachmentsToCase(List<Attachment> attachments)
    {
        Map<Id,Id> parentIdMap = getCaseIdsMap(attachments);

        if(parentIdMap.size() > 0){
            List<ContentVersion> contentVersions = getContentDocumentVersionsFromAttachments(attachments, parentIdMap);

            if(contentVersions.size() > 0){
                insert contentVersions;
                
                List<ContentDocumentLink> documentLinks = getContentDocumentLinkFromContentVersion(contentVersions);
                if(documentLinks.size() > 0){
                    insert documentLinks;
                }
            }
        }
    }
}