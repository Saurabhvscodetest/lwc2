@isTest
public with sharing class UserTestDataFactory {
	
	public static User getRunningUser(){
		User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
		return runningUser;
	}

	public static Id getProfileId(String profileName) {
    	Profile prof = [Select Id From Profile Where Name = :profileName limit 1];
    	return prof.Id;
    }

    public static Id getRoleId(String roleName) {
    	UserRole role = [Select Id From UserRole Where Name = :roleName limit 1];
    	return role.Id;
    }

	public static User createUser (String firstName, String profileName, String roleName, String primaryTeamName) {
        Id profileId = getProfileId(profileName);
        Id roleId = getRoleId(roleName);
        User user = new User(FirstName = firstName, LastName = 'Test', Username=firstName+'@test.com', 
                            Email=firstName+'@test.com', CommunityNickname=firstName, 
                            Alias=firstName.abbreviate(8) ,TimeZoneSidKey='Europe/London', 
                            LocaleSidKey='en_GB', EmailEncodingKey='ISO-8859-1', 
                            ProfileId=profileId, LanguageLocaleKey='en_US', isActive=true,
                            Team__c = primaryTeamName, Tier__c =2, userRoleid = roleId
                        );
        return user;
    }

    public static User createContactCentreUser(){
    	return createUser('ContactCentreTestUser', 'JL: ContactCentre Tier2-3', 'JLP:CC:CST:T2', 'Hamilton/Didsbury - CST');
    }

    public static User createCapitaUser(){
    	return createUser('CapitaTestUser', 'JL: CapitaContactCentre Tier2-3', 'JLP:CC:JLCOM:BackOffice:CST:T2', 'JL.com - CST');
    }

	public static User createBranchUser(){
    	return createUser('BranchTestUser', 'JL: Case Profile', 'JLP:Branch:OXFORDSTREET:CoreHomeServices:T2', 'Branch - CST Oxford Street');
    }    
    public static void assignPermissionSetToUser(Id userId, String permSetName){
        PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = :permSetName];
        PermissionSetAssignment psa = new PermissionSetAssignment();
        psa.AssigneeId = userId;
        psa.PermissionSetId = ps.Id;
        insert psa; 
    }   
}