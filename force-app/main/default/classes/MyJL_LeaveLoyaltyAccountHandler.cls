/**
*  @author: Amit Kumar
*  @date: 2014-06-30
*  @description:
*      Main logic for Leave MyJL function
*
*  Version history
*  2014-08-26 - MJL-829 - AG
*  populate MyJL_ESB_Message_ID__c on Leave
*
*  001     09/03/15    NTJ         MJL-1610 - ShopperId case must be preserved
*  002     20/04/15    ntj         Remove Debug.log 
*  003     07/05/15    NTJ         Put flag into disable CUSTOMER_ID_NOT_FOUND. ESB only knows about LOYALTY_ACCOUNT_NOT_FOUND so a temporary fix is for us to return
*                                  the former error. ESB need a CR to make a change and we can do it in Salesforce in an agile fashion.
*  004     08/05/15    NTJ         Small change to 003 to ensure Leave works
*  005     24/06/15    NTJ         Add a log header for when we really do a LEAVE (i.e. there were no errors)
*  006     27/06/16    NA          Decommissioning contact profile
*/

public with sharing class MyJL_LeaveLoyaltyAccountHandler {
    
    // 003
    private static final Boolean CustomerIdCheck = false;
    
    public class SFDCCustomerWrapper extends MyJL_Util.CustomerWrapper{ 
        
        public string customerID;
        public string customerGroupName;
        public final String email;
        public Integer requestIndex = 0;
        public String shopperId;
        public string stackTraceDetails = null;
        public String stackTrace = '';
        public SFDCMyJLCustomerTypes.CustomerRecordResultType resultType;
        public Loyalty_Account__c loyaltyAccount;
        public SFDCMyJLCustomerTypes.Customer customer;
        
        
        public SFDCCustomerWrapper(Integer pRequestIndex, SFDCMyJLCustomerTypes.Customer pCustomer, String pMessageId) {
            customer = pCustomer;
            requestIndex = pRequestIndex;
            
            email = getEmailAddress();
            messageId = pMessageId;
        }
        
        public override SFDCMyJLCustomerTypes.Customer getCustomerInput() {
            return customer;
        }
        
        public override SFDCMyJLCustomerTypes.CustomerRecordResultType getCustomerResult(){
            return resultType;
        }
        
        public override String getStackTrace() {
            return this.stackTraceDetails;
        }
        
        public void setStackTrace(String stackTraceDetails) {
            this.stackTraceDetails = stackTraceDetails;
        }
    }
    ///////////////////////////////////////////// END OF CustomerWrapper /////////////////////////////////
    
    
    public static SFDCMyJLCustomerTypes.CustomerResponseType process(SFDCMyJLCustomerTypes.CustomerRequestType request){
        ID logHeaderId = LogUtil.startLogHeader(LogUtil.SERVICE.WS_LEAVE_CUSTOMER, request.requestHeader,  request.Customer);
        String logHeaderStackTrace = Null;  
        
        SFDCMyJLCustomerTypes.ResponseHeaderType responseHeader = new SFDCMyJLCustomerTypes.ResponseHeaderType();
        SFDCMyJLCustomerTypes.CustomerResponseType finalResponse = new SFDCMyJLCustomerTypes.CustomerResponseType();
        SFDCMyJLCustomerTypes.ResultStatus resultStatus = MyJL_Util.getSuccessResultStatus();
        final SFDCMyJLCustomerTypes.RequestHeaderType requestHeader = request.RequestHeader;
        Map<String, SFDCCustomerWrapper> sfdcCustomerWrapperMap = new Map<String, SFDCCustomerWrapper>();
        List<Loyalty_Account__c> LoyaltyAccountsToBeDeactivatedList = new List<Loyalty_Account__c>();
        Map<String,Loyalty_Account__c> LoyaltyAccountsToBeDeactivatedMap = new Map<String,Loyalty_Account__c>(); 
        Map<Integer,SFDCMyJLCustomerTypes.CustomerRecordResultType> responsesMap = new  Map<Integer,SFDCMyJLCustomerTypes.CustomerRecordResultType>();          
        List<SFDCCustomerWrapper> processCustomerWrapperList = new List<SFDCCustomerWrapper>();    
        try{    
            if(request.requestHeader != null && !MyJL_Util.isNull(request.RequestHeader.MessageId) && String.IsNotBlank(request.RequestHeader.MessageId) ) {  
                if(MyJL_Util.isMessageIdUnique(request.requestHeader.MessageId)) {
                    
                    if(request.Customer != null) {
                        ResponseHeader.MessageId = request.requestHeader.MessageId;          
                        set<string> receivedEmailAddresses = new set<string>();
                        set<string> receivedCustomerIds = new set<string>();
                        
                        for(Integer i=0; i< request.Customer.size(); i++) {
                            if(request.Customer[i] != null) {
                                SFDCCustomerWrapper sfdcWrapper = new SFDCCustomerWrapper(i,request.Customer[i], request.requestHeader.MessageId);
                                
                                String customerId = MyJL_Util.getCustomerId(request.Customer[i]);
                                //system.debug('customerId : '+ customerId);
                                
                                if(String.IsNotBlank(customerId)) {
                                    if(receivedCustomerIds.contains(customerId)){
                                        //customerIds.add(customerId);
                                        SFDCMyJLCustomerTypes.CustomerRecordResultType customerResult = new SFDCMyJLCustomerTypes.CustomerRecordResultType();
                                        customerResult = MyJL_Util.populateErrorDetails(MyJL_Const.ERRORCODE.CUSTOMERID_NOT_UNIQUE, customerResult);
                                        customerResult.customer = request.Customer[i];
                                        
                                        sfdcWrapper.resultType = customerResult;
                                        responsesMap.put(i,customerResult);
                                        processCustomerWrapperList.add(sfdcWrapper);
                                    }
                                    else
                                    {
                                        receivedCustomerIds.add(customerId);
                                        sfdcCustomerWrapperMap.put(customerId, new SFDCCustomerWrapper(i,request.Customer[i], request.requestHeader.MessageId));
                                    }
                                }
                                else
                                {
                                    SFDCMyJLCustomerTypes.CustomerRecordResultType customerResult = new SFDCMyJLCustomerTypes.CustomerRecordResultType();
                                    customerResult = MyJL_Util.populateErrorDetails(MyJL_Const.ERRORCODE.CUSTOMERID_NOT_PROVIDED, customerResult);
                                    customerResult.customer = request.Customer[i];
                                    
                                    sfdcWrapper.resultType = customerResult;
                                    responsesMap.put(i,customerResult);
                                    processCustomerWrapperList.add(sfdcWrapper);
                                }
                            }
                        }//for loop ends here
                        
                        //system.debug('AMIT sfdcCustomerWrapperMap values: '+ sfdcCustomerWrapperMap);
                        set<string> existingCustomerProfiles = new set<string>();
                        map<string, contact> contactProfileToLoyaltyAccount = new map<string, contact>();  //<<006>>
                        
                        contact[] contactProfiles = queryContactProfilesWithActiveLoyaltyAccounts(sfdcCustomerWrapperMap.keySet()); //<<006>>
                        //Need to ask Arif, Mutiple Loyalty Accounts can look up to same Contact profile. hence query will return multiple Loyalty accounts, which one to deactivate. 
                        
                        Set<Loyalty_Account__c> loyaltyAccounts = new Set<Loyalty_Account__c>();
                        for(contact contactProfile : contactProfiles) {  //<<006>>
                            String customerId = contactProfile.Shopper_Id__c;
                            if(!String.isBlank(customerId)) {
                                if(contactProfile.MyJL_Accounts__r != null){
                                    // pre 001  
                                    //contactProfileToLoyaltyAccount.put(customerId.toLowerCase(),contactProfile);
                                    contactProfileToLoyaltyAccount.put(customerId,contactProfile);
                                    loyaltyAccounts.addAll(contactProfile.MyJL_Accounts__r);
                                    
                                }
                                // pre 001
                                //existingCustomerProfiles.add(customerId.toLowerCase());
                                existingCustomerProfiles.add(customerId);
                            }
                            
                        }                                                
                        
                        Integer successResultCounter = 0;    
                        Map<Integer, String> successResultCustomerIdMap = new Map<Integer, String>();
                        
                        for(SFDCCustomerWrapper wrapper : sfdcCustomerWrapperMap.values()) {
                            
                            if(!existingCustomerProfiles.contains(wrapper.getCustomerId())) {
                                // 003
                                MyJL_Const.ERRORCODE error = (CustomerIdCheck == true) ? MyJL_Const.ERRORCODE.CUSTOMERID_NOT_FOUND : MyJL_Const.ERRORCODE.LOYALTY_ACCOUNT_NOT_FOUND;
                                
                                SFDCMyJLCustomerTypes.CustomerRecordResultType customerResult = new SFDCMyJLCustomerTypes.CustomerRecordResultType();
                                responsesMap.put(wrapper.requestIndex,MyJL_Util.populateErrorDetails(error, customerResult));
                                SFDCCustomerWrapper sfdcWrapper = new SFDCCustomerWrapper(wrapper.requestIndex,wrapper.Customer, wrapper.messageId);
                                sfdcWrapper.resultType = MyJL_Util.populateErrorDetails(error, customerResult);
                                //system.debug('AMIT sfdcWrapper : '+ sfdcWrapper);
                                processCustomerWrapperList.add(sfdcWrapper);
                            } else {
                                //system.debug('AMIT MyJL_Accounts__r: ' + contactProfileToLoyaltyAccount.get(wrapper.getCustomerId()).MyJL_Accounts__r);
                                if(contactProfileToLoyaltyAccount.containsKey(wrapper.getCustomerId())){
                                    if(contactProfileToLoyaltyAccount.get(wrapper.getCustomerId()).MyJL_Accounts__r != null && contactProfileToLoyaltyAccount.get(wrapper.getCustomerId()).MyJL_Accounts__r.size() > 0 ){
                                        Integer counter=0;
                                        for(Loyalty_Account__c la : contactProfileToLoyaltyAccount.get(wrapper.getCustomerId()).MyJL_Accounts__r){
                                            if(la.IsActive__c == true && !LoyaltyAccountsToBeDeactivatedMap.containsKey(wrapper.getCustomerId())){
                                                wrapper.loyaltyAccount = deactivateLoyaltyAccounts(la, wrapper.messageId);
                                                
                                                
                                                LoyaltyAccountsToBeDeactivatedMap.put(wrapper.getCustomerId(), wrapper.loyaltyAccount);
                                                LoyaltyAccountsToBeDeactivatedList.add(wrapper.loyaltyAccount);
                                                counter++;
                                                //system.debug('AMIT LoyaltyAccountsToBeDeactivatedMap : '+ LoyaltyAccountsToBeDeactivatedMap);
                                                successResultCustomerIdMap.put(successResultCounter, wrapper.getCustomerId());
                                                successResultCounter++;
                                            }
                                            
                                        }
                                        
                                        if(counter == 0){
                                            SFDCMyJLCustomerTypes.CustomerRecordResultType customerResult = new SFDCMyJLCustomerTypes.CustomerRecordResultType();
                                            responsesMap.put(wrapper.requestIndex,MyJL_Util.populateErrorDetails(MyJL_Const.ERRORCODE.NO_ACTIVE_LOYALTY_ACCOUNT_FOUND, customerResult));
                                            SFDCCustomerWrapper sfdcWrapper = new SFDCCustomerWrapper(wrapper.requestIndex,wrapper.Customer, wrapper.messageId);
                                            sfdcWrapper.resultType = MyJL_Util.populateErrorDetails(MyJL_Const.ERRORCODE.NO_ACTIVE_LOYALTY_ACCOUNT_FOUND, customerResult);
                                            processCustomerWrapperList.add(sfdcWrapper);
                                        }
                                        //system.debug('successResultCustomerIdMap: '+ successResultCustomerIdMap);
                                    }
                                    else {
                                        SFDCMyJLCustomerTypes.CustomerRecordResultType customerResult = new SFDCMyJLCustomerTypes.CustomerRecordResultType();
                                        responsesMap.put(wrapper.requestIndex,MyJL_Util.populateErrorDetails(MyJL_Const.ERRORCODE.LOYALTY_ACCOUNT_NOT_FOUND, customerResult));
                                        SFDCCustomerWrapper sfdcWrapper = new SFDCCustomerWrapper(wrapper.requestIndex,wrapper.Customer, wrapper.messageId);
                                        sfdcWrapper.resultType = MyJL_Util.populateErrorDetails(MyJL_Const.ERRORCODE.LOYALTY_ACCOUNT_NOT_FOUND, customerResult);
                                        processCustomerWrapperList.add(sfdcWrapper);
                                    }  
                                } 
                            }  
                        }  //sfdcCustomerWrapperMap loop ends here
                        
                        //system.debug('AMIT LoyaltyAccountsToBeDeactivatedList' + LoyaltyAccountsToBeDeactivatedList);
                        //Insert Limited Customer profiles in DB
                        //system.debug('Amit LoyaltyAccountsToBeDeactivatedList size'+LoyaltyAccountsToBeDeactivatedList.size());
                        if(LoyaltyAccountsToBeDeactivatedList.size()>0){
                            ID logHeaderId2 = LogUtil.startLogHeader(LogUtil.SERVICE.LEAVE_DEACTIVATE_MYJL, request.requestHeader,  request.Customer);                      
                            
                            Database.Saveresult[] saveresults = Database.Update(LoyaltyAccountsToBeDeactivatedList, false);                            
                            //system.debug('Amit saveresult Sucess size'+saveresults.size());
                            
                            for(Integer i=0; i<saveresults.size(); i++) {
                                
                                if(saveresults[i].success) {
                                    //system.debug('i size '+i);
                                    SFDCCustomerWrapper sfdcWrapper = new SFDCCustomerWrapper(sfdcCustomerWrapperMap.get(successResultCustomerIdMap.get(i)).requestIndex,sfdcCustomerWrapperMap.get(successResultCustomerIdMap.get(i)).Customer, sfdcCustomerWrapperMap.get(successResultCustomerIdMap.get(i)).messageId);
                                    sfdcWrapper.resultType = myJl_Util.getSuccessResult();
                                    responsesMap.put(sfdcCustomerWrapperMap.get(successResultCustomerIdMap.get(i)).requestIndex,myJl_Util.getSuccessResult());
                                    //system.debug('Response Map : '+ responsesMap);
                                    //system.debug('Response Map Size : '+ responsesMap.size());
                                    processCustomerWrapperList.add(sfdcWrapper);
                                } 
                                else {
                                    for(Database.Error err : saveresults[i].getErrors()) {
                                        //system.debug('AMIT err.getStatusCode(): '+err.getStatusCode());
                                        SFDCMyJLCustomerTypes.CustomerRecordResultType customerResult = new SFDCMyJLCustomerTypes.CustomerRecordResultType();
                                        SFDCCustomerWrapper sfdcWrapper = new SFDCCustomerWrapper(sfdcCustomerWrapperMap.get(successResultCustomerIdMap.get(i)).requestIndex,sfdcCustomerWrapperMap.get(successResultCustomerIdMap.get(i)).Customer, sfdcCustomerWrapperMap.get(successResultCustomerIdMap.get(i)).messageId);
                                        sfdcWrapper.resultType = MyJL_Util.populateErrorDetails(MyJL_Const.ERRORCODE.FAILED_TO_PERSIST_DATA, customerResult);
                                        responsesMap.put(sfdcCustomerWrapperMap.get(successResultCustomerIdMap.get(i)).requestIndex,MyJL_Util.populateErrorDetails(MyJL_Const.ERRORCODE.FAILED_TO_PERSIST_DATA, customerResult));
                                        sfdcWrapper.setStackTrace('Message: '+err.getMessage()+', Status Code: '+err.getStatusCode()+', Field that affected this error: '+err.getFields())  ;
                                        processCustomerWrapperList.add(sfdcWrapper);
                                    }
                                }
                            }
                        }
                        
                        
                        //system.debug('AMIT responsesMap values: '+ responsesMap);
                        //system.debug('AMIT responsesMap keyset: '+ responsesMap.keyset());
                        
                        SFDCMyJLCustomerTypes.CustomerRecordResultType[] resArray = new SFDCMyJLCustomerTypes.CustomerRecordResultType[responsesMap.size()];
                        
                        if(!responsesMap.isEmpty()) {
                            for(integer i : responsesMap.keyset()) {
                                
                                try {
                                    resArray[i] = responsesMap.get(i);
                                } catch(ListException le) {
                                    system.debug('The following exception has occurred: ' + le.getMessage());
                                    SFDCCustomerWrapper sfdcWrapper = new SFDCCustomerWrapper(sfdcCustomerWrapperMap.get(successResultCustomerIdMap.get(i)).requestIndex,sfdcCustomerWrapperMap.get(successResultCustomerIdMap.get(i)).Customer, sfdcCustomerWrapperMap.get(successResultCustomerIdMap.get(i)).messageId);
                                    sfdcWrapper.setStackTrace('Message: '+le.getMessage());
                                }
                            }
                        }
                        
                        resultStatus = MyJL_Util.getSuccessResultStatus();
                        responseHeader.ResultStatus = resultStatus;
                        finalResponse.ResponseHeader = responseHeader;
                        
                        for(Integer idx = 0; idx < resArray.size(); idx++) {
                            resArray[idx].Customer = request.Customer[idx];
                        }
                        
                        //Calling Logging Method to Log details 
                        LogUtil.logDetails(logHeaderID, processCustomerWrapperList);
                        //Set response payload                
                        finalResponse.ActionRecordResults = resArray;
                    }
                    else 
                    { // Customer Array is not Null: Ends here
                        resultStatus = MyJL_Util.populateErrorDetailsHeader(MyJL_Const.ERRORCODE.NO_CUSTOMER_DATA, resultStatus);
                        responseHeader.ResultStatus = resultStatus; 
                        finalResponse.ResponseHeader = responseHeader;
                    }
                } 
                else 
                { //Message Id Unique Check ends here.
                    //to do: implement logging if message id exists
                    resultStatus = MyJL_Util.populateErrorDetailsHeader(MyJL_Const.ERRORCODE.MESSAGEID_DUPLICATE, resultStatus);
                    responseHeader.ResultStatus = resultStatus; 
                    finalResponse.ResponseHeader = responseHeader;
                }
            } // request header is not null: ENDS HERE
            else 
            {  
                resultStatus = MyJL_Util.populateErrorDetailsHeader(MyJL_Const.ERRORCODE.MESSAGEID_NOT_PROVIDED, resultStatus);
                responseHeader.ResultStatus = resultStatus; 
                finalResponse.ResponseHeader = responseHeader;    
            }
            
            
            
            LogUtil.updateLogHeader(logHeaderID, responseHeader, null);
            
        }
        catch (Exception e) {
            //SFDCMyJLCustomerTypes.ResultStatus resultStatus = new SFDCMyJLCustomerTypes.ResultStatus();
            SFDCMyJLCustomerTypes.CustomerResponseType response = new SFDCMyJLCustomerTypes.CustomerResponseType();
            resultStatus.Code = null;
            resultStatus.Message = null;
            resultStatus.Type_x = null;
            resultStatus.Success = false;
            responseHeader.ResultStatus = resultStatus;
            response.ResponseHeader = responseHeader;
            
            responseHeader.ResultStatus = MyJL_Util.populateErrorDetailsHeader(MyJL_Const.ERRORCODE.UNHANDLED_EXCEPTION, responseHeader.ResultStatus);
            LogUtil.updateLogHeader(logHeaderID, responseHeader, e.getStackTraceString());
            //System.assertEquals(false, Test.isRunningTest(), 'exception thrown: ' + e);
            //system.debug('AMIT e=' + e);
            //system.debug('AMIT e.stack=' + e.getStackTraceString());
        }
        return finalResponse;
    }
    
    
    
    private static contact[] queryContactProfilesWithActiveLoyaltyAccounts(Set<String> customerIds) {  //<<006>>
        //system.debug('LeaveCustomerResponse-queryContactProfiles list of IDs for the Contact_Profile__c query: ' + customerIds);
        
        List<contact> contactProfiles = new List<contact>();  //<<006>>
        
        if(customerIds != null && customerIds.size() > 0) {
            // MJL-1790
            for (contact cp:[
                SELECT
                id,
                Shopper_ID__c, 
                (Select 
                 id, Name,
                 IsActive__c,
                 contact__c
                 From MyJL_Accounts__r )
                FROM 
                contact
                WHERE
                Shopper_ID__c IN :customerIds
                AND SourceSystem__c = :Label.MyJL_JL_comSourceSystem
            ]) {  //<<006>>
                if (String.isNotBlank(cp.Shopper_ID__c)) {
                    contactProfiles.add(cp);
                }
            }
        }
        
        return contactProfiles;
    }
    
    private static Loyalty_Account__c deactivateLoyaltyAccounts(Loyalty_Account__c la, final String messageId){
        Loyalty_Account__c loyaltyAccount = new Loyalty_Account__c();
        loyaltyAccount.IsActive__c = false;
        loyaltyAccount.Deactivation_Date_Time__c = datetime.now();
        loyaltyAccount.id = la.id;
        loyaltyAccount.MyJL_ESB_Message_ID__c = messageId;
        return loyaltyAccount;
    }
    
    
    public static void deactivatePartnershipCard(List<Loyalty_Card__c> lc){
        try{
            List<Loyalty_Account__c> loyaltyAccountsToUpdate = new List<Loyalty_Account__c>();
            Loyalty_Account__c loyaltyAccount = new Loyalty_Account__c();
            
            for(Loyalty_Card__c lcard: lc){
                lcard.Disabled__c = true;
                loyaltyAccount.Membership_Number__c = lcard.Loyalty_Account__r.MyJL_Old_Membership_Number__c;
                loyaltyAccount.Name = lcard.Loyalty_Account__r.MyJL_Old_Membership_Number__c;
                loyaltyAccount.Customer_Loyalty_Account_Card_ID__c = lcard.Loyalty_Account__r.MyJL_Old_Barcode__c;
                loyaltyAccount.Id = lcard.Loyalty_Account__c;
                loyaltyAccountsToUpdate.add(loyaltyAccount);         
            }
            
            update lc;
            
            
            update loyaltyAccountsToUpdate;
            
        }catch(Exception ex){
            System.debug('An error has occured when deactivating the loyalty card ' + ex.getMessage());
            
        }                        
        
    }
}