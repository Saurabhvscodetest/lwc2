/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class JLActivationControllerTest {
	@testSetup static void setup() {
         UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
    }
	
	@IsTest 
    static  void testSSTUserFlow() {
        // TO DO: implement unit test
        
         User u = [select title, firstname, lastname, email, phone, mobilephone, fax, street, city, state, postalcode, country
                           FROM User WHERE id =: UserInfo.getUserId()];
                           
          // create an agent user
          
          
              Profile[] profiles = [Select UserType, Name, Id From Profile where UserType = 'Standard'];
        PermissionSet[] perms = [Select Id, profileid, UserLicenseId From PermissionSet where name like 'JL%'];
        Userrole[] roles = [Select PortalType, ParentRoleId, Name, Id From UserRole where name like '%T3'];
        
        String profileId = profiles[0].Id;
        String userRoleId = roles[0].Id;
        String permId = perms[0].Id;
        String licId = perms[0].UserLicenseId;
        
        User u1;
        

        System.runAs ( new User(Id = UserInfo.getUserId()) ) {
        // Profile profile = []
            u1 = new User(
            Username ='testuser@jlp.johnlewis.com',
            firstName = 'FirstName', 
            LastName = 'LastName', 
            Email = 'test@test.com', 
            Alias = 'dummy1', 
            CommunityNickname = 'dummy1.xx.zz.johnlewis.com', 
            TimeZoneSidKey = 'Europe/London', 
            LocaleSidKey = 'en_GB', 
            EmailEncodingKey = 'ISO-8859-1',
            ProfileId = profileId,
            UserRoleId = userRoleId,  
            LanguageLocaleKey = 'en_US',
            Team__c='Branch - CST Poole',
            Net_Dom_ID__c='1234');
            insert u1;
        } 
        
        JLActivationController jlactivationCon = new JLActivationController();
        jlactivationCon.firstName ='FirstName';  
        jlactivationCon.lastName ='LastName'; 
        jlactivationCon.confirmUser = true;
        jlactivationCon.partnerId = '1234';
        jlactivationCon.question= system.label.ActSite_pet_name;
        jlactivationCon.answer='1234';
        jlactivationCon.agentUsername= 'testuser@jlp.johnlewis.com';
        jlactivationCon.IsEditMode= true;
     
        
        jlactivationCon.continueToAgentScreen();
        jlactivationCon.getSecurityQuestions();
        jlactivationCon.checkUserDetails();
        jlactivationCon.saveUserSecurityDetails();
        jlactivationCon.checkAgentDetails();
        jlactivationCon.verifyAgentSecurityDetails();
        jlactivationCon.Cancel();
        
     
        
        
        JLActivationController jlactivationCon1 = new JLActivationController();
        jlactivationCon1.firstName ='FirstName';  
        jlactivationCon1.lastName ='LastName'; 
        jlactivationCon1.confirmUser = true;
        jlactivationCon1.partnerId = '12345';
        jlactivationCon1.question= system.label.ActSite_pet_name;
        jlactivationCon1.answer='12345';
        jlactivationCon1.agentUsername= 'testuser1@jlp.johnlewis.com';
        jlactivationCon1.IsEditMode= true;
        
       
        
        jlactivationCon1.continueToAgentScreen();
        jlactivationCon1.checkUserDetails();
        //jlactivationCon1.saveUserSecurityDetails();
        jlactivationCon1.checkAgentDetails();
        //jlactivationCon1.verifyAgentSecurityDetails();
        PageReference cancelRef= jlactivationCon1.Cancel();
        
        system.assertEquals(system.label.ActSite_home_page_url,cancelRef.getUrl());
        
       
          
    }
}