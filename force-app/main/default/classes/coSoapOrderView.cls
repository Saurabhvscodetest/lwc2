/*************************************************
coSoapOrderView

class to encapsulate soap requests

Author: Steven Loftus (MakePositive)
Created Date: 12/11/2014
Modification Date: 
Modified By: 
Edit    By      Date        Description
001     NTJ     17/12/14    OCEP-1752 - Problems parsing an empty child e.g. <ProductCode/> so use a 'pointer' safe method
002     NTJ     22/12/14    OCEP-1752 - Fixed GetGrandchildString to get text
003     KG      11/06/15    Parsing XML Response to include new fields to display as part of OM Tickets - CMP-66,CMP-67,CMP-68,MP-84,CMP-85,CMP-86,CMP-87
004     MP      06/07/15    CMP-479   - Change the history Total Amount fields to use new getSignedDecimal method so negatives are not missed. 
										Also changed quantity so that it cannot be negative.
**************************************************/
public class coSoapOrderView {

    // public parameters to use in the search
    public String OrderId {get; set;}

    // public result parameters
    public String Status {get; set;}
    public ErrorResult Error {get; set;}
    public coOrderDetail Order {get; set;}

    public String getSoapRequest() {

        // always need to have a first and last name
        String soapRequest =    '<?xml version="1.0" encoding="UTF-8"?>'+
                                '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://soa.johnlewispartnership.com/service/acs/ocip/Transaction/CustomerOrder/CustomerOrder/ViewCustomerOrder/v2">' +
                                   '<soapenv:Header/>' +
                                   '<soapenv:Body>' +
                                      '<v1:GetCustomerOrder>' +
                                         '<v1:ApplicationArea>' +
                                            '<MessageID>' + coSoapHelper.generateGuid() + '</MessageID>' +
                                            '<TrackingID>' + coSoapHelper.generateGuid() + '</TrackingID>' +
                                         '</v1:ApplicationArea>' +
                                         '<DataArea>' +
                                            '<Order>' +
                                               '<CustomerOrderId>' + this.OrderId + '</CustomerOrderId>' +
                                            '</Order>' +
                                         '</DataArea>' +
                                      '</v1:GetCustomerOrder>' +
                                   '</soapenv:Body>' +
                                '</soapenv:Envelope>';

        // return the built soap request
        return soapRequest;
    }

    // called to parse the soap response from the service
    public void setSoapResponse(string soapResponseDocument) {

        Dom.Document doc = new Dom.Document();
        
        doc.load(soapResponseDocument);

        Dom.XMLNode rootNode = doc.getRootElement();

        // get all the namespaces out of the response
        String serverEndPoint = 'http://soa.johnlewispartnership.com/';

        String viewcordNS = serverEndPoint + 'service/acs/ocip/Transaction/CustomerOrder/CustomerOrder/ViewCustomerOrder/v2';
                                              
        String soapenvNS = 'http://schemas.xmlsoap.org/soap/envelope/';
        String xsiNS = 'http://www.w3.org/2001/XMLSchema-instance';

        try {

            // get the dataarea and show nodes
            Dom.XMLNode dataAreaNode = rootNode.getChildElement('Body', soapenvNS).getChildElement('ShowCustomerOrder', viewcordNS).getChildElement('DataArea', null);
            Dom.XMLNode showNode = dataAreaNode.getChildElement('Show', null);

            // if there is a problem then get the error details and return
            if (rootNode.getChildElement('Body', soapenvNS).getChildElement('Fault', soapenvNS) != null) {

                dealWithFaultNode(rootNode.getChildElement('Body', soapenvNS).getChildElement('Fault', soapenvNS));
                return;
            }
            else{
                this.Status = showNode.getAttributeValue('responseCode', '');
            }

            // only an error should be reported - a warning indicates that comments will not be available if the code says PARTIAL_DATA_READ
            if (this.Status == 'Error' || (this.Status == 'Warning' && showNode.getChildElement('ServiceError', null).getChildElement('Code', null).getText() != 'PARTIAL_DATA_READ')) {
                setErrorDetails(showNode.getChildElement('ServiceError', null));
                return;
            }

            // if we get here then there is no error and we may have an order
            Dom.XMLNode orderNode = dataAreaNode.getChildElement('Order', null);

            this.Order = new coOrderDetail();
            this.Order.OrderNumber = orderNode.getChildElement('OrderNumber', null).getText();

            system.debug('this.Order.OrderNumber [' + this.Order.OrderNumber + ']');

            this.Order.OrderDate = Date.valueOf(orderNode.getChildElement('DatePlaced', null).getText().removeEnd('Z'));
            system.debug('this.Order.OrderDate [' + this.Order.OrderDate + ']');
            //003     KG      11/06/15    Parsing XML Response to include new fields to display as part of OM Tickets - CMP-66,CMP-67,CMP-68,MP-84,CMP-85,CMP-86,CMP-87
            if(orderNode.getChildElement('InternalOrderStatus', null) != null){
            this.Order.OrderStatusCode = orderNode.getChildElement('InternalOrderStatus', null).getText();
            }
            if(orderNode.getChildElement('InternalOrderStatusDescription', null) != null){
            this.Order.OrderStatusDescription = orderNode.getChildElement('InternalOrderStatusDescription', null).getText();
            }
            this.Order.TotalOrderValue = coSoapHelper.getDecimal(orderNode.getChildElement('GrandTotal', null).getText());
            
           

            // get the billing address - assumption is there is only 1 at the time of coding
            this.Order.BillingAddress = new coOrderDetail.AddressType();
            this.Order.BillingAddress.LastName = orderNode.getChildElement('CustomerBillingLastName', null).getText();
            this.Order.BillingAddress.FirstName = orderNode.getChildElement('CustomerBillingFirstName', null).getText();
            this.Order.BillingAddress.Title = orderNode.getChildElement('CustomerBillingTitle', null).getText();
            this.Order.BillingAddress.Street1 = orderNode.getChildElement('CustomerBillingStreet1', null).getText();
            this.Order.BillingAddress.Street2 = orderNode.getChildElement('CustomerBillingStreet2', null).getText();
            this.Order.BillingAddress.Street3 = orderNode.getChildElement('CustomerBillingStreet3', null).getText();
            this.Order.BillingAddress.Street4 = orderNode.getChildElement('CustomerBillingStreet4', null).getText();
            this.Order.BillingAddress.City = orderNode.getChildElement('CustomerBillingCity', null).getText();
            this.Order.BillingAddress.County = orderNode.getChildElement('CustomerBillingCounty', null).getText();
            this.Order.BillingAddress.Country = orderNode.getChildElement('CustomerBillingCountry', null).getText();
            this.Order.BillingAddress.Phone = orderNode.getChildElement('CustomerBillingPhone', null).getText();
            this.Order.BillingAddress.PhoneEvening = orderNode.getChildElement('CustomerBillingPhoneEvening', null).getText();
            this.Order.BillingAddress.Email = orderNode.getChildElement('CustomerBillingEmail', null).getText();
            this.Order.BillingAddress.PostCode = orderNode.getChildElement('CustomerBillingPostCode', null).getText();

            // get the shipping address - assumption is there is only 1 at the time of coding
            Dom.XMLNode shippingNode = orderNode.getChildElement('ShippingAddress', null);
            this.Order.ShippingAddress = new coOrderDetail.AddressType();
            this.Order.ShippingAddress.LastName = shippingNode.getChildElement('LastName', null).getText();
            this.Order.ShippingAddress.FirstName = shippingNode.getChildElement('FirstName', null).getText();
            this.Order.ShippingAddress.Title = shippingNode.getChildElement('Title', null).getText();
            this.Order.ShippingAddress.Company = shippingNode.getChildElement('Company', null).getText();
            this.Order.ShippingAddress.HouseNumberOrName = shippingNode.getChildElement('HouseNumberOrName', null).getText();
            this.Order.ShippingAddress.Street1 = shippingNode.getChildElement('StreetName', null).getText();
            this.Order.ShippingAddress.Street2 = shippingNode.getChildElement('Street1', null).getText();
            this.Order.ShippingAddress.Street3 = shippingNode.getChildElement('Street2', null).getText();
            this.Order.ShippingAddress.Street4 = shippingNode.getChildElement('Street3', null).getText();
            this.Order.ShippingAddress.City = shippingNode.getChildElement('City', null).getText();
            this.Order.ShippingAddress.County = shippingNode.getChildElement('County', null).getText();
            this.Order.ShippingAddress.Country = shippingNode.getChildElement('Country', null).getText();
            this.Order.ShippingAddress.Phone = shippingNode.getChildElement('Phone', null).getText();
            this.Order.ShippingAddress.PhoneEvening = shippingNode.getChildElement('PhoneEvening', null).getText();
            this.Order.ShippingAddress.PostCode = shippingNode.getChildElement('Zip', null).getText();

            // get the tenders - 0 to many
            Dom.XMLNode tendersNode = orderNode.getChildElement('Tenders', null);
            for (Dom.XMLNode tenderNode : tendersNode.getChildElements()) {

                coOrderDetail.TenderType tender = new coOrderDetail.TenderType();
                if (tenderNode.getChildElement('CardHolderName', null) != null) tender.CardHolderName = tenderNode.getChildElement('CardHolderName', null).getText();
                if (tenderNode.getChildElement('CardNumber', null) != null) tender.CardNumber = tenderNode.getChildElement('CardNumber', null).getText();
                if (tenderNode.getChildElement('ExpiryDateMonth', null) != null && tenderNode.getChildElement('ExpiryDateYear', null) != null) tender.ExpiryDate = tenderNode.getChildElement('ExpiryDateMonth', null).getText() + '/' + tenderNode.getChildElement('ExpiryDateYear', null).getText();
                system.debug('tender.ExpiryDate [' + tender.ExpiryDate + ']');
                if (tenderNode.getChildElement('TenderAmount', null) != null) tender.TenderAmount = coSoapHelper.getDecimal(tenderNode.getChildElement('TenderAmount', null).getText());
                if (tenderNode.getChildElement('StartDateMonth', null) != null && tenderNode.getChildElement('StartDateYear', null) != null) tender.StartDate = tenderNode.getChildElement('StartDateMonth', null).getText() + '/' + tenderNode.getChildElement('StartDateYear', null).getText();
                system.debug('tender.StartDate [' + tender.StartDate + ']');
                tender.CardType = tenderNode.getChildElement('TenderDescription', null).getText();

                this.Order.addTender(tender);
            }

            // get the batches
            Dom.XMLNode batchesNode = orderNode.getChildElement('Batches', null);
            for (Dom.XMLNode batchNode : batchesNode.getChildElements()) {

                // add the batch into the order
                this.Order.addBatch(getBatchData(batchNode));
            }

            // get any emails for the order
            Dom.XMLNode emailsNode = orderNode.getChildElement('OrderEmails', null);
            if (emailsNode != null) {
                for (Dom.XMLNode emailNode : emailsNode.getChildElements()) {

                    coOrderDetail.EmailType email = new coOrderDetail.EmailType();
                    email.MessageQueueId = emailNode.getChildElement('MessageQueueId', null).getText();
                    email.RecipientEmail = emailNode.getChildElement('RecipientEmailAddress', null).getText();
                    email.SentDate = coSoapHelper.getDateTime(emailNode.getChildElement('DateSent', null).getText());
                    system.debug('email.SentDate [' + email.SentDate + ']');
                    email.Subject = emailNode.getChildElement('Subject', null).getText();

                    this.Order.addEmail(email);

                    system.debug('email [' + email + ']');
                }
            }
            //003     KG      11/06/15    Parsing XML Response to include new fields to display as part of OM Tickets - CMP-66,CMP-67,CMP-68,MP-84,CMP-85,CMP-86,CMP-87
            //get the Payment History for the Order
            Dom.XMLNode PaymentsNode = orderNode.getChildElement('PaymentHistory',null);
            for ( Dom.XMLNode PaymentNode : PaymentsNode.getChildElements()) {
               if (PaymentNode.getName() == 'PaymentDetails') {
                this.Order.addPayment(getPaymentData(PaymentNode));
                system.debug('PaymentNode [' + PaymentNode + ']');
             }
           } 
           //003     KG      11/06/15    Parsing XML Response to include new fields to display as part of OM Tickets - CMP-66,CMP-67,CMP-68,MP-84,CMP-85,CMP-86,CMP-87
            //get the Payment History Details for the Order
           Dom.XMLNode HistoryDetailsNode = orderNode.getChildElement('PaymentHistory',null);
            
            for ( Dom.XMLNode DetailNode : HistoryDetailsNode.getChildElements()) { // PaymentHistoryDetails
                for(Dom.XMLNode HistoryNode : DetailNode.getChildElements()){ // Transaction
                    coOrderDetail.OrderHistoryDisplayRecord dispRec; // <<MP>>
                    if (HistoryNode.getName() == 'Transaction') {
                        dispRec = this.Order.getOrderHistoryDisplayRecord(); //<<MP>>
                        this.Order.addHistory(getHistoryData(HistoryNode));
                        dispRec.addTopLevel(getHistoryData(HistoryNode)); //<<MP>>
                        system.debug('HistoryNode [' + HistoryNode + ']');
                    }
                    for ( Dom.XMLNode TransNode : HistoryNode.getChildElements()) { // Transaction Details
                        for ( Dom.XMLNode TransactionNode : TransNode.getChildElements()) { //Transaction Detail
                            if(TransactionNode.getName() == 'TransactionDetail'){
                                this.Order.addTransaction(getTransactionData(TransactionNode));
                                if (dispRec != null) {
                                    dispRec.addDetailLevel(getTransactionData(TransactionNode)); //<<MP>>
                                }
                                system.debug('TransactionNode [' + TransactionNode + ']');
                            }                        
                        }
                    }                 
                }
            }
            //003     KG      11/06/15    Parsing XML Response to include new fields to display as part of OM Tickets - CMP-66,CMP-67,CMP-68,MP-84,CMP-85,CMP-86,CMP-87
            //004     KG      03/07/15    Parsing XML Response to include GiftCardHistory as part of OM Tickets - CMP-66,CMP-67,CMP-68,MP-84,CMP-85,CMP-86,CMP-87
            //get the Payment History Details for the Order
           Dom.XMLNode GiftCardDetailsNode = orderNode.getChildElement('PaymentHistory',null);
            
            for ( Dom.XMLNode DetailNode : GiftCardDetailsNode.getChildElements()) { // GiftCardHistory
                for(Dom.XMLNode GiftCardNode : DetailNode.getChildElements()){ // GiftCardTransactionHistory
                    if (GiftCardNode.getName() == 'GiftCardTransactionHistory') {
                        this.Order.addGiftCard(getGiftDetailsData(GiftCardNode));
                        system.debug('GiftCardNode [' + GiftCardNode + ']');
                    }
                } 
            }
            // sort the list into descending order
            if (!(this.Order.displayList == null || this.Order.displayList.isEmpty())) {
                this.Order.displayList.sort();
            } 
                                  
            // check status here for a warning and don't bother looking - partial data read indicated
            if (this.Status != 'Warning') {

                Dom.XMLNode commentsNode = orderNode.getChildElement('OrderComments', null);
                if (commentsNode != null) {
                    for (Dom.XMLNode commentNode : commentsNode.getChildElements()) {

                        coOrderDetail.CommentType comment = new coOrderDetail.CommentType();
                        comment.OrderComments = commentNode.getChildElement('Comment', null).getText();
                        comment.CommentTime = coSoapHelper.getDateTime(commentNode.getChildElement('CommentDateTime', null).getText());
                        system.debug('comment.CommentTime [' + comment.CommentTime + ']');
                        comment.EnteredByName = commentNode.getChildElement('CommentAgent', null).getText();
                        comment.EnteredByPosition = commentNode.getChildElement('AgentPosition', null).getText();

                        this.Order.addComment(comment);
                    }
                }
            }

        } catch (Exception e) {

            this.Status = 'Error';
            // catch all in case we try to access a node we think should be there but it is not
            setErrorDetails('INTERNAL_ERROR', 'Technical', 'An exception was generated during the parsing of the soap response: ' + e.getMessage());
        }
    }
   
   //003     KG      11/06/15    Parsing XML Response to include new fields to display as part of OM Tickets - CMP-66,CMP-67,CMP-68,MP-84,CMP-85,CMP-86,CMP-87   
   // get Payment Details to display in Payment Details Section 
    private coOrderDetail.PaymentType getPaymentData(Dom.XMLNode paymentNode) {

        coOrderDetail.PaymentType payment = new coOrderDetail.PaymentType();
        if(paymentNode.getChildElement('BillingEmail', null) != null){
            payment.BillingEmail = paymentNode.getChildElement('BillingEmail', null).getText();
        }
        if(paymentNode.getChildElement('Title', null) != null){
            payment.Title = paymentNode.getChildElement('Title', null).getText();
        }
        if(paymentNode.getChildElement('FirstName', null) != null){
            payment.FirstName = paymentNode.getChildElement('FirstName', null).getText();
        }
        if(paymentNode.getChildElement('LastName', null) != null){
            payment.LastName = paymentNode.getChildElement('LastName', null).getText();
        }
        if(paymentNode.getChildElement('CompanyName', null) != null){
            payment.CompanyName = paymentNode.getChildElement('CompanyName', null).getText();
        }
        if(paymentNode.getChildElement('HouseNumberOrName', null) != null){
            payment.HouseNumberOrName = paymentNode.getChildElement('HouseNumberOrName', null).getText();
        }
        if(paymentNode.getChildElement('StreetName', null) != null){
            payment.StreetName = paymentNode.getChildElement('StreetName', null).getText();
        }
        if(paymentNode.getChildElement('AddressLine2', null) != null){
            payment.AddressLine2 = paymentNode.getChildElement('AddressLine2', null).getText();
        }
        if(paymentNode.getChildElement('AddressLine3', null) != null){
            payment.AddressLine3 = paymentNode.getChildElement('AddressLine3', null).getText();
        }
        if(paymentNode.getChildElement('AddressLine4', null) != null){
            payment.AddressLine4 = paymentNode.getChildElement('AddressLine4', null).getText();
        }
        if(paymentNode.getChildElement('Town', null) != null){
            payment.Town = paymentNode.getChildElement('Town', null).getText();
        }
        if(paymentNode.getChildElement('County', null) != null){
            payment.County = paymentNode.getChildElement('County', null).getText();
        }
        if(paymentNode.getChildElement('PostCode', null) != null){
            payment.PostCode = paymentNode.getChildElement('PostCode', null).getText();
        }
        if(paymentNode.getChildElement('Country', null) != null){
            payment.Country = paymentNode.getChildElement('Country', null).getText();
        }
        if(coSoapHelper.getChildString(paymentNode,'DaytimeTelephoneNumber', null) != null){
            payment.DayTimeTelephoneNumber = coSoapHelper.getChildString(paymentNode,'DaytimeTelephoneNumber', null);
        }
        if(paymentNode.getChildElement('EveningTelephoneNumber', null) != null){
            payment.EveningTelephoneNumber = coSoapHelper.getChildString(paymentNode,'EveningTelephoneNumber', null);
        }
        if(paymentNode.getChildElement('DateOfBirth', null) != null){
            payment.DateOfBirth = Date.valueOf(paymentNode.getChildElement('DateOfBirth', null).getText());
            system.debug('payment.DateOfBirth [' + payment.DateOfBirth + ']');
        }
        if(paymentNode.getChildElement('PaymentMethod', null) != null){
            payment.PaymentMethod = paymentNode.getChildElement('PaymentMethod', null).getText();
        }
        if(paymentNode.getChildElement('NameOnCreditCard', null) != null){
            payment.NameOnCreditCard = paymentNode.getChildElement('NameOnCreditCard', null).getText();
        }
        if(paymentNode.getChildElement('MaskedCardNumber', null) != null){
            payment.MaskedCardNumber = paymentNode.getChildElement('MaskedCardNumber', null).getText();
        }
        if(paymentNode.getChildElement('ExpiryMonth', null) != null){
            payment.ExpiryMonth = paymentNode.getChildElement('ExpiryMonth', null).getText();
        }
        if(paymentNode.getChildElement('ExpiryYear', null) != null){
            payment.ExpiryYear = paymentNode.getChildElement('ExpiryYear', null).getText();
        }
        if(paymentNode.getChildElement('StartMonth', null) != null){
            payment.StartMonth = paymentNode.getChildElement('StartMonth', null).getText();
        }
        if(paymentNode.getChildElement('StartYear', null) != null){
            payment.StartYear = paymentNode.getChildElement('StartYear', null).getText();
        }
        if(paymentNode.getChildElement('IssueNumber', null) != null){
            payment.IssueNumber = paymentNode.getChildElement('IssueNumber', null).getText();
        }
        if(paymentNode.getChildElement('SmartPayID', null) != null){
            payment.SmartPayID = paymentNode.getChildElement('SmartPayID', null).getText();
        }
        if(paymentNode.getChildElement('Currency', null) != null){
            payment.Curency = paymentNode.getChildElement('Currency', null).getText();
        }
        if(paymentNode.getChildElement('PayPalPayerId', null) != null){
            payment.PayPalPayerId = paymentNode.getChildElement('PayPalPayerId', null).getText();
        }
        if(paymentNode.getChildElement('PayPalStatus', null) != null){
            payment.PayPalStatus = paymentNode.getChildElement('PayPalStatus', null).getText();
        }
      return payment;  
    }
    // get Transaction to display in Transaction table
     private coOrderDetail.HistoryType getHistoryData(Dom.XMLNode historyNode) {
        
        coOrderDetail.HistoryType toplevel = new coOrderDetail.HistoryType();
        if(historyNode.getchildElement('TransactionDate',null) != null){
            toplevel.TransactionDate = coSoapHelper.getDateTime(coSoapHelper.getchildString(historyNode,'TransactionDate',null));
            system.debug('toplevel.TransactionDate [' + toplevel.TransactionDate + ']');
        }
        if(coSoapHelper.getInteger(coSoapHelper.getChildString(historyNode,'OrderNumber',null))!= null) {
            toplevel.OrderNumber = coSoapHelper.getInteger(coSoapHelper.getChildString(historyNode,'OrderNumber',null));
            system.debug('toplevel.OrderNumber [' + toplevel.OrderNumber + ']');
        }
        if(historyNode.getchildElement('TransactionMethod', null) != null){
            toplevel.TransactionMethod = coSoapHelper.getChildString(historyNode,'TransactionMethod', null);
        } 
        if(historyNode.getchildElement('AuthCode', null) != null){
            toplevel.AuthCode = historyNode.getChildElement('AuthCode',null).getText();
        }
        if(historyNode.getchildElement('AVSCode', null) != null){
            toplevel.AVSCode = historyNode.getChildElement('AVSCode',null).getText();
        }
        if(historyNode.getchildElement('TransactionType', null) != null){
            toplevel.TransactionType = historyNode.getChildElement('TransactionType', null).getText();
        }
        // <<04>> Changed to use getSignedDecimal
        if(coSoapHelper.getSignedDecimal(coSoapHelper.getChildString(historyNode,'TotalAmount', null)) != null) {        
            toplevel.TotalAmount = coSoapHelper.getSignedDecimal(coSoapHelper.getChildString(historyNode,'TotalAmount', null));
            system.debug('toplevel.TotalAmount [' + toplevel.TotalAmount + ']'); 
        }        
        if(historyNode.getchildElement('TransactionResult', null) != null){
            toplevel.TransactionResult = historyNode.getChildElement('TransactionResult',null).getText();
        }
        if(historyNode.getchildElement('Reason', null) != null){
            toplevel.Reason = historyNode.getChildElement('Reason',null).getText();
        }
        if(historyNode.getchildElement('Authentication', null) != null){
            toplevel.Authentication = historyNode.getChildElement('Authentication',null).getText();
        }
        if(coSoapHelper.getInteger(coSoapHelper.getChildString(historyNode,'TransactionID',null)) != null) {
            toplevel.TransactionID = coSoapHelper.getInteger(coSoapHelper.getChildString(historyNode,'TransactionID',null));
        }
        //Dom.XMLNode TransactionDetailsNode = historyNode.getChildElement('Transaction',null); 
      return toplevel;
    }
    
    // get Transaction Detail Information to display in Transaction Detail Table
    private coOrderDetail.TransactionType getTransactionData(Dom.XMLNode transactionNode) {
        coOrderDetail.TransactionType detailLevel = new coOrderDetail.TransactionType();
        if(coSoapHelper.getInteger(coSoapHelper.getChildString(TransactionNode,'OrderNumber',null)) != null) {
            detailLevel.OrderNumbers = coSoapHelper.getInteger(coSoapHelper.getChildString(TransactionNode,'OrderNumber',null));
        }
        if(TransactionNode.getChildElement('Description',null) != null){
            detailLevel.Description = TransactionNode.getChildElement('Description',null).getText();
        }
        if(TransactionNode.getChildElement('Type',null) != null) {
            detailLevel.Type = TransactionNode.getChildElement('Type',null).getText();
        }
        if(coSoapHelper.getInteger(coSoapHelper.getChildString(TransactionNode,'Quantity',null)) != null) {
            // <<04>> Reverse sign on negative quantities
            Integer qty = coSoapHelper.getInteger(coSoapHelper.getChildString(TransactionNode,'Quantity',null));
            if (qty < 0) {
            	qty *= -1;
            }
            // detailLevel.Quantity = coSoapHelper.getInteger(coSoapHelper.getChildString(TransactionNode,'Quantity',null));
            detailLevel.Quantity = qty;
        }
        // <<04>> Changed to use getSignedDecimal
        if(coSoapHelper.getSignedDecimal(coSoapHelper.getChildString(TransactionNode,'TotalAmount', null)) != null) {
            detailLevel.TotalAmount = coSoapHelper.getSignedDecimal(coSoapHelper.getChildString(TransactionNode,'TotalAmount', null));
            system.debug('detailLevel.TotalAmount [' + detailLevel.TotalAmount + ']');
        }
        if(coSoapHelper.getDecimal(coSoapHelper.getChildString(TransactionNode,'TotalVoucher', null)) != null) {
            detailLevel.TotalVoucher = coSoapHelper.getDecimal(coSoapHelper.getChildString(TransactionNode,'TotalVoucher', null));
        }
        if(coSoapHelper.getDecimal(coSoapHelper.getChildString(TransactionNode,'TotalGiftCard', null)) != null) {
            detailLevel.TotalGiftCard = coSoapHelper.getDecimal(coSoapHelper.getChildString(TransactionNode,'TotalGiftCard', null));
        }
        if(coSoapHelper.getDecimal(coSoapHelper.getChildString(TransactionNode,'TotalDiscount', null)) != null) {
            detailLevel.TotalDiscount = coSoapHelper.getDecimal(coSoapHelper.getChildString(TransactionNode,'TotalDiscount', null));
        }
        if(coSoapHelper.getInteger(coSoapHelper.getChildString(TransactionNode,'ReceiptItemID', null)) != null) {
            detailLevel.ReceiptItemID = coSoapHelper.getInteger(coSoapHelper.getChildString(TransactionNode,'ReceiptItemID', null));
        }
     return detailLevel;   
    }  
   
   //003     KG      11/06/15    Parsing XML Response to include new fields to display as part of OM Tickets - CMP-66,CMP-67,CMP-68,MP-84,CMP-85,CMP-86,CMP-87 -- End 
    private coOrderDetail.GiftCardType getGiftDetailsData(Dom.XMLNode giftcardNode) {
        coOrderDetail.GiftCardType giftcard = new coOrderDetail.GiftCardType();
        if(giftcardNode.getChildElement('GiftCardNumber', null) != null){
            giftcard.GiftCardNumber = giftcardNode.getChildElement('GiftCardNumber', null).getText();
        }
        if(giftcardNode.getChildElement('TransactionType', null) != null){
            giftcard.TransactionType = giftcardNode.getChildElement('TransactionType', null).getText();
        }
        if(coSoapHelper.getDecimal(coSoapHelper.getChildString(giftcardNode,'Amount', null)) != null) {
            giftcard.Amount = coSoapHelper.getDecimal(coSoapHelper.getChildString(giftcardNode,'Amount', null));
        }
        if(giftcardNode.getChildElement('Result', null) != null){
            giftcard.Result = giftcardNode.getChildElement('Result', null).getText();
        }
        return giftcard;
    }
    
    
    private coOrderDetail.BatchType getBatchData(Dom.XMLNode batchNode) {

        coOrderDetail.BatchType batch = new coOrderDetail.BatchType();
        batch.InternalDeliveryMethodDescription = batchNode.getChildElement('InternalDeliveryMethodDescription', null).getText();
        if(batchNode.getChildElement('DeliveryDate', null) != null){
            batch.DeliveryDate = Date.valueOf(batchNode.getChildElement('DeliveryDate', null).getText());
            
            system.debug('batch.DeliveryDate [' + batch.DeliveryDate + ']');
        }
        if(batchNode.getChildElement('LeadTime', null) != null){
            batch.LeadTime = coSoapHelper.getInteger(batchNode.getChildElement('LeadTime', null).getText());
        }
        
        Dom.XMLNode orderItemsNode = batchNode.getChildElement('OrderItems', null);
        for (Dom.XMLNode orderItemNode : orderItemsNode.getChildElements()) {

            for (Dom.XMLNode childNode : orderItemNode.getChildElements()) {
                system.debug('name: '+childNode.getName());                
                if (childNode.getName() == 'ItemDetail') {

                    batch.addItem(getItemData(childNode));

                } else if (childNode.getName() == 'ChildProducts') {

                    for (Dom.XMLNode childOrderItemNode : childNode.getChildElements()) {
                        system.debug('childOrderItemNode'+childOrderItemNode);
                        for (Dom.XMLNode childItemdetailNode : childOrderItemNode.getChildElements()) {
                            system.debug('childItemdetailNode'+childItemdetailNode);
                            batch.addItem(getItemData(childItemDetailNode));
                        }
                    }
                }
            }
        }  

        return batch;
    }

    private coOrderDetail.ItemType getItemData(Dom.XMLNode itemDetailNode) {

        coOrderDetail.ItemType item = new coOrderDetail.ItemType();
        // old code
        /*
        item.ProductCode = itemDetailNode.getChildElement('ProductCode', null).getText();
        item.Item = itemDetailNode.getChildElement('OrderItemTitle', null).getText();
        item.UnitPrice = coSoapHelper.getDecimal(itemDetailNode.getChildElement('PriceIncVAT', null).getText());
        item.UnitPartnerDiscount = coSoapHelper.getDecimal(itemDetailNode.getChildElement('UnitPartnerDiscount', null).getText());
        item.QuantityOrdered = coSoapHelper.getInteger(itemDetailNode.getChildElement('OrderedQuantity', null).getChildElement('Quantity', null).getText());
        Decimal totalPriceIncVAT = coSoapHelper.getDecimal(itemDetailNode.getChildElement('TotalPriceIncVAT', null).getText());
        Decimal totalPartnerDiscount = coSoapHelper.getDecimal(itemDetailNode.getChildElement('TotalPartnerDiscount', null).getText());
        item.TotalPrice = (totalPartnerDiscount == null) ? totalPriceIncVAT : totalPriceIncVAT - totalPartnerDiscount;
        item.Status = itemDetailNode.getChildElement('OrderedQuantity', null).getChildElement('Status', null).getText();
        //if there is consigment date then this is the shipped date and means that the shipped quantity should be set to the ordered quantity
        if (itemDetailNode.getChildElement('ConsignmentTracking', null).getChildElement('ConsignmentDate', null) != null) {
            item.DateShipped = Date.valueOf(itemDetailNode.getChildElement('ConsignmentTracking', null).getChildElement('ConsignmentDate', null).getText().removeEnd('Z'));
            item.QuantityShipped = item.QuantityOrdered;
        }

        // get the quantity returned if it is there
        if (itemDetailNode.getChildElement('ReturnedQuantity', null) != null) {     
            item.QuantityReturned = coSoapHelper.getInteger(itemDetailNode.getChildElement('ReturnedQuantity', null).getChildElement('Quantity', null).getText());
        }
        */
        // start 001 code
        item.ProductCode = coSoapHelper.getChildString(itemDetailNode, 'ProductCode', null);
        item.Item = coSoapHelper.getChildString(itemDetailNode, 'OrderItemTitle', null);
        //003     KG      11/06/15    Parsing XML Response to include new fields to display as part of OM Tickets - CMP-66,CMP-67,CMP-68,MP-84,CMP-85,CMP-86,CMP-87
        if(coSoapHelper.getChildString(itemDetailNode, 'Distributor', null) != null) {
        item.Distributor = coSoapHelper.getChildString(itemDetailNode, 'Distributor', null);
        }
        item.UnitPrice = coSoapHelper.getDecimal(coSoapHelper.getChildString(itemDetailNode, 'PriceIncVAT', null));
        item.UnitPartnerDiscount = coSoapHelper.getDecimal(coSoapHelper.getChildString(itemDetailNode, 'UnitPartnerDiscount', null));
        item.QuantityOrdered = coSoapHelper.getInteger(coSoapHelper.getGrandChildString(itemDetailNode, 'OrderedQuantity', null, 'Quantity', null));
        //003     KG      11/06/15    Parsing XML Response to include new fields to display as part of OM Tickets - CMP-66,CMP-67,CMP-68,MP-84,CMP-85,CMP-86,CMP-87 
        if (coSoapHelper.getChildString(itemDetailNode,'DistributorReference', null) != null) {         
            item.DistributorReference = coSoapHelper.getChildString(itemDetailNode,'DistributorReference', null);  
        }
        if(coSoapHelper.getInteger(coSoapHelper.getChildString(itemDetailNode, 'PurchaseOrderID', null)) != null) {      
        item.PurchaseOrderId = coSoapHelper.getInteger(coSoapHelper.getChildString(itemDetailNode, 'PurchaseOrderID', null)); 
        }
        //003     KG      11/06/15    Parsing XML Response to include new fields to display as part of OM Tickets - CMP-66,CMP-67,CMP-68,MP-84,CMP-85,CMP-86,CMP-87 --End       
        Decimal totalPriceIncVAT = coSoapHelper.getDecimal(coSoapHelper.getChildString(itemDetailNode, 'TotalPriceIncVAT', null));
        Decimal totalPartnerDiscount = coSoapHelper.getDecimal(coSoapHelper.getChildString(itemDetailNode, 'TotalPartnerDiscount', null));
        item.TotalPrice = (totalPartnerDiscount == null) ? totalPriceIncVAT : totalPriceIncVAT - totalPartnerDiscount;
        item.Status = coSoapHelper.getGrandChildString(itemDetailNode, 'OrderedQuantity', null, 'Status', null);
        // if there is consigment date then this is the shipped date and means that the shipped quantity should be set to the ordered quantity
        if (coSoapHelper.getGrandChild(itemDetailNode, 'ConsignmentTracking', null, 'ConsignmentDate', null) != null) {
            item.DateShipped = Date.valueOf(itemDetailNode.getChildElement('ConsignmentDate', null));
            system.debug('item.DateShipped [' + item.DateShipped + ']');
            
            item.QuantityShipped = item.QuantityOrdered;
        }
         //003     KG      11/06/15    Parsing XML Response to include new fields to display as part of OM Tickets - CMP-66,CMP-67,CMP-68,MP-84,CMP-85,CMP-86,CMP-87
         // get the quantity cancelled if it is there 
         if(coSoapHelper.getInteger(coSoapHelper.getGrandChildString(itemDetailNode, 'CancelledQuantity', null, 'Quantity', null)) != null) {
            item.QuantityCancelled = coSoapHelper.getInteger(coSoapHelper.getGrandChildString(itemDetailNode, 'CancelledQuantity', null, 'Quantity', null));
         } 
         
         //003     KG      11/06/15    Parsing XML Response to include new fields to display as part of OM Tickets - CMP-66,CMP-67,CMP-68,MP-84,CMP-85,CMP-86,CMP-87   
        // get the quantity returned if it is there 
        if(coSoapHelper.getInteger(coSoapHelper.getGrandChildString(itemDetailNode, 'ReturnedQuantity', null, 'Quantity', null)) != null) {
            item.QuantityReturned = coSoapHelper.getInteger(coSoapHelper.getGrandChildString(itemDetailNode, 'ReturnedQuantity', null, 'Quantity', null));
        }
        // end 001 code

        return item;
    }

    private void dealWithFaultNode(Dom.XMLNode faultNode) {

        this.Status = 'Error';
        setErrorDetails(faultNode.getChildElement('faultcode', null).getText(), null, faultNode.getChildElement('faultstring', null).getText());
    }

    // override eof setErrorDetails
    private void setErrorDetails(Dom.XMLNode errorNode) {

        system.debug('errorNode [' + errorNode + ']');

        String code = errorNode.getChildElement('Code', null).getText();
        String description = errorNode.getChildElement('Description', null).getText();

        setErrorDetails(code, null, description);
    }

    // set the error details passed in
    private void setErrorDetails(String code, String type, String description) {

        system.debug('code [' + code + '], type [' + type + '], description [' + description + ']');

        this.Error = new ErrorResult();
        this.Error.ErrorCode = code;
        this.Error.ErrorType = type;
        this.Error.ErrorDescription = description;          

        system.debug('this.Error [' + this.Error + ']');
    }

    // class to hold the error
    public class ErrorResult {

        public String ErrorCode {get; set;}
        public String ErrorType {get; set;}
        public String ErrorDescription {get; set;}
    }
}