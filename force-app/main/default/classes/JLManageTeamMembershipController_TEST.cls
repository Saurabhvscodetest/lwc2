/******************************************************************************
* @author       Ranjan Samal
* @date         21/09/2017
* @description  Wrapper class to contain User record and a boolean flag
******************************************************************************/
@isTest
private class JLManageTeamMembershipController_TEST {
    
    @isTest 
    static void test_getAllTeams () {
        
        User sysAdmin = UnitTestDataFactory.getSystemAdministrator('admin1');
        insert sysAdmin;        
        User bookingsManager;
        
        System.runAs(sysAdmin) {
            PageReference pageRef = Page.JLManageTeamMembership;
            Test.setCurrentPage(pageRef);
            JLManageTeamMembershipController controller = new JLManageTeamMembershipController();
            controller.hasCapitaDutyManagerPermissionSet = true;
            controller.hasInHouseTeamManagerPermissionSet = true;
            List<SelectOption> allTeamsOptions = controller.getAllTeams();
            List<SelectOption> getPublicGroups = controller.getPublicGroups();
        }
    }
    
    @isTest 
    static void test_chooseNewTeam () {
             
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];                    
        system.runAs(thisUser ) {
            String FirstName = 'sysAdmin2';
            thisUser = UnitTestDataFactory.getSystemAdministrator('admin1');
            insert thisUser;
            
            PageReference pageRef = Page.JLManageTeamMembership;
            Test.setCurrentPage(pageRef);
            JLManageTeamMembershipController controller = new JLManageTeamMembershipController();
            
            //Simulate choosing a valid team from the dropdown
            UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
            
            Team__c bookingsCS = Team__c.getValues('JL.com - Bookings');
            controller.newTeamId = bookingsCS.id;
            controller.chooseNewTeam();            
        }
       
    }
    
    @isTest 
    static void test_moveUsersToNewTeam () {
    
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];                    
        system.runAs(thisUser ) {
            String FirstName = 'sysAdmin2';
            thisUser = UnitTestDataFactory.getSystemAdministrator('admin1');
            insert thisUser;
            
            Id publicGroupId = [SELECT Id,DeveloperName,Name,OwnerId FROM Group WHERE Name LIKE 'CPM%' LIMIT 1].Id;
            
            PageReference pageRef = Page.JLManageTeamMembership;
            pageref.getparameters().put('usr',(string.ValueOf(thisUser.Id)));
            Test.setCurrentPage(pageRef);
            UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
            
            //Simulate choosing a valid team from the dropdown
            JLManageTeamMembershipController controller = new JLManageTeamMembershipController();
            Team__c bookingsCS = Team__c.getValues('JL.com - Bookings');
            controller.newTeamId = bookingsCS.id;
            controller.chooseNewTeam();
            Boolean isSelected = true;
            JLUserWrapper wrapperController = new JLUserWrapper (thisUser,isSelected);
            controller.JLUserWrappers = new List<JLUserWrapper>();
            controller.JLUserWrappers.add(wrapperController);
            controller.newPublicGrpID = thisUser.ID;
            controller.chooseNewPublicGrp();
            
            Team__c bookings = Team__c.getValues('JL.com - Front Office');
            controller.newTeamId = bookings.id;
            controller.chooseNewTeam();
            
            controller.processSelected();
            //After the Move to Front Office, bookings.agent team should be front office 
            system.assertEquals('JL.com - Front Office',thisUser.Team__c,'BOokings agent 1 primary team should be Front Office !');
        }       
    }
    @isTest 
    static void test_runSearch () {
    
        User thisUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];                    
        system.runAs(thisUser ) {
            String FirstName = 'sysAdmin2';
            thisUser = UnitTestDataFactory.getSystemAdministrator('admin1');
            insert thisUser;

            PageReference pageRef = Page.JLManageTeamMembership;
            pageref.getparameters().put('usr',(string.ValueOf(thisUser.Id)));
            Test.setCurrentPage(pageRef);
            JLManageTeamMembershipController controller = new JLManageTeamMembershipController();
            Boolean isSelected = false;
            JLUserWrapper wrapperController = new JLUserWrapper (thisUser,isSelected);          
            controller.JLUserWrappers = new List<JLUserWrapper>();
            controller.JLUserWrappers.add(wrapperController);
            
            boolean test1 = controller.hasNext;
            boolean test2 = controller.hasPrevious;
            controller.runSearch();
        }
    }
}