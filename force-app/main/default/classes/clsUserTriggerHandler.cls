/******************************************************************************
* @author       Kevin Burke
* @date         14/04/2014
* @description  Class that calls trigger actions for the User object.
*
* EDIT RECORD
*
* LOG   DATE            Author  JIRA        COMMENT      
* 001   23/10/2014      JA					---
* 002   26/04/2015		ES		CMP-208		Linking User.Primary & User.Additional Teams to List Views via Public Groups
* 003   29/04/2015		ES		CMP-206		Override a subordinate user’s primary Team
*											CAPITA Team Manager access to Team Management App to override user primary team.
* 004   30/04/2015		ES		CMP-197		Enable agents to perform additional team roles each using a different profile (tier) to their primary role profile
* 005   13/07/2015		ES		CMP-517		Batch return to permanent team not working. Fixing Phase I method causing "Too Many SOQL" exceptions
* 006   14/07/2015		MP		CMP-521		Add 'In House CC Team Manager' to SOQL
* 007   28/07/2015		MP		CMP-598		Fix exception being thrown when deactivating Tier 3 users
* 008   30/09/2015      MP      COPT-277    Stamp JL Email Address on reopened cases.
* 009   03/12/2015		MP		COPT-466	Automatically assign Permission Sets for Roles / Profiles.  Extensive refactoring of code
* 010   19/05/2016		MP		COPT-657	Complete refactoring of class.
*
*******************************************************************************
*/
public without sharing class clsUserTriggerHandler {
	
	private static final String ACCESS_BUTTON_PERMISSION_NAME = 'Access Button Permission';
	private static final String JL_TIER_3_PERMISSION_NAME = 'JL Tier 3 Permissions';
	private static final String SST_TAB_PERMISSION_NAME = 'SST Tab Permission';
	private static final String TRANSFER_CASES_PERMISSION_NAME = 'Transfer Cases';
	private static final String DUTY_MANAGER_PERMISSION_NAME = 'Duty Manager';
	private static final String IN_HOUSE_CC_TEAM_MANAGER_PERMISSION_NAME = 'In House CC Team Manager';
	private static final String SPECIAL_CIRCUMSTANCES_PERMISSION_NAME = 'Special Circumstances';
	private static final String NO_ROLE_SPECIFIED = 'NO ROLE SPECIFIED';
    private static Map<String, Auto_Assign_Permission_Set__c> autoAssignPSMap;
    private static Map<Id, UserRole> userRoleMap;
    private static Map<Id, Profile> profileMap;
    private static Map<String, PermissionSet> permissionSetNameXrefMap;
    @testVisible private static Boolean mapsInitialised = false;
    
    /**
     * mainEntry method called from the Trigger.
	 * @params:	Standard Trigger context variables. 
	 * @rtnval:	void
     */
    public static void mainEntry(Boolean isBefore, 
                               Boolean isDelete, Boolean isAfter, 
                               Boolean isInsert, Boolean isUpdate, 
                               Boolean isExecuting, List<User> newlist, 
                               Map<ID, Object> inNewMap, List<User> oldlist, 
                               Map<Id, Object> inOldMap) {

        System.debug('@@entering clsUserTriggerHandler');
        
        if (!mapsInitialised) {
        	initialiseStaticMaps();
        }
		
		Map<ID, User> newMap = (Map<ID, User>) inNewMap;
		Map<ID, User> oldMap = (Map<ID, User>) inOldMap;        
        
		// Before trigger operations
        if (isBefore && (isInsert || isUpdate)) {
			handleBeforeInsertsAndUpdates(newList, oldmap, isUpdate); // <<010>>
        }

        // After trigger operations
        if (isAfter && (isInsert || isUpdate)) {
            Map<String, Set<String>> aapsPermissionsToRemoveMap = new Map<String, Set<String>>(); // <<009>> Blank map - this is only used when updating the AAPS record
            assignPermissionSets(oldMap, newMap, isUpdate, aapsPermissionsToRemoveMap); // <<009>>

            //26/04/2015  ES      CMP-208- Linking User.Primary & User.Additional Teams to List Views via Public Groups 
            updatePublicGroupMembership(newList);
        }

        System.debug('@@exiting clsUserTriggerHandler');
    }
    
	/**
	 * <<010>> Perform all field update operations for before trigger.
	 * Combines code from old setProfileAndRole, setBranchTier and setQueueNames methods for single 'for' loop.
	 * @params:	List<User> newList		The list of User records being inserted/updated from trigger.new.
	 * 			Map<Id, User> oldmap	Corresponds to trigger.oldMap.
	 * 			Boolean isUpdate		Corresponds to trigger.isUpdate.
	 * @rtnval:	void
	 */
	private static void handleBeforeInsertsAndUpdates(List<User> newList, Map<Id, User> oldmap, Boolean isUpdate) {
        // FILTERING USERS TO  ENSURE THIS METHOD ONLY SET PROFILE WHEN THE FOLLOWING CONDITIONS ARE MET:
        // 1) A USER IS BEING MOVED BETWEEN 2 TEAMS AND
        // 2) THE TEAM THAT THE USER IS BEING MOVED TO HAS ROLE AND PROFILE IN TEAM CUSTOM SETTING
		List<User> usersMovingPrimaryTeam = new List <User>();
        Map <String, Profile>  teamName2ProfileMap = TeamUtils.getTeamProfileMap();
        Map <String, UserRole> teamName2RoleMap = TeamUtils.getTeamRolesMap();

        for (User u : newList) {
			// setProfileAndRole code Part 1
			if (isUpdate) {
            String previousPrimaryTeam = oldMap.get(u.id).Team__c;
            if (!String.IsBlank(u.Team__c) && !String.IsBlank(previousPrimaryTeam) && 
                u.Team__c!=previousPrimaryTeam && teamName2ProfileMap!=null && 
                teamName2ProfileMap.get(u.Team__c)!=null &&
                !String.IsBlank(teamName2ProfileMap.get(u.Team__c).name)  &&
                teamName2RoleMap!=null && 
                teamName2RoleMap.get(u.Team__c)!=null &&
                !String.IsBlank(teamName2RoleMap.get(u.Team__c).name)) {
                
                    usersMovingPrimaryTeam.add (u);
            }
        }

			// setBranchTier code
			if (u.UserRoleId != null) {
				UserRole ur = userRoleMap.get(u.UserRoleId);

				System.debug('UserRole name');
				System.debug(ur.Name);
				String strTier = getTierFromRole(ur.Name);
				strTier= strTier.replaceAll( '\\s+', '');
				System.debug(strTier);
				try {
					u.Tier__c = Integer.valueOf(strTier.substring(0,1));
				} catch (Exception e) {
					System.debug('role is not in the correct format for auto assigning of tiers');
				}
				u.Branch__c = getBranchFromRole(ur.Name);
			}

			// setQueueNames code
			// ES:13-07-15: SET THE QUEUE NAMES FOR PRIMARY TEAM AND PRIMARY TEAMS AND THEN USE
			List <String> allQueueNames = new List<String>();
			if (!String.IsBlank(u.Team__c)) {
				//try to retrieve queue name from custom setting
				Team__c t = Team__c.getValues(u.Team__c);
				if (t != null) {
					allQueueNames.add (t.Queue_Name__c);
					allQueueNames.addAll(getRelatedQueues(t.Queue_Name__c));
					system.debug('RELATED QUEUES ==>' + getRelatedQueues(t.Queue_Name__c));
				}
			}
			if (!String.IsBlank(u.My_Teams__c)) {
				String [] additionalTeamNames = u.My_Teams__c.split(';');
				for (String additionalTeamName : additionalTeamNames) {
					Team__c t = Team__c.getValues(additionalTeamName);
					if (t != null) {
						allQueueNames.add(t.Queue_Name__c);
						allQueueNames.addAll(getRelatedQueues(t.Queue_Name__c));
						system.debug('RELATED QUEUES ==>' + getRelatedQueues(t.Queue_Name__c));
					}
				}
			}
			if (!allQueueNames.isEmpty()) {

               // De dupe List
               Set<String> uniqueQueues = new Set<String>(allQueueNames);
               String queues = TeamUtils.concatenateStrings(new List<String>(uniqueQueues),',');

               // Error gracefully if field exceeds 255 error limit
               if(queues.length() > 255) {
               		if(System.isBatch()) {
               			String truncatedQueues = queues.subString(0,255);
               			String cleanTruncatedQueues = truncatedQueues.substringBeforeLast(',');
               			u.Queues__c = cleanTruncatedQueues;
               		} 
                   
                   //Removed error message as this blocks the update of users in managed teams app
                   
                   /*
                   else {
						u.My_Teams__c.addError('The 255 character limit on the Queues field has been reached. This is due to the User belonging to multiples Teams and Additional Teams. ' +
								'Please remove one or more Team and Additional Teams to reduce the Queues which the User belongs to. ' +
								'Note: Teams which use Omni have many Queues, and can use a significant amount of the character limit.');
					}
                   
                   */
               } else {
					u.Queues__c = queues;
               }
            }
			
			u.My_Teams_Text__c = '';
			if (u.Team__c != null) {
				u.My_Teams_Text__c = u.Team__c;
			}
			if (u.My_Teams__c != null && u.My_Teams_Text__c.length() > 3) {
				// add in a semicolon as separater as more items will follow
				if (u.My_Teams_Text__c != null) {
					u.My_Teams_Text__c += ';';
				}
				u.My_Teams_Text__c += u.My_Teams__c;
			}
		}
            
		// setProfileAndRole code Part 2
		if (!usersMovingPrimaryTeam.isEmpty()) {
            for (User u : usersMovingPrimaryTeam) {
				if (teamName2ProfileMap.containsKey(u.Team__c)) {
                Profile profileForNewTeam = teamName2ProfileMap.get(u.Team__c);
                    u.profileId = profileForNewTeam.id;
                }
				if (teamName2RoleMap.containsKey(u.Team__c)) {
                UserRole roleForNewTeam = teamName2RoleMap.get(u.Team__c);
                    u.userroleid = roleForNewTeam.id;
                }
            }
        }
    }
    
    //Add the additional queues.
    @testVisible
    private static list<String> getRelatedQueues(String queueName) {

    	list<String> relatedQueuesList = new list<String>();

    	//Only run if this is a base queue
    	for(TeamUtils.QUEUE_TYPES qType : TeamUtils.QUEUE_TYPES.values()) {
    		if(queueName.endsWithIgnoreCase(TeamUtils.SEPARATOR + qType.Name())) {
    			return relatedQueuesList;
    		}
    	}

    	Id defaultQueueid = TeamUtils.getQueueId(queueName);
        //Commented as a part of COPT-3585
    	//Id warningQueueId = TeamUtils.getQueueId(defaultQueueid, TeamUtils.QUEUE_TYPES.WARNING);
    	Id actionedQueueId = TeamUtils.getQueueId(defaultQueueid, TeamUtils.QUEUE_TYPES.ACTIONED);
    	Id failedQueueId = TeamUtils.getQueueId(defaultQueueid, TeamUtils.QUEUE_TYPES.FAILED);
    	//Commented as a part of COPT-3585
        /*if(warningQueueId !=  null){
    		relatedQueuesList.add(queueName + TeamUtils.SEPARATOR + TeamUtils.QUEUE_TYPES.WARNING.name());
    	}*/
    	
    	if(actionedQueueId !=  null){
    		relatedQueuesList.add(queueName + TeamUtils.SEPARATOR + TeamUtils.QUEUE_TYPES.ACTIONED.name());
    	}
    	
    	if(failedQueueId !=  null){
    		relatedQueuesList.add(queueName + TeamUtils.SEPARATOR + TeamUtils.QUEUE_TYPES.FAILED.name());
    	}
    	return relatedQueuesList;
    }
    
    // parse the user's tier from their role
    @testVisible
    private static String getTierFromRole(String roleName) {
        String strReturn = '1'; // default to 1
        if (roleName != null) {
            string[] st = roleName.split(':');
            Integer n = st.size();
            String Tier = st[n-1];
            Integer TierLength = Tier.length();         
            if (TierLength > 1) {               
                strReturn = Tier.substring(1).trim();
            }
        }
        return strReturn;
    }

    // parse the user's tier from their role
    @testVisible
    private static String getBranchFromRole(String roleName) {
        String strReturn = null; // default to blank
        if (roleName != null) {
            string[] st = roleName.split(':');
            Integer n = st.size();
            
            if (n > 2 && st[1] == 'Branch') {
                strReturn = st[2];
            }
        }
        return strReturn;
    }

    /**
     * Pull all the auto assign permission set settings into static map
	 * @params:	none
	 * @rtnval:	void
     */
	public static void initialiseStaticMaps() {
		// Build map of Permission Set lists that should be auto-assigned based on user Role / Profile
		autoAssignPSMap = new Map<String, Auto_Assign_Permission_Set__c>();
		List<Auto_Assign_Permission_Set__c> autoAssignPSList = [SELECT Id, Name, Permission_Sets_To_Assign__c FROM Auto_Assign_Permission_Set__c];
		if (!autoAssignPSList.isEmpty()) {
			for (Auto_Assign_Permission_Set__c aaps : autoAssignPSList) {
				autoAssignPSMap.put(aaps.Name, aaps);
			}
		}
		
		// Build map of roles by Id
		userRoleMap = new Map<Id, UserRole> ([SELECT Id, Name FROM UserRole]);
		
		// Build map of profiles by Id
		profileMap = new Map<Id, Profile> ([SELECT Id, Name FROM Profile]);
		
		// by the multi-select picklist used in Auto_Assign_Permission_Set__c.Permission_Sets_To_Assign__c
		permissionSetNameXrefMap = new Map<String, PermissionSet>();
		List<PermissionSet> permissionSetList = [SELECT Id, Label FROM PermissionSet WHERE IsOwnedByProfile = false];
		if (!permissionSetList.isEmpty()) {
			for (PermissionSet ps : permissionSetList) {
				permissionSetNameXrefMap.put(ps.Label, ps);
			}
		}
		
		mapsInitialised = true;
	}

    /**
     * <<009>> Future Method to handle automated permission set assignments for users in response to
     * Auto Assign Permission Set updates, because you can't call it directly due to stupid mixed DML restriction.
	 * @params:	Set<String> roleNamesToSelect		List of user role names to be selected
	 *			Set<String> profileNamesToSelect	List of profile names to be selected
	 *			Map<String, String> dbxPermissionsToRemoveMap			Key = AAPS Name (ROLE: <role> or PROFILE: <profile>), Values = List of permission set names to remove
	 *																	which is a semi-colon delimited string, as you can't use Set<String> within the Map due to Future call.
	 *																	This is only populated when called from the AutoAssignPermissionSetHandler class
	 *																	when permissions have been removed from an Auto_Assign_Permission_Set__c record and Users need updating.
	 * @rtnval:	void
     */
	@future
	public static void aapsFutureAssignPermissionSets(Set<String> roleNamesToSelect, Set<String> profileNamesToSelect, Map<String, String> dbxPermissionsToRemoveMap) {
		try {
			aapsAssignPermissionSets(roleNamesToSelect, profileNamesToSelect, dbxPermissionsToRemoveMap);
		} catch (Exception ex) {
	        EmailNotifier.sendNotificationEmail('clsUserTriggerHandler Auto Assign Permission Sets Update Error', 'Error details: ' + ex.getMessage() + '\n\n' + dbxPermissionsToRemoveMap);
		}
	}

    /**
     * <<009>> Non Future Method to handle automated permission set assignments for users in response to Auto Assign Permission Set updates, 
     * because you can't call it directly due to stupid mixed DML restriction.  This is used directly from Unit Tests, as future method too slow when
     * initialising data.
	 * @params:	Set<String> roleNamesToSelect		List of user role names to be selected
	 *			Set<String> profileNamesToSelect	List of profile names to be selected
	 *			Map<String, String> dbxPermissionsToRemoveMap			Key = AAPS Name (ROLE: <role> or PROFILE: <profile>), Values = List of permission set names to remove
	 *																	which is a semi-colon delimited string, as you can't use Set<String> within the Map due to Future call.
	 *																	This is only populated when called from the AutoAssignPermissionSetHandler class
	 *																	when permissions have been removed from an Auto_Assign_Permission_Set__c record and Users need updating.
	 * @rtnval:	void
     */
	public static void aapsAssignPermissionSets(Set<String> roleNamesToSelect, Set<String> profileNamesToSelect, Map<String, String> dbxPermissionsToRemoveMap) {
		// Select users to update based on Roles / Profiles
		Map<Id, User> userUpdateMap = new Map<Id, User>([SELECT Id, IsActive, UserRoleId, ProfileId, Tier__c, Trusted_User__c, JL_com_Duty_Manager__c, Is_In_House_CC_Team_Manager__c
														 FROM User
														 WHERE IsActive = true AND (UserRole.Name IN :roleNamesToSelect OR Profile.Name IN :profileNamesToSelect)]);
		
		// Rewrite dbx map into proper map
		Map<String, Set<String>> aapsPermissionsToRemoveMap = new Map<String, Set<String>>();
		for (String mapKey : dbxPermissionsToRemoveMap.keySet()) {
			List<String> tempList = dbxPermissionsToRemoveMap.get(mapKey).split(';');
			Set<String> tempSet = new Set<String>(tempList);
			aapsPermissionsToRemoveMap.put(mapKey, tempSet);
		}
		assignPermissionSets(userUpdateMap, userUpdateMap, true, aapsPermissionsToRemoveMap);
	}

    /**
     * <<009>> Method to handle automated permission set assignments for users
	 * @params:	Map<Id, User> oldMap		Map of Users from Trigger.oldMap
	 *			Map<Id, User> newMap		Map of Users from Trigger.newMap
	 *			Boolean isUpdate			Trigger.update flag
	 *			Map<String, Set<String>> aapsPermissionsToRemoveMap			Key = AAPS Name (ROLE: <role> or PROFILE: <profile>), Values = List of permission set names to remove.
	 *																		This is only populated when called from the AutoAssignPermissionSetHandler class
	 *																		when permissions have been removed from an Auto_Assign_Permission_Set__c record and Users need updating.
	 * @rtnval:	void
     */
	private static void assignPermissionSets(Map<Id, User> oldMap, Map<Id, User> newMap, Boolean isUpdate, Map<String, Set<String>> aapsPermissionsToRemoveMap){
        System.debug('@@entering assignPermissionSets (refactored)');

        // In case called from AutoAssignPermissionSets trigger
        if (!mapsInitialised) {
        	initialiseStaticMaps();
        }
        
        // This is a nasty map of maps.
        // Map1 Key = User Id
        // Map2 Key = Permission Set Id
        // Map2 Value = PermissionSetAssignment record
        Map<Id, Map<Id, PermissionSetAssignment>> userPermissionAssignmentMap = new Map<Id, Map<Id, PermissionSetAssignment>>();
        
        // Build set of active users - we don't care about assignments on inactive users.
        Map<Id, User> activeUserMap = new Map<Id, User>();
        for (User u : newMap.values()) {
        	if (u.isActive) {
        		activeUserMap.put(u.Id, u);
        	}
        }
        
        // Build horror-map of existing assignments per User
        if (!activeUserMap.isEmpty()) {
	        List<PermissionSetAssignment> existingPSAList = [SELECT Id, AssigneeId, PermissionSetId
															 FROM PermissionSetAssignment
															 WHERE AssigneeId in :activeUserMap.keySet()];
        	
        	if (!existingPSAList.isEmpty()) {
        		for (PermissionSetAssignment psa : existingPSAList) {
        			Map<Id, PermissionSetAssignment> assignMap;
        			Boolean newUserId = false;
        			if (userPermissionAssignmentMap.containsKey(psa.AssigneeId)) {
        				assignMap = userPermissionAssignmentMap.get(psa.AssigneeId);
	        			if (!assignMap.containsKey(psa.PermissionSetId)) {
	        				assignMap.put(psa.PermissionSetId, psa);
    	    			}
        			} else {
        				assignMap = new Map<Id, PermissionSetAssignment>();
        				assignMap.put(psa.PermissionSetId, psa);
        				userPermissionAssignmentMap.put(psa.AssigneeId, assignMap);
        			}
        		}
        	}
        	
        	// Blank map so we can use the same methods for insert and update
        	Map<Id, PermissionSetAssignment> blankUserPermissionAssignmentMap = new Map<Id, PermissionSetAssignment>();
        	
        	// Prepare insert/delete lists
        	List<PermissionSetAssignment> deletePSAList = new List<PermissionSetAssignment>();
        	List<PermissionSetAssignment> insertPSAList = new List<PermissionSetAssignment>();
        	
        	for (User newUser : activeUserMap.values()) {
        		Set<Id> permissionSetsToKeep = new Set<Id>();
        		Set<Id> permissionSetsToAdd = new Set<Id>();
        		Set<Id> permissionSetsToRemove = new Set<Id>();
				String newRolePermissionKeyString = newUser.UserRoleId != null ? 'ROLE: ' + userRoleMap.get(newUser.UserRoleId).Name : 'ROLE: ' + NO_ROLE_SPECIFIED;
				String newProfilePermissionKeyString = 'PROFILE: ' + profileMap.get(newUser.ProfileId).Name;
        		
        		// Build the Add, Keep and Remove sets for this user
        		if (isUpdate) {
        			User oldUser = oldMap.get(newUser.Id);
					String oldRolePermissionKeyString = oldUser.UserRoleId != null ? 'ROLE: ' + userRoleMap.get(oldUser.UserRoleId).Name : 'ROLE: ' + NO_ROLE_SPECIFIED;
					String oldProfilePermissionKeyString = 'PROFILE: ' + profileMap.get(oldUser.ProfileId).Name;
					Map<Id, PermissionSetAssignment> existingUserPermissionAssignmentMap;
					if (userPermissionAssignmentMap.containsKey(newUser.Id)) {
						existingUserPermissionAssignmentMap = userPermissionAssignmentMap.get(newUser.Id);
					} else {
						existingUserPermissionAssignmentMap = new Map<Id, PermissionSetAssignment>();
					}
					
					// Build list of permissions to add or keep
					autoAssignPermissionSetsAddOrKeep(permissionSetsToKeep, permissionSetsToAdd, permissionSetsToRemove, existingUserPermissionAssignmentMap, newRolePermissionKeyString);
					autoAssignPermissionSetsAddOrKeep(permissionSetsToKeep, permissionSetsToAdd, permissionSetsToRemove, existingUserPermissionAssignmentMap, newProfilePermissionKeyString);

	        		// If role or profile has changed, remove any permissions from old role / profile (unless in keep list)
	        		if (oldUser.UserRoleId != newUser.UserRoleId) {
						autoAssignPermissionSetsRemove(permissionSetsToKeep, permissionSetsToAdd, permissionSetsToRemove, existingUserPermissionAssignmentMap, oldRolePermissionKeyString);
    	    		}
	        		if (oldUser.ProfileId != newUser.ProfileId) {
						autoAssignPermissionSetsRemove(permissionSetsToKeep, permissionSetsToAdd, permissionSetsToRemove, existingUserPermissionAssignmentMap, oldProfilePermissionKeyString);
    	    		}
    	    		
    	    		// Update static permissions from user record checkboxes
					processStaticPermissionSetAssignments(permissionSetsToKeep, permissionSetsToAdd, permissionSetsToRemove, existingUserPermissionAssignmentMap, newUser);
					
					// Remove any permissions when the Auto_Assign_Permission_Set__c record has been updated
					// This section is applicable only when called from the AutoAssignPermissionSetTrigger
					if (aapsPermissionsToRemoveMap.containsKey(newRolePermissionKeyString)) {
						Set<String> changedAAPSToRemove = aapsPermissionsToRemoveMap.get(newRolePermissionKeyString);
						for (String aapsToRemove : changedAAPSToRemove) {
							removePermissionSet(permissionSetsToKeep, permissionSetsToAdd, permissionSetsToRemove, existingUserPermissionAssignmentMap, aapsToRemove);
						}
					}
					if (aapsPermissionsToRemoveMap.containsKey(newProfilePermissionKeyString)) {
						Set<String> changedAAPSToRemove = aapsPermissionsToRemoveMap.get(newProfilePermissionKeyString);
						for (String aapsToRemove : changedAAPSToRemove) {
							removePermissionSet(permissionSetsToKeep, permissionSetsToAdd, permissionSetsToRemove, existingUserPermissionAssignmentMap, aapsToRemove);
						}
					}
        		} else {
					autoAssignPermissionSetsAddOrKeep(permissionSetsToKeep, permissionSetsToAdd, permissionSetsToRemove, blankUserPermissionAssignmentMap, newRolePermissionKeyString);
					autoAssignPermissionSetsAddOrKeep(permissionSetsToKeep, permissionSetsToAdd, permissionSetsToRemove, blankUserPermissionAssignmentMap, newProfilePermissionKeyString);
					processStaticPermissionSetAssignments(permissionSetsToKeep, permissionSetsToAdd, permissionSetsToRemove, blankUserPermissionAssignmentMap, newUser);
        		}
        		
        		// If there are permission sets to add, add to the insert list
        		if (!permissionSetsToAdd.isEmpty()) {
        			for (Id psId : permissionSetsToAdd) {
						PermissionSetAssignment psa = new PermissionSetAssignment(AssigneeId = newUser.Id, PermissionSetId = psId);
						insertPSAList.add(psa);
        			}
        		}

        		// If there are permission sets to remove, add to the delete list
        		if (!permissionSetsToRemove.isEmpty()) {
        			for (Id psId : permissionSetsToRemove) {
        				PermissionSetAssignment psa = userPermissionAssignmentMap.get(newUser.Id).get(psId);
        				deletePSAList.add(psa);
        			}
        		}
        	}
        	
        	// If we have Permission Sets to insert do it.
        	If (!insertPSAList.isEmpty()) {
        		insert insertPSAList;
        	}

        	// If we have Permission Sets to delete do it.
        	If (!deletePSAList.isEmpty()) {
        		delete deletePSAList;
        	}
        }
	}
	
    /**
     * <<009>> Method to build the list of automatic permission sets to be added/kept for Roles or Profiles
	 * @params:	Set<Id> permissionSetsToKeep		Set of existing Permission Set Ids to be kept 
	 *			Set<Id> permissionSetsToAdd			Set of new Permission Set Ids to be added
	 *			Set<Id> permissionSetsToRemove		Set of existing Permission Set Ids to be removed
	 *			Map<Id, PermissionSetAssignment> 	existingUserPermissionAssignmentMap		Map of Permission Set Assignments by Permission Set Id for that User
	 *			String newPermissionKeyString		The Label string for the Permission Set to be added/kept
	 * @rtnval:	void
     */
	private static void autoAssignPermissionSetsAddOrKeep(Set<Id> permissionSetsToKeep, Set<Id> permissionSetsToAdd, Set<Id> permissionSetsToRemove, 
														  Map<Id, PermissionSetAssignment> existingUserPermissionAssignmentMap, String newPermissionKeyString) {

		if (autoAssignPSMap.containsKey(newPermissionKeyString)) {
			Auto_Assign_Permission_Set__c aaps = autoAssignPSMap.get(newPermissionKeyString);
			List<String> psToAssignList = aaps.Permission_Sets_To_Assign__c.split(';');
			for (String psToAssignName : psToAssignList) {
				addOrKeepPermissionSet(permissionSetsToKeep, permissionSetsToAdd, permissionSetsToRemove, existingUserPermissionAssignmentMap, psToAssignName);
			}
		}
	}
	
    /**
     * <<009>> Method to build the list of automatic permission sets to be removed for Roles or Profiles
	 * @params:	Set<Id> permissionSetsToKeep		Set of existing Permission Set Ids to be kept 
	 *			Set<Id> permissionSetsToAdd			Set of new Permission Set Ids to be added
	 *			Set<Id> permissionSetsToRemove		Set of existing Permission Set Ids to be removed
	 *			Map<Id, PermissionSetAssignment> 	existingUserPermissionAssignmentMap		Map of Permission Set Assignments by Permission Set Id for that User
	 *			String oldPermissionKeyString		The Label string for the Permission Set to be added/kept
	 * @rtnval:	void
     */
	private static void autoAssignPermissionSetsRemove(Set<Id> permissionSetsToKeep, Set<Id> permissionSetsToAdd, Set<Id> permissionSetsToRemove,
													   Map<Id, PermissionSetAssignment> existingUserPermissionAssignmentMap, String oldPermissionKeyString) {
		
		if (autoAssignPSMap.containsKey(oldPermissionKeyString)) {
			Auto_Assign_Permission_Set__c aaps = autoAssignPSMap.get(oldPermissionKeyString);
			List<String> psToRemoveList = aaps.Permission_Sets_To_Assign__c.split(';');
			for (String psToRemoveName : psToRemoveList) {
				removePermissionSet(permissionSetsToKeep, permissionSetsToAdd, permissionSetsToRemove, existingUserPermissionAssignmentMap, psToRemoveName);
			}
		}
	}
	
    /**
     * <<009>> Method to set a single permission set to Add or Keep depending on whether already assigned.
	 * @params:	Set<Id> permissionSetsToKeep		Set of existing Permission Set Ids to be kept 
	 *			Set<Id> permissionSetsToAdd			Set of new Permission Set Ids to be added
	 *			Set<Id> permissionSetsToRemove		Set of existing Permission Set Ids to be removed
	 *			Map<Id, PermissionSetAssignment> 	existingUserPermissionAssignmentMap		Map of Permission Set Assignments by Permission Set Id for that User
	 *			String psToAssignName				The Label string for the Permission Set to be added/kept
	 * @rtnval:	void
     */
	private static void addOrKeepPermissionSet(Set<Id> permissionSetsToKeep, Set<Id> permissionSetsToAdd, Set<Id> permissionSetsToRemove, 
											   Map<Id, PermissionSetAssignment> existingUserPermissionAssignmentMap, String psToAssignName) {
		if (permissionSetNameXrefMap.containsKey(psToAssignName)) {
			PermissionSet ps = permissionSetNameXrefMap.get(psToAssignName);
			// If it is already assigned, keep it and make sure it's not removed.  Otherwise add it.
			if (existingUserPermissionAssignmentMap.containsKey(ps.Id)) {
				permissionSetsToKeep.add(ps.Id);
				permissionSetsToRemove.remove(ps.Id);
			} else {
				permissionSetsToAdd.add(ps.Id);
			}
		}
	}
	
    /**
     * <<009>> Method to set a single permission set to be Removed depending on whether already assigned.
	 * @params:	Set<Id> permissionSetsToKeep		Set of existing Permission Set Ids to be kept 
	 *			Set<Id> permissionSetsToAdd			Set of new Permission Set Ids to be added
	 *			Set<Id> permissionSetsToRemove		Set of existing Permission Set Ids to be removed
	 *			Map<Id, PermissionSetAssignment> 	existingUserPermissionAssignmentMap		Map of Permission Set Assignments by Permission Set Id for that User
	 *			String oldPermissionKeyString		The Label string for the Permission Set to be added/kept
	 * @rtnval:	void
     */
	private static void removePermissionSet(Set<Id> permissionSetsToKeep, Set<Id> permissionSetsToAdd, Set<Id> permissionSetsToRemove, 
											Map<Id, PermissionSetAssignment> existingUserPermissionAssignmentMap, String psToRemoveName) {
		if (permissionSetNameXrefMap.containsKey(psToRemoveName)) {
			PermissionSet ps = permissionSetNameXrefMap.get(psToRemoveName);
			// Add to remove list if user has already got that permission and it's not flagged to be kept
			if (existingUserPermissionAssignmentMap.containsKey(ps.Id) && !permissionSetsToKeep.contains(ps.Id)) {
				permissionSetsToRemove.add(ps.Id);
			}
		}
	}	
	
    /**
     * <<009>>  Method to build the list of static permission sets to be added/removed depended on User flags
     *			This replaces the original assignPermissionSets (List<User> newlist) method
	 * @params:	Set<Id> permissionSetsToKeep		Set of existing Permission Set Ids to be kept 
	 *			Set<Id> permissionSetsToAdd			Set of new Permission Set Ids to be added
	 *			Set<Id> permissionSetsToRemove		Set of existing Permission Set Ids to be removed
	 *			Map<Id, PermissionSetAssignment> 	existingUserPermissionAssignmentMap		Map of Permission Set Assignments by Permission Set Id for that User
	 *			User userRec						The user record for which static permissions are being set
	 * @rtnval:	void
     */
	private static void processStaticPermissionSetAssignments(Set<Id> permissionSetsToKeep, Set<Id> permissionSetsToAdd, Set<Id> permissionSetsToRemove,
															  Map<Id, PermissionSetAssignment> existingUserPermissionAssignmentMap, User userRec) {

		// Add or remove JL Tier 3 Permissions
		if (userRec.Tier__c != null && userRec.Tier__c > 2) {
			addOrKeepPermissionSet(permissionSetsToKeep, permissionSetsToAdd, permissionSetsToRemove, existingUserPermissionAssignmentMap, JL_TIER_3_PERMISSION_NAME);
		} else if (userRec.Tier__c != null && userRec.Tier__c <= 2) {
			removePermissionSet(permissionSetsToKeep, permissionSetsToAdd, permissionSetsToRemove, existingUserPermissionAssignmentMap, JL_TIER_3_PERMISSION_NAME);
		}

		// Add or remove Access Button Permission
		if (userRec.Tier__c != null && userRec.Tier__c > 2) {
			addOrKeepPermissionSet(permissionSetsToKeep, permissionSetsToAdd, permissionSetsToRemove, existingUserPermissionAssignmentMap, ACCESS_BUTTON_PERMISSION_NAME);
		} else if (userRec.Tier__c == null || userRec.Tier__c <= 2) {
			removePermissionSet(permissionSetsToKeep, permissionSetsToAdd, permissionSetsToRemove, existingUserPermissionAssignmentMap, ACCESS_BUTTON_PERMISSION_NAME);
		}

		// Add or remove SST Tab Permission Permission
		if (userRec.Trusted_User__c) {
			addOrKeepPermissionSet(permissionSetsToKeep, permissionSetsToAdd, permissionSetsToRemove, existingUserPermissionAssignmentMap, SST_TAB_PERMISSION_NAME);
		} else {
			removePermissionSet(permissionSetsToKeep, permissionSetsToAdd, permissionSetsToRemove, existingUserPermissionAssignmentMap, SST_TAB_PERMISSION_NAME);
		}
		
		// Add or remove Duty Manager Permission
		if (userRec.JL_com_Duty_Manager__c) {
			addOrKeepPermissionSet(permissionSetsToKeep, permissionSetsToAdd, permissionSetsToRemove, existingUserPermissionAssignmentMap, DUTY_MANAGER_PERMISSION_NAME);
		} else {
			removePermissionSet(permissionSetsToKeep, permissionSetsToAdd, permissionSetsToRemove, existingUserPermissionAssignmentMap, DUTY_MANAGER_PERMISSION_NAME);
		}

		// Add or remove In-House CC Team Manager Permission
		if (userRec.Is_In_House_CC_Team_Manager__c) {
			addOrKeepPermissionSet(permissionSetsToKeep, permissionSetsToAdd, permissionSetsToRemove, existingUserPermissionAssignmentMap, IN_HOUSE_CC_TEAM_MANAGER_PERMISSION_NAME);
		} else {
			removePermissionSet(permissionSetsToKeep, permissionSetsToAdd, permissionSetsToRemove, existingUserPermissionAssignmentMap, IN_HOUSE_CC_TEAM_MANAGER_PERMISSION_NAME);
		}
	}
	
	/**
	 * ES:26/04/2015 : CMP-208- CAPITA DROP 2 - Linking User.Primary & User.Additional Team to List Views via Public Groups
	 * @params:	List<User> users		The list of User records being inserted/updated from trigger.new.
	 * @rtnval:	void
    */
    private static void updatePublicGroupMembership(List <User> users) {

        Set <ID> userIDs = new Set <ID> ();
        Map <String, ID> ptGroupMap = TeamUtils.getPrimaryTeamNamesToGroupMap();
        List<GroupMember> membershipsToDelete = new List<GroupMember>();
        List<GroupMember> membershipsToUpsertOrDelete = new List<GroupMember>();
        List<GroupMember> membershipsToIgnore = new List<GroupMember>();
        List<GroupMember> membershipsToUpsert = new List<GroupMember>();
        //Includes All GroupMember records for all Users involved
        List <GroupMember> allUserGroupMemberships;

        //Get All User IDS Involved
        for (User u : users) {
            userIDs.add(u.id);
        }
        
		if (!ptGroupMap.isEmpty()) {
			Set <Id> ptGroupIDs = new Set<Id> (ptGroupMap.values());
            //Get all their Memberships to ANY Public Groups
			for (GroupMember gm : [SELECT Id, GroupId, UserOrGroupId FROM GroupMember WHERE UserOrGroupId IN :userIDs]) {

                //For Every Membership check...
                //if Membership is not related to a primary team group, ignore
                if (!ptGroupIDs.contains(gm.groupId)) {
                    membershipsToIgnore.add (gm);
                } else {
                //otherwise this membership may need to be deleted or upserted
                    membershipsToUpsertOrDelete.add(gm);
                }
            }

            for (User u : users) {
                ID uPrimaryTeamID = ptGroupMap.get(u.Team__c);
                
                //Primary Team Public Group Membership
                if (uPrimaryTeamID != null) {
                    GroupMember gmRecord = new GroupMember(GroupId = uPrimaryTeamID, UserOrGroupId = u.id); 
                    membershipsToUpsert.add (gmRecord);
                    //remove gmRecord from membershipsToUpsertOrDelete
                    membershipsToUpsertOrDelete = removeFromList(membershipsToUpsertOrDelete, gmRecord);
                }
				
                //Additional team Public Group Memberships
                if (u.My_Teams__c != null) {
                    for (String additionalTeamName : u.My_Teams__c.split(';')) {
                        ID additionalTeamID = ptGroupMap.get(additionalTeamName);
                        if (additionalTeamID != null) {
                            GroupMember gmRecord = new GroupMember(GroupId = additionalTeamID, UserOrGroupId = u.id); 
                            membershipsToUpsert.add (gmRecord);
                            //remove gmRecord from membershipsToUpsertOrDelete
                            membershipsToUpsertOrDelete = removeFromList(membershipsToUpsertOrDelete, gmRecord);
                        }
                    }
                }
            }

			if (!membershipsToUpsert.isEmpty()) {
                insert membershipsToUpsert;
            }

			if (!membershipsToUpsertOrDelete.isEmpty()) {
                delete membershipsToUpsertOrDelete;
            }
        }
    }

	/**
	 * ES:26/04/2015 : Helper Function
	 * @params:	List<GroupMember> theList		The list of GroupMembers
	 *			GroupMember theMember			The member to remove.
	 * @rtnval:	void
	 */
    private static List<GroupMember> removeFromList(List<GroupMember> theList, GroupMember theMember) {

        List<GroupMember> result = new List <GroupMember> ();

        if (theList != null && theMember != null) {
            for (GroupMember gm : theList) {
				if (!(gm.GroupId == theMember.GroupID && gm.UserOrGroupId == theMember.UserOrGroupId)) {
                    result.add(gm);
            }
        }
		}
        return result;
    }
}