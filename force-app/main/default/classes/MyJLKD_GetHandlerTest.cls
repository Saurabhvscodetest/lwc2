/**
 *	@author: Amit Kumar (Titanium Fire)
 *	@date: 2014-08-14  
 *	@description:
 *	    Test methods for MyJLKD_GetHandler
 *	
 *	Version History :   
 *		
 */
@isTest
private class MyJLKD_GetHandlerTest {

    static testMethod void requestHeaderPositiveTest() {
        // TO DO: implement unit test
        
        list<String> sampleMessageIds = new list<String>{'a', 'A', '1', '1234567890', '  abcdefghijklmnopqrstuvwxyz  ', '123456789 123456789 123456789 123456789 123456789'};
        list<String> sampleTransIds = new list<String>{'a', 'A', '1', '1234567890', '  abcdefghijklmnopqrstuvwxyz  ', '123456789 123456789 123456789 123456789 123456789'};
        
        //Test for all messageIds in List the same value is mirrored
        for(String sampleMessageId : sampleMessageIds) {
            SFDCKitchenDrawerTypes.RequestHeader fakerequestHeader = new SFDCKitchenDrawerTypes.RequestHeader();
            fakerequestHeader.MessageId = getUniqueMessageId(sampleMessageId);
            
            
            //Call code
            SFDCKitchenDrawerTypes.ActionResponse fakeResponse = MyJLKD_GetHandler.process(fakerequestHeader,sampleTransIds);
            
            //Assert we get the same value back with a Success status of true
            System.assertEquals(fakeResponse.success, true);
            System.assertEquals(fakerequestHeader.MessageId, fakeResponse.MessageId);
        } 
    }
    
    //Test missing request header
    static testMethod void requestHeaderNegativeTest1() {
        
        SFDCKitchenDrawerTypes.RequestHeader fakerequestHeader = new SFDCKitchenDrawerTypes.RequestHeader();
        list<String> sampleTransIds = new list<String>{'a', 'A', '1', '1234567890', '  abcdefghijklmnopqrstuvwxyz  ', '123456789 123456789 123456789 123456789 123456789'};
        
        //fakeRequest.Customer = testCustomerArray;
        
        //Call code
        SFDCKitchenDrawerTypes.ActionResponse fakeResponse = MyJLKD_GetHandler.process(fakerequestHeader,sampleTransIds);
        
        //Assert we get an error back when sending no messageId
        System.assertEquals(fakeResponse.success, false);
        System.assertEquals(fakeResponse.Code, MyJL_Const.ERRORCODE.MESSAGEID_NOT_PROVIDED.name());
    }
    
     //Test missing Transactions
    static testMethod void requestHeaderNegativeTest2() {
        
        list<String> sampleMessageIds = new list<String>{'a', 'A', '1', '1234567890', '  abcdefghijklmnopqrstuvwxyz  ', '123456789 123456789 123456789 123456789 123456789'};
        
        list<String> sampleTransIds = new list<String>();
        //fakeRequest.Customer = testCustomerArray;
        
		for(String sampleMessageId : sampleMessageIds) {
            SFDCKitchenDrawerTypes.RequestHeader fakerequestHeader = new SFDCKitchenDrawerTypes.RequestHeader();
            fakerequestHeader.MessageId = getUniqueMessageId(sampleMessageId);
            
            
            //Call code
            SFDCKitchenDrawerTypes.ActionResponse fakeResponse = MyJLKD_GetHandler.process(fakerequestHeader,sampleTransIds);
            
            //Assert we get the same value back with a Success status of true
            System.assertEquals(fakeResponse.success, false);
        System.assertEquals(fakeResponse.Code, MyJL_Const.ERRORCODE.NO_TRANSACTION_DATA.name());
        } 
        //Assert we get an error back when sending no messageId
        
    }
    
    //Test number of Transactions going in is identical to number of statuses coming back
    static testMethod void requestTransactionCountTest() {
        list<String> sampleTransIds = new list<String>{'a', 'A', '1', '1234567890', '  abcdefghijklmnopqrstuvwxyz  ', '123456789 123456789 123456789 123456789 123456789'};
        
        //Generate new messageId (hopefully unique) by taking current time in ms
        SFDCKitchenDrawerTypes.RequestHeader requestHeader = new SFDCKitchenDrawerTypes.RequestHeader();
        requestHeader.MessageId = '' + (Datetime.now()).getTime();
            
        SFDCKitchenDrawerTypes.ActionResponse fakeResponse = MyJLKD_GetHandler.process(requestHeader,sampleTransIds);
            
            
        //Assert the call was succesfull and we get the same number of records back
        System.assertEquals(fakeResponse.success, true);
        System.assertEquals(fakeResponse.MessageId, requestHeader.MessageId);
        System.assertEquals(fakeResponse.results.size(), sampleTransIds.size());
    }
    
    static testMethod void requestTransactionHeader() {
    	list<String> TransIds = new list<String>();
    	List<Loyalty_Card__c> cards = generateMYJL_LoyaltyCards(3);
		final List<Transaction_Header__c> trans = new List<Transaction_Header__c>{
			new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR0_0', Card_Number__c = cards[0].Id, Transaction_Date_Time__c = System.now()),
			new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR1_0', Card_Number__c = cards[1].Id, Transaction_Date_Time__c = System.now())
			
		};
		Database.insert(trans);
		
		TransIds.add(trans[0].Receipt_Barcode_Number__c);
		TransIds.add(trans[1].Receipt_Barcode_Number__c);
		
		//generate XML attachments
		final List<Attachment> atts = new List<Attachment>();
		atts.addAll(initAttachments(trans[0], 'short xml 0_0', 'full xml 0_0'));
		atts.addAll(initAttachments(trans[1], 'short xml 1_0', 'full xml 1_0'));

		Database.insert(atts);
        
		SFDCKitchenDrawerTypes.RequestHeader requestHeader = new SFDCKitchenDrawerTypes.RequestHeader();
		requestHeader.messageId = 'message-1';
            
            
        //Call code
        SFDCKitchenDrawerTypes.ActionResponse Response = MyJLKD_GetHandler.process(requestHeader,TransIds);
            
            
        //Assert the call was succesfull and we get the same number of records back
        System.assertEquals(Response.success, true, Response);
        System.assertEquals(Response.MessageId, requestHeader.MessageId);
        System.assertEquals(Response.results.size(), trans.size());
        System.assertEquals('TR0_0', response.results[0].kdTransaction.transactionId, 'Expected first transaction to be the one which goes first in the ordered list. ' + response.results[0].kdTransaction);
	
        System.assertEquals('full xml 0_0', response.results[0].kdTransaction.fullXml, 'Expected full XML to be returned for transaction 0');
		System.assert(String.isBlank(response.results[0].kdTransaction.shortXml), 'Expected NO Short XML to be returned for transaction 0');
	}
	
	
	 static testMethod void getDetailTransaction_ForPartnership_Cards() {
		list<String> transIds = new list<String>();
		final String SHOPPER_ID = '212112121212121212121';
		final String PARTNERSHIP_CARD_NO = '1234567890123456';
		final String RECEIPT_BARCODE_NO_1 = 'TR0';
		final String RECEIPT_BARCODE_NO_2 = 'TR1';
		final String FULL_XML_1 = 'full xml 0_0';
		final String FULL_XML_2 = 'full xml 0_1';
		Contact con = new Contact(Shopper_ID__c = SHOPPER_ID, LastName = 'SSSS');
		Database.insert(con);
        
        Loyalty_Account__c loyaltyAcc = new Loyalty_Account__c(Name = 'LA_', Email_Address__c = 'la_' + '@test.com', Contact__c = con.Id, Customer_Loyalty_Account_Card_ID__c = null);
        Database.insert(loyaltyAcc);
        
        
        Loyalty_Card__c myJLCard = new Loyalty_Card__c(Name = 'Card_' , Loyalty_Account__c = loyaltyAcc.Id);
        Loyalty_Card__c partnershipCard = new Loyalty_Card__c(Name = PARTNERSHIP_CARD_NO, Card_Number__c = '1234567890123456', Loyalty_Account__c = loyaltyAcc.Id
        									, Card_Type__c = MyJL_Const.MY_PARTNERSHIP_CARD_TYPE, Date_Created__c = DateTime.Now(),Origin__c = 'TEST' );
        									
        Database.insert(new List<Loyalty_Card__c>{myJLCard, partnershipCard});
        
        
        loyaltyAcc.Customer_Loyalty_Account_Card_ID__c = PARTNERSHIP_CARD_NO; 
        update loyaltyAcc;
                
        
        final List<Transaction_Header__c> trans = new List<Transaction_Header__c>{
            new Transaction_Header__c(Receipt_Barcode_Number__c = RECEIPT_BARCODE_NO_1, Card_Number__c = myJLCard.Id, Transaction_Date_Time__c = System.now(), IsValid__c = true),
            new Transaction_Header__c(Receipt_Barcode_Number__c = RECEIPT_BARCODE_NO_2, Card_Number__c = partnershipCard.Id, Transaction_Date_Time__c = System.now(), IsValid__c = true)
        };
        
        Database.insert(trans);
        
		transIds.add(trans[0].Receipt_Barcode_Number__c);
		transIds.add(trans[1].Receipt_Barcode_Number__c);
		
		//generate XML attachments
		final List<Attachment> atts = new List<Attachment>();
		atts.addAll(initAttachments(trans[0], 'short xml 0_0', FULL_XML_1));
		atts.addAll(initAttachments(trans[1], 'short xml 1_0', FULL_XML_2));

		Database.insert(atts);
		
		
        SFDCKitchenDrawerTypes.RequestHeader requestHeader = new SFDCKitchenDrawerTypes.RequestHeader();
        requestHeader.messageId = 'message-1';

   		SFDCKitchenDrawerTypes.ActionResponse Response = MyJLKD_GetHandler.process(requestHeader,transIds);
            
            
        //Assert the call was succesfull and we get the same number of records back
        System.assertEquals(Response.success, true, Response);
        System.assertEquals(Response.MessageId, requestHeader.MessageId);
        System.assertEquals(Response.results.size(), trans.size());
        System.assertEquals(RECEIPT_BARCODE_NO_1, response.results[0].kdTransaction.transactionId, 'Expected first transaction to be the one which goes first in the ordered list. ' + response.results[0].kdTransaction);
	
        System.assertEquals(FULL_XML_1, response.results[0].kdTransaction.fullXml, 'Expected full XML to be returned for transaction 0');
		System.assert(String.isBlank(response.results[0].kdTransaction.shortXml), 'Expected NO Short XML to be returned for transaction 0');
	}
    
    
    static testmethod void requestTransactionIDNOTFOUND(){
    	list<String> TransIds = new list<String>();
    	
        TransIds.add('TR1_Fake_0');
        
        SFDCKitchenDrawerTypes.RequestHeader requestHeader = new SFDCKitchenDrawerTypes.RequestHeader();
		requestHeader.messageId = 'message-2';
        
        SFDCKitchenDrawerTypes.ActionResponse Response = MyJLKD_GetHandler.process(requestHeader,TransIds);
        
        System.assertEquals(Response.success, true);
        System.assertEquals(Response.MessageId, requestHeader.MessageId);
        System.assertEquals(Response.results.size(), TransIds.size());
        System.assertEquals(Response.results[0].code,MyJL_Const.ERRORCODE.TRANSACTIONID_NOT_FOUND.name());
	
    }
    
    static testMethod void testVariousErrors () {
		//missing message Id
		list<String> TransIds = new list<String>();
    	
    	
        TransIds.add('TR1_Fake_0');
		SFDCKitchenDrawerTypes.RequestHeader requestHeader = new SFDCKitchenDrawerTypes.RequestHeader();
		
		SFDCKitchenDrawerTypes.ActionResponse response = MyJLKD_GetHandler.process(requestHeader,TransIds);
		System.assertEquals(false, response.success, 'Must have failed due to missing message Id');
		System.assertEquals(MyJL_Const.ERRORCODE.MESSAGEID_NOT_PROVIDED.name(), response.code, 'Must have failed due to missing message Id');

		
		//non unique message Id
		CustomSettings.setTestLogHeaderConfig(true, -1);
		CustomSettings.setTestLogDetailConfig(true, -1);

		Database.insert(new Log_Header__c(Message_ID__c = 'TEST-Duplicate_1', Service__c = MyJL_Util.SOURCE_SYSTEM));

		requestHeader = new SFDCKitchenDrawerTypes.RequestHeader();
		requestHeader.messageId = 'TEST-Duplicate_1';
		
		response = MyJLKD_GetHandler.process(requestHeader, TransIds);
		System.assertEquals(false, response.success, 'Must have failed due to duplicate MessageId');
		System.assertEquals(MyJL_Const.ERRORCODE.MESSAGEID_DUPLICATE.name(), response.code, 'Must have failed due to duplicate MessageId');
		
		
	
		
	}
    static {
		BaseTriggerHandler.addSkipReason(CalloutHandler.SKIP_ALL_NOTIFICATIONS_CODE, 'disable outbound events in this test class');
	}
	

	/************************ DATA PREPARATION *******************************************/
	/**
	 * this method will create numberOfCards Loyalty_Account__c and Loyalty_Card__c records,
	 * i.e. one Loyalty_Card__c per Loyalty_Account__c
	 */
	private static List<Loyalty_Card__c> generateMYJL_LoyaltyCards(final Integer numberOfCards) {
		
		
		final List<Loyalty_Account__c> loyaltyAccounts = new List<Loyalty_Account__c>();
		
		for(Integer i = 0; i < numberOfCards; i++) {
			loyaltyAccounts.add(new Loyalty_Account__c(Name = 'LA_' + i, Email_Address__c = 'la_' + i + '@test.com'));
		}
		Database.insert(loyaltyAccounts);
		
		final List<Loyalty_Card__c> cards = new List<Loyalty_Card__c>();
		
		for(Integer i = 0; i < numberOfCards; i++) {
			cards.add(new Loyalty_Card__c(Name = 'Card_' + i, Loyalty_Account__c = loyaltyAccounts[i].Id));
		}
		Database.insert(cards);
		return cards;
	}

	/**
	 * create Attachment documents for given Transaction_Header__c
	 * shortXmlText - if not empty then Short XML Attachment will be generated
	 * fullXmlText - if not empty then Full XML Attachment will be generated
	 */
	private static List<Attachment> initAttachments(final Transaction_Header__c tran, final String shortXmlText, final String fullXmlText) {
		final List<Attachment> atts = new List<Attachment>();
		
		if (!String.IsBlank(shortXmlText)) {
			Attachment att = new Attachment(ParentId = tran.Id, Name = MyJLKD_GetHandler.getShortXmlName());
			att.Body = Blob.valueOf(shortXmlText);
			att.ContentType = 'text/xml';
			atts.add(att);
		}

		
		if (!String.IsBlank(fullXmlText)) {
			Attachment att = new Attachment(ParentId = tran.Id, Name = MyJLKD_GetHandler.getFullXmlName());
			att.Body = Blob.valueOf(fullXmlText);
			att.ContentType = 'text/xml';
			atts.add(att);
		}
		return atts;
	}
	/************************ END DATA PREPARATION *******************************************/
    
    private static String getUniqueMessageId(String input) {
    	String output = (input != null ? input : '') + '_' + (Datetime.now()).getTime();
    	
    	return output;
    }
}