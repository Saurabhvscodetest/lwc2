global class coOrderHeader implements Comparable {
    
	@AuraEnabled public String OrderNumber {get; set;}
	@AuraEnabled public String Salutation {get; set;}
	@AuraEnabled public String FirstName {get; set;}
	@AuraEnabled public String LastName {get; set;}
	@AuraEnabled public String Email {get; set;}
	@AuraEnabled public String Postcode {get; set;}
	@AuraEnabled public String OrderStatus {get; set;}
	@AuraEnabled public Date DatePlaced {get; set;}
	@AuraEnabled public Decimal TotalOrderValue {get; set;}

	// default constructor
	public coOrderHeader() {}

	// override constructor
	public coOrderHeader(String orderNumber, String salutation, String firstName, String lastName, String email, String postcode, String orderStatus, Date datePlaced, Decimal totalOrderValue) {

		this.OrderNumber = orderNumber;
		this.Salutation = salutation;
		this.FirstName = firstName;
		this.LastName = lastName;
		this.Email = email;
		this.Postcode = postcode;
		this.OrderStatus = orderStatus;
		this.DatePlaced = datePlaced;
		this.TotalOrderValue = totalOrderValue;
	}

	global Integer compareTo(Object compareTo) {
		coOrderHeader compareToHeader = (coOrderHeader)compareTo;

		Date localCompareFields = this.DatePlaced;
		Date passedCompareFields = compareToHeader.DatePlaced;

		Integer returnValue = 0;
        if (localCompareFields > passedCompareFields) {
            // Set return value to a positive value.
            returnValue = -1;
        } else if (localCompareFields < passedCompareFields) {
            // Set return value to a negative value.
            returnValue = 1;
        }
        
        return returnValue;      
	}
}