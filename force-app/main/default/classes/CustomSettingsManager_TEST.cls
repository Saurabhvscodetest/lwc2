@IsTest
public with sharing class CustomSettingsManager_TEST 
{


	@testSetup static void setup() {
         UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
    }
	// This code must access custom settings values
	// It also asserts that expected values exist for deployment -
	// Therefore it must see all data!
	@IsTest
    public static void testCustomSettingsManager()
    {
        CustomSettingsManager.getAllConfigSettings();
        CustomSettingsManager.getConfigSettingBooleanVal(CustomSettingsManager.NEW_CASE_FROM_EMAIL_CLOSED_DAYS);
        CustomSettingsManager.getConfigSetting(CustomSettingsManager.NEW_CASE_FROM_EMAIL_CLOSED_DAYS);
        CustomSettingsManager.getConfigSetting(CustomSettingsManager.NEW_CASE_FROM_EMAIL_CLOSED_DAYS);
        CustomSettingsManager.getConfigSettingNumberVal(CustomSettingsManager.NEW_CASE_FROM_EMAIL_CLOSED_DAYS);
      	CustomSettingsManager.getConfigSettingIntegerVal(CustomSettingsManager.NEW_CASE_FROM_EMAIL_CLOSED_DAYS);
      	CustomSettingsManager.getConfigSettingStringVal(CustomSettingsManager.NEW_CASE_FROM_EMAIL_CLOSED_DAYS);
              
        List<String> requiredSettings = new List<String>();

        requiredSettings.add(CustomSettingsManager.NEW_CASE_FROM_EMAIL_CLOSED_DAYS);
        
        for (String setting: requiredSettings)
        {
        	try{ 
        		
        		System.assert(null != CustomSettingsManager.getConfigSetting(setting), 'Cannot Find Custom Setting '+setting);
        	} catch (Exception e)
        	{
        		System.assert(false,'Cannot Find Custom Setting '+setting);
        	}
        }
    } 
}