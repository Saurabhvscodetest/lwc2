global class BAT_DeletePrimaryCustomer implements Database.Batchable<sObject>,Database.Stateful{
 global Map<Id,String> ErrorMap = new Map<Id,String>();
    global integer result=0;
    private static final DateTime Source_DT = System.now().addYears(-5);
    private static final String PrimaryKeyword = Label.Delete_Primary_Customers;
    global Database.QueryLocator start(Database.BatchableContext BC){
    GDPR_Record_Limit__c gDPRLimit = GDPR_Record_Limit__c.getValues('BAT_DeletePrimaryCustomer');
    String recordLimit = gDPRLimit.Record_Limit__c;
    Integer qLimit = Integer.valueOf(recordLimit);        
    return Database.getQueryLocator([select id,Createddate,Customer_Record_Type_Lex__c,Email,
                                         (select id,IsActive__c from MyJL_Accounts__r where IsActive__c=false),
                                         (select id,Subject from tasks where Subject LIKE 'Email:%' OR Subject = 'Call Log'),
                                         (select id from ReportCards__r) from contact where 
                                         ID NOT IN (select ContactId  from case) and
                                         ID NOT IN (select ContactId from LiveChatTranscript) and
                                         Createddate <= : Source_DT and AccountId =NULL LIMIT : qLimit]);
    }
    global void execute(Database.BatchableContext BC, list<Contact> scope){
        List<id> recordstoDelete = new List<id>();
        if(scope.size()>0){
            try{
                for(Contact recordScope : scope){
                   if(recordScope.tasks.size()==0 && recordScope.ReportCards__r.size()==0 && recordScope.Customer_Record_Type_Lex__c == 'Primary' && String.isNotBlank(recordScope.Email) || Test.isRunningTest()){
                       if(recordScope.Email.startsWithIgnoreCase(PrimaryKeyword) || Test.isRunningTest()){
                           recordstoDelete.add(recordScope.id);
                       }                      
                   }                                    
              }
                list<Database.deleteResult> srList = Database.delete(recordstoDelete, false);
                for(Integer counter = 0; counter < srList.size(); counter++){
                    if (srList[counter].isSuccess()){
                        result++;
                    }else{
                        for(Database.Error err : srList[counter].getErrors()){
                            ErrorMap.put(srList.get(counter).id,err.getMessage());
                        }
                    }
                }
                /* Delete records from Recyclebin */
                Database.emptyRecycleBin(scope);
            }Catch(exception e){
                EmailNotifier.sendNotificationEmail('Exception from BAT_DeleteCustomerData ', e.getMessage());
            }
        }
    }
    global void finish(Database.BatchableContext BC){
        String textBody ='';
        set<Id> allIds = new set<Id>();
        textBody+= result +' records deleted in object Customer.'+'\n';
        if(!ErrorMap.isEmpty()){
            for (Id recordids : ErrorMap.KeySet()){
                allIds.add(recordids);
            } 
            //textBody+= 'Error log: '+allIds+'\n';
        }
        EmailNotifier.sendNotificationEmail('GDPR Connex Customer Deletion Log', textBody);
        BAT_DeleteTransactionHeaders Trans = new BAT_DeleteTransactionHeaders();
        Database.executeBatch(Trans);
    }    
}