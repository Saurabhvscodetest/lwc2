@isTest
public class SF_ConnexToTeleoptiTest {
    
    private static final String fromTimeStamp = '1561981035000';
    private static final String toTimeStamp = '1562749404000';
	
    static testMethod void serviceException() {
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/SF_ConnexToTeleoptiService/';
        req.addParameter('fromTimeStamp', fromTimeStamp);
        req.addParameter('toTimeStamp', toTimeStamp);
        req.httpMethod = 'GET';
        
        RestContext.request = req;
        RestContext.response = NULL;
                
        Test.startTest();
        try {
            SF_ConnexToTeleopti.SF_ConnexToTeleoptiWrapper crw = SF_ConnexToTeleopti.fetchData();
        }
        catch(Exception e) {}
        Test.stopTest();        
    }
    
    static testMethod void fetchDataMethod() {
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/SF_ConnexToTeleoptiService/';
        req.addParameter('fromTimeStamp', fromTimeStamp);
        req.addParameter('toTimeStamp', toTimeStamp);
        req.httpMethod = 'GET';
        
        RestContext.request = req;
        RestContext.response = res;
                
        Test.startTest();
            SF_ConnexToTeleopti.SF_ConnexToTeleoptiWrapper crw = SF_ConnexToTeleopti.fetchData();
        Test.stopTest();        
    }
}