@isTest
private class MyLoyaltyServiceHelper_Test {

	private static final String SHOPPER_ID = '1234abcdefg';
    private static final String PARTNERSHIP_CARD_NUMBER = '1234567890123456';

	@TestSetup
	static void initData(){

		Loyalty_Account__c myAccount = new Loyalty_Account__c(ShopperId__c = SHOPPER_ID, IsActive__c = true);

		insert myAccount;
	}

	@isTest static void testGetLoyaltyAccountsWithShopperId() {

		List<Loyalty_Account__c> accounts = MyLoyaltyServiceHelper.getLoyaltyAccountsWithShopperId(SHOPPER_ID);
		System.assertEquals(1, accounts.size(), 'Expecting to have one loyalty account, as one was inserted above');
	}
    
    @isTest static void testgetLoyaltyCardWithLoyaltyAccount() {
       final List<Loyalty_Account__c> loyaltyAccounts = new List<Loyalty_Account__c>();
		loyaltyAccounts.add(new Loyalty_Account__c(Name = PARTNERSHIP_CARD_NUMBER ,ShopperId__c=SHOPPER_ID,IsActive__c = true,Membership_Number__c = PARTNERSHIP_CARD_NUMBER));
		insert loyaltyAccounts;
        
		final List<Loyalty_Card__c> cardList = new List<Loyalty_Card__c>();
		cardList.add(new Loyalty_Card__c(Name = PARTNERSHIP_CARD_NUMBER,Disabled__c = false , Loyalty_Account__c = loyaltyAccounts[0].Id));
		Database.insert(cardList);
        
        test.startTest();
		List<Loyalty_Card__c> cards = MyLoyaltyServiceHelper.getLoyaltyCardWithLoyaltyAccount(loyaltyAccounts[0].Id,PARTNERSHIP_CARD_NUMBER);
		System.assertEquals(1, cards.size());
        test.stopTest();
	}
}