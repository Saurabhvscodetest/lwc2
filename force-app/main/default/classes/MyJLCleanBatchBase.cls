/******************************************************************************
* @author       Matt Povey
* @date         29/12/2015
* @description  Extendable class used in MyJL Cleanup Batch Processes.
*
* EDIT RECORD
*
* LOG     DATE        Author    JIRA        COMMENT      
* 001     29/12/2015  MP        MJL-2154    Original code
* 002      28/01/2016  NA       MJL-2154    Adding the start date logic
*
*******************************************************************************
*/
global virtual class MyJLCleanBatchBase {
    protected MyJL_Cleanup_Process_Settings__c configSettings;
    protected Boolean recordUpdatesEnabled;
    protected List<String> emailReportUsers;
    protected Boolean emailReportEnabled;
    protected datetime reportStartDate;  //<<002>>
    protected datetime reportEndDate;  //<<002>>
    protected Boolean processEnabled;
    @testVisible protected Integer totalRecordCount = 0;
    protected String reportBody;
    protected String reportBodyHTML;
    protected final String headerStyle = 'nowrap="" style="border:1pt solid windowtext;padding:0cm 5.4pt;height:15pt;background:rgb(215,228,188)"';
    protected final String rowStyle = 'nowrap="" valign="bottom" style="width:14.48%;border-style:none solid solid;border-right-color:windowtext;border-bottom-color:windowtext;border-left-color:windowtext;border-right-width:1pt;border-bottom-width:1pt;border-left-width:1pt;padding:0cm 5.4pt;height:15pt"';
    protected final String tableStyle = 'style="border-spacing:0px"';

    /**
     * Common base constructor method
     * @params: String className    The name of the subclass passed in from their constructor.
     */
    global MyJLCleanBatchBase(String className) {
        configSettings = MyJL_Cleanup_Process_Settings__c.getInstance(className);
        recordUpdatesEnabled = configSettings != null ? configSettings.Record_Updates_Enabled__c : false; 
        reportStartDate = configSettings != null ? configSettings.Start_Date_Time__c : Datetime.now();   //<<002>>//<<002>>
        reportEndDate = configSettings != null ? configSettings.End_Date_Time__c : Datetime.now();   //<<002>>//<<002>>
        emailReportUsers = configSettings != null ? configSettings.Email_Report_Recipient_List__c.split(';') : new List<String>();
        emailReportEnabled = configSettings != null && !emailReportUsers.isEmpty() ? configSettings.Email_Report_Enabled__c : false;
        processEnabled = configSettings != null && (emailReportEnabled || recordUpdatesEnabled) ? configSettings.Process_Enabled__c : false;
        if (!processEnabled) {
            system.debug(className + ' process has been initialised but is not enabled');
        }
    }
    
    /**
     * Common report sender method
     * @params: String className    The name of the subclass passed in from their constructor.
     *          String objectName   Textual name of the object being reported on (purely used for output).
     *          String updateAction Textual action for email text output (e.g. 'updated' or 'deleted').
     */
    global void sendReportEmail(String className, String objectName, String updateAction) {
        if (processEnabled) {
            if (totalRecordCount > 0) {
                String emailBody = 'INVALID MYJL ' + objectName + ' RECORDS ';
                if (recordUpdatesEnabled) {
                    emailBody += updateAction + ':';
                } else {
                    emailBody += 'FOUND:';
                }
                emailBody += ' ' + totalRecordCount;
                if (emailReportEnabled) {
                    // emailBody += '\n\n' + reportBody;
                    emailBody += '<br><br>' + reportBodyHTML;
                    EmailNotifier.sendNotificationEmail(emailReportUsers, className + ' COMPLETED', emailBody, true);
                } else {
                    system.debug(emailBody);
                }
            } else {
                system.debug(className + ' process has run but found no matching records to process');
            }
        } else {
            system.debug(className + ' process has run but is not enabled - no updates made or reports generated');
        }
    }
    
    /**
     * Common method used to add blank cells to the HTML report body
     * @params: Integer noOfBlankCells  Add the specified number of blank <td> cells to the report.
     */
    global void addBlankReportCells(Integer noOfBlankCells) {
        for (Integer i = 1; i <= noOfBlankCells; i++) {
            addBlankReportCell();
        }
    }

    /**
     * Common method used to add a blank cell to the HTML report body
     * @params: None
     */
    global void addBlankReportCell() {
        reportBodyHTML += '<td ' + rowStyle + '></td>';
    }

    /**
     * Common error email sender method
     * @params: String errorSubject     The email subject text passed from the subclass.
     *          String errorMessage     The error message body text passed from the subclass.
     */
    global void sendErrorEmail(String errorSubject, String errorMessage) {
        system.debug(errorMessage);
        if (emailReportEnabled) {
            EmailNotifier.sendNotificationEmail(emailReportUsers, errorSubject, errorMessage);
        } else {
            EmailNotifier.sendNotificationEmail(errorSubject, errorMessage);
        }
    }
}