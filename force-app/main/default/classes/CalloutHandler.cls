/**
 *  @author: Andrey Gavrikov (westbrook)
 *  @date: 2014-08-29
 *  @description:
 *      support for "Retry" functionality for callouts
 *      similar to standard outbound message retry function
 *  
 *  In order to take adavantage of "Retry" function classes that make a callout 
 *  need to implement RetriableCallout interface and use CalloutHandler to initiate actual callout
 *
 * Usage:
 * 1. Implement RetriableCallout interface
 * 2. Invoke CalloutHandler.callout(serviceName, objects) or callout(objectsByServiceName)
 *
 * @see MyJL_ContactProfileHandler for implementation example
 *
 *  Version History :
 *  2014-08-29 - MJL-754 - AG
 *  initial version
 *  
 *  2014-11-20 - MJL-1306 - AG
 *  Retry part of "retriable callout" is now initiated by batch process CalloutRetryBatch
 *  
 001    17/03/15    NTJ                 MJL-1674, clear the errors as you no longer have any (remember this is ProcessSuccess)
                                        Also do the update after all the messages are sent to avoid the 'You have uncommitted work pending. Please commit or rollback before calling out'
                                        error that apex throws if you do a DML (i.e. Update of Log_Header__c) before the next web service call
 002    15/04/15    NTJ                 MJL-1790 - Do not do a SELECT WHERE x IN set, if we have an empty set
 003    27/04/15    NTJ                 MJL-1823 - Do not go @future if we are already @future  
 004    17/06/15    NTJ                 MJL-2090 - Make any IO problem a soft retriable error
 005    07/08/15    NA					Extended the logic in callout to prevent any null email address and null activations in outbound notifications	
 */
public with sharing class CalloutHandler extends BaseTriggerHandler {
    public static String CALLOUT_STATUS_NEW = 'New';
    public static String CALLOUT_STATUS_SOFT_FAIL = 'Retry';
    public static String CALLOUT_STATUS_HARD_FAIL = 'Hard Fail';
    
    private static Set<String> RETRIABLE_STATUSES = new Set<String>{CALLOUT_STATUS_NEW, CALLOUT_STATUS_SOFT_FAIL};

    public static String SKIP_ALL_NOTIFICATIONS_CODE = 'SkipAllNotifications';//this is mainly for test purposes

    override public void beforeUpdate () {
        final Set<Id> calloutHeaderIds = new Set<Id>();//id of headers where callout is required
        for(Log_Header__c header : (List<Log_Header__c>)Trigger.new) {
            if (header.Do_Callout__c && RETRIABLE_STATUSES.contains(header.Callout_Status__c)) {
                calloutHeaderIds.add(header.Id);
                header.Do_Callout__c = false;
                header.Activate_Callout_Retry_Workflow__c = false;
            }
        }
        if (!calloutHeaderIds.isEmpty()) {
            // MJL-1823 - Don't go @ future if we are already in the future (you cannot call @future from @future)
            callout(calloutHeaderIds, !System.isFuture());
        }
    }

    /**
     * call single service
     */
    public static void callout(final String serviceName, final List<Object> records) {
        if (!BaseTriggerHandler.hasSkipReason(SKIP_ALL_NOTIFICATIONS_CODE)) {
            callout(new Map<String, List<Object>> {serviceName => records});
        }
    }

    /**
     * call multiple services
     */
    public static void callout(final Map<String, List<Object>> objectsByServiceName) {
       
        if (BaseTriggerHandler.hasSkipReason(SKIP_ALL_NOTIFICATIONS_CODE)) {
            return;
        }
        //log outbound event
        final Set<Id> headerIds = startCalloutHeader(objectsByServiceName, CalloutHandler.CALLOUT_STATUS_NEW);

        //now real call-out
        // MJL-1823 - cannot call @future from @future
        callout(headerIds, !(System.isFuture() ));
    }
    
    /**
     * using list of header Ids split them by services and send a separate callout for each service
     */
    public static void callout(final Set<Id> calloutLogHeaderIds, Boolean useFuture) {
        
        final Map<String, Set<Id>> headerIdsByService = new Map<String, Set<Id>>();
        List<Log_Header__c> headers = new List<Log_Header__c>();

        // MJL-1790
        if (!calloutLogHeaderIds.isEmpty()) {
            //system.debug('calloutLogHeaderIds: '+calloutLogHeaderIds);
            for(Log_Header__c header : [select Id, Callout_Attempts__c, Service__c, Callout_Status__c
                                                from Log_Header__c 
                                                where Id in :calloutLogHeaderIds
                                                order by Service__c]) {
                if (!MyJL_Util.isBlank(header.Service__c)) {
                    addToMap(headerIdsByService, header.Service__c, header.Id);
                }
            }           
        }
        //system.debug('header count: '+headerIdsByService.size());
        //
        for(String service : headerIdsByService.keySet()) {
            //system.debug('service: '+service);
            if (useFuture) {
                sendCalloutAsFuture(service, headerIdsByService.get(service));
            } else {
                List<Log_Header__c> headersForThisService = sendCallout(service, headerIdsByService.get(service));
                headers.addAll(headersForThisService);
                //system.debug('header update count: '+headers.size());
            }
        }
        // MJL-1674 - Do the update at the end
        if ((!useFuture) && (headers.isEmpty() == false)) {
            Database.update(headers);           
        }
    }

    /**
     * @service - a service defined in Callout Settings
     *  e.g. OUTBOUND_CONTACT_PROFILE_UPDATE_NOTIFY
     */
    private static RetriableCallout geRetriableImpl(final String service) {
        final String className = getCalloutSettings(service).getString('Class_Name__c');
        final Type t = Type.forName(className);
        System.assertNotEquals(null, t, 'Failed to instantiate implementation class "' + className + '" for service:' + service);
        
        final RetriableCallout impl = (RetriableCallout)t.newInstance();
        return impl;
    }
    /**
     * @service - a service defined in Callout Settings
     *  e.g. OUTBOUND_CONTACT_PROFILE_UPDATE_NOTIFY
     */
    @future(callout=true)
    private static void sendCalloutAsFuture(final String service, final Set<Id> calloutLogHeaderIds) {
        List<Log_Header__c> logHeadersForThisService = sendCallout(service, calloutLogHeaderIds);
        // MJL-1674 - Do the update at the end
        if (logHeadersForThisService.isEmpty() == false) {
            Database.update(logHeadersForThisService);                  
        }
    }
    private static List<Log_Header__c> sendCallout(final String service, final Set<Id> calloutLogHeaderIds) {
        final CustomSettings.Record config = getCalloutSettings(service);
        final String className = config.getString('Class_Name__c');
        final Type t = Type.forName(className);
        System.assertNotEquals(null, t, 'Failed to instantiate implementation class "' + className + '" for service:' + service);
        
        final RetriableCallout impl = geRetriableImpl(service);
        impl.setEndpoint(config.getString('Endpoint__c'));
        Double timeout = config.getNumber('Timeout_Milliseconds__c');
        if (null != timeout && timeout > 0) {
            impl.setTimeout(timeout.intValue());
        }
        impl.setTopic(config.getString('Topic__c'));
        impl.setClientCertificateName(config.getString('Certificate_Name__c'));

        final List<Object> recs = new List<Object>();
        final List<Log_Header__c> headers = new List<Log_Header__c>();
        //System.assert(false, 'MUST FAIL HERE, calloutLogHeaderIds=' + calloutLogHeaderIds + '; service=' + service);
        
        // MJL-1790
        if (!calloutLogHeaderIds.isEmpty()) {
            for(Log_Header__c header : [select Id, Callout_Attempts__c, Service__c, Callout_Status__c, Serialised_Data__c
                                                from Log_Header__c 
                                                where Id in :calloutLogHeaderIds and Service__c = :service
                                        ]) {
            
                if (String.isNotBlank(header.Serialised_Data__c)) {
                    Object rec = impl.deserialise(header.Serialised_Data__c);
                    recs.add(rec);
                    headers.add(header);
                }
            }           
        }

        
        if (!recs.isEmpty()) {
            try {
                impl.callService(recs);
                processSuccessfulCallout(headers);
            } catch (Exception e) {
                processFailedCallout(service, headers, e);
            }
        }
        return headers;
    }

    @TestVisible
    private static void processFailedCallout(final String service, final List<Log_Header__c> headers, final Exception e) {
        final String status = getFailureStatus(e);
        final Boolean isSoftFail = isSoftFail(status);
        final CustomSettings.Record config = getCalloutSettings(service);

        final List<Log_Header__c> headersToUpdate = new List<Log_Header__c>();
        for(Log_Header__c header : headers) {
            header.Callout_Attempts__c = MyJL_Util.getOrElse(header.Callout_Attempts__c, 0) + 1;
            header.Do_callout__c = false;
            header.Response_Data__c = e.getMessage();
            header.Error_Detail__c = e.getStackTraceString();
            header.Callout_Status__c = status;
            header.Activate_Callout_Retry_Workflow__c = false;
            
            setNextCalloutTime(config, header);
        }
        // MJL-1674
        //Database.update(headers);
    }

    private static void processSuccessfulCallout(final List<Log_Header__c> headers) {
        for(Log_Header__c header : headers) {
            header.Callout_Status__c = 'Done';
            header.Do_callout__c = false;
            header.Callout_Attempts__c = MyJL_Util.getOrElse(header.Callout_Attempts__c, 0) + 1;
            header.Activate_Callout_Retry_Workflow__c = false;
            header.Success__c = true;
            // MJL-1674, clear the errors as you no longer have any (remember this is ProcessSuccess)
            header.Error_Code__c = null;
            header.Error_Detail__c = null;
            header.Response_Data__c = null;
            // MJL-1674 - if processing time not set then try and calculate it
            if (header.Processing_Time__c == null) {
                Long processingStartInMillis = System.currentTimeMillis();
                Long processingStopInMillis = System.currentTimeMillis();
                // base it on the next callout date
                if (header.Next_Callout_Date__c != null) {
                    processingStartInMillis = header.Next_Callout_Date__c.getTime();
                }
                Long processingTime = processingStopInMillis - processingStartInMillis;
                header.Processing_Time__c = processingTime;                         
            }
        }
        // MJL-1674
        //Database.update(headers);
    }

    // MJL-1867 - workaround the callout not allowed from trigger error by detecting the error and 
    // making it a soft failure. The retry will send it in N minutes and this is ok as it is not time critical
    private static String getFailureStatus(final Exception e) {
        //system.debug('e: '+e);
        if (e instanceof System.CalloutException && e.getMessage().indexOf('IO Exception:') >=0) { 
            return CALLOUT_STATUS_SOFT_FAIL;
        } if (e instanceof System.CalloutException && e.getMessage().indexOf('Callout from triggers are currently not supported.') >=0) {
            return CALLOUT_STATUS_SOFT_FAIL;
        }else {
            return CALLOUT_STATUS_HARD_FAIL;
        }
    }
    
    private static Boolean isSoftFail(final String status) {
        return CALLOUT_STATUS_SOFT_FAIL == status;
    }

    @TestVisible
    private static void setNextCalloutTime(final CustomSettings.Record config, final Log_Header__c header) {
        final Decimal maxRetries = MyJL_Util.getOrElse(config.getNumber('Number_of_Retries__c'), 0);

        header.Activate_Callout_Retry_Workflow__c = false;
        
        if (isSoftFail(header.Callout_Status__c)){
            Decimal attempts = MyJL_Util.getOrElse(header.Callout_Attempts__c, 0);
            //system.debug('attempts: '+attempts);
            if ( attempts < maxRetries) {
                Integer extraMinutes = (attempts / maxRetries * 60).intValue();
                Datetime nextCalloutDate = System.now().addMinutes(extraMinutes);
                header.Next_Callout_Date__c = nextCalloutDate;
                header.Activate_Callout_Retry_Workflow__c = true;
                //system.debug('header: '+header);
            } else {
                header.Callout_Status__c = 'Persistent Timeout';
            }
        } else {
            //system.debug('hard error');
        }       
    }

    private static void addToMap(Map<String, Set<Id>> valuesByKey, String key, final Id value) {
        Set<Id> values = valuesByKey.get(key);
        if (null == values) {
            values = new Set<Id>();
            valuesByKey.put(key, values);
        }
        values.add(value);

    }
    
    private static CustomSettings.Record getCalloutSettings(final String service) {
        final CustomSettings.Record config = CustomSettings.getCalloutSetting(service);
        System.assertNotEquals(null, config.getString('name'), 'Invalid config, no config defined for service ' + service + ' in Callout_Settings__c');
        return config;
    }

    /**
     * callout log header is very different from normal Inbound webservice call log header
     * we are just using the same object but there is no good reason to put this code in LogUtils
     * because callout "log" is not a real log, but rather a Retry mechanism data storage.
     * @return: callout log header Ids
     */
    private static Set<Id> startCalloutHeader(final Map<String, List<Object>> objectsByServiceName, final String calloutStatus) {
        final List<Log_Header__c> headers  = new List<Log_Header__c>();
        final set<string> shopperIdwithNullEmailaddress = new set<string>();
        final set<string> shopperIdwithNullActivationDates = new set<string>();

        Boolean sendAccountsWithOutEmails = CustomSettings.getMyJLSetting().getBoolean('Send_My_JL_Accounts_without_email__c');

        for(String serviceName : objectsByServiceName.keySet()) {
            if (isCalloutEnabled(serviceName)) {
                Serialisable serialiser = CalloutHandler.geRetriableImpl(serviceName);
                for(Object rec : objectsByServiceName.get(serviceName)) {
                    system.debug('rec'+rec);
                    final Log_Header__c logHeader = new Log_Header__c();
                    //logHeader.Session_Id__c = UserInfo.getSessionId();                    
                    logHeader.Service__c = serviceName;
                    logHeader.Callout_Status__c = calloutStatus;
                    //record serialised version of every SObject involved in a callout
                    logHeader.Serialised_Data__c = serialiser.serialise(rec);
                    logHeader.Shopper_ID__c = getShopperId(rec, serviceName);
                   
                    //restric to send full join notify if registration email address is null
                    if(logHeader.Serialised_Data__c != null && logHeader.Service__c == 'FULL_JOIN_NOTIFY'){
                        if(logHeader.Shopper_ID__c != null && logHeader.Serialised_Data__c.contains('"RegistrationEmailAddressID":null') 
                            && !sendAccountsWithOutEmails){
                            shopperIdwithNullEmailaddress.add(logHeader.Shopper_ID__c); 
                            logheader.Resend_Null_Email_Join_Notifications__c = true;
                        }   
                    }
                    
                    //restric to send full join notify if registration activation date is null
                   if(logHeader.Serialised_Data__c != null && (logHeader.Service__c == 'FULL_JOIN_NOTIFY' || logHeader.Service__c == 'LEAVE_NOTIFY' )){
                        if(logHeader.Shopper_ID__c != null && logHeader.Serialised_Data__c.contains('"ActivationDateTime":null')){
                            shopperIdwithNullActivationDates.add(logHeader.Shopper_ID__c); 
                        }   
                    }
                    headers.add(logHeader);
                }
            }
        }

        Database.insert(headers);

        final Set<Id> headerIds  = new Set<Id>();
        Integer i=0;
        for(Log_Header__c header: headers) {
            String serviceName = header.Service__c;
            if (String.isNotBlank(serviceName) && !shopperIdwithNullEmailaddress.contains(header.shopper_id__c) && !shopperIdwithNullActivationDates.contains(header.shopper_id__c)) {
                headerIds.add(header.Id);
            }

        }
        return headerIds;
    }

    private static Boolean isCalloutEnabled(final String serviceName) {
        return true != getCalloutSettings(serviceName).getBoolean('Disabled__c');
    }

    private static String getShopperId(Object rec, String serviceName) {
        String shopperId = null;

        /* original */
        //system.debug('serviceName: '+serviceName);
        // NB We don't have a Shopper Id in Partial Join
        if ((serviceName == MyJL_LoyaltyAccountHandler.FULL_JOIN_NOTIFY) || 
             (serviceName == MyJL_LoyaltyAccountHandler.LEAVE_NOTIFY) ||
             (serviceName == MyJL_LoyaltyAccountHandler.PARTIAL_JOIN_NOTIFY)) {
            SFDCMyJLCustomerTypes.Customer customer = (SFDCMyJLCustomerTypes.Customer) rec;
            //system.debug('customer: '+customer);
            if (customer != null) {
                shopperId = myJL_Util.getCustomerId(customer);
                //system.debug('shopperId: '+shopperId);
            }
        } else if (serviceName == MyJL_LoyaltyAccountHandler.OUTBOUND_CONTACT_PROFILE_UPDATE_NOTIFY) {
            contact cp = (contact) rec;
            //system.debug('customer: '+customer);
            if (cp != null) {
                shopperId = cp.shopper_Id__c;
                //system.debug('shopperId: '+shopperId);
            }           
        }
        /* original */      
        return shopperId;
    }
}