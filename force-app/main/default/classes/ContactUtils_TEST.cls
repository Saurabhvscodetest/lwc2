@isTest
private class ContactUtils_TEST {
  
  static testMethod void test_cleanNullValue(){
  
      //if input is not null, should return the input value unaltered 
      system.assertEquals(ContactUtils.cleanNullValue('My Value'), 'My Value');
      
      //if input is null, should return an empty string
      system.assertEquals(ContactUtils.cleanNullValue(null), ''); 
      
  }
    
  static testMethod void test_cleanMatchingValue(){
  
      //if input is null, should return an empty string
      system.assertEquals(ContactUtils.cleanMatchingValue(null), ''); 
      
      //should convert to lower case
      system.assertEquals(ContactUtils.cleanMatchingValue('MyUpperValX'), 'myuppervalx'); 
      
      //should leave lowercase alone
      system.assertEquals(ContactUtils.cleanMatchingValue('myval'), 'myval');
      
      //should remove spaces
      system.assertEquals(ContactUtils.cleanMatchingValue(' my val '), 'myval');
      system.assertEquals(ContactUtils.cleanMatchingValue(' My Val '), 'myval');  
          
      //should remove CRLF from the input string
      system.assertEquals(ContactUtils.cleanMatchingValue('MyVal\r\n'), 'myval');
      system.assertEquals(ContactUtils.cleanMatchingValue('MyVal\r\nMyNextVal'), 'myvalmynextval');
      system.assertEquals(ContactUtils.cleanMatchingValue('\r\nMyVal'), 'myval');
      
      //should remove LF from the input string
      system.assertEquals(ContactUtils.cleanMatchingValue('MyVal\n'), 'myval');
      system.assertEquals(ContactUtils.cleanMatchingValue('MyVal\nMyNextVal'), 'myvalmynextval');
      system.assertEquals(ContactUtils.cleanMatchingValue('\nMyVal'), 'myval');
  
  }      
  
        
  static testMethod void test_validateCaseMatchingKeys(){ 
  
      //should return false if it can't make at least one of the two keys
      //can't test every combination here but we'll try the most common in the data
      //i.e. null, empty string, or spaces only, in component fields for the keys
      
      //key 1 invalid combinations - key2 all nulls
      system.assertEquals(ContactUtils.validateCaseMatchingKeys(null, null, null, null, null), false);
      system.assertEquals(ContactUtils.validateCaseMatchingKeys('myval', null, null, null, null), false);
      system.assertEquals(ContactUtils.validateCaseMatchingKeys(null, 'myval', null, null, null), false);
      system.assertEquals(ContactUtils.validateCaseMatchingKeys(null, null, 'myval', null, null), false);
      system.assertEquals(ContactUtils.validateCaseMatchingKeys('myval', 'myval', null, null, null), false); 
      system.assertEquals(ContactUtils.validateCaseMatchingKeys('myval', null, 'myval', null, null), false); 
      system.assertEquals(ContactUtils.validateCaseMatchingKeys(null, 'myval', 'myval', null, null), false); 
      
      system.assertEquals(ContactUtils.validateCaseMatchingKeys('', '', '', null, null), false);
      system.assertEquals(ContactUtils.validateCaseMatchingKeys('myval', '', '', null, null), false);
      system.assertEquals(ContactUtils.validateCaseMatchingKeys('', 'myval', '', null, null), false);
      system.assertEquals(ContactUtils.validateCaseMatchingKeys('', '', 'myval', null, null), false);
      system.assertEquals(ContactUtils.validateCaseMatchingKeys('myval', 'myval', '', null, null), false); 
      system.assertEquals(ContactUtils.validateCaseMatchingKeys('myval', '', 'myval', null, null), false); 
      system.assertEquals(ContactUtils.validateCaseMatchingKeys('', 'myval', 'myval', null, null), false); 
      
      system.assertEquals(ContactUtils.validateCaseMatchingKeys(' ', ' ', ' ', null, null), false);
      system.assertEquals(ContactUtils.validateCaseMatchingKeys('myval', ' ', ' ', null, null), false);
      system.assertEquals(ContactUtils.validateCaseMatchingKeys(' ', 'myval', ' ', null, null), false);
      system.assertEquals(ContactUtils.validateCaseMatchingKeys(' ', ' ', 'myval', null, null), false);
      system.assertEquals(ContactUtils.validateCaseMatchingKeys('myval', 'myval', ' ', null, null), false); 
      system.assertEquals(ContactUtils.validateCaseMatchingKeys('myval', ' ', 'myval', null, null), false); 
      system.assertEquals(ContactUtils.validateCaseMatchingKeys(' ', 'myval', 'myval', null, null), false); 
      
      //key2 additional invalid combinations 
      system.assertEquals(ContactUtils.validateCaseMatchingKeys('myval', null, null, 'myval', null), false); 
      system.assertEquals(ContactUtils.validateCaseMatchingKeys('myval', null, null, null, 'myval'), false); 
      system.assertEquals(ContactUtils.validateCaseMatchingKeys('myval', null, null, 'myval', 'myval'), false);    
      system.assertEquals(ContactUtils.validateCaseMatchingKeys(null, 'myval', null, 'myval', null), false); 
      system.assertEquals(ContactUtils.validateCaseMatchingKeys(null, 'myval', null, null, 'myval'), false);  
      system.assertEquals(ContactUtils.validateCaseMatchingKeys(null, 'myval', null, 'myval', 'myval'), false);
      system.assertEquals(ContactUtils.validateCaseMatchingKeys('myval', 'myval', null, 'myval', null), false); 
      system.assertEquals(ContactUtils.validateCaseMatchingKeys('myval', 'myval', null, null, 'myval'), false); 

      //key1 valid, key2 invalid
      system.assertEquals(ContactUtils.validateCaseMatchingKeys('myval', 'myval', 'myval', null, 'myval'), true);  
      
      //key2 valid, key1 invalid
      system.assertEquals(ContactUtils.validateCaseMatchingKeys('myval', 'myval', null, 'myval', 'myval'), true);  
      
      //both key1 and key2 valid
      system.assertEquals(ContactUtils.validateCaseMatchingKeys('myval', 'myval', 'myval', 'myval', 'myval'), true);      
           
  }       
    
    
  static testMethod void test_createMatchKey1(){ 
    
        //should return a valid key
        system.assertEquals(ContactUtils.createMatchKey1('Val1', 'val2', 'VAL 3'),'val1val2val3');
        system.assertEquals(ContactUtils.createMatchKey1('Val1\r\n', 'val2', 'VAL 3\n'),'val1val2val3');
    
        //should return an empty string
        system.assertEquals(ContactUtils.createMatchKey1('', '', ''),'');
        system.assertEquals(ContactUtils.createMatchKey1('', 'val2', 'VAL 3'),'');
        system.assertEquals(ContactUtils.createMatchKey1('Val1', '', 'VAL 3'),'');
        system.assertEquals(ContactUtils.createMatchKey1('Val1', 'val2', ''),'');
        
        system.assertEquals(ContactUtils.createMatchKey1(null, null, null),'');
        system.assertEquals(ContactUtils.createMatchKey1(null, 'val2', 'VAL 3'),'');
        system.assertEquals(ContactUtils.createMatchKey1('Val1', null, 'VAL 3'),'');
        system.assertEquals(ContactUtils.createMatchKey1('Val1', 'val2', null),'');
        
        
  }
    
  static testMethod void test_createMatchKey2(){ 
    
        //should return a valid key
        system.assertEquals(ContactUtils.createMatchKey2('Val1', 'val2', 'VAL 3', 'val4'),'val1val2val3val4');
        system.assertEquals(ContactUtils.createMatchKey2('Val1\r\n', 'val2', 'VAL 3\n', 'va l4'),'val1val2val3val4');
    
        //should return an empty string
        system.assertEquals(ContactUtils.createMatchKey2('', '', '',''),'');
        system.assertEquals(ContactUtils.createMatchKey2('', 'val2', 'VAL 3', 'val4'),'');
        system.assertEquals(ContactUtils.createMatchKey2('Val1', '', 'VAL 3', 'val4'),'');
        system.assertEquals(ContactUtils.createMatchKey2('Val1', 'val2', '', 'val4'),'');
        system.assertEquals(ContactUtils.createMatchKey2('Val1', 'val2', 'VAL 3', ''),'');
        
        system.assertEquals(ContactUtils.createMatchKey2(null, null, null, null),'');
        system.assertEquals(ContactUtils.createMatchKey2(null, 'val2', 'VAL 3', 'val4'),'');
        system.assertEquals(ContactUtils.createMatchKey2('Val1', null, 'VAL 3', 'val4'),'');
        system.assertEquals(ContactUtils.createMatchKey2('Val1', 'val2', null, 'val4'),'');
        system.assertEquals(ContactUtils.createMatchKey2('Val1', 'val2', 'VAL 3', null),'');
        
  } 
    
  
  static testMethod void test_validateCaseMatchKey1(){ 
    
      //invalid combinations 
      system.assertEquals(ContactUtils.validateCaseMatchKey1(null, null, null), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey1('myval', null, null), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey1(null, 'myval', null), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey1(null, null, 'myval'), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey1('myval', 'myval', null), false); 
      system.assertEquals(ContactUtils.validateCaseMatchKey1('myval', null, 'myval'), false); 
      system.assertEquals(ContactUtils.validateCaseMatchKey1(null, 'myval', 'myval'), false); 
      
      system.assertEquals(ContactUtils.validateCaseMatchKey1('', '', ''), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey1('myval', '', ''), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey1('', 'myval', ''), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey1('', '', 'myval'), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey1('myval', 'myval', ''), false); 
      system.assertEquals(ContactUtils.validateCaseMatchKey1('myval', '', 'myval'), false); 
      system.assertEquals(ContactUtils.validateCaseMatchKey1('', 'myval', 'myval'), false); 
      
      system.assertEquals(ContactUtils.validateCaseMatchKey1(' ', ' ', ' '), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey1('myval', ' ', ' '), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey1(' ', 'myval', ' '), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey1(' ', ' ', 'myval'), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey1('myval', 'myval', ' '), false); 
      system.assertEquals(ContactUtils.validateCaseMatchKey1('myval', ' ', 'myval'), false); 
      system.assertEquals(ContactUtils.validateCaseMatchKey1(' ', 'myval', 'myval'), false);    
      
      //key1 valid,
      system.assertEquals(ContactUtils.validateCaseMatchKey1('myval', 'myval', 'myval'), true);     
    
  }
  
  
  static testMethod void test_validateCaseMatchKey2(){ 
    
      //invalid combinations 
      system.assertEquals(ContactUtils.validateCaseMatchKey2(null, null, null, null), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey2('myval', null, null, null), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey2(null, 'myval', null, null), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey2(null, null, 'myval', null), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey2(null, null, null, 'myval'), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey2('myval', 'myval', null, null), false); 
      system.assertEquals(ContactUtils.validateCaseMatchKey2('myval', null, 'myval', null), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey2('myval', null, null, 'myval'), false); 
      system.assertEquals(ContactUtils.validateCaseMatchKey2(null, 'myval', 'myval','myval'), false); 
      system.assertEquals(ContactUtils.validateCaseMatchKey2(null, null, 'myval','myval'), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey2(null, 'myval', null,'myval'), false);  

      system.assertEquals(ContactUtils.validateCaseMatchKey2('', '', '', ''), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey2('myval', '', '', ''), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey2('', 'myval', '', ''), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey2('', '', 'myval', ''), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey2('', '', '', 'myval'), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey2('myval', 'myval', '', ''), false); 
      system.assertEquals(ContactUtils.validateCaseMatchKey2('myval', '', 'myval', ''), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey2('myval', '', '', 'myval'), false); 
      system.assertEquals(ContactUtils.validateCaseMatchKey2('', 'myval', 'myval','myval'), false); 
      system.assertEquals(ContactUtils.validateCaseMatchKey2('', '', 'myval','myval'), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey2('', 'myval', '','myval'), false);  
      
      system.assertEquals(ContactUtils.validateCaseMatchKey2(' ', ' ', ' ', ' '), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey2('myval', ' ', ' ', ' '), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey2(' ', 'myval', ' ', ' '), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey2(' ', ' ', 'myval', ' '), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey2(' ', ' ', ' ', 'myval'), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey2('myval', 'myval', ' ', ' '), false); 
      system.assertEquals(ContactUtils.validateCaseMatchKey2('myval', ' ', 'myval', ' '), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey2('myval', ' ', ' ', 'myval'), false); 
      system.assertEquals(ContactUtils.validateCaseMatchKey2(' ', 'myval', 'myval','myval'), false); 
      system.assertEquals(ContactUtils.validateCaseMatchKey2(' ', ' ', 'myval','myval'), false);
      system.assertEquals(ContactUtils.validateCaseMatchKey2(' ', 'myval', ' ','myval'), false);  
      
      //key2 valid,
      system.assertEquals(ContactUtils.validateCaseMatchKey2('myval', 'myval', 'myval', 'myval'), true);     
    
  }  
    

    
  static testMethod void test_updateContactCaseMatchKeys(){ 
  
      //set good contact keys for key1
      Contact c1 = UnitTestDataFactory.CreateContact();
      c1.FirstName = 'Matt';
      c1.LastName = 'Shilvock';
      c1.Email = 'mshilvock@salesforce.com';
      
      ContactUtils.updateContactCaseMatchKeys(c1);
      system.assertEquals(c1.Case_Matching_Key_1__c, 'mattshilvockmshilvock@salesforce.com');
      system.assertEquals(c1.Case_Matching_Key_2__c, '');
      
      
      //set good contact keys for key2
      Contact c2 = UnitTestDataFactory.CreateContact();
      c2.FirstName = 'Clare';
      c2.LastName = 'Shilvock';
      c2.MailingStreet = 'Flat 1A';
      c2.MailingPostalCode = 'B50 3AX';
      
      ContactUtils.updateContactCaseMatchKeys(c2);
      system.assertEquals(c2.Case_Matching_Key_1__c, '');
      system.assertEquals(c2.Case_Matching_Key_2__c, 'clareshilvockflat1ab503ax');
      
      //set good contact keys for key1 and for Key2
      Contact c3 = UnitTestDataFactory.CreateContact();
      c3.FirstName = 'Clare';
      c3.LastName = 'Shilvock';
      c3.Email = 'clare@madeupemail.com';
      c3.MailingStreet = 'Flat 1A';
      c3.MailingPostalCode = 'B50 3AX';
      
      ContactUtils.updateContactCaseMatchKeys(c3);
      system.assertEquals(c3.Case_Matching_Key_1__c, 'clareshilvockclare@madeupemail.com');
      system.assertEquals(c3.Case_Matching_Key_2__c, 'clareshilvockflat1ab503ax');

  }
    
  
  static testMethod void test_updateContactProfileCaseMatchKeys(){ 

      Contact c = UnitTestDataFactory.CreateContact();
      
      //set good contactProfile keys for key1
      Contact_Profile__c cp1 = UnitTestDataFactory.CreateContactProfile(c);
      cp1.FirstName__c = 'Matt';
      cp1.LastName__c = 'Shilvock';
      cp1.Email__c = 'mshilvock@salesforce.com';
      
      ContactUtils.updateContactProfileCaseMatchKeys(cp1);
      system.assertEquals(cp1.Case_Matching_Key_1__c, 'mattshilvockmshilvock@salesforce.com');
      system.assertEquals(cp1.Case_Matching_Key_2__c, '');
      
      
      //set good contactProfile keys for key2
      Contact_Profile__c cp2 = UnitTestDataFactory.CreateContactProfile(c);
      cp2.FirstName__c = 'Clare';
      cp2.LastName__c = 'Shilvock';
      cp2.Mailing_Street__c  = 'Flat 1A';
      cp2.MailingPostCode__c = 'B50 3AX';
      
      ContactUtils.updateContactProfileCaseMatchKeys(cp2);
      system.assertEquals(cp2.Case_Matching_Key_1__c, '');
      system.assertEquals(cp2.Case_Matching_Key_2__c, 'clareshilvockflat1ab503ax');
      
      //set good contactProfile keys for key1 and for Key2
      Contact_Profile__c cp3 = UnitTestDataFactory.CreateContactProfile(c);
      cp3.FirstName__c = 'Clare';
      cp3.LastName__c = 'Shilvock';
      cp3.Email__c = 'clare@madeupemail.com';
      cp3.Mailing_Street__c = 'Flat 1A';
      cp3.MailingPostCode__c = 'B50 3AX';
      
      ContactUtils.updateContactProfileCaseMatchKeys(cp3);
      system.assertEquals(cp3.Case_Matching_Key_1__c, 'clareshilvockclare@madeupemail.com');
      system.assertEquals(cp3.Case_Matching_Key_2__c, 'clareshilvockflat1ab503ax');

  }
    
     static testMethod void test_updateCustomerOwner(){ 
         Contact c = UnitTestDataFactory.CreateContact();
         c.Assign_Customer_to_Me__c = false;
         insert c;
         
         test.startTest();
         Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];

         User usr = new User(Alias = 'standt', Email='Testjlp@tjlptuser.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = p.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='Testtest@t.com');
            //insert usr ;
            //System.runAs(usr)
            //{
                c.Assign_Customer_to_Me__c = true;
                update c;
            //}
         test.stopTest();
         
     }
}