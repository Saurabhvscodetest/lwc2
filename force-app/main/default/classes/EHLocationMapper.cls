/*
 Manges the configuration of the CDH Locations. 
*/
public class EHLocationMapper {
    /*Map to hold the cdh loations and thier corresponding display name */
    static Map<String, String> mappingLocations= null;
    static Map<Integer, String> locationIdMap= new Map<Integer, String>();
    public static final String SPLITTER = '-';
    
    static {
        Map<String, EHCDHLocation__c> locationsMap = EHCDHLocation__c.getAll();
            mappingLocations = new Map<String, String>();
            if(locationsMap != null && locationsMap.size() > 0){
                for(String key:  locationsMap.keySet()){
                	/*
	            		Populates the maps with the below Info 
	            		Key   - EventHub CDH name.
	            		Value - Display Name in Connex.  
	            	*/
                    EHCDHLocation__c location = locationsMap.get(key);
                    mappingLocations.put(key.toUpperCase(), location.Value__c);
                	system.debug('KEy' + key);
                    if(String.isNotBlank(key)){
	                    try{
	                    	String[] cdhValues = key.split(SPLITTER);
	                    	Integer locationId = Integer.valueOf(cdhValues[0].trim());
	                    	String cdhValue = cdhValues[1].trim();
	                    	locationIdMap.put(locationId, cdhValue);
	                    } catch (Exception e) {
	                    	//Its ok to NOT handle the exceptions, since we need to pupulate the map only if we have a valid value 
	                    }
            		}
                	system.debug('locationIdMap' + locationIdMap);
                }
        }
    }
    
    /* Get the display name for the CDH */
    public static String getMappedLocation(String ehValue){
        String location = null;
        if(String.isNotBlank(ehValue)){
            location = mappingLocations.get(ehValue.toUpperCase());
            if(String.isBlank(location)){
                    location = ehValue;
            } 
        }
        return location;
    }

    public static String getLocation(String locationId){
    	String cdhValue = null;
    	try {
    		Integer locationIdNo = Integer.valueOf(locationId.trim());
    		cdhValue = locationIdMap.get(locationIdNo);
    		cdhValue = cdhValue.trim();
    	} catch (Exception e){
    		cdhValue = null;
    	}
    	return cdhValue;
    }
}