/**
 *	@author: Various
 *	@date: 2014-06-20 11:02:13 
 *	@description:
 *	    unit tests for MyJL_CreateCustomerHandler
 *	
 *	Version History :   
 *	2014-07-21 - MJL-490 - AG
 *	check that when enough information is provided on Create then customer is
 *	promoted to full profile without intermediate Potential_Customer__c step,
 *	i.e. Contact_Profile__c + Contact are created instead of Potential_Customer__c
 *	
 *	001		13/11/14	NTJ		MJL-1294	Unit test needs to create the custom settings when SeeAllData not set	
 *	002		14/04/15	NTJ		MJL-1784	Only convert the profile if the data comes from a Billing Address
 *	003		30/04/15	NTJ		MJL-1823 	Add tests for async createCustomer/UpdateCustomer/FullJoin sequences
 *	003		30/04/15	NTJ		MJL-1871 	Add tests for async createCustomer/UpdateCustomer/PartialJoin sequences
 *  004     07/07/16    NA					Decommissioning Contact Profile
 */
@isTest
public class MyJL_CreateCustomerHandlerTest {
	static String SOURCE_SYSTEM = System.Label.MyJL_JL_comSourceSystem;
	
	static {
		BaseTriggerHandler.addSkipReason(CalloutHandler.SKIP_ALL_NOTIFICATIONS_CODE, 'disable outbound events in this test class');
	}
	/**
	 * check that if single record sent with enough details then Potential_Customer__c gets created
	 */
	static testMethod void testSingleRecordInsert () {
		// temp out
		//return;

		// Ensure custom setting exists
		MyJL_TestUtil.createCustomSettings();
		 
		SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
		final String email = 'testSingleRecordInsert@example.com';
		SFDCMyJLCustomerTypes.Customer customer = MyJL_TestUtil.getCustomer('shopper1', new Map<String, Object>{'EmailAddress' => email});
		 
		request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer};
		
        Test.StartTest();
        SFDCMyJLCustomerServices.CreateCustomer(request.RequestHeader, new SFDCMyJLCustomerTypes.Customer[]{customer});
		Test.StopTest();

		//now check if record has been created
		System.assertEquals(1, [select count() from Potential_Customer__c where Email__c = :email and Shopper_ID__c = :'shopper1'], 
								'Expected exactly 1 record of Potential_Customer__c to be created with email=' + email + '; shopperId=' + 'shopper1');
		
		//check that MyJL_ESB_Message_ID__c & Source_System__c fields are set
		final Potential_Customer__c partialCustomer = [select MyJL_ESB_Message_ID__c, Source_System__c from Potential_Customer__c 
														where Email__c = :email and Shopper_ID__c = :'shopper1'];
		System.assert(!String.isBlank(partialCustomer.MyJL_ESB_Message_ID__c), 'Value of MyJL_ESB_Message_ID__c is missing');
		System.assert(!String.isBlank(partialCustomer.Source_System__c), 'Value of Source_System__c is missing');
		//System.assertEquals(partialCustomer.ShopperIdAndSourceSystem__c,'shopper1@JL.com');
		
	}
	
	/**
	 * check that if several records sent with enough details then Potential_Customer__c records are created
	 */
	 
	static testMethod void testMultiRecordInsert () {
		// temp out
		//return;
		// Ensure custom setting exists
		MyJL_TestUtil.createCustomSettings();

		SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
		final String email = 'testSingleRecordInsert@example.com';
		final List<SFDCMyJLCustomerTypes.Customer> customers = new List<SFDCMyJLCustomerTypes.Customer>();
		for(Integer i = 0; i < 5; i++) {
			SFDCMyJLCustomerTypes.Customer customer = MyJL_TestUtil.getCustomer('shopper' + i, new Map<String, Object>{'EmailAddress' => email + i});
			customers.add(customer);
		    
		}
		request.Customer = customers;

		Test.StartTest();
        SFDCMyJLCustomerServices.CreateCustomer(request.RequestHeader, request.Customer);
		Test.StopTest();

		//check if a record has been created
		System.assertEquals(1, [select count() from Potential_Customer__c where Email__c = 'testSingleRecordInsert@example.com1' and Shopper_ID__c = :'shopper1'], 
								'Expected exactly 1 record of Potential_Customer__c to be created with email=testSingleRecordInsert@example.com1; shopperId=shopper1');
		//check if all records have been created
		System.assertEquals(5, [select count() from Potential_Customer__c where Email__c like 'testSingleRecordInsert@example.com%' and Shopper_ID__c like 'shopper%'], 
								'Expected exactly 5 records of Potential_Customer__c to be created');
		
	}
	

	/**
	 * check what happens if a request with existing email comes in
	 */
	static testMethod void testSingleDuplicateEmailRecordInsert () {
		// temp out
		//return;
		// Ensure custom setting exists
		MyJL_TestUtil.createCustomSettings();

		Database.insert(new Potential_Customer__c(Shopper_ID__c = 'shopper1', Email__c = 'mixed@CASE.com', Source_System__c = SOURCE_SYSTEM));
		SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
		final String email = 'MIXED@case.com';//email is the same but case is different
		
		SFDCMyJLCustomerTypes.Customer customer = MyJL_TestUtil.getCustomer('shopper2', new Map<String, Object>{'EmailAddress' => email});
		request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer};

		SFDCMyJLCustomerTypes.CustomerResponseType response = SFDCMyJLCustomerServices.CreateCustomer(request.RequestHeader, request.Customer);
		//check main response OK
		System.assert(MyJL_TestUtil.getResponseStatus(response).Success, 'expected main request to succeed');
		//check individual response = FAIL because email is a duplicate
		if (ASYNC_CREATE == false) {
			/* this is no longer required as shopper will not supply duplicate emails
			System.assertEquals(true, MyJL_TestUtil.getCustomerResponseStatus(response, 0).Success, 'expected second customer record to fail because it is having duplicate EmailId');
			System.assertEquals(MyJL_TestUtil.getCustomerResponseStatus(response, 0).Code,'EMAIL_ALREADY_USED');
			*/
			//System.assertEquals((response.ActionRecordResults[0]).Code,'EMAIL_ALREADY_USED');
		} else {
			System.assertEquals(true, MyJL_TestUtil.getCustomerResponseStatus(response, 0).Success, 'expected second customer record to fail because it is having duplicate EmailId');						
		}
	}

	/**
	 * check what happens if a request with existing Customer Id comes in
	 */
	static testMethod void testSingleDuplicateShopperIdRecordInsert () {
		// temp out
		//return;
		// Ensure custom setting exists
		MyJL_TestUtil.createCustomSettings();

		Database.insert(new Potential_Customer__c(Shopper_ID__c = 'shopper1', Email__c = 'mixed@CASE.com', Source_System__c = SOURCE_SYSTEM));
		SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
		final String email = 'MIXED1@case.com';//email is the same but case is different
		
		SFDCMyJLCustomerTypes.Customer customer = MyJL_TestUtil.getCustomer('shopper1', new Map<String, Object>{'EmailAddress' => email});
		request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer};

		SFDCMyJLCustomerTypes.CustomerResponseType response = SFDCMyJLCustomerServices.CreateCustomer(request.RequestHeader, request.Customer);
		//check main response OK
		System.assert(MyJL_TestUtil.getResponseStatus(response).Success, 'expected main request to succeed');
		//check individual response = FAIL because email is a duplicate
		if (ASYNC_CREATE == true) {
			System.assertEquals(true, MyJL_TestUtil.getCustomerResponseStatus(response, 0).Success, 'expected second customer record to fail because it is having duplicate shopperId');			
		} else {
			System.assertEquals(false, MyJL_TestUtil.getCustomerResponseStatus(response, 0).Success, 'expected second customer record to fail because it is having duplicate shopperId');
			System.assertEquals('CUSTOMERID_ALREADY_USED', MyJL_TestUtil.getCustomerResponseStatus(response, 0).Code);
			//System.assertEquals((response.ActionRecordResults[0]).Code,'EMAIL_ALREADY_USED');			
		}
	}
	
	/**
	 * check empty messageId
	 */
	static testMethod void testMessageIdProblems () {
		// temp out
		//return;
		// Ensure custom setting exists
		MyJL_TestUtil.createCustomSettings();

		//Empty Message Id
		SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
		request.RequestHeader.MessageId = '';
		SFDCMyJLCustomerTypes.CustomerResponseType response = SFDCMyJLCustomerServices.CreateCustomer(request.RequestHeader, request.Customer);
		//check main response FAIL
		System.assertEquals(false, MyJL_TestUtil.getResponseStatus(response).Success, 'expected main request to fail because message Id is blank');

		//null message Id
		request = MyJL_TestUtil.prepareBlankRequest();
		request.RequestHeader.MessageId = null;
		response = SFDCMyJLCustomerServices.CreateCustomer(request.RequestHeader, request.Customer);
		//check main response FAIL
		System.assertEquals(false, MyJL_TestUtil.getResponseStatus(response).Success, 'expected main request to fail because message Id is null');

		//'NULL' message Id
		request = MyJL_TestUtil.prepareBlankRequest();
		request.RequestHeader.MessageId = 'NULL';
		SFDCMyJLCustomerTypes.Customer customer = MyJL_TestUtil.getCustomer('shopper1', new Map<String, Object>{'EmailAddress' => 'some@test.com'});
		request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer};
		response = SFDCMyJLCustomerServices.CreateCustomer(request.RequestHeader, request.Customer);
		//check main response FAIL
		System.assertEquals(false, MyJL_TestUtil.getResponseStatus(response).Success, 'expected main request to fail because message Id is "NULL"');
	}
	/**
	 * check request without customers
	 */
	static testMethod void testMessageWithoutCustomers () {
		// temp out
		//return;
		// Ensure custom setting exists
		MyJL_TestUtil.createCustomSettings();

		//'NULL' message Id
		SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
		//try {
			SFDCMyJLCustomerTypes.CustomerResponseType response = SFDCMyJLCustomerServices.CreateCustomer(request.RequestHeader, request.Customer);
		//} catch (Exception e) {
			//System.assert(false, 'Exception must not be thrown. Result: ' + e);
		//}
		if (ASYNC_CREATE == true) {
			System.assertEquals(true, MyJL_TestUtil.getResponseStatus(response).Success, 'expected main request to fail because customer array is blank');			
		} else {
			System.assertEquals(false, MyJL_TestUtil.getResponseStatus(response).Success, 'expected main request to fail because customer array is blank');
			System.assertEquals(MyJL_TestUtil.getResponseStatus(response).Code,'NO_CUSTOMER_DATA');			
		}
	}

	/**
	 * check messages with missing ShopperId and Email
	 */
	static testMethod void testMissingCustomerDetailsProblems () {
		// temp out
		//return;
		// Ensure custom setting exists
		MyJL_TestUtil.createCustomSettings();

		//blank shopper Id
		SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
		final String email = 'testSingleRecordInsert@example.com';
		SFDCMyJLCustomerTypes.Customer customer = MyJL_TestUtil.getCustomer('', new Map<String, Object>{'EmailAddress' => email});
		request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer};

		SFDCMyJLCustomerServices.CreateCustomer(request.RequestHeader, request.Customer);

		//now check if record has NOT been created
		System.assertEquals(0, [select count() from Potential_Customer__c where Email__c = :email], 
								'Expected exactly 0 record of Potential_Customer__c to be created with email=' + email + '; shopperId=""');
		
		//blank Email
		request = MyJL_TestUtil.prepareBlankRequest();
		customer = MyJL_TestUtil.getCustomer('shopper1', new Map<String, Object>{'EmailAddress' => null});
		request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer};

		SFDCMyJLCustomerServices.CreateCustomer(request.RequestHeader, request.Customer);

		//now check if record has NOT been created
		System.assertEquals(0, [select count() from Potential_Customer__c where Shopper_ID__c = :'shopper1'], 
								'Expected exactly 0 records of Potential_Customer__c to be created with email=""' + '; shopperId=' + 'shopper1');
	}


	public static SFDCMyJLCustomerTypes.ResultStatus getResponseStatus(SFDCMyJLCustomerTypes.CustomerResponseType response) {
		return response.ResponseHeader.ResultStatus;
	}
	public static SFDCMyJLCustomerTypes.CustomerRecordResultType getCustomerResponseStatus(SFDCMyJLCustomerTypes.CustomerResponseType response, Integer index) {
		System.assert(response.ActionRecordResults.size() > index, 'Number of Customer Result records is lower than provided index');
		return response.ActionRecordResults[index];
	}
	/**
	 * MJL-490
	 * Condition to create Contact_Profile__c + Contact
	 *
	 *	!isBlank(getFirstName()) && !isBlank(getLastName()) && !isBlank(getEmailAddress())
	 *	OR
	 *	null != address && !isBlank(getFirstName()) && !isBlank(getLastName()) 
	 *					&& !isBlank(getAddressLine1()) && !isBlank(getPostalCode());
	 *
	 * MJL-1784 but only if the Address is a Billing Address
	 */
	static testMethod void testPromotionToFullProfile () {
		// temp out
		//return;
		SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
		SFDCMyJLCustomerTypes.Customer customer1 = MyJL_TestUtil.getCustomer('shopper1', new Map<String, Object>{'EmailAddress' => 'some1@example.com', 
																												'FirstName' => 'Alice', 'LastName' => 'Doe'});
		MyJL_TestUtil.addAddress(customer1, 'Billing', new Map<String, Object>{'AddressLine1' => 'AddressLine1', 'PostalCode' => 'AB12 3CD'});

		SFDCMyJLCustomerTypes.Customer customer2 = MyJL_TestUtil.getCustomer('shopper2-with-address', new Map<String, Object>{'FirstName' => 'Bob', 'LastName' => 'Doe'});
		MyJL_TestUtil.addAddress(customer2, 'Billing', new Map<String, Object>{'AddressLine1' => 'AddressLine1', 'PostalCode' => 'AB12 3CD'});

		SFDCMyJLCustomerTypes.Customer customer3 = MyJL_TestUtil.getCustomer('shopper3-partial', new Map<String, Object>{'EmailAddress' => 'some3@example.com', 
																												'FirstName' => 'John'});
		request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer1, customer2, customer3};

		SFDCMyJLCustomerTypes.UpdateCustomerResponseType responseTemp;
		SFDCMyJLCustomerTypes.CustomerResponseType response;
		Test.StartTest();
		MyJL_UpdateCustomerHandler.mode = MyJL_UpdateCustomerHandler.MODE_CREATE;
		responseTemp = MyJL_UpdateCustomerHandler.process(request);
		//convert UpdateCustomerResponseType into CustomerResponseType
		response = new SFDCMyJLCustomerTypes.CustomerResponseType();
		response.ActionRecordResults = responseTemp.ActionRecordResults;
		response.ResponseHeader = responseTemp.ResponseHeader;
		Test.StopTest();

		//check main response OK
		System.assert(getResponseStatus(response).Success, 'expected main request to succeed');
		
		//check that exactly 2 profiles were immediately converted
		final List<Contact> profiles = [select Id, Shopper_ID__c, firstname,accountid, email, mailingstreet 
													from contact order by Shopper_ID__c];  //<<004>>
		System.assertEquals(2, profiles.size(), 'Expected exactly 2 records' + profiles );
		//check that incomplete profile was created for shopper3-partial
		System.assertEquals(1, [select count() from Potential_Customer__c where Email__c = 'some3@example.com'], 'Expected exactly 1 record' );

		//check that shopper1 && shopper2-with-address were created as Contact_Profile__c + Contact with corect data
		//shopper1
		System.assertEquals('some1@example.com', profiles[0].email, 'some1@example.com must have contact by now');  //<<004>>
		//..System.assertNotEquals(null, profiles[0].accountid, 'Expected Contact_Profile__c to be linked to contact');  //<<004>>
		
		//shopper2-with-address
		System.assertEquals('Bob', profiles[1].firstname, 'shopper2-with-address - invalid first name');  //<<004>>
		//..System.assertNotEquals(null, profiles[1].accountid, 'Expected account to be linked to contact');  //<<004>>
		System.assertEquals('AddressLine1', profiles[1].mailingstreet, 'shopper2-with-address must have an address  ');	//<<004>>		
	}
	
	private static final Boolean ASYNC_CREATE = MyJL_UpdateCustomerHandler.ASYNC_CREATE;
	
	// MJL-1823 tests for async operations
	
	// happy path - this is how it should be
	// create produces a potential customer
	// update makes a contact, account, contact profile (links them together right to left) - delete the potential customer 
	// join makes a Loyalty Account, Loyalty Card and links to the Contact Profile
	//
	// We need two tests as we can only execute one async call per test	

	// pretend the update, we are testing the async create here
	private static testmethod void test_create_update_join_realcreate() {	
		// temp out
		//return;
		// Need start/stop to envelope the async call, StopTest() waits for the async to complete
		Test.StartTest();
			// Do the Create
			realCreate();
		Test.StopTest();
		
		testCreate(true);
		
		pretendUpdate(false);
		testUpdate(true);
		
		fullJoin();
		testFullJoin(true, false);		 
	}

	// pretend the create, we are testing the async update here
	private static testmethod void test_create_update_join_realupdate() {
		// temp out
		//return;
		pretendCreate();
		testCreate(true);

		// Need start/stop to envelope the async call, StopTest() waits for the async to complete
		Test.StartTest();
		realUpdate();
		Test.StopTest();
		
		testUpdate(true);
		
		fullJoin();
		testFullJoin(true, false);		 
	}

	// Async Create completes but Async Update does not occur until after the Full Join
	//
	// create produces a potential customer
	// join makes a Loyalty Account, Loyalty Card 
	// Update makes a contact, account, contact profile (links them together right to left), also links Loyalty Account to Contact Profile - delete the potential customer
	//
	// We need two tests as we can only execute one async call per test	

	// pretend the update
	private static testmethod void test_create_join_update_realcreate() {
		// temp out
		//return;
		// Need start/stop to envelope the async call, StopTest() waits for the async to complete
		Test.StartTest();
			// Do the Create
			realCreate();
		Test.StopTest();
		testCreate(true);
		
		// sync join request
		fullJoin();
		testFullJoin(false, false);
		
		pretendUpdate(true);
		testupdate(true);
		
		// The update should have joined the Loyalty Account to the Contact Profile	
		testFullJoin(true, false);		 
	}
	
	// pretend the create
	private static testmethod void test_create_join_update_realupdate() {
		// temp out
		//return;
		pretendCreate();
		
		// sync join request
		fullJoin();
		testFullJoin(false, false);

		// Need start/stop to envelope the async call, StopTest() waits for the async to complete	
		Test.StartTest();
			// Do the Create
			realUpdate();
		Test.StopTest();
		
		testUpdate(true);
		
		// The update should have joined the Loyalty Account to the Contact Profile	
		testFullJoin(true, false);		 
	}

	// Async Update completes before Async Create and full Join happens last
	//
	// update makes a contact, account, contact profile (links them together right to left) - delete the potential customer
	// create does nothing
	// join makes a Loyalty Account, Loyalty Card and links to the Contact Profile
	//
	// We need two tests as we can only execute one async call per test	
	private static testmethod void test_update_create_join_realcreate() {
		// temp out
		//return;
		pretendUpdate(false);
		testupdate(true);
		
		// Need start/stop to envelope the async call, StopTest() waits for the async to complete	
		Test.StartTest();
			// Do the Create
			realCreate();
		Test.StopTest();
		testCreate(false);

		// sync join request
		fullJoin();
		testFullJoin(true, false);
	}
	
	private static testmethod void test_update_create_join_realupdate() {
		// temp out
		//return;
		// Need start/stop to envelope the async call, StopTest() waits for the async to complete	
		Test.StartTest();
			realUpdate();
		Test.StopTest();

		testupdate(true);
		
		// Do the Create
		pretendCreate();
		testCreate(false);

		// sync join request
		fullJoin();
		testFullJoin(true, false);
		
	}
	
	// Async Update completes before Join but Async Create completes after join
	//
	// update makes a contact, account, contact profile (links them together right to left) - delete the potential customer
	// join makes a Loyalty Account, Loyalty Card and links to the Contact Profile
	// create does nothing
	//
	// We need two tests as we can only execute one async call per test	
	private static testmethod void test_update_join_create_realupdate() {
		// temp out
		//return;
		// Need start/stop to envelope the async call, StopTest() waits for the async to complete	
		Test.StartTest();
			realUpdate();
		Test.StopTest();

		testupdate(true);
		
		// sync join request
		fullJoin();
		testFullJoin(true, false);

		// Do the Create
		pretendCreate();
		testCreate(false);		
	}
	
	private static testmethod void test_update_join_create_realcreate() {
		// temp out
		//return;
		// Need start/stop to envelope the async call, StopTest() waits for the async to complete	
		pretendUpdate(false);

		testupdate(true);
		
		// sync join request
		fullJoin();
		testFullJoin(true, false);

		// Do the Create
		Test.StartTest();
			realCreate();
		Test.StopTest();
		testCreate(false);		
	}

	// Join completes before Async Create/Update 
	//
	// Join makes a Loyalty Account, Loyalty Card but cannot link to a Contact Profile
	// create makes a Potential Customer
	// update makes a Contact Profile, Deletes Potential Customer and links Contact Profile to Loyalty Account
	//
	// We need two tests as we can only execute one async call per test	
	private static testmethod void test_join_create_update_realcreate() {
		// temp out
		//return;

		// sync join request this should fail as you cannot do a full join if there is no such customer
		fullJoin();
		testFullJoinFailure();
		return;

		/*
		// Do the Create
		// Need start/stop to envelope the async call, StopTest() waits for the async to complete	
		Test.StartTest();
			realCreate();
		Test.StopTest();
		
		testCreate(true);

		pretendUpdate(false);
		testupdate(true);

		testCreate(false);
		*/		
	}			
	
	private static testmethod void test_join_create_update_realupdate() {
		// temp out
		//return;

		// sync join request this should fail as you cannot do a full join if there is no such customer
		fullJoin();
		testFullJoinFailure();
		return;

		/*
		pretendCreate();
		testCreate(true);

		// Do the Create
		// Need start/stop to envelope the async call, StopTest() waits for the async to complete	
		Test.StartTest();
			realUpdate();
		Test.StopTest();
		
		testupdate(true);
		*/
	}			

	// Join completes before Async Update /Create
	//
	// Join makes a Loyalty Account, Loyalty Card but cannot link to a Contact Profile
	// Update makes a Potential Customer Deletes Potential Customer and links Contact Profile to Loyalty Account
	// create does nothing 
	//
	// We need two tests as we can only execute one async call per test	
	private static testmethod void test_join_update_create_realupdate() {
		// temp out
		//return;

		// sync join request this should fail as you cannot do a full join if there is no such customer
		fullJoin();
		testFullJoinFailure();
		return;

		/*
		// Do the Create
		// Need start/stop to envelope the async call, StopTest() waits for the async to complete	
		Test.StartTest();
			realUpdate();
		Test.StopTest();
		
		testupdate(true);

		pretendCreate();
		testCreate(true);
		*/		
	}	
	
	private static testmethod void test_join_update_create_realcreate() {
		// temp out
		//return;

		// sync join request this should fail as you cannot do a full join if there is no such customer
		fullJoin();
		testFullJoinFailure();
		return;

		/*
		pretendUpdate(false);
		testupdate(true);

		// Do the Create
		// Need start/stop to envelope the async call, StopTest() waits for the async to complete	
		Test.StartTest();
			realCreate();
		Test.StopTest();
		testCreate(false);
		*/		
	}	

	// Partial Join
	// Test the main use case - partial join, create customer, full join
	private static testmethod void test_partialjoin_create_fulljoin_update_realcreate() {
		// temp out
		//return;

		// sync join request
		partialJoin();
		testPartialJoin(true, false);

		// Do the Create
		// Need start/stop to envelope the async call, StopTest() waits for the async to complete	
		Test.StartTest();
			realCreate();
		Test.StopTest();
		//testCreate(true);
	
		fullJoin();
		testFullJoin(false, false);

		pretendUpdate(false);
		testupdate(true);
	}	

	private static testmethod void test_partialjoin_create_update_fulljoin_realupdate() {
		// temp out
		////return;

		// sync join request
		partialJoin();
		testPartialJoin(true, false);

		pretendCreate();
		testCreate(true);
	
		// Do the Create
		// Need start/stop to envelope the async call, StopTest() waits for the async to complete	
		Test.StartTest();
			realUpdate();
		Test.StopTest();
		testupdate(true);

		fullJoin();
		// todo
		//testFullJoin(true, false);
	}	

	// helper methods
	private static final String SHOPPERID='shopper1';
	private static final String EMAIL='yacht.boat@yacht.boat.edu';
	
	private static SFDCMyJLCustomerTypes.CustomerResponseType realCreate() {
		SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
		SFDCMyJLCustomerTypes.Customer customer1 = MyJL_TestUtil.getCustomer(SHOPPERID, new Map<String, Object>{'EmailAddress' => 'some1@example.com', 
																												'FirstName' => 'Alice', 'LastName' => 'Doe'});
		request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer1};
		
		SFDCMyJLCustomerTypes.UpdateCustomerResponseType responseTemp;
		SFDCMyJLCustomerTypes.CustomerResponseType response;

		MyJL_UpdateCustomerHandler.mode = MyJL_UpdateCustomerHandler.MODE_CREATE;
		responseTemp = MyJL_UpdateCustomerHandler.process(request);
		//convert UpdateCustomerResponseType into CustomerResponseType
		response = new SFDCMyJLCustomerTypes.CustomerResponseType();
		response.ActionRecordResults = responseTemp.ActionRecordResults;
		response.ResponseHeader = responseTemp.ResponseHeader;
		
		return response;
	}
	
	private static void pretendCreate() {
		Potential_Customer__c pc = new Potential_Customer__c(Shopper_ID__c=SHOPPERID, Source_System__c=SOURCE_SYSTEM,Email__c=EMAIL);
		insert pc;
	}	
	
	private static void testCreate(Boolean normalSequence) {
		if (normalSequence) {
			// should create a pc
			List<Potential_Customer__c> pcs = [SELECT Id FROM Potential_Customer__c];
			system.assertNotEquals(null, pcs);
			system.assertEquals(1, pcs.size());
		} else {
			// should be a cp
			testUpdate(true);
		}
	}
	
	private static void realUpdate() {
		SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
		//shopper1 - Contact_Profile__c should be updated
		SFDCMyJLCustomerTypes.Customer customer1 = MyJL_TestUtil.getCustomer('shopper1', new Map<String, Object>{'FirstName' => 'Alice', 'LastName' => 'Doe', 
																												'EmailAddress' => EMAIL,
																												'TelephoneNumber' => '01256 000 000'});
		MyJL_TestUtil.addAddress(customer1, 'Billing', new Map<String, Object>{'AddressLine1' => 'Some Street 1', 'PostalCode' => 'AB12 3CD'});
		request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer1};

		SFDCMyJLCustomerTypes.UpdateCustomerResponseType response;
		response = MyJL_UpdateCustomerHandler.process(request);
		//check main response OK
		System.assert(response.ResponseHeader.ResultStatus.Success, 'expected main request to succeed');
		
	}
	
	private static void pretendUpdate(Boolean linkLA) {
		contact cp = new contact(lastname='Doe',firstname='Alice',Shopper_Id__c=SHOPPERID, email=EMAIL, SourceSystem__c=SOURCE_SYSTEM);  //<<004>>
		system.debug(cp);
		insert cp;
		
		List<Potential_Customer__c> pcs = [SELECT Id FROM Potential_Customer__c];
		delete pcs;

		if (linkLA) {
			List<Loyalty_Account__c> las = [SELECT Id FROM Loyalty_Account__c];
			system.assertEquals(1, las.size());
			las[0].contact__c=cp.Id;   //<<004>>
			update las;
		}
	}	
	
	private static contact cpX;  //<<004>>
	private static void testUpdate(Boolean normalSequence) {
		if (normalSequence) {
			List<contact> cps = [SELECT id FROM contact];  //<<004>>
			system.assertNotEquals(null, cps);
			system.assertEquals(1, cps.size());
			cpX = cps[0];
		} else {
			
		}
	}

	private static void fullJoin() {
		// Ensure custom setting exists
		MyJL_TestUtil.createCustomSettings();
		// turn logging ON
		CustomSettings.setTestLogHeaderConfig(true, -1);
		CustomSettings.setTestLogDetailConfig(true, -1);
		// prepare request
		SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
		SFDCMyJLCustomerTypes.Customer customer1 = MyJL_TestUtil.getCustomer(SHOPPERID, new Map<String, Object>());
		request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer1};
		
        SFDCMyJLCustomerTypes.CustomerResponseType response = MyJL_JoinLoyaltyAccountHandler.process(request);
		//check main response OK
		System.assert(getResponseStatus(response).Success, 'expected main request to succeed');		
	}
	
	private static void partialJoin() {
		// Ensure custom setting exists
		MyJL_TestUtil.createCustomSettings();
		// turn logging ON
		CustomSettings.setTestLogHeaderConfig(true, -1);
		CustomSettings.setTestLogDetailConfig(true, -1);
		// prepare request
		SFDCMyJLCustomerTypes.CustomerRequestType request = MyJL_TestUtil.prepareBlankRequest();
		SFDCMyJLCustomerTypes.Customer customer1 = MyJL_TestUtil.getCustomer(null, new Map<String, Object>{'EmailAddress' => 'some1@example.com', 'CustomerLoyaltyAccountCardId' => 'test-partial-card-1'});
		request.Customer = new List<SFDCMyJLCustomerTypes.Customer>{customer1};
		
        SFDCMyJLCustomerTypes.CustomerResponseType response = MyJL_JoinLoyaltyAccountHandler.process(request);
		//check main response OK
		System.assert(getResponseStatus(response).Success, 'expected main request to succeed');		
	}

	private static void testFullJoinFailure() {
		// test we got la and lc
		List<Loyalty_Account__c> las = [SELECT Id, contact__c, Send_welcome_Email__c, Welcome_Email_Sent_Flag__c, (SELECT Id FROM MyJL_Cards__r) FROM Loyalty_Account__c];
		system.assertNotEquals(null, las);
		system.assertEquals(0, las.size());
	}

	private static void testFullJoin(Boolean normalSequence, Boolean testEmail) {
		// test we got la and lc
		List<Loyalty_Account__c> las = [SELECT Id, contact__c, Send_welcome_Email__c, Welcome_Email_Sent_Flag__c, (SELECT Id FROM MyJL_Cards__r) FROM Loyalty_Account__c];  //<<004>>
		system.assertNotEquals(null, las);
		system.assertEquals(1, las.size());
		Integer cardCount = 0;
		for (Loyalty_Card__c lc:las[0].MyJL_Cards__r) {
			cardCount++;			
		}
		system.assertEquals(1, cardCount);	
		
		if (normalSequence) {
			// test contact profile linked to loyalty account
			// get the cp
			testUpdate(true);
			// compare against la
			system.assertEquals(cpX.Id, las[0].contact__c);		 //<<004>>
		} else {
			// we could not link contact profile to loyalty account as we don't have a contact profile
			system.assertEquals(null, las[0].contact__c);		 //<<004>>
		}
		if (testEmail) {
			// test email sent			
			system.assertEquals(true, las[0].Send_welcome_Email__c);					
		}
	}
	
	private static void testPartialJoin(Boolean normalSequence, Boolean testEmail) {
		// test we got la and lc
		List<Loyalty_Account__c> las = [SELECT Id, contact__c, Send_welcome_Email__c, Welcome_Email_Sent_Flag__c, (SELECT Id FROM MyJL_Cards__r) FROM Loyalty_Account__c];
		system.assertNotEquals(null, las);
		system.assertEquals(1, las.size());
		Integer cardCount = 0;
		for (Loyalty_Card__c lc:las[0].MyJL_Cards__r) {
			cardCount++;			
		}
		system.assertEquals(1, cardCount);	
		
		if (normalSequence) {
			// we could not link contact profile to loyalty account as we don't have a contact profile
			system.assertEquals(null, las[0].Contact__c);		
		} 
		if (testEmail) {
			// test email sent			
			system.assertEquals(true, las[0].Send_welcome_Email__c);					
		}
	}			
}