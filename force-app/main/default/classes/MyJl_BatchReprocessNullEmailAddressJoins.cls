/**
 *  @author: Nawaz Ahmed
 *  @date: 2015-08-06 15:33:06 
 *  @description:
 *      this class is a scheduled batch which monitors Log_Header__c to see any null email address join events and update the request data
 *  
 *  Version History :   
 *  2015-08-06
 *  initial version
 *  
 */

public with sharing class MyJl_BatchReprocessNullEmailAddressJoins implements Database.Batchable<SObject>, Database.AllowsCallouts {
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        
        //query the records where notification has not been sent when email address is null
        return Database.getQueryLocator(
                [select Id, Callout_Status__c, Do_Callout__c, shopper_id__c,serialised_data__c, Service__c from Log_Header__c 
                    where Resend_Null_Email_Join_Notifications__c=true limit 10000]);
    }
    
    public void execute(Database.BatchableContext bc, List<SObject> scope) {
        
        //map to store email to shopper, 
        map<string,string> emailtoshopper = new map<string,string>();
        set<string> shopperids = new set<string>();
        
        //list to bulk update log headers
        list<log_header__c> updateLH = new list<log_header__c>();
        
        //loop through the scope to collect shopper ids
        
        for(Log_Header__c header : (List<Log_Header__c>)scope) {
            
            if(header.shopper_id__c !=null){
                shopperids.add(header.shopper_id__c);   
            }
            
        }
        
        //retreive email using the shopper ids from loyalty accouts
        for(loyalty_account__c getla:[select id,email_address__c,shopperid__c from loyalty_Account__c where shopperid__c in:shopperids]){
            if(getla.email_address__c !=null){
                emailtoshopper.put(getla.shopperid__c,getla.email_address__c);  
            }
        }
        
        system.debug('emailtoshopper'+emailtoshopper);
        //Now update the log headers with correct serialised data
        for(Log_Header__c getHeader:[select id,serialised_data__c,Resend_Null_Email_Join_Notifications__c,
                                     Next_Callout_Date__c,Activate_Callout_Retry_Workflow__c,Callout_Status__c,
                                     success__c,shopper_id__c from log_Header__c where shopper_id__c in :emailtoshopper.keyset()]){
                Datetime nextCalloutDate = System.now().addMinutes(10);
                getHeader.Next_Callout_Date__c = nextCalloutDate;
                getHeader.Activate_Callout_Retry_Workflow__c = true;
                if(getHeader.serialised_data__c !=null){
                	boolean x =getHeader.serialised_data__c.contains('"RegistrationEmailAddressID":null');
                	system.debug('x'+x+emailtoshopper.get(getHeader.shopper_id__c));
                    getHeader.serialised_data__c = getHeader.serialised_data__c.replace('"RegistrationEmailAddressID":null','"RegistrationEmailAddressID":"'+emailtoshopper.get(getHeader.shopper_id__c)+'"');
                }                
                getHeader.Callout_Status__c = 'Retry';
                getHeader.success__c = false;
                getHeader.Resend_Null_Email_Join_Notifications__c = false;
                updateLH.add(getHeader);
        }
        
        //update log headers
        if (updateLH.size() >0)
            update updateLH;
    }
    
    public void finish(Database.BatchableContext bc){ 
    	if(!test.isrunningtest()){
    		MyJl_BatchReprocessNullEmailAddressJoins batchable = new MyJl_BatchReprocessNullEmailAddressJoins();
            System.scheduleBatch(batchable, 'Null Email Address Joins', 15,50);
    	}
    }
    
    

}