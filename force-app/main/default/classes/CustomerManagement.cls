//  001     26/02/15    NTJ     MJL-1581 - Make House No a Text field so that it does not display as 999,999 (should be 999999)
//  002     27/02/15    NTJ     MJL-1598 - Attach Contact Profile to the best Contact 
//  003     19/03/15    NTJ     As a result of Perf testing we saw what happened when there were 13 million duplicate contacts. Code restructured to cope.
//  004     20/03/15    NTJ     The original search by contact profile in findContacts is now only done if the IN clause has some entries in the set and split in two to
//                              avoid the OR clause.
//  005     20/03/15    NTJ     MJL-1692 - Add a matching key for <First><Last><Town><PostCode>
//  006     24/03/15    NTJ     Added Match method so we know how the contact was matched to contact profile
//  007     13/04/15    NTJ     Add a limit to stop bad data slowing down the contact profile lookups
//  008     20/05/15    NTJ     MJL-1957 - use LastModifiedDateMs__c as it has milliseconds
//  009     29/06/15    NTJ     MJL-2100 - Use Contact_Profile__c.AnonymousFlag2__c
//  010     26/08/15    MJP     CMP-509 - Exclude superseded contacts from matching queries
public with sharing class CustomerManagement {
    
    // MJL-1692 - Add some constants so we can record what pass matched Contact Profile to Contact
    public static final String MATCH_KEY1_PCD = 'Key1 PCD';
    public static final String MATCH_KEY1 = 'Key 1';
    public static final String MATCH_KEY2_PCD = 'Key2 PCD';
    public static final String MATCH_KEY2 = 'Key 2';
    public static final String MATCH_KEY3_PCD = 'Key3 PCD';
    public static final String MATCH_KEY3 = 'Key 3';
    public static final String MATCH_CP_KEY1 = 'Contact Profile Key 1';
    public static final String MATCH_CP_KEY2 = 'Contact Profile Key 2';
    public static final String MATCH_CP_KEY3 = 'Contact Profile Key 3';
    public static final String MATCH_CREATED = 'Created Contact';
    
    // We would expect a limited number of contact profiles hanging off a contact
    public static final Integer LOOKUP_CP_LIMIT = 10;
    
    public abstract class CustomerWrapper {
        public virtual Contact getContact() {
            //implement if necessary
            return null;
        }
        public virtual void setContact(final Contact contact) {
            //implement if necessary
        }
        
        public virtual void setMatchMethod(String method) {
            // implement if necessary
        }       
        
        public abstract String getEmailAddress();
        
        public abstract String getFirstName();
        
        public abstract String getLastName();
        
        public abstract String getAddressLine1();
        
        public abstract String getPostalCode();
        // MJL-1692
        public abstract String getCity();
        
        public abstract String getCustomerId();
        // MJL-1693
        public abstract Date getBirthDate();
        public abstract String getInitials();
        public abstract String getCountry();
        public abstract String getState();
        public abstract String getSalutation();
        public abstract String getPhone();
        
    }
    
    // MJL-1693 - keep the keys in a class
    public class KeyTriplet {
        public String key1;
        public String key2;
        public String key3;
        
        public KeyTriplet(String k1, String k2, String k3) {
            key1 = k1;
            key2 = k2;
            key3 = k3;
        }
    }   
    
    //
    // A class that helps find the Contacts that match Contact Profiles
    //
    public class FindContactResult {
        public Map<String, CustomerWrapper> wrapperByKey1;
        public Map<String, CustomerWrapper> wrapperByKey2;
        public Map<String, CustomerWrapper> wrapperByKey3;
        public Set<String> foundShopperIds;
        public Set<Id> contactsMatched;
        public Map<Id, Contact> contactMap1;
        public Map<Id, Contact> contactMap2;
        public List<Contact> contactList1;
        public List<Contact> contactList2;
        public Map<String, KeyTriplet> key12Triplet;
        public Map<String, KeyTriplet> key22Triplet;
        // MJL-1692
        public Map<String, KeyTriplet> key32Triplet;
        
        // constructor
        public FindContactResult() {
            wrapperByKey1 = new Map<String, CustomerWrapper>();
            wrapperByKey2 = new Map<String, CustomerWrapper>();
            wrapperByKey3 = new Map<String, CustomerWrapper>();
            foundShopperIds = new Set<String>();
            contactsMatched = new Set<Id>();
            key12Triplet = new Map<String, KeyTriplet>();
            key22Triplet = new Map<String, KeyTriplet>();
            // MJL-1692
            key32Triplet = new Map<String, KeyTriplet>();
        }
        
        // query to get contacts that have matching keys.
        // Note we need both a list and a map. The list has to be *ordered* by Id (Id's always are allocated upwards - could consider doing this by CreatedDate but Id is easier in unit tests!) 
        // and a mao as we need to access the Contacts by Id
        public void query(Integer pass) {
            // Need to limit the number of results as we have governor limits for heap space
            // We could have a maximum of 200 keys, so allow max 10 contacts each (2000 rows). There is a danger we miss some and we will end up creating yet another contact
            // This is really only a problem with data load of Contact Profiles (i.e. migration from Shopper)
            // Get Contacts with matching key 1
            // <<010>> CMP-509 add in ' and Is_Replaced__c = false' clause
            id orderrecordtype = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Ordering Customer').getRecordTypeId();
            if ( (pass == 1) || (pass == 3) ) {
                if (wrapperByKey1.keySet().size() > 0) {
                    contactList1 = [select Id, FirstName, LastName, Email, PCD_ID__c, Case_Matching_Key_1__c, Case_Matching_Key_2__c, PCD_Timestamp__c
                                    from Contact 
                                    where Case_Matching_Key_1__c in :wrapperByKey1.keySet() and Is_Replaced__c = false and recordtypeid=:orderrecordtype  ORDER BY Id DESC LIMIT 2000 ];                    
                } else {
                    contactList1 = new List<Contact>();
                }
                contactList2 = new List<Contact>();
            } else if ( (pass == 2) || (pass == 4) ) {
                // get contacts with matching key 2
                if (wrapperByKey2.keySet().size() > 0) {
                    contactList2 = [select Id, FirstName, LastName, MailingStreet, MailingPostalCode, PCD_ID__c, Case_Matching_Key_1__c, Case_Matching_Key_2__c, PCD_Timestamp__c
                                    from Contact 
                                    where Case_Matching_Key_2__c in :wrapperByKey2.keySet() and Is_Replaced__c = false and recordtypeid=:orderrecordtype ORDER BY Id DESC LIMIT 2000];                                      
                } else {
                    contactList2 = new List<Contact>();                 
                }
                contactList1 = new List<Contact>();
            } else if ( (pass == 5) || (pass == 6) ) {
                // MJL-1692
                // get contacts with matching key 2 equals our key 3
                // this is because PCD Data has key2 of <First><Last><Town><PostCode>
                if (wrapperByKey3.keySet().size() > 0) {
                    contactList2 = [select Id, FirstName, LastName, MailingStreet, MailingPostalCode, PCD_ID__c, Case_Matching_Key_1__c, Case_Matching_Key_2__c, PCD_Timestamp__c
                                    from Contact 
                                    where Case_Matching_Key_2__c in :wrapperByKey3.keySet() and Is_Replaced__c = false and recordtypeid=:orderrecordtype ORDER BY Id DESC LIMIT 2000];                                      
                } else {
                    contactList2 = new List<Contact>();                 
                }
                contactList1 = new List<Contact>();
            }
            
            // build maps
            contactMap1 = new Map<Id, Contact>();
            contactMap2 = new Map<Id, Contact>();
            for (Contact c:contactList1) {
                contactMap1.put(c.Id, c);
            }                                               
            for (Contact c:contactList2) {
                contactMap2.put(c.Id, c);                   
            }
        }
        
        // Nice to record how we matched
        private String getMatchMethod(Integer pass) {
            // default is that we did not match
            String matchMethod = '';
            if (pass == 1) {
                matchMethod = MATCH_KEY1_PCD;
            } else if (pass == 2) {
                matchMethod = MATCH_KEY2_PCD;               
            } else if (pass == 3) {
                matchMethod = MATCH_KEY1;               
            } else if (pass == 4) {
                matchMethod = MATCH_KEY2;               
            } else if (pass == 5) {
                matchMethod = MATCH_KEY3_PCD;               
            } else if (pass == 6) {
                matchMethod = MATCH_KEY3;               
            }
            return matchMethod;
        }
        
        public void pass(Integer passNo, Boolean findPcdId, List<Contact> contactList) {         
            for(Contact contact :contactList) {
                // if we have already matched 
                // if we have not already matched the contact 
                if (contactsMatched.contains(contact.Id) == false) {
                    // pcd_id__c is not blank then look at it, otherwise leave it for the next pass
                    if (String.isNotBlank(contact.PCD_ID__c) == findPcdId) {
                        
                        // get the keys
                        String key1 = contact.Case_Matching_Key_1__c;
                        String key2 = contact.Case_Matching_Key_2__c;
                        String key3 = key2;
                        
                        // MJL-1692 Depending on the pass - get the wrapper
                        // old code CustomerWrapper wrapper = ((passNo == 1) || (passNo == 3)) ? wrapperByKey1.get(key1) : wrapperByKey2.get(key2);
                        CustomerWrapper wrapper = ((passNo == 1) || (passNo == 3)) ? wrapperByKey1.get(key1) : null;
                        
                        // MJL-1692
                        // Choose the correct wrapper by key
                        if (wrapper == null) {
                            wrapper = ((passNo == 2) || (passNo == 4)) ? wrapperByKey2.get(key2) : wrapper;                         
                        }
                        if (wrapper == null) {
                            wrapper = ((passNo == 5) || (passNo == 6)) ? wrapperByKey3.get(key3) : wrapper;                         
                        }
                        
                        // if we have the wrapper still in the map (if it's not there then we have already processed it!)
                        if (null != wrapper) {
                            wrapper.setContact(contact);
                            foundShopperIds.add(wrapper.getCustomerId());
                            // prevent the contact from finding further candidates
                            if ((passNo == 1) || (passNo == 3)) {
                                removeKey1(key1);
                            } else if ((passNo == 2) || (passNo == 4)) {
                                removeKey2(key2);
                            } else if ((passNo == 5) || (passNo == 6)) {
                                removeKey3(key3);
                            }
                            contactsMatched.add(contact.Id);
                            wrapper.setMatchMethod(getMatchMethod(passNo));  
                        }
                    }               
                }  
            }
            
            // Remove matched contacts from the map2, note if the Id is not present then it is benign
            for (Id contactId:contactsMatched) {
                if ((passNo == 1) || (passNo == 3)) {
                    contactMap1.remove(contactId); 
                } else {
                    contactMap2.remove(contactId);                  
                }
            }
            
        }
        
        private void deleteKeyTriplet(KeyTriplet kt) {
            if (!String.isBlank(kt.key1)) {
                wrapperByKey1.remove(kt.key1);                  
            }
            if (!String.isBlank(kt.key2)) {
                wrapperByKey2.remove(kt.key2);                  
            }
            if (!String.isBlank(kt.key3)) {
                wrapperByKey3.remove(kt.key3);                  
            }
            
        } 
        
        public void removeKey1(String key1) {
            KeyTriplet matchingKt = (KeyTriplet) key12Triplet.get(key1);         
            deleteKeyTriplet(matchingKt);
        }
        
        public void removeKey2(String key2) {
            KeyTriplet matchingKt = (KeyTriplet) key22Triplet.get(key2);
            deleteKeyTriplet(matchingKt);
        }
        
        public void removeKey3(String key3) {
            KeyTriplet matchingKt = (KeyTriplet) key32Triplet.get(key3);
            deleteKeyTriplet(matchingKt);
        }
    }
    
    /**
* @return: Set<String> with Shopper ids of found countacts
* if contact is found then CustomerWrapper.contact will be populated
*
* Match against:
*
* Assuming that there are multiple Contacts in Connex that match the OCE Matching Rules then we need to select the best candidate.
* 1. Order the contacts by CreatedDate DESC and PCD_ID__c ASC
* 2. Loop through the contacts
*    a) Matches against matching key 1?
*    b) Matches against matching key 2?
*
* @return: 
*      - shopper Ids of matching contacts
*      - real found contacts will be set directly to the passed wrappers
*/
    public static Set<String> findContacts(List<CustomerWrapper> wrappers) {
        FindContactResult result = new FindContactResult();
        // Build maps based on wrappers
        for(CustomerWrapper wrapper : wrappers) {
            String firstName = wrapper.getFirstName();
            String lastName = wrapper.getLastName();
            String email = wrapper.getEmailAddress();
            String addressLine1 = wrapper.getAddressLine1();
            String postalCode = wrapper.getPostalCode();
            // MJL-1692
            String city = wrapper.getCity();
            
            String key1 = getKey1(firstName, lastName, email);
            String key2 = getKey2(firstName, lastName, addressLine1, postalCode);
            String key3 = getKey3(firstName, lastName, addressLine1, city, postalCode);
            
            KeyTriplet kt =  new KeyTriplet(key1, key2, key3);
            
            if (!String.isBlank(key1)) {
                result.wrapperByKey1.put(key1, wrapper);
                result.key12Triplet.put(key1, kt);
            }
            
            if (!String.isBlank(key2)) {
                result.wrapperByKey2.put(key2, wrapper);
                result.key22Triplet.put(key2, kt);
            }
            // MJL-1692
            
            if (!String.isBlank(key3)) {
                result.wrapperByKey3.put(key3, wrapper);
                result.key32Triplet.put(key3, kt);
            }           
        }
        
        // lookup the key1 in SOQL  
        result.query(1);     
        
        // Pass 1 is to look for Contacts that have non-null PCD_ID__c and have matching Key1   
        result.pass(1, true, result.contactList1);
        
        // lookup the key2 in SOQL  
        result.query(2);     
        
        // Pass 2 is to look for Contacts that have non-null PCD_ID__c and have matching Key2   
        result.pass(2, true, result.contactList2);
        
        // lookup the key3 in SOQL against key2  
        result.query(5);     
        
        // Pass 5 is to look for Contacts that have non-null PCD_ID__c and have matching Key2 that matches the lookup key3
        result.pass(5, true, result.contactList2);      
        
        // lookup the keys in SOQL - retired - results have not changed since previous SELECT in pass 1  
        result.query(3);     
        
        // Pass 3 is to look for Contacts that have null PCD_ID__c and have matching Key1   
        result.pass(3, false, result.contactList1);
        
        // lookup the keys in SOQL - retired - results have not changed since previous SELECT in pass 2 
        result.query(4);     
        
        // Pass 4 is to look for Contacts that have null PCD_ID__c and have matching Key2   
        result.pass(4, false, result.contactList2);
        
        // lookup the keys in SOQL - retired - results have not changed since previous SELECT in pass 5  
        result.query(6);     
        
        // Pass 1 is to look for Contacts that have null PCD_ID__c and have matching Key2 that matches the lookup key3   
        result.pass(6, false, result.contactList2);
        
        
        // for all contacts still not identified try to search via Contact_Profile__c        
        // This also has to work within governor limits. We can only get max of 50K rows and we need to work with less than that
        // It could be that the Contact itself has different matching keys, imagine the First Name on Contact Profile is Kate but the contact says Catherine
        // This may have happened because the Call Centre updated the Contact Record.
        if (result.wrapperByKey1.keySet().size() > 0) {
            for(Contact_Profile__c profile : [select Id, FirstName__c, LastName__c, Email__c, Mailing_Street__c, MailingPostCode__c,
                                              Case_Matching_Key_1__c, Case_Matching_Key_2__c,
                                              Contact__r.Id, Contact__r.FirstName, Contact__r.LastName, Contact__r.MailingStreet, Contact__r.MailingPostalCode
                                              from Contact_Profile__c 
                                              where Case_Matching_Key_1__c in :result.wrapperByKey1.keySet() ORDER BY CreatedDate DESC LIMIT 50000]) {
                                                  String key1 = profile.Case_Matching_Key_1__c;
                                                  String key2 = profile.Case_Matching_Key_2__c;
                                                  CustomerWrapper wrapper = result.wrapperByKey1.get(key1);
                                                  if (null != wrapper) {
                                                      wrapper.setContact(profile.Contact__r);
                                                      result.foundShopperIds.add(wrapper.getCustomerId());
                                                      result.removeKey1(key1);
                                                      wrapper.setMatchMethod(MATCH_CP_KEY1);
                                                  }                                   
                                              }           
        }
        
        if (result.wrapperByKey2.keySet().size() > 0) {
            Integer limit2 = LOOKUP_CP_LIMIT * result.wrapperByKey2.keySet().size();
            
            for(Contact_Profile__c profile : [select Id, FirstName__c, LastName__c, Email__c, Mailing_Street__c, MailingPostCode__c,
                                              Case_Matching_Key_1__c, Case_Matching_Key_2__c,
                                              Contact__r.Id, Contact__r.FirstName, Contact__r.LastName, Contact__r.MailingStreet, Contact__r.MailingPostalCode
                                              from Contact_Profile__c 
                                              where Case_Matching_Key_2__c in :result.wrapperByKey2.keySet() ORDER BY CreatedDate DESC LIMIT :limit2]) {
                                                  String key2 = profile.Case_Matching_Key_2__c;
                                                  CustomerWrapper wrapper = result.wrapperByKey2.get(key2);
                                                  if (null != wrapper) {
                                                      wrapper.setContact(profile.Contact__r);
                                                      result.foundShopperIds.add(wrapper.getCustomerId());
                                                      result.removeKey2(key2);
                                                      wrapper.setMatchMethod(MATCH_CP_KEY2);
                                                  }                                   
                                              }           
        }
        
        if (result.wrapperByKey3.keySet().size() > 0) {
            Integer limit3 = LOOKUP_CP_LIMIT * result.wrapperByKey3.keySet().size();
            for(Contact_Profile__c profile : [select Id, FirstName__c, LastName__c, Email__c, Mailing_Street__c, MailingPostCode__c,
                                              Case_Matching_Key_1__c, Case_Matching_Key_2__c,
                                              Contact__r.Id, Contact__r.FirstName, Contact__r.LastName, Contact__r.MailingStreet, Contact__r.MailingPostalCode
                                              from Contact_Profile__c 
                                              where Case_Matching_Key_2__c in :result.wrapperByKey3.keySet() ORDER BY CreatedDate DESC LIMIT :limit3]) {
                                                  String key3 = profile.Case_Matching_Key_2__c;
                                                  CustomerWrapper wrapper = result.wrapperByKey3.get(key3);
                                                  if (null != wrapper) {
                                                      wrapper.setContact(profile.Contact__r);
                                                      result.foundShopperIds.add(wrapper.getCustomerId());
                                                      result.removeKey3(key3);
                                                      wrapper.setMatchMethod(MATCH_CP_KEY3);                  
                                                  }                                   
                                              }           
        }        
        
        return result.foundShopperIds;
    }
    
    
    /**
* load Contact_Profile__c(s) and their associated Loyalty_Account__c records
* @deprecated
*/
    /*
public static List<SFDCMyJLCustomerTypes.Customer> getCustomers(final Set<Id> contactProfileIds) {
final List<SFDCMyJLCustomerTypes.Customer> customers = new List<SFDCMyJLCustomerTypes.Customer>();
for(Contact_Profile__c profile : loadContactProfiles(contactProfileIds)) {
customers.add(MyJL_MappingUtil.populateCustomer(profile));
}
return customers;
}
*/
    
    public static List<contact>  loadContactProfiles(final Set<Id> contactProfileIds) {
        final List<contact> customers = new List<contact>();
        for(contact profile : [select Id, Case_Matching_Key_1__c, Case_Matching_Key_2__c,
                               name, Shopper_ID__c,lastmodifieddatems__c,LastModifiedDate,  birthdate, salutation, title, firstname,
                               lastname, email, mobilephone, 
                               mailingstreet,  mailingcity, mailingstate, mailingcountry,mailingcountrycode, Mailing_House_No_Text__c , 
                               mailingpostalcode, 
                               otherstreet, othercity, otherstate,
                               othercountry, otherpostalcode, 
                               MyJL_ESB_Message_ID__c, AnonymousFlag2__c,
                               (select Name, Email_Address__c, Scheme_Joined_Date_Time__c, Activation_Date_Time__c, My_Partnership_Activation_Date_Time__c, Deactivation_Date_Time__c,
                                Welcome_Email_Sent_Flag__c, Registration_Workstation_ID__c, Registration_Business_Unit_ID__c, Channel_ID__c,
                                LastModifiedById, IsActive__c, LastModifiedDate, LastModifiedDateMs__c, Voucher_Preference__c
                                from MyJL_Accounts__r)
                               from contact where Id in :contactProfileIds]) {
                                   customers.add(profile);
                               }
        return customers;
    }
    
    public static String getKey1(final String firstName, final String lastName, final String email) {
        if (!isBlank(firstName) && !isBlank(lastName) && !isBlank(email)) {
            return sanitiseKey(firstName + lastName + email);
        }
        return null;
    }
    
    public static String getKey2(final String firstName, final String lastName, final String addressLine1, final String postalCode) {
        if (!isBlank(firstName) && !isBlank(lastName) && !isBlank(addressLine1) && !isBlank(postalCode)) {
            return sanitiseKey(firstName + lastName + addressLine1 + postalCode);
        }
        return null;
    }
    
    // MJL-1692
    public static String getKey3(final String firstName, final String lastName, final String addressLine1, final String city, final String postalCode) {
        if (!isBlank(firstName) && !isBlank(lastName) && !isBlank(addressLine1) && !isBlank(city) && !isBlank(postalCode)) {
            return sanitiseKey(firstName + lastName + addressLine1 + city + postalCode);
        }
        return null;
    }
    
    /**
* strip white spaces and convert text to lower case
* note: here we do not check for nulls because null keys are invalid
*/
    @TestVisible
    private static String sanitiseKey(final String key) {
        return key.toLowerCase().deleteWhitespace().replaceAll('(\r\n|\n|\t|,)', '');
    }
    
    public static Boolean isNull(Object obj) {
        return MyJL_Util.isNull(obj);
    }
    
    public static Boolean isBlank(String val) {
        return String.isBlank(val) || isNull(val);
    }
    
    
}