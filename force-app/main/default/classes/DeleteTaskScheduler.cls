/******************************************************************************

* @author		Stuart Barber
* @date			06/03/2017
* @description	Class that enables the scheduling of DeleteTaskBatch.apxc
* @Notes		This class should be scheduled as a manual step (daily)

******************************************************************************/

public class DeleteTaskScheduler implements Schedulable {
    
    public void execute(SchedulableContext sc){
        
        List<Delete_Task_Batch_Settings__c> customSettings = Delete_Task_Batch_Settings__c.getAll().values();
        
        for(Delete_Task_Batch_Settings__c customSetting: customSettings){
        
            DeleteTaskBatch taskBatch = new DeleteTaskBatch();
            taskBatch.taskRecordType = customSetting.Name;
            taskBatch.daysToRetain = Integer.valueOf(customSetting.Retention_Period__c);
            database.executeBatch(taskBatch);
    	}        
    }   
}