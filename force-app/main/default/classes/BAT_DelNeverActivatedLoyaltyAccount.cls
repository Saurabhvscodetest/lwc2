/* Description  : Delete Never activated Loyalty Account records
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   25/1/2020        Ramesh Vasudevan                Created                 COPT-5232
*
*/
global class BAT_DelNeverActivatedLoyaltyAccount implements Database.Batchable<sObject>,Database.Stateful{
    global Map<Id,String> ErrorMap = new Map<Id,String>();
    global integer j=0;
    global datetime startdatetime = datetime.now();
    global Database.QueryLocator start(Database.BatchableContext BC){
       
       GDPR_Record_Limit__c gDPRLimit = GDPR_Record_Limit__c.getValues('BAT_DelNeverActivatedLoyaltyAccount');
       System.debug('gDPRLimit'+gDPRLimit);
        if(gDPRLimit != NULL && gDPRLimit.Record_Limit__c != NULL){
           String recordLimit = gDPRLimit.Record_Limit__c;
           Integer qLimit = Integer.valueOf(recordLimit);
        
            return Database.getQueryLocator([Select id from Loyalty_Account__c where
                                             (IsActive__c = false and Activation_Date_Time__c = NULL) LIMIT :qLimit ]);
                                             
        } else {
            
            return Database.getQueryLocator([Select id from Loyalty_Account__c where
                                             (IsActive__c = false and Activation_Date_Time__c = NULL) LIMIT 10 ]);
        }
        
    }
    global void execute(Database.BatchableContext BC, list<Loyalty_Account__c> scope){
        List<id> recordstoDelete = new List<id>();
        if(scope.size()>0){
            try{
                for(Loyalty_Account__c recordScope : scope){
                    recordstoDelete.add(recordScope.id);
                }
                list<Database.deleteResult> srList = Database.delete(recordstoDelete, false);
                for(Integer i = 0; i < srList.size(); i++) {
                    if (srList[i].isSuccess()) {
                        j++;
                    }else {
                        for(Database.Error err : srList[i].getErrors()) {
                            ErrorMap.put(srList.get(i).id,err.getMessage());
                        }
                    }
                }
                Database.emptyRecycleBin(scope);
            }Catch(exception e){
                EmailNotifier.sendNotificationEmail('Exception from BAT_DelNeverActivatedLoyaltyAccount', e.getMessage());
            } 
        }
        
    }
    global void finish(Database.BatchableContext BC){
        String textBody ='';
        set<Id> allIds = new set<Id>();
        textBody+= j +' records deleted in object Loyalty Account'+'\n';
        textBody+= ' started on: '+string.valueof(startdatetime) +'\n';
        textBody+= ' finished at: '+string.valueof(datetime.now())+'\n';
        if (!ErrorMap.isEmpty()){
            for (Id recordids : ErrorMap.KeySet())
            {
                allIds.add(recordids);
            } 
            textBody+= 'Error log: '+allIds+'\n';
        }
        EmailNotifier.sendNotificationEmail('Connex NeverActivated Deletion Log', textBody);        
    }    
}