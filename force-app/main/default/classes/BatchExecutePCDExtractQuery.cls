/******************************************************************************
* @author       Matt Povey
* @date         18 Mar 2016
* @description  Noddy class that just runs a query for the PCD team each night.
*				This should run at 00:05 every night.  To schedule this, execute the
*				following from Developer Console:
*				System.Schedule('PCD Extract Query','0 5 0 ? * *', new BatchExecutePCDExtractQuery());
*
* EDIT RECORD
*
* LOG     DATE         Author	JIRA		COMMENT      
* 001     18 Mar 2016  MP		CMP-1053	Original code
* 002     11 Apr 2016  MP		CMP-nnnn	Change SystemModStamp to LastModifiedDate
*
*******************************************************************************
*/
global class BatchExecutePCDExtractQuery implements Database.Batchable<SObject>, Schedulable {
	private static final Config_Settings__c EMAIL_NOTIFICATION_DEFAULT_ADDRESS_SETTING = Config_Settings__c.getInstance('EMAIL_NOTIFICATION_DEFAULT_ADDRESS');
	private static final String EMAIL_NOTIFICATION_DEFAULT_ADDRESS = EMAIL_NOTIFICATION_DEFAULT_ADDRESS_SETTING != null ? EMAIL_NOTIFICATION_DEFAULT_ADDRESS_SETTING.Text_Value__c : 'matthew.povey@johnlewis.co.uk';
	private static final Config_Settings__c EMAIL_SETTINGS = Config_Settings__c.getInstance('PCD_QUERY_RUNNER_EMAIL_LIST');
	private static final List<String> EMAIL_USERS = EMAIL_SETTINGS != null ? EMAIL_SETTINGS.Text_Value__c.split(';') : new List<String> {EMAIL_NOTIFICATION_DEFAULT_ADDRESS};
	private static final Config_Settings__c DAYS_SETTINGS = Config_Settings__c.getInstance('PCD_QUERY_RUNNER_NUMBER_OF_DAYS');
	private static final Integer NUMBER_OF_DAYS = DAYS_SETTINGS != null ? Integer.valueOf(DAYS_SETTINGS.Number_Value__c) : 1;
	private static final Config_Settings__c EXCLUDE_USER_SETTINGS = Config_Settings__c.getInstance('PCD_QUERY_RUNNER_EXCLUDE_USERS');
	private static final List<Id> DEFAULT_EXCLUDE_USER_IDS = new List<Id>{'005b0000001DohCAAS', '005b0000002TtUSAA0'};
	private static final List<Id> EXCLUDE_USERS = EXCLUDE_USER_SETTINGS != null ? EXCLUDE_USER_SETTINGS.Text_Value__c.split(';') : DEFAULT_EXCLUDE_USER_IDS;

    /**
     * execute - Schedule the batch job
	 * @params:	Database.SchedulableContext sc	Schedulable Context from system.
     */
	global void execute(SchedulableContext sc){
       	BatchExecutePCDExtractQuery pcdQueryJob = new BatchExecutePCDExtractQuery();
       	Database.executeBatch(pcdQueryJob);
	}

    /**
     * start - run the PCD query
	 * @params:	Database.BatchableContext bc	Batchable Context from system.
     */
	global Database.QueryLocator start(Database.BatchableContext bc){
        DateTime currentDay = system.now();
        DateTime previousDay = currentDay.addDays(NUMBER_OF_DAYS * -1);
        DateTime startDateTime = DateTime.newInstance(previousDay.year(), previousDay.month(), previousDay.day());
        DateTime endDateTime = DateTime.newInstance(currentDay.year(), currentDay.month(), currentDay.day());
        // return Database.getQueryLocator([SELECT Id FROM Contact WHERE SystemModStamp > :startDateTime AND SystemModStamp <= :endDateTime AND LastModifiedById NOT IN :EXCLUDE_USERS]);
        return Database.getQueryLocator([SELECT Id FROM Contact WHERE LastModifiedDate > :startDateTime AND LastModifiedDate <= :endDateTime AND LastModifiedById NOT IN :EXCLUDE_USERS]);
	}

    /**
     * Execute - does not need to do anything.
     * @params: Database.BatchableContext bc    Batchable Context from system.
     *          List<Contact> conList           Batch of Contact records
     */
    global void execute(Database.BatchableContext bc, List<Contact> conList){
    	system.debug('BatchPCDQueryRunner EXECUTING');
	}   

	/**
	 * Finish - send notification email.
	 * @params: Database.BatchableContext bc    Batchable Context from system.
	 */
	global void finish(Database.BatchableContext bc){
		EmailNotifier.sendNotificationEmail(EMAIL_USERS, 'BatchPCDQueryRunner COMPLETED', 'BatchPCDQueryRunner has completed.');
	}
}