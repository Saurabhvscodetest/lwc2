@isTest
public with sharing class CaseRoutingTestDataFactory {

    public static Case_Routing_Categorisation__c createCaseRoutingCategorisation(String contactReason, String reasonDetail, String assignTo, String recordType) {
		String uniqueId = contactReason + '_' + reasonDetail;
        
        Case_Routing_Categorisation__c crc = new Case_Routing_Categorisation__c();
        crc.Unique_ID__c = uniqueId;
        crc.Contact_Reason__c = contactReason;
        crc.Reason_Detail__c = reasonDetail;
        crc.Assign_To__c = assignTo; 
        crc.Record_Type__c = recordType;

        return crc;
    }
}