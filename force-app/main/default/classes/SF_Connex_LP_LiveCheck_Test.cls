@isTest
public class SF_Connex_LP_LiveCheck_Test {
    
    private static final String fromTimeStamp = '1561981035000';
    private static final String toTimeStamp = '1562749404000';


    private static final String customer_Email = 'vijayreddyambati@gmail.com';
    private static final String customer_PostCode = 'e78eu';
    private static final String customer_Name = 'vijay ambati';
    private static final String customer_address = '62 Strone Road';
    
    static testMethod void serviceException() {
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/SF_ConnexToLPCheck/';
        req.addParameter('customeremail', customer_Email);
        req.addParameter('customerpostcode', customer_PostCode);
        req.addParameter('customername', customer_Name);
        req.addParameter('customeraddress', customer_address);
        req.httpMethod = 'GET';
        
        RestContext.request = req;
        RestContext.response = res;
                
        Test.startTest();
        try {
            SF_Connex_LP_LiveCheck.SF_ConnexToLivePersonWrapper crw = SF_Connex_LP_LiveCheck.fetchData();
        }
        catch(Exception e) {}
        Test.stopTest();        
    }


  static testMethod void testGet() {
     
        Contact testContact = new Contact(LastName = 'Test111',
                                          FirstName = 'John',
                                          Salutation = 'Mr',
                                          Email = 'test12341234@madeupemail1234test.com',
                                          MailingStreet = 'Mailing Street',
                                         MailingCity = 'Mailing City',
                                         MailingState = 'Kent',
                                         MailingPostalCode = 'Mailing Postal Code',
                                         MailingCountry = 'United Kingdom');
      
            Insert testContact;
      
      Contact con = [ SELECT name,email,MailingPostalCode,MailingStreet from contact where email =: testContact.email order by createddate desc limit 1];
          
      RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();             
        req.requestURI = '/services/apexrest/SF_ConnexToLPCheck/' + con.email + con.MailingPostalCode + con.name + con.MailingStreet;
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response= res;
        Test.startTest();
        try {
            SF_Connex_LP_LiveCheck.SF_ConnexToLivePersonWrapper crw = SF_Connex_LP_LiveCheck.fetchData();
        }
        catch(Exception e) {}
        Test.stopTest();  
    }

    
    static testMethod void fetchDataMethod() {
        
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
          req.requestURI = '/SF_ConnexToLPCheck/';
        req.addParameter('customeremail', customer_Email);
        req.addParameter('customerpostcode', customer_PostCode);
        req.addParameter('customername', customer_Name);
        req.addParameter('customeraddress', customer_address);
        req.httpMethod = 'GET';
        
        RestContext.request = req;
        RestContext.response = res;
                
        Test.startTest();
            SF_Connex_LP_LiveCheck.SF_ConnexToLivePersonWrapper crw = SF_Connex_LP_LiveCheck.fetchData();
        Test.stopTest();        
    }
}