/*
* File Name     : ConstantValues
* Description   : Class which stores all the constant variables , useful across Salesforce Application
* Client        : JLP
* =========================================================================== 
* Ver Date          Author                               Modification 
* --- ---- ------ ------------------------------------ ---------------------- 
* 1.0  16-Apr-2019   Ragesh G                                Created 
*/ 

public class ConstantValues {
    
    // Yes value is stored in the variable which will be used in the BatchProcessUpdateCTellingDateContact - Clienteling_Permission__c
    public static String CLIENT_TELLING_PERMISSION                       = 'YES';
    public static String CLIENT_TELLING_CONTACT_UPDATE_BATCH_NAME        = 'BAT_UpdateCTellingDateOnContact';
    public static String CLIENT_TELLING_UPDATE_BATCH_NAME                = 'BAT_UpdateClienTellingData';
    public static final DateTime FIVE_YEARS_AGO                          = System.now().addYears( Integer.valueOf ( Label.CT_CONTACT_UPDATE_BATCH_YEAR_LIMIT) );
    public static String CT_CONTACT_UPDATE_BATCH_EXECUTION_FAILED        = Label.CT_CONTACT_UPDATE_BATCH_EXECUTION_FAILED;
    public static String CT_UPDATE_BATCH_EXECUTION_FAILED                = Label.CT_UPDATE_BATCH_EXECUTION_FAILED;
    public static String CT_CONTACT_UPDATE_BATCH_EXECUTION_SUCCESS       = Label.CT_CONTACT_UPDATE_BATCH_EXECUTION_SUCCESS+'\n';
    public static String CT_UPDATE_BATCH_EXECUTION_SUCCESS               = Label.CT_UPDATE_BATCH_EXECUTION_SUCCESS+'\n';
    public static String CT_GDPR_DELETE_LOG_SUBJECT                      = Label.CT_GDPR_DELETE_LOG_SUBJECT;
    
}