/**
 *	@author: Andrey Gavrikov (westbrook)
 *	@date: 2014-12-02 12:31:19 
 *	@description:
 *	    MJL-1350
 *	    Delete log headers and details after N days
 *	
 *		to execute manually - use developer console and run:
 *		Database.executeBatch(new HouseKeeperBatch(new LogCleanerBatch(), true));
 *
 *	Version History :   
 *	2014-12-02 - MJL-1350 - AG	initial version
 *	2019-12-20 - VC - 	  -     Added custom setting limit value to the number of records to process
 *                              Use a custom setting to obtain the batch size  
 *		
 */
public with sharing class LogCleanerBatch implements HouseKeeperBatch.Processor {
	public String getName() {
		return 'LogCleanerBatch';
	}
		
	public Database.QueryLocator start(Database.BatchableContext bc) {
        final Double tempMaxRecords = CustomSettings.getLogHeaderConfig().getNumber('Housekeeping_Records_Per_Run__c');
        final Integer maxRecords = tempMaxRecords == null?0 : tempMaxRecords.intValue();      
		final Double headerDeleteAfterNDays = CustomSettings.getLogHeaderConfig().getNumber('Delete_after_N_Days__c');
		Datetime cutOffDate = Datetime.newInstanceGmt(1970, 01, 01);//by default do not delete anything
		
        if(null != headerDeleteAfterNDays ) {
			cutOffDate = System.now().addDays(-1 * headerDeleteAfterNDays.intValue());
		}

        return Database.getQueryLocator( [select Id from Log_Header__c where CreatedDate < :cutOffDate limit :maxRecords]);
	}

	public void execute(Database.BatchableContext bc, List<SObject> scope) {
		Database.delete(scope);
	}

	public void finish(Database.BatchableContext bc){ }

	public Integer getBatchSize() {
		Double tempBatchSize = CustomSettings.getLogHeaderConfig().getNumber('Housekeeping_Batch_Size__c');
		final Integer batchSize = tempBatchSize == null?0 : tempBatchSize.intValue();
		return batchSize;
	}
}