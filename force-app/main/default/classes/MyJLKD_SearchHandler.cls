/**
 *  @author: Andrey Gavrikov (westbrook)
 *  @date: 2014-08-11 10:31:57 
 *  @description:
 *      This custom Apex web service will allow external endpoints to search Retail
 *      Transactions in Salesforce based on some search criteria.  
 *      Direction: John Lewis Enterprise Service -> SFDC 
 *      Interface Data: Retail Transaction Header and Detail 
 *      Invocation Type: Synchronous
 *
 *  
 *  Version History :   
 *  2014-08-11 - MJL-729 - AG
 *  initial version 
 *  
 *  001     15/04/15    NTJ     MJL-1790 - Do not do a SELECT WHERE x IN set, if we have an empty set
 *  002     20/04/15    ntj     Remove Debug.log
 *  003     02/06/15    NTJ     Change the SELECT on Lyalty_Account__c so we only get the rows that we are interested in.
 */
public with sharing class MyJLKD_SearchHandler {
    private static Integer MAX_ITEMS_LIMIT = 1000;//Max number of items we can return as part of Search return result
    private static final String LOYALTY_ACCOUNT_SOQL   ='select Name, (select Id from MyJL_Cards__r) from Loyalty_Account__c ';
    private static final String CONTENT_TYPE   = 'text/xml';

    private static Id logHeaderId;
    public static SFDCKitchenDrawerTypes.ActionResponse process(final SFDCKitchenDrawerTypes.RequestHeader requestHeader, 
                                                                final SFDCKitchenDrawerTypes.TransactionSearchCriteria searchCriteria) {
        startLog(requestHeader, searchCriteria);
        
        SFDCKitchenDrawerTypes.ActionResponse response = new SFDCKitchenDrawerTypes.ActionResponse();
        response.messageId = requestHeader.messageId;
        response.success = false;
        try {
            if (null != requestHeader && !MyJL_Util.isBlank(requestHeader.MessageId)) {
                if (MyJL_Util.isMessageIdUnique(requestHeader.MessageId)) {
                    if (validateSearchCriteria(searchCriteria)) {
                        loadTransactions(response, searchCriteria, getCardIds(searchCriteria));
                        response.success = true;
                    } else {
                        MyJL_Util.populateKDErrorDetailsHeader(MyJL_Const.ERRORCODE.INCORRECT_SEARCH_REQUEST, response);
                    }
                } else {
                    //duplicate message Id
                    MyJL_Util.populateKDErrorDetailsHeader(MyJL_Const.ERRORCODE.MESSAGEID_DUPLICATE, response);
                }
            } else {
                //missing message id
                MyJL_Util.populateKDErrorDetailsHeader(MyJL_Const.ERRORCODE.MESSAGEID_NOT_PROVIDED, response);
            }
            updateLog(response, null);

        } catch (Exception e) {
            //responseHeader.ResultStatus.Success = false;
            //we do not have the code for this situation
            //throw new TerminateProcessException(MyJL_Const.ERRORCODE.UNHANDLED_EXCEPTION, logHeaderId, responseHeader, e);
            response = MyJL_Util.populateKDErrorDetailsHeader(MyJL_Const.ERRORCODE.UNHANDLED_EXCEPTION, response);
            updateLog(response, e.getStackTraceString());
            System.assertEquals(false, Test.isRunningTest(), 'exception thrown: ' + e + '; ' + e.getStackTraceString());
            System.debug('agX e=' + e);
            System.debug('agX e.stack=' + e.getStackTraceString());
        }

        return response;
    }

    private static Id startLog(final SFDCKitchenDrawerTypes.RequestHeader requestHeader, final SFDCKitchenDrawerTypes.TransactionSearchCriteria searchCriteria) {
        try {
            logHeaderId = LogUtil.startKDLogHeader(LogUtil.SERVICE.WS_KD_SEARCH_TRANSACTIONS, requestHeader, searchCriteria, null);
        } catch (Exception e) {
            //if logging step fails then just ignore it
        }
        return logHeaderId;
        
    }
    
    private static void updateLog(SFDCKitchenDrawerTypes.ActionResponse responseHeader, final String errorStackTrace) {
        try {
            LogUtil.updateKDLogHeader(logHeaderId, responseHeader, errorStackTrace);
        } catch (Exception e) {
            //if logging step fails then just ignore it
        }
        
    }

    /**
     * Required fields in Search call:
     * "Customer Id" OR "Email" OR "membershipNumber" OR "cardNumber"
     */
    @TestVisible private static Boolean TEST_DISABLE_VALIDATION = false;//used in unit tests to disable unnecessary validation
    private static Boolean validateSearchCriteria(final SFDCKitchenDrawerTypes.TransactionSearchCriteria searchCriteria) {
        return TEST_DISABLE_VALIDATION || !MyJL_Util.isBlank(searchCriteria.customerId) || !MyJL_Util.isBlank(searchCriteria.email) 
                || !MyJL_Util.isBlank(searchCriteria.membershipNumber) || !MyJL_Util.isBlank(searchCriteria.cardNumber);
    }

    public static String getShortXmlName() {
        return MyJL_Util.getShortXmlName();
    }

    public static String getFullXmlName() {
        return MyJL_Util.getFullXmlName();
    }

    @TestVisible
    private static Set<Id> getCardIds(final SFDCKitchenDrawerTypes.TransactionSearchCriteria searchCriteria) {
        
        final String cardNumber = searchCriteria.cardNumber;
        final String email = searchCriteria.email;
        final String customerId = searchCriteria.customerId;
        final String membershipNumber = searchCriteria.membershipNumber;
        
        String soqlWhere = '';
        
        String soqlStr = '';
        if (MyJL_Util.isBlank(cardNumber)) {
            soqlStr = LOYALTY_ACCOUNT_SOQL ;
        } else {
            soqlStr = LOYALTY_ACCOUNT_SOQL;

            soqlWhere = 'Customer_Loyalty_Account_Card_ID__c = :cardNumber ';
        }
        
        if (!MyJL_Util.isBlank(email)) {
            soqlWhere += andX(soqlWhere) + 'Email_Address__c = :email';
        }
        if (!MyJL_Util.isBlank(customerId)) {
            soqlWhere += andX(soqlWhere) + 'Contact__r.Shopper_ID__c = :customerId';
        }
        if (!MyJL_Util.isBlank(membershipNumber)) {
            soqlWhere += andX(soqlWhere) + 'Name = :membershipNumber';
        }
        if (!String.isBlank(soqlWhere)) {
            soqlWhere = ' where ' + soqlWhere;
        }

        soqlStr = soqlStr + ' ' + soqlWhere;
        system.debug('agX getCardIds.soql = ' + soqlStr);
        
        final Set<Id> cardIds = new Set<Id>();
        // MJL-1790 TODO
        
        for(Loyalty_Account__c acc : Database.query(soqlStr)) {
            if (null != acc.MyJL_Cards__r) {
                for(Loyalty_Card__c card : acc.MyJL_Cards__r) {
                    cardIds.add(card.Id);
                }
            }
        }
        
        //system.debug('agX cardIds=' + cardIds);
        return cardIds;
    }
    
    @TestVisible
    private static SFDCKitchenDrawerTypes.ActionResponse loadTransactions(final SFDCKitchenDrawerTypes.ActionResponse response, 
                                                                            final SFDCKitchenDrawerTypes.TransactionSearchCriteria searchCriteria, 
                                                                            final Set<Id> cardIds) {

        final String channel = searchCriteria.channel;
        final Boolean isFullXml = null == searchCriteria.isFullXml? false : searchCriteria.isFullXml;

        final String branchNumber = searchCriteria.branchNumber;
        final String transactionId = searchCriteria.transactionId;
        
        final Datetime fromDate = searchCriteria.transactionsStartDate;
        final Datetime toDate = searchCriteria.transactionsEndDate;

        final SFDCKitchenDrawerTypes.SortDirections direction = searchCriteria.sortDirection;
        final SFDCKitchenDrawerTypes.SortByFields orderByFName = searchCriteria.sortByField;

        String soqlStr = 'select Id, Receipt_Barcode_Number__c, Channel__c, Card_Number__c, Card_Number__r.Name, Store_Code__c, Transaction_Amount__c, Transaction_Date_Time__c';
        soqlStr += ', Voided_Transaction_Response__c, Voiding_Transaction_Id__c, Transaction_Type__c';
        soqlStr += ', (select Id, Associated_Transaction__r.Receipt_Barcode_Number__c, Transaction__r.Receipt_Barcode_Number__c, Transaction__r.IsValid__c from Associated_Transactios1__r)';
        soqlStr += '  from Transaction_Header__c';
        
        String soqlWhere = ' where Card_Number__c in :cardIds and IsValid__c = true ';

        if (!MyJL_Util.isBlank(channel)) {
            soqlWhere += andX(soqlWhere) + 'Channel__c = :channel';
        }
        if (!MyJL_Util.isBlank(branchNumber)) {
            soqlWhere += andX(soqlWhere) + 'Store_Code__c = :branchNumber';
        }
        if (!MyJL_Util.isBlank(transactionId)) {
            soqlWhere += andX(soqlWhere) + 'Receipt_Barcode_Number__c = :transactionId';
        }

        if (null != fromDate) {
            soqlWhere += andX(soqlWhere) + 'Transaction_Date_Time__c >= :fromDate';
        }
        if (null != toDate) {
            soqlWhere += andX(soqlWhere) + 'Transaction_Date_Time__c <= :toDate';
        }
        
        String soqlOrderBy = '';
        String soqlOffset = '';
        //String soqlLimit = '';

        String sortByField = getSortByField(orderByFName);
        if (!String.isBlank(sortByField)) {
            soqlOrderBy = 'ORDER BY ' + sortByField;
            if (SFDCKitchenDrawerTypes.SortDirections.ASCENDING == direction) {
                soqlOrderBy += ' ASC ';
            }
        }
        if (null != searchCriteria.recordSetStartNumber) {
            soqlOffset = ' OFFSET ' + searchCriteria.recordSetStartNumber;
        }
        /*
        //can not use LIMIT SOQL clause because need to know how many records total could be returned
        if (null != searchCriteria.maxItems) {
            soqlLimit = ' LIMIT ' + searchCriteria.maxItems;
        }
        */
        //assume that we never need to return more than 10K records in a single request
        Integer maxItems = null != searchCriteria.maxItems? searchCriteria.maxItems : MAX_ITEMS_LIMIT;
        maxItems = Math.min(maxItems, MAX_ITEMS_LIMIT);

        //soqlStr = soqlStr + ' ' + soqlWhere + ' ' + soqlOrderBy + ' ' + soqlLimit + ' ' + soqlOffset;
        soqlStr = soqlStr + ' ' + soqlWhere + ' ' + soqlOrderBy + ' ' + soqlOffset;
        system.debug('agX loadTransactions.soql = ' + soqlStr);
        
        final Set<Id> transactionIds = new Set<Id>();
        // MJL-1790 TODO
        final List<Transaction_Header__c> transactionsQueried = Database.query(soqlStr);
        final List<Transaction_Header__c> transactions = new List<Transaction_Header__c>();
        for(Transaction_Header__c trans : transactionsQueried) {
            if (transactions.size() >= maxItems) {
                break;
            }
            transactionIds.add(trans.Id);
            transactions.add(trans);
        }
        //system.debug('agX transactionIds=' + transactionIds);
        //load XML bodies
        final String xmlDocumentName = isFullXml ? getFullXmlName(): getShortXmlName();
        final Map<Id, String> xmlByTransactionId = new Map<Id, String>();
        // MJL-1790
        if (!transactionIds.isEmpty()) {
            for(Attachment att : [select Id, Name, Body, ParentId from Attachment 
                                                        where Name = :xmlDocumentName 
                                                            and ParentId in :transactionIds 
                                                            and ContentType =: CONTENT_TYPE]) {
                xmlByTransactionId.put(att.ParentId, att.Body.toString());
            }           
        }
        //system.debug('agX xmlByTransactionId=' + xmlByTransactionId);

        //prepare results
        final List<SFDCKitchenDrawerTypes.ActionResult> results = new List<SFDCKitchenDrawerTypes.ActionResult>();
        for(Transaction_Header__c tran : transactions) {
            String xmlText = xmlByTransactionId.get(tran.Id);
            String shortXml = null;
            String fullXml = null;
            if (!String.isBlank(xmlText)) {
                if (isFullXml) {
                    fullXml = xmlText;
                } else {
                    shortXml = xmlText;
                }
            }
            //system.debug('agX ' + tran.Receipt_Barcode_Number__c + '; xmlText=' + xmlText);
            SFDCKitchenDrawerTypes.KDTransaction kdTransaction = populateKDTransaction(tran, shortXml, fullXml);
            SFDCKitchenDrawerTypes.ActionResult result = new SFDCKitchenDrawerTypes.ActionResult();
            result.kdTransaction = kdTransaction;
            result.success = true;
            //for search results we do not have a scenario when Detail record will contain an Error
            result.message = '';//
            result.code = ''; //
            result.resultType = MyJL_Const.ERRORTYPE.BUSINESS.name();
            results.add(result);
            
        }


        response.results = results;
        response.searchResultHeader = new SFDCKitchenDrawerTypes.SearchResultHeader();
        response.searchResultHeader.recordSetTotal = transactionsQueried.size();
        response.searchResultHeader.recordSetCount = transactions.size();
        response.searchResultHeader.recordSetCompleteIndicator = response.searchResultHeader.recordSetTotal <= response.searchResultHeader.recordSetCount;

        return response;
    }

    public static SFDCKitchenDrawerTypes.KDTransaction populateKDTransaction(final Transaction_Header__c tran, final String shortXml, final String fullXml) {
        SFDCKitchenDrawerTypes.KDTransaction kdTransaction = new SFDCKitchenDrawerTypes.KDTransaction();
        kdTransaction.transactionId = tran.Receipt_Barcode_Number__c;
        kdTransaction.branchNumber = tran.Store_Code__c;
        kdTransaction.cardNumber = tran.Card_Number__r.Name;
        kdTransaction.channel = tran.Channel__c;
        kdTransaction.fullXml = fullXml;
        kdTransaction.shortXml = shortXml;
        kdTransaction.transactionAmount = tran.Transaction_Amount__c;
        kdTransaction.transactionDate = tran.Transaction_Date_Time__c;
        kdTransaction.voidingTransactionId = tran.Voiding_Transaction_Id__c;
        kdTransaction.transactionType = tran.Transaction_Type__c;

        //system.debug('agX tran=' + tran);
        //system.debug('agX tran.Associated_Transactios1__r=' + tran.Associated_Transactios1__r);
        kdTransaction.associatedTransactionIds = getAssociatedTransactionIds(tran.Associated_Transactios1__r);

        return kdTransaction;
    }

    private static String[] getAssociatedTransactionIds(final List<Associated_Transaction__c> recs) {
        final List<String> res = new List<String>();
        if (null != recs) {
            for(Associated_Transaction__c tran : recs) {
                if (true == tran.Transaction__r.IsValid__c) {
                    String associatedId = tran.Transaction__r.Receipt_Barcode_Number__c;
                    res.add(associatedId);
                }
            }
        }

        return res;
    }

    private static String andX(final String whereSoFar) {
        return String.isBlank(whereSoFar)? '' : ' and ';
    }

    private static String getSortByField(final SFDCKitchenDrawerTypes.SortByFields sortByField) {
        final Map<SFDCKitchenDrawerTypes.SortByFields, String> fieldMap = new Map<SFDCKitchenDrawerTypes.SortByFields, String>{
            SFDCKitchenDrawerTypes.SortByFields.TRANSACTION_DATE => 'Transaction_Date_Time__c',
            SFDCKitchenDrawerTypes.SortByFields.TRANSACTION_NUMBER => 'Receipt_Barcode_Number__c'
        };
        String fName = fieldMap.get(sortByField);
        if (String.isBlank(fName)) {
            fName = 'Transaction_Date_Time__c';//if "sort by" field is not provided then order by Transaction_Date_Time__c
        }
        return fName;
    }
}