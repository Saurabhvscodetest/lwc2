/******************************************************************************
* @author       Unknown
* @date         Unknown
* @description  Class that seems to just create a manual share record for cases.
*
* EDIT RECORD
*
* LOG     DATE        Author	JIRA		COMMENT      
* 001     03/03/2016  MP		COPT-654	Complete refactor of class.
* 002     03/03/2016  Llyr		COPT-1265	Rename class
*******************************************************************************
*/
public without sharing class Sharing {
   
	public static final String EDIT_ACCESS_LEVEL = 'Edit';
	public static final String READ_ACCESS_LEVEL = 'Read';
	
    /**
     * Creates a new manual share record.  Boolean value specifies if it should be
     * inserted or returned.
	 * @params:	Id recordId			The Id of the record being shared.
	 *			Id userOrGroupId 	The Id of the User or Group to be shared with.
	 *			String accessLevel 	Read or Edit.
	 *			Boolean insertToDatabase	Denots whether record should be inserted.
     */
	public static CaseShare createManualCaseShare(Id recordId, Id userOrGroupId, String accessLevel, Boolean insertToDatabase) {
		// Create new sharing object for the custom object case.
		CaseShare caseShr  = new CaseShare();
		
		// Set the ID of record being shared.
		caseShr.CaseId = recordId;
		system.debug('manualShareRead.caseID: ' + recordId);
		
		// Set the ID of user or group being granted access.
		caseShr.UserOrGroupId = userOrGroupId;
		system.debug('manualShareRead.UserOrGroupId: ' + UserOrGroupId);
		
		// Set the access level.
		caseShr.CaseAccessLevel = accessLevel;
		
		if (insertToDatabase) {
			system.debug('input case share record');
			insert caseShr;
		}
		
		return caseShr;
	}
}