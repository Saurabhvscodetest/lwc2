/*
* @File Name   : MyJLChatBotResponseModel
* @Description : Model class to build the response   
* @Copyright   : Zensar
* @Jira #      : #5066
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0       19-Sep-19         Ragesh G                     Created
*/


global class MyJLChatBotResponseModel {
    global String responseCode {get;set;} 
    global Integer statusCode {get;set;} 
    global String responseMessage {get;set;} 
    global String Success		  {get;set;} 
	global String CaseNumbers      {get;set;}  
        
        global MyJLChatBotResponseModel(){
        	
        }
        global void setResponseModel( Integer statusCodeVal,  String reponseCodeVal, String responseMessageVal ) {
        	this.Success = 'false';
        	this.statusCode = statusCodeVal;
            this.responseCode = reponseCodeVal;
            this.responseMessage = responseMessageVal;
        }
        
        global void setSuccessResponse( Integer statusCodeVal,  String reponseCodeVal, String responseMessageVal ) {
        	this.Success = 'true';
            this.responseCode = reponseCodeVal;
            this.statusCode = statusCodeVal;
            this.responseMessage = MyJLChatBotConstants.CASE_CREATED_SUCCESS_MESSAGE;
            if ( responseMessageVal != null ) {
            	responseMessageVal = responseMessageVal.replaceAll ('\\(','');
            	responseMessageVal = responseMessageVal.replaceAll ('\\)','');
            }
            this.CaseNumbers = responseMessageVal;
        }
        
        global void setFailureResponse( Integer statusCodeVal,  String reponseCodeVal, String responseMessageVal ) {
        	this.Success = 'false';
        	this.statusCode = statusCodeVal;
            this.responseCode = reponseCodeVal;
            this.CaseNumbers = 'NULL';
            this.responseMessage = responseMessageVal;
        }
        
}