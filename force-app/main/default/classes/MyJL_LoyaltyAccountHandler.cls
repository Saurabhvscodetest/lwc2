public without sharing class MyJL_LoyaltyAccountHandler extends MyJL_TriggerHandler {
    
    public static final String FULL_JOIN_NOTIFY = 'FULL_JOIN_NOTIFY';
    public static final String PARTIAL_JOIN_NOTIFY = 'PARTIAL_JOIN_NOTIFY';
    public static final String LEAVE_NOTIFY = 'LEAVE_NOTIFY';
    public static final String OUTBOUND_CONTACT_PROFILE_UPDATE_NOTIFY = 'OUTBOUND_CONTACT_PROFILE_UPDATE_NOTIFY';
    
    override public void afterUpdate () {     
        if (!BaseTriggerHandler.hasSkipReason('JoinNotification')) {
            prepareIdsToSendJoinOrLeaveNotification( (Map<ID, Loyalty_Account__c>)Trigger.newMap, (Map<ID, Loyalty_Account__c>) Trigger.oldMap);
        }
    }
    
    /**
* check where IsActive__c flag has changed and pass Ids of such records to the outbound call
*/
    private void prepareIdsToSendJoinOrLeaveNotification(final Map<ID, Loyalty_Account__c> newMap, final Map<ID, Loyalty_Account__c> oldMap) {
        final Set<Id> changedIds = new Set<Id>();
        for(Loyalty_Account__c acc : newMap.values()) {
            Loyalty_Account__c oldAccount = oldMap.get(acc.ID);
            
            if(acc.IsActive__c != oldAccount.IsActive__c) {
                changedIds.add(acc.Id);
             }else if(acc.Number_Of_Partnership_Cards__c != oldAccount.Number_Of_Partnership_Cards__c){
                List<Loyalty_Account__c> newPartnershipLoyaltyAccount = [SELECT Id, Name
                                                                         FROM Loyalty_Account__c
                                                                         WHERE Id in : newMap.keySet() AND Number_Of_Partnership_Cards__c > 0 LIMIT 1];                   
                if(newPartnershipLoyaltyAccount.size() > 0) {
                    changedIds.add(newPartnershipLoyaltyAccount[0].Id);
                }
            }
        }
        
        sendJoinOrLeaveNotification(changedIds);
    }
    
    /**
* call this method to send an Outbound Join/Leave notification with Set of Loyalty_Account__c Ids
*/
    public static void sendJoinOrLeaveNotification(final Set<Id> loyaltyAccountIds) {
        if (loyaltyAccountIds.isEmpty() || BaseTriggerHandler.hasSkipReason('JoinNotification')) {
            return;
        }
        final Map<String, List<SFDCMyJLCustomerTypes.Customer>> customersByCalloutType = getCustomersByCalloutType(loyaltyAccountIds);
        if (!customersByCalloutType.isEmpty()) {
            //make a call out
            CalloutHandler.callout(customersByCalloutType);
        }
    }
    
    ///////////////////////////// RetriableCallout /////////////////////////////////////////////
    public class JoinLeaveCallout implements RetriableCallout {
        private String topic;
        private String certName;
        
        private MyJL_LoyaltyAccountHandler.NotifyCustomerEventPort service = new MyJL_LoyaltyAccountHandler.NotifyCustomerEventPort();
        
        public String serialise (final Object obj) {
            return JSON.serialize(obj);
        }
        public Object deserialise (final String val) {
            return JSON.deserialize(val, SFDCMyJLCustomerTypes.Customer.class);
        }
        
        public void setEndpoint(final String url) {
            service.endpoint_x = url;
        }
        public void setTimeout(final Integer mills) {
            service.timeout_x = mills;
        }
        public void setTopic(final String topic) {
            this.topic = topic;
        }
        public void setClientCertificateName(final String uniqueName) {
            this.certName = uniqueName;
        }
        
        public virtual void callService(final List<Object> objects) {
            final List<SFDCMyJLCustomerTypes.Customer> customers = castCustomersFromObjects(objects);
            System.assert(String.isNotBlank(this.topic), 'Topic must have been defined. Check if Callout_Settings__c are configured properly');
            service.clientCertName_x = certName;
            service.NotifyCustomerEvent(customers, this.topic);
        }
        
        private List<SFDCMyJLCustomerTypes.Customer> castCustomersFromObjects(final List<Object> objects) {
            final List<SFDCMyJLCustomerTypes.Customer> customers = new List<SFDCMyJLCustomerTypes.Customer>();
            for(Object obj : objects) {
                customers.add((SFDCMyJLCustomerTypes.Customer)obj);
            }
            return customers;
        }
    }
    
    @TestVisible
    public static Map<String, List<SFDCMyJLCustomerTypes.Customer>> getCustomersByCalloutType(final Set<Id> loyaltyAccountIds) {
        final Map<Id, SFDCMyJLCustomerTypes.Customer> customerByProfileId = new Map<Id, SFDCMyJLCustomerTypes.Customer>();
        final Map<Id, SFDCMyJLCustomerTypes.Customer> customerByLoyaltyAccountId = new Map<Id, SFDCMyJLCustomerTypes.Customer>();
        final Map<Id, SFDCMyJLCustomerTypes.CustomerAccount> customerAccountByLoyaltyAccountId = new Map<Id, SFDCMyJLCustomerTypes.CustomerAccount>();
        final Map<Id, String> eventTypeById = new Map<Id, String>();
        final Map<String, List<SFDCMyJLCustomerTypes.Customer>> customersByCalloutType = new Map<String, List<SFDCMyJLCustomerTypes.Customer>> ();
        // MJL-1790
        if (!loyaltyAccountIds.isEmpty()) {
            // TODO - this would be better if we did not check for NULL
            for(Loyalty_Account__c acc : [select Id, Name, contact__c,
                                          Activation_Date_Time__c, Channel_ID__c, Customer_Loyalty_Account_Card_ID__c,
                                          Deactivation_Date_Time__c, Email_Address__c, IsActive__c, LastModifiedById,
                                          LastModifiedDate, LastModifiedDateMS__c, Registration_Business_Unit_ID__c, Registration_Email_Sent_Flag__c,
                                          Registration_Workstation_ID__c, Scheme_Joined_Date_Time__c, Welcome_Email_Sent_Flag__c,
                                          ShopperId__c, Voucher_Preference__c, My_Partnership_Activation_Date_Time__c, Number_Of_Partnership_Cards__c, MyJL_Old_Membership_Number__c, Membership_Number__c, MyJL_Old_Barcode__c,
                                          (select Id, Loyalty_Account__c,Name from MyJL_Cards__r),
                                          contact__r.SourceSystem__c
                                          from Loyalty_Account__c
                                          where
                                          Id in :loyaltyAccountIds]) {  //<<010>>
                                              
                                              if (null == acc.MyJL_Cards__r || acc.MyJL_Cards__r.isEmpty()) {
                                                  //Loyalty_Account__c without a card is irrelevant and shall be ignored
                                                  continue;
                                              }
                                              
                                              // MJL-1790 - removed null check from SOQL to be efficient
                                              Boolean isJL = false;
                                              if (acc.contact__c != null) {
                                                  isJL = (acc.contact__r.SourceSystem__c == System.Label.MyJL_JL_comSourceSystem);  //<<010>>
                                              }                                             
                                              
                                              //Determine event to use
                                              //Full Join:    Active flag set to true and has a reference to a Contact Profile
                                              //Partial Join: Active flag set to true and no reference to a Contact Profile
                                              //Leave:        Active flag set to false
                                              String eventType = null;
                                              if (String.isNotBlank(acc.ShopperId__c)) {
                                                  //if(null != acc.Customer_Profile__c) {
                                                  if (true == acc.IsActive__c) {                      
                                                      eventType = FULL_JOIN_NOTIFY;
                                                  } else {
                                                      eventType = LEAVE_NOTIFY;
                                                  }
                                              } else  {
                                                  //Loyalty_Account__c without Contact_Profile__c can only Partially Join
                                                  // 008
                                                  if (acc.contact__c == null) {  //<<010>>
                                                      eventType = PARTIAL_JOIN_NOTIFY;
                                                  } else {
                                                      eventType = OUTBOUND_CONTACT_PROFILE_UPDATE_NOTIFY;
                                                  }
                                              }
                                              
                                              // Async Note: Join may have been partial or overtaken the Update that converted to a Contact Profile. In either case we can't add to the map
                                              if (null != acc.contact__c) {  //<<010>>
                                                  // TODO                    
                                                  customerByProfileId.put(acc.contact__c, null);  //<<010>>
                                                  eventTypeById.put(acc.contact__c, eventType);
                                              } else  {
                                                  // Async note: Ab>Initio only cares about Customers, does not care if they are Contact Profiles or Potential Customers. We need to create one
                                                  // so that by the end of this else statement we are in the same position as if we had a Contact Profile
                                                  
                                                  // Create a blank customer to fill in
                                                  SFDCMyJLCustomerTypes.Customer customer = new SFDCMyJLCustomerTypes.Customer();
                                                  
                                                  // MJL-1533 - Only add one as it is a CHOICE
                                                  Boolean emailPresent = String.isNotBlank(acc.Email_Address__c);
                                                  //Boolean telephonePresnt = String.isNotBlank(acc.Email_Address__c);
                                                  
                                                  // Add the email
                                                  customer.partyContactMethod = new List<SFDCMyJLCustomerTypes.PartyContactMethod>();
                                                  if (emailPresent) {
                                                      SFDCMyJLCustomerTypes.PartyContactMethod partyContactMethod = new SFDCMyJLCustomerTypes.PartyContactMethod();
                                                      SFDCMyJLCustomerTypes.EmailAddress emailAddress = new SFDCMyJLCustomerTypes.EmailAddress();
                                                      emailAddress.EmailAddress = acc.Email_Address__c;
                                                      partyContactMethod.EmailAddress = emailAddress;
                                                      customer.partyContactMethod.add(partyContactMethod);
                                                  }
                                                  
                                                  // Now add the customer account to the customer and populate it
                                                  customer.CustomerAccount = new List<SFDCMyJLCustomerTypes.CustomerAccount>();
                                                  SFDCMyJLCustomerTypes.CustomerAccount customerAccount = MyJL_MappingUtil.populateLoyaltyAccount(acc);
                                                  
                                                  customer.CustomerPreference = MyJL_MappingUtil.populateCustomerPreferenceForVoucherPreference(acc);
                                                  customer.CustomerAccount.add(customerAccount);
                                                  
                                                  // MJL-1912
                                                  customerAccount.WelcomeEmailSentFlag = indicateWelcomeEmailSent(eventType);
                                                  
                                                  // Add the newly formed customer to the maps
                                                  customerByLoyaltyAccountId.put(acc.Id, customer);
                                                  customerAccountByLoyaltyAccountId.put(acc.Id, customerAccount);
                                                  
                                                  // TODO
                                                  eventTypeById.put(acc.contact__c, eventType);  //<<010>>
                                                  addToMap(customersByCalloutType, eventType, customer);
                                              }
                                          }
        }
        
        // MJL-1790
        List<Loyalty_Card__c> cardsToUpdate = new List<Loyalty_Card__c>();
        if (!customerByProfileId.keySet().isEmpty()) {
            //load all Contact_Profile__c-s
            for(contact profile : [select Id, Name, Shopper_ID__c, Customer_Group_Name__c, birthdate,
                                   firstname, lastname, salutation, SourceSystem__c,MobilePhone,
                                   mailingstreet,otherstreet,Mailing_House_No_Text__c,Mailing_House_Name__c,Mailing_Street__c,
                                   Mailing_Address_Line2__c,Mailing_Address_Line3__c,Mailing_Address_Line4__c,
                                   mailingcity, othercity, mailingstate, otherstate,
                                   mailingcountry,mailingcountrycode, othercountry, mailingpostalcode, otherpostalcode,
                                   title, phone, email, AnonymousFlag2__c,
                                   (select Id, contact__c, Name,
                                    Activation_Date_Time__c, Channel_ID__c, Customer_Loyalty_Account_Card_ID__c,
                                    Deactivation_Date_Time__c, Email_Address__c, IsActive__c, LastModifiedById,
                                    LastModifiedDate, LastModifiedDateMS__c, Registration_Business_Unit_ID__c, Registration_Email_Sent_Flag__c,
                                    Registration_Workstation_ID__c, Scheme_Joined_Date_Time__c, Welcome_Email_Sent_Flag__c, Voucher_Preference__c,
                                    Number_Of_Partnership_Cards__c, My_Partnership_Activation_Date_Time__c, MyJL_Old_Membership_Number__c, Membership_Number__c, MyJL_Old_Barcode__c
                                    from MyJL_Accounts__r where Id in :loyaltyAccountIds
                                   )
                                   from contact
                                   where Id in :customerByProfileId.keySet() and SourceSystem__c =: System.Label.MyJL_JL_comSourceSystem
                                  ]) {  //<<010>>   //<<011>>
                                      SFDCMyJLCustomerTypes.Customer customer = MyJL_MappingUtil.populateCustomer(profile, null);
                                      customerByProfileId.put(profile.Id, customer);
                                      String eventType = eventTypeById.get(profile.Id);
                                      addToMap(customersByCalloutType, eventType, customer);
                                      
                                      if (null != profile.MyJL_Accounts__r) {
                                          customer.CustomerAccount = new List<SFDCMyJLCustomerTypes.CustomerAccount>();
                                          Map<String, Loyalty_Card__c> loyaltyAccountCards = MyJL_MappingUtil.getLoyaltyAccountCards(loyaltyAccountIds);
                                          
                                          for(Loyalty_Account__c acc : profile.MyJL_Accounts__r) {
                                              SFDCMyJLCustomerTypes.CustomerAccount customerAccount = MyJL_MappingUtil.populateLoyaltyAccount(acc);
                                              
                                              //FOL Changes
                                              System.debug('<FOL> customerAccount Data ' + customerAccount);
                                              
                                              
                                              if(acc.Number_Of_Partnership_Cards__c > 0){  
                                                  customer.CustomerPreference = MyJL_MappingUtil.updateCustomerWithPartnershipDetails(customer, acc); 
                                                  if(acc.IsActive__c == false) {
                                                      cardsToUpdate.add(loyaltyAccountCards.get(MyJL_Const.MY_PARTNERSHIP_CARD_TYPE));
                                                  }
                                                  
                                                  SFDCMyJLCustomerTypes.CustomerLoyaltyAccountCard card = new SFDCMyJLCustomerTypes.CustomerLoyaltyAccountCard();
                                                  SFDCMyJLCustomerTypes.CustomerLoyaltyAccountCard[] cardsToInsert = new SFDCMyJLCustomerTypes.CustomerLoyaltyAccountCard[]{};
                                                      System.debug('<FOL> eventType = ' +eventType);
                                                  if(eventType == OUTBOUND_CONTACT_PROFILE_UPDATE_NOTIFY){
                                                      card.CustomerLoyaltyAccountCardId = acc.Membership_Number__c;
                                                  }else{
                                                      card.CustomerLoyaltyAccountCardId = acc.IsActive__c?  acc.MyJL_Old_Membership_Number__c : acc.Membership_Number__c;                     
                                                  }
                                                  cardsToInsert.add(card);
                                                  customerAccount.CustomerLoyaltyAccountCard = cardsToInsert;
                                              }
                                              // MJL-1912, set email
                                              customerAccount.WelcomeEmailSentFlag = indicateWelcomeEmailSent(eventType);
                                              customer.CustomerAccount.add(customerAccount);
                                              
                                              customerAccountByLoyaltyAccountId.put(acc.Id, customerAccount);
                                          }
                                      }
                                  }
        }
        
        if(cardsToUpdate.size() >0){
            MyJL_LeaveLoyaltyAccountHandler.deactivatePartnershipCard(cardsToUpdate);
        }
        
        // MJL-1790
        if (!loyaltyAccountIds.isEmpty()) {
            //load all Loyalty_Card__c-s
            for(Loyalty_Card__c card : [select Id, Loyalty_Account__c, Name, Loyalty_Account__r.contact__c
                                        from Loyalty_Card__c where Loyalty_Account__c in :loyaltyAccountIds]) {  //<<010>>
                                            SFDCMyJLCustomerTypes.CustomerAccount customerAccount = customerAccountByLoyaltyAccountId.get(card.Loyalty_Account__c);
                                            if (null == customerAccount) {
                                                continue;
                                            }
                                            // MJL-1615, if we have already done this card processing - don't do it again. The presence of the list tells us...
                                            if (customerAccount.CustomerLoyaltyAccountCard != null) {
                                                continue;
                                            }
                                            
                                            SFDCMyJLCustomerTypes.Customer customer;
                                            if (null != card.Loyalty_Account__r.contact__c) {  //<<010>>
                                                customer = customerByProfileId.get(card.Loyalty_Account__r.contact__c); //<<010>>
                                            } else {
                                                customer = customerByLoyaltyAccountId.get(card.Loyalty_Account__c);
                                            }
                                            
                                            
                                            //Create CustomerLoyaltyAccountCard object
                                            SFDCMyJLCustomerTypes.CustomerLoyaltyAccountCard customerLoyaltyAccountCard = MyJL_MappingUtil.populateCustomerLoyaltyAccountCard(card);
                                            
                                            //Add to List
                                            if(customerAccount.CustomerLoyaltyAccountCard == null) {
                                                customerAccount.CustomerLoyaltyAccountCard = new List<SFDCMyJLCustomerTypes.CustomerLoyaltyAccountCard>();
                                            }
                                            customerAccount.CustomerLoyaltyAccountCard.add(customerLoyaltyAccountCard);
                                        }
        }
        
        return customersByCalloutType;
    }
    
    private static Boolean indicateWelcomeEmailSent(String eventType) {
        Boolean emailSent = false;
        // MJL-1912, set welcome email flag set to true for FULL and PARTIAL JOIN. We are sending it but it will happen asynchronously and caller needs to know
        if (eventType == FULL_JOIN_NOTIFY) {
            emailSent = true;
        }
        return emailSent;
    }
    
    // TODO: if key is null then consider not adding to map
    private static void addToMap(Map<String, List<SFDCMyJLCustomerTypes.Customer>> customersByCalloutType, String key, SFDCMyJLCustomerTypes.Customer value) {
        List<SFDCMyJLCustomerTypes.Customer> customers = customersByCalloutType.get(key);
        if (null == customers) {
            customers = new List<SFDCMyJLCustomerTypes.Customer>();
            customersByCalloutType.put(key, customers);
        }
        customers.add(value);
    }
    
    public class NotifyCustomerEventPort {
        public String endpoint_x = ''; //'http://requestb.in/sgcl8csg'
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://soa.johnlewispartnership.com/salesforce/custom/Customer/types', 'SFDCMyJLCustomerTypes', 'http://soa.johnlewispartnership.com/service/ACS/ALL/NotifyCustomer/v1', 'MyJL_NotifyServices'};
            
            public SFDCMyJLCustomerTypes.PublishCustomerEventResponse NotifyCustomerEvent(SFDCMyJLCustomerTypes.Customer[] Customer,String EventName) {
                SFDCMyJLCustomerTypes.PublishCustomerEventRequest request_x = new SFDCMyJLCustomerTypes.PublishCustomerEventRequest();
                request_x.Customer = Customer;
                request_x.EventName = EventName;
                
                SFDCMyJLCustomerTypes.PublishCustomerEventResponse response_x;
                Map<String, SFDCMyJLCustomerTypes.PublishCustomerEventResponse> response_map_x = new Map<String, SFDCMyJLCustomerTypes.PublishCustomerEventResponse>();
                response_map_x.put('response_x', response_x);
                System.debug('<FOL> Customer ' + Customer);
                System.debug('<FOL> serivce has been invoked here ' + request_x);
                WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                        'http://soa.johnlewispartnership.com/salesforce/custom/NotifyCustomerEvent/v1',
                        'http://soa.johnlewispartnership.com/salesforce/custom/Customer/types',
                        'PublishCustomerEventRequest',
                        'http://soa.johnlewispartnership.com/salesforce/custom/Customer/types',
                        'PublishCustomerEventResponse',
                        'SFDCMyJLCustomerTypes.PublishCustomerEventResponse'}
                );
                response_x = response_map_x.get('response_x');
                
                // MJL-1533 - Look in the response to see if we need to do retry
                processWebServiceResult(response_x);
                processWebServiceResult(response_x);
                
                return response_x;
            }
    }
    
    private static void processWebServiceResult(SFDCMyJLCustomerTypes.PublishCustomerEventResponse response_x) {
        Boolean ok = true;
        
        if (response_x.ResponseHeader.ResultStatus.success != true) {
            ok = false;
        } else {
            for (SFDCMyJLCustomerTypes.CustomerRecordResultType crrt:response_x.ActionRecordResults) {
                if (!crrt.success) {
                    ok = false;
                }
            }
        }
        
        if (!ok) {
            throw new JLException('ESB Web Service call reported error');
        }
    }
    
    
    
    override public void beforeUpdate () {
        for(Loyalty_Account__c newLA : (List<Loyalty_Account__c>) trigger.new){
            Loyalty_Account__c oldLA = (Loyalty_Account__c) trigger.oldMap.get(newLA.id);
            if (!myJL_Util.lastModifiedIsLater(oldLA.Source_LastModifiedDate__c, newLA.Source_LastModifiedDate__c)) {
                copyLA(oldLA, newLA);
            }
        }
    }
    
    private static void copyLA(Loyalty_Account__c oldLA, Loyalty_Account__c newLA) {
        newLA.Scheme_Joined_Date_Time__c = oldLA.Scheme_Joined_Date_Time__c;
        newLA.Registration_Business_Unit_ID__c = oldLA.Registration_Business_Unit_ID__c;
        newLA.Extract_ConcatReference__c = oldLA.Extract_ConcatReference__c;
        newLA.Activation_Date_Time__c = oldLA.Activation_Date_Time__c;
        newLA.Registration_Workstation_ID__c = oldLA.Registration_Workstation_ID__c;
        newLA.IsActive__c = oldLA.IsActive__c;
        newLA.Welcome_Email_Sent_Flag__c = oldLA.Welcome_Email_Sent_Flag__c;
        newLA.Source_LastModifiedDate__c = oldLA.Source_LastModifiedDate__c;
        newLA.Membership_Number__c = oldLA.Membership_Number__c;
        newLA.Deactivation_Date_Time__c = oldLA.Deactivation_Date_Time__c;
        newLA.Email_Address__c = oldLA.Email_Address__c;
        newLA.Channel_ID__c = oldLA.Channel_ID__c;
        newLA.Voucher_Preference__c = oldLA.Voucher_Preference__c;
    }
}