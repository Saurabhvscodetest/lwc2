@isTest
private class MyJL_LeaveRequestTest {
  private static final String SHOPPER_ID = '1234abcdefg';
  private static final String LOYALTY_NUMBER = '345678945612';
  private static final String MESSAGE_NUMBER = '1111111111';
  private static final String CARD_NUMBER = '1111111111111111111111';
    Private static final Contact customer;
    Private static final Case cs;
    static{
    BaseTriggerHandler.addSkipReason(CalloutHandler.SKIP_ALL_NOTIFICATIONS_CODE, 'disable outbound events in this test class');
    }
    @testSetup
    static void setup(){
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        
        Contact customer = MyJL_TestUtil.createCustomer('Ordering_Customer');
        customer.Shopper_ID__c = SHOPPER_ID;
        update customer;
        
        final List<Loyalty_Account__c> loyaltyAccounts = new List<Loyalty_Account__c>();
    loyaltyAccounts.add(new Loyalty_Account__c(Name = LOYALTY_NUMBER ,ShopperId__c=SHOPPER_ID,IsActive__c = true,Membership_Number__c = LOYALTY_NUMBER, contact__c = customer.id));
    insert loyaltyAccounts;
        
    final List<Loyalty_Card__c> cards = new List<Loyalty_Card__c>();
    cards.add(new Loyalty_Card__c(Name = CARD_NUMBER,Disabled__c = false , Loyalty_Account__c = loyaltyAccounts[0].Id));
    Database.insert(cards);
        
    }
        
  
    @isTest static void testServiceWithNoLoyaltyNumber() {

    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();

    RestContext.request = req;
    RestContext.response = res;

    req.requestURI = '/MyJL_LeaveRequest/customer/' ;
    req.httpMethod = 'POST';

    Test.startTest();
    MyJL_LeaveRequest.ResponseWrapper rs = MyJL_LeaveRequest.postLeaveRequest( MESSAGE_NUMBER, SHOPPER_ID,  null);
    Test.stopTest();
  }

  
  @isTest static void testServiceWithNoShopperProvided() {

    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();

    RestContext.request = req;
    RestContext.response = res;

    req.requestURI = '/MyJL_LeaveRequest/customer/';
    req.httpMethod = 'POST';

    Test.startTest();
    MyJL_LeaveRequest.ResponseWrapper rs = MyJL_LeaveRequest.postLeaveRequest(MESSAGE_NUMBER, null,  LOYALTY_NUMBER );
    Test.stopTest();
  }
  
  @isTest static void testServiceWithNoMessageProvided() {

    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();

    RestContext.request = req;
    RestContext.response = res;

    req.requestURI = '/MyJL_LeaveRequest/customer/';
    req.httpMethod = 'POST';

    Test.startTest();
    MyJL_LeaveRequest.ResponseWrapper rs = MyJL_LeaveRequest.postLeaveRequest(null, SHOPPER_ID,  LOYALTY_NUMBER );
    Test.stopTest();
  }
  
  @isTest static void testServiceShopperNotFound() {

    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();

    RestContext.request = req;
    RestContext.response = res;

    req.requestURI = '/MyJL_LeaveRequest/customer/';
    req.httpMethod = 'POST';

    Test.startTest();
    MyJL_LeaveRequest.ResponseWrapper rs = MyJL_LeaveRequest.postLeaveRequest(MESSAGE_NUMBER, 'aaaaaaa', LOYALTY_NUMBER );
    Test.stopTest();
  }
    
    @isTest static void testServiceShopperNotFoundCaseCheck() {

    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();

    RestContext.request = req;
    RestContext.response = res;

    req.requestURI = '/MyJL_LeaveRequest/customer/';
    req.httpMethod = 'POST';

    Test.startTest();
    MyJL_LeaveRequest.ResponseWrapper rs = MyJL_LeaveRequest.postLeaveRequest(MESSAGE_NUMBER, '1234abcdefG', LOYALTY_NUMBER );
    Test.stopTest();
  }
  
  @isTest static void testServiceWithLoyaltyAccountNotFound() {

    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();

    RestContext.request = req;
    RestContext.response = res;

    req.requestURI = '/MyJL_LeaveRequest/customer/';
    req.httpMethod = 'POST';

    Test.startTest();
    MyJL_LeaveRequest.ResponseWrapper rs = MyJL_LeaveRequest.postLeaveRequest(MESSAGE_NUMBER, SHOPPER_ID, '1111111' );
    Test.stopTest();
  }
  
   @isTest static void testServiceWithDeactivatedAccount() {

    Loyalty_Account__c account = [select IsActive__c From Loyalty_Account__c where ShopperId__c = :SHOPPER_ID Limit 1];

    if( account != null){
      account.IsActive__c = false;
      update account;
    }

    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();

    RestContext.request = req;
    RestContext.response = res;

    req.requestURI = '/MyJL_LeaveRequest/customer/' + SHOPPER_ID;
    req.httpMethod = 'POST';

    Test.startTest();
    MyJL_LeaveRequest.ResponseWrapper rs = MyJL_LeaveRequest.postLeaveRequest(MESSAGE_NUMBER, SHOPPER_ID, LOYALTY_NUMBER );
    Test.stopTest();
  }
  
  
  @isTest static void testServiceWithDifferentMembershipNumber() { // check

    Loyalty_Account__c account = [select name,Membership_Number__c From Loyalty_Account__c where ShopperId__c = :SHOPPER_ID Limit 1];

    if( account != null){
      account.Membership_Number__c =  '22222222222';
      account.Name =  '22222222222';
      update account;
    }

    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();

    RestContext.request = req;
    RestContext.response = res;

    req.requestURI = '/MyJL_LeaveRequest/customer/' + SHOPPER_ID;
    req.httpMethod = 'POST';

    Test.startTest();
    MyJL_LeaveRequest.ResponseWrapper rs = MyJL_LeaveRequest.postLeaveRequest(MESSAGE_NUMBER, SHOPPER_ID, LOYALTY_NUMBER );
    Test.stopTest();
  }
  
  @isTest static void testServiceWithValidData() {

    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();

    RestContext.request = req;
    RestContext.response = res;

    req.requestURI = '/MyJL_LeaveRequest/customer/';
    req.httpMethod = 'POST';

    Test.startTest();
    MyJL_LeaveRequest.ResponseWrapper rs = MyJL_LeaveRequest.postLeaveRequest(MESSAGE_NUMBER, SHOPPER_ID, LOYALTY_NUMBER );
    Test.stopTest();    
  }
    
    @isTest static void testServiceWithDuplicateMessageId() {

    RestRequest req = new RestRequest(); 
    RestResponse res = new RestResponse();

    RestContext.request = req;
    RestContext.response = res;

    req.requestURI = '/MyJL_LeaveRequest/customer/';
    req.httpMethod = 'POST';

    Test.startTest();
    MyJL_LeaveRequest.ResponseWrapper rs = MyJL_LeaveRequest.postLeaveRequest(MESSAGE_NUMBER, SHOPPER_ID, LOYALTY_NUMBER );
    rs = MyJL_LeaveRequest.postLeaveRequest(MESSAGE_NUMBER, SHOPPER_ID, LOYALTY_NUMBER );    
    Test.stopTest();
  }
}