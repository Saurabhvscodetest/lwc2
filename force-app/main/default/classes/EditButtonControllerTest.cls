@isTest
private class EditButtonControllerTest {
	
	@testSetup static void initEntitlementTestData() {
		MilestoneUtilsTest.initEntitlementTestData();
	}


	@isTest static void testOpenCase() {

		User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		User tier1User;

		System.runAs(runningUser) {
			tier1User = UnitTestDataFactory.getFrontOfficeUser('TestUser');
			insert tier1User;
		}

		System.runAs(tier1User) {

			// Create an open case
			Case openCase = createCase();
			insert openCase;

			// Assert its open
			openCase = [SELECT IsClosed FROM Case WHERE Id = :openCase.Id];

			System.assertEquals(false, openCase.IsClosed);

			// Now press New Comment button
			ApexPages.StandardController standardController = new ApexPages.StandardController(openCase);
			EditButtonController controllerExtension = new EditButtonController(standardController);

			// Mimic the onload action
			PageReference returnedPage = controllerExtension.validate();

			String returnedPageUrl = returnedPage.getUrl();
			String expectedUrl = '/'+openCase.Id+'/e'+'?retURL=%2F'+openCase.Id;
			
			System.assertEquals(expectedUrl, returnedPageUrl);

		}

	}



	@isTest static void testCloseCase() {
		
		User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		User tier1User;
		Case closedCase;

		System.runAs(runningUser) {

			tier1User = UnitTestDataFactory.getFrontOfficeUser('TestUser');
			insert tier1User;
		}


		System.runAs(tier1User) {

			// Insert case
			closedCase = createCase();
			insert closedCase;

			// Close it
			closedCase.Status = 'Closed - Resolved';
			update closedCase;

			// Assert its closed
			closedCase = [SELECT IsClosed FROM Case WHERE Id = :closedCase.Id];
			System.assertEquals(true, closedCase.IsClosed);

			// Now press New Comment button
			Test.startTest();

				PageReference currentPage = Page.EditButtonPage;
				Test.setCurrentPage(currentPage);

				ApexPages.StandardController standardController = new ApexPages.StandardController(closedCase);
				EditButtonController controllerExtension = new EditButtonController(standardController);

				// Mimic onload action
				PageReference returnedPage = controllerExtension.validate();

				// Assert no page reference (redirect) was returned
				System.assertEquals(null, returnedPage);

				// Assert message was added
				List<ApexPages.Message> pageMessages = ApexPages.getMessages();
				System.assertEquals(1, pageMessages.size());
				System.assertEquals('You are trying to edit a closed case, would you like to reopen it?', pageMessages[0].getDetail());

				// Reset Trigger variables - there is some odd behaviour on second iteration.
				ClsCaseTriggerHandler.resetCaseTriggerStaticVariables();
				// Flush this list from previous execution, otherwise it will attempt to update the close milestones
				MilestoneHandler.caseMilestoneListToBeUpdated.clear();

				PageReference editPage = controllerExtension.reopenCase();
				String returnedPageUrl = editPage.getUrl();
				String expectedUrl = '/'+closedCase.Id+'/e'+'?retURL=%2F'+closedCase.Id;

				System.assertEquals(expectedUrl, returnedPageUrl);

			Test.stopTest();

		}

	}


	@isTest static void testErrorSurfacedFromReopenUtility() {

		User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		User testUser;
		Case testCase;

		System.runAs(runningUser) {
			testUser = UnitTestDataFactory.getFrontOfficeUser('TestUser');
			testUser.Team__c = '';
			insert testUser;
		}


		System.runAs(testUser) {
			
			// Create Case
			testCase = createCase();
			insert testCase;

			// Take ownership of Case, Remove the 'Last Queue', Close Case
			testCase.OwnerId = UserInfo.getUserId();
			update testCase; // Have to update first, otherwise Last Queue will get re-populated when we remove it.

			testCase.Status = 'Closed - Resolved';
			testCase.JL_Last_Queue_Name__c = '';
			testCase.JL_Last_Queue__c = '';
			update testCase;

			// Flush this list from previous execution, otherwise it will attempt to update the close milestones
			MilestoneHandler.caseMilestoneListToBeUpdated.clear();

			Test.startTest();

				PageReference currentPage = Page.EditButtonPage;
				Test.setCurrentPage(currentPage);

				ApexPages.StandardController standardController = new ApexPages.StandardController(testCase);
				EditButtonController controllerExtension = new EditButtonController(standardController);
	
				// Reset Trigger variables - there is some odd behaviour on second iteration.
				ClsCaseTriggerHandler.resetCaseTriggerStaticVariables();
				// Flush this list from previous execution, otherwise it will attempt to update the close milestones
				MilestoneHandler.caseMilestoneListToBeUpdated.clear();

				// Mimic onload action + reopen
				controllerExtension.validate();
				PageReference returnedPage = controllerExtension.reopenCase();

				// Assert message was added
				List<ApexPages.Message> pageMessages = ApexPages.getMessages();
				Set<String> errorMessages = new Set<String>();

				for(ApexPages.Message pageMessage : pageMessages) {
					errorMessages.add(pageMessage.getDetail());
				}

				System.assert(errorMessages.contains('You cannot re-open a case as a Tier 1 user if you are not assigned to a Primary Team.'));

			Test.stopTest();

		}

	}

	/* Helper */

	private static Case createCase() {

		Case newCase = UnitTestDataFactory.createCase(UnitTestDataFactory.createContact().Id);
		newCase.Bypass_Standard_Assignment_Rules__c = true;
		newCase.JL_Branch_master__c = 'Oxford Street';
		newCase.Contact_Reason__c = 'Pre-Sales';
		newCase.Reason_Detail__c = 'Buying Office Enquiry';
		
		return newCase;

	}
}