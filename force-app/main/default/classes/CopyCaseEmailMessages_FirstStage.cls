/*****************************************************************
**@author Ramachandran 
**  Edit    Date            By          Comment
**  001     15/12/15       Ram          Created. Base Chained Batch Apex Code.
**  002     17/12/15       Ram          Edited some sections of the code so that they do not execute under Test Execution     
*/

// First Stage of the Four Stage Batch Apex Class
global class CopyCaseEmailMessages_FirstStage implements Database.batchable<SObject>,Database.stateful{
    // Query to fetch all cases which have email Messages with Attachments
    global final String Query='select Id, Pending_Email_Messages__c from Case where Pending_Email_Messages__c != null';
    //Collection identifier used to store all Case Id's so as to process the all Cases to have the 
    //Email Message attachment Identifier as none, this is chained and passed on to last stage.
    global Set<id> allCaseId = new Set<id>();
    //Collection to Store all EmailMessageIds. ( Ideally Set is Preferred and used in Finish Method).
      global List<String> emailMessageIdList = new List<String>();
    
    // Base Query Locator used to process the query Provided  
    global Database.QueryLocator start(Database.BatchableContext BC){
     if (!Test.isRunningTest())
      {
      return Database.getQueryLocator(query);
      }else{
      return Database.getQueryLocator(query+' limit 200');
      }
   }

   // Identifying all the email Messages which has attachment and store the case Ids, This is done in Loop/ Batches 
   global void execute(Database.BatchableContext BC, List<sObject> scope){
      List<Case> listCases = scope;
    
      Map<Id,Id> emailMessageCaseMap = new Map<Id,Id>();
      for(Case c: listCases){
                emailMessageIdList.addAll(c.Pending_Email_Messages__c.split(';'));
               allCaseId.add(c.id);
        }
        
    }

    // Pass on the details of the stored emailMessage which has attachment and the case ids on to the Second Stage.
   global void finish(Database.BatchableContext BC){
       Set<String> emailMessageNewList = new Set<String>();
       for (String s: emailMessageIdList )
       {
           emailMessageNewList.add(s);
       }
       if (!Test.isRunningTest())
       {
           DataBase.ExecuteBatch(new CopyCaseEmailMessages_SecondStage(allCaseId,emailMessageNewList));
        }
   }

}