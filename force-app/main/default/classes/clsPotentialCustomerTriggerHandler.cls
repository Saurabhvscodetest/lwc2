//	Edit	Date		Who			Comment
//	001 	19/02/15	NTJ			MJL-1570 - If in migration mode then check source last modified date		
//	002		26/02/15	NTJ		MJL-1581 - Make House No a Text field so that it does not display as 999,999 (should be 999999)
//
public with sharing class clsPotentialCustomerTriggerHandler {


    public static boolean FIRST_BEFORE_TRIGGER_RUN = true;
    public static boolean FIRST_AFTER_TRIGGER_RUN = true;
    
    public static void mainEntry(Boolean isBefore, 
                               Boolean isDelete, Boolean isAfter, 
                               Boolean isInsert, Boolean isUpdate, 
                               Boolean isExecuting, List<Potential_Customer__c> newlist, 
                               Map<ID,Sobject> newmap, List<Potential_Customer__c> oldlist, 
                               Map<Id,Sobject> oldmap) {
         
        BaseTriggerHandler.process('MyJL_SystemDateHandler'); 
        System.debug('@@entering clsPotentialCustomerTriggerHandler');
        
        // TODO - This code has been commented out as its task are no longer needed. Consider retiring 
        /*
        clsPotentialCustomerTriggerHandler handler = new clsPotentialCustomerTriggerHandler();
        */
            
        if (isInsert && isBefore)
        {
            System.debug('@@calling handleContactProfileBeforeInserts');
            // TODO - This code has been commented out as its task are no longer needed. Consider retiring 
            /*
            handler.handlePotentialCustomerBeforeInserts(newlist);
            */
            FIRST_BEFORE_TRIGGER_RUN = false;
        } else if (isUpdate && isBefore) {
        	beforeHandler((Map<Id,Potential_Customer__c>)oldmap, newlist);
        }       
              
        System.debug('@@exiting clsPotentialCustomerTriggerHandler');
    }
    
    // TODO - this is not referenced anywhere so consider retiring
    /*
    public void handleContactBeforeInserts(List<Contact> newContacts) {        
        
        // create contact matching keyes
        for(Contact newC : newContacts){
            ContactUtils.updateContactCaseMatchKeys(newC);
        }
    }
    */
    
    // TODO - the field that is written to is deprecated so consider retirng this code
    /*
    public void handlePotentialCustomerBeforeInserts(List <Potential_Customer__c> newPotentialCustomers) {
        
        for(Potential_Customer__c newP : newPotentialCustomers){
            PotentialCustomerUtils.updatePotentialCustomerUniqueKeys(newP,skipAllEventsForCurrentUser());
        }
       // PotentialCustomerUtils.handlePotentialCustomerUniqueKey((Map<Id,Potential_Customer__c>) newMap, skipAllEventsForCurrentUser());
    }
    */
        
    public Boolean skipAllEventsForCurrentUser() {
        JL_RunTriggers__c runTriggerConfig = JL_RunTriggers__c.getValues(userinfo.getProfileId());
        return null != runTriggerConfig && false == runTriggerConfig.Run_Triggers__c;
    }
    
    // Start Edit 001
    private static void beforeHandler(Map<Id, Potential_Customer__c> oldMap, List<Potential_Customer__c> newList) {
        for(Potential_Customer__c newPC : (List<Potential_Customer__c>) newList){
            Potential_Customer__c oldPC = (Potential_Customer__c) trigger.oldMap.get(newPC.id);
    		// Edit 001
    		// MJL-1570 - Check whether migration in-progress and check source last modification
            if (!myJL_Util.lastModifiedIsLater(oldPC.Source_LastModifiedDate__c, newPC.Source_LastModifiedDate__c)) {
            	copyPC(oldPC, newPC);
            }
            // End 001
        }		    	
    }
    
    // MJL-1570 - Because we decided that the new record had a modified date *before* the old record -> we restore the old data.  
    private static void copyPC(Potential_Customer__c oldPC, Potential_Customer__c newPC) {
		newPC.Mailing_House_Name__c = oldPC.Mailing_House_Name__c;
		newPC.Mailing_Street__c = oldPC.Mailing_Street__c;
		newPC.FirstName__c = oldPC.FirstName__c;
		newPC.Mailing_House_No_Text__c = oldPC.Mailing_House_No_Text__c;
		newPC.Telephone_Number__c = oldPC.Telephone_Number__c;
		newPC.Salutation__c = oldPC.Salutation__c;
		newPC.Initials__c = oldPC.Initials__c;
		newPC.Mailing_City__c = oldPC.Mailing_City__c;
		newPC.Mailing_Address_Line2__c = oldPC.Mailing_Address_Line2__c;
		newPC.Date_of_Birth__c = oldPC.Date_of_Birth__c;
		newPC.Source_System__c = oldPC.Source_System__c;
		newPC.Mailing_County_Name__c = oldPC.Mailing_County_Name__c;
		newPC.Extract_ConcatReference__c = oldPC.Extract_ConcatReference__c;
		newPC.Source_LastModifiedDate__c = oldPC.Source_LastModifiedDate__c;
		newPC.Mailing_Address_Line3__c = oldPC.Mailing_Address_Line3__c;
		newPC.LastName__c = oldPC.LastName__c;
		newPC.Shopper_ID__c = oldPC.Shopper_ID__c;
		newPC.Mailing_Address_Line4__c = oldPC.Mailing_Address_Line4__c;
		newPC.Mailing_Country__c = oldPC.Mailing_Country__c;
		newPC.Email__c = oldPC.Email__c;  
		newPC.anonymousFlag__c = oldPC.anonymousFlag__c;  
	}    
    
    // End Edit 001
}