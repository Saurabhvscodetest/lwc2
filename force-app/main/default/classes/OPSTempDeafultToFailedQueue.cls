global class OPSTempDeafultToFailedQueue implements Database.Batchable<sObject>,Database.Stateful{
    Datetime currentTime = Datetime.now();
    Datetime currentTimeMinus30min = currentTime.addMinutes(-30);
    global integer result=0;
    global Map<Id,String> ErrorMap = new Map<Id,String>();
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator([Select id, CaseNumber, jl_Queue_Name__c, jl_Customer_Response_By__c,Next_Case_Activity_Due_Date_Time__c, 
                    Number_of_OpenCaseActivities__c,Number_of_Case_Activities__c, Case_Owner__c FROM Case 
                    Where CreatedDate > 2019-09-25T00:00:00.000+0000 
                    and Next_Case_Activity_Due_Date_Time__c <  :currentTimeMinus30min
                    and (NOT jl_Queue_Name__c like '%Failed%')
                    and (NOT jl_Queue_Name__c like '%ACTIONED%')
                    and (NOT jl_Queue_Name__c like '%Branch%')
                    and (NOT jl_Queue_Name__c like '%Robot%')
                    and (NOT jl_Queue_Name__c like '%ROBOT%')
                    and (NOT jl_Queue_Name__c like '%Do Not Use%')
                    and (NOT jl_Queue_Name__c like '%CDH%')
                    and (NOT jl_Queue_Name__c like '%No_Response%')
                    and (NOT jl_Queue_Name__c like '%JLTS%')
                    and  jl_Queue_Name__c != 'Email: Spam'
                    and jl_Queue_Name__c != ''
                    and jl_Queue_Name__c NOT IN('OSO Inbox',
                    'OSO Inbox ACTIONED',
                    'JLFS Banking',
                    'JLFS Banking ACTIONED',
                    'JLFS Insurance JLFS Insurance ACTIONED',
                    'CHS Fitted Bathrooms Orders',
                    'CHS Fitted Bathrooms Orders ACTIONED',
                    'CHS Fitted Flooring Orders',
                    'CHS Fitted Flooring Orders ACTIONED',
                    'CHS Fitted Kitchens Orders',
                    'CHS Fitted Kitchens Orders ACTIONED',
                    'CHS WD Orders',
                    'CHS WD Orders ACTIONED',
                    'Email: COT Outstanding Orders',
                    'Email: COT Stationery Orders',
                    'Email: Customer Orders Hamilton',
                    'Email: Email US',
                    'EHT POS Hamilton',
                    'Email: JLCom International',
                    'Email: Head of Customer Service',
                    'JLCom Front Office',
                    'Email: CST Delivery Enquiries',
                    'Email: CST Customer Assistance',
                    'Email: CST Contact Centre',
                    'Hamilton Finance Admin',
                    'Hamilton Finance Admin ACTIONED',
                    'JLCom Back Office Email Team',
                    'Data Management Office',
                    'Email: JLCom MyJL',
                    'CHS Payments Windows',
                    'Data Management Office ACTIONED',
                    'CSA Hamilton & Didsbury')]);
    }
    
    
    
    global void execute(Database.BatchableContext BC, List<Case> scope)
    {
        //System.debug('Size of batch : '+scope.size());
        List<Group> queueList = [Select Id, Name, DeveloperName, Type from Group where Type = 'Queue'];
        Map<Case,String> caseOwnerUpdateMap = new Map<Case,String>();
        Set<Case> updatedOwnerList = new Set<Case>();
        List<Case> finalCaseList = new List<Case>();
        String newCaseOwner;
        for(Case case1 : scope){
            //System.debug('Case Owner - '+case1.Case_Owner__c+'Case Number - '+case1.CaseNumber);
            newCaseOwner = case1.Case_Owner__c+' FAILED';
            //System.debug('New Owner Name - '+newCaseOwner);
            caseOwnerUpdateMap.put(case1,newCaseOwner);
        }
        updatedOwnerList = caseOwnerUpdateMap.keySet();
        for(Case value1: updatedOwnerList){
            //System.debug('value1 --- '+value1);
            String updatedqueue = caseOwnerUpdateMap.get(value1);
            //System.debug('Updated Name - '+updatedqueue);
            for(Group queuedetail : queueList){
                if((queuedetail.Name).equalsIgnoreCase(updatedqueue)){
                    //System.debug(queuedetail.Name+' '+queuedetail.Id);
                    value1.ownerId = queuedetail.Id;
                    finalCaseList.add(value1);
                }
            }
        }
        List<Database.SaveResult> srList = Database.update(finalCaseList, false);
        if(srList.size()>0){
            for(Integer counter = 0; counter < srList.size(); counter++){
                if (srList[counter].isSuccess()){
                    result++;
                }else{
                    if(srList[counter].getErrors().size()>0){
                        for(Database.Error err : srList[counter].getErrors()){
                            ErrorMap.put(srList.get(counter).id,err.getMessage());
                        }
                    }                                
                }
            }
        }       
    }
    global void finish(Database.BatchableContext BC){
        String textBody = '';
        Set<Id> errorid = new Set<ID>();
        textBody+= result +' cases updated successfully '+'\n';
        if(!ErrorMap.isEmpty()){
            for(Id recordids : ErrorMap.KeySet()){
                errorid.add(recordids);
            }
            textBody+='Error Log: '+errorid+'\n';
        }
        EmailNotifier.sendNotificationEmail('Batch Executed Successfully', textbody);
        OPSTempActionToDeafultQueue movecasefromActtiDef = new OPSTempActionToDeafultQueue();
        Database.executeBatch(movecasefromActtiDef,50);
    }
}