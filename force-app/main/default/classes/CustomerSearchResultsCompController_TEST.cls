@isTest
public class CustomerSearchResultsCompController_TEST {

     @isTest 
     static void shouldSetTheTotalrecordsCorrectlyWhenContactlistisNotnull(){
          User sysAdmin       = UnitTestDataFactory.getSystemAdministrator('admin1');
          User backOfficeUser   = UnitTestDataFactory.getBackOfficeUser('bo', 'JL.com - Bookings');
            
          insert sysAdmin;  
          insert backOfficeUser;
        
          System.runAs(sysAdmin) {
            UnitTestDataFactory.initialiseContactCentreAndTeamSettings();   
            jl_runvalidations__c jrv = new jl_runvalidations__c(Name = backOfficeUser.Id, Run_Validations__c = true);
            insert jrv;             
         }
        
         List<Contact> contactList = new List<Contact>();
         System.runAs(backOfficeUser) {
          
           for (Integer i = 1; i <= 10; i++) {
             contactList .add(UnitTestDataFactory.createContact(i, false));             
           }
           insert contactList ;
         }  
           CustomerSearchResultsCompController  cont = new CustomerSearchResultsCompController ();
           Test.startTest();               
               cont.ContactCollection =  contactList ;
           Test.stopTest();  
           System.assertEquals(10, cont.totalRecordCount); 
         
     }
     
     @isTest 
     static void shouldSetTheTotalrecordsCorrectlyWhenContactlistisNull(){
          User sysAdmin       = UnitTestDataFactory.getSystemAdministrator('admin1');
          User backOfficeUser   = UnitTestDataFactory.getBackOfficeUser('bo', 'JL.com - Bookings');
            
          insert sysAdmin;  
          insert backOfficeUser;
        
          System.runAs(sysAdmin) {
            UnitTestDataFactory.initialiseContactCentreAndTeamSettings();   
            jl_runvalidations__c jrv = new jl_runvalidations__c(Name = backOfficeUser.Id, Run_Validations__c = true);
            insert jrv;             
         }
        
         
           CustomerSearchResultsCompController  cont = new CustomerSearchResultsCompController ();
           Test.startTest();               
               cont.ContactCollection =  null;
           Test.stopTest();  
           System.assertEquals(0, cont.totalRecordCount, 'Record count must not be set' ); 
         
     }
     
     @isTest 
     static void getContactsWhichWereSetfromCollection(){
          User sysAdmin       = UnitTestDataFactory.getSystemAdministrator('admin1');
          User backOfficeUser   = UnitTestDataFactory.getBackOfficeUser('bo', 'JL.com - Bookings');
            
          insert sysAdmin;  
          insert backOfficeUser;
        
          System.runAs(sysAdmin) {
            UnitTestDataFactory.initialiseContactCentreAndTeamSettings();   
            jl_runvalidations__c jrv = new jl_runvalidations__c(Name = backOfficeUser.Id, Run_Validations__c = true);
            insert jrv;             
         }
        
         List<Contact> contactList = new List<Contact>();
         System.runAs(backOfficeUser) {
          
           for (Integer i = 1; i <= 10; i++) {
             contactList .add(UnitTestDataFactory.createContact(i, false));             
           }
           insert contactList ;
         }  
           CustomerSearchResultsCompController  cont = new CustomerSearchResultsCompController ();
           cont.ContactCollection =  contactList ;
           List<CustomerSearchResultsCompController.RowItem> rowItems = null;
           Test.startTest();               
               rowItems  = cont.getContacts();
           Test.stopTest();  
           System.assertEquals(10, rowItems.size()); 
           System.assertEquals(1, cont.getTotalPages());
         
     }
     
     
     
}