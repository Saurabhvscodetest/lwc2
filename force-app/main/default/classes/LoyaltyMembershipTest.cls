@isTest
private class LoyaltyMembershipTest {
	private static final String SHOPPER_ID = '1234abcdefg';
	private static final String LOYALTY_MEMBERSHIP_NUMBER = '34567894561';
    
    Private static final Contact customer;
    Private static final Case cs;
    static{
    BaseTriggerHandler.addSkipReason(CalloutHandler.SKIP_ALL_NOTIFICATIONS_CODE, 'disable outbound events in this test class');
    }
    @testSetup
    static void setup(){
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        
        final List<Loyalty_Account__c> loyaltyAccounts = new List<Loyalty_Account__c>();
		loyaltyAccounts.add(new Loyalty_Account__c(Name = LOYALTY_MEMBERSHIP_NUMBER ,ShopperId__c=SHOPPER_ID,IsActive__c = true,Membership_Number__c = LOYALTY_MEMBERSHIP_NUMBER));
		insert loyaltyAccounts;
        
		final List<Loyalty_Card__c> cards = new List<Loyalty_Card__c>();
		cards.add(new Loyalty_Card__c(Name = LOYALTY_MEMBERSHIP_NUMBER,Disabled__c = false ,card_type__c = MyJL_Const.MY_JL_CARD_TYPE, Loyalty_Account__c = loyaltyAccounts[0].Id));
		Database.insert(cards);
        
    }
        
  
    @isTest static void testServiceWithNoPartnershipCardNumber() {

		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = '/LoyaltyMembership/RequestCard/' + SHOPPER_ID;
		req.httpMethod = 'PUT';

		Test.startTest();
		LoyaltyMembership.ResponseWrapper rs = LoyaltyMembership.getCardByLoyaltyMembership(null);
		Test.stopTest();
		
        
        integer statusCode = res.statusCode;
        System.assertEquals(400, statusCode);
        System.assertEquals('MISSING_LOYALTY_NUMBER',rs.errorCode);
        System.assertEquals('loyaltyNumber: is not provided',rs.errorMessage);
        
		
	}

	
	@isTest static void testServiceWithNoShopperProvided() {

		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = '/LoyaltyMembership/RequestCard/';
		req.httpMethod = 'PUT';

		Test.startTest();
		LoyaltyMembership.ResponseWrapper rs = LoyaltyMembership.getCardByLoyaltyMembership(LOYALTY_MEMBERSHIP_NUMBER );
		Test.stopTest();
        
		integer statusCode = res.statusCode;
        System.assertEquals(400, statusCode);
        System.assertEquals('MISSING_JL_SHOPPER_ID',rs.errorCode);
        System.assertEquals('shopperId: is not provided',rs.errorMessage);
		
	}

	@isTest static void testServiceWithDeactivatedAccount() {

		Loyalty_Account__c account = [select IsActive__c From Loyalty_Account__c where ShopperId__c = :SHOPPER_ID Limit 1];

		if( account != null){
			account.IsActive__c = false;
			update account;
		}

		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = '/LoyaltyMembership/RequestCard/' + SHOPPER_ID;
		req.httpMethod = 'PUT';

		Test.startTest();
		LoyaltyMembership.ResponseWrapper rs = LoyaltyMembership.getCardByLoyaltyMembership(LOYALTY_MEMBERSHIP_NUMBER );
		Test.stopTest();
		
        integer statusCode = res.statusCode;
   
	}
   
    @isTest static void testServiceWithDisabledCard() {
		List<Loyalty_Account__c> accountList = [select id From Loyalty_Account__c where ShopperId__c = :SHOPPER_ID Limit 1];
        for(Loyalty_Account__c lc : accountList){
            lc.Membership_Number__c = LOYALTY_MEMBERSHIP_NUMBER;
        }
        update accountList;
        
		Loyalty_Card__c card = [Select id,Disabled__c, card_type__c from Loyalty_Card__c where Name =:LOYALTY_MEMBERSHIP_NUMBER Limit 1];
        
		if( card != null){
			card.Disabled__c = true;
			 update card;      
		}
        
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = '/LoyaltyMembership/RequestCard/' + SHOPPER_ID;
		req.httpMethod = 'PUT';

		Test.startTest();
		LoyaltyMembership.ResponseWrapper rs = LoyaltyMembership.getCardByLoyaltyMembership(LOYALTY_MEMBERSHIP_NUMBER);
		Test.stopTest();
		
        integer statusCode = res.statusCode;
      

	}

	@isTest static void testServiceWithShopper() {
        
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = '/LoyaltyMembership/RequestCard/' + SHOPPER_ID;
		req.httpMethod = 'PUT';

		Test.startTest();
        Contact customer = MyJL_TestUtil.createCustomer('Ordering_Customer');
        customer.Shopper_ID__c = SHOPPER_ID;
        update customer;
        
        MyJL_Outbound_Staging__c JL = new MyJL_Outbound_Staging__c();
		LoyaltyMembership.ResponseWrapper rs = LoyaltyMembership.getCardByLoyaltyMembership(LOYALTY_MEMBERSHIP_NUMBER);
        List<Case> getcase = new list<Case>([SELECT id,despatch_status__c,status,Address_Line_1__c,Address_Line_2__c, Address_Line_3__c,Address_Line_4__c  FROM Case]);        
        List<MyJL_Outbound_Staging__c> getOutst = new list<MyJL_Outbound_Staging__c>([select id,Case_Reference_ID__c,Address_LIne_1__c, Address_Line_2__c, Address_Line_3__c, Address_Line_4__c  from MyJL_Outbound_Staging__c where Case_Reference_ID__c =:getcase[0].Id]);
        
       
       
		system.assert(!getcase.isEmpty(), 'Case should be created' );
		Case actualCase = getcase[0];        
        system.assertequals('Closed - No Response Required',actualCase.status); 
        system.assertequals(customer.Mailing_Street__c,actualCase.Address_Line_1__c); 
        system.assertequals(customer.Mailing_Address_Line2__c,actualCase.Address_Line_2__c); 
        system.assertequals(customer.Mailing_Address_Line3__c,actualCase.Address_Line_3__c); 
        system.assertequals(customer.Mailing_Address_Line4__c,actualCase.Address_Line_4__c); 
        system.assertequals('Processing',actualCase.despatch_status__c); 
        
        system.assertequals(1,getOutst.size());
        MyJL_Outbound_Staging__c outboundStagingRecord = getOutst[0]; 
        
        system.assertequals(customer.Mailing_Street__c,outboundStagingRecord.Address_Line_1__c); 
        system.assertequals(customer.Mailing_Address_Line2__c,outboundStagingRecord.Address_Line_2__c); 
        system.assertequals(customer.Mailing_Address_Line3__c,outboundStagingRecord.Address_Line_3__c); 
        system.assertequals(customer.Mailing_Address_Line4__c,outboundStagingRecord.Address_Line_4__c); 
  
		Test.stopTest();

	}

	@isTest static void testServiceForCatchBlock() {

		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = null;
		req.httpMethod = 'PUT';

		Test.startTest();
		LoyaltyMembership.ResponseWrapper rs = LoyaltyMembership.getCardByLoyaltyMembership(LOYALTY_MEMBERSHIP_NUMBER );
		Test.stopTest();

        integer statusCode = res.statusCode;
        System.assertEquals(500, statusCode);
        System.assertEquals('CONNEX_SERVER_ERROR',rs.errorCode);
	}
    @isTest static void testServiceWithEmptyAccount() {

		Loyalty_Account__c account = [select IsActive__c From Loyalty_Account__c where ShopperId__c = :SHOPPER_ID Limit 1];

		if( account != null){
			account.IsActive__c = false;
			update account;
		}
			RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = '/LoyaltyMembership/RequestCard/' + SHOPPER_ID;
		req.httpMethod = 'PUT';

		Test.startTest();
		LoyaltyMembership.ResponseWrapper rs = LoyaltyMembership.getCardByLoyaltyMembership(LOYALTY_MEMBERSHIP_NUMBER );
		Test.stopTest();
		
        integer statusCode = res.statusCode;
      	
	
	}
    @isTest static void testServiceWithMultipleCard() {
		      
		Loyalty_Card__c card = [Select id,Disabled__c, card_type__c from Loyalty_Card__c where Name =:LOYALTY_MEMBERSHIP_NUMBER Limit 1];
        
		if( card != null && card.Card_Type__c == 'MyJL_Const.MY_PARTNERSHIP_CARD_TYPE'){
			card.Disabled__c = true;
			 update card;      
		}
        
		RestRequest req = new RestRequest(); 
		RestResponse res = new RestResponse();

		RestContext.request = req;
		RestContext.response = res;

		req.requestURI = '/LoyaltyMembership/RequestCard/' + SHOPPER_ID;
		req.httpMethod = 'PUT';

		Test.startTest();
		LoyaltyMembership.ResponseWrapper rs = LoyaltyMembership.getCardByLoyaltyMembership(LOYALTY_MEMBERSHIP_NUMBER);
		Test.stopTest();
		
        integer statusCode = res.statusCode;
      

	}
    
    
}