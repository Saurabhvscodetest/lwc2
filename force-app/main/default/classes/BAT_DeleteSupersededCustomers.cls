/* Description  : GDPR - Superseded Customer Data Deletion
*
* Modification Log
=============================================================================================
* Ver     Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   03/01/2019        Vijay Ambati                       Created              COPT-4270
*
*/

global class BAT_DeleteSupersededCustomers implements Database.Batchable<sobject>,Database.Stateful
{
    private static final String ConRecType='Superseded';
    global Map<Id,String> ErrorMap = new Map<Id,String>();
    global integer Counter=0;
	global String query;
    private static final DateTime Prior_Date_Months = System.now().addMonths(-2);
    global Database.QueryLocator start(Database.BatchableContext BC){
    GDPR_Record_Limit__c gDPRLimit = GDPR_Record_Limit__c.getValues('BAT_DeleteSupersededCustomers');
    if(gDPRLimit.Record_Limit__c != NULL){
	String recordLimit = gDPRLimit.Record_Limit__c;
    String limitSpace = ' ' + 'Limit' + ' '; 
	query = 'SELECT Id FROM Contact where RecordType.Name =: ConRecType AND LastModifiedDate <: Prior_Date_Months' + limitSpace + recordLimit;
        }
        else{
            query = 'SELECT Id FROM Contact where RecordType.Name =: ConRecType AND LastModifiedDate <: Prior_Date_Months';
        }
        return Database.getQueryLocator(query);
    }
     global void execute(Database.BatchableContext BC, List<Contact> scope)
    {
            try{
                List<Id> recordstoDelete = new List<Id>();
                for(Contact recordScope : scope){
                recordstoDelete.add(recordScope.id);
                }
                list<Database.deleteResult> srList = Database.delete(recordstoDelete, false);
                for(Integer result = 0; result < srList.size(); result++) {
                    if (srList[result].isSuccess()) {
                        Counter++;
                    }else {
                        for(Database.Error err : srList[result].getErrors()) {
                            ErrorMap.put(srList.get(result).id,err.getMessage());
                        }
                    }
                }
                /* Delete records from Recyclebin */
                Database.emptyRecycleBin(scope);
            }
            Catch(exception e){
                EmailNotifier.sendNotificationEmail('Exception from BAT_DeleteSupersededCustomers', e.getMessage()); 
            }  
    }
    
    global void finish(Database.BatchableContext BC)
    {		  
        String textBody ='';
        set<Id> allIds = new set<Id>();
        textBody += Counter +' Superseded records deleted in object Contact'+'\n';
        if (!ErrorMap.isEmpty()){
            for (Id recordids : ErrorMap.KeySet())
            {
                allIds.add(recordids);
            } 
            textBody+= 'Error log: '+allIds+'\n';
        }
        EmailNotifier.sendNotificationEmail('GDPR - Superseded Customer Data Deletion',textBody);   
    }
    
    
}