/* Description  : GDPR - Decouple Accounts from Cases
*
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   28/03/2019      Vijay Ambati                        Created             COPT-4432
*
*/

@IsTest
Public class BAT_DecoupleAccountsFromCasesTest {
    
    @IsTest
    Public static void testCreateCase(){
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        Case testCase;
        Contact testContact;
        System.runAs(runningUser) 
        {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;       
            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        System.runAs(testUser) 
        {
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;            
            List<Case> cList = new List<Case>(); 
            for(integer i=0;i<10;i++){  
                cList.add(new Case(Bypass_Standard_Assignment_Rules__c = True,ContactId = testContact.Id));
            }
            GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
            gDPR.GDPR_Class_Name__c = 'BAT_DecoupleAccountsFromCases';
            gDPR.Name = 'BAT_DecoupleAccountsFromCases';
            gDPR.Record_Limit__c = '5000';            
            try{
                Insert cList;
                Insert gDPR;
                system.assertEquals(10, cList.size());
            }
            Catch(Exception e){
                system.debug('Exception Caught:'+e.getmessage());
            }            
            Database.BatchableContext BC;
            BAT_DecoupleAccountsFromCases obj = new BAT_DecoupleAccountsFromCases();
            Test.startTest();
            obj.start(BC);
            obj.execute(BC,cList); 
            obj.finish(BC);
            Test.stopTest();   
        }
    }
    @IsTest
    Public static void testExceptionMethod(){
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        User testUser;
        Case testCase;
        Contact testContact;
        System.runAs(runningUser) 
        {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;       
            insert CustomSettingTestDataFactory.createConfigSettings();
        }
        System.runAs(testUser) 
        {
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;
            List<Case> cList = new List<Case>(); 
            for(integer i=0;i<10;i++){                
                cList.add(new Case(Bypass_Standard_Assignment_Rules__c = True,ContactId = testContact.Id));
            }
            GDPR_Record_Limit__c gDPR = new GDPR_Record_Limit__c();
            gDPR.GDPR_Class_Name__c = 'BAT_DecoupleAccountsFromCases';
            gDPR.Name = 'BAT_DecoupleAccountsFromCases';
            gDPR.Record_Limit__c = '';
            
            try{
                Insert cList;
                Insert gDPR;
                system.assertEquals(10, cList.size());
            }
            Catch(Exception e){
                system.debug('Exception Caught:'+e.getmessage());
            }
            Database.BatchableContext BC;
            BAT_DecoupleAccountsFromCases obj = new BAT_DecoupleAccountsFromCases();
            Test.startTest();
            obj.start(BC);
            Database.DeleteResult[] Delete_Result = Database.delete(cList, false);
            obj.execute(BC,cList); 
            obj.finish(BC);
            Test.stopTest();  
            
        }        
    }
    
}