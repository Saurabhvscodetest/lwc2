public class ViewCaseResultController {
	
    @AuraEnabled 
    public static List < Case > fetchCaseRecordList ( String searchTerm ) {        
        if (searchTerm!='') {
            String caseSOQL = 'SELECT Id, CaseNumber, Contact.Name, Case_Owner__c, jl_Case_Type__c, LastViewedDate From Case WHERE CaseNumber LIKE \'%' + searchTerm + '%\' LIMIT 30';
        	return Database.query( caseSOQL );
        }
        else {
        	String caseSOQL = 'SELECT Id, CaseNumber, Contact.Name, Case_Owner__c, jl_Case_Type__c, LastViewedDate FROM Case WHERE LastViewedDate != NULL AND CaseNumber LIKE \'%' + searchTerm + '%\' ORDER BY LastViewedDate DESC LIMIT 30';
        	return Database.query( caseSOQL );
        }
    }
}