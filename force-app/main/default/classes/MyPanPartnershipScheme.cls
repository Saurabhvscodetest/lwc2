@RestResource(urlmapping='/MyPanPartnershipScheme/RequestCard/*')
global class MyPanPartnershipScheme {
    
    @Httpput
    global static ResponseWrapper getPanPartnershipCard(String partnershipCardNumber){
        String SERVICE_NAME_FOR_ERROR = 'My Pan Partnership Card Rest Service';
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;	
        ResponseWrapper responseJSON = new ResponseWrapper();
        
        try{
            
            string shopperId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);

            
            if( String.isEmpty(partnershipCardNumber)){
                res.statusCode = 400;
                responseJSON.errorCode = 'MISSING_PARTNERSHIP_NUMBER';
                responseJSON.errorMessage = 'partnershipCardNumber: is not provided';
                MyJL_Util.logRejection(MyJL_Const.ERRORCODE.MY_PARTNERSHIP_MISSING_DATA, '', '', SERVICE_NAME_FOR_ERROR);
            }
            
            else if( String.isEmpty(shopperId)){
                res.statusCode = 400;
                responseJSON.errorCode = 'MISSING_JL_SHOPPER_ID';
                responseJSON.errorMessage = 'shopperId: is not provided';
                MyJL_Util.logRejection(MyJL_Const.ERRORCODE.CUSTOMERID_NOT_FOUND, '', shopperId, SERVICE_NAME_FOR_ERROR);
            }
            else {
                List<Loyalty_Account__c> accounts = MyLoyaltyServiceHelper.getLoyaltyAccountsWithShopperId(shopperId);
                
                if(accounts.isEmpty())
                {
					res.statusCode = 404;
                    responseJSON.errorCode = 'NO_LAYOUT_ACCOUNTS_FOUND';
                    responseJSON.errorMessage = 'No Loyalty accounts found for shopperId';   
                    MyJL_Util.logRejection(MyJL_Const.ERRORCODE.LOYALTY_ACCOUNT_NOT_FOUND, '', shopperId,SERVICE_NAME_FOR_ERROR);
                    //return;
                }
                else{ 
                    for(Loyalty_Account__c a : accounts){
                        List<Loyalty_Card__c> cards = MyLoyaltyServiceHelper.getLoyaltyCardWithLoyaltyAccount(a.Id,partnershipCardNumber);
                        if(!a.IsActive__c){ 
                            res.statusCode = 409;
                            responseJSON.errorCode = 'ACCOUNT_NOT_ACTIVE'; // 409
                            responseJSON.errorMessage = 'Account is not active';
                            MyJL_Util.logRejection(MyJL_Const.ERRORCODE.NO_ACTIVE_LOYALTY_ACCOUNT_FOUND, '', shopperId,SERVICE_NAME_FOR_ERROR);
                        }
                        else if(a.Membership_Number__c != partnershipCardNumber){
                            res.statusCode = 409;
                            responseJSON.errorCode = 'PARTNERSHIP_NUMBER_NOT_ASSOCIATED_WITH_SHOPPER_ID'; //409
                            responseJSON.errorMessage = 'Partnership Card Number is not associated to shopperId';
                            MyJL_Util.logRejection(MyJL_Const.ERRORCODE.NO_ACTIVE_LOYALTY_ACCOUNT_FOUND, '', shopperId,SERVICE_NAME_FOR_ERROR);   
                        }    
                        else if(cards[0].Disabled__c == true){
                            res.statusCode = 409;
                            responseJSON.errorCode = 'LOYALTY_CARD_IS_NOT_ACTIVE'; //409
                            responseJSON.errorMessage = 'Loyalty Card is not active';
                            MyJL_Util.logRejection(MyJL_Const.ERRORCODE.NO_ACTIVE_LOYALTY_ACCOUNT_FOUND, '', shopperId,SERVICE_NAME_FOR_ERROR);   
                        }
                        else{
                            //fetching contactdetails through Shopper ID
                            Contact  cpforStaging = [select Id, Name,Customer_Group_Name__c, birthdate,
                                                     firstname, lastname, salutation, SourceSystem__c, 
                                                     mailingstreet, otherstreet,Mailing_House_No_Text__c,Mailing_House_Name__c,
                                                     mailingcity, othercity, mailingstate, otherstate,
                                                     mailingcountry, othercountry, mailingpostalcode, otherpostalcode,
                                                     title, mobilephone, email, AnonymousFlag2__c, 
                                                     Mailing_Street__c, Mailing_Address_Line2__c, Mailing_Address_Line3__c, Mailing_Address_Line4__c
                                                     from contact where Shopper_ID__c =:shopperId];
                            
                            System.debug('@@@ case'+cpforStaging);
                            
                            //fetching LoyaltyAccount through Shopper ID
                            Loyalty_Account__c laforstaging =  [select Id, contact__c, Name,
                                                                Activation_Date_Time__c, Channel_ID__c, Customer_Loyalty_Account_Card_ID__c,
                                                                Deactivation_Date_Time__c, Email_Address__c, IsActive__c, LastModifiedById,
                                                                LastModifiedDate, LastModifiedDateMS__c, Registration_Business_Unit_ID__c, Registration_Email_Sent_Flag__c,
                                                                Registration_Workstation_ID__c, Scheme_Joined_Date_Time__c, Welcome_Email_Sent_Flag__c
                                                                from Loyalty_Account__c where ShopperId__c = :shopperId];
                            
                          
                            
                            // For staging
                            String title ='';
                            string initials = '';
                            
                            if(cpforStaging.title!=null){
                                title = cpforStaging.title;
                            }
                            
                            if(cpforStaging.salutation!=null){
                                title = cpforStaging.salutation;
                            }
                            
                            if(cpforStaging.firstname!=null){
                                initials = cpforStaging.firstname.substring(0,1).touppercase();
                            }
                            
                            // Create case 
                            Case cs = new Case();
                            // need to check for record type
                            
                            id myjlrecordtypeid = [select id from recordtype where developername ='myJL_Request' and SobjectType ='Case' limit 1].id;
                            cs.address_Line_1__c = cpforStaging.Mailing_Street__c;
                            cs.address_line_2__c =   cpforStaging.Mailing_Address_Line2__c;  
                            cs.address_line_3__c =   cpforStaging.Mailing_Address_Line3__c;       
                            cs.address_line_4__c =   cpforStaging.Mailing_Address_Line4__c;  
                            cs.Mailing_City__c =     cpforStaging.mailingcity;          
                            cs.Mailing_Country__c = cpforStaging.mailingcountry;
                            cs.Mailing_County__c =   cpforStaging.mailingstate;
                            cs.Post_Code__c =        cpforStaging.mailingpostalcode;    
                            cs.myJL_Membership_Number__c =     laforstaging.name;
                            cs.MYJL_Barcode_Number__c =        laforstaging.Customer_Loyalty_Account_Card_ID__c;
                            cs.recordtypeid =   myjlrecordtypeid;   
                            cs.status =    'Closed - No Response Required';
                            cs.Contactid =      cpforStaging.Id;              
                            cs.Despatch_Status__c =  'Processing';      
                            cs.description = 'mY JL Request'; 
                            cs.Request_Type__c = 'Pan Partnership card';
                            cs.Fast_track__c = False;
							cs.Bypass_Standard_Assignment_Rules__c = true;
                            
                           
                            insert cs;
                            
                            Case caseforStaging = [select id,owner.firstname,Integration_Ref_Holder__c,MyJL_Request_Record_Locked__c,owner.lastname,jl_Authorised_Person_Comments__c,
                                                   Mailing_City__c,Mailing_Country__c,Mailing_County__c,fast_track__c,Request_Type__c from case where id =:cs.Id];
                            
                            System.debug('@@@ case'+caseforStaging);
                            
                            //insert record in staging table for Abinitio to pick up                                       
                            MyJL_Outbound_Staging__c insertMyjstage = new MyJL_Outbound_Staging__c();
                            insertMyjstage.Activation_Date_Time__c = laforstaging.Activation_Date_Time__c; 
                            insertMyjstage.Address_LIne_1__c = cpforStaging.Mailing_Street__c;  
                            insertMyjstage.Address_Line_2__c = cpforStaging.Mailing_Address_Line2__c; 
                            insertMyjstage.Address_Line_3__c = cpforStaging.Mailing_Address_Line3__c; 
                            insertMyjstage.Address_Line_4__c = cpforStaging.Mailing_Address_Line4__c;
                            insertMyjstage.Address_Line_5__c = cpforStaging.Mailing_House_No_Text__c;
                            insertMyjstage.Address_Line_6__c = cpforStaging.Mailing_House_Name__c; 
                            insertMyjstage.Agent_Id__c = caseforStaging.owner.firstname +' '+caseforStaging.owner.lastname;
                            insertMyjstage.Case_Reference_ID__c = cs.Id; 
                            insertMyjstage.Case_Request_Cancelled__c = false; //
                            insertMyjstage.Change__c = ''; 
                            insertMyjstage.Comments__c = caseforStaging.jl_Authorised_Person_Comments__c;
                            insertMyjstage.County__c = cpforStaging.mailingstate;  
                            insertMyjstage.Created_Date_Holder__c = datetime.now().format('YYYY-MM-dd HH:mm:ss.SSS','UTC')+'Z'; //cwr
                            insertMyjstage.Deactivation_Date_Time__c = laforstaging.Deactivation_Date_Time__c;
                            insertMyjstage.Email_Address__c = cpforStaging.email;
                            insertMyjstage.Fast_track__c = caseforStaging.fast_track__c;
                            insertMyjstage.First_Name__c = cpforStaging.firstname; 
                            insertMyjstage.Initials__c = initials; 
                            insertMyjstage.Is_Active__c = laforstaging.IsActive__c;    
                            insertMyjstage.Last_Modified_Date_Holder__c = insertMyjstage.Created_Date_Holder__c;
                            insertMyjstage.Last_Name__c = cpforStaging.lastname;
                            insertMyjstage.MYJL_Barcode_NUmber__c = laforstaging.Customer_Loyalty_Account_Card_ID__c;
                            insertMyjstage.MYJL_Membership_Number__c = laforstaging.name;
                            insertMyjstage.Post_Code__c = cpforStaging.mailingpostalcode; 
                            insertMyjstage.Request_Type__c = caseforStaging.Request_Type__c;
                            insertMyjstage.Scheme_Joined_Date_Time__c = laforstaging.Scheme_Joined_Date_Time__c;
                            insertMyjstage.Shopper_ID__c = shopperId;    
                            insertMyjstage.Title__c = title;  
                            insertMyjstage.Town__c = cpforStaging.mailingcity;
                            
                            Insert insertMyjstage;
                            
                            //Update the case with outbound staging reference
                            caseforStaging.Integration_Ref_Holder__c = insertMyjstage.id;
                            caseforStaging.MyJL_Request_Record_Locked__c = true;
                            update caseforStaging;
                            
                            
                            // default success JSON 
                            res.statusCode = 200;
                           
                        }
                    }
                }
                
            }
            
        }catch(Exception e){
            res.statusCode = 500;
            responseJSON.errorCode = 'CONNEX_SERVER_ERROR';
            responseJSON.errorMessage = e.getMessage();
            MyJL_Util.logRejection(MyJL_Const.ERRORCODE.UNHANDLED_EXCEPTION, '', '',SERVICE_NAME_FOR_ERROR);
        }
        
        return responseJSON;
    }
    
    
    global class ResponseWrapper {
        
        global String errorCode {get;set;} 
        global String errorMessage {get;set;}   
        
        global ResponseWrapper() {
            this.errorCode = 'OK';
            this.errorMessage = 'Success';
        }
    }
    
}