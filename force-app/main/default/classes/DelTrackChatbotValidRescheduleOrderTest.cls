/*
* @File Name   : DelTrackChatbotValidRescheduleOrderTest 
* @Description : Test Class to related to MyJLChatBot Delivery Tracking functionalities  
* @Copyright   : Zensar
* @Jira #      : #5174
----------------------------------------------------------------------------------------------------------------
Ver           Date               Author                     Modification
---           ----               ------                     ------------
*   1.0      18-MAR-20          Vijay A                       Created
*/
@IsTest
Public class DelTrackChatbotValidRescheduleOrderTest {

@IsTest
    Public Static void testMethodForDelTrackChatBotProcess(){
        
        DelTrackChatbotValidRescheduleOrder.DelTrackChatBotRequest delRequest = new DelTrackChatbotValidRescheduleOrder.DelTrackChatBotRequest();
        delRequest.orderNumberInput = '213456789';
        List<DelTrackChatbotValidRescheduleOrder.DelTrackChatBotRequest> listDelRequest = new List<DelTrackChatbotValidRescheduleOrder.DelTrackChatBotRequest>();
        listDelRequest.add(delRequest);
               
        DelTrackChatbotValidRescheduleOrder.DelTrackChatBotProcess(listDelRequest);

    }

@IsTest
    Public Static void testMethodForDelTrackChatBotProcessNew(){

        DelTrackChatbotValidRescheduleOrder.DelTrackChatBotRequest delRequest1 = new DelTrackChatbotValidRescheduleOrder.DelTrackChatBotRequest();
        delRequest1.orderNumberInput = 'I dont have a valid number';
        List<DelTrackChatbotValidRescheduleOrder.DelTrackChatBotRequest> listDelRequest1 = new List<DelTrackChatbotValidRescheduleOrder.DelTrackChatBotRequest>();
        listDelRequest1.add(delRequest1);

        DelTrackChatbotValidRescheduleOrder.DelTrackChatBotProcess(listDelRequest1);

    }
    
    
@IsTest
    Public Static void testMethodForDelTrackChatBotProcessException(){

        DelTrackChatbotValidRescheduleOrder.DelTrackChatBotRequest delRequest1 = new DelTrackChatbotValidRescheduleOrder.DelTrackChatBotRequest();
        
        List<DelTrackChatbotValidRescheduleOrder.DelTrackChatBotRequest> listDelRequest1 = new List<DelTrackChatbotValidRescheduleOrder.DelTrackChatBotRequest>();
        listDelRequest1.add(delRequest1);

        DelTrackChatbotValidRescheduleOrder.DelTrackChatBotProcess(listDelRequest1);

    }

}