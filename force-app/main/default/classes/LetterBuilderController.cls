public with sharing class LetterBuilderController {
	
	private static final String DATE_FORMAT = 'YYYY-MM-dd HH:mm';
	
	private String caseRecordId {get; set;}

	public Case caseRecord {get; set;}
	public Contact contactRecord {get; set;}
	public ContentVersion cv { get; set; }
	
	public String customerName {get; set;}
	
	public String addressArea {get; set;}
	
	public String letterBody {get; set;}
	
	public LetterBuilderController(){	
	}
	
	public LetterBuilderController(ApexPages.StandardController stdController){
		
		caseRecordId = stdController.getId();
		caseRecord = (Case) stdController.getRecord();
		if(caseRecordId == NULL){
			caseRecordId = ApexPages.currentPage().getParameters().get('caseId');
		}
        caseRecord = [SELECT Id, CaseNumber FROM Case WHERE Id = :caseRecordId LIMIT 1];
		// prepopulate address and contact information
		List<Contact> contactList = [SELECT Id, FirstName, LastName, Salutation, MailingState, MailingStreet, MailingPostalCode, MailingCity, MailingCountry FROM Contact WHERE Id IN (SELECT ContactId FROM Case WHERE Id=:caseRecordId)];
		if(contactList.size() != 1){
			// don't prepopulate the fields, but leave the option to write a letter manually
			return;
		}
		contactRecord = contactList.iterator().next();
		
		customerName = generateCustomerName(contactRecord);
		
		addressArea = String.isBlank(customerName) ? '' : (customerName + '\n');
		addressArea += String.isBlank(contactRecord.MailingStreet) ? '' : (contactRecord.MailingStreet + '\n');
		addressArea += String.isBlank(contactRecord.MailingCity) ? '' : (contactRecord.MailingCity + '\n');
		addressArea += String.isBlank(contactRecord.MailingState) ? '' : (contactRecord.MailingState + '\n');
		addressArea += String.isBlank(contactRecord.MailingPostalCode) ? '' : (contactRecord.MailingPostalCode);
		
		letterBody = 'Dear ' + (String.isEmpty(contactRecord.Salutation) ? '' : (contactRecord.Salutation+ ' '))  + contactRecord.LastName;
	}
	
	public PageReference generateLetter(){
		cv = NEW ContentVersion(Title ='Letter.pdf');
		PageReference letterPage = Page.GeneratingLetterPage;
		letterPage.getParameters().put('id',caseRecordId);
		letterPage.setRedirect(false);
		return letterPage;
	}
	
	public PageReference attachPdfToCase(){
		cv = NEW ContentVersion();
        PageReference letterPage = Page.GeneratingLetterPage;
        letterPage.getParameters().put('id',caseRecordId);
        cv.Title = 'Letter (' + System.now().format(DATE_FORMAT) + ').pdf';
        cv.PathOnClient = 'S';
        cv.Origin = 'H';
        cv.FirstPublishLocationId = caseRecordId;
        if(Test.isRunningTest()){
            cv.VersionData = Blob.valueOf('UNIT TEST');
        } else {
            cv.VersionData = letterPage.getContent();
        }
        insert cv;
		
		PageReference pref = Page.GenerateLetterRedirectPage;
		pref.getParameters().put('caseName', caseRecord.CaseNumber);
		pref.getParameters().put('caseId', caseRecordId);
		return pref;
	}
	
	public List<String> getAddressLines() {
		if (addressArea == null) {
			return new List<String>();
		}
		List<String> addrLines = addressArea.split('\n');
		Integer addrSize = addrLines.size();
		for(Integer i = addrSize; i<6; i++){
			addrLines.add('');
		}
		return addrLines;
	}
	
	public List<String> getLetterLines() {
		if (letterBody == null) {
			return new List<String>();
		}
		return letterBody.split('\n');
	}
	
	public PageReference cancel(){
		return new PageReference('/' + caseRecordId);
	}
	
	private String generateCustomerName(Contact customer){
		String customerName = '';
		
		if(customer != null){
			customerName += String.isEmpty(contactRecord.Salutation) ? '' : (customer.Salutation + ' ');
			customerName += String.isEmpty(contactRecord.FirstName) ? '' : (contactRecord.FirstName.substring(0,1) + ' ');
			customerName += contactRecord.LastName;
			customerName = customerName.trim();
		} 
		
		return customerName;
		
	}

}