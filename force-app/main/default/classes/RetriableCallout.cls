/**
 *	@author: Andrey Gavrikov (westbrook)
 *	@date: 2014-08-29
 *	@description:
 *	    part of Callout with "Retry" functionality
 *	    @see also CalloutHandler
 *
	Every time when we need to make a call-out
	Instead of doing a call out - record: 
	- Log Header - with service name (the one to invoke for the call out) & Status = "New"
	- call out details - 1 detail record per Id of the record involved in a call out
	--- in addition to that every detail record contains serialised version of SObject involved in the callout
	- finally update "Log Header" with "Do Callout" flag

	Trigger on "Log Header" fires every time when it is updated and "Do Callout" is TRUE.

	Then following happens:
	1. Uncheck "Do Callout"
	2. Using "service" specified in the "Log Header" we invoke appropriate callout handler
	3. Callout Handler de-serialise all relevant detail records and makes actual callout
	If callout is successful then 
	- mark "Log Header" as "Done"

	If callout failed then depending on the type of failure
	- SOFT failure (e.g. Connection timed out): 
	---- change "Log Header" Status to "Retry"
	---- Set "Next Attempt Time" to the required value (every service can have its own retry interval)
	---- Set "Activate Workflow" Flag to True. 
		This flag is picked up by a workflow with time based trigger which
		(upon specified timeout) sets "Do Callout" to True and process repeats
		from step 1.

	- HARD failure (we can define what these are):
	---- Set "Callout Log Header" status to "Hard fail". STOP.
 *	
 *	Version History :   
 *	2014-08-29 - MJL-754 - AG
 *	initial version
 *		
 */
public interface RetriableCallout extends Serialisable {
	String serialise (final Object obj);
	Object deserialise (final String val);

	void setEndpoint(final String url);
	void setTimeout(final Integer mills);
	void setTopic(final String topic);
	void setClientCertificateName(final String uniqueName);

	void callService(final List<Object> objects);

}