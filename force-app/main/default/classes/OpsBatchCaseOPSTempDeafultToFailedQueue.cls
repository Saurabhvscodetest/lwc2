global class OpsBatchCaseOPSTempDeafultToFailedQueue implements Schedulable {

    global void execute(SchedulableContext sc) {

        OPSTempDeafultToFailedQueue batchRun = new OPSTempDeafultToFailedQueue();
    
        // Set scope size to 50;
        database.executebatch(batchRun, 50);
    
    }

}