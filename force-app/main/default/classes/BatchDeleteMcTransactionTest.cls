@isTest
public class BatchDeleteMcTransactionTest {
    @isTest
    Public static void testSingleMcTransactionTest() {
        MC_Transactions__c mc = new MC_Transactions__c();
        mc.Case_Number__c = 'abc';
        mc.Keyword_ID__c = 'xyz';
        mc.Keyword__c = 'rop';
        mc.Name = 'name1';
        insert mc;
        
        test.startTest();
        BatchDeleteMcTransaction obj = new BatchDeleteMcTransaction();
        Database.executeBatch(obj, 200);
        test.stopTest();
    }
    
   
    @isTest
    Public static void testDmlException_Delete()
    {
        
        list<MC_Transactions__c> mcList = new list<MC_Transactions__c>();
        MC_Transactions__c mcTrans = new MC_Transactions__c();
        mcTrans.Case_Number__c = 'abc';
        mcTrans.Keyword_ID__c = 'xyz';
        mcTrans.Keyword__c = 'rop';
        mcTrans.Name = 'name1';
        mcList.add(mcTrans);
        
        MC_Transactions__c mcTrans2 = new MC_Transactions__c();
        mcTrans2.Case_Number__c = 'abc2';
        mcTrans2.Keyword_ID__c = 'xyz2';
        mcTrans2.Keyword__c = 'rop2';
        mcTrans2.Name = 'name2';
        mcList.add(mcTrans2);
        
        try{
            insert mcList;
            system.assertEquals(2, mcList.size());
        }
        Catch(exception e){
            system.debug('Exception Caught:'+e.getmessage());
        }
        Database.DeleteResult[] Delete_Result = Database.delete(mcList, false);
        DmlException expectedException;
        Test.startTest();
        try { 
            delete mcList;
        }
        catch (DmlException dException) {
            expectedException = dException; 
            system.assertNotEquals(NULL, expectedException);
        }
        Test.stopTest();
    }
    
    @isTest
    Public static void testSchedule(){
        Test.startTest();
        ScheduleBatchDeleteMcTransaction obj = NEW ScheduleBatchDeleteMcTransaction();
        obj.execute(null);  
        Test.stopTest();
    }
}