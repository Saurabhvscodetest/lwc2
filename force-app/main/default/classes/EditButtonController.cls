/**
* @description	Controller for the custom Case Edit Button
* @author		@tomcarman		
*/

public class EditButtonController {

	public Boolean isCaseClosed {get; set;}
	private ApexPages.StandardController standardController;
	private Id recordId;	
	private PageReference editPage;
	private Case caseRecord;


	public EditButtonController(ApexPages.StandardController standardController) {
		this.standardController = standardController;
	}
 

 	/**
 	* @description	Page level validation			
 	* @author		@tomcarman
 	*/

	public PageReference validate() {

		recordId = standardController.getId();
		caseRecord = [SELECT Id, IsClosed FROM Case WHERE Id = :recordId];
		
		editPage = new PageReference('/'+recordId+'/e'+'?retURL=%2F'+recordId);

		if(!caseRecord.IsClosed) {
			return editPage;

		} else {

			ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.WARNING, 'You are trying to edit a closed case, would you like to reopen it?'));
			isCaseClosed = true;
			return null; 
		}
	}


	/**
	* @description	Reopens case, returns either Edit page, or error		
	* @author		@tomcarman
	*/

	public PageReference reopenCase () {

		caseRecord = CaseReopenUtilities.reopenCase(caseRecord.Id);

		if(CaseReopenUtilities.errorMessages.isEmpty()) {

			update caseRecord;
			return editPage;

		} else {

			for(String errorMessage : CaseReopenUtilities.errorMessages) {
				ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, errorMessage));
			}

			return null;
		}

	}

}