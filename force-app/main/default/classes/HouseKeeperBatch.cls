/**
 *	@author: Andrey Gavrikov (westbrook)
 *	@date: 2014-12-03 10:19:07 
 *	@description:
 *	    base class which invokes all "house keeping" jobs
 *
 *	    House keeping jobs are those that are low priority and need to be run
 *	    periodically one after another, to make sure that we consume only 1
 *	    Apex Job Thread for all such jobs at any point in time.
 *	    
 *	    Example: Log Cleaner and Transaction Cleaner jobs - they are not important jobs, but need
 *	    to be run every day to keep Log_Header__c and Transaction_Header__c objects from growing too much
 *	    There is also no need to run these processes concurrently.
 *	
 *		==== Using HouseKeeperBatch as a developer ====
 *		In order to become a part of House keeping process you need to
 *		1. Create a class which implements HouseKeeperBatch.Processor
 *		2. Add new record to House_Keeping_Config__c custom settings
 *			making sure that getName() of your implementation of
 *			HouseKeeperBatch.Processor returns the same name as you write in
 *			House_Keeping_Config__c.Name field
 *
 *		=== INVOKING HouseKeeperBatch MANUALLY ===
 *
 *		To start HouseKeeperBatch sequence of all configured House Keeping jobs manually run: 
 *		Database.executeBatch(new HouseKeeperBatch(null, false));
 *
 *		To start from specific job:
 *		Database.executeBatch(new HouseKeeperBatch(new YourJobClassHere(), false));
 *
 *		To run only specific job:
 *		Database.executeBatch(new HouseKeeperBatch(new YourJobClassHere(), true));
 *		e.g. to run LogCleanerBatch and no other processor after it do this:
 *			Database.executeBatch(new HouseKeeperBatch(new LogCleanerBatch(), true));
 *
 *	Version History :   
 *	2014-12-03 - MJL-1350 - AG
 *	initial version was implemented as part of MJL-1350
 *	Note: unit tests for current version of HouseKeeperBatch are in LogCleanerBatchTest
 *  2018-07-19 = COPT-3775 Add an active flag to the HousekeeperBatch Custom setting     
 * 	2020-01-06 -          - VC
 *	Only select active House_Keeping_Config__c custom setting records when choosing next class to run.	
 */
public with sharing class HouseKeeperBatch implements Database.Batchable<SObject>, Schedulable, Database.Stateful {

	/**
	 * Each Housekeeping class must implement HouseKeeperBatch.Processor to be run
	 */
	public interface Processor {
		//name of the process specified in House_Keeping_Config__c custom setting
		String getName();
		Integer getBatchSize();
		Database.QueryLocator  start(Database.BatchableContext bc);
		void execute(Database.BatchableContext bc, List<SObject> scope);
		void finish(Database.BatchableContext bc);
	}
	
	private Processor processor;
	private Boolean singleProcess = false;

	public void execute(SchedulableContext SC) {
       	final HouseKeeperBatch houseKeeper = new HouseKeeperBatch();
		if (null != houseKeeper.processor) {
			Database.executeBatch(houseKeeper, houseKeeper.getBatchSize());
		} else {
			System.debug('agX NO processes ready to be run based on current config/schedule');
		}
	}

	//Default set up to run all houskeeping classes
	public HouseKeeperBatch() { 
		this(null, false);
	}
	
	public HouseKeeperBatch(final Processor processor, final Boolean singleProcess) {

        this.processor = processor != null ? processor : getNextProcessor(null);
		this.singleProcess = singleProcess;
	}

	public Database.QueryLocator start(Database.BatchableContext bc) {
		return processor.start(bc);
	}
	public void execute(Database.BatchableContext bc, List<SObject> scope) {
		if (null != processor) {
			processor.execute(bc, scope);
		}

	}
	public void finish(Database.BatchableContext bc){ 
		processor.finish(bc);
		//stamp Last_Run_Time__c
		final House_Keeping_Config__c processorConfig = getProcessorConfig(processor);
		processorConfig.Last_Run_Time__c = System.now();
		Database.update(processorConfig);
		
        if (!singleProcess) {
			processor = getNextProcessor(processor);
            if (null != processor) {
                final HouseKeeperBatch nextProcessor = new HouseKeeperBatch(processor, singleProcess);
                Database.executeBatch(nextProcessor, nextProcessor.getBatchSize());
            }
        }
	}
	
	private Integer getBatchSize() {
		System.assertNotEquals(null, this.processor, 'At this point this.processor must be defined');
		return processor.getBatchSize();
	}
	private static Processor getNextProcessor(final Processor previousProcessor) {
		final House_Keeping_Config__c processorConfig = getNextProcessorConfig(previousProcessor);
		if (null != processorConfig) {
			Type t = Type.forName(processorConfig.Class_Name__c);
			if (null != t) {
				HouseKeeperBatch.Processor p = (HouseKeeperBatch.Processor)t.newInstance();
				return p;
			}
			System.assert(false, 'Unsupported batch type=' + processorConfig.Name);
		}
		return null;
	}

	private static House_Keeping_Config__c getNextProcessorConfig(final Processor previousProcessor) {
		Integer previousOrder = -1000;

		if (null != previousProcessor) {
			final House_Keeping_Config__c previousProcessConfig = getProcessorConfig(previousProcessor);
			previousOrder = previousProcessConfig.Order__c.intValue();
		}

		//query next process
		for (House_Keeping_Config__c config : [select Name, Order__c, Class_Name__c, Last_Run_Time__c, Run_Frequency_Days__c 
												from House_Keeping_Config__c 
												where Active__c = true
												  and Order__c > :previousOrder 
												  and Run_Frequency_Days__c <> null
												order by Order__c]) {
			//find and active config where Last_Run_Time__c & Run_Frequency_Days__c allows it to be run now
			Datetime lastRun = (null == config.Last_Run_Time__c) ? Datetime.newInstance(1970, 01, 01) : config.Last_Run_Time__c;
			if (lastRun.addDays(config.Run_Frequency_Days__c.intValue()) < System.now()) {
				return config;
			}

		}
		return null;
	}
	
	private static House_Keeping_Config__c getProcessorConfig(final Processor aprocessor) {
		System.assert(String.isNotBlank(aprocessor.getName()), 'Processor.getName() must return not blank value');
		final Map<String, House_Keeping_Config__c> configMap = House_Keeping_Config__c.getAll();
		final House_Keeping_Config__c config = configMap.get(aprocessor.getName());
		System.assertNotEquals(null, config, 'No House_Keeping_Config__c record defined for process: ' + aprocessor.getName());
		return config;
	}
}