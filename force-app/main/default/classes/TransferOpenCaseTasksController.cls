public without sharing class TransferOpenCaseTasksController 
{
	public User myUser = null;
	public Task taskForOwner {get;set;}
	public List<TaskWrapper> openCaseTaskWrappersForUser = null;
	public List<TaskWrapper> closedCaseTaskWrappersForUser = null;
	
	public TransferOpenCaseTasksController()
	{
		taskForOwner = new Task();
	}
	
	public void findTasksForUser()
	{
		if (JLHelperMethods.valueIsNull(taskForOwner.OwnerId))
		{
        	ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, 'Please specify a user!'));
        	return;
        }
        
        if (!isThisUserAllowedToReassignTaskForUser(taskForOwner.OwnerId))
        {
        	ApexPages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR, 'You are not permitted to reassign open tasks for this user!'));
        	return;
        }
        
        // Need to null them to refresh values
        openCaseTaskWrappersForUser = null;
		closedCaseTaskWrappersForUser = null;
	}
	
	public List<TaskWrapper> getOpenCaseTaskWrappersForUser()
	{
		if (null == openCaseTaskWrappersForUser)
		{
			openCaseTaskWrappersForUser = new List<TaskWrapper>();
		
			for (TaskWrapper tWrapper : getOpenTasksOnCases())
			{
				if (tWrapper.ParentCase.IsClosed == false)
				{
					openCaseTaskWrappersForUser.add(tWrapper);
				}
			}
		}
		
		return openCaseTaskWrappersForUser;
	}
	
	public List<TaskWrapper> getClosedCaseTaskWrappersForUser()
	{
		if (null == closedCaseTaskWrappersForUser)
		{
			closedCaseTaskWrappersForUser = new List<TaskWrapper>();
			
			for (TaskWrapper tWrapper : getOpenTasksOnCases())
			{
				if (tWrapper.ParentCase.IsClosed)
				{
					closedCaseTaskWrappersForUser.add(tWrapper);
				}
			}
		}
		
		return closedCaseTaskWrappersForUser;
	}
	
	public List<Task> getOpenTasksForUser()
	{
		// going to put a limit on this
		if  (JLHelperMethods.valueIsNull(taskForOwner.OwnerId))
		{
			return new List<Task>();
		}
		
		// do we need to return them in any order?
		return [Select jl_Team__c, jl_Description__c, WhatId, Subject, Status, Priority, OwnerId, IsClosed, Id, CallType, AccountId, ActivityDate  From Task where IsClosed = false and OwnerId = :taskForOwner.OwnerId AND WhatId != null LIMIT 200];
		
	}
	
	public List<TaskWrapper> getOpenTasksOnCases()
	{
		List<TaskWrapper> taskWrappers = new List<TaskWrapper>();
		
		List<Id> caseIds = new List<Id>();
		for (Task t: getOpenTasksForUser())
		{
			if (JLHelperMethods.isCaseId(t.Whatid))
			{
				caseIds.add(t.Whatid);
			}
		}
		
		Map<Id,Case> mapOfTaskCases = new Map<Id,Case>([Select CaseNumber, Status, jl_Access__c, IsClosed, Id, Contact.Name, ContactId From Case where Id in :caseIds]);
		for (Task t: getOpenTasksForUser())
		{
			if (JLHelperMethods.isCaseId(t.Whatid))
			{
				Case c = mapOfTaskCases.get(t.Whatid);
				
				// There are many reason not to put this in the list
				// Mainly lets cut out Tasks on Case where they are Private / Limited Access
				if (c.jl_Access__c != 'Limited')
				{
					TaskWrapper wrapper = new TaskWrapper();
					wrapper.parentCase = c;
					wrapper.task = t;
					taskWrappers.add(wrapper);
				}
			}
		}
		
		return taskWrappers;
	}
	
	public Boolean isThisUserAllowedToReassignTaskForUser(Id userId)
	{
		// TODO - implement
		return true;
	}
	
	public void transferSelectedTasksToMe()
	{
		List<Task> tasksTotransfer = new List<Task>();
		for (TaskWrapper tWrapper : openCaseTaskWrappersForUser)
		{
			if (tWrapper.isSelected)
			{
				tasksTotransfer.add(tWrapper.task);
			}
		}
		
		for (TaskWrapper tWrapper : closedCaseTaskWrappersForUser)
		{
			if (tWrapper.isSelected)
			{
				tasksTotransfer.add(tWrapper.task);
			}
		}
		
		if (tasksTotransfer.size() > 0)
		{
			for (Task t: tasksTotransfer)
			{
				t.OwnerId = Userinfo.getUserId();
			}
			
			
			update tasksTotransfer;
		}
		else
		{
			// Throw an error?
		}
		
		openCaseTaskWrappersForUser = null;
		closedCaseTaskWrappersForUser = null;
	}
	
	public class TaskWrapper
	{
		public Task task {get;set;}
		public Boolean isSelected {get;set;}
		public Case parentCase {get;set;}
		
		public TaskWrapper()
		{
			isSelected = false;
		}
	}
	
}