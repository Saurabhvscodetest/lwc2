/******************************************************************************
* @author       Kevin Burke
* @date         14.04.2014
* @description  Class that calls the various handler classes depending on the 
*               Trigger Object and Trigger event. This class, part of the Trigger Framework, 
*               regulates the execution of triggers based on the custom settings of the current 
*               user profile
* Edit    Date        Author      Comment
* 001     05/08/15    MP          CMP-507 Don't execute any non-contact updates if called as part of PCD superseded record logic. 
* 002     22/09/15    MP          CMP-684 Add new clsInteractionTriggerHandler call. 
* 003     23/10/25    NA          MJLP-391 to fix too many soql errors during performance run  
* 004     07/12/15    MP          COPT-466 Add new AutoAssignPermissionSetHandler call. 
* 005     10/12/15    MP          COPT-485 Add new DuplicateRecordItemHandler call. 
* 006     11/03/16    MP          COPT-654 isEnabled() was only operating at Profile Level.  Now using getInstance(). 
******************************************************************************/

public without sharing class clsTriggerHandler {
    //private static final Boolean isEnabled = isEnabled(); // <<001>>
    private static Boolean isEnabled = true;


    public static void mainEntry(String triggerObject, Boolean isBefore, 
                               Boolean isDelete, Boolean isAfter, 
                               Boolean isInsert, Boolean isUpdate, 
                               Boolean isExecuting, List<SObject> newlist, 
                               Map<ID,SObject> newmap, List<SObject> oldlist, 
                               Map<Id,SObject> oldmap)
    {
        // Changes COPT-5856
         isEnabled = isEnabled();
         // End Changes COPT-5856
        System.debug('@@entering clsTriggerHandler for object: ' + triggerObject);
        if (Trigger.new != null)
        {
            System.debug('Trigger size: ' + System.Trigger.new.size());
        }

        // <<001>> Boolean isEnabled = isEnabled();
        
        // <<01>> Section refactored to try Contact first.  Also changed list of ifs to use else ifs so that every type isn't checked every time
        if (isEnabled) { 
            if ('Contact'.equals(triggerObject)) {
                clsContactTriggerHandler.mainEntry(
                    trigger.IsBefore,trigger.IsDelete,
                    trigger.IsAfter,trigger.IsInsert,
                    trigger.IsUpdate,trigger.IsExecuting,trigger.new,
                    trigger.newmap, trigger.old,trigger.oldmap);
            } 
            //<<003>> clsDirectMailUpdatesTriggerHandler.is_jl_request_update==true
            else if (PCDUpdateHandler.IS_RUNNING_PCD_UPDATE || DirectMailUpdatesTriggerHandler.is_jl_request_update==true) {
                System.debug('@@clsTriggerHandler PCD update skipping for ' + triggerObject + ' trigger');
                return;
            } else if ('Case'.equals(triggerObject)) {
                clsCaseTriggerHandler.mainEntry(
                    trigger.IsBefore,trigger.IsDelete,
                    trigger.IsAfter,trigger.IsInsert,
                    trigger.IsUpdate,trigger.IsExecuting,trigger.new,
                    trigger.newmap, trigger.old,trigger.oldmap);
            } else if ('Interaction__c'.equals(triggerObject)) {
                // <<02>>
                InteractionTriggerHandler.mainEntry(
                    trigger.IsBefore,trigger.IsDelete,
                    trigger.IsAfter,trigger.IsInsert,
                    trigger.IsUpdate,trigger.IsExecuting,trigger.new,
                    trigger.newmap, trigger.old,trigger.oldmap);
            } else if ('CaseComment'.equals(triggerObject)) {
                CaseCommentTriggerHandler.mainEntry(
                    trigger.IsBefore,trigger.IsDelete,
                    trigger.IsAfter,trigger.IsInsert,
                    trigger.IsUpdate,trigger.IsExecuting,trigger.new,
                    trigger.newmap, trigger.old,trigger.oldmap);
            } else if ('Task'.equals(triggerObject)) {
                clsTaskTriggerHandler.mainEntry(
                    trigger.IsBefore,trigger.IsDelete,
                    trigger.IsAfter,trigger.IsInsert,
                    trigger.IsUpdate,trigger.IsExecuting,trigger.new,
                    trigger.newmap, trigger.old,trigger.oldmap);
            } else if ('EmailMessage'.equals(triggerObject)) {
                clsEmailMessageTriggerHandler.mainEntry(
                    trigger.IsBefore,trigger.IsDelete,
                    trigger.IsAfter,trigger.IsInsert,
                    trigger.IsUpdate,trigger.IsExecuting,trigger.new,
                    trigger.newmap, trigger.old,trigger.oldmap);
            } else if ('User'.equals(triggerObject)) {
                clsUserTriggerHandler.mainEntry(
                    trigger.IsBefore,trigger.IsDelete,
                    trigger.IsAfter,trigger.IsInsert,
                    trigger.IsUpdate,trigger.IsExecuting,trigger.new,
                    trigger.newmap, trigger.old,trigger.oldmap);
            } else if ('Load_User'.equals(triggerObject)) {
                UserLoadTriggerHandler.mainEntry(
                    trigger.IsBefore,trigger.IsDelete,
                    trigger.IsAfter,trigger.IsInsert,
                    trigger.IsUpdate,trigger.IsExecuting,trigger.new,
                    trigger.newmap, trigger.old,trigger.oldmap);
            } else if ('Attachment'.equals(triggerObject)) {
                AttachmentHandler.mainEntry(
                    trigger.IsBefore,trigger.IsDelete,
                    trigger.IsAfter,trigger.IsInsert,
                    trigger.IsUpdate,trigger.IsExecuting,trigger.new,
                    trigger.newmap, trigger.old,trigger.oldmap);
            } else if ('Data_Hub__c'.equals(triggerObject)) {
                DataHubHandler.mainEntry(
                    trigger.IsBefore,trigger.IsDelete,
                    trigger.IsAfter,trigger.IsInsert,
                    trigger.IsUpdate,trigger.IsExecuting,trigger.new,
                    trigger.newmap, trigger.old,trigger.oldmap);   
            } else if ('Contact_Profile__c'.equals(triggerObject)) {
                ContactProfileTriggerHandler.mainEntry(
                    trigger.IsBefore,trigger.IsDelete,
                    trigger.IsAfter,trigger.IsInsert,
                    trigger.IsUpdate,trigger.IsExecuting,trigger.new,
                    trigger.newmap, trigger.old,trigger.oldmap);
            } else if ('Direct_Mail_Update__c'.equals(triggerObject)){
                DirectMailUpdatesTriggerHandler.mainEntry(
                    trigger.IsBefore,trigger.IsDelete,
                    trigger.IsAfter,trigger.IsInsert,
                    trigger.IsUpdate,trigger.IsExecuting,trigger.new,
                    trigger.newmap, trigger.old,trigger.oldmap);                
            } else if ('Rewards__c'.equals(triggerObject)){
                clsRewardsTriggerHandler.mainEntry(
                    trigger.IsBefore,trigger.IsDelete,
                    trigger.IsAfter,trigger.IsInsert,
                    trigger.IsUpdate,trigger.IsExecuting,trigger.new,
                    trigger.newmap, trigger.old,trigger.oldmap);                
            } else if ('Auto_Assign_Permission_Set__c'.equals(triggerObject)){
                AutoAssignPermissionSetHandler.mainEntry(
                    trigger.IsBefore,trigger.IsDelete,
                    trigger.IsAfter,trigger.IsInsert,
                    trigger.IsUpdate,trigger.IsExecuting,trigger.new,
                    trigger.newmap, trigger.old,trigger.oldmap);                
            } else if ('DuplicateRecordItem'.equals(triggerObject)){
                DuplicateRecordItemTriggerHandler.mainEntry(
                    trigger.IsBefore,trigger.IsDelete,
                    trigger.IsAfter,trigger.IsInsert,
                    trigger.IsUpdate,trigger.IsExecuting,trigger.new,
                    trigger.newmap, trigger.old,trigger.oldmap);                
            } else if ('AgentWork'.equals(triggerObject)){
                AgentWorkTriggerHandler.mainEntry(
                    trigger.IsBefore,trigger.IsDelete,
                    trigger.IsAfter,trigger.IsInsert,
                    trigger.IsUpdate,trigger.IsExecuting,trigger.new,
                    trigger.newmap, trigger.old,trigger.oldmap);
            } else if ('Case_Activity__c'.equals(triggerObject)){
                CaseActivityTriggerHandler.mainEntry(
                    trigger.IsBefore,trigger.IsDelete,
                    trigger.IsAfter,trigger.IsInsert,
                    trigger.IsUpdate,trigger.IsExecuting,trigger.new,
                    trigger.newmap, trigger.old,trigger.oldmap);
            }else if ('MC_Transactions__c'.equals(triggerObject)){
                McTransactionsTriggerHandler.mainEntry(
                    trigger.IsBefore,trigger.IsDelete,
                    trigger.IsAfter,trigger.IsInsert,
                    trigger.IsUpdate,trigger.IsExecuting,trigger.new,
                    trigger.newmap, trigger.old,trigger.oldmap);
            }
        }
        System.debug('@@exiting clsTriggerHandler');
    }
    
    public static boolean isEnabled () {
        //enable the trigger for test classes
        if(Test.isrunningTest()) {
            return true; 
        } else {
            System.debug('checking user profile to determine if triggers should be run');
            // <<006>> return jl_runtriggers__c.getInstance(UserInfo.getProfileId()).Run_Triggers__c;
            return jl_runtriggers__c.getInstance().Run_Triggers__c && CommonStaticUtils.runTrigger ; // <<006>>
        }
    }   
}