global class GoogleSheetToCreateCases implements Messaging.InboundEmailHandler {
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,Messaging.InboundEnvelope envelope) {
        
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        Messaging.InboundEmail.BinaryAttachment[] binaryAttachments;
        Messaging.InboundEmail.binaryAttachment binaryattachment;
        
        try{
            binaryAttachments = email.BinaryAttachments;
            boolean NOCSVAttachment = true; 
            boolean EmailSubjectFlag = true;
            
            //EMAIL BODY TO CASE
            //MP3 Exceptions/Queries
            if(email.subject != null && email.subject.containsIgnoreCase('MP3 Exceptions/Queries')){
                Id QueryRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Query').getRecordTypeId();
                Id ComplaintRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('Complaint').getRecordTypeId();         
                Map<String, sObject> mapOfCustomSetting = MP3Exceptions_FieldMapping__c.getAll();
                CSVToCaseRecordsUtil.createEmailBodyToCaseRecord(email,mapOfCustomSetting,QueryRecordTypeId,ComplaintRecordTypeId,email.subject);
            }

            //COPT -5975- Start
            else if(binaryAttachments != null)
            {
				Google_Sheet_Configuration__mdt[] googleSheetSettings = [SELECT MasterLabel, Email_Subject__c, Case_Origin__c, Case_Record_Type_Name__c, Field_Mapping_Custom_Setting_Name__c,CsvNameCheck__c FROM Google_Sheet_Configuration__mdt];
				Map<String,Google_Sheet_Configuration__mdt> map_googleSheetSettings = new Map<String,Google_Sheet_Configuration__mdt>();
				for(Google_Sheet_Configuration__mdt temp : googleSheetSettings){
					map_googleSheetSettings.put(temp.Email_Subject__c, temp);
				}
                
				for(integer k=0; k < binaryAttachments.size(); k++){
                    binaryattachment = binaryAttachments[k];
                    If(binaryattachment.Filename.endsWithIgnoreCase('.csv'))
                    {
                        NOCSVAttachment = false;
                        String RecordTypeName;
						Id RecordTypeId;
						String emailSubject;
                        String CaseOrigin;
                        boolean isMatchingAttachment;
                        Map<String, sObject> mapOfCustomSetting=new Map<String,sObject>();
                        if(email.subject != null){ 
							//New fully configurable block - COPT 5975
							if(map_googleSheetSettings.containsKey(email.subject)){
                                EmailSubjectFlag = false;
                                isMatchingAttachment=false;
                                String csvNameCheckRequired=map_googleSheetSettings.get(email.subject).CsvNameCheck__c;
                                System.debug(' is Blank csvNameCheckRequired'+String.isNotBlank(csvNameCheckRequired));
                                if(String.isNotBlank(csvNameCheckRequired)){
                                    if(binaryattachment.filename.containsIgnoreCase(csvNameCheckRequired)){
                                        isMatchingAttachment=true;
                                    }
                                }
                                else{
                                    isMatchingAttachment=true;
                                }
                                System.debug('---isMatchingAttachment---'+isMatchingAttachment);
                                if(isMatchingAttachment){
                                CaseOrigin = map_googleSheetSettings.get(email.subject).Case_Origin__c;
								RecordTypeName = map_googleSheetSettings.get(email.subject).Case_Record_Type_Name__c;
                                RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get(RecordTypeName).getRecordTypeId();
                                String custSettingName = map_googleSheetSettings.get(email.subject).Field_Mapping_Custom_Setting_Name__c;
                                Schema.DescribeSObjectResult[] customSetting = Schema.describeSObjects(new List<String> { custSettingName });
								String fieldNames = '';
    							Map<String, Schema.SObjectField> fieldMap = customSetting[0].fields.getMap();
    							for(String field : fieldMap.keySet()) {
        						Schema.DescribeFieldResult fieldResult = fieldMap.get(field).getDescribe();
        						if(fieldResult.isCustom()) {
            					fieldNames += field + ',';
                                }
                                }
                                fieldNames = fieldNames.removeEnd(',');
    							string query = 'SELECT Name,' + fieldNames + ' FROM '+ custSettingName;
    							List<sObject> customSettingData = Database.query(query);
                                for(sObject obj:customSettingData){
                                    mapOfCustomSetting.put(String.valueOf(obj.get('Name')),obj);
                                }
                            }
                            }
                            
                            //COPT-5975 END
                            //Method Call to Create Cases
                            If(mapOfCustomSetting != null)
                                CSVToCaseRecordsUtil.createCSVToCaseRecords(binaryattachment.body, mapOfCustomSetting,RecordTypeId, binaryattachment.filename,email.subject, CaseOrigin);
                        }
                        if(EmailSubjectFlag){
                            CSVToCaseRecordsUtil.sendEmail(binaryattachment.body, binaryattachment.filename, 'An error has occured while processing email. Email subject not defined, Please send the defined email subject only.<br/>', 0, 0);
                        }
                    }
                }
                if(NOCSVAttachment == true){
                    CSVToCaseRecordsUtil.sendEmail(null,null,'An error has occured while processing email. No CSV file as an attachment found.<br/>', 0, 0);
                }
            }
            else{
                CSVToCaseRecordsUtil.sendEmail(null,null,'An error has occured while processing email. Please send the email with defined email subject or attach correct CSV file.<br/>', 0, 0);
                System.debug('*********------->>>>>>>>>No CSV Found*********------->>>>>>>>>');
            }
        }
        catch (Exception e)
        {
            CSVToCaseRecordsUtil.sendEmail(binaryattachment.body, binaryattachment.filename,'An exception has occured while processing email. Please refer the error message. <br/> Error Message: '+e.getMessage()+'<br/><br/>', 0, 0);
            system.debug('Exception Message ***** '+e);
        }  
        result.success = true;
        return result;
    }
}