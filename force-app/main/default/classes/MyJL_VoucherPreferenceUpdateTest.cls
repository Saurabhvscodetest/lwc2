@isTest
public class MyJL_VoucherPreferenceUpdateTest {
    
    private static final String shopperId = 'RaviTeluShopperId2020';
    private static final String loyaltyNumber = '60020488866';
    private static final String cardNumber = '8018505410660204905765';
    private static final String messageId = 'RaviTeluMessageId20';
    private static final String voucherPreference = 'Digital';
    
    static {
    	BaseTriggerHandler.addSkipReason(CalloutHandler.SKIP_ALL_NOTIFICATIONS_CODE, 'disable outbound events in this test class');
    }
    
    @testSetup
    static void testData() {
        
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        
        Contact c = MyJL_TestUtil.createCustomer('Ordering_Customer');
        c.Shopper_ID__c = shopperId;
        update c;
        
        final List<Loyalty_Account__c> loyAccList = NEW List<Loyalty_Account__c>();
        loyAccList.add(NEW Loyalty_Account__c(Name=loyaltyNumber, Voucher_Preference__c=voucherPreference, ShopperId__c=shopperId, IsActive__c=true, Membership_Number__c=loyaltyNumber, Contact__c=c.Id));
        insert loyAccList;
        
        final List<Loyalty_Card__c> loyCardList = NEW List<Loyalty_Card__c>();
        loyCardList.add(NEW Loyalty_Card__c(Name=cardNumber, Disabled__c=false, Loyalty_Account__c=loyAccList[0].Id));
        Database.insert(loyCardList);
    }
    
    @isTest static void missingPreference() {
                
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        RestContext.request = req;
        RestContext.response = res;        
        req.requestURI = '/MyJL_VoucherPreferenceUpdate/customer/' ;
        req.httpMethod = 'POST';
        
        Test.startTest();
            try {
                MyJL_VoucherPreferenceUpdate.VoucherPreferenceUpdateWrapper vouchPref = MyJL_VoucherPreferenceUpdate.updateVoucherPreference(NULL, shopperId, loyaltyNumber, messageId);
            }
            catch(Exception e) {}
        Test.stopTest();
    }
    
    @isTest static void missingLoyaltyNumber() {        
                
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        RestContext.request = req;
        RestContext.response = res;        
        req.requestURI = '/MyJL_VoucherPreferenceUpdate/customer/' ;
        req.httpMethod = 'POST';
        
        Test.startTest();
            try {
                MyJL_VoucherPreferenceUpdate.VoucherPreferenceUpdateWrapper vouchPref = MyJL_VoucherPreferenceUpdate.updateVoucherPreference(voucherPreference, shopperId, NULL, messageId);
            }
            catch(Exception e) {}
        Test.stopTest();
    }
    
    @isTest static void missingShopperId() {        
                
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        RestContext.request = req;
        RestContext.response = res;        
        req.requestURI = '/MyJL_VoucherPreferenceUpdate/customer/' ;
        req.httpMethod = 'POST';
        
        Test.startTest();
            try {
                MyJL_VoucherPreferenceUpdate.VoucherPreferenceUpdateWrapper vouchPref = MyJL_VoucherPreferenceUpdate.updateVoucherPreference(voucherPreference, NULL, loyaltyNumber, messageId);
            }
            catch(Exception e) {}
        Test.stopTest();
    }
    
    @isTest static void missingMessageId() {        
                
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        RestContext.request = req;
        RestContext.response = res;        
        req.requestURI = '/MyJL_VoucherPreferenceUpdate/customer/' ;
        req.httpMethod = 'POST';
        
        Test.startTest();
            try {
                MyJL_VoucherPreferenceUpdate.VoucherPreferenceUpdateWrapper vouchPref = MyJL_VoucherPreferenceUpdate.updateVoucherPreference(voucherPreference, shopperId, loyaltyNumber, NULL);
            }
            catch(Exception e) {}
        Test.stopTest();
    }
    
    @isTest static void noCustomerAvailable() {        
                
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        RestContext.request = req;
        RestContext.response = res;        
        req.requestURI = '/MyJL_VoucherPreferenceUpdate/customer/' ;
        req.httpMethod = 'POST';
        
        Test.startTest();
            try {
                MyJL_VoucherPreferenceUpdate.VoucherPreferenceUpdateWrapper vouchPref = MyJL_VoucherPreferenceUpdate.updateVoucherPreference(voucherPreference, 'shopperId', loyaltyNumber, messageId);
            }
            catch(Exception e) {}
        Test.stopTest();
    }    
	    
    @isTest static void duplicateMessageId() {        
                
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        RestContext.request = req;
        RestContext.response = res;        
        req.requestURI = '/MyJL_VoucherPreferenceUpdate/customer/' ;
        req.httpMethod = 'POST';
        
        //non unique message Id
		CustomSettings.setTestLogHeaderConfig(true, -1);
		CustomSettings.setTestLogDetailConfig(true, -1);

		Database.insert(new Log_Header__c(Message_ID__c = 'TEST-Duplicate_1', Service__c = MyJL_Util.SOURCE_SYSTEM));
        
        Test.startTest();
            try {
                MyJL_VoucherPreferenceUpdate.VoucherPreferenceUpdateWrapper vouchPref = MyJL_VoucherPreferenceUpdate.updateVoucherPreference(voucherPreference, shopperId, loyaltyNumber, 'TEST-Duplicate_1');
            }
            catch(Exception e) {}
        Test.stopTest();
        try {
        	MyJL_VoucherPreferenceUpdate.VoucherPreferenceUpdateWrapper vouchPref2 = MyJL_VoucherPreferenceUpdate.updateVoucherPreference(voucherPreference, shopperId, loyaltyNumber, 'TEST-Duplicate_1');
        }
        catch(Exception e) {}
    }
    
    @isTest static void inActiveLoyaltyAccount() {        
                
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        RestContext.request = req;
        RestContext.response = res;        
        req.requestURI = '/MyJL_VoucherPreferenceUpdate/customer/' ;
        req.httpMethod = 'POST';
        
        List<Loyalty_Account__c> laList = [ SELECT Id, IsActive__c, ShopperId__c FROM Loyalty_Account__c WHERE IsActive__c=true AND ShopperId__c = 'RaviTeluShopperId2020' LIMIT 1 ];
        laList[0].IsActive__c = false;
        update laList;
        
        Test.startTest();
            try {
                MyJL_VoucherPreferenceUpdate.VoucherPreferenceUpdateWrapper vouchPref = MyJL_VoucherPreferenceUpdate.updateVoucherPreference(voucherPreference, shopperId, loyaltyNumber, messageId);
            }
            catch(Exception e) {}
        Test.stopTest();
    }
    
    @isTest static void noLoyaltyAccount() {        
                
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        RestContext.request = req;
        RestContext.response = res;        
        req.requestURI = '/MyJL_VoucherPreferenceUpdate/customer/' ;
        req.httpMethod = 'POST';
        
        Test.startTest();
            try{
                MyJL_VoucherPreferenceUpdate.VoucherPreferenceUpdateWrapper vouchPref = MyJL_VoucherPreferenceUpdate.updateVoucherPreference(voucherPreference, shopperId, 'loyaltyNumber', messageId);
            }
        	catch(Exception e) {}
        Test.stopTest();
    }
    
    @isTest static void commitException() {        
                
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        RestContext.request = req;
        RestContext.response = res;        
        req.requestURI = '/MyJL_VoucherPreferenceUpdate/customer/' ;
        req.httpMethod = 'POST';
        
        Test.startTest();
            try{
                MyJL_VoucherPreferenceUpdate.VoucherPreferenceUpdateWrapper vouchPref = MyJL_VoucherPreferenceUpdate.updateVoucherPreference('voucherPreference', shopperId, loyaltyNumber, messageId);
            } 
        	catch(Exception e) {}
        Test.stopTest();
    }
    
    @isTest static void unhandledException() {        
                
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        RestContext.request = req;
        RestContext.response = NULL;        
        req.requestURI = '/MyJL_VoucherPreferenceUpdate/customer/' ;
        req.httpMethod = 'POST';
        
        Test.startTest();
            try {
                MyJL_VoucherPreferenceUpdate.VoucherPreferenceUpdateWrapper vouchPref = MyJL_VoucherPreferenceUpdate.updateVoucherPreference(voucherPreference, shopperId, loyaltyNumber, messageId);
            }
        	catch(Exception e) {}
        Test.stopTest();
    }
    
    @isTest static void noCustomerAvailableCaseSensitivity() {        
                
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();        
        RestContext.request = req;
        RestContext.response = NULL;        
        req.requestURI = '/MyJL_VoucherPreferenceUpdate/customer/' ;
        req.httpMethod = 'POST';
        
        Test.startTest();
            try {
                MyJL_VoucherPreferenceUpdate.VoucherPreferenceUpdateWrapper vouchPref = MyJL_VoucherPreferenceUpdate.updateVoucherPreference(voucherPreference, 'RaviteluShopperId2020', loyaltyNumber, messageId);
            }
            catch(Exception e) {}
        Test.stopTest();
    }
}