@isTest
public class NextActionActivityControllerLEX_TEST {
    
    @isTest static void testCheckIfNKUCase() {
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        User testUser;
        Case testCase;
        Case_Activity__c testCaseActivity;
        Contact testContact;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
        }
        
        System.runAs(testUser) {
            
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'stuart@little.com';
            insert testContact;
            
            testCase = CaseTestDataFactory.createPSECase(testContact.Id);
            insert testCase;
            
            Test.startTest();
            
            System.assertEquals(true, NextActionActivityControllerLEX.checkIfNKUCase(testCase.Id).get('PSE'));
            System.assertEquals(false, NextActionActivityControllerLEX.checkIfNKUCase(testCase.Id).get('NKU'));
            
            testCase.RecordTypeId = CommonStaticUtils.getRecordTypeID('Case', 'NKU');
            update testCase;
            
            System.assertEquals(true, NextActionActivityControllerLEX.checkIfNKUCase(testCase.Id).get('NKU'));
            System.assertEquals(false, NextActionActivityControllerLEX.checkIfNKUCase(testCase.Id).get('PSE'));
            
            Test.stopTest();
            
        }
    }
    
    @isTest static void testSaveCaseActivity() {
        
        String CUSTOMER_CONTACT_PROMISE = 'Customer Contact Promise';
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User testUser;
        Case testCase;
        Case_Activity__c testCaseActivity;
        Case_Activity__c testCaseActivityInsert;
        Contact testContact;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
            
            insert CustomSettingTestDataFactory.createConfigSettings();            
        }
        
        System.runAs(testUser) {
            
            
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'tester@testing.com';
            insert testContact;
            
            
            testCase = CaseTestDataFactory.createCase(testContact.Id);
            insert testCase;
            
            Test.startTest();
            
            testCaseActivity = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivity(testCase.Id);
            
            testCaseActivityInsert = CaseActivityTestDataFactory.createCustomerContactPromiseCaseActivityInsert(testCase.Id);
            
            NextActionActivityControllerLEX.caseRestrictTransferPencil(testCaseActivityInsert.id);
            
            /* COPT-5415
            NextActionActivityControllerLEX.checkPSETransfer(testCase.id);
            COPT-5415 */
                
            NextActionActivityControllerLEX.fetchCurrentCaseQueue(testCase.id);
            
            NextActionActivityControllerLEX.fetchAvailableSlots('Branch-CCLA',testCase.id);
            NextActionActivityControllerLEX.fetchSlotsBasedOnDate('Branch-CCLA', string.ValueOf(date.today()),testCase.id);
          
            
            String caseActivityJSON = JSON.serialize(testCaseActivity);
            NextActionActivityControllerLEX.saveCaseActivity(caseActivityJSON,'');
           // NextActionActivityControllerLEX.updatecaseActivity(caseActivityJSON);
            
            List<Case_Activity__c> createdCaseActivities = [SELECT Id, Activity_Description__c, Activity_Date__c, Activity_Start_Time__c, Activity_End_Time__c, Customer_Promise_Type__c,Activity_Start_Date_Time__c, Activity_End_Date_Time__c 
                                                    FROM Case_Activity__c where Customer_Promise_Type__c = :CUSTOMER_CONTACT_PROMISE];
            
            
            Id caseBusinessHourId = [select BusinessHoursId from Case where Id =: testCase.Id].get(0).BusinessHoursId;

            Case_Activity__c createdCaseActivity = createdCaseActivities[0];

            System.assertEquals('test description', createdCaseActivity.Activity_Description__c);
            System.assertEquals(Date.today().addDays(2), createdCaseActivity.Activity_Date__c);
            system.assert(BusinessHours.isWithin(caseBusinessHourId,createdCaseActivity.Activity_Start_Date_Time__c),'Case Activity start date is not within business hours');
            System.assertEquals(CUSTOMER_CONTACT_PROMISE, createdCaseActivity.Customer_Promise_Type__c);
            Test.stopTest();
            
        }
    }

    @isTest static void testCaseActivityCustomValidationsWithErrors() {

        Date inputCaseActivityDate = Date.today();
        String caseActivityDateToString = String.valueOf(inputCaseActivityDate);
        String caseActivityStartTime = '10:00';
        String caseActivityEndTime =  '11:00';

        Test.startTest();

            String expectedErrorMessage = 'Selected time slot should have at least 2 business hours to complete. Please amend the activity time or date accordingly.';
            String actualErrorMessage = NextActionActivityControllerLEX.checkCaseActivityValidations(caseActivityDateToString, caseActivityStartTime, caseActivityEndTime,null, null, false, false);
            
        Test.stopTest(); 
    }

    @isTest static void testCaseActivityCustomValidationsWithNoErrors() {

        Date inputCaseActivityDate = Date.today().addDays(2);
        String caseActivityDateToString = String.valueOf(inputCaseActivityDate);
        String caseActivityStartTime = '10:00';
        String caseActivityEndTime = '14:00';

        Test.startTest();

            String expectedErrorMessage = 'No errors';
            String actualErrorMessage = NextActionActivityControllerLEX.checkCaseActivityValidations(caseActivityDateToString, caseActivityStartTime, caseActivityEndTime,null, null, false, false);

        Test.stopTest(); 
    }
    
    @isTest static void testFetchCaseAssignmentDetails() {
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        
        User testUser;
        Case testCase;
        Case_Activity__c testCaseActivity;
        Contact testContact;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
            insert CustomSettingTestDataFactory.createConfigSettings();            
        }
        
        System.runAs(testUser) {
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'tester@testing.com';
            insert testContact;
            
            testCase = CaseTestDataFactory.createCase(testContact.Id);
            insert testCase;
            
            Test.startTest();
            Map<String, SObject> returnMap = NextActionActivityControllerLEX.fetchCaseAssignmentDetails(testCase.Id);
            system.assert(returnMap.containsKey('CaseAssignment'));
            system.assert(returnMap.containsKey('CurrentUserDetail'));
            Test.stopTest();
        }
    }

    @isTest static void fetchCurrentCaseQueueForCaseOwnerTest() {
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        User testUser;
        Case testCase;
        Case_Activity__c testCaseActivity;
        Contact testContact;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
        }
        
        System.runAs(testUser) {
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'tester@testing.com';
            insert testContact;
            
            testCase = CaseTestDataFactory.createCase(testContact.Id);
            insert testCase;
            
            Test.startTest();
                NextActionActivityControllerLEX.fetchCurrentCaseQueueForCaseOwner(testCase.Id);
            Test.stopTest();
        }
    }
    
     @isTest static void fetchAvailableDateandTimeSlotTest() {
        
        User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        User testUser;
        Case testCase;
        Contact testContact;
        
        System.runAs(runningUser) {
            testUser = UserTestDataFactory.createContactCentreUser();
            insert testUser;
        }
        
        System.runAs(testUser) {
            testContact = CustomerTestDataFactory.createContact();
            testContact.Email = 'tester@testing.com';
            insert testContact;
            
            testCase = CaseTestDataFactory.createCase(testContact.Id);
            insert testCase;
            
            Test.startTest();
               NextActionActivityControllerLEX.fetchAvailableSlotsDateTime('Branch-CCLA',testCase.id);
            Test.stopTest();
        }
    }
}