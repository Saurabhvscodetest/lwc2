public with sharing class MyJL_Const {
    public enum ERRORCODE {
        INACTIVE_SERVICE,
        MESSAGEID_NOT_PROVIDED,
        MESSAGEID_DUPLICATE,
        CUSTOMERID_NOT_PROVIDED,
        CUSTOMERID_NOT_FOUND,
        CUSTOMERID_NOT_UNIQUE,
        CUSTOMERID_ALREADY_USED,
        EMAIL_NOT_PROVIDED,
        EMAIL_NOT_UNIQUE,
        EMAIL_ALREADY_USED,
        FAILED_TO_PERSIST_DATA,
        NO_CUSTOMER_DATA,
        UNHANDLED_EXCEPTION,
        /* Join */
        INCORRECT_JOIN_REQUEST,
        BARCODE_ALREADY_USED,
        EMAIL_TOO_LONG,
        CUSTOMER_ALREADY_JOINED,
        /* Leave */
        LOYALTY_ACCOUNT_NOT_FOUND,
        NO_ACTIVE_LOYALTY_ACCOUNT_FOUND,
        /* KD GET */
        TRANSACTIONID_NOT_PROVIDED,
        TRANSACTIONID_ALREADY_USED,
        TRANSACTIONID_NOT_FOUND,
        NO_TRANSACTION_DATA,
        /* KD Search */
        INCORRECT_SEARCH_REQUEST,
        /* KD Create */
        TRANSACTIONID_NOT_UNIQUE,
        CARD_NUMBER_NOT_PROVIDED,
        ASSOCIATED_TRANSACTION_NOT_FOUND,
        CARD_NUMBER_NOT_FOUND,
        FULL_XML_NOT_PROVIDED,
        SHORT_XML_NOT_PROVIDED,
        TRANSACTION_AMOUNT_NOT_PROVIDED,
        TRANSACTION_DATE_NOT_PROVIDED,
        VOIDEDTRANSACTIONID_NOT_FOUND,
        MY_PARTNERSHIP_JOIN_REST_ERROR,
        MY_PARTNERSHIP_MISSING_DATA,
        MY_LOYALTY_NUMBER_MISSING_DATA
            
    }
    
    public enum ERRORTYPE {
        BUSINESS,
        TECHNICAL
    }
    
    public enum CONTACTMETHODTYPE {
        BILLING,
        OTHER
    }
    
    public static final String ERROR_NOT_CONFIGURED_MESSAGE = '<No Message details have been configured for this Error>';
    public static final String ERROR_NOT_CONFIGURED_TYPE = ERRORTYPE.BUSINESS.name();

    public static final String MY_PARTNERSHIP_CARD_TYPE = 'My Partnership';
    public static final String MY_JL_CARD_TYPE = 'My John Lewis';

    public static final String PACK_TYPE_PHYSICAL = 'Physical';
    public static final String PACK_TYPE_DIGITAL = 'Digital';

    public static final String MY_JL_VOUCHER_PREFERENCE_KEY = 'VoucherPreference';
}