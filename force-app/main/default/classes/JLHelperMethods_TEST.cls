@Istest
public with sharing class JLHelperMethods_TEST 
{

    public static TestMethod void  testHelperMethods()
    {
        String id18Char = '00DA0000000gWvLAAA';
        String id15Char = '00DA0000000gWvL';
        String id15CharDiff = '00DA0000000gWvK';
        
        System.assert(JLHelperMethods.valueIsNull(null), 'Id should be NULL');
        System.assert(JLHelperMethods.valueIsNull(''),'Id should be NULL');
        System.assert(!JLHelperMethods.valueIsNull('Not NULL'),'Id should NOT be NULL');
        System.assert(JLHelperMethods.idsAreEqual(id18Char,id18Char),'Id should be equal');
        System.assert(JLHelperMethods.idsAreEqual(id15Char,id15Char),'Id should be equal');
        System.assert(JLHelperMethods.idsAreEqual(id15Char,id18Char),'Id should be equal');
        System.assert(JLHelperMethods.idsAreEqual(id18Char,id15Char),'Id should be equal');
        System.assert(!JLHelperMethods.idsAreEqual(id18Char,id15CharDiff),'Id should be different');
        System.assert(!JLHelperMethods.idsAreEqual(id15Char,id15CharDiff),'Id should be different');
        System.assert(JLHelperMethods.idsAreEqual(null,null),'Nulls should be same');
        
        JLHelperMethods.isProductionOrg();
        JLHelperMethods.isSandboxOrg();
        JLHelperMethods.isSystemAdminUser();
        
       	Account descObj = new Account();
        Sobject sObj = (Sobject)descObj;
        JLHelperMethods.getPickListValues(sObj,'Type');
        
        JLHelperMethods.getCreateableFields(new Account());
        JLHelperMethods.getAllFields(new Account());
        
        System.assert(JLHelperMethods.isGroupId('00G'),'Valid Group id');
        System.assert(JLHelperMethods.isCaseId('500'),'Valid Case Id');
        
        System.assert(!JLHelperMethods.isGroupId('005'),'INValid Group id');
        System.assert(!JLHelperMethods.isCaseId('00G'),'INValid Case Id');
        
        
    }
}