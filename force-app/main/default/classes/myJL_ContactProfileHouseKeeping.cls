//
// This batch job can be used to do various jobs on the contact profiles
//
// Edit		Date		Who?		Comment
//	001		27/03/15	NTJ			Initial Version - for updating the anonymousflag because migration got the logic flipped (true should be false and vice versa)
//  002  27/06/16    NA                  Decommissioning contact profile  
public with sharing class myJL_ContactProfileHouseKeeping implements HouseKeeperBatch.Processor {
	private static Integer BATCH_SIZE = 200;//number of items in apex batch job scope

	public String getName() {
		return 'ContactProfileBatch';
	}

	public Database.QueryLocator start(Database.BatchableContext bc) {
		Database.QueryLocator ql;

		// Get the custom setting
		Integer rowLimit = 0;
		if (Test.isRunningTest() == false) {
			MyJL_Settings__c cp = MyJL_Settings__c.getInstance();
			if (cp != null) {
				if (cp.Batch_Size__c != null) {
					rowLimit = (Integer)cp.Batch_Size__c;
				}
			}
		}
		
		system.debug('rowLimit: '+rowLimit);
		if (rowLimit == 0) {
			ql = Database.getQueryLocator([SELECT Id, AnonymousFlag__c, AnonymousFlag2__c FROM contact WHERE Batch_Processed__c = false]);  //<<002>>
		} else {
			ql = Database.getQueryLocator([SELECT Id, AnonymousFlag__c, AnonymousFlag2__c FROM contact WHERE Batch_Processed__c = false LIMIT :rowLimit]);//<<002>>
		}

		return ql;
		
	}

	public void execute(Database.BatchableContext bc, List<SObject> scope) {
		List<contact> cps = (List<contact>) scope;  //<<002>
		
		for (contact cp:cps) {    //<<002>
			cp.AnonymousFlag2__c = (cp.AnonymousFlag__c) ? false : true;
			cp.Batch_Processed__c = true;
		}

		Database.update(cps);

	}

	public void finish(Database.BatchableContext bc){ }

	public Integer getBatchSize() {
		return BATCH_SIZE;
	}
}