/******************************************************************************
* @author       Matt Povey
* @date         09/06/2015
* @description  Class to produce and email a CSV file of the Salesfore report specified
*
* EDIT RECORD
*
* LOG     DATE        Author	JIRA		COMMENT      
* 001     09/06/2015  MP		CMP-98		Original code
*
*/
@RestResource(urlMapping='/sendCSVReportEmail/*')
global class CSVReportEmailer {
	private static final String ORG_ENDPOINT =  URL.getSalesforceBaseUrl().toExternalForm();
	private static final String DEV_ORG_CLIENT_ID = '3MVG9RHx1QGZ7OsjcO.zwKCSWtNMSEnGbtkfIR6q0KJkmmOBHDUm.eY4ilyxAwwOaoEZAYkXp0EFmOHTFbego';
	private static final String DEV_ORG_CLIENT_SECRET = '6652521600615679020';
	private static Id WORK_ALLOCATION_REPORT_USER;
	private static User WORK_ALLOCATION_REPORT_USER_RECORD;
	
	/**
	 * Email the specified report
	 * *** WARNING ***
	 * Cannot be used in triggers, scheduled jobs, test classes, batch jobs or Apex email services due to getContent() method
	 * @params:	String reportId			The Salesforce Id for the report to run
	 *			String reportName		The name to give the csv file
	 *			String emailRecipients	The email address(es) of the recipients (if more than one, separate with ';')
	 */
	@HttpPost
	global static void emailCSVReport(String reportId, String reportName, String emailRecipients) {
		emailRecipients = emailRecipients.remove(' '); // Because you just know someone will put spaces in it.
		List<String> emailRecipientList = emailRecipients.split(';', 0);
		ApexPages.PageReference report = new ApexPages.PageReference('/' + reportId + '?csv=1');
		Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
		attachment.setFileName(reportName + '.csv');
		if (!Test.isRunningTest()) {
			attachment.setBody(report.getContent());
		}
		attachment.setContentType('text/csv');
		
		Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
		message.setFileAttachments(new Messaging.EmailFileAttachment[] {attachment});
		message.setSubject(reportName);
		message.setPlainTextBody(reportName + ' report is attached.');
		message.setToAddresses(emailRecipientList);

		if (!Test.isRunningTest()) {
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] {message});
		}
	}

	/**
	 * Call the emailCSVReport REST web service.  This method can be called from Scheduled Jobs due to @future annotation
	 * @params:	String reportId			The Salesforce Id for the report to run
	 *			String reportName		The name to give the csv file
	 *			String emailRecipients	The email address(es) of the recipients (if more than one, separate with ';')
	 *			String userSessionId	The user session id - this can be passed in from non-scheduled jobs and calls
	 */
	@future(callout=true)
	global static void invokeEmailCSVReportService(String reportId, String reportName, String emailRecipients, String userSessionId) {
		system.debug('CSVReportEmailerDEBUG SESSION ID: ' + userSessionId);
		
		// If incoming userSessionId is blank then need to login to get one
		if (String.isBlank(userSessionId)) {
			try {
				WORK_ALLOCATION_REPORT_USER = Config_Settings__c.getInstance('WORK_ALLOCATION_REPORT_USER').Text_Value__c;
				WORK_ALLOCATION_REPORT_USER_RECORD = [SELECT Id, Username, Password_Encrypted__c FROM User WHERE Id = :WORK_ALLOCATION_REPORT_USER];
				// userSessionId = getSessionIdFromREST(); // REST DOESN'T WORK - RETURNS EMPTY REPORT
				// if (String.isBlank(userSessionId)) {
					userSessionId = getSessionIdFromSOAP();
				// }
			} catch (Exception e) {
		        EmailNotifier.sendNotificationEmail('CSVReportEmailer Error', 'CSVReportEmailer invokeEmailCSVReportService error: ' + e.getMessage());
			}
		}
		
		// Invoke REST Service
		HttpRequest req = new HttpRequest();
		req.setEndpoint(ORG_ENDPOINT + '/services/apexrest/sendCSVReportEmail');
		req.setMethod('POST');
		req.setHeader('Authorization', 'OAuth ' + userSessionId);
		req.setHeader('Content-Type','application/json');
		Map<String, String> postBody = new Map<String, String>();
		postBody.put('reportId', reportId);
		postBody.put('reportName', reportName);
		postBody.put('emailRecipients', emailRecipients);
		String reqBody = JSON.serialize(postBody);
		req.setBody(reqBody);
		
		Http http = new Http();
		if (!Test.isRunningTest()) {
			HttpResponse response = http.send(req);
			system.debug('CSVReportEmailerDEBUG CSV REPORT EMAIL RESPONSE: ' + response);
		}
	}
	
	/**
	 * Get the access token using REST.  Returns the access token
	 * @params:
	 */
	/*
	private static String getSessionIdFromREST() {
		HttpRequest req = new HttpRequest();
		req.setMethod('POST');
		req.setEndpoint(ORG_ENDPOINT + '/services/oauth2/token');
		req.setBody('grant_type=password' 
					+ '&client_id=' + DEV_ORG_CLIENT_ID 
					+ '&client_secret=' + DEV_ORG_CLIENT_SECRET 
					+ '&username=' + EncodingUtil.urlEncode(WORK_ALLOCATION_REPORT_USER_RECORD.UserName, 'UTF-8') 
					+ '&password=' + EncodingUtil.urlEncode(WORK_ALLOCATION_REPORT_USER_RECORD.Password_Encrypted__c, 'UTF-8'));

		Http http = new Http();
		HTTPResponse res;
		if (!Test.isRunningTest()) {
			res = http.send(req);
		}
			
		System.debug('BODY: ' + res.getBody());
		System.debug('STATUS: ' + res.getStatus());
		System.debug('STATUS_CODE: ' + res.getStatusCode());
			
		JSONObject jsonObj = new JSONObject(res.getBody());
		String accessToken = jsonObj.getValue('access_token').str;
		String instanceUrl = jsonObj.getValue('instance_url').str;

		System.debug('ACCESS TOKEN: ' + accessToken);
		System.debug('INSTANCE URL: ' + instanceUrl);
		
		return accessToken;		
	}
	*/
	
	/**
	 * Get a session id using SOAP login.  Returns the session id
	 * @params:
	 */
	private static String getSessionIdFromSOAP() {
		String userName = WORK_ALLOCATION_REPORT_USER_RECORD.userName;
		String password = WORK_ALLOCATION_REPORT_USER_RECORD.Password_Encrypted__c;
		HttpRequest loginReq= new HttpRequest();
		loginReq.setMethod('POST');
		loginReq.setTimeout(60000);
		loginReq.setEndpoint(ORG_ENDPOINT + '/services/Soap/u/22.0');
		loginReq.setHeader('Content-Type', 'text/xml;charset=UTF-8');

		loginReq.setHeader('SOAPAction', '""');
		String loginBody = '<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/"><Header/><Body><login xmlns="urn:partner.soap.sforce.com"><username>' + userName + '</username><password>' + password + '</password></login></Body></Envelope>';
		loginReq.setBody(loginBody);

		Http loginHttp = new Http();
		HttpResponse loginResponse;
		if (!Test.isRunningTest()) {
			loginResponse = loginHttp.send(loginReq);
		}

		system.debug('CSVReportEmailerDEBUG LOGIN RESPONSE: ' + loginResponse);
		system.debug('MPDEBUG LOGIN BODY: ' + loginResponse.getBody());
	    			   	
		// Parse XML response and get session id
		Dom.Document doc = loginResponse.getBodyDocument();
		Dom.XMLNode root = doc.getRootElement();
		String userSessionId = getSessionIdFromResponse(root);

	   	system.debug('CSVReportEmailerDEBUG RESPONSE SESSION ID: ' + userSessionId);
	   	return userSessionId;
	}
	
	/**
	 * Traverse the DOM.XMLNode passed in and find the sessionId element
	 * @params:	DOM.XMLNode node		The XMLNode for which to search for the sessionId tag
	 */
	@TestVisible
	private static String getSessionIdFromResponse(DOM.XMLNode node) {
		String sessionId;
   		system.debug('CSVReportEmailerDEBUG NODE NAME: ' + node.getName());
   		system.debug('CSVReportEmailerDEBUG NODE TEXT: ' + node.getText());
		if (node.getName() == 'sessionId') {
			sessionId = node.getText().trim();
		} else {
			for (Dom.XMLNode child : node.getChildElements()) {
				sessionId = getSessionIdFromResponse(child);
				if (String.isNotBlank(sessionId)) {
					return sessionId;
				}
			}
		}
		return sessionId;
	}
}