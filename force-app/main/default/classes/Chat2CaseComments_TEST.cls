/**
 * The purpose of this test is to check the functionality of adding a Live Agent Chat Transcript as a Case Comment.
 * The idea is that when a Chat Transcript is associated with a Case it causes a Case Comment to be written and associated with 
 * the Case.
 * 
 * There are many types of Case and each is processed in a specific way. Therefore we need to test each type of case.
 * 
 * At the time of writing it is expected that associating a Transcript with a Sub Case is not supported. 
 *
 * The LiveAgentTranscript trigger will gracefully associate the transcript with the Parent Case
 */
@isTest
private class Chat2CaseComments_TEST {

    // Simple chat transcripts
    private static final String TRANSCRIPT_TEXT_HTML = 'Project Team:T2 // <p align="center">Chat Started: Monday, December 01, 2014, 16:42:53 (+0000)</p><p align="center">Chat Origin: Buying Assistance</p><p align="center">Agent Neil J</p>( 1s ) Neil J: 01/12/2014: Welcome to John Lewis, how can I help you today?<br>( 6s ) Visitor: 265370<br>( 27s ) Neil J: done<br>';
    private static final String TRANSCRIPT_TEXT_PLAIN_FRAGMENT = '( 1s ) Neil J: 01/12/2014: Welcome to John Lewis, how can I help you today?';

    // Long Chat transcript
    private static final String KOREAN50 = '고려사람고고려사람고고려사람고고려사람고고려사람고고려사람고고려사람고고려사람고고려사람고고려사람고';
    private static final String KOREAN100 = KOREAN50 + KOREAN50;
    private static final String KOREAN1000 = KOREAN100 + KOREAN100 + KOREAN100 + KOREAN100 + KOREAN100 + KOREAN100 + KOREAN100 + KOREAN100 + KOREAN100 + KOREAN100;
    private static final String KOREAN2000 = KOREAN1000 + KOREAN1000;
    private static final String KOREAN4000 = KOREAN1000 + KOREAN1000 + KOREAN1000 + KOREAN1000;
    private static final String KOREAN8000 = KOREAN4000 + KOREAN4000;

    //
    // insert tests
    //
    static testMethod void complaint_test() {
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        testInsert('Complaint', null, TRANSCRIPT_TEXT_HTML, TRANSCRIPT_TEXT_PLAIN_FRAGMENT);
     }
    /* COPT-5272
    static testMethod void emailTriage_test() {
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        testInsert('Email_Triage', null, TRANSCRIPT_TEXT_HTML, TRANSCRIPT_TEXT_PLAIN_FRAGMENT);
    }
    COPT-5272 */
    
    static testMethod void nku_test() {
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        testInsert('NKU', null, TRANSCRIPT_TEXT_HTML, TRANSCRIPT_TEXT_PLAIN_FRAGMENT);
    }
    // pse sub case
    /* COPT-5272
    static testMethod void pseSubCase_test() {
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        testInsert('Sub_Query',null, TRANSCRIPT_TEXT_HTML, TRANSCRIPT_TEXT_PLAIN_FRAGMENT);
    } 
	COPT-5272 */
    
    // pse case
    static testMethod void productStockEnquiry_test() {
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        testInsert('Product_Stock_Enquiry', null, TRANSCRIPT_TEXT_HTML, TRANSCRIPT_TEXT_PLAIN_FRAGMENT);
    }
    
    static testMethod void query_test() {
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        testInsert('Query', null, TRANSCRIPT_TEXT_HTML, TRANSCRIPT_TEXT_PLAIN_FRAGMENT);
    }
    
    /* COPT-5272
    static testMethod void webTriage_test() {
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        testInsert('Web_Triage', null, TRANSCRIPT_TEXT_HTML, TRANSCRIPT_TEXT_PLAIN_FRAGMENT);
    }
    COPT-5272 */
    
    static testMethod void largeTranscript_test() {
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        // try adding a 8000 char transcript. CaseComment can only be 4000 bytes, approx 2000 chars. Check the first 1000 chars are as expected.
        testInsert('Query', null, KOREAN8000, KOREAN1000); 
    }
    
    static testMethod void emptyTranscript_test() {
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        // try adding a 8000 char transcript. CaseComment can only be 4000 bytes, approx 2000 chars. Check the first 1000 chars are as expected.
        testInsert('Query', null, '', '');     
    }

    // update tests
    
    /* This test commented out for now due to bizarre duplicate value in custom settings error
    static testMethod void update_test() {
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        // do insert and test results
        LiveChatTranscript lct = testInsert('Query', null, TRANSCRIPT_TEXT_HTML, TRANSCRIPT_TEXT_PLAIN_FRAGMENT);

        // Create a new case
        Id recordTypeId = getRecordTypeId('Query');
        Case newCase = createCase(recordTypeId, null);
        
        // set the transcript to point to the new case
        Id oldCaseId = lct.CaseId;
        lct.CaseId = newCase.Id;
        
        update lct;
        
        // requery the new case
        newCase = [SELECT Number_of_Email_Contacts__c, Number_of_Customer_Contacts__c, Number_of_Chat_Contacts__c, Number_of_Call_Contacts__c,Last_Customer_Contact__c FROM Case WHERE Id=:newCase.Id];
        system.assertNotEquals(null, newCase);

        // Test that the number of Chat Contacts got updated
        // Test that the total number of Contacts has been updated
        system.assertEquals(0, newCase.Number_of_Email_Contacts__c);
        system.assertEquals(1, newCase.Number_of_Customer_Contacts__c);
        system.assertEquals(1, newCase.Number_of_Chat_Contacts__c);
        system.assertEquals(0, newCase.Number_of_Call_Contacts__c);
        system.assertNotEquals(null, newCase.Last_Customer_Contact__c);

        // requery the old case
        Case oldCase = [SELECT Number_of_Email_Contacts__c, Number_of_Customer_Contacts__c, Number_of_Chat_Contacts__c, Number_of_Call_Contacts__c, Last_Customer_Contact__c FROM Case WHERE Id=:oldCaseId];
        system.assertNotEquals(null, oldCase);

        // Test that the number of Chat Contacts got updated
        // Test that the total number of Contacts has been updated
        system.assertEquals(0, oldCase.Number_of_Email_Contacts__c);
        system.assertEquals(0, oldCase.Number_of_Customer_Contacts__c);
        system.assertEquals(0, oldCase.Number_of_Chat_Contacts__c);
        system.assertEquals(0, oldCase.Number_of_Call_Contacts__c);
        system.assertEquals(null, oldCase.Last_Customer_Contact__c);
    }
    */

    //
    // Helpers
    //

    //
    // This is the main helper
    //
    // Param:
    // recordTypeName:          The record type of the case that is to be created
    // parentRecordTypeName:    If the record type is non-null then this is the record type of the parent
    // 
    static LiveChatTranscript testInsert(String recordTypeName, String parentRecordTypeName, String transcriptText, String caseCommentText) {
        // create case of specific record type
        Case cParent;
        Case cChild;
        Id cUnderTestId;
        LiveChatTranscript lct;
        
        Id recordTypeId = getRecordTypeId(recordTypeName);
        
        if (parentRecordTypeName != null) {
            Id parentRecordTypeId = getRecordTypeId(parentRecordTypeName);

            // if pse sub case
            cParent = createCase(parentRecordTypeId, null); 
            cChild = createCase(recordTypeId, cParent.Id);
            cUnderTestId = cChild.Id;           
        } else {
            cParent = createCase(recordTypeId, null); 
            cUnderTestId = cParent.Id;
        }
        
        Test.StartTest();
        
            // Create the transcript and associate the transcript with the case
            lct = createTranscript(cUnderTestId, transcriptText);
        
        Test.stopTest();
        
        // test that the case comment got created
        Integer expectedCaseComments = (String.isBlank(transcriptText)) ? 0 : 2;

        List<CaseComment> caseComments = [SELECT Id, CommentBody, ParentId FROM CaseComment];

        if (expectedCaseComments == 1) {
            // test CaseComment is associated with the parent case
            system.assertEquals(cParent.Id, caseComments[0].ParentId);
    
            // test that the contents got copied
            system.assert(caseComments[0].CommentBody.contains(caseCommentText));           
        }
        
        // Now requery the cases
        Case parentCase = [SELECT Number_of_Email_Contacts__c, Number_of_Customer_Contacts__c, Number_of_Chat_Contacts__c, Number_of_Call_Contacts__c FROM Case WHERE Id=:cParent.Id];
        system.assertNotEquals(null, parentCase);

        // Test that the number of Chat Contacts got updated
        // Test that the total number of Contacts has been updated
        system.assertEquals(0, parentCase.Number_of_Email_Contacts__c);
        system.assertEquals(1, parentCase.Number_of_Customer_Contacts__c);
        system.assertEquals(1, parentCase.Number_of_Chat_Contacts__c);
        system.assertEquals(0, parentCase.Number_of_Call_Contacts__c);

        // check the sub case
        if (parentRecordTypeName != null) {
            Case childCase = [SELECT Number_of_Email_Contacts__c, Number_of_Customer_Contacts__c, Number_of_Chat_Contacts__c, Number_of_Call_Contacts__c, recordTypeId FROM Case WHERE Id=:cChild.Id];
            system.assertNotEquals(null, childCase);
            system.debug('childCase: '+childCase);

            // should all be zero           
            system.assertEquals(0, childCase.Number_of_Email_Contacts__c);
            system.assertEquals(0, childCase.Number_of_Customer_Contacts__c);
            system.assertEquals(0, childCase.Number_of_Chat_Contacts__c);
            system.assertEquals(0, childCase.Number_of_Call_Contacts__c);
        }
                
        // Test that the transcript is still associated with the case (obviously pse sub case will not)
        // Note: the expected result is that the parent Case will be referred to. The trigger for LiveChatTranscript will move the associated case from the child to
        // the parent for a PSE subcase
        lct = [SELECT caseId FROM LiveChatTranscript WHERE Id=:lct.Id];
        system.assertNotEquals(null, lct);
        
        system.assertEquals(parentCase.Id, lct.caseId);
        
        return lct;
    }

    static Id getRecordTypeId(String recordTypeName) {
            
        List<RecordType> caseRecordTypes = [Select Id, SobjectType, DeveloperName From RecordType WHERE SobjectType='Case' AND DeveloperName=:recordTypeName];
        system.assertEquals(1, caseRecordTypes.size());
        return caseRecordTypes[0].Id;       
    }
 
    static Case createCase(Id recordTypeId, Id parentCaseId) {
        UnitTestDataFactory.createTeamAndContactCentre();
        UnitTestDataFactory.createEmailContactCentre();
        
        Config_Settings__c setting1 = new Config_Settings__c();
        setting1.Name = 'WS_Endpoint_ViewCustomerOrdersV1';
        setting1.Checkbox_Value__c = True;
        setting1.Text_Value__c = 'Test1';
        setting1.Description__c = 'Inserting customsettings1';
        setting1.Number_Value__c = 1;
        insert setting1;
        
        Config_Settings__c setting2 = new Config_Settings__c();
        setting2.Name = 'WS_Endpoint_EmailContent';
        setting2.Checkbox_Value__c = False;
        setting2.Text_Value__c = 'Test2';
        setting2.Description__c = 'Inserting customsettings2';
        setting2.Number_Value__c = 2;
        insert setting2;
        
        Config_Settings__c setting3 = new Config_Settings__c();
        setting3.Name = 'WS_Endpoint_ManageCustomerComments';
        setting3.Checkbox_Value__c = True;
        setting3.Text_Value__c = 'Test3';
        setting3.Description__c = 'Inserting customsettings3';
        setting3.Number_Value__c = 3;
        insert setting3;
        
        Config_Settings__c setting4 = new Config_Settings__c();
        setting4.Name = 'WS_Endpoint_SearchCustomer';
        setting4.Checkbox_Value__c = False;
        setting4.Text_Value__c = 'Test4';
        setting4.Description__c = 'Inserting customsettings4';
        setting4.Number_Value__c = 4;
        insert setting4;
        
        Config_Settings__c setting5 = new Config_Settings__c();
        setting5.Name = 'WS_Endpoint_ViewCustomerOrdersV2';
        setting5.Checkbox_Value__c = False;
        setting5.Text_Value__c = 'Test5';
        setting5.Description__c = 'Inserting customsettings5';
        setting5.Number_Value__c = 5;
        insert setting5;

        Account acc = UnitTestDataFactory.createAccount('Name');
        insert acc;
        
        Contact con = UnitTestDataFactory.createContact(acc);
        insert con;
        
        Case c = UnitTestDataFactory.createNormalCase(con);
        c.recordTypeId = recordTypeId;

        system.assertNotEquals(null, c);

        if (parentCaseId != null) {
            c.ParentId = parentCaseId;
        }
        insert c;
        system.assertNotEquals(null, c.Id);
        system.debug('c: '+c);
        return c;
    }

    static LiveChatTranscript createTranscript(Id caseId, String transcriptText) {
           
        LiveChatVisitor lcv = new LiveChatVisitor();        
        insert lcv;

        LiveChatTranscript lct = new LiveChatTranscript();
        system.assertNotEquals(null, lct);
        
        if (caseId != null) {
            lct.CaseId = caseId;
        }
        
        lct.Body = transcriptText;
        lct.LiveChatVisitorId = lcv.Id;
        
        insert lct;
        system.assertNotEquals(null, lct.Id);
        
        system.debug('lct: '+lct);
        return lct;
    }         
}