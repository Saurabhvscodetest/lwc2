/*************************************************
coSoapHelper

helper utility class for the coSoapManager class

Author: Steven Loftus (MakePositive)
Created Date: 13/11/2014
Modification Date: 
Modified By: 

Edit    By      Date        Description
001     NTJ     17/12/14    OCEP-1752 - Problems parsing an empty child e.g. <ProductCode/> so write a 'pointer' safe method
002     NTJ     22/12/14    OCEP-1752 - Fixed GetGrandchildString to get text
003     MP      06/07/15    CMP-479   - Add getSignedDecimal method to return negative numbers for Total Amount field
**************************************************/
public class coSoapHelper {

    public static String generateGuid() {

        Blob aes = Crypto.generateAesKey(128);
        String hex = EncodingUtil.convertToHex(aes);

        String result = hex.left(8) + '-' + hex.substring(8, 12) + '-' + hex.substring(12, 16) + '-' + hex.substring(16, 20) + '-' + hex.right(11);
        
        return result;
    }

    // attempt a string to decimal conversion safely - return null for zero
    public static Decimal getDecimal(String strDecimal){

        if (String.isBlank(strDecimal)) {
            return null;
        } else {
            Decimal retVal = null;
            try {
                retVal = Decimal.valueOf(strDecimal).setScale(2);
            } catch (Exception e) {
                System.debug('Decimal value present, but in wrong format!!');
            }
            return (retVal > 0) ? retVal : null;
        }        
    }    

    /**
     * <<03>> Attempt a string to decimal conversion safely - include zero and negatives
	 * @params:	String strDecimal			String to be converted
     */
    public static Decimal getSignedDecimal(String strDecimal){

        if (String.isBlank(strDecimal)) {
            return null;
        } else {
            Decimal retVal = null;
            try {
                retVal = Decimal.valueOf(strDecimal).setScale(2);
            } catch (Exception e) {
                System.debug('Decimal value present, but in wrong format!!');
            }
            return retVal;
        }        
    }    

    // attempt a string to integer conversion safely
    public static Integer getInteger(String strInteger){

        if (String.isBlank(strInteger)) {
            return null;
        } else {
            Integer retVal = null;
            try {
                retVal = Integer.valueOf(strInteger);
            } catch (Exception e) {
                System.debug('Integer value present, but in wrong format!!');
            }
            return retVal;
        }        
    }    

    // manipulating iso8601 timestamps via http://salesforce.stackexchange.com/questions/1013/parsing-an-iso-8601-timestamp-to-a-datetime
    public static DateTime getDateTime(String strDateTime) {

        if (String.isBlank(strDateTime)) {
            return null;
        } else {
            DateTime retVal = null;
            try {
                DateTime tempDateTime = (DateTime)JSON.deserialize('"' + strDateTime + '"', DateTime.class);
                retVal = DateTime.newInstance(tempDateTime.getTime());
            } catch (Exception e) {
                System.debug('dateTime value present, but in wrong format!!');
            }
            return retVal;
        }                    
    }

    // safe way of getting a child element
    public static String getChildString(Dom.XMLNode itemDetailNode, String childName, String childNameSpace) {
        system.debug('getChildString: '+childName);
        String result = '';
        
        Dom.XmlNode childNode = itemDetailNode.getChildElement(childName, childNameSpace);
        system.debug('getChildString: '+childNode);
        if (childNode != null) {
            result = childNode.getText();
        }
        
        system.debug('getChildString: result: '+result);
        return result;
    }
    
    // safe way of getting a grandchild element
    public static String getGrandChildString(Dom.XMLNode itemDetailNode, String childName, String childNameSpace, String grandChildName, String grandChildNameSpace) {
        system.debug('getGrandChildString: '+childName+' '+grandChildName);
        String result = '';
        
        Dom.XmlNode childNode = itemDetailNode.getChildElement(childName, childNameSpace);
        system.debug('childNode: '+childNode);
        if (childNode != null) {
            Dom.XmlNode grandChildNode = childNode.getChildElement(grandChildName, grandChildNameSpace);
            system.debug('grandChildNode: '+grandChildNode);
            if (grandChildNode != null) {
                result = grandChildNode.getText();              
            }
        }
        
        system.debug('getChildString: result: '+result);
        return result;
    }
    
    // safe way of testing child/grandchild exists
    public static Dom.XMLNode getGrandChild(Dom.XMLNode itemDetailNode, String childName, String childNameSpace, String grandChildName, String grandChildNameSpace) {
        system.debug('getGrandChild: '+childName+' '+grandChildName);
        Dom.XMLNode grandChildNode = null;
        
        Dom.XmlNode childNode = itemDetailNode.getChildElement(childName, childNameSpace);
        if (childNode != null) {
            grandChildNode = childNode.getChildElement(grandChildName, grandChildNameSpace);
        }
        
        return grandChildNode;
    }
}