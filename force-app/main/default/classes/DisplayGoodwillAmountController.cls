/*
* File Name     : DisplayGoodwillAmountController
* Description   : Controller class used for displaying the sum of 
* Client        : JLP
* =========================================================================== 
* Ver Date          Author                               Modification 
* --- ---- ------ ------------------------------------ ---------------------- 
* 1.0  15-Sep-2020   Ragesh G                                Created 
*/ 
public class DisplayGoodwillAmountController {
    
    public static String COMPLAINT_RECORDTYPEID ='012b0000000M1FS';
    public static String CASE_LIGHTNING_URL ='/lightning/r/Case/';
    public static String VIEW ='view';
    
     @AuraEnabled
    public static List < DisplayGoodwillAmountWrapper > fetchGoodwillAmountData ( String contactID ) {
        List < DisplayGoodwillAmountWrapper > goodwillDataList = new List < DisplayGoodwillAmountWrapper > ();
        for ( Case caseRecord : [  SELECT Id,CaseNumber, CreatedDate, ContactId,jl_Customer_Allowance__c,RecordTypeId FROM Case Where 		RecordTypeId=:COMPLAINT_RECORDTYPEID AND ContactId = :contactID ]) {
			if ( caseRecord.jl_Customer_Allowance__c != null ) {
				DisplayGoodwillAmountWrapper goodwillDataObj = new DisplayGoodwillAmountWrapper ();
                goodwillDataObj.caseNumberValue = caseRecord.CaseNumber;
                goodwillDataObj.caseNumberURL = CASE_LIGHTNING_URL + caseRecord.Id + '/'+VIEW;
                goodwillDataObj.goodwillAmount = caseRecord.jl_Customer_Allowance__c;
                goodwillDataObj.createdDate = formattedDateValue(''+caseRecord.CreatedDate);
                
                goodwillDataList.add(goodwillDataObj);
			}
		}
        
        System.debug(' goodwillDataList ' + goodwillDataList);
        return goodwillDataList;
        
    }
    
    public static String formattedDateValue ( String currentDate ) {
        String formattedDate = '';
        DateTime dateValue = Datetime.valueOf(currentDate.replace('T',' '));
        formattedDate = dateValue.date ().format();
        return formattedDate;
    }
    	
    public class DisplayGoodwillAmountWrapper{
        @AuraEnabled
        public string caseNumberValue;
        @AuraEnabled
        public Decimal goodwillAmount;
        @AuraEnabled
        public string createdDate;
        @AuraEnabled
        public string caseNumberURL;

    }
}