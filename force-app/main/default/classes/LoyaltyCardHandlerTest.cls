/**
*	@author: Andrey Gavrikov (westbrook)
*	@date: 2014-08-26
*	@description:
*	    Test methods for LoyaltyCardHandler
*	
*	Version History :   
*	2014-08-26 - MJL-768 - AG
*	initial version
*		
*/
@isTest
public class LoyaltyCardHandlerTest  {
    private static final String SHOPPER_ID = '1234abcdefg';
    private static final String PARTNERSHIP_CARD_NUMBER = '3456789456123';
    private static final String ORIGIN = 'Unit Test';
    private static final DateTime DATE_CREATED = DateTime.NOW(); 

    static {
        BaseTriggerHandler.addSkipReason(CalloutHandler.SKIP_ALL_NOTIFICATIONS_CODE, 'disable outbound events in this test class');
    }
    /************************ DATA PREPARATION *******************************************/
    /**
* this method will create numberOfCards Loyalty_Account__c and Loyalty_Card__c records,
* i.e. one Loyalty_Card__c per Loyalty_Account__c
*/
    
    public static List<Loyalty_Card__c> generateCards(final List<String> cardNumbers) {
        
        final List<Loyalty_Account__c> loyaltyAccounts = new List<Loyalty_Account__c>();
        for(Integer i = 0; i < cardNumbers.size(); i++) {
            loyaltyAccounts.add(new Loyalty_Account__c(Name = 'LA_' + i, Email_Address__c = 'la_' + i + '@test.com'));
        }
        Database.insert(loyaltyAccounts);
        final List<Loyalty_Card__c> cards = new List<Loyalty_Card__c>();
        Integer i = 0;
        for(String cardNumber : cardNumbers) {
            cards.add(new Loyalty_Card__c(Name = cardNumber, Loyalty_Account__c = loyaltyAccounts[i].Id));
            i++;
        }
        Database.insert(cards);
        return cards;
    }
    
    /**
* MJL-768
* check linking of orphaned Loyalty_Card__c-s
*
* Pre-test data
* 3 transactions, of which
* - one is linked to a card, Card_Number_Text_Version__c = "cArd_1"
* - two are orphaned (tran_2, tran_3): Card_Number_Text_Version__c = "Card_2", "caRD_3"
*
*/
    static testMethod void testOrphanLink () {
        Test.startTest();
        List<Loyalty_Card__c> cards = LoyaltyCardHandlerTest.generateCards(new List<String>{'card_1'});
        
        final List<Transaction_Header__c> trans = new List<Transaction_Header__c>{
            new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR1', Card_Number_Text_Version__c = 'card_1', Card_Number__c = cards[0].Id),
                new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR2', Card_Number_Text_Version__c = 'Card_2'),
                new Transaction_Header__c(Receipt_Barcode_Number__c = 'TR3', Card_Number_Text_Version__c = 'caRD_3')
                };
                    Database.insert(trans);
        
        System.assertEquals(2, [select count() from Transaction_Header__c where Receipt_Barcode_Number__c in ('TR1', 'TR2', 'TR3') and Card_Number__c = null], 
                            'expected  TR2, TR3 not to be linked to existing card');
        
        //unlink TR1
        trans[0].Card_Number__c = null;
        Database.update(trans[0]);
        
        //now update card1 and insert card2 & card3
        Loyalty_Card__c card1 = cards[0];
        card1.Force_Orphan_Transaction_Search__c = true;
        
        Loyalty_Card__c card2 = new Loyalty_Card__c(Name = 'card_2', Loyalty_Account__c = cards[0].Loyalty_Account__c);
        Loyalty_Card__c card3 = new Loyalty_Card__c(Name = 'card_3', Loyalty_Account__c = cards[0].Loyalty_Account__c);
        
        Database.upsert(new Loyalty_Card__c[] {card1, card2, card3});
        
        if (LoyaltyCardHandler.OPTIMISED_JOIN_CODE) {
            Set<Id> cardSet = new Set<Id>();
            cardSet.add(card1.Id);
            cardSet.add(card2.Id);
            cardSet.add(card3.Id);
            
            MyJL_JoinLoyaltyAccountHandler.postJoinProcessCards(cardSet);
        }
        
        Test.stopTest();
        
        //check that TR1 is still linked to card1
        System.assertEquals('card_1', [select Card_Number__r.Name from Transaction_Header__c where Receipt_Barcode_Number__c = 'TR1'].Card_Number__r.Name, 
                            'expected TR1 to be still linked to existing card_1');
        
        //check that TR2 is NOW linked to card2
        System.assertEquals('card_2', [select Card_Number__r.Name from Transaction_Header__c where Receipt_Barcode_Number__c = 'TR2'].Card_Number__r.Name, 
                            'expected TR2 to be linked to new card_2');
        
        //check that TR3 is NOW linked to card3
        System.assertEquals('card_3', [select Card_Number__r.Name from Transaction_Header__c where Receipt_Barcode_Number__c = 'TR3'].Card_Number__r.Name, 
                            'expected TR3 to be linked to new card_3');
    }
    
    static testmethod void insertParnershipCard(){
        
        final List<Loyalty_Account__c> loyaltyAccounts = new List<Loyalty_Account__c>();
        loyaltyAccounts.add(new Loyalty_Account__c(Name = PARTNERSHIP_CARD_NUMBER ,Customer_Loyalty_Account_Card_ID__c = PARTNERSHIP_CARD_NUMBER ,ShopperId__c=SHOPPER_ID,IsActive__c = true,Membership_Number__c = PARTNERSHIP_CARD_NUMBER));
        insert loyaltyAccounts;
        
        final List<Loyalty_Card__c> cards = new List<Loyalty_Card__c>();
        cards.add(new Loyalty_Card__c(Name = PARTNERSHIP_CARD_NUMBER,Card_Type__c = 'My Partnership',Disabled__c = false , Loyalty_Account__c = loyaltyAccounts[0].Id));
        
        Test.startTest();
        try
        {
            insert cards;
        }Catch(Exception e)
        {
            System.debug('Exception'+e.getMessage());
        }	
        Test.stopTest();
        
        
    }
    
    static testmethod void AccountUpdate(){
        
        List<Loyalty_Card__c> cards = LoyaltyCardHandlerTest.generateCards(new List<String>{'card_1'});
          
        for(Loyalty_card__c cas : cards)
        {
            cas.Card_Type__c = 'My Partnership'; 
            cas.Disabled__c = true;
            cas.Card_Number__c = PARTNERSHIP_CARD_NUMBER;
        }
        
        Test.startTest();
        try
        {
            update cards;
        }Catch(Exception e)
        {
            System.debug('Exception'+e.getMessage());
        }	
        Test.stopTest();
        Integer UpdatedAccountSize = [select count() from Loyalty_Card__c where Card_Type__c='My Partnership' and Disabled__c= true ];
        System.assertEquals(1, UpdatedAccountSize);
        
    	Loyalty_Account__c LoyaltyAccountList = [select Membership_Number__c,MyJL_Old_Membership_Number__c from Loyalty_Account__c];
        System.assertEquals(LoyaltyAccountList.MyJL_Old_Membership_Number__c, LoyaltyAccountList.Membership_Number__c);
    }
    
    static testmethod void DeleteCards(){
        List<Loyalty_Card__c> cards = LoyaltyCardHandlerTest.generateCards(new List<String>{'card_1'});
        
        for(Loyalty_card__c cas : cards)
        {
            cas.Card_Type__c = 'My Partnership'; 
        }
        
        update cards;
        Test.startTest();
        try
        {
            delete cards;
        }Catch(Exception e)
        {
            System.debug('Exception'+e.getMessage());
        }	
        Test.stopTest();
        Loyalty_Account__c LoyaltyAccountList = [select Membership_Number__c,MyJL_Old_Membership_Number__c from Loyalty_Account__c];
        System.assertEquals(LoyaltyAccountList.MyJL_Old_Membership_Number__c, LoyaltyAccountList.Membership_Number__c);
        
    }
    

     static testMethod void testUpdatePartnershipLoyaltyAccount(){
        Contact customer = new Contact(LastName = 'Test', SourceSystem__c='johnlewis.com', Shopper_ID__c='123456789123');
        Database.insert(customer);
        
        Loyalty_Account__c account = new Loyalty_Account__c( Email_Address__c = 'test666@test.com', Contact__c = customer.id,
                                                            ShopperID__c = customer.Shopper_ID__c,
                                                            Voucher_Preference__c = 'Digital',
                                                            IsActive__c = true);
        Database.insert(account);
        Loyalty_Card__c myJLCard = new Loyalty_Card__c(Loyalty_Account__c = account.Id, Name = 'myJL-card-id', Card_Type__c = MyJL_Const.MY_JL_CARD_TYPE);
        Database.insert(myJLCard);
        CalloutHandlerTest.initCalloutSettings(MyJL_LoyaltyAccountHandler.FULL_JOIN_NOTIFY, 'MyJL_LoyaltyAccountHandler.JoinLeaveCallout', 10);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        RestContext.request = req;
        RestContext.response = res;
        
        req.requestURI = '/MyLoyaltyService/Subscribe/' + customer.Shopper_ID__c;
        req.httpMethod = 'PUT';
        
        Test.startTest();
        BaseTriggerHandler.removeSkipReason('JoinNotification');
        
        MyLoyaltyService.joinShopper( PARTNERSHIP_CARD_NUMBER, ORIGIN, DATE_CREATED);
        
        Contact contactProfile = [SELECT Id, Name, (SELECT Id, Name, IsActive__c, Join_Date__c, ShopperId__c,
                                                    Membership_Number__c, Scheme_Joined_Date_Time__c, 
                                                    Activation_Date_Time__c, Deactivation_Date_Time__c, 
                                                    contact__c, contact__r.id, 
                                                    contact__r.Name, Channel_ID__c 
                                                    FROM MyJL_Accounts__r WHERE IsActive__c = true ORDER BY Join_Date__c),
                                  Shopper_Id__c, AnonymousFlag2__c, Birthdate, Salutation,
                                  firstname, lastname, email, MobilePhone, Mailing_House_No_Text__c,
                                  Mailing_House_Name__c, mailingstreet, mailingcity, mailingstate,
                                  mailingcountry, mailingpostalcode, mailingcountrycode, lastmodifieddate
                                  FROM contact
                                  WHERE Id =: customer.Id];    
        SFDCMyJLCustomerTypes.Customer customerToBeSent = MyJL_MappingUtil.populateCustomer2(contactProfile, null); 
        
        SFDCMyJLCustomerTypes.CustomerAccount customerAccount = customerToBeSent.CustomerAccount[0];
        System.assertNOTEquals(NULL, customerAccount.ActivationDateTime); 
        System.assertEquals(customerAccount.SchemeJoinedDateTime, customerAccount.ActivationDateTime); 
        
    }
    
}