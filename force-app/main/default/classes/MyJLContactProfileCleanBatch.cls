/******************************************************************************
* @author       Matt Povey
* @date         21/12/2015
* @description  Batch class for cleaning up erroneous Contact Profile records
*				created by MyJL sign up process.  To run in Exec Anon:
*				MyJLContactProfileCleanBatch b = new MyJLContactProfileCleanBatch();
* 				Database.executeBatch(b);
*
* EDIT RECORD
*
* LOG     DATE        Author	JIRA		COMMENT      
* 001     21/12/2015  MP		MJL-2154	Original code
*
*******************************************************************************
*/
global class MyJLContactProfileCleanBatch extends MyJLCleanBatchBase implements Database.Batchable<SObject>, Database.Stateful, Schedulable {
	public static final String CLASS_NAME = 'MyJLContactProfileCleanBatch';
	
	/**
	 * Override no args constructor to initialise settings
	 * @params: none
	 */
	global MyJLContactProfileCleanBatch() {
		super(CLASS_NAME);
	}
	
    /**
     * execute - Schedule the batch job
	 * @params:	Database.SchedulableContext sc	Schedulable Context from system.
     */
	global void execute(SchedulableContext sc){
        if (processEnabled) {
        	MyJLContactProfileCleanBatch contactProfileCleanJob = new MyJLContactProfileCleanBatch();
        	Database.executeBatch(contactProfileCleanJob);
        }
	}

    /**
     * Start - Select all Contact Profiles where:
     * Name starts with "a00b00"
     * Mailing Street is null
     * Contact has more than one Contact Profile record
	 * @params:	Database.BatchableContext BC	Batchable Context from system.
     */
	global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([SELECT Id, Name, Contact__c, Mailing_Street__c, 
   	    										Contact__r.Name, Shopper_ID__c, (SELECT Id, Name, Membership_Number__c, Scheme_Joined_Date_Time__c, Activation_Date_Time__c, Deactivation_Date_Time__c, IsActive__c, Channel_ID__c FROM MyJL_Accounts__r)
       									 FROM Contact_Profile__c 
       									 WHERE Name LIKE 'a00%' AND Mailing_Street__c = null AND Contact__r.Contact_Profile_Count__c > 1]);
	}
	
    /**
     * Execute - build list of matching records for reporting and delete if specified by custom setting.
	 * @params:	Database.BatchableContext BC		Batchable Context from system.
	 *			List<Contact_Profile__c> cpList		Batch of Contact Profile with issues
     */
	global void execute(Database.BatchableContext BC, List<Contact_Profile__c> cpList){
		String headerStyle = ' nowrap="" style="border:1pt solid windowtext;padding:0cm 5.4pt;height:15pt;background:rgb(215,228,188)"';
		String rowStyle = ' nowrap="" valign="bottom" style="width:14.48%;border-style:none solid solid;border-right-color:windowtext;border-bottom-color:windowtext;border-left-color:windowtext;border-right-width:1pt;border-bottom-width:1pt;border-left-width:1pt;padding:0cm 5.4pt;height:15pt"';
		if (processEnabled) {
			if (emailReportEnabled) {
				if (reportBodyHTML == null) {
					reportBodyHTML = '<table ' + tableStyle + '>';
					reportBodyHTML += '<tr>';
					reportBodyHTML += '<td ' + headerStyle + '><b>Profile ID</b></td>';
					reportBodyHTML += '<td ' + headerStyle + '><b>Contact Name</b></td>';
					reportBodyHTML += '<td ' + headerStyle + '><b>Contact ID</b></td>';
					reportBodyHTML += '<td ' + headerStyle + '><b>Shopper ID</b></td>';
					reportBodyHTML += '<td ' + headerStyle + '><b>LA Membership No</b></td>';
					reportBodyHTML += '<td ' + headerStyle + '><b>LA Join Date</b></td>';
					reportBodyHTML += '<td ' + headerStyle + '><b>LA Activation Date</b></td>';
					reportBodyHTML += '<td ' + headerStyle + '><b>LA Deactivation Date</b></td>';
					reportBodyHTML += '<td ' + headerStyle + '><b>LA Active</b></td>';
					reportBodyHTML += '<td ' + headerStyle + '><b>LA Source</b></td>';
					reportBodyHTML += '</tr>';
				}
				for (Contact_Profile__c cp : cpList) {
					Integer laCount = cp.MyJL_Accounts__r.size();
					if (reportBody == null) {
						reportBody = 'PROFILE ID: ' + cp.Id + ', CONTACT NAME: ' + cp.Contact__r.Name + ', CONTACT ID: ' + cp.Contact__c + ', SHOPPER ID: ' + cp.Shopper_ID__c + ', LOYALTY ACCOUNTS: ' + laCount;
					} else {
						reportBody += '\nPROFILE ID: ' + cp.Id + ', CONTACT NAME: ' + cp.Contact__r.Name + ', CONTACT ID: ' + cp.Contact__c + ', SHOPPER ID: ' + cp.Shopper_ID__c + ', LOYALTY ACCOUNTS: ' + laCount;
					}
					reportBodyHTML += '<tr>';
					reportBodyHTML += '<td ' + rowStyle + '>' + cp.Id + '</td>';
					reportBodyHTML += '<td ' + rowStyle + '>' + cp.Contact__r.Name + '</td>';
					reportBodyHTML += '<td ' + rowStyle + '>' + cp.Contact__c + '</td>';
					reportBodyHTML += '<td ' + rowStyle + '>' + cp.Shopper_ID__c + '</td>';
					if (laCount == 0) {
						addBlankReportCells(6);
						reportBodyHTML += '</tr>';
					} else {
						Integer pos = 0;
						for (Loyalty_Account__c la : cp.MyJL_Accounts__r) {
							if (pos > 0) {
								// Put in blanks so no duplication of Contact Profile details
								reportBodyHTML += '<tr>';
								addBlankReportCells(4);
							}
							reportBodyHTML += '<td ' + rowStyle + '>' + la.Membership_Number__c + '</td>';
							reportBodyHTML += '<td ' + rowStyle + '>' + la.Scheme_Joined_Date_Time__c + '</td>';
							reportBodyHTML += '<td ' + rowStyle + '>' + la.Activation_Date_Time__c + '</td>';
							reportBodyHTML += '<td ' + rowStyle + '>' + la.Deactivation_Date_Time__c + '</td>';
							reportBodyHTML += '<td ' + rowStyle + '>' + la.IsActive__c + '</td>';
							reportBodyHTML += '<td ' + rowStyle + '>' + la.Channel_ID__c + '</td>';
							reportBodyHTML += '</tr>';
							pos++;
						}
					}
				}
			}
			totalRecordCount += cpList.size();
			if (recordUpdatesEnabled && totalRecordCount > 0) {
				try {
					delete cpList;
				} catch (Exception ex) {
					sendErrorEmail(CLASS_NAME + ' FATAL ERROR', ex.getMessage());
				}
			}
		}
	}	
	
    /**
     * Finish - send notification email.
	 * @params:	Database.BatchableContext BC	Batchable Context from system.
     */
    global void finish(Database.BatchableContext bc){
		// Close off report html table
		if (emailReportEnabled) {
			reportBodyHTML += '</table>';
		}
		sendReportEmail(CLASS_NAME, 'CONTACT PROFILE', 'DELETED');
	}
}