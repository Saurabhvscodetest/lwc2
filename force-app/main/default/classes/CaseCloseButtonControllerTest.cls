@isTest
private class CaseCloseButtonControllerTest {

	@testSetup static void initEntitlementTestData() {
		MilestoneUtilsTest.initEntitlementTestData();
	}
	


	@isTest static void testCaseWithOpenTasks() {

		User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		User tier1User;

		System.runAS(runningUser) {
			tier1User = UnitTestDataFactory.getFrontOfficeUser('TestUser');
			insert tier1User;
		}

		System.runAs(tier1User) {

			Case openCase = createCase();
			insert openCase;

			Task relatedTask = new Task();
			relatedTask.WhatId = openCase.Id;
			relatedTask.WhoId = openCase.ContactId;
			relatedTask.ActivityDate = Date.today();
			relatedTask.Type = 'Test relatedTask';
			relatedTask.Subject = 'Test Subject';
			relatedTask.Status = 'New';
			relatedTask.JL_Created_From_Case__c = false;
			relatedTask.JL_Team__c = 'CST';
			insert relatedTask;

			openCase = [SELECT Id, JL_NumberOutstandingTasks__c FROM Case WHERE Id = :openCase.Id];

			System.assertEquals(1, openCase.JL_NumberOutstandingTasks__c);

			// Now press the Close Case button
			ApexPages.StandardController standardController = new ApexPages.StandardController(openCase);
			CaseCloseButtonController customController = new CaseCloseButtonController(standardController);

			PageReference returnedPage = customController.validate();

			// Assert was not redirected to Close Case screen
			System.assertEquals(null, returnedPage);

			List<ApexPages.Message> pageMessages = ApexPages.getMessages();
			SYstem.assertEquals(1, pageMessages.size());
			System.assertEquals('This case has 1 or more open activities. Please review and close all open activities prior to closing this case.', pageMessages[0].getDetail());

		}


	}




	@isTest static void testCaseWithoutOpenTasks() {

		User runningUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		User tier1User;

		System.runAs(runningUser) {
			tier1User = UnitTestDataFactory.getFrontOfficeUser('TestUser');
			insert tier1user;
		}

		System.runAs(tier1User) {

			Case openCase = createCase();
			insert openCase;

			openCase = [SELECT Id, IsClosed, JL_NumberOutstandingTasks__c FROM Case WHERE Id = :openCase.Id];

			System.assertEquals(false, openCase.IsClosed);
			System.assertEquals(0, openCase.JL_NumberOutstandingTasks__c);

			// Now press the Close Case button
			ApexPages.StandardController standardController = new ApexPages.StandardController(openCase);
			CaseCloseButtonController controllerExtension = new CaseCloseButtonController(standardController);

			PageReference returnedPage = controllerExtension.validate();

			String returnedPageUrl = returnedPage.getUrl();

			String expectedUrl = '/'+openCase.Id+'/s?cas7=Closed+-+Resolved&retURL=%2F'+openCase.Id;

			// Assert the Close Case screen was returned
			System.assertEquals(expectedUrl, returnedPageUrl);


		}

	}


	private static Case createCase() {

		Case newCase = UnitTestDataFactory.createCase(UnitTestDataFactory.createContact().Id);
		newCase.Bypass_Standard_Assignment_Rules__c = true;
		newCase.JL_Branch_master__c = 'Oxford Street';
		newCase.Contact_Reason__c = 'Pre-Sales';
		newCase.Reason_Detail__c = 'Buying Office Enquiry';
		
		return newCase;

	}

}