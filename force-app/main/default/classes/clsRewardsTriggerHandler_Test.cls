@isTest
private class clsRewardsTriggerHandler_Test {

    static testMethod void rewardsParentingNewWelcomePack() {
        
      PageReference pageRef = Page.myJL_MembershipDetails;
        Test.setCurrentPage(pageRef); 
        
        //Prepare Test Data
        Contact con =  UnitTestDataFactory.createContact();
        Insert con;
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        
        Contact_Profile__c cp = new Contact_Profile__c(FirstName__c='Harry', LastName__c='Hippo', contact__c=con.id,email__c='harry.hoppo@animals.org');
		cp.Shopper_ID__c = 'shopper1';
		cp.SourceSystem__c = 'johnlewis.com';
		cp.Mailing_House_No_Text__c = '15';
		cp.Mailing_House_Name__c = 'Wixford';
		cp.Mailing_Street__c = 'Oakfield';
		cp.Mailing_Address_Line2__c = 'Block A';
		cp.Mailing_Address_Line3__c = 'Square C';
		cp.Mailing_Address_Line4__c = 'Area E';
		cp.MailingCity__c = 'Liverpool';
		cp.Mailing_County_Name__c = 'Merseyside';
		cp.MailingCountry__c = 'UK';
		cp.MailingPostCode__c = 'L4 2QH';
        insert cp;
        
        Loyalty_Account__c LA = new Loyalty_Account__c(Name='00000000012', Email_Address__c = 'harry.hoppo@animals.org', IsActive__c = true, ShopperId__c='shopper1');
        LA.Customer_Loyalty_Account_Card_ID__c = 'TestCard';
        LA.Channel_ID__c = 'johnlewis.com';
        LA.Customer_Profile__c = cp.id;
        LA.Activation_Date_Time__c = datetime.now();
        Insert LA;
        
        Loyalty_Card__c LC = new Loyalty_Card__c(Name = 'TestCard', Loyalty_Account__c = LA.Id);
        insert Lc;
        
        ApexPages.standardController controller = new ApexPages.standardController(con);
        myJL_MembershipDetailsExtension pge = new myJL_MembershipDetailsExtension(controller);
        myJL_MembershipDetailsExtension.ProfileWithLoyaltyAccount myJlReq = new myJL_MembershipDetailsExtension.ProfileWithLoyaltyAccount(con.id,con,LA);
        myJLReq.getMyJLNewRequest();
        
        Test.setCurrentPage(myJLReq.getMyJLNewRequest()); 
        
        ApexPages.standardController controller1 = new ApexPages.standardController(con);
        myJL_InformationPageController pge1 = new myJL_InformationPageController(controller1);
        
        pge1.goback();
    
        Case ca =  UnitTestDataFactory.createNormalCase(con);
        ca.Request_type__c = 'Replacement Welcome Pack';
        ca.fast_track__c = true;
        
        
        Test.setCurrentPage(pge1.myjlnewrequest());
        ApexPages.standardController controller2 = new ApexPages.standardController(ca);
        myJLRequestCreateCaseExtension pge2 = new myJLRequestCreateCaseExtension(controller2);
        pge2.cancel();
        pge2.save();
        
        Test.startTest();
        List<Case> getcase = new list<Case>([SELECT id,despatch_status__c,status FROM Case]);
       
        
        List<MyJL_Outbound_Staging__c> getOutst = new list<MyJL_Outbound_Staging__c>([select id,Case_Reference_ID__c,MYJL_Membership_Number__c from MyJL_Outbound_Staging__c where Case_Reference_ID__c =:getcase[0].Id]);
        
        system.assertequals(1,getOutst.size());

        Direct_Mail_Update__c dmu = new Direct_Mail_Update__c();
        dmu.MYJL_Membership_Number__c = getOutst[0].MYJL_Membership_Number__c;
        dmu.Despatched__c = date.today();
        insert dmu;

        
        List<Direct_Mail_Update__c> getDms = new list<Direct_Mail_Update__c>([select id,Mailing_Type__c,MYJL_Membership_Number__c,Unique_Identifier__c from Direct_Mail_Update__c where id = :dmu.id]);

        system.assertequals('New Welcome Pack',getDms[0].Mailing_Type__c);   
        
        
        Rewards__c rew = new Rewards__c();
        rew.MYJL_Membership_Number__c = getOutst[0].MYJL_Membership_Number__c;
        insert rew;
        
        list<Rewards__c> getrew = new list<Rewards__c>([select id,Mailing_Type__c,Direct_Mail_Update__c from Rewards__c where id = :rew.id]);
        
        system.assertequals('New Welcome Pack',getrew[0].Mailing_Type__c);   
        system.assertequals(dmu.id,getrew[0].Direct_Mail_Update__c);  
        
    }
    
     static testMethod void replacementPack() {
      PageReference pageRef = Page.myJL_MembershipDetails;
        Test.setCurrentPage(pageRef); 
        
        //Prepare Test Data
        Contact con =  UnitTestDataFactory.createContact();
        Insert con;
        UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
        
        Contact_Profile__c cp = new Contact_Profile__c(FirstName__c='Harry', LastName__c='Hippo', contact__c=con.id,email__c='harry.hoppo@animals.org');
		cp.Shopper_ID__c = 'shopper1';
		cp.SourceSystem__c = 'johnlewis.com';
		cp.Mailing_House_No_Text__c = '15';
		cp.Mailing_House_Name__c = 'Wixford';
		cp.Mailing_Street__c = 'Oakfield';
		cp.Mailing_Address_Line2__c = 'Block A';
		cp.Mailing_Address_Line3__c = 'Square C';
		cp.Mailing_Address_Line4__c = 'Area E';
		cp.MailingCity__c = 'Liverpool';
		cp.Mailing_County_Name__c = 'Merseyside';
		cp.MailingCountry__c = 'UK';
		cp.MailingPostCode__c = 'L4 2QH';
        insert cp;
        
        Loyalty_Account__c LA = new Loyalty_Account__c(Name='00000000012', Email_Address__c = 'harry.hoppo@animals.org', IsActive__c = true, ShopperId__c='shopper1');
        LA.Customer_Loyalty_Account_Card_ID__c = 'TestCard';
        LA.Channel_ID__c = 'johnlewis.com';
        LA.Customer_Profile__c = cp.id;
        LA.Activation_Date_Time__c = datetime.now();
        Insert LA;
        
        Loyalty_Card__c LC = new Loyalty_Card__c(Name = 'TestCard', Loyalty_Account__c = LA.Id);
        insert Lc;
        
        ApexPages.standardController controller = new ApexPages.standardController(con);
        myJL_MembershipDetailsExtension pge = new myJL_MembershipDetailsExtension(controller);
        myJL_MembershipDetailsExtension.ProfileWithLoyaltyAccount myJlReq = new myJL_MembershipDetailsExtension.ProfileWithLoyaltyAccount(con.id,con,LA);
        myJLReq.getMyJLNewRequest();
        
        Test.setCurrentPage(myJLReq.getMyJLNewRequest()); 
        
        ApexPages.standardController controller1 = new ApexPages.standardController(con);
        myJL_InformationPageController pge1 = new myJL_InformationPageController(controller1);
        
        pge1.goback();
      
        Case ca =  UnitTestDataFactory.createNormalCase(con);
        ca.Request_type__c = 'Replacement Welcome Pack';
        ca.fast_track__c = true;
        
        
        Test.setCurrentPage(pge1.myjlnewrequest());
        ApexPages.standardController controller2 = new ApexPages.standardController(ca);
        myJLRequestCreateCaseExtension pge2 = new myJLRequestCreateCaseExtension(controller2);
        pge2.cancel();
        pge2.save();
        
        Test.startTest();
        List<Case> getcase = new list<Case>([SELECT id,despatch_status__c,status FROM Case]);
        
        List<MyJL_Outbound_Staging__c> getOutst = new list<MyJL_Outbound_Staging__c>([select id,Case_Reference_ID__c,MYJL_Membership_Number__c from MyJL_Outbound_Staging__c where Case_Reference_ID__c =:getcase[0].Id]);
        
        system.assertequals(1,getOutst.size());
        system.assertequals('Closed - No Response Required',getcase[0].status); 
        system.assertequals('Processing',getcase[0].despatch_status__c); 
        
        Direct_Mail_Update__c dmu = new Direct_Mail_Update__c();
        dmu.Case_Reference_ID__c = getcase[0].Id;
        dmu.MYJL_Membership_Number__c = getOutst[0].MYJL_Membership_Number__c;
        dmu.Despatched__c = date.today();
        insert dmu;
        
        
        Rewards__c rew = new Rewards__c();
        rew.MYJL_Membership_Number__c = getOutst[0].MYJL_Membership_Number__c;
        rew.Case_Reference_ID__c = getcase[0].Id;
        insert rew;
        
        list<Rewards__c> getrew = new list<Rewards__c>([select id,Mailing_Type__c,Direct_Mail_Update__c from Rewards__c where id = :rew.id]);
        
        system.assertequals('Replacement welcome pack',getrew[0].Mailing_Type__c);   
        system.assertequals(dmu.id,getrew[0].Direct_Mail_Update__c);  
        
    }
}