/***************************************************************************************
* ScheduleCopyAttachments
**
**  Edit    Date            By          Comment
**  001     17/12/15        Ram          Changes made to the constructor of the Scheudler to accept          
**                                       Collection of caseIds ( Specific Processing)
**                                       Added Condition to fetch total Body Length of Attachments
**                                       Limiting the Total Attachments to Process to be Half the size of allowed heap limit.
*/
global class ScheduleCopyAttachments implements Schedulable{

global Set<id> caseIds;
    
    global void execute(SchedulableContext SC) {
      
        List<Case> caseList = [select Id, Pending_Email_Messages__c from Case where Pending_Email_Messages__c != null and id in :caseIds];
        List<String> emailMessageIdList = new List<String>();
        Map<Id,Id> emailMessageCaseMap = new Map<Id,Id>();
        
        if(caseList.size() > 0){ 
            for(Case c: caseList){
                emailMessageIdList.addAll(c.Pending_Email_Messages__c.split(';'));
                c.Pending_Email_Messages__c = '';
            }
            
            List<EmailMessage> emList = [select Id, ParentId from EmailMessage where Id IN: emailMessageIdList];
            for(EmailMessage em : emList){
                emailMessageCaseMap.put(em.Id, em.ParentId);
            }
            // Email Message Id is a List - Need to Change to Set
            Set<String> emailMessageSet = new Set<String>();
            for (String s: emailMessageIdList)
            {
                emailMessageSet.add(s);
            }
           // Note :- We are Processing only attachments where total Attachment size is less than Half of Heap size
           List<AggregateResult> results= [Select Count(BodyLength) from Attachment where ParentId IN :emailMessageSet ];
           Integer bodyLengthBytes =(Integer)results[0].get('expr0');
           if( bodyLengthBytes <= (Limits.getLimitHeapSize()/2) ){
            List<Attachment> attList = [select Id, ParentId, Body, Name  from Attachment where ParentId IN :emailMessageSet ];
            
            List<Attachment> attToInsert = new List<Attachment>();
            if(attList.size() > 0){
                for(Attachment att : attList){
                    Attachment newAtt = att.clone();
                    newAtt.parentId = emailMessageCaseMap.get(att.ParentId);
                    attToInsert.add(newAtt);
                }
                
                insert attToInsert;
            }
          }  
            update caseList;
        }   
        
       System.abortJob(SC.getTriggerId());
   }
   
   global  ScheduleCopyAttachments (Set<Id> csId)
   {
       caseIds = csId;
   }
}