/*
 Manges the configuration of the exceptions raised from EventHub. 
*/
public class EHExceptionMapper {
	/* Map to hold the mapping between DeliveryType-Exception Name - to the 
	EHExceptionToCaseInformation*/ 
	static map<String, EHExceptionToCaseInformation> ehExceptionsMap     = new map<String, EHExceptionToCaseInformation>();
	
	public enum CaseType {QUERY, COMPLAINT}
    
    static {
    	/*
    	 EH_Exception_Mapper__c holds the exception information in the format
    	 Index, DeliveryType, Exception name , Case Type and Case Description
    	*/
        Map<String, EH_Exception_Mapper__c> tempExpMap = EH_Exception_Mapper__c.getAll();
        if(tempExpMap != null && tempExpMap.size() > 0){
            for(String key:  tempExpMap.keySet()){
           		//Construct the Exception Information
                EH_Exception_Mapper__c exceptionInfo = tempExpMap.get(key);
                EHExceptionToCaseInformation excepinfo = new EHExceptionToCaseInformation();
                excepinfo.ehCasetype = (CaseType.QUERY.name().equalsIgnoreCase(exceptionInfo.Case_Type__c) ? CaseType.QUERY : CaseType.COMPLAINT);
                excepinfo.isTaskToBeCreated = exceptionInfo.Handle_Exception__c;
                excepinfo.exceptionName = exceptionInfo.Exception_Name__c;
                excepinfo.deliveryType = exceptionInfo.Delivery_Type__c;
                excepinfo.taskDescription = exceptionInfo.Description__c;
                excepinfo.isTwoWayEnabled = exceptionInfo.Two_Way_Enabled__c;
                excepinfo.extendedPromiseDuration = exceptionInfo.Promise_Start_Time_Delay__c;
                excepinfo.extendedPromiseWithBusinessEndDate = exceptionInfo.Promise_Hours_Before_Business_EndTime__c;
             	/*
            		Populates the maps with the below Info 
            		Key   - DeliveryType-ExceptionName
            		Value - EHExceptionToCaseInformation  
            	*/
                ehExceptionsMap.put((exceptionInfo.Delivery_Type__c + EHConstants.SPLITTER + exceptionInfo.Exception_Name__c).toUppercase(),excepinfo);
            }
        }
    }
    
    /*
       Checks if the config exists for the provided inputs , if it exists return the isTaskToBeCreated field 
    */
    public static boolean isExceptionNeededToBeHandled(String deliveryType,String exceptionName){
    	boolean isTaskToBeCreated = false;
		if(!ehExceptionsMap.isEmpty() ){
			EHExceptionToCaseInformation expToCase = ehExceptionsMap.get((deliveryType + EHConstants.SPLITTER  +  exceptionName).toUppercase());
			isTaskToBeCreated = (expToCase != null) ? expToCase.isTaskToBeCreated :  false;
		} 
		return isTaskToBeCreated;   	
    }
    
    /*
    Get the description for the DeliveryType-ExceptionName from the Map.
    */
    public static EHExceptionToCaseInformation getDescriptionForException(String deliveryType,String exceptionName){
        return ehExceptionsMap.get((deliveryType + EHConstants.SPLITTER  +  exceptionName).toUppercase());
    }

	// Get all the values of given exception name 
    public static EHExceptionToCaseInformation getExceptionInfoForException(String exceptionName){
        return ehExceptionsMap.get((exceptionName).toUppercase());
    }
    
     public class EHExceptionToCaseInformation {
     	 public CaseType ehCasetype;
     	 public Boolean isTaskToBeCreated;
     	 public String exceptionName;
     	 public String deliveryType;
     	 public String taskDescription;
         public Boolean isTwoWayEnabled;
         public Decimal extendedPromiseDuration;
         public Decimal extendedPromiseWithBusinessEndDate;
     }

}