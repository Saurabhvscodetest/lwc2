/******************************************************************************
* @author       Matt Povey
* @date         22/12/2015
* @description  Batch class for cleaning up erroneous Loyalty Account records
*               created by MyJL sign up process.  To run in Exec Anon:
*               MyJLLoyaltyAccountCleanBatch b = new MyJLLoyaltyAccountCleanBatch();
*               Database.executeBatch(b);
*               MyJLLoyaltyAccountCleanBatch b = new MyJLLoyaltyAccountCleanBatch();
*               Database.executeBatch(b, 20); // set batch size
*
* EDIT RECORD
*
* LOG     DATE        Author    JIRA        COMMENT      
* 001     22/12/2015  MP        MJL-2154    Original code
* 002     08/07/16    NA      Decommission Contact Profile    
*******************************************************************************
*/
global class MyJLLoyaltyAccountCleanBatch extends MyJLCleanBatchBase implements Database.Batchable<SObject>, Database.Stateful, Schedulable {
    public static final String CLASS_NAME = 'MyJLLoyaltyAccountCleanBatch';
    
    /**
     * Override no args constructor to initialise settings
     * @params: none
     */
    global MyJLLoyaltyAccountCleanBatch() {
        super(CLASS_NAME);
    }
    
    /**
     * execute - Schedule the batch job
     * @params: Database.SchedulableContext sc  Schedulable Context from system.
     */
    global void execute(SchedulableContext sc){
        if (processEnabled) {
            MyJLLoyaltyAccountCleanBatch loyaltyAccountCleanJob = new MyJLLoyaltyAccountCleanBatch();
            Database.executeBatch(loyaltyAccountCleanJob);
        }
    }

    /**
     * Start - Select all Contact Profiles where there is at least one active loyalty account
     * @params: Database.BatchableContext BC    Batchable Context from system.
     */
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([SELECT Id, Name, (SELECT Id, Name, IsActive__c, Join_Date__c, ShopperId__c,
                                                                                   Membership_Number__c, Scheme_Joined_Date_Time__c, 
                                                                                   Activation_Date_Time__c, Deactivation_Date_Time__c, 
                                                                                   contact__c, contact__r.id, 
                                                                                   contact__r.Name, Channel_ID__c 
                                                                            FROM MyJL_Accounts__r WHERE IsActive__c = true ORDER BY Join_Date__c)
                                         FROM contact
                                         WHERE Id IN (SELECT contact__c FROM Loyalty_Account__c WHERE IsActive__c = true and  createddate >= :reportStartDate and createddate <= :reportEndDate )]);  //<<002>>
    }
    
    /**
     * Execute - build list of matching records for reporting and delete if specified by custom setting.
     * @params: Database.BatchableContext BC        Batchable Context from system.
     *          List<Contact_Profile__c> cpList     Batch of Contact Profile with active loyalty accounts
     */
    global void execute(Database.BatchableContext BC, List<contact> cpList){
        if (processEnabled) {
            // Create Map of Loyalty Accounts that should be deactivated
            Map<Id, Loyalty_Account__c> accountsToDeactivateMap = new Map<Id, Loyalty_Account__c>();
            DateTime deactivationDateTime = system.now();
            for (contact cp : cpList) {     //<<002>>
                if (cp.MyJL_Accounts__r.size() > 1) {
                    Integer pos = 0;
                    for (Loyalty_Account__c la : cp.MyJL_Accounts__r) {
                        if (pos > 0) {
                            if (recordUpdatesEnabled) {
                                la.IsActive__c = false;
                                la.Deactivation_Date_Time__c = deactivationDateTime;
                            }
                            accountsToDeactivateMap.put(la.Id, la);
                            totalRecordCount++;
                        }
                        pos++;
                    }
                }
            }

            // Construct report and perform update
            if (emailReportEnabled) {
                
                // Build list of linked Kitchen Drawer transactions
                List<Loyalty_Card__c> linkedCardList = [SELECT Id, Name, Loyalty_Account__c, Card_Number__c, (SELECT Id, Name FROM Receipt_Headers__r) FROM Loyalty_Card__c WHERE Loyalty_Account__c IN :accountsToDeactivateMap.keySet()];
                Map<Id, List<Loyalty_Card__c>> cardToLAMap = new Map<Id, List<Loyalty_Card__c>>();

                for (Loyalty_Card__c lc : linkedCardList) {
                    List<Loyalty_Card__c> tempLCList;
                    if (cardToLAMap.containsKey(lc.Loyalty_Account__c)) {
                        tempLCList = cardToLAMap.get(lc.Loyalty_Account__c);
                        tempLCList.add(lc);
                    } else {
                        tempLCList = new List<Loyalty_Card__c> {lc};
                        cardToLAMap.put(lc.Loyalty_Account__c, tempLCList);
                    }
                }

                if (reportBodyHTML == null) {
                    reportBodyHTML = '<table ' + tableStyle + '>';
                    reportBodyHTML += '<tr>';
                    reportBodyHTML += '<td ' + headerStyle + '><b>Account ID</b></td>';
                    reportBodyHTML += '<td ' + headerStyle + '><b>Contact Name</b></td>';
                    reportBodyHTML += '<td ' + headerStyle + '><b>Contact ID</b></td>';
                    reportBodyHTML += '<td ' + headerStyle + '><b>Profile ID</b></td>';
                    reportBodyHTML += '<td ' + headerStyle + '><b>Shopper ID</b></td>';
                    reportBodyHTML += '<td ' + headerStyle + '><b>Membership No</b></td>';
                    reportBodyHTML += '<td ' + headerStyle + '><b>Join Date</b></td>';
                    reportBodyHTML += '<td ' + headerStyle + '><b>Activation Date</b></td>';
                    reportBodyHTML += '<td ' + headerStyle + '><b>Deactivation Date</b></td>';
                    reportBodyHTML += '<td ' + headerStyle + '><b>Active</b></td>';
                    reportBodyHTML += '<td ' + headerStyle + '><b>Source</b></td>';
                    reportBodyHTML += '<td ' + headerStyle + '><b>Card Number</b></td>';
                    reportBodyHTML += '<td ' + headerStyle + '><b>KD Transactions</b></td>';
                    reportBodyHTML += '</tr>';
                }

                for (Loyalty_Account__c la : accountsToDeactivateMap.values()) {
                    Integer transCount = 0;
                    if (reportBody == null) {
                        reportBody = 'ID: ' + la.Id + ', CONTACT NAME: ' + la.Contact__r.Name + ', CONTACT ID: ' + la.Contact__r.id + ', PROFILE ID: ' + la.contact__c + ', SHOPPER ID: ' + la.ShopperId__c + ', JOIN DATE: ' + la.Join_Date__c + ', KD Transactions: ' + transCount;
                    } else {
                        reportBody += '\nID: ' + la.Id + ', CONTACT NAME: ' + la.Contact__r.Name + ', CONTACT ID: ' + la.Contact__r.id + ', PROFILE ID: ' + la.contact__c + ', SHOPPER ID: ' + la.ShopperId__c + ', JOIN DATE: ' + la.Join_Date__c + ', KD Transactions: ' + transCount;
                    }
                    reportBodyHTML += '<tr>';
                    reportBodyHTML += '<td ' + rowStyle + '>' + la.Id + '</td>';
                    reportBodyHTML += '<td ' + rowStyle + '>' + la.Contact__r.Name + '</td>';
                    reportBodyHTML += '<td ' + rowStyle + '>' + la.Contact__r.id + '</td>';
                    reportBodyHTML += '<td ' + rowStyle + '>' + la.contact__c + '</td>';
                    reportBodyHTML += '<td ' + rowStyle + '>' + la.ShopperId__c + '</td>';
                    reportBodyHTML += '<td ' + rowStyle + '>' + la.Membership_Number__c + '</td>';
                    reportBodyHTML += '<td ' + rowStyle + '>' + la.Scheme_Joined_Date_Time__c + '</td>';
                    reportBodyHTML += '<td ' + rowStyle + '>' + la.Activation_Date_Time__c + '</td>';
                    reportBodyHTML += '<td ' + rowStyle + '>' + la.Deactivation_Date_Time__c + '</td>';
                    reportBodyHTML += '<td ' + rowStyle + '>' + la.IsActive__c + '</td>';
                    reportBodyHTML += '<td ' + rowStyle + '>' + la.Channel_ID__c + '</td>';
                    
                    if (cardToLAMap.containsKey(la.Id)) {
                        List<Loyalty_Card__c> lcList = cardToLAMap.get(la.Id);
                        Integer pos = 0;
                        for (Loyalty_Card__c lc : lcList) {
                            if (pos > 0) {
                                // Put in blanks so no duplication of Contact Profile details
                                reportBodyHTML += '<tr>';
                                addBlankReportCells(9);
                            }
                            reportBodyHTML += '<td ' + rowStyle + '>' + lc.Card_Number__c + '</td>';
                            reportBodyHTML += '<td ' + rowStyle + '>' + lc.Receipt_Headers__r.size() + '</td>';
                            reportBodyHTML += '</tr>';
                            pos++;
                        }
                    } else {
                        // Put in blanks for card detail columns
                        addBlankReportCells(2);
                        reportBodyHTML += '</tr>';
                    }
                }
            }
            if (recordUpdatesEnabled && totalRecordCount > 0) {
                try {
                    update accountsToDeactivateMap.values();
                } catch (Exception ex) {
                    sendErrorEmail(CLASS_NAME + ' FATAL ERROR', ex.getMessage());
                }
            }
        }
    }   
    
    /**
     * Finish - send notification email.
     * @params: Database.BatchableContext BC    Batchable Context from system.
     */
    global void finish(Database.BatchableContext bc){
        // Close off report html table
        if (emailReportEnabled) {
            reportBodyHTML += '</table>';
        }
        sendReportEmail(CLASS_NAME, 'LOYALTY ACCOUNT', 'DEACTIVATED');
    }
}