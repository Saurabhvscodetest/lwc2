/**
* Unit tests for PersonalTemplateJobs class methods.
*/
@isTest
public class PersonalTemplateJobs_TEST 
{
    public static final String CONTACT_CENTRE_TEAM_NAME = 'Hamilton/Didsbury - CST';
	public static final String CONTACT_CENTRE_QUEUE_NAME = 'CST_Hamilton_Didsbury';
    
    @IsTest
    static void testDeleteSingleTemplate()
    {
        
        User runningUser = UserTestDataFactory.getRunningUser();
        
        User contactCentreUser;
        
        Test.startTest();
        System.runAs(runningUser) {
            
            CustomSettingTestDataFactory.initialiseTeamCustomSettings(CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_TEAM_NAME, CONTACT_CENTRE_QUEUE_NAME);
            
            contactCentreUser = UserTestDataFactory.createContactCentreUser();
            insert contactCentreUser;
            
            EmailTemplate testEmailTemplate = new EmailTemplate();
            testEmailTemplate.isActive = true;
            testEmailTemplate.Name = 'TestEmail1';
            testEmailTemplate.DeveloperName = 'unique_name_addSomethingSpecialHere1';
            testEmailTemplate.TemplateType = 'text';
            testEmailTemplate.FolderId = UserInfo.getUserId();		
            insert testEmailTemplate;
            
            PersonalTemplateSettings__c setting = new PersonalTemplateSettings__c();        
            setting.FullName__c = 'JL: ContactCentre Tier2-3' ;
            setting.IsCapita__c = false;
            setting.Active__c = true ;
            setting.Type__c = 'Test';
            setting.Name = 'Test';
            insert setting;
        }
        
        System.runAs(contactCentreUser) {                       
            
            EmailTemplate testEmailTemplate2 = new EmailTemplate();
            testEmailTemplate2.isActive = true;
            testEmailTemplate2.Name = 'TestEmail2';
            testEmailTemplate2.DeveloperName = 'unique_name_addSomethingSpecialHere2';
            testEmailTemplate2.TemplateType = 'text';
            testEmailTemplate2.FolderId = UserInfo.getUserId();		
            insert testEmailTemplate2;
            
        }
        // PersonalTemplateJobs.SweepPersonalTemplates();		
       
        
        Test.stopTest();
        List<EmailTemplate> emailTemplates = [SELECT Id, Name FROM EmailTemplate 
                                              WHERE CreatedDate = LAST_N_DAYS:1 AND
                                              (OwnerId = :runningUser.Id OR OwnerId = :contactCentreUser.Id) Order by CreatedDate DESC  ];
       // System.assertNOTEquals(0, emailTemplates.size());
       // System.assertEquals('TestEmail1', emailTemplates.get(0).Name);
    }        
}