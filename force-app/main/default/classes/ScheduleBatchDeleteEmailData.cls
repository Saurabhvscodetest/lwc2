/* Description  : Schedule class for Delete Task records based on the criteria
* 
* Modification Log
=============================================================================================
* Ver   Date              Author                          Modification           JIRA No
---------------------------------------------------------------------------------------------
* 1.0   20/02/2019        Vignesh Kotha                   Created               COPT-4300
*
*/
global class ScheduleBatchDeleteEmailData implements Schedulable{
   global void execute(SchedulableContext sc){
        BAT_DeleteEmailMessages obj = new BAT_DeleteEmailMessages();
        Database.executebatch(obj);
    }   
}