/**
* @description  This Class is have utility methods which has beed used for cases
*               See COPT-4857 for more information.
* @author       @dpansari  
* @date         2019-08-12
* @version      1.0
*/
/******************************************************************************/
public class CaseUtils 
{
    // This is new method to add the contact while creation for PSE 
    public static void getContactForPSECase(List<case> caseList)
    {
        Set<string> emailSet = new Set<string>();
        Map<string,List<contact>> emailPrimaryContactMap = new Map<string,List<contact>>();
        Map<string,List<contact>> emailSecondaryContactMap = new Map<string,List<contact>>();
        
        for(case objCase :caseList)
        {
            if(objCase.SuppliedEmail!=null && objCase.contactid == null)
            {
                emailSet.add(objCase.SuppliedEmail);
            }
        }
        if(emailSet.size()>0)
        {
            List<Contact> conList =  new List<Contact>();
            conList = [select id,email,Customer_Record_Type_Lex__c from contact where email in :emailSet];
            for(Contact objCon : conList)
            {
                List<contact> tempList = new List<contact>();
                if(objCon.Customer_Record_Type_Lex__c=='Primary')
                {
                    tempList.add(objCon);
                    if(emailPrimaryContactMap.containsKey(objCon.email))
                    {
                        tempList.addALL(emailPrimaryContactMap.get(objCon.email));
                    }
                    emailPrimaryContactMap.put(objCon.email,tempList);
                }
                else if(objCon.Customer_Record_Type_Lex__c=='Secondary')
                {
                    tempList.add(objCon);
                    if(emailSecondaryContactMap.containsKey(objCon.email))
                    {
                        tempList.addALL(emailSecondaryContactMap.get(objCon.email));
                    }
                    emailSecondaryContactMap.put(objCon.email,tempList);
                }       
            }
        }
        /*
        1. One Primary Customer & One or more secondary, then match to Primary Customer 
        2. One Primary Customer, then match to Primary Customer 
        3. No Primary Customer and One secondary customer, then match to Secondary Customer
        4. No Primary Customer and more than one Secondary Customer, then do not match  
        */
        for(Case objCase :caseList)
        {
            if(objCase.SuppliedEmail!=null)
            {
                if(emailPrimaryContactMap.containsKey(objCase.SuppliedEmail))
                {
                    List<contact> tempContactList = new List<contact>();
                    tempContactList = emailPrimaryContactMap.get(objCase.SuppliedEmail);
                    if(tempContactList.size()==1)
                    {
                        objCase.contactid = tempContactList[0].id;
                    }
                }
                else if(emailSecondaryContactMap.containsKey(objCase.SuppliedEmail))
                {
                    List<contact> tempContactList = new List<contact>();
                    tempContactList = emailSecondaryContactMap.get(objCase.SuppliedEmail);
                    if(tempContactList.size()==1)
                    {
                        objCase.contactid = tempContactList[0].id;
                    }   
                }
            }
        }
    }
    // This method is used to map braches as per connex when it comes via online
    // eg. Oxford in Check Store Stock is called Oxford City in Connex
    public static void correctPSEBranchMapping(List<case> caseList)
    {
        Map<string,string> pseBrachMapping = new Map<string,string>();
        for(String keyValue : (system.label.PSEBranchMapping).split(';'))
        {
            List<string> cityMapList = new List<string>();
            cityMapList = keyValue.split(':');
            if(cityMapList.size()==2)
            {
                pseBrachMapping.put(cityMapList[0].trim().toUppercase(),cityMapList[1].trim());
            }
        }
        for(case objCase :caseList)
        {
            if(objCase.Assign_To_New_Branch__c!=null && pseBrachMapping.containsKey((objCase.Assign_To_New_Branch__c).toUppercase()))
            {
               objCase.Assign_To_New_Branch__c = pseBrachMapping.get(objCase.Assign_To_New_Branch__c.toUppercase());
            }
        }
    }

    // Method to change Onwer to user
    // COPT-5324 Closed Case in Omni
    // This Method is used to change owner to current logged in user if
    // Case is closed and owner is still a queue
    @future
    public static void ownerChangeToUserForCloseCase(String userID ,List<id> caseIDList)
    {
        List<case> caseList = new List<case>();
        List<case> caseListToUpdate = new List<case>();
        caseList = [select id , OwnerId from case where isclosed=true and id in :caseIDList];     
        for(case objCase : caseList)
        {
            if(objCase.OwnerId.getSobjectType() != User.getSobjectType())
            {
                objCase.OwnerId = userID;
                caseListToUpdate.add(objCase);
            }       
        }
        if(!caseListToUpdate.isEmpty())
        {
            CommonStaticUtils.runTrigger = false;
            if(!Test.isRunningTest()) {
                database.update(caseListToUpdate,false);
            }
        }
        
    }

    /** This method is used to populate "CustomRequestedDateTime" with current date when case is moved to failed queue 
    -- if "CustomRequestedDateTime" will not reset then it will taking default queue timing for OMNI queue as well.
    -- This will help to move cases exactly when they came to OMNI failed queue
    COPT-5293
    **/
    
    public static void resetOMNIWaitTimeForFailedQueue(Map<ID,Case> newValues , Map<ID,Case> oldValues)
    {
        set<id> caseID = new set<id>(); 
        for (Case newCase : newValues.values()) 
        {
            Case oldCase = oldValues.get(newCase.Id);
            if(oldCase.ownerid != null && newCase.ownerid != null && (oldCase.ownerid != newCase.ownerid) && (oldCase.ownerid.getSobjectType()!=Schema.User.SObjectType) && oldCase.ownerid.getSobjectType() == newCase.ownerid.getSobjectType() && newCase.jl_ListView_QueueName__c<>null && (newCase.jl_ListView_QueueName__c).containsIgnoreCase('FAILED'))  
            {
                caseID.add(newcase.id);
            }               
        }
        if(!caseID.isEmpty())
        {
            List<PendingServiceRouting> psrList = [select id,CustomRequestedDateTime,WorkItemId from PendingServiceRouting where WorkItemId=:caseID];
            for(PendingServiceRouting objPsr :psrList )
            {
                objPsr.CustomRequestedDateTime = system.now();
            }               
            if(!psrList.isEmpty())
            {
                database.update(psrList,false); 
            }
        }           
    }
}