/**
 */
@isTest
private class TestLiveChatTranscriptEventTrigger {

    static testMethod void myUnitTest() {
	TestChatCreateCustomSettings.createCustomSettings(false);

    LiveChatVisitor lcv = new LiveChatVisitor();    
    insert lcv;

    LiveChatTranscript lct = new LiveChatTranscript();
    system.assertNotEquals(null, lct);
    
    lct.Body = 'hello mr customer';
    lct.LiveChatVisitorId = lcv.Id;
    
    insert lct;
    system.assertNotEquals(null, lct.Id);

    LiveChatTranscriptEvent lcte = new LiveChatTranscriptEvent();
    lcte.LiveChatTranscriptId = lct.Id;
    lcte.type = 'Other';
    lcte.Time = system.now();
    
    insert lcte;
    
    try {
      delete lcte;
      system.assertEquals('Error as cannot delete transcript event', 'deleted transcript event');    
    } catch (Exception e) {
      system.assertEquals(true, true);
    }

    }    
}