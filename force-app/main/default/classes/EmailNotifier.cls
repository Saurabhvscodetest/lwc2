/******************************************************************************
* @author       Matt Povey
* @date         11/06/2015
* @description  Simple utility class to send a single email from Salesforce.  Can be used by any classes that
*				need to send a completion notification email or error email.
*
* EDIT RECORD
*
* LOG     DATE        Author	JIRA		COMMENT      
* 001     11/06/2015  MP		CMP-98		Original code
* 002     21/12/2015  MP		MJL-2154	Add in code to use HTML body
* 003     16/09/2016  MP		N/A			Default if setting missing
*/
global class EmailNotifier {

	/**
	 * Send a notification email from Salesforce to default notification user
	 * @params:	String subject					The email subject
	 *			String bodyText					The main email content
	 */
	global static void sendNotificationEmail(String subject, String bodyText) {
        Config_Settings__c emailConfig = Config_Settings__c.getInstance('EMAIL_NOTIFICATION_DEFAULT_ADDRESS');
        String emailNotificationDefaultAddress = emailConfig != null ? emailConfig.Text_Value__c : 'connex.it.support@johnlewis.co.uk';
        List<String> emailRecipientList = emailNotificationDefaultAddress.split(';');
        sendNotificationEmail(emailRecipientList, subject, bodyText);
	}

	/**
	 * Send a notification email from Salesforce
	 * @params:	List<String> emailRecipients	List of email addresses of recipients
	 *			String subject					The email subject
	 *			String bodyText					The main email content
	 */
	global static void sendNotificationEmail(List<String> emailRecipients, String subject, String bodyText) {
        sendNotificationEmail(emailRecipients, subject, bodyText, false);
	}

	/**
	 * Send a notification email from Salesforce <<002>>
	 * @params:	List<String> emailRecipients	List of email addresses of recipients
	 *			String subject					The email subject
	 *			String bodyText					The main email content
	 *			Boolean useHTMLBody				Send the content as HTML rather than plain text
	 */
	global static void sendNotificationEmail(List<String> emailRecipients, String subject, String bodyText, Boolean useHTMLBody) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(emailRecipients);
        mail.setSubject(subject);
        if (useHTMLBody) {
        	mail.setHtmlBody(bodyText);
        } else {
        	mail.setPlainTextBody(bodyText);
        }

		if (!Test.isRunningTest()) {
	        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
		}                         
	}
}