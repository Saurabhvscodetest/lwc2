@isTest
private class CopyCaseEmailMessage_Stage_Test{
	
@testSetup static void setup() {
         UnitTestDataFactory.initialiseContactCentreAndTeamSettings();
    }	

private static final String TEXT_WITH_REF = 'This is an auto-generated email, please do not reply ref:_00Dm011SJ._500m01dzel:ref mary had a little lamb';
  private static final String TEXT_NO_REF = 'Mary had a little lamb';
  private static final String HTML_WITH_REF = '<html><head/><body>'+TEXT_WITH_REF+'</body></html>';
  private static final String HTML_NO_REF = '<html><head/><body>'+TEXT_NO_REF+'</body></html>';
  private static final String TO_ADDRESS = 'terry453@terry453.com.tv';
  
 static EmailMessage createEmailMessage(Case c, Boolean incoming, String textBody, String htmlBody, String toAddress) {
    EmailMessage em = new EmailMessage();
    em.ParentId = c.Id;
    em.Incoming = incoming;
    em.ToAddress = toAddress;
    em.Status = '3';
    if (textBody != null) {
      em.TextBody = textBody;
    }
    if (htmlBody != null) {
      em.htmlBody = htmlBody;
    }
    insert em;
    system.assertNotEquals(null, em.Id);
    return em;
  }

static testMethod void testStartMethod_Stage1()
{
    Account testAccount = UnitTestDataFactory.createAccount(1, true);
    Contact masterCon = UnitTestDataFactory.createContact(1, testAccount.id, false);
    Case testCase1 = UnitTestDataFactory.createQueryCase(masterCon.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, true);
    EmailMessage ems = createEmailMessage(testCase1 ,false,TEXT_WITH_REF,HTML_WITH_REF,TO_ADDRESS);
    String emid = ems.id;
    testCase1.Pending_Email_Messages__c =emid;
    update testCase1;
    Database.BatchableContext bc = null;
    CopyCaseEmailMessages_FirstStage cceM= new CopyCaseEmailMessages_FirstStage();
    Test.startTest();
     Database.QueryLocator dql = cceM.start(bc);  
    Test.stopTest();
}

static testMethod void testExecuteMethod_Stage1()
{
 Account testAccount = UnitTestDataFactory.createAccount(2, true);
    Contact masterCon = UnitTestDataFactory.createContact(2, testAccount.id, false);
    Case testCase1 = UnitTestDataFactory.createQueryCase(masterCon.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, true);
    EmailMessage ems = createEmailMessage(testCase1 ,false,TEXT_WITH_REF,HTML_WITH_REF,TO_ADDRESS);
    String emid = ems.id;
    testCase1.Pending_Email_Messages__c =emid;
    update testCase1;
    Database.BatchableContext bc = null;
    CopyCaseEmailMessages_FirstStage cceM= new CopyCaseEmailMessages_FirstStage();
    List<Case> casListToProcess = new List<Case>();
    casListToProcess.add(testCase1);
    Test.startTest();
     cceM.execute(bc,casListToProcess);  
    Test.stopTest();
}

static testMethod void testStartMethod_Stage2()
{
Account testAccount = UnitTestDataFactory.createAccount(3, true);
    Contact masterCon = UnitTestDataFactory.createContact(3, testAccount.id, false);
    Case testCase1 = UnitTestDataFactory.createQueryCase(masterCon.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, true);
    EmailMessage ems = createEmailMessage(testCase1 ,false,TEXT_WITH_REF,HTML_WITH_REF,TO_ADDRESS);
    String emid = ems.id;
    testCase1.Pending_Email_Messages__c =emid;
    update testCase1;
    Database.BatchableContext bc = null;
    Set<Id> allcaseId = new Set<Id>();
    allcaseId.add(testCase1.id);
    Set<String> allemailMessageId= new Set<String>();
    allemailMessageId.add(emid);
    CopyCaseEmailMessages_SecondStage cceM= new CopyCaseEmailMessages_SecondStage(allcaseId,allemailMessageId);
    Test.startTest();
     Database.QueryLocator dql = cceM.start(bc);  
    Test.stopTest();
}


static testMethod void testExecuteMethod_Stage2()
{
     Account testAccount = UnitTestDataFactory.createAccount(4, true);
    Contact masterCon = UnitTestDataFactory.createContact(4, testAccount.id, false);
    Case testCase1 = UnitTestDataFactory.createQueryCase(masterCon.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, true);
    EmailMessage ems = createEmailMessage(testCase1 ,false,TEXT_WITH_REF,HTML_WITH_REF,TO_ADDRESS);
    String emid = ems.id;
    testCase1.Pending_Email_Messages__c =emid;
    update testCase1;
    List<EmailMessage> allEmailMessage= new List<EmailMessage>();
    allEmailMessage.add(ems);
    Database.BatchableContext bc = null;  
    Set<Id> allcaseId = new Set<Id>();
    allcaseId.add(testCase1.id);
    Set<String> allemailMessageId= new Set<String>();
    allemailMessageId.add(emid);  
    CopyCaseEmailMessages_SecondStage cceM= new CopyCaseEmailMessages_SecondStage(allcaseId,allemailMessageId);
    Test.startTest();
     cceM.execute(bc,allEmailMessage);  
    Test.stopTest();
}

static testMethod void testStartMethod_Stage3()
{
Account testAccount = UnitTestDataFactory.createAccount(5, true);
    Contact masterCon = UnitTestDataFactory.createContact(5, testAccount.id, false);
    Case testCase1 = UnitTestDataFactory.createQueryCase(masterCon.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, true);
    EmailMessage ems = createEmailMessage(testCase1 ,false,TEXT_WITH_REF,HTML_WITH_REF,TO_ADDRESS);
    String emid = ems.id;
    Attachment attach=new Attachment();     
    attach.Name='Unit Test Attachment';
    Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    attach.body=bodyBlob;
    attach.parentId=ems.id;
    insert attach;    
    testCase1.Pending_Email_Messages__c =emid;
    update testCase1;
    Database.BatchableContext bc = null;
    Set<Id> allcaseId = new Set<Id>();
    allcaseId.add(testCase1.id);
    Set<String> allemailMessageId= new Set<String>();
    allemailMessageId.add(emid);
    Map<Id,Id> emailMessageCaseMap = new Map<Id,Id>();
    emailMessageCaseMap.put(ems.id,testCase1.id);
    CopyCaseEmailMessages_ThirdStage cceM= new CopyCaseEmailMessages_ThirdStage(allcaseId,allemailMessageId,emailMessageCaseMap);
    Test.startTest();
     Database.QueryLocator dql = cceM.start(bc);  
    Test.stopTest();
}

static testMethod void testExecuteMethod_Stage3()
{
Account testAccount = UnitTestDataFactory.createAccount(6, true);
    Contact masterCon = UnitTestDataFactory.createContact(6, testAccount.id, false);
    Case testCase1 = UnitTestDataFactory.createQueryCase(masterCon.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, true);
    EmailMessage ems = createEmailMessage(testCase1 ,false,TEXT_WITH_REF,HTML_WITH_REF,TO_ADDRESS);
    String emid = ems.id;
    Attachment attach=new Attachment();     
    attach.Name='Unit Test Attachment';
    Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    attach.body=bodyBlob;
    attach.parentId=ems.id;
    insert attach;    
    List<Attachment> listAtt = new List<Attachment>();
    listAtt.add(attach);
    testCase1.Pending_Email_Messages__c =emid;
    update testCase1;
    Database.BatchableContext bc = null;
    Set<Id> allcaseId = new Set<Id>();
    allcaseId.add(testCase1.id);
    Set<String> allemailMessageId= new Set<String>();
    allemailMessageId.add(emid);
    Map<Id,Id> emailMessageCaseMap = new Map<Id,Id>();
    emailMessageCaseMap.put(ems.id,testCase1.id);
    CopyCaseEmailMessages_ThirdStage cceM= new CopyCaseEmailMessages_ThirdStage(allcaseId,allemailMessageId,emailMessageCaseMap);
    Test.startTest();
     cceM.execute(bc,listAtt);  
    Test.stopTest();

}

static testMethod void testStartMethod_StageLast()
{
        Account testAccount = UnitTestDataFactory.createAccount(7, true);
    Contact masterCon = UnitTestDataFactory.createContact(7, testAccount.id, false);
    Case testCase1 = UnitTestDataFactory.createQueryCase(masterCon.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, true);
    EmailMessage ems = createEmailMessage(testCase1 ,false,TEXT_WITH_REF,HTML_WITH_REF,TO_ADDRESS);
    String emid = ems.id;
    testCase1.Pending_Email_Messages__c =emid;
    update testCase1;
    List<EmailMessage> allEmailMessage= new List<EmailMessage>();
    allEmailMessage.add(ems);
    Set<Id> allcaseId = new Set<Id>();
    allcaseId.add(testCase1.id);
    Database.BatchableContext bc = null;
    CopyCaseEmailMessages_FinalStage cceM= new CopyCaseEmailMessages_FinalStage(allcaseId);
    Test.startTest();   
     Database.QueryLocator dql = cceM.start(bc);  
    Test.stopTest();
}

static testMethod void testExecuteMethod_StageLast()
{
    Account testAccount = UnitTestDataFactory.createAccount(8, true);
    Contact masterCon = UnitTestDataFactory.createContact(8, testAccount.id, false);
    Case testCase1 = UnitTestDataFactory.createQueryCase(masterCon.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, true);
    EmailMessage ems = createEmailMessage(testCase1 ,false,TEXT_WITH_REF,HTML_WITH_REF,TO_ADDRESS);
    String emid = ems.id;
    testCase1.Pending_Email_Messages__c =emid;
    update testCase1;
    List<EmailMessage> allEmailMessage= new List<EmailMessage>();
    allEmailMessage.add(ems);
    Set<Id> allcaseId = new Set<Id>();
    allcaseId.add(testCase1.id);
    Database.BatchableContext bc = null;
    List<Case> allCases= new List<Case>();
    allCases.add(testCase1);
    CopyCaseEmailMessages_FinalStage cceM= new CopyCaseEmailMessages_FinalStage(allcaseId);
    Test.startTest();
     cceM.execute(bc,allCases);  
    Test.stopTest();
}

static testMethod void testFinalMethod_allStages()
{
        Account testAccount = UnitTestDataFactory.createAccount(9, true);
    Contact masterCon = UnitTestDataFactory.createContact(9, testAccount.id, false);
    Case testCase1 = UnitTestDataFactory.createQueryCase(masterCon.id, 'JohnLewis.com', 'JL.com - Collection', 'JL.com - Green Van Fleet', 'JL.com - Bookings', true, true);
    EmailMessage ems = createEmailMessage(testCase1 ,false,TEXT_WITH_REF,HTML_WITH_REF,TO_ADDRESS);
    String emid = ems.id;
    Attachment attach=new Attachment();     
    attach.Name='Unit Test Attachment';
    Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    attach.body=bodyBlob;
    attach.parentId=ems.id;
    insert attach;    
    List<Attachment> listAtt = new List<Attachment>();
    listAtt.add(attach);
    testCase1.Pending_Email_Messages__c =emid;
    update testCase1;
    Database.BatchableContext bc = null;
    Set<Id> allcaseId = new Set<Id>();
    allcaseId.add(testCase1.id);
    Set<String> allemailMessageId= new Set<String>();
    allemailMessageId.add(emid);
    Map<Id,Id> emailMessageCaseMap = new Map<Id,Id>();
    emailMessageCaseMap.put(ems.id,testCase1.id);
    CopyCaseEmailMessages_FirstStage frstM= new CopyCaseEmailMessages_FirstStage();
    CopyCaseEmailMessages_SecondStage secondM= new CopyCaseEmailMessages_SecondStage(allcaseId,allemailMessageId);
     CopyCaseEmailMessages_ThirdStage thirdM= new CopyCaseEmailMessages_ThirdStage(allcaseId,allemailMessageId,emailMessageCaseMap);
     CopyCaseEmailMessages_FinalStage finalM= new CopyCaseEmailMessages_FinalStage(allcaseId);
    Test.startTest();
    frstM.finish(bc);  
     secondM.finish(bc);  
     thirdM.finish(bc);  
     finalM.finish(bc);  
    Test.stopTest();
}


}